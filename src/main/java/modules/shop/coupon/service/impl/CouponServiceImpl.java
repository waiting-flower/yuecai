package modules.shop.coupon.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.HqlQuery;
import common.util.StringUtil;
import modules.shop.ad.entity.AdEntity;
import modules.shop.coupon.entity.CouponEntity;
import modules.shop.coupon.service.CouponService;

@Service
public class CouponServiceImpl extends CommonServiceImpl implements CouponService{

	@Override
	public DataTableReturn loadCouponByPage(JQueryDataTables dataTable, HttpServletRequest request) {
		StringBuffer hql = new StringBuffer(
				" from CouponEntity as  a where 1 = 1  and a.status = '1' ");
		List<Object> params = new ArrayList<Object>();
		String name = request.getParameter("name");
		if (StringUtil.isNotEmpty(name)) {
			hql.append(" and a.name like ? ");
			params.add("%" + name + "%");
		}
		hql.append(" order by a.createDate desc");
		HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
		int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
		hqlQuery.setCurPage(currPage + 1);
		hqlQuery.setPageSize(dataTable.getDisplayLength());
		DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
		List<CouponEntity> pList = (List<CouponEntity>) d.getAaData();
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		for (CouponEntity p : pList) {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("id", p.getId());
			/*
			 *  * private String name;//优惠券名称 10元优惠券
	private Double money;//优惠券金额  10
	private Double conditionMoney;//需要满足的条件金额 100
	private Date beginTime;//开始时间
	private Date endTime;//结束时间
	private Integer allNum;//总数量
	private Integer leftNum;//剩余数量
			 */
			item.put("name", p.getName());
			item.put("tips", p.getTips());
			item.put("money", p.getMoney());
			item.put("conditionMoney", p.getConditionMoney());
			item.put("beginTime", DateUtil.getTimeByCustomPattern(p.getBeginTime(), DateUtil.yyyyMMddHHmmss));
			item.put("endTime", DateUtil.getTimeByCustomPattern(p.getEndTime(), DateUtil.yyyyMMddHHmmss));
			item.put("allNum", p.getAllNum());//当前数量
			item.put("joinNum", p.getJoinNum());//已经领取数量
			item.put("status", p.getStatus());
			item.put("time",DateUtil.getTimeByCustomPattern( p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
			mapList.add(item);
		}
		d.setAaData(mapList);
		d.setsEcho(dataTable.getEcho());
		return d;
	}

	@Override
	public Map<String, Object> saveCoupon(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String money = request.getParameter("money");
		String conditionMoney = request.getParameter("conditionMoney");
		String beginTime = request.getParameter("beginTime");
		String endTime = request.getParameter("endTime");
		String allNum = request.getParameter("allNum");
		String tips = request.getParameter("tips");
		
		CouponEntity a = null;
		if(StringUtil.isNotEmpty(id)) {
			a = get(CouponEntity.class, Long.valueOf(id));
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
		}
		else {
			a = new CouponEntity();
			a.setCreateDate(DateUtil.getCurrentTime());
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
			a.setJoinNum(0);
		}
		a.setName(name);
		a.setTips(tips);
		a.setMoney(Double.valueOf(money));
		a.setConditionMoney(Double.valueOf(conditionMoney));
		a.setBeginTime(DateUtil.parse(beginTime, DateUtil.yyyyMMddHHmmss));
		a.setEndTime(DateUtil.parse(endTime, DateUtil.yyyyMMddHHmmss));
		a.setAllNum(Integer.parseInt(allNum));
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> deleteCoupon(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		CouponEntity a = get(CouponEntity.class, Long.valueOf(id));
		if (a == null) { 
			map.put("code", 1);
			return map;
		}
		a.setStatus("0");
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

}
