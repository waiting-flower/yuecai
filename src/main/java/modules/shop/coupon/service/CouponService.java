package modules.shop.coupon.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;

public interface CouponService extends CommonService{

	DataTableReturn loadCouponByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveCoupon(HttpServletRequest request);

	Map<String, Object> deleteCoupon(HttpServletRequest request);

}
