package modules.shop.coupon.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.util.StringUtil;
import modules.shop.ad.entity.AdEntity;
import modules.shop.coupon.entity.CouponEntity;
import modules.shop.coupon.service.CouponService;

/**
 * 优惠券，web控制层
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "admin/shop/coupon")
public class CouponController extends BaseController{
	
	@Autowired
	CouponService service; 
	
	

	/**
	 *商城优惠券对象列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list")
	public String cateList( HttpServletRequest request, HttpServletResponse response) {
		return templatePath + "shop/coupon/couponList";
	}
	
	/**
	 * 商城优惠券对象详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "info")
	public String info( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			CouponEntity menu = service.get(CouponEntity.class, Long.valueOf(id));
			request.setAttribute("a", menu);
		} 
		return templatePath + "shop/coupon/couponInfo";
	}
	
	
	/**
	 * 分页获取商城优惠券对象列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadCouponByPage")
	@ResponseBody
	public DataTableReturn loadCouponByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = service.loadCouponByPage(dataTable, request);
		return d;
	}
	
	/**
	 * 保存一个商城优惠券对象
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveCoupon")
	@ResponseBody
	public Map<String, Object> saveCoupon(HttpServletRequest request) {
		return service.saveCoupon(request);
	}
	
	/**
	 * 删除一个商城优惠券对象
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteCoupon")
	@ResponseBody
	public Map<String, Object> deleteCoupon(HttpServletRequest request) {
		return service.deleteCoupon(request);
	}
	
}
