package modules.shop.coupon.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
/**
 * 用户的优惠券列表
 * @author Administrator
 *
 */
import modules.shop.user.entity.WUserEntity;
@Entity 
@Table(name = "shop_user_coupon", schema = "")
public class WUserCouponEntity extends CommonEntity{
	private WUserEntity user;
	private CouponEntity coupon;
	private String isUsed;//是否使用  0：未使用  1：已使用
	private Date usedTime;//使用时间
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public WUserEntity getUser() {
		return user;
	}
	public void setUser(WUserEntity user) {
		this.user = user;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "couponId")
	public CouponEntity getCoupon() {
		return coupon;
	}
	public void setCoupon(CouponEntity coupon) {
		this.coupon = coupon;
	}
	public String getIsUsed() {
		return isUsed;
	}
	public void setIsUsed(String isUsed) {
		this.isUsed = isUsed;
	}
	public Date getUsedTime() {
		return usedTime;
	}
	public void setUsedTime(Date usedTime) {
		this.usedTime = usedTime;
	}
	
}
