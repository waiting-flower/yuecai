package modules.shop.coupon.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import common.entity.CommonEntity;

/**
 * 优惠券对象
 * @author Administrator
 *
 */
@Entity 
@Table(name = "shop_coupon", schema = "")
public class CouponEntity extends CommonEntity{
	private String name;//优惠券名称 10元优惠券
	private String tips;//优惠券说明
	private Double money;//优惠券金额  10
	private Double conditionMoney;//需要满足的条件金额 100
	private Date beginTime;//开始时间
	private Date endTime;//结束时间
	private Integer allNum;//总数量
	private Integer joinNum;//已领取数量
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	public Double getConditionMoney() {
		return conditionMoney;
	}
	public void setConditionMoney(Double conditionMoney) {
		this.conditionMoney = conditionMoney;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Integer getAllNum() {
		return allNum;
	}
	public void setAllNum(Integer allNum) {
		this.allNum = allNum;
	}
	public Integer getJoinNum() {
		return joinNum;
	}
	public void setJoinNum(Integer joinNum) {
		this.joinNum = joinNum;
	}
	public String getTips() {
		return tips;
	}
	public void setTips(String tips) {
		this.tips = tips;
	}
	
}
