package modules.shop.action.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
import modules.shop.goods.entity.BrandEntity;

/**
 * 品牌满赠活动，选择的品牌
 * @author Administrator
 *
 */
@Entity
@Table(name = "shop_action_brand", schema = "")
public class ActionBrandEntity extends CommonEntity{
	private ActionEntity action;
	private BrandEntity brand;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "actionId")
	public ActionEntity getAction() {
		return action;
	}
	public void setAction(ActionEntity action) {
		this.action = action;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "brandId")
	public BrandEntity getBrand() {
		return brand;
	}
	public void setBrand(BrandEntity brand) {
		this.brand = brand;
	}
	
}
