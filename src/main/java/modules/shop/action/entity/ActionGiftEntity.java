package modules.shop.action.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
import modules.shop.gift.entity.GiftEntity;


/**
 * 活动的赠品对象
 * @author Administrator
 *
 */
@Entity
@Table(name = "shop_action_gift", schema = "")
public class ActionGiftEntity extends CommonEntity{
	private ActionEntity action;
	private GiftEntity gift;


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "actionId")
	public ActionEntity getAction() {
		return action;
	}
	public void setAction(ActionEntity action) {
		this.action = action;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "giftId")
	public GiftEntity getGift() {
		return gift;
	}
	public void setGift(GiftEntity gift) {
		this.gift = gift;
	}



}
