package modules.shop.action.entity;

import common.entity.CommonEntity;
import modules.shop.goods.entity.GoodsEntity;

import javax.persistence.*;

/**
 * 商品满赠活动，参与的商品
 *
 * @author Administrator
 */
@Entity
@Table(name = "shop_action_goods", schema = "")
public class ActionGoodsEntity extends CommonEntity {
    private ActionEntity action;
    private GoodsEntity goods;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "actionId")
    public ActionEntity getAction() {
        return action;
    }

    public void setAction(ActionEntity action) {
        this.action = action;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "goodsId")
    public GoodsEntity getGoods() {
        return goods;
    }

    public void setGoods(GoodsEntity goods) {
        this.goods = goods;
    }

}
