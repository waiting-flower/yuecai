package modules.shop.action.entity;

import common.entity.CommonEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * 商城活动对象
 * @author Administrator
 *
 */
@Entity
@Table(name = "shop_action", schema = "")
public class ActionEntity extends CommonEntity{
	private String name;//活动名称
	private String actionType;//活动类型 1:品牌满赠  2：商品满赠
	private Date beginTime;//开始时间
	private Date endTime;//结束时间
	private Double conditionMoney;//满足的条件金额 满足100元
	private String giftType;//赠品方式  1：指定N件   2：只有一件（暂时giftType=2）
	private Integer giftNum;//赠送的数量  2-N（giftNum=1）
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Double getConditionMoney() {
		return conditionMoney;
	}
	public void setConditionMoney(Double conditionMoney) {
		this.conditionMoney = conditionMoney;
	}
	public String getGiftType() {
		return giftType;
	}
	public void setGiftType(String giftType) {
		this.giftType = giftType;
	}
	public Integer getGiftNum() {
		return giftNum;
	}
	public void setGiftNum(Integer giftNum) {
		this.giftNum = giftNum;
	}
	
	
}
