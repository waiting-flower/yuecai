package modules.shop.action.controller;

import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.util.StringUtil;
import modules.shop.action.entity.ActionBrandEntity;
import modules.shop.action.entity.ActionEntity;
import modules.shop.action.entity.ActionGiftEntity;
import modules.shop.action.entity.ActionGoodsEntity;
import modules.shop.action.service.ActionService;
import modules.shop.goods.entity.BrandEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 商城活动web控制层
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "admin/shop/action")
public class ActionController extends BaseController{

	@Autowired
	ActionService service;
	
	
	/**
	 * 活动管理列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list")
	public String cateList( HttpServletRequest request, HttpServletResponse response) {
		return templatePath + "shop/action/actionList";
	}
	
	/**
	 * 活动详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "info")
	public String info( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			ActionEntity menu = service.get(ActionEntity.class, Long.valueOf(id));
			request.setAttribute("a", menu);
			if(menu.getActionType().equals("1")) {
				//品牌满赠活动，获得品牌id
				String hql = "from ActionBrandEntity as a where a.action.id=" + id;
				List<ActionBrandEntity> abList = service.findByQueryString(hql);
				String brandIds = "";
				for(ActionBrandEntity gift : abList) {
					brandIds = brandIds + gift.getBrand().getId() + ",";
				}
				brandIds = brandIds.substring(0, brandIds.length() - 1);
				request.setAttribute("brandIds", brandIds);
			}
			else {
				//商品满赠活动，获得商品信息
				String hql = "from ActionGoodsEntity as a where a.action.id=" + id;
				String goodsIds = "";
				List<ActionGoodsEntity> goodsList = service.findByQueryString(hql);
				request.setAttribute("goodsList", goodsList);
				for(ActionGoodsEntity gift : goodsList) {
					goodsIds = goodsIds + gift.getGoods().getId() + ",";
				}
				goodsIds = goodsIds.substring(0, goodsIds.length() - 1);
				request.setAttribute("goodsIds", goodsIds);
			}
			//获得赠品信息
			String hql = "from ActionGiftEntity as a where a.action.id=" + id;
			String giftIds = "";
			List<ActionGiftEntity> giftList = service.findByQueryString(hql);
			for(ActionGiftEntity gift : giftList) {
				giftIds = giftIds + gift.getGift().getId() + ",";
			}
			giftIds = giftIds.substring(0, giftIds.length() - 1);
			request.setAttribute("giftIds", giftIds);
			request.setAttribute("giftList", giftList);
		} 
		String hql = "from BrandEntity as a where a.status = '1' order by a.orderNum desc";
		List<BrandEntity> brandList = service.findByQueryString(hql);
		request.setAttribute("brandList", brandList);
		return templatePath + "shop/action/actionInfo";
	}
	
	
	/**
	 * 分页获取商城活动列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadActionByPage")
	@ResponseBody
	public DataTableReturn loadActionByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = service.loadActionByPage(dataTable, request);
		return d;
	}
	
	/**
	 * 保存一个商城活动
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveAction")
	@ResponseBody
	public Map<String, Object> saveAction(HttpServletRequest request) {
		return service.saveAction(request);
	}
	
	/**
	 * 删除一个商城活动
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteAction")
	@ResponseBody
	public Map<String, Object> deleteAction(HttpServletRequest request) {
		return service.deleteAction(request);
	}
	
	
	/**
	 * 满赠活动  选择商品
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "goodsChoiceList")
	public String goodsChoiceList( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			ActionEntity a = service.get(ActionEntity.class, Long.valueOf(id));
			request.setAttribute("a", a); 
		} 
		return templatePath + "shop/action/goodsChoiceList";
	}
	
	/**
	 * 分页获取满赠活动要去选择的商品列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadGoodsByPage")
	@ResponseBody
	public DataTableReturn loadGoodsByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = service.loadGoodsByPage(dataTable, request);
		return d;
	}
	
	
	
	
	/**
	 * 满赠活动  选择赠品
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "giftChoiceList")
	public String giftChoiceList( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			ActionEntity a = service.get(ActionEntity.class, Long.valueOf(id));
			request.setAttribute("a", a); 
		} 
		String maxLen = request.getParameter("maxLen");
		request.setAttribute("maxLen", maxLen);
		return templatePath + "shop/action/giftChoiceList";
	}
	
	/**
	 * 分页获取满赠活动要去选择的赠品列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadGiftByPage")
	@ResponseBody
	public DataTableReturn loadGiftByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = service.loadGiftByPage(dataTable, request);
		return d;
	}
	
	
}
