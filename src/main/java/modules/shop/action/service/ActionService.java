package modules.shop.action.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;

public interface ActionService extends CommonService{

	DataTableReturn loadActionByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveAction(HttpServletRequest request);

	Map<String, Object> deleteAction(HttpServletRequest request);

	DataTableReturn loadGoodsByPage(JQueryDataTables dataTable, HttpServletRequest request);

	DataTableReturn loadGiftByPage(JQueryDataTables dataTable, HttpServletRequest request);

}
