package modules.shop.action.service.impl;

import cn.hutool.core.lang.Console;
import cn.hutool.json.JSON;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.HqlQuery;
import common.util.StringUtil;
import modules.shop.action.entity.ActionBrandEntity;
import modules.shop.action.entity.ActionEntity;
import modules.shop.action.entity.ActionGiftEntity;
import modules.shop.action.entity.ActionGoodsEntity;
import modules.shop.action.service.ActionService;
import modules.shop.gift.entity.GiftEntity;
import modules.shop.goods.entity.BrandEntity;
import modules.shop.goods.entity.GoodsEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ActionServiceImpl extends CommonServiceImpl implements ActionService {

    @Override
    public DataTableReturn loadActionByPage(JQueryDataTables dataTable, HttpServletRequest request) {
        StringBuffer hql = new StringBuffer(
                " from ActionEntity as  a where 1 = 1  and a.status = '1' ");
        List<Object> params = new ArrayList<Object>();
        String name = request.getParameter("name");
        if (StringUtil.isNotEmpty(name)) {
            hql.append(" and a.name like ? ");
            params.add("%" + name + "%");
        }
        hql.append(" order by a.createDate desc");
        HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
        int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
        hqlQuery.setCurPage(currPage + 1);
        hqlQuery.setPageSize(dataTable.getDisplayLength());
        DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
        List<ActionEntity> pList = (List<ActionEntity>) d.getAaData();
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (ActionEntity p : pList) {
            Map<String, Object> item = new HashMap<String, Object>();
            item.put("id", p.getId());
			/*
			 * private String name;//活动名称
	private String actionType;//活动类型 1:品牌满赠  2：商品满赠
	private Date beginTime;//开始时间
	private Date endTime;//结束时间
	private Double conditionMoney;//满足的条件金额 满足100元
	private String giftType;//赠品方式  1：指定N件   2：只有一件
	private Integer giftNum;//赠送的数量  2-N
			 */
            item.put("name", p.getName());
            item.put("actionType", p.getActionType().equals("1") ? "品牌满赠" : "商品满赠");
            item.put("beginTime", DateUtil.getTimeByCustomPattern(p.getBeginTime(), DateUtil.yyyyMMddHHmmss));
            item.put("endTime", DateUtil.getTimeByCustomPattern(p.getEndTime(), DateUtil.yyyyMMddHHmmss));
            item.put("conditionMoney", p.getConditionMoney());
            item.put("giftType", p.getGiftType().equals("1") ? "指定N件" : "只有一件");
            item.put("giftNum", p.getGiftNum());
            item.put("status", p.getStatus());
            item.put("time", DateUtil.getTimeByCustomPattern(p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
            mapList.add(item);
        }
        d.setAaData(mapList);
        d.setsEcho(dataTable.getEcho());
        return d;
    }

    @Override
    public Map<String, Object> saveAction(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String actionType = request.getParameter("actionType");
        String beginTime = request.getParameter("beginTime");
        String endTime = request.getParameter("endTime");
        String conditionMoney = request.getParameter("conditionMoney");
        String giftType = request.getParameter("giftType");
        String giftNum = request.getParameter("giftNum");
        String brandList = request.getParameter("brandList");
        String goodsIds = request.getParameter("goodsIds");
        String giftIds = request.getParameter("giftIds");
		
		/*
		 * private String name;//活动名称
	private String actionType;//活动类型 1:品牌满赠  2：商品满赠
	private Date beginTime;//开始时间
	private Date endTime;//结束时间
	private Double conditionMoney;//满足的条件金额 满足100元
	private String giftType;//赠品方式  1：指定N件   2：只有一件
	private Integer giftNum;//赠送的数量  2-N
	brandList : brandList != null ? brandList.join(","):"",
    			goodsIds : goodsIds,
    			giftIds : giftIds,
		 */
        ActionEntity a = null;
        if (StringUtil.isNotEmpty(id)) {
            a = get(ActionEntity.class, Long.valueOf(id));
            a.setUpdateDate(DateUtil.getCurrentTime());
            a.setStatus("1");
        } else {
            a = new ActionEntity();
            a.setCreateDate(DateUtil.getCurrentTime());
            a.setUpdateDate(DateUtil.getCurrentTime());
            a.setStatus("1");
        }
        a.setName(name);
        a.setActionType(actionType);
        a.setBeginTime(DateUtil.parse(beginTime, DateUtil.yyyyMMddHHmmss));
        a.setEndTime(DateUtil.parse(endTime, DateUtil.yyyyMMddHHmmss));
        a.setConditionMoney(Double.valueOf(conditionMoney));
        a.setGiftType(giftType);
        a.setGiftNum(Integer.parseInt(giftNum));
        saveOrUpdate(a);
        if ("1".equals(actionType)) {
            //1、品牌满赠活动
            //删除原有的
            String hql = "from ActionBrandEntity as a where a.action.id=" + a.getId();
            List<ActionBrandEntity> list = findByQueryString(hql);
            deleteAllEntitie(list);
            //添加新的
            String brandIds[] = brandList.split(",");
            for (String brandId : brandIds) {
                ActionBrandEntity ab = new ActionBrandEntity();
                ab.setCreateDate(DateUtil.getCurrentTime());
                ab.setUpdateDate(DateUtil.getCurrentTime());
                ab.setStatus("1");
                ab.setAction(a);
                ab.setBrand(get(BrandEntity.class, Long.valueOf(brandId)));
                save(ab);
            }
        } else {
            //2、商品满赠活动
            //删除原有的
            String hql = "from ActionGoodsEntity as a where a.action.id=" + a.getId();
            List<ActionGoodsEntity> list = findByQueryString(hql);
            deleteAllEntitie(list);
            //添加新的
            String goodsIdArr[] = goodsIds.split(",");
            for (String goodsId : goodsIdArr) {
                ActionGoodsEntity ab = new ActionGoodsEntity();
                ab.setCreateDate(DateUtil.getCurrentTime());
                ab.setUpdateDate(DateUtil.getCurrentTime());
                ab.setStatus("1");
                ab.setAction(a);
                ab.setGoods(get(GoodsEntity.class, Long.valueOf(goodsId)));
                save(ab);
            }
        }

        //3、处理赠品信息
        //删除原有的
        String hql = "from ActionGiftEntity as a where a.action.id=" + a.getId();
        List<ActionGiftEntity> list = findByQueryString(hql);
        deleteAllEntitie(list);
        //添加新的
        String giftIdArr[] = giftIds.split(",");
        for (String giftId : giftIdArr) {
            ActionGiftEntity ab = new ActionGiftEntity();
            ab.setCreateDate(DateUtil.getCurrentTime());
            ab.setUpdateDate(DateUtil.getCurrentTime());
            ab.setStatus("1");
            ab.setAction(a);
            ab.setGift(get(GiftEntity.class, Long.valueOf(giftId)));
            save(ab);
        }
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> deleteAction(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String id = request.getParameter("id");
        ActionEntity a = get(ActionEntity.class, Long.valueOf(id));
        if (a == null) {
            map.put("code", 1);
            return map;
        }
        a.setStatus("0");
        saveOrUpdate(a);
        map.put("code", 0);
        return map;
    }

    @Override
    public DataTableReturn loadGoodsByPage(JQueryDataTables dataTable, HttpServletRequest request) {
        StringBuffer hql = new StringBuffer(
                " from GoodsEntity as a where 1 = 1  and a.status = '1' ");
        List<Object> params = new ArrayList<Object>();
        String name = request.getParameter("name");
        if (StringUtil.isNotEmpty(name)) {
            hql.append(" and a.name like ? ");
            params.add("%" + name + "%");
        }
        hql.append(" order by a.createDate desc");
        System.out.println(params.toArray());
        HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
        int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
        hqlQuery.setCurPage(currPage + 1);
        hqlQuery.setPageSize(dataTable.getDisplayLength());
        DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
        List<GoodsEntity> pList = (List<GoodsEntity>) d.getAaData();
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (GoodsEntity p : pList) {
            Map<String, Object> item = new HashMap<String, Object>();
            item.put("id", p.getId());
            item.put("name", p.getName());
            item.put("imgUrl", p.getImgUrl());
            item.put("stockNum", p.getStockNum());
            item.put("price", p.getPrice());
            item.put("groupState", "0");//未参与秒杀
            item.put("time", DateUtil.getTimeByCustomPattern(p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
            mapList.add(item);
        }
        d.setAaData(mapList);
        d.setsEcho(dataTable.getEcho());
        return d;
    }

    @Override
    public DataTableReturn loadGiftByPage(JQueryDataTables dataTable, HttpServletRequest request) {
        StringBuffer hql = new StringBuffer(" from GiftEntity as  a where 1 = 1  and a.status = '1' ");
        List<Object> params = new ArrayList<Object>();
        String name = request.getParameter("name");
        if (StringUtil.isNotEmpty(name)) {
            hql.append(" and a.goods.name like ? ");
            params.add("%" + name + "%");
        }
        hql.append(" order by a.createDate desc");
        HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
        int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
        hqlQuery.setCurPage(currPage + 1);
        hqlQuery.setPageSize(dataTable.getDisplayLength());
        DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
        List<GiftEntity> pList = (List<GiftEntity>) d.getAaData();
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (GiftEntity p : pList) {
            Map<String, Object> item = new HashMap<String, Object>();
            item.put("id", p.getId());
            item.put("name", p.getName());
            item.put("imgUrl", p.getImgUrl());
            item.put("stockNum", p.getStockNum());
            item.put("price", p.getMoney());
            item.put("groupState", "0");//未参与秒杀
            item.put("time", DateUtil.getTimeByCustomPattern(p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
            mapList.add(item);
        }
        d.setAaData(mapList);
        d.setsEcho(dataTable.getEcho());
        return d;
    }

}
