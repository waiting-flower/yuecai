package modules.shop.ad.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import common.entity.CommonEntity;

@Entity
@Table(name = "shop_ad", schema = "")
public class AdEntity extends CommonEntity{
	private String imgUrl;//展示的图片
	private String title;//标题名称，只是方面后台管理
	private Integer orderNum;//权重，展示排序，越大越靠前
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
	
}
