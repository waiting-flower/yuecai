package modules.shop.ad.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.HqlQuery;
import common.util.StringUtil;
import modules.shop.ad.entity.AdEntity;
import modules.shop.ad.service.AdService;
import modules.shop.goods.entity.BannerEntity;

@Service
public class AdServiceImpl extends CommonServiceImpl implements AdService{

	@Override
	public DataTableReturn loadAdByPage(JQueryDataTables dataTable, HttpServletRequest request) {
		StringBuffer hql = new StringBuffer(
				" from AdEntity as  a where 1 = 1  and a.status = '1' ");
		List<Object> params = new ArrayList<Object>();
		String title = request.getParameter("title");
		if (StringUtil.isNotEmpty(title)) {
			hql.append(" and a.title like ? ");
			params.add("%" + title + "%");
		}
		hql.append(" order by a.orderNum desc, a.createDate desc");
		HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
		int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
		hqlQuery.setCurPage(currPage + 1);
		hqlQuery.setPageSize(dataTable.getDisplayLength());
		DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
		List<AdEntity> pList = (List<AdEntity>) d.getAaData();
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		for (AdEntity p : pList) {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("id", p.getId());
			item.put("title", p.getTitle());
			item.put("imgUrl", p.getImgUrl());
			item.put("orderNum", p.getOrderNum());
			item.put("status", p.getStatus());
			item.put("time",DateUtil.getTimeByCustomPattern( p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
			mapList.add(item);
		}
		d.setAaData(mapList);
		d.setsEcho(dataTable.getEcho());
		return d;
	}

	@Override
	public Map<String, Object> saveAd(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		String title = request.getParameter("title");
		String orderNum = request.getParameter("orderNum");
		String imgUrl = request.getParameter("imgUrl");
		AdEntity a = null;
		if(StringUtil.isNotEmpty(id)) {
			a = get(AdEntity.class, Long.valueOf(id));
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
		}
		else {
			a = new AdEntity();
			a.setCreateDate(DateUtil.getCurrentTime());
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
		}
		a.setTitle(title);
		a.setOrderNum(Integer.parseInt(orderNum));
		a.setImgUrl(imgUrl);
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> deleteAd(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		AdEntity a = get(AdEntity.class, Long.valueOf(id));
		if (a == null) { 
			map.put("code", 1);
			return map;
		}
		a.setStatus("0");
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> getStartAdList(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		String hql = "from AdEntity as a where a.status='1' order by a.orderNum desc";
		List<AdEntity> adList = findByQueryString(hql);
		if(adList == null || adList.isEmpty()) {
			map.put("code", 1);
			return map;
		}
		map.put("code", 0);
		map.put("adList", adList);
		return map;
	}

}
