package modules.shop.ad.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;

public interface AdService extends CommonService{

	DataTableReturn loadAdByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveAd(HttpServletRequest request);

	Map<String, Object> deleteAd(HttpServletRequest request);

	Map<String, Object> getStartAdList(HttpServletRequest request);

}
