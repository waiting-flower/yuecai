package modules.shop.ad.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.util.StringUtil;
import modules.shop.ad.entity.AdEntity;
import modules.shop.ad.service.AdService;
import modules.shop.goods.entity.BannerEntity;

/**
 * APP启动广告控制层
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "admin/shop/ad")
public class AdController extends BaseController{
	@Autowired
	AdService service;
	
	/**
	 * 轮播管理列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list")
	public String cateList( HttpServletRequest request, HttpServletResponse response) {
		return templatePath + "shop/ad/adList";
	}
	
	/**
	 * 轮播详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "info")
	public String info( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			AdEntity menu = service.get(AdEntity.class, Long.valueOf(id));
			request.setAttribute("a", menu);
		} 
		return templatePath + "shop/ad/adInfo";
	}
	
	
	/**
	 * 分页获取商城轮播列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadAdByPage")
	@ResponseBody
	public DataTableReturn loadBannerByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = service.loadAdByPage(dataTable, request);
		return d;
	}
	
	/**
	 * 保存一个商城轮播
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveAd")
	@ResponseBody
	public Map<String, Object> saveAd(HttpServletRequest request) {
		return service.saveAd(request);
	}
	
	/**
	 * 删除一个商城轮播
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteAd")
	@ResponseBody
	public Map<String, Object> deleteAd(HttpServletRequest request) {
		return service.deleteAd(request);
	}
}
