package modules.shop.driver.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import common.entity.CommonEntity;

/**
 * 司机对象
 * @author Administrator
 *
 */
@Entity 
@Table(name = "shop_driver", schema = "")
public class DriverEntity extends CommonEntity{
	private String name;//姓名
	private String mobile;//手机号码
	private String brand;//车辆品牌
	private String licenseNo;//车牌号码
	private Integer size;//容积
	private Integer wet;//载重量
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public Integer getWet() {
		return wet;
	}
	public void setWet(Integer wet) {
		this.wet = wet;
	}
	
}
