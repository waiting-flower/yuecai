package modules.shop.driver.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;

public interface DriverService extends CommonService{

	DataTableReturn loadDriverByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveDriver(HttpServletRequest request);

	Map<String, Object> deleteDriver(HttpServletRequest request);

}
