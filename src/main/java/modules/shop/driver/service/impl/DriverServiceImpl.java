package modules.shop.driver.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.HqlQuery;
import common.util.StringUtil;
import modules.shop.driver.entity.DriverEntity;
import modules.shop.driver.service.DriverService;

@Service
public class DriverServiceImpl extends CommonServiceImpl implements DriverService{

	@Override
	public DataTableReturn loadDriverByPage(JQueryDataTables dataTable, HttpServletRequest request) {
		StringBuffer hql = new StringBuffer(
				" from DriverEntity as  a where 1 = 1  and a.status = '1' ");
		List<Object> params = new ArrayList<Object>();
		String name = request.getParameter("name");
		if (StringUtil.isNotEmpty(name)) {
			hql.append(" and a.name like ? ");
			params.add("%" + name + "%");
		}
		hql.append(" order by a.createDate desc");
		HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
		int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
		hqlQuery.setCurPage(currPage + 1);
		hqlQuery.setPageSize(dataTable.getDisplayLength());
		DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
		List<DriverEntity> pList = (List<DriverEntity>) d.getAaData();
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		for (DriverEntity p : pList) {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("id", p.getId());
			/*
			 ** private String name;
	private String mobile;
	private String brand;
	private String licenseNo;
	private Integer size;
	private Integer wet;
			 */
			item.put("name", p.getName());
			item.put("mobile", p.getMobile());
			item.put("brand", p.getBrand());
			item.put("licenseNo", p.getLicenseNo());
			item.put("size", p.getSize());
			item.put("wet", p.getWet());
			item.put("status", p.getStatus());
			item.put("time",DateUtil.getTimeByCustomPattern( p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
			mapList.add(item);
		}
		d.setAaData(mapList);
		d.setsEcho(dataTable.getEcho());
		return d;
	}

	@Override
	public Map<String, Object> saveDriver(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String mobile = request.getParameter("mobile");
		String brand = request.getParameter("brand");
		String licenseNo = request.getParameter("licenseNo");
		String size = request.getParameter("size");
		String wet = request.getParameter("wet");
		/*
		 * private String name;
	private String mobile;
	private String brand;
	private String licenseNo;
	private Integer size;
	private Integer wet;
		 */
		DriverEntity a = null;
		if(StringUtil.isNotEmpty(id)) {
			a = get(DriverEntity.class, Long.valueOf(id));
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
		}
		else {
			a = new DriverEntity();
			a.setCreateDate(DateUtil.getCurrentTime());
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
		}
		a.setName(name);
		a.setMobile(mobile);
		a.setBrand(brand);
		a.setLicenseNo(licenseNo);
		a.setSize(Integer.parseInt(size));
		a.setWet(Integer.parseInt(wet));
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> deleteDriver(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		DriverEntity a = get(DriverEntity.class, Long.valueOf(id));
		if (a == null) { 
			map.put("code", 1);
			return map;
		}
		a.setStatus("0");
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

}
