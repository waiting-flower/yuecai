package modules.shop.driver.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.util.StringUtil;
import modules.shop.coupon.entity.CouponEntity;
import modules.shop.driver.entity.DriverEntity;
import modules.shop.driver.service.DriverService;

/**
 * 司机管理web控制层
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "admin/shop/driver")
public class DriverController extends BaseController{

	@Autowired
	DriverService service; 
	
	/**
	 * 司机对象列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list")
	public String cateList( HttpServletRequest request, HttpServletResponse response) {
		return templatePath + "shop/driver/driverList";
	}
	
	/**
	 * 司机对象详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "info")
	public String info( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			DriverEntity menu = service.get(DriverEntity.class, Long.valueOf(id));
			request.setAttribute("a", menu);
		} 
		return templatePath + "shop/driver/driverInfo";
	}
	
	
	/**
	 * 分页获取司机对象列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadDriverByPage")
	@ResponseBody
	public DataTableReturn loadDriverByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = service.loadDriverByPage(dataTable, request);
		return d;
	}
	
	/**
	 * 保存一个司机对象
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveDriver")
	@ResponseBody
	public Map<String, Object> saveDriver(HttpServletRequest request) {
		return service.saveDriver(request);
	}
	
	/**
	 * 删除一个司机对象
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteDriver")
	@ResponseBody
	public Map<String, Object> deleteDriver(HttpServletRequest request) {
		return service.deleteDriver(request);
	}
	
}
