package modules.shop.api.controller;

import com.google.common.collect.Maps;
import common.controller.BaseController;
import modules.shop.ad.service.AdService;
import modules.shop.api.service.APIService;
import modules.shop.goods.entity.GoodsEntity;
import modules.shop.goods.service.BannerService;
import modules.shop.goods.service.BrandService;
import modules.shop.indexad.entity.IndexAdEntity;
import modules.shop.indexad.service.IndexAdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * API接口列表
 *
 * @author Administrator
 */
@Controller
@RequestMapping(value = "API")
public class APIController extends BaseController {

    /**
     * 评论订单数据
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "commentOrder")
    public ModelAndView commentOrder(HttpServletRequest request) {
        initShopWapController(request);
        apiService.initCommentOrder(request);
        ModelAndView mv = new ModelAndView("wap/html/commentOrder");
        return mv;
    }

    @Autowired
    AdService adService;

    @Autowired
    IndexAdService indexAdService;

    @Autowired
    BannerService bannerService;

    @Autowired
    BrandService brandService;


    @Autowired
    APIService apiService;

    /**
     * 获取秒杀时间段列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getSeckillTimeList")
    @ResponseBody
    public Map<String, Object> getSeckillTimeList(HttpServletRequest request) {
        return apiService.getSeckillTimeList(request);
    }

    /**
     * 获取秒杀活动的商品
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getSecGoodsList")
    @ResponseBody
    public Map<String, Object> getSecGoodsList(HttpServletRequest request) {
        return apiService.getSecGoodsList(request);
    }

    /**
     * 获取启动时候的广告
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getStartAdList")
    @ResponseBody
    public Map<String, Object> getStartAdList(HttpServletRequest request) {
        return adService.getStartAdList(request);
    }

    /**
     * 获取首页弹窗候的广告
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getIndexad")
    @ResponseBody
    public IndexAdEntity getIndexad(HttpServletRequest request) {
        return indexAdService.getCurrent();
    }


    /**
     * 获取轮播图片列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getBannerList")
    @ResponseBody
    public Map<String, Object> getBannerList(HttpServletRequest request) {
        return bannerService.getBannerList(request);
    }


    /**
     * 获取品牌列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getBrandList")
    @ResponseBody
    public Map<String, Object> getBrandList(HttpServletRequest request) {
        return brandService.getBrandList(request);
    }

    /**
     * 获取分类列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getCateList")
    @ResponseBody
    public Map<String, Object> getCateList(HttpServletRequest request) {
        return apiService.getCateList(request);
    }


    /**
     * 获取首页秒杀商品列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getHomeSeckillList")
    @ResponseBody
    public Map<String, Object> getHomeSeckillList(HttpServletRequest request) {
        return apiService.getHomeSeckillList(request);
    }

    /**
     * 获取首页特价折扣商品列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getHomeDiscountGoodsList")
    @ResponseBody
    public Map<String, Object> getHomeDiscountGoodsList(HttpServletRequest request) {
        return apiService.getHomeDiscountGoodsList(request);
    }

    /**
     * 获取首页品牌列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getHomeBrandList")
    @ResponseBody
    public Map<String, Object> getHomeBrandList(HttpServletRequest request) {
        return apiService.getHomeBrandList(request);
    }


    /**
     * 获取商品列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getGoodsList")
    @ResponseBody
    public Map<String, Object> getGoodsList(HttpServletRequest request) {
        return apiService.getGoodsList(request);
    }

    /**
     * 用户注册保存
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveReg")
    @ResponseBody
    public Map<String, Object> saveReg(HttpServletRequest request) {
        return apiService.saveReg(request);
    }

    /**
     * 用户登录
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveLogin")
    @ResponseBody
    public Map<String, Object> saveLogin(HttpServletRequest request) {
        return apiService.saveLogin(request);
    }

    /**
     * 获取当前登录用户的信息
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getUserData")
    @ResponseBody
    public Map<String, Object> getUserData(HttpServletRequest request) {
        return apiService.saveWorkUserData(request);
    }


    /**
     * 获取商品详情
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getGoodsDetail")
    @ResponseBody
    public Map<String, Object> getGoodsDetail(HttpServletRequest request) {
        String id = request.getParameter("id");
        GoodsEntity goods = apiService.get(GoodsEntity.class, Long.valueOf(id));
        goods.setLookNum(goods.getLookNum() + 1);
        apiService.saveOrUpdate(goods);
        apiService.saveGoodsView(request);
        return apiService.getGoodsDetail(request);
    }

    /**
     * 获取我的收货地址列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getAddressList")
    @ResponseBody
    public Map<String, Object> getAddressList(HttpServletRequest request) {
        return apiService.getAddressList(request);
    }

    /**
     * 保存一个地址对象
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveAddress")
    @ResponseBody
    public Map<String, Object> saveAddress(HttpServletRequest request) {
        return apiService.saveAddress(request);
    }


    /**
     * 删除一个地址对象
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "deleteAddress")
    @ResponseBody
    public Map<String, Object> deleteAddress(HttpServletRequest request) {
        return apiService.deleteAddress(request);
    }

    /**
     * 設置默認地址对象
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "editDefault")
    @ResponseBody
    public Map<String, Object> editDefault(HttpServletRequest request) {
        return apiService.editDefault(request);
    }


    /**
     * 将一个地址设置为默认地址
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "setDefaultAddress")
    @ResponseBody
    public Map<String, Object> setDefaultAddress(HttpServletRequest request) {
        return apiService.setDefaultAddress(request);
    }

    /**
     * 获取商品详情
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getAddressDetail")
    @ResponseBody
    public Map<String, Object> getAddressDetail(HttpServletRequest request) {
        return apiService.getAddressDetail(request);
    }


    /**
     * 保存昵称
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveUserNick")
    @ResponseBody
    public Map<String, Object> saveUserNick(HttpServletRequest request) {
        return apiService.saveUserNick(request);
    }

    /**
     * 保存业务员ID
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveUserPcode")
    @ResponseBody
    public Map<String, Object> saveUserPcode(HttpServletRequest request) {
        return apiService.saveUserPcode(request);
    }

    /**
     * 保存用户头像
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveUserPhoto")
    @ResponseBody
    public Map<String, Object> saveUserPhoto(HttpServletRequest request) {
        return apiService.saveUserPhoto(request);
    }

    /**
     * 获取上传token
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getUpToken")
    @ResponseBody
    public Map<String, Object> getUpToken(HttpServletRequest request) {
        return apiService.getUpToken(request);
    }

    /**
     * 获取优惠券列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getCouponList")
    @ResponseBody
    public Map<String, Object> getCouponList(HttpServletRequest request) {
        return apiService.getCouponList(request);
    }


    /**
     * 获取我的优惠券列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getUserCouponList")
    @ResponseBody
    public Map<String, Object> getUserCouponList(HttpServletRequest request) {
        return apiService.getUserCouponList(request);
    }


    /**
     * 用户获取一个优惠券
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveUserCoupon")
    @ResponseBody
    public Map<String, Object> saveUserCoupon(HttpServletRequest request) {
        return apiService.saveUserCoupon(request);
    }


    /**
     * 保存商品到购物车
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveGoodsToCart")
    @ResponseBody
    public Map<String, Object> saveGoodsToCart(HttpServletRequest request) {
        return apiService.saveGoodsToCart(request);
    }


    /**
     * 获取商品的购买数据详情
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getGoodsBuyDetail")
    @ResponseBody
    public Map<String, Object> getGoodsBuyDetail(HttpServletRequest request) {
        return apiService.getGoodsBuyDetail(request);
    }

    /**
     * 获取购物车数据
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getCartData")
    @ResponseBody
    public Map<String, Object> getCartData(HttpServletRequest request) {
        return apiService.getCartData(request);
    }


    /**
     * 删除一个购物车的商品
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "deleteCart")
    @ResponseBody
    public Map<String, Object> deleteCart(HttpServletRequest request) {
        return apiService.deleteCart(request);
    }

    /**
     * 更新购物车的商品数量
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "updateCart")
    @ResponseBody
    public Map<String, Object> updateCart(HttpServletRequest request) {
        return apiService.updateCart(request);
    }


    /**
     * 获取购物车的商品数量
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getCartNum")
    @ResponseBody
    public Map<String, Object> getCartNum(HttpServletRequest request) {
        return apiService.getCartNum(request);
    }


    /**
     * 添加一个商品到收藏中
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveGoodsCollect")
    @ResponseBody
    public Map<String, Object> saveGoodsCollect(HttpServletRequest request) {
        return apiService.saveGoodsCollect(request);
    }


    /**
     * 获取购物车支付结算商品数据
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getCartBuyData")
    @ResponseBody
    public Map<String, Object> getCartBuyData(HttpServletRequest request) {
        return apiService.getCartBuyData(request);
    }


    /**
     * 生成商品支付订单
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "createGoodsBuyOrder")
    @ResponseBody
    public Map<String, Object> createGoodsBuyOrder(HttpServletRequest request) {
        return apiService.createGoodsBuyOrder(request);
    }


    /**
     * 生成购物车支付订单
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "createCartBuyOrder")
    @ResponseBody
    public Map<String, Object> createCartBuyOrder(HttpServletRequest request) {
        return apiService.createCartBuyOrder(request);
    }


    /**
     * 获取用户的足迹列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getUserViewGoodsList")
    @ResponseBody
    public Map<String, Object> getUserViewGoodsList(HttpServletRequest request) {
        return apiService.getUserViewGoodsList(request);
    }

    /**
     * 获取用户的商品收藏列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getUserCollectGoodsList")
    @ResponseBody
    public Map<String, Object> getUserCollectGoodsList(HttpServletRequest request) {
        return apiService.getUserCollectGoodsList(request);
    }


    /**
     * 获取用户的订单列表数据
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getOrderList")
    @ResponseBody
    public Map<String, Object> getOrderList(HttpServletRequest request) {
        return apiService.getOrderList(request);
    }

    /**
     * 获取用户业绩的订单列表数据
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getOrderListyeji")
    @ResponseBody
    public Map<String, Object> getOrderListyeji(HttpServletRequest request) {
        return apiService.getOrderListyeji(request);
    }


    /**
     * 获取用户的订单详情数据
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getOrderDetail")
    @ResponseBody
    public Map<String, Object> getOrderDetail(HttpServletRequest request) {
        return apiService.getOrderDetail(request);
    }

    /**
     * 取消订单
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveCancleOrder")
    @ResponseBody
    public Map<String, Object> saveCancleOrder(HttpServletRequest request) {
        return apiService.saveCancleOrder(request);
    }

    /**
     * 确认收货订单
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveReceiveOrder")
    @ResponseBody
    public Map<String, Object> saveReceiveOrder(HttpServletRequest request) {
        return apiService.saveReceiveOrder(request);
    }


    /**
     * 用户订单退款
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveRefoundOrder")
    @ResponseBody
    public Map<String, Object> saveRefoundOrder(HttpServletRequest request) throws Exception {
        return apiService.saveRefoundOrder(request);
    }

    /**
     * 用户的订单晒单
     *
     * @param request
     * @return
     */
    @RequestMapping(params = "saveOrderComment")
    @ResponseBody
    public Map<String, Object> saveOrderComment(HttpServletRequest request) {
        Map<String, Object> map = apiService.saveOrderComment(request);
        return map;
    }


    /**
     * 根据commentid 获取用户的晒单
     *
     * @param request
     * @return
     */
    @RequestMapping(params = "loadCommentById")
    @ResponseBody
    public Map<String, Object> loadCommentById(HttpServletRequest request) {
        Map<String, Object> map = apiService.loadCommentById(request);
        return map;
    }

    /**
     * 晒单添加图片
     *
     * @param request
     * @return
     */
    @RequestMapping(params = "addCommetPic")
    @ResponseBody
    public Map<String, Object> addCommetPic(HttpServletRequest request) {
        Map<String, Object> map = apiService.addCommetPic(request);
        return map;
    }


    /**
     * 用户取消收藏一个商品
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveCancleGoodsCollect")
    @ResponseBody
    public Map<String, Object> saveCancleGoodsCollect(HttpServletRequest request) {
        Map<String, Object> map = apiService.saveCancleGoodsCollect(request);
        return map;
    }

    /**
     * 取消收藏全部的商品
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveCancleAllGoods")
    @ResponseBody
    public Map<String, Object> saveCancleAllGoods(HttpServletRequest request) {
        Map<String, Object> map = apiService.saveCancleAllGoods(request);
        return map;
    }


    /**
     * 获取我推荐的用户
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getUserTuijianList")
    @ResponseBody
    public Map<String, Object> getUserTuijianList(HttpServletRequest request) {
        Map<String, Object> map = apiService.getUserTuijianList(request);
        return map;
    }

    /**
     * 获取品牌满赠商品列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getNowBrandGiftActionGoodsList")
    @ResponseBody
    public Map<String, Object> getNowBrandGiftActionGoodsList(HttpServletRequest request) {
        Map<String, Object> map = apiService.getNowBrandGiftActionGoodsList(request);
        return map;
    }

    /**
     * 获取商品满赠商品列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getNowGoodsGiftActionGoodsList")
    @ResponseBody
    public Map<String, Object> getNowGoodsGiftActionGoodsList(HttpServletRequest request) {
        Map<String, Object> map = apiService.getNowGoodsGiftActionGoodsList(request);
        return map;
    }


    /**
     * 获取系统配置数据
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getConfigData")
    @ResponseBody
    public Map<String, Object> getConfigData(HttpServletRequest request) {
        Map<String, Object> map = apiService.getConfigData(request);
        return map;
    }

    @RequestMapping("version")
    @ResponseBody
    public Map<String, String> version(String v,HttpServletRequest request) {
        String userAgent = request.getHeader("user-agent");
        boolean mAndroid= userAgent.contains("Android");  //安卓
        boolean miPhone= userAgent.contains("iPhone");  //苹果IOS
        Map map = Maps.newHashMap();
        String mequipment="";  //定义设备类型变量
        if(mAndroid==true && !"1.2.4".equalsIgnoreCase(v)){
            map.put("msg", "检测到新版本更新，请更新！");
            map.put("url", "https://fir.im/jpld");
        }else if(miPhone== true && !"1.2.4".equalsIgnoreCase(v)){
            map.put("msg", "检测到新版本更新，请更新！");
            map.put("url", "https://copy.im/s/pCF0");
        }
        return map;
    }

}
