package modules.shop.api.service.impl;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.druid.support.json.JSONUtils;
import com.qiniu.util.Auth;
import com.qiniu.util.StringMap;
import common.service.impl.CommonServiceImpl;
import common.util.*;
import modules.shop.action.entity.ActionEntity;
import modules.shop.action.entity.ActionGiftEntity;
import modules.shop.api.service.APIService;
import modules.shop.cart.entity.CartEntity;
import modules.shop.coupon.entity.CouponEntity;
import modules.shop.coupon.entity.WUserCouponEntity;
import modules.shop.gift.entity.GiftEntity;
import modules.shop.goods.entity.*;
import modules.shop.order.entity.*;
import modules.shop.seckill.entity.SeckillActionEntity;
import modules.shop.seckill.entity.SeckillConfigEntity;
import modules.shop.seckill.entity.SeckillGoodsMapEntity;
import modules.shop.seckill.entity.SeckillGoodsPriceSetEntity;
import modules.shop.user.entity.AddressEntity;
import modules.shop.user.entity.UserLoginLogEntity;
import modules.shop.user.entity.WUserEntity;
import net.sf.json.groovy.GJson;
import org.apache.commons.lang.ObjectUtils;
import org.jsoup.select.Evaluator;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.persistence.Id;
import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class APIServiceImpl extends CommonServiceImpl implements APIService {

    @Override
    public Map<String, Object> getSeckillTimeList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Date nowTime = DateUtil.getCurrentTime();
        //当前的时间
        SeckillConfigEntity sConfig = get(SeckillConfigEntity.class, Long.valueOf(1));
        String openHour = sConfig.getOpenHour();//开放的时段
        String openHoursArr[] = openHour.split(",");

        String nowDay = DateUtil.getCurrentTimeByCustomPattern(DateUtil.yyyyMMdd);
        String nowHours = null;
        List<Map<String, Object>> list = new ArrayList<>();
        for (String hoursFlag : openHoursArr) {
            String hoursArr[] = hoursFlag.split("~");
            String begin = hoursArr[0];
            String end = hoursArr[1];

            Map<String, Object> item = new HashMap<>();
            item.put("begin", begin);
            item.put("end", end);
            item.put("tips", "已结束");
            item.put("flag", "0");
            String beginTimeStr = nowDay + " " + begin + ":00";
            String endTimeStr = nowDay + " " + end + ":00";
            Date beginTime = DateUtil.parse(beginTimeStr, DateUtil.yyyyMMddHHmmss);
            Date endTime = DateUtil.parse(endTimeStr, DateUtil.yyyyMMddHHmmss);
            if (beginTime.getTime() <= nowTime.getTime() && endTime.getTime() >= nowTime.getTime()) {
                nowHours = hoursFlag;
                item.put("tips", "抢购中");
                item.put("flag", "1");
            }
            if (beginTime.getTime() > nowTime.getTime()) {
                item.put("tips", "即将开始");
                item.put("flag", "2");
            }

            list.add(item);
        }
        if (StringUtil.isEmpty(nowHours)) {
            nowHours = openHoursArr[0];
        }
        map.put("list", list);
        map.put("nowHours", nowHours);
        return map;
    }

    @Override
    public Map<String, Object> getSecGoodsList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        List<Map<String, Object>> mapList = new ArrayList<>();
        String nowHours = request.getParameter("nowHours");
        String userId = request.getParameter("userId");
        WUserEntity user = null;
        boolean isAuth = false;
        if (StringUtil.isNotEmpty(userId)) {
            user = get(WUserEntity.class, Long.valueOf(userId));
            if (user.getIsAuth().equals("1"))
                isAuth = true;
        }
        Date nowTime = DateUtil.getCurrentTime();
        if (StringUtil.isNotEmpty(nowHours)) {
            String[] nowHoursArr = nowHours.split("~");
            Date beginTime = DateUtil.getDateFromTimeStr(nowHoursArr[0]);//促销时段开始时间
            Date endTime = DateUtil.getDateFromTimeStr(nowHoursArr[1]);//促销时段结束时间
            String hql = "from SeckillGoodsPriceSetEntity as a "
                    + " where a.status = '1'"
                    + " and exists "
                    + " (select 1 from SeckillActionEntity as b where b.id = a.seckillAction.id and b.status ='1' and b.beginTime <= ? and b.endTime >= ? and b.openHours like ? )";
            List<SeckillGoodsPriceSetEntity> sgList = commonDao.getListByPage(hql, 1, 10, new Object[]{beginTime, endTime, "%" + nowHours + "%"});

            for (SeckillGoodsPriceSetEntity sgm : sgList) {
                Map<String, Object> item = new HashMap<>();
                item.put("id", sgm.getId());
                item.put("goodsId", sgm.getGoods().getId());
                item.put("goodsNum", sgm.getSeckillGoodsMap().getGoodsNum());
                item.put("goodsName", sgm.getGoods().getName());
                item.put("imgUrl", sgm.getGoods().getImgUrl());
                item.put("goodsPrice", sgm.getGoods().getPrice() * sgm.getDiscount() / 10);
                if (!isAuth) {
                    item.put("goodsPrice", 0.0);
                }
                item.put("stockNum", sgm.getGoods().getStockNum());
                item.put("oldPrice", sgm.getGoods().getOldPrice());
                mapList.add(item);
            }
        } else {
            String hql = "from SeckillGoodsPriceSetEntity as a"
                    + " where a.status = '1'"
                    + " and exists "
                    + " (select 1 from SeckillActionEntity as b where b.id ="
                    + " a.seckillAction.id and b.status ='1' and b.beginTime <= ? and b.endTime >= ? )";
            List<SeckillGoodsPriceSetEntity> sgList = commonDao.getListByPage(hql, 1, 10, new Object[]{nowTime, nowTime});
            for (SeckillGoodsPriceSetEntity sgm : sgList) {
                Map<String, Object> item = new HashMap<>();
                item.put("id", sgm.getId());
                item.put("goodsId", sgm.getGoods().getId());
                item.put("goodsNum", sgm.getSeckillGoodsMap().getGoodsNum());
                item.put("goodsName", sgm.getGoods().getName());
                item.put("imgUrl", sgm.getGoods().getImgUrl());
                item.put("goodsPrice", sgm.getGoods().getPrice() * sgm.getDiscount() / 10);
                if (!isAuth) {
                    item.put("goodsPrice", 0.0);
                }
                item.put("stockNum", sgm.getGoods().getStockNum());
                item.put("oldPrice", sgm.getGoods().getOldPrice());
                mapList.add(item);
            }
        }
        if (mapList.isEmpty()) {
            map.put("msg", "不存在");
            map.put("code", 1);
            return map;
        }
        map.put("code", 0);
        map.put("list", mapList);
        return map;
    }


    @Override
    public Map<String, Object> getHomeSeckillList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Date nowTime = DateUtil.getCurrentTime();
        //当前的时间
        SeckillConfigEntity sConfig = get(SeckillConfigEntity.class, Long.valueOf(1));
        String openHour = sConfig.getOpenHour();//开放的时段
        String openHoursArr[] = openHour.split(",");
        String userId = request.getParameter("userId");
        WUserEntity user = null;
        boolean isAuth = false;
        if (StringUtil.isNotEmpty(userId)) {
            user = get(WUserEntity.class, Long.valueOf(userId));
            if (user.getIsAuth().equals("1"))
                isAuth = true;
        }
        String nowDay = DateUtil.getCurrentTimeByCustomPattern(DateUtil.yyyyMMdd);
        String nowHours = null;
        for (String hoursFlag : openHoursArr) {
            String hoursArr[] = hoursFlag.split("~");
            String begin = hoursArr[0];
            String end = hoursArr[1];
            String beginTimeStr = nowDay + " " + begin + ":00";
            String endTimeStr = nowDay + " " + end + ":00";
            Date beginTime = DateUtil.parse(beginTimeStr, DateUtil.yyyyMMddHHmmss);
            Date endTime = DateUtil.parse(endTimeStr, DateUtil.yyyyMMddHHmmss);
            if (beginTime.getTime() <= nowTime.getTime() && endTime.getTime() >= nowTime.getTime()) {
                nowHours = hoursFlag;
                break;
            }
        }
        List<Map<String, Object>> mapList = new ArrayList<>();
        if (StringUtil.isNotEmpty(nowHours)) {
            String hql = "from SeckillGoodsMapEntity as a"
                    + " where a.status = '1'"
                    + " and exists "
                    + " (select 1 from SeckillActionEntity as b where b.id ="
                    + " a.seckillAction.id and b.status='1' and b.beginTime <= ? and b.endTime >= ? and b.openHours like ? )";
            List<SeckillGoodsMapEntity> sgList = commonDao.getListByPage(hql, 1, 10, new Object[]{nowTime, nowTime, "%" + nowHours + "%"});
            for (SeckillGoodsMapEntity sgm : sgList) {
                Map<String, Object> item = new HashMap<>();
                item.put("id", sgm.getId());
                item.put("goodsId", sgm.getGoods().getId());
                item.put("goodsNum", sgm.getGoodsNum());
                item.put("goodsName", sgm.getGoods().getName());
                item.put("imgUrl", sgm.getGoods().getImgUrl());
                item.put("goodsPrice", sgm.getGoods().getPrice());
                if (!isAuth) {
                    item.put("goodsPrice", 0.0);
                }
                item.put("stockNum", sgm.getGoods().getStockNum());
                item.put("oldPrice", sgm.getGoods().getOldPrice());
                item.put("openHours", StringUtil.splitString(nowHours, ","));
                mapList.add(item);
            }
            if (mapList.isEmpty()) {
                hql = "from SeckillGoodsMapEntity as a"
                        + " where a.status = '1'"
                        + " and exists "
                        + " (select 1 from SeckillActionEntity as b where b.id ="
                        + " a.seckillAction.id and b.status='1' and b.beginTime <= ? and b.endTime >= ? )";
                sgList = commonDao.getListByPage(hql, 1, 10, new Object[]{nowTime, nowTime});
                for (SeckillGoodsMapEntity sgm : sgList) {
                    Map<String, Object> item = new HashMap<>();
                    item.put("id", sgm.getId());
                    item.put("goodsId", sgm.getGoods().getId());
                    item.put("goodsNum", sgm.getGoodsNum());
                    item.put("goodsName", sgm.getGoods().getName());
                    item.put("imgUrl", sgm.getGoods().getImgUrl());
                    item.put("goodsPrice", sgm.getGoods().getPrice());
                    if (!isAuth) {
                        item.put("goodsPrice", 0.0);
                    }
                    item.put("stockNum", sgm.getGoods().getStockNum());
                    item.put("oldPrice", sgm.getGoods().getOldPrice());
                    item.put("openHours", StringUtil.splitString(sgm.getSeckillAction().getOpenHours(), ","));
                    mapList.add(item);
                }
            }
        } else {
            String hql = "from SeckillGoodsMapEntity as a"
                    + " where a.status = '1'"
                    + " and exists "
                    + " (select 1 from SeckillActionEntity as b where b.id ="
                    + " a.seckillAction.id and b.status='1' and b.beginTime <= ? and b.endTime >= ? )";
            List<SeckillGoodsMapEntity> sgList = commonDao.getListByPage(hql, 1, 10, new Object[]{nowTime, nowTime});
            for (SeckillGoodsMapEntity sgm : sgList) {
                Map<String, Object> item = new HashMap<>();
                item.put("id", sgm.getId());
                item.put("goodsId", sgm.getGoods().getId());
                item.put("goodsNum", sgm.getGoodsNum());
                item.put("goodsName", sgm.getGoods().getName());
                item.put("imgUrl", sgm.getGoods().getImgUrl());
                item.put("goodsPrice", sgm.getGoods().getPrice());
                if (!isAuth) {
                    item.put("goodsPrice", 0.0);
                }
                item.put("stockNum", sgm.getGoods().getStockNum());
                item.put("oldPrice", sgm.getGoods().getOldPrice());
                item.put("openHours", StringUtil.splitString(sgm.getSeckillAction().getOpenHours(), ","));
                mapList.add(item);
            }
        }
        if (mapList.isEmpty()) {
            map.put("list", mapList);
            map.put("code", 1);
            return map;
        }
        map.put("code", 0);
        map.put("list", mapList);
        return map;
    }

    @Override
    public Map<String, Object> getHomeBrandList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();

        String hql = "from BrandEntity as a where a.status = '1' order by a.orderNum desc,a.createDate desc";
        List<BrandEntity> brandList = commonDao.getListByPage(hql, 1, 6, new Object[]{});
        map.put("code", 0);
        map.put("brandList", brandList);
        return map;
    }

    @Override
    public Map<String, Object> getHomeDiscountGoodsList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String userId = request.getParameter("userId");
        WUserEntity user = null;
        boolean isAuth = false;
        if (StringUtil.isNotEmpty(userId)) {
            user = get(WUserEntity.class, Long.valueOf(userId));
            if (user.getIsAuth().equals("1"))
                isAuth = true;
        }
        String hql = "from GoodsEntity as a where a.status = '1' and a.isOnSale = '1' and a.isDiscount = '1' order by a.saleNum desc,a.createDate desc";
        List<GoodsEntity> goodsList = commonDao.getListByPage(hql, 1, 10, new Object[]{});
        List<Map<String, Object>> list = new ArrayList<>();
        for (GoodsEntity goods : goodsList) {
            Map<String, Object> item = new HashMap<>();
            item.put("id", goods.getId());
            item.put("imgUrl", goods.getImgUrl());
            item.put("cornerImg", goods.getCornerImg());
            item.put("name", goods.getName());
            item.put("name2", goods.getName2());
            item.put("price", goods.getPrice() * goods.getDiscount() / 10);
            if (!isAuth) {
                item.put("price", "登陆后查看");
            }
            item.put("oldPrice", goods.getOldPrice());
            item.put("saleNum", goods.getSaleNum());
            item.put("stockNum", goods.getStockNum());
            item.put("cornerImg", goods.getCornerImg());
            item.put("isHasSku", "0");
            String tempHql = "from GoodsSkuEntity as a where a.goods.id=" + goods.getId() + " and a.status = '1'";
            List<GoodsSkuEntity> skuList = findByQueryString(tempHql);
            if (skuList != null && !skuList.isEmpty()) {
                item.put("isHasSku", "1");
                GoodsSkuEntity sku = skuList.get(0);
                item.put("price", sku.getPrice() * goods.getDiscount() / 10);
                item.put("oldPrice", sku.getOldPrice());
                item.put("stockNum", sku.getStockNum());
                if (StringUtil.isNotEmpty(sku.getImgUrl())) {
                    item.put("imgUrl", sku.getImgUrl());
                }
            }
            list.add(item);
        }
        map.put("code", 0);
        map.put("goodsList", list);
        return map;
    }

    @Override
    public Map<String, Object> getGoodsList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        int pageNo = 1;
        String pageNoStr = request.getParameter("pageNo");
        if (StringUtil.isNotEmpty(pageNoStr)) {
            pageNo = Integer.parseInt(pageNoStr);
        }

        int pageSize = 10;
        String pageSizeStr = request.getParameter("pageSize");
        if (StringUtil.isNotEmpty(pageSizeStr)) {
            pageSize = Integer.parseInt(pageSizeStr);
        }
        List<Object> params = new ArrayList<>();
        String hql = "from GoodsEntity as a where a.status = '1' and a.isOnSale='1' ";
        String cateId = request.getParameter("cateId");
        if (StringUtil.isNotEmpty(cateId)) {
            hql = hql + " and a.cate.id= ? ";
            params.add(Long.valueOf(cateId));
        }

        String parentCateId = request.getParameter("parentCateId");
        if (StringUtil.isNotEmpty(parentCateId)) {
            hql = hql + " and a.cate.parentCate.id= ? ";
            params.add(Long.valueOf(parentCateId));
        }
        String brandId = request.getParameter("brandId");
        if (StringUtil.isNotEmpty(brandId)) {
            hql = hql + " and a.brand.id= ? ";
            params.add(Long.valueOf(brandId));
        }
        String isDiscount = request.getParameter("isDiscount");
        if (StringUtil.isNotEmpty(isDiscount)) {
            hql = hql + " and a.isDiscount = ? ";
            params.add(isDiscount);
        }
        String keyword = request.getParameter("keyword");
        if (StringUtil.isNotEmpty(keyword)) {
            hql = hql + " and a.name like ? ";
            params.add("%" + keyword + "%");
        }
        String getType = request.getParameter("getType");
        String homeFlag = request.getParameter("homeFlag");
        boolean sort = false;
        if (StringUtil.isNotEmpty(homeFlag)) {
            if ("hot".equals(homeFlag)) {
                hql = hql + "  order by a.isRec desc,a.sort desc";
            } else if ("news".equals(homeFlag)) {
                hql = hql + "  order by a.createDate desc,a.sort desc";
            }
            sort = true;
        }
        if (StringUtil.isNotEmpty(getType)) {
            if ("10".equals(getType)) {
                //热销
                hql = hql + "  order by a.isRec desc,a.sort desc";
            } else if ("5".equals(getType)) {
                //综合
                hql = hql + "  order by a.commentNum desc,a.saleNum desc ,a.score desc,a.sort desc";
            } else if ("20".equals(getType)) {
                //销量
                hql = hql + "  order by a.saleNum desc,a.sort desc";
            } else if ("50".equals(getType)) {
                //新品
                hql = hql + "  order by a.createDate desc,a.sort desc";
            } else if ("30".equals(getType)) {
                //价格由高到低30,由低到高31
                hql = hql + "  order by a.price desc";
            } else if ("31".equals(getType)) {
                hql = hql + "  order by a.price asc";
            }
            sort = true;
        }
        if (!sort) {
            hql = hql + " order by a.saleNum desc,a.createDate desc,a.sort desc";
        }
        String userId = request.getParameter("userId");
        WUserEntity user = null;
        boolean isAuth = false;
        if (StringUtil.isNotEmpty(userId)) {
            user = get(WUserEntity.class, Long.valueOf(userId));
            if (user.getIsAuth().equals("1"))
                isAuth = true;
        }
        HqlQuery hqlQuery = new HqlQuery();
        hqlQuery.setCurPage(pageNo);
        hqlQuery.setQueryString(hql);
        hqlQuery.setPageSize(pageSize);
        hqlQuery.setParam(params.toArray());
        PageModel pm = commonDao.getPageList(hqlQuery, true);
        if (pm.getTotalRecords() < 1 && StringUtil.isNotEmpty(pm.getTotalRecords())) {
            map.put("code", 1);//表示没有
            map.put("msg", "暂无数据");
            return map;
        }
        List<GoodsEntity> goodsList = (List<GoodsEntity>) pm.getList();
        List<Map<String, Object>> list = new ArrayList<>();
        for (GoodsEntity goods : goodsList) {
            Map<String, Object> item = new HashMap<>();
            item.put("id", goods.getId());
            item.put("imgUrl", goods.getImgUrl());
            item.put("cornerImg", goods.getCornerImg());
            item.put("name", goods.getName());
            item.put("name2", goods.getName2());
            item.put("price", goods.getPrice());
            item.put("oldPrice", goods.getOldPrice());
            item.put("saleNum", goods.getSaleNum());
            item.put("stockNum", goods.getStockNum());
            // item.put("cornerImg", goods.getCornerImg());
            item.put("commentNum", goods.getCommentNum());
            item.put("isTejia", "0");
            item.put("isManjian", "0");
            item.put("isMiaosha", "0");
            item.put("parmasValuesJson", "");
            item.put("isDiscount", goods.getIsDiscount());//是否特价折扣 1-是 0-否
            item.put("discount", goods.getDiscount());//折扣的值

            if (StringUtil.isNotEmpty(goods.getIsDiscount()) && "1".equals(goods.getIsDiscount())) {
                item.put("price", goods.getPrice() * goods.getDiscount() / 10);
                item.put("isTejia", "1");
            }
            item.put("isHasSku", "0");
            String tempHql = "from GoodsSkuEntity as a where a.goods.id=" + goods.getId() + " and a.status = '1'";
            List<GoodsSkuEntity> skuList = findByQueryString(tempHql);
            if (skuList != null && !skuList.isEmpty()) {
                item.put("isHasSku", "1");
                GoodsSkuEntity sku = skuList.get(0);
                item.put("price", sku.getPrice());
                if (StringUtil.isNotEmpty(goods.getIsDiscount()) && "1".equals(goods.getIsDiscount())) {
                    item.put("price", sku.getPrice() * goods.getDiscount() / 10);
                }
                item.put("oldPrice", sku.getOldPrice());
                item.put("stockNum", sku.getStockNum());
                if (StringUtil.isNotEmpty(sku.getImgUrl())) {
                    item.put("imgUrl", sku.getImgUrl());
                }
                String parmasValuesJson = sku.getParmasValuesJson();
                if (StringUtil.isNotEmpty(parmasValuesJson)) {
                    parmasValuesJson = parmasValuesJson
                            .replace("\"", "")
                            .replace("{", "")
                            .replace("}", "");
                } else {
                    parmasValuesJson = "";
                }
                item.put("parmasValuesJson", parmasValuesJson);
            }
            if (!isAuth) {
                item.put("price", "登陆后查看");
            }
            list.add(item);
        }
        map.put("list", list);
        map.put("code", 0);
        map.put("totalPage", pm.getTotalPage());
        map.put("currPage", pm.getCurrPage());
        return map;
    }

    @Override
    public Map<String, Object> saveReg(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String mobile = request.getParameter("mobile");
        String password = request.getParameter("password");
        String yaoqingma = request.getParameter("yaoqingma");
        String pro = request.getParameter("pro");
        String city = request.getParameter("city");
        String county = request.getParameter("county");
        String address = request.getParameter("address");
        String name = request.getParameter("name");
        String hql = "from WUserEntity as a where a.mobile = ? ";
        List<WUserEntity> userList = commonDao.findHql(hql, new Object[]{mobile});
        if (userList != null && !userList.isEmpty()) {
            map.put("code", 1);
            map.put("msg", "您输入的手机号码已被注册，请重新输入");
            return map;
        }
        WUserEntity parentUser = null;
        if (StringUtil.isNotEmpty(yaoqingma)) {
            parentUser = findUniqueByProperty(WUserEntity.class, "shareCode", yaoqingma);
            if (parentUser == null) {
                map.put("code", 1);
                map.put("msg", "您输入的邀请码不存在，或者错误，请核实后重新输入");
                return map;
            }
        }

        WUserEntity user = new WUserEntity();
        user.setPro(pro);
        user.setCity(city);
        user.setCounty(county);
        user.setName(name);
        user.setAddress(address);
        user.setCreateDate(DateUtil.getCurrentTime());
        user.setUpdateDate(DateUtil.getCurrentTime());
        user.setStatus("1");
        user.setMobile(mobile);
        user.setPassword(password);
        user.setIsAuth("0");
        user.setIsDelete("0");
        user.setLoginCount(0);
        user.setIsYewuyuan("0");
        user.setParentUser(parentUser);

        user.setLastLoginDate(DateUtil.getCurrentTime());
        user.setLastLoginIp(request.getRemoteAddr());
        user.setRegDate(DateUtil.getCurrentTime());
        user.setRegIp(request.getRemoteAddr());
        save(user);

        String code = ShareCodeUtils.getUserShareCode(user.getId());
        user.setShareCode(code);
        saveOrUpdate(user);
        map.put("userId", user.getId());
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> saveLogin(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String mobile = request.getParameter("mobile");
        String password = request.getParameter("password");
        String osName = request.getParameter("osName");
        String hql = "from WUserEntity as a where a.mobile = ? ";
        List<WUserEntity> userList = commonDao.findHql(hql, new Object[]{mobile});
        if (userList == null || userList.isEmpty()) {
            map.put("code", 1);
            map.put("msg", "您输入的手机号码不存在");
            return map;
        }
        WUserEntity user = userList.get(0);
        if (!user.getPassword().equals(password)) {
            map.put("code", 1);
            map.put("msg", "您输入的手机号码或者密码错误");
            return map;
        }
        if (user.getIsDelete().equals("1")) {
            map.put("code", 1);
            map.put("msg", "您的账号因为违规已被禁止操作");
            return map;
        }
        user.setDevice(osName);
        user.setLoginCount(user.getLoginCount() + 1);
        user.setLastLoginDate(DateUtil.getCurrentTime());
        user.setLastLoginIp(request.getRemoteAddr());
        saveOrUpdate(user);
        map.put("userId", user.getId());
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> saveWorkUserData(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        WUserEntity puserId = user.getParentUser();
        map.put("name", user.getName());
        map.put("nick", user.getNick());
        map.put("headUrl", user.getHeadUrl());
        map.put("shareCode", user.getShareCode());
        map.put("pUsercode", "null");
        if (StringUtil.isNotEmpty(puserId)) {
            map.put("pUserId", puserId.getId());
            map.put("pUsercode", puserId.getShareCode());
        }
        map.put("isAuth", user.getIsAuth());
        map.put("isYewuyuan", "0");
        if (StringUtil.isNotEmpty(user.getIsYewuyuan())) {
            map.put("isYewuyuan", user.getIsYewuyuan());
        }

        String hql = "from GoodsCollectEntity as a where a.user.id=" + userId;
        List<GoodsCollectEntity> gcList = findByQueryString(hql);
        map.put("collectCount", gcList.size());

        hql = "from GoodsViewEntity as a where a.user.id=" + userId;
        List<GoodsViewEntity> gvList = findByQueryString(hql);
        map.put("viewCount", gvList.size());


        UserLoginLogEntity ulll = new UserLoginLogEntity();
        ulll.setCreateDate(DateUtil.getCurrentTime());
        ulll.setUpdateDate(DateUtil.getCurrentTime());
        ulll.setStatus("1");
        ulll.setUser(user);
        ulll.setDay(DateUtil.getTimeByCustomPattern(DateUtil.getCurrentTime(), DateUtil.yyyyMMdd));
        save(ulll);

        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> getGoodsDetail(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String id = request.getParameter("id");
        String userId = request.getParameter("userId");
        WUserEntity user = null;
        boolean isAuth = false;
        if (StringUtil.isNotEmpty(userId)) {
            user = get(WUserEntity.class, Long.valueOf(userId));
            if (user.getIsAuth().equals("1"))
                isAuth = true;
        }
        GoodsEntity a = get(GoodsEntity.class, Long.valueOf(id));
        //lengsong
        //价格--判断是否是秒杀&折扣
        //定义折扣
        double basedis = a.getDiscount() / 10;
        if (a.getSeckillId() != null) {
            String hqls = "from SeckillGoodsPriceSetEntity as a "
                    + " where a.status = '1'"
                    + " and a.goods.id = " + id;
            List<SeckillGoodsPriceSetEntity> secList = findByQueryString(hqls);
            SeckillGoodsPriceSetEntity sec = secList.get(0);
            basedis = sec.getDiscount() / 10;
        } else {
            if (StringUtil.isNotEmpty(a.getIsDiscount()) && "1".equals(a.getIsDiscount())) {
                basedis = a.getDiscount() / 10;
            }
        }


        map.put("imgList", a.getImgList());
        map.put("discount", a.getDiscount());
        if (StringUtil.isNotEmpty(a.getImgList()) && a.getImgList().startsWith(",")) {
            map.put("imgList", a.getImgList().substring(1, a.getImgList().length()));
        }
        map.put("stockNum", a.getStockNum());
        map.put("saleNum", a.getSaleNum());
        map.put("price", a.getPrice() * basedis);
        if (!isAuth) {
            map.put("price", 0.0);
        }
        map.put("oldPrice", a.getOldPrice());
        map.put("info", a.getInfo());
        map.put("lookNum", a.getLookNum());
        map.put("imgUrl", a.getImgUrl());
        map.put("name", a.getName());
        map.put("isSeckill", "0");//是否是秒杀商品
        map.put("isCollect", "0");
        if (StringUtil.isNotEmpty(userId)) {
            String tempHql = "from GoodsCollectEntity as a where a.goods.id=" + id + " and a.user.id=" + userId;
            List<GoodsCollectEntity> gcList = findByQueryString(tempHql);
            if (gcList != null && !gcList.isEmpty()) {
                map.put("isCollect", "1");
            }
        }

        if (a.getSeckillId() != null) {
            map.put("isSeckill", 1);
            //当前秒杀的活动
            Date now = DateUtil.getCurrentTime();//当前时间
            SeckillActionEntity seckillAction = get(SeckillActionEntity.class, a.getSeckillId());
            if (now.getTime() < seckillAction.getBeginTime().getTime()) {
                //秒杀活动未开始
                map.put("isSeckill", "0");
            } else {
                if (now.getTime() > seckillAction.getEndTime().getTime()) {
                    //秒杀活动已结束
                    map.put("isSeckill", 0);
                } else {
                    //正在秒杀
                    Date end = seckillAction.getEndTime();
                    map.put("diffTime", end.getTime() - now.getTime());
                    map.put("isSeckill", "1");
                }
            }
        }
        map.put("isOnSale", a.getIsOnSale());
        map.put("isHasSku", "0");
        String hql = "from GoodsSkuEntity as a where a.goods.id=" + id + " and a.status = '1'";
        List<GoodsSkuEntity> goodsSkuList = findByQueryString(hql);
        List<Map<String, Object>> skuMapList = new ArrayList<>();
        if (goodsSkuList != null && !goodsSkuList.isEmpty()) {
            map.put("isHasSku", "1");
            GoodsSkuEntity sku = goodsSkuList.get(0);
            map.put("price", sku.getPrice() * basedis);
            if (!isAuth) {
                map.put("price", 0.0);
            }
            map.put("oldPrice", sku.getOldPrice());
            map.put("stockNum", sku.getStockNum());
            if (StringUtil.isNotEmpty(sku.getImgUrl())) {
                map.put("imgUrl", sku.getImgUrl());
            }

            for (GoodsSkuEntity skuItem : goodsSkuList) {
                Map<String, Object> item = new HashMap<>();
                item.put("id", skuItem.getId());
                item.put("price", skuItem.getPrice() * basedis);
                if (!isAuth) {
                    item.put("price", "登陆后查看");
                }
                item.put("stockNum", skuItem.getStockNum());
                item.put("imgUrl", skuItem.getImgUrl());
                item.put("parmasValuesJson", skuItem.getParmasValuesJson());
                skuMapList.add(item);
            }
            map.put("goodsSkuList", skuMapList);//商品的sku列表

            Map<String, Object> currSku = new HashMap<>();

            currSku.put("parmasValuesJson", sku.getParmasValuesJson());
            currSku.put("imgUrl", sku.getImgUrl());
            currSku.put("price", sku.getPrice() * basedis);
            if (!isAuth) {
                currSku.put("price", 0.0);
            }
            currSku.put("id", sku.getId());
            currSku.put("stockNum", sku.getStockNum());

            map.put("currSku", currSku);
        }
        hql = "from GoodsParamsEntity as a where a.goods.id=" + id;
        List<GoodsParamsEntity> paramsList = findByQueryString(hql);
        List<Map<String, Object>> skuList = new ArrayList<>();
        for (GoodsParamsEntity params : paramsList) {
            Map<String, Object> item = new HashMap<>();
            item.put("id", params.getId());
            item.put("skuName", params.getParamsName());
            String paramsVal = params.getParmasValues();
            String tempArr[] = paramsVal.split(",");
            List<Map<String, Object>> itemList = new ArrayList<>();
            int i = 0;
            for (String temp : tempArr) {
                Map<String, Object> tempMap = new HashMap<>();
                tempMap.put("isSelect", "0");
                if (i == 0) {
                    tempMap.put("isSelect", "1");
                }
                tempMap.put("name", temp);
                itemList.add(tempMap);
                i++;
            }
            item.put("itemList", itemList);
            skuList.add(item);
        }

        //查找赠品
        //商品满减
        String giftSql = "SELECT " +
                " a.name ," +
                " a.imgUrl ," +
                " a.stockNum, " +
                " c.conditionMoney " +
                "FROM " +
                " shop_gift a  " +
                " LEFT JOIN shop_action_gift b  on b.giftId =a.id " +
                " LEFT JOIN shop_action  c on c.ID = b.actionId " +
                " left JOIN shop_action_goods d on c.id = d.actionId " +
                " left join shop_goods e on e.id = d.goodsId " +
                " where  c.status = '1'  and  goodsId ='" + id + "'";
        List<GiftEntity> giftList = commonDao.findListbySql(giftSql);
        //品牌满减
        if (giftList.isEmpty()) {
            String brandSql = "SELECT " +
                    "a.NAME," +
                    "a.imgUrl," +
                    "a.stockNum, " +
                    " c.conditionMoney " +
                    "FROM " +
                    "shop_gift a " +
                    "LEFT JOIN shop_action_gift b ON a.id = b.giftId " +
                    "LEFT JOIN shop_action c ON b.actionId = c.id " +
                    "LEFT JOIN shop_action_brand d ON c.id = d.actionId " +
                    "LEFT JOIN shop_goods e ON d.brandId = e.brandId " +
                    "WHERE " +
                    "a.`status` = '1' " +
                    "AND c.actionType = '1' " +
                    "AND e.ID = '" + id + "'";
            giftList = commonDao.findListbySql(brandSql);
        }
        map.put("giftList", giftList);//赠品列表
        map.put("skuList", skuList);
        map.put("code", 0);
        return map;
    }

    @Override
    public void saveGoodsView(HttpServletRequest request) {
        String userId = request.getParameter("userId");
        if (StringUtil.isEmpty(userId)) {
            return;
        }
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        String goodsId = request.getParameter("id");
        GoodsEntity goods = get(GoodsEntity.class, Long.valueOf(goodsId));
        String hql = "from GoodsViewEntity as a where a.user.id=" + userId + " and a.goods.id=" + goodsId;
        List<GoodsViewEntity> gvList = findByQueryString(hql);
        if (gvList == null || gvList.isEmpty()) {
            GoodsViewEntity gv = new GoodsViewEntity();
            gv.setCreateDate(DateUtil.getCurrentTime());
            gv.setUpdateDate(DateUtil.getCurrentTime());
            gv.setStatus("1");
            gv.setGoods(goods);
            gv.setUser(user);
            save(gv);
        } else {
            GoodsViewEntity gv = gvList.get(0);
            gv.setCreateDate(DateUtil.getCurrentTime());
            gv.setUpdateDate(DateUtil.getCurrentTime());
            saveOrUpdate(gv);
        }
    }

    @Override
    public Map<String, Object> getAddressList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        String hql = "from AddressEntity as a where a.status='1' and a.user.id=" + user.getId();
        List<AddressEntity> addrList = findByQueryString(hql);
        if (addrList == null || addrList.isEmpty()) {
            map.put("code", 1);
            map.put("msg", "异常");
            return map;
        }
        List<Map<String, Object>> list = new ArrayList<>();
        for (AddressEntity addr : addrList) {
            Map<String, Object> item = new HashMap<>();
            item.put("id", addr.getId());
            item.put("name", addr.getName());
            item.put("mobile", addr.getMobile());
            item.put("info", addr.getInfo());
            item.put("pro", addr.getPro());
            item.put("city", addr.getCity());
            item.put("county", addr.getCounty());
            item.put("isDefault", addr.getIsDefault());
            if (StringUtil.isEmpty(addr.getIsDefault())) {
                item.put("isDefault", "0");
            }
            list.add(item);
        }
        map.put("code", 0);
        map.put("list", list);
        return map;
    }

    @Override
    public Map<String, Object> saveAddress(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        String id = request.getParameter("id");
        String pro = request.getParameter("pro");
        String city = request.getParameter("city");
        String county = request.getParameter("county");
        String name = request.getParameter("name");
        String mobile = request.getParameter("mobile");
        String info = request.getParameter("info");
        AddressEntity a = null;
        if (StringUtil.isEmpty(id)) {//地址为新地址
            a = new AddressEntity();
            a.setCreateDate(DateUtil.getCurrentTime());
            a.setUpdateDate(DateUtil.getCurrentTime());
            a.setStatus("1");
        } else {//地址为更新的话
            a = get(AddressEntity.class, Long.valueOf(id));
            a.setUpdateDate(DateUtil.getCurrentTime());
        }
        a.setName(name);
        a.setMobile(mobile);
        a.setPro(pro);
        a.setCity(city);
        a.setCounty(county);
        a.setInfo(info);
        a.setIsDefault("0");
        String hql = "from AddressEntity as a where a.status='1' and a.user.id=" + user.getId();
        List<AddressEntity> addrList = findByQueryString(hql);
        if (addrList.isEmpty()) {
            a.setIsDefault("1");
        }
        a.setUser(user);
        saveOrUpdate(a);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> editDefault(HttpServletRequest request) {
        String userId = request.getParameter("userId");
        String id = request.getParameter("id");

        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));

        String hql = "from AddressEntity as a where a.user.id=" + user.getId();
        List<AddressEntity> addrList = findByQueryString(hql);
        for(AddressEntity ad:addrList){
            ad.setIsDefault("0");
        }

        Map<String, Object> map = new HashMap<>();
        AddressEntity addr = get(AddressEntity.class, Long.valueOf(id));
        if (!addr.getUser().getId().equals(user.getId())) {
            map.put("code", 1);
            map.put("msg", "权限错误");
            return map;
        }
        addr.setIsDefault("1");
        addr.setStatus("1");
        saveOrUpdate(addr);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> deleteAddress(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        String id = request.getParameter("id");
        AddressEntity addr = get(AddressEntity.class, Long.valueOf(id));
        if (!addr.getUser().getId().equals(user.getId())) {
            map.put("code", 1);
            map.put("msg", "权限错误");
            return map;
        }
        addr.setIsDefault("0");
        addr.setStatus("0");
        saveOrUpdate(addr);

        String hql = "from AddressEntity as a where a.status='1' and a.user.id=" + user.getId();
        List<AddressEntity> addrList = findByQueryString(hql);
        if (addrList != null && !addrList.isEmpty()) {
            if (addrList.size() == 1) {
                addrList.get(0).setIsDefault("1");
                saveOrUpdate(addrList.get(0));
            }
        }
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> setDefaultAddress(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        String id = request.getParameter("id");
        AddressEntity addr = get(AddressEntity.class, Long.valueOf(id));
        if (!addr.getUser().getId().equals(user.getId())) {
            map.put("code", 1);
            map.put("msg", "权限错误");
            return map;
        }
        String hql = "from AddressEntity as a where a.status='1' and a.user.id=" + user.getId();
        List<AddressEntity> addrList = findByQueryString(hql);
        if (addrList != null && !addrList.isEmpty()) {
            for (AddressEntity a : addrList) {
                a.setIsDefault("0");
                saveOrUpdate(a);
            }
        }
        addr.setIsDefault("1");
        saveOrUpdate(addr);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> getAddressDetail(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String id = request.getParameter("id");
        AddressEntity a = get(AddressEntity.class, Long.valueOf(id));
        map.put("code", 0);
        map.put("pro", a.getPro());
        map.put("city", a.getCity());
        map.put("county", a.getCounty());
        map.put("info", a.getInfo());
        map.put("name", a.getName());
        map.put("mobile", a.getMobile());
        map.put("addrId", a.getId());
        return map;
    }

    @Override
    public Map<String, Object> saveUserNick(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        String nick = request.getParameter("nick");
        user.setNick(nick);
        saveOrUpdate(user);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> saveUserPcode(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        String pUsercode = request.getParameter("pUsercode");
        String hql = "from WUserEntity as a where shareCode = "+pUsercode;
        List<WUserEntity> pUser = commonDao.findHql(hql);
        //WUserEntity pUser = get(WUserEntity.class, Long.valueOf(pUserId));
        if (pUser.size() > 0){
            user.setParentUser(pUser.get(0));
            saveOrUpdate(user);
            map.put("code", 0);

        }else {
            map.put("code", 1);
            map.put("msg", "业务员分享码错误！");
        }
        return map;
    }

    @Override
    public Map<String, Object> saveUserPhoto(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        String url = request.getParameter("url");
        user.setHeadUrl(url);
        saveOrUpdate(user);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> getUpToken(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        String ak = "dh-B1G5wdgzDY6DxQZCfI0ZB2R_gIWDrGyvu7Grn";
        String sk = "oyzuAZHY2q7IsvGBTccbXn_hteBLfRbKMrXiE0K7";    // 密钥配置
        Auth auth = Auth.create(ak, sk);
        String bucketname = "image";
        String token = auth.uploadToken(bucketname, null, 3600, new StringMap().put("insertOnly", 1));
        System.out.println(token);
        map.put("upToken", token);
        return map;
    }

    @Override
    public Map<String, Object> getCouponList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String hql = "from CouponEntity as a where a.status = '1' and a.beginTime <= ? and a.endTime >= ? order by a.createDate desc";
        List<CouponEntity> couponList = commonDao.findHql(hql, new Object[]{DateUtil.getCurrentTime(), DateUtil.getCurrentTime()});
        List<Map<String, Object>> list = new ArrayList<>();
        for (CouponEntity cp : couponList) {
            Map<String, Object> item = new HashMap<>();
            item.put("id", cp.getId());
			/*
			 * private String name;//优惠券名称 10元优惠券
	private String tips;//优惠券说明
	private Double money;//优惠券金额  10
	private Double conditionMoney;//需要满足的条件金额 100
	private Date beginTime;//开始时间
	private Date endTime;//结束时间
	private Integer allNum;//总数量
	private Integer joinNum;//已领取数量
			 */
            item.put("name", cp.getName());
            item.put("allNum", cp.getAllNum());
            item.put("tips", cp.getTips());
            item.put("money", cp.getMoney());
            item.put("conditionMoney", cp.getConditionMoney());
            item.put("joinNum", cp.getJoinNum());
            item.put("endTime", DateUtil.getTimeByDefaultPattern(cp.getEndTime()));
            list.add(item);
        }
        if (list == null || list.isEmpty()) {
            map.put("code", 1);
            return map;
        }
        map.put("code", 0);
        map.put("list", list);
        return map;
    }

    @Override
    public Map<String, Object> getUserCouponList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        String state = request.getParameter("state");

        List<Map<String, Object>> list = new ArrayList<>();
        List<WUserCouponEntity> wcpList = null;
        String hql = null;
        if ("0".equals(state)) {
            //未使用的
            hql = "from WUserCouponEntity as a where a.user.id=" + userId + " and a.isUsed = '0' and a.coupon.endTime >= ?";
            wcpList = findHql(hql, new Object[]{DateUtil.getCurrentTime()});
        } else if ("1".equals(state)) {
            //已使用的
            hql = "from WUserCouponEntity as a where a.user.id=" + userId + " and a.isUsed = '1' ";
            wcpList = findHql(hql, new Object[]{});
        } else if ("2".equals(state)) {
            //已过期的
            hql = "from WUserCouponEntity as a where a.user.id=" + userId + " and a.isUsed = '0' and a.coupon.endTime < ?";
            wcpList = findHql(hql, new Object[]{DateUtil.getCurrentTime()});
        }
        if (wcpList != null) {
            for (WUserCouponEntity wcp : wcpList) {
                Map<String, Object> item = new HashMap<>();
                CouponEntity cp = wcp.getCoupon();
                item.put("id", cp.getId());
                item.put("name", cp.getName());
                item.put("allNum", cp.getAllNum());
                item.put("tips", cp.getTips());
                item.put("money", cp.getMoney());
                item.put("conditionMoney", cp.getConditionMoney());
                item.put("joinNum", cp.getJoinNum());
                item.put("endTime", DateUtil.getTimeByDefaultPattern(cp.getEndTime()));
                list.add(item);
            }
        } else {
            map.put("code", 1);
            return map;
        }
        map.put("code", 0);
        map.put("list", list);
        return map;
    }

    @Override
    public Map<String, Object> saveUserCoupon(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        String id = request.getParameter("id");

        CouponEntity coupon = get(CouponEntity.class, Long.valueOf(id));

        String hql = "from WUserCouponEntity as a where a.user.id=" + userId + " and a.coupon.id=" + id;
        List<WUserCouponEntity> wcpList = findByQueryString(hql);

        if (wcpList != null && !wcpList.isEmpty()) {
            map.put("code", 1);
            map.put("msg", "您已经领取了该优惠券，请不要重复领取");
            return map;
        }

        WUserCouponEntity wcp = new WUserCouponEntity();
        wcp.setIsUsed("0");
        wcp.setUser(user);
        wcp.setCoupon(coupon);
        wcp.setCreateDate(DateUtil.getCurrentTime());
        wcp.setUpdateDate(DateUtil.getCurrentTime());
        wcp.setStatus("1");
        save(wcp);

        map.put("code", 0);

        return map;
    }

    @Override
    public Map<String, Object> saveGoodsToCart(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        if (user == null) {
            map.put("code", 10);
            map.put("msg", "您尚未登录，请登录后重新操作");
            return map;
        }
        String goodsId = request.getParameter("goodsId");
        String skuId = request.getParameter("skuId");//库存ID
        int num = Integer.parseInt(request.getParameter("num"));
        GoodsSkuEntity sku = null;
        if (StringUtil.isNotEmpty(skuId)) {
            sku = get(GoodsSkuEntity.class, Long.valueOf(skuId));
        }
        GoodsEntity goods = get(GoodsEntity.class, Long.valueOf(goodsId));
        if (sku != null) {
            if (sku.getStockNum() < num) {
                map.put("code", 1);
                map.put("msg", "库存不足");
                return map;
            }
            CartEntity cart = null;
            String hql = "from CartEntity as a where a.user.id=" + user.getId() + " and a.sku.id=" + skuId;
            List<CartEntity> cartList = findByQueryString(hql);
            if (cartList == null || cartList.isEmpty()) {
                cart = new CartEntity();
                cart.setCreateDate(DateUtil.getCurrentTime());
                cart.setUpdateDate(DateUtil.getCurrentTime());
                cart.setStatus("1");
                cart.setUser(user);
                cart.setSku(sku);
                cart.setNum(num);
                cart.setGoods(goods);
                save(cart);
            } else {
                cart = cartList.get(0);
                cart.setUpdateDate(DateUtil.getCurrentTime());
                cart.setUser(user);
                cart.setSku(sku);
                cart.setGoods(goods);
                cart.setNum(cart.getNum() + num);
                saveOrUpdate(cart);
            }
        } else {
            if (goods.getStockNum() < num) {
                map.put("code", 1);
                map.put("msg", "库存不足");
                return map;
            }
            CartEntity cart = null;
            String hql = "from CartEntity as a where a.user.id=" + user.getId() + " and a.goods.id=" + goodsId;
            List<CartEntity> cartList = findByQueryString(hql);
            if (cartList == null || cartList.isEmpty()) {
                cart = new CartEntity();
                cart.setCreateDate(DateUtil.getCurrentTime());
                cart.setUpdateDate(DateUtil.getCurrentTime());
                cart.setStatus("1");
                cart.setUser(user);
                cart.setSku(sku);
                cart.setNum(num);
                cart.setGoods(goods);
                save(cart);
            } else {
                cart = cartList.get(0);
                cart.setUpdateDate(DateUtil.getCurrentTime());
                cart.setUser(user);
                cart.setSku(sku);
                cart.setGoods(goods);
                cart.setNum(cart.getNum() + num);
                saveOrUpdate(cart);
            }
        }
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> getGoodsBuyDetail(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String userId = request.getParameter("userId");
        if (StringUtil.isEmpty(userId)) {
            map.put("code", 10);
            map.put("msg", "您尚未登录，请登录后重新操作");
            return map;
        }
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        user = get(WUserEntity.class, user.getId());
        map.put("addressId", "");
        //获取用户的默认地址
        String tempHql = "from AddressEntity as a where a.user.id=" + userId + " and a.isDefault='1'";
        List<AddressEntity> addrList = findByQueryString(tempHql);
        if (addrList != null && !addrList.isEmpty()) {
            AddressEntity addr = addrList.get(0);
            map.put("addressId", addr.getId());
            map.put("address", addr.getName() + addr.getMobile() + addr.getCity() + addr.getInfo());
        }
        String goodsId = request.getParameter("goodsId");
        String skuId = request.getParameter("skuId");//库存ID
        int num = Integer.parseInt(request.getParameter("num"));
        GoodsEntity goods = get(GoodsEntity.class, Long.valueOf(goodsId));
        //lengsong
        //价格--判断是否是秒杀&折扣
        //定义折扣
        double basedis = 1;
        if (goods.getSeckillId() != null) {
            String hqls = "from SeckillGoodsPriceSetEntity as a "
                    + " where a.status = '1'"
                    + " and a.goods.id = " + goodsId;
            List<SeckillGoodsPriceSetEntity> secList = findByQueryString(hqls);
            SeckillGoodsPriceSetEntity sec = secList.get(0);
            basedis = sec.getDiscount() / 10;
        } else {
            if (StringUtil.isNotEmpty(goods.getIsDiscount()) && "1".equals(goods.getIsDiscount())) {
                basedis = goods.getDiscount() / 10;
            }
        }
        map.put("num", num);
        map.put("name", goods.getName());
        map.put("imgUrl", goods.getImgUrl());
        map.put("price", goods.getPrice() * num * basedis);
        map.put("sku", "");
        map.put("money", goods.getPrice() * num * basedis);
        GoodsSkuEntity sku = null;
        if (StringUtil.isNotEmpty(skuId)) {
            sku = get(GoodsSkuEntity.class, Long.valueOf(skuId));
            map.put("imgUrl", sku.getImgUrl());
            map.put("price", sku.getPrice() * basedis);
            map.put("sku", sku.getParmasValuesJson());
            map.put("money", sku.getPrice() * num * basedis);
        }
        Double money = sku.getPrice() * num * basedis;
        map.put("couponId", "");
        tempHql = "from WUserCouponEntity as a where a.coupon.conditionMoney <= ? and a.user.id=" + userId + " and a.isUsed= '0' and a.coupon.endTime >= ? and a.coupon.beginTime <= ? order by a.coupon.money desc";
        List<WUserCouponEntity> wcList = findHql(tempHql, new Object[]{money, DateUtil.getCurrentTime(), DateUtil.getCurrentTime()});
        if (wcList != null && !wcList.isEmpty()) {
            WUserCouponEntity wc = wcList.get(0);
            map.put("couponId", wc.getCoupon().getId());
            map.put("userCouponId", wc.getId());
            map.put("couponName", wc.getCoupon().getName());
            map.put("couponMoney", wc.getCoupon().getMoney());
        }
        //得到赠品活动列表
        List<ActionEntity> actionlist = getActionListByGoodsEntity(goods, money);
        //处理购物商品和赠品间的关系
        int giftnum = 0;
        if (actionlist != null && actionlist.size() > 0) {
            for (ActionEntity actionEntity : actionlist) {
                String ghql = "from GiftEntity as a where a.status ='1' and a.stockNum > 0 " +
                        "and exists (select 1 from ActionGiftEntity as b where b.gift.id = a.id and b.action.id =" + actionEntity.getId() + ")";
                List<GiftEntity> giftList = findHql(ghql);
                List<Map<String, Object>> glist = new ArrayList<>();
                for (GiftEntity gf : giftList) {
                    giftnum += 1;
                    Map<String, Object> item = new HashMap<>();
                    item.put("id", gf.getId());
                    item.put("imgUrl", gf.getImgUrl());
                    item.put("gname", gf.getName());
                    item.put("price", gf.getMoney());
                    item.put("num", giftnum);
                    item.put("saleNum", gf.getSaleNum());
                    item.put("stockNum", gf.getStockNum());
                    glist.add(item);
                    map.put("giftList", glist);
                }
                giftnum = 0;
            }
        }
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> deleteCart(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        if (user == null) {
            map.put("code", 10);
            return map;
        }
        String id = request.getParameter("id");
        CartEntity cart = get(CartEntity.class, Long.valueOf(id));
        if (!cart.getUser().getId().equals(user.getId())) {
            map.put("code", 1);
            return map;
        }
        delete(cart);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> getCartData(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        if (user == null) {
            map.put("code", 10);
            return map;
        }
        user = get(WUserEntity.class, user.getId());
        String hql = "from CartEntity as a where a.user.id=" + user.getId();
        List<CartEntity> cartList = findByQueryString(hql);
        if (cartList == null || cartList.isEmpty()) {
            map.put("code", 1);
            map.put("msg", "购物车为空");
            return map;
        }

        List<Map<String, Object>> listMap = new ArrayList<>();
        for (CartEntity cart : cartList) {
            //lengsong
            //价格--判断是否是秒杀&折扣
            //定义折扣
            double basedis = 1;
            if (cart.getGoods().getSeckillId() != null) {
                String hqls = "from SeckillGoodsPriceSetEntity as a "
                        + " where a.status = '1'"
                        + " and a.goods.id = " + cart.getGoods().getId();
                List<SeckillGoodsPriceSetEntity> secList = findByQueryString(hqls);
                SeckillGoodsPriceSetEntity sec = secList.get(0);
                basedis = sec.getDiscount() / 10;
            } else {
                if (StringUtil.isNotEmpty(cart.getGoods().getIsDiscount()) && "1".equals(cart.getGoods().getIsDiscount())) {
                    basedis = cart.getGoods().getDiscount() / 10;
                }
            }
            Map<String, Object> item = new HashMap<>();
            item.put("id", cart.getId());
            item.put("name", cart.getGoods().getName());
            item.put("imgUrl", cart.getGoods().getImgUrl());
            item.put("price", cart.getGoods().getPrice() * basedis);
            item.put("isOnSale", cart.getGoods().getIsOnSale());
            item.put("cornerImg", cart.getGoods().getCornerImg());
            item.put("stockNum", cart.getGoods().getStockNum());
            item.put("isHasSku", "0");
            item.put("sku", "");
            if (cart.getSku() != null) {
                item.put("imgUrl", cart.getSku().getImgUrl());
                item.put("price", cart.getSku().getPrice() * basedis);
                //item.put("price", cart.getSku().getPrice());
                item.put("stockNum", cart.getSku().getStockNum());
                item.put("parmasValuesJson", cart.getSku().getParmasValuesJson());
                item.put("isHasSku", "1");
            }
            item.put("num", cart.getNum());
            item.put("selected", true);
            if (!cart.getGoods().getIsOnSale().equals("1")) {
                item.put("selected", false);
            }
            listMap.add(item);
        }
        map.put("code", 0);
        map.put("list", listMap);
        return map;
    }

    @Override
    public Map<String, Object> updateCart(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        String userId = request.getParameter("userId");
        if (StringUtil.isEmpty(userId)) {
            map.put("code", 10);
            return map;
        }
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        if (user == null) {
            map.put("code", 10);
            return map;
        }
        String id = request.getParameter("id");
        CartEntity cart = get(CartEntity.class, Long.valueOf(id));
        int num = Integer.parseInt(request.getParameter("num"));
        cart.setNum(num);
        saveOrUpdate(cart);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> getCartNum(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        String userId = request.getParameter("userId");
        if (StringUtil.isEmpty(userId)) {
            map.put("code", 10);
            return map;
        }
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        String hql = "from CartEntity as a where a.user.id=" + user.getId();
        List<CartEntity> cartList = findByQueryString(hql);
        if (cartList == null || cartList.isEmpty()) {
            map.put("code", 1);
            map.put("num", 0);
            return map;
        }
        map.put("code", 0);
        map.put("num", cartList.size());
        return map;
    }

    @Override
    public Map<String, Object> saveGoodsCollect(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String userId = request.getParameter("userId");
        if (StringUtil.isEmpty(userId)) {
            map.put("code", 10);
            return map;
        }
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        String goodsId = request.getParameter("goodsId");
        GoodsEntity goods = get(GoodsEntity.class, Long.valueOf(goodsId));
        String hql = "from GoodsCollectEntity as a where a.user.id=" + userId + " and a.goods.id=" + goodsId;
        List<GoodsCollectEntity> gvList = findByQueryString(hql);
        if (gvList == null || gvList.isEmpty()) {
            GoodsCollectEntity gv = new GoodsCollectEntity();
            gv.setCreateDate(DateUtil.getCurrentTime());
            gv.setUpdateDate(DateUtil.getCurrentTime());
            gv.setStatus("1");
            gv.setGoods(goods);
            gv.setUser(user);
            save(gv);
        } else {
            GoodsCollectEntity gv = gvList.get(0);
            gv.setCreateDate(DateUtil.getCurrentTime());
            gv.setUpdateDate(DateUtil.getCurrentTime());
            saveOrUpdate(gv);
        }
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> getCartBuyData(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        String userId = request.getParameter("userId");
        if (StringUtil.isEmpty(userId)) {
            map.put("code", 10);
            return map;
        }
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        if (user == null) {
            map.put("code", 10);
            return map;
        }
        user = get(WUserEntity.class, user.getId());

        map.put("addressId", "");
        //获取用户的默认地址
        String tempHql = "from AddressEntity as a where a.user.id=" + userId + " and a.isDefault='1'";
        List<AddressEntity> addrList = findByQueryString(tempHql);
        if (addrList != null && !addrList.isEmpty()) {
            AddressEntity addr = addrList.get(0);
            map.put("addressId", addr.getId());
            map.put("address", addr.getName() + addr.getMobile() + addr.getCity() + addr.getInfo());
        }

        String cartIds = request.getParameter("cartIds");
        String cartIdArr[] = cartIds.split(",");
        List<Map<String, Object>> listMap = new ArrayList<>();
        Double money = 0d;
        int num = 0;
        int giftnum = 0;
        List<Map<String, Object>> glist = new ArrayList<>();
        for (String cartId : cartIdArr) {
            //购物车每个商品的总价
            Double itemmoney = 0d;
            CartEntity cart = get(CartEntity.class, Long.valueOf(cartId));
            //lengsong
            //价格--判断是否是秒杀&折扣
            //定义折扣
            double basedis = 1;
            if (cart.getGoods().getSeckillId() != null) {
                String hqls = "from SeckillGoodsPriceSetEntity as a "
                        + " where a.status = '1'"
                        + " and a.goods.id = " + cart.getGoods().getId();
                List<SeckillGoodsPriceSetEntity> secList = findByQueryString(hqls);
                SeckillGoodsPriceSetEntity sec = secList.get(0);
                basedis = sec.getDiscount() / 10;
            } else {
                if (StringUtil.isNotEmpty(cart.getGoods().getIsDiscount()) && "1".equals(cart.getGoods().getIsDiscount())) {
                    basedis = cart.getGoods().getDiscount() / 10;
                }
            }
            Map<String, Object> item = new HashMap<>();
            item.put("id", cart.getId());
            item.put("name", cart.getGoods().getName());
            item.put("imgUrl", cart.getGoods().getImgUrl());
            item.put("price", cart.getGoods().getPrice() * basedis);
            //item.put("price", cart.getGoods().getPrice());
            item.put("isOnSale", cart.getGoods().getIsOnSale());
            item.put("cornerImg", cart.getGoods().getCornerImg());
            item.put("stockNum", cart.getGoods().getStockNum());
            item.put("isHasSku", "0");
            item.put("sku", "");
            if (cart.getSku() != null) {
                item.put("imgUrl", cart.getSku().getImgUrl());
                item.put("price", cart.getSku().getPrice() * basedis);
                //item.put("price", cart.getSku().getPrice());
                item.put("stockNum", cart.getSku().getStockNum());
                item.put("parmasValuesJson", cart.getSku().getParmasValuesJson());
                item.put("isHasSku", "1");
                itemmoney += cart.getSku().getPrice() * cart.getNum() * basedis;
                money += cart.getSku().getPrice() * cart.getNum() * basedis;
            } else {
                itemmoney += cart.getSku().getPrice() * cart.getNum() * basedis;
                money += cart.getGoods().getPrice() * cart.getNum() * basedis;
            }
            num += cart.getNum();
            item.put("num", cart.getNum());
            item.put("selected", true);
            if (!cart.getGoods().getIsOnSale().equals("1")) {
                item.put("selected", false);
            }
            listMap.add(item);
            //得到赠品活动列表
            GoodsEntity goods = get(GoodsEntity.class, Long.valueOf(cart.getGoods().getId()));
            List<ActionEntity> actionlist = getActionListByGoodsEntity(goods, itemmoney);
            //处理购物商品和赠品间的关系
            if (actionlist != null && actionlist.size() > 0) {
                for (ActionEntity actionEntity : actionlist) {
                    String ghql = "from GiftEntity as a where a.status ='1' and a.stockNum > 0 " +
                            "and exists (select 1 from ActionGiftEntity as b where b.gift.id = a.id and b.action.id =" + actionEntity.getId() + ")";
                    List<GiftEntity> giftList = findHql(ghql);
                    for (GiftEntity gf : giftList) {
                        Map<String, Object> items = new HashMap<>();
                        Optional<Map<String, Object>> op = glist.stream().filter(l -> {
                            Long id = (Long) l.get("id");
                            return id == gf.getId();
                        }).findFirst();
                        if (op.isPresent()) {
                            Map<String, Object> goodsMap = op.get();
                            goodsMap.put("num", NumberUtil.add(String.valueOf(goodsMap.get("num")), "1").intValue());
                            continue;
                        }
                        items.put("id", gf.getId());
                        items.put("imgUrl", gf.getImgUrl());
                        items.put("gname", gf.getName());
                        items.put("price", gf.getMoney());
                        items.put("num", "1");
                        items.put("saleNum", gf.getSaleNum());
                        items.put("stockNum", gf.getStockNum());
                        glist.add(items);
                    }
                }
            }
        }
        map.put("giftList", glist);
        map.put("couponId", "");
        tempHql = "from WUserCouponEntity as a where a.coupon.conditionMoney <= ? and a.user.id=" + userId + " and a.isUsed= '0' and a.coupon.endTime >= ? and a.coupon.beginTime <= ? order by a.coupon.money desc";
        List<WUserCouponEntity> wcList = findHql(tempHql, new Object[]{money, DateUtil.getCurrentTime(), DateUtil.getCurrentTime()});
        if (wcList != null && !wcList.isEmpty()) {
            WUserCouponEntity wc = wcList.get(0);
            map.put("couponId", wc.getCoupon().getId());
            map.put("userCouponId", wc.getId());
            map.put("couponName", wc.getCoupon().getName());
            map.put("couponMoney", wc.getCoupon().getMoney());
        }

        map.put("money", money);
        map.put("num", num);
        map.put("code", 0);
        map.put("list", listMap);
        return map;
    }

    @Override
    public Map<String, Object> createGoodsBuyOrder(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        String userId = request.getParameter("userId");
        if (StringUtil.isEmpty(userId)) {
            map.put("code", 10);
            return map;
        }
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        if (user == null) {
            map.put("code", 10);
            return map;
        }
        user = get(WUserEntity.class, user.getId());
        String goodsId = request.getParameter("goodsId");
        String skuId = request.getParameter("skuId");//库存ID
        int num = Integer.parseInt(request.getParameter("num"));
        String bak = request.getParameter("bak");
        String addressId = request.getParameter("addressId");
        String couponId = request.getParameter("couponId");
        Double couponMoney = 0d;
        if (StringUtil.isNotEmpty(couponId)) {
            WUserCouponEntity wcp = get(WUserCouponEntity.class, Long.valueOf(couponId));
            wcp.setIsUsed("1");
            wcp.setUsedTime(DateUtil.getCurrentTime());
            saveOrUpdate(wcp);
            couponMoney = wcp.getCoupon().getMoney();
        }
        AddressEntity addr = get(AddressEntity.class, Long.valueOf(addressId));
        GoodsEntity goods = get(GoodsEntity.class, Long.valueOf(goodsId));
        GoodsSkuEntity sku = null;
        if (StringUtil.isNotEmpty(skuId)) {
            sku = get(GoodsSkuEntity.class, Long.valueOf(skuId));
        }
        Double price = goods.getPrice();
        //lengsong
        //价格--判断是否是秒杀&折扣
        //定义折扣
        double basedis = 1;
        if (goods.getSeckillId() != null) {
            String hqls = "from SeckillGoodsPriceSetEntity as a "
                    + " where a.status = '1'"
                    + " and a.goods.id = " + goodsId;
            List<SeckillGoodsPriceSetEntity> secList = findByQueryString(hqls);
            SeckillGoodsPriceSetEntity sec = secList.get(0);
            basedis = sec.getDiscount() / 10;
        } else {
            if (StringUtil.isNotEmpty(goods.getIsDiscount()) && "1".equals(goods.getIsDiscount())) {
                basedis = goods.getDiscount() / 10;
            }
        }
        //判断是秒杀活动
        if (goods.getSeckillId() != null) {
            SeckillActionEntity seckillAction = get(SeckillActionEntity.class, goods.getSeckillId());
            String seclimit = seckillAction.getLimitBuy();
            int limit = Integer.parseInt(seclimit);
            if (limit != 0) {
                //判断当前订单数量
                if (limit >= num) {
                    String secHql = "from OrderDetailEntity as b where b.goods.id = " + goodsId + " and exists (select id from OrderEntity where id = b.sOrder.id and orderState !='-1' and  user.id=" + userId + " and to_days(now()) - to_days(createDate) = 0)";
                    List<OrderDetailEntity> secList = findByQueryString(secHql);
                    int disnum = 0;
                    for (OrderDetailEntity diss : secList) {
                        disnum += diss.getNum();
                    }
                    if (disnum >= limit) {
                        map.put("code", 10);
                        map.put("msg", "秒杀商品购买数量超出当日限额！");
                        return map;
                    }
                } else {
                    map.put("code", 10);
                    map.put("msg", "秒杀商品购买数量超出当日限额！");
                    return map;
                }
            }
        } else {
            //判断是折扣商品活动
            if (StringUtil.isNotEmpty(goods.getIsDiscount()) && "1".equals(goods.getIsDiscount())) {
                int dis = goods.getDislimit();
                //获取用户折扣订单限制数量
                if (StringUtil.isNotEmpty(sku.getDisbuylimit())) {
                    dis = sku.getDisbuylimit();
                } else {
                    dis = goods.getDislimit();
                }

                //判断当前订单数量
                if (dis >= num) {
                    String nowDay = DateUtil.getTimeByCustomPattern(DateUtil.getCurrentTime(), DateUtil.yyyyMMddHHmmss);
                    String disHql = "from OrderDetailEntity as b where b.goods.id = " + goodsId + " and exists (select id from OrderEntity where id = b.sOrder.id and orderState !='-1' and  user.id=" + userId + " and to_days(now()) - to_days(createDate) = 0)";
                    List<OrderDetailEntity> disList = findByQueryString(disHql);
                    int disnum = 0;
                    for (OrderDetailEntity diss : disList) {
                        disnum += diss.getNum();
                    }
                    if (disnum >= dis) {
                        map.put("code", 10);
                        map.put("msg", "折扣商品购买数量超出当日限额！");
                        return map;
                    }
                } else {
                    map.put("code", 10);
                    map.put("msg", "折扣商品购买数量超出当日限额！");
                    return map;
                }
            }
        }
        if (StringUtil.isNotEmpty(skuId)) {
            sku = get(GoodsSkuEntity.class, Long.valueOf(skuId));
            if (sku.getStockNum() < num) {
                map.put("code", 1);
                map.put("msg", "库存不足");
                return map;
            }
            price = sku.getPrice();
        } else {
            if (goods.getStockNum() < num) {
                map.put("code", 1);
                map.put("msg", "库存不足");
                return map;
            }
        }
        OrderEntity order = new OrderEntity();
        order.setCreateDate(DateUtil.getCurrentTime());
        order.setUpdateDate(DateUtil.getCurrentTime());
        order.setStatus("1");
        order.setUser(user);
        order.setOrderState("1");
        order.setIsFendan("0");
        order.setIsRefund("0");
        order.setIsSalesAfter("0");
        order.setBak(bak);
        order.setOrderNo(EShopUtil.getOrderNo());
        order.setNum(num);
        order.setMoney(num * price * basedis - couponMoney);
        order.setCouponMoney(couponMoney);
        order.setName(addr.getName());
        order.setMobile(addr.getMobile());
        order.setAddress(addr.getPro() + addr.getCity() + addr.getCounty() + addr.getInfo());
        save(order);

        OrderDetailEntity od = new OrderDetailEntity();
        od.setCreateDate(DateUtil.getCurrentTime());
        od.setUpdateDate(DateUtil.getCurrentTime());
        od.setSku(sku);
        od.setStatus("1");
        od.setMoney(num * price * basedis);
        od.setPrice(price * basedis);
        od.setsOrder(order);
        od.setGoods(goods);
        od.setNum(num);
        save(od);
        if (sku != null) {
            sku.setStockNum(sku.getStockNum() - num);
            sku.setSaleNum(sku.getSaleNum() + num);
            // GoodsSkuEntity goodsSkuEntity = new GoodsSkuEntity();
            //BeanUtils.copyProperties(sku, goodsSkuEntity);
            //goodsSkuEntity.setId(null);
            //goodsSkuEntity.setCreateDate(new Date());
            sku.setIsNew("1");
            saveOrUpdate(sku);
        }
        goods.setStockNum(goods.getStockNum() - num);
        saveOrUpdate(goods);
        Double money = 0D;
        money = num * price * basedis - couponMoney;
        //商品满赠&品牌满赠
        //得到赠品活动列表
        List<ActionEntity> actionlist = getActionListByGoodsEntity(goods, money);
        //处理订单和赠品间的关系
        if (actionlist != null && actionlist.size() > 0) {
            int giftnum = 0;
            for (ActionEntity actionEntity : actionlist) {
                giftnum += 1;
                String ghql = "from GiftEntity as a where a.status ='1' and a.stockNum > 0 " +
                        "and exists (select 1 from ActionGiftEntity as b where b.gift.id = a.id and b.action.id =" + actionEntity.getId() + ")";
                List<GiftEntity> giftList = findHql(ghql);
                for (GiftEntity gf : giftList) {
                    if (gf.getStockNum() > 0) {
                        gf.setStockNum(gf.getStockNum() - 1);
                        gf.setSaleNum(gf.getSaleNum() + 1);
                    }
                    OrderActionEntity orderActionEntity = new OrderActionEntity();
                    orderActionEntity.setGift(gf);
                    orderActionEntity.setAction(actionEntity);
                    orderActionEntity.setOrder(order);
                    orderActionEntity.setCreateDate(DateUtil.getCurrentTime());
                    orderActionEntity.setUpdateDate(DateUtil.getCurrentTime());
                    orderActionEntity.setStatus("1");
                    orderActionEntity.setSaleNum(giftnum);
                    save(orderActionEntity);
                    save(gf);
                }
                giftnum = 0;
            }
        }
        map.put("code", 0);
        map.put("orderId", order.getId());
        return map;
    }

    @Override
    public Map<String, Object> createCartBuyOrder(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));

        if (user == null) {
            map.put("code", 10);
            return map;
        }
        user = get(WUserEntity.class, user.getId());
        String bak = request.getParameter("bak");
        String cartIds = request.getParameter("cartIds");
        String addressId = request.getParameter("addressId");
        String couponId = request.getParameter("couponId");
        Double couponMoney = 0d;
        if (StringUtil.isNotEmpty(couponId)) {
            WUserCouponEntity wcp = get(WUserCouponEntity.class, Long.valueOf(couponId));
            wcp.setIsUsed("1");
            wcp.setUsedTime(DateUtil.getCurrentTime());
            saveOrUpdate(wcp);
            couponMoney = wcp.getCoupon().getMoney();
        }

        AddressEntity addr = get(AddressEntity.class, Long.valueOf(addressId));
        Double money = 0D;
        Double itemmoney = 0D;
        int goodsNum = 0;
        String cartIdArr[] = cartIds.split(",");
        List<CartEntity> cartList = new ArrayList<>();
        for (String id : cartIdArr) {
            CartEntity cart = get(CartEntity.class, Long.valueOf(id));
            cartList.add(cart);
            GoodsEntity goods = cart.getGoods();
            GoodsSkuEntity sku = cart.getSku();
            goods = get(GoodsEntity.class, goods.getId());
            //lengsong
            //价格--判断是否是秒杀&折扣
            //定义折扣
            double basedis = 1;
            if (goods.getSeckillId() != null) {
                String hqls = "from SeckillGoodsPriceSetEntity as a "
                        + " where a.status = '1'"
                        + " and a.goods.id = " + goods.getId();
                List<SeckillGoodsPriceSetEntity> secList = findByQueryString(hqls);
                SeckillGoodsPriceSetEntity sec = secList.get(0);
                basedis = sec.getDiscount() / 10;
            } else {
                if (StringUtil.isNotEmpty(goods.getIsDiscount()) && "1".equals(goods.getIsDiscount())) {
                    basedis = goods.getDiscount() / 10;
                }
            }
            //lengsong 折扣&秒杀限制
            //判断是秒杀活动商品活动
            if (goods.getSeckillId() != null) {
                SeckillActionEntity seckillAction = get(SeckillActionEntity.class, goods.getSeckillId());
                String seclimit = seckillAction.getLimitBuy();
                int limit = Integer.parseInt(seclimit);
                if (limit != 0) {
                    //判断当前订单数量
                    if (limit >= cart.getNum()) {
                        String secHql = "from OrderDetailEntity as b where b.goods.id = " + goods.getId() + " and exists (select id from OrderEntity where id = b.sOrder.id and orderState !='-1' and  user.id=" + user.getId() + " and to_days(now()) - to_days(createDate) = 0)";
                        List<OrderDetailEntity> secList = findByQueryString(secHql);
                        int disnum = 0;
                        for (OrderDetailEntity diss : secList) {
                            disnum += diss.getNum();
                        }
                        if (disnum >= limit) {
                            map.put("code", 10);
                            map.put("msg", "秒杀商品购买数量超出当日限额！");
                            return map;
                        }
                    } else {
                        map.put("code", 10);
                        map.put("msg", "秒杀商品购买数量超出当日限额！");
                        return map;
                    }
                }
            } else {
                //折扣商品限制
                //判断是折扣商品
                if (StringUtil.isNotEmpty(goods.getIsDiscount()) && "1".equals(goods.getIsDiscount())) {
                    //获取用户折扣订单限制数量
                    int dis = goods.getDislimit();
                    if (StringUtil.isNotEmpty(sku.getDisbuylimit())) {
                        dis = sku.getDisbuylimit();
                    } else {
                        dis = goods.getDislimit();
                    }
                    if (dis != 0) {
                        //判断当前订单数量
                        if (dis >= cart.getNum()) {
                            String nowDay = DateUtil.getTimeByCustomPattern(DateUtil.getCurrentTime(), DateUtil.yyyyMMddHHmmss);
                            String disHql = "from OrderDetailEntity as b where b.goods.id = " + goods.getId() + " and exists (select id from OrderEntity where id = b.sOrder.id and orderState !='-1' and  user.id=" + user.getId() + " and to_days(now()) - to_days(createDate) = 0)";
                            List<OrderDetailEntity> disList = findByQueryString(disHql);
                            int disnum = 0;
                            for (OrderDetailEntity diss : disList) {
                                disnum += diss.getNum();
                            }
                            if (disnum >= dis) {
                                map.put("code", 10);
                                map.put("msg", "折扣商品购买数量超出当日限额！");
                                return map;
                            }
                        } else {
                            map.put("code", 10);
                            map.put("msg", "折扣商品购买数量超出当日限额！");
                            return map;
                        }
                    }
                }
            }

            if (sku != null) {
                sku = get(GoodsSkuEntity.class, sku.getId());
                if (sku.getStockNum() < cart.getNum()) {
                    map.put("code", 1);
                    map.put("msg", "商品" + goods.getName() + "库存不足");
                    return map;
                }
                money += sku.getPrice() * cart.getNum() * basedis;
            } else {
                if (goods.getStockNum() < cart.getNum()) {
                    map.put("code", 1);
                    map.put("msg", "商品" + goods.getName() + "库存不足");
                    return map;
                }
                money += goods.getPrice() * cart.getNum() * basedis;
            }
            goodsNum += cart.getNum();
        }

        OrderEntity order = new OrderEntity();
        order.setCreateDate(DateUtil.getCurrentTime());
        order.setUpdateDate(DateUtil.getCurrentTime());
        order.setStatus("1");
        order.setUser(user);
        order.setOrderState("1");
        order.setIsFendan("0");
        order.setIsRefund("0");
        order.setIsSalesAfter("0");
        order.setBak(bak);
        order.setMoney(money - couponMoney);
        order.setCouponMoney(couponMoney);
        order.setNum(goodsNum);
        order.setOrderNo(EShopUtil.getOrderNo());
        order.setName(addr.getName());
        order.setMobile(addr.getMobile());
        order.setAddress(addr.getPro() + addr.getCity() + addr.getCounty() + addr.getInfo());
        save(order);
        List<OrderDetailEntity> odeList = new ArrayList<>();
        for (CartEntity cart : cartList) {
            GoodsEntity goods = cart.getGoods();
            goods = get(GoodsEntity.class, goods.getId());
            //lengsong
            //价格--判断是否是秒杀&折扣
            //定义折扣
            double basedis = 1;
            if (goods.getSeckillId() != null) {
                String hqls = "from SeckillGoodsPriceSetEntity as a "
                        + " where a.status = '1'"
                        + " and a.goods.id = " + goods.getId();
                List<SeckillGoodsPriceSetEntity> secList = findByQueryString(hqls);
                SeckillGoodsPriceSetEntity sec = secList.get(0);
                basedis = sec.getDiscount() / 10;
            } else {
                if (StringUtil.isNotEmpty(goods.getIsDiscount()) && "1".equals(goods.getIsDiscount())) {
                    basedis = goods.getDiscount() / 10;
                }
            }
            GoodsSkuEntity sku = cart.getSku();
            OrderDetailEntity od = new OrderDetailEntity();
            od.setCreateDate(DateUtil.getCurrentTime());
            od.setUpdateDate(DateUtil.getCurrentTime());
            od.setStatus("1");
            od.setSku(sku);
            if (sku != null) {
                itemmoney = cart.getNum() * sku.getPrice() * basedis;
                od.setMoney(cart.getNum() * sku.getPrice() * basedis);
                od.setPrice(sku.getPrice() * basedis);
                sku.setStockNum(sku.getStockNum() - cart.getNum());
                sku.setSaleNum(sku.getSaleNum() + cart.getNum());
                sku.setIsNew("1");
                saveOrUpdate(sku);
            } else {
                itemmoney = cart.getNum() * goods.getPrice() * basedis;
                od.setMoney(cart.getNum() * goods.getPrice() * basedis);
                od.setPrice(goods.getPrice() * basedis);
            }
            od.setGoods(goods);
            od.setsOrder(order);
            od.setNum(cart.getNum());
            save(od);
            odeList.add(od);

            //品牌满赠&商品满赠
            //得到赠品活动列表
            List<ActionEntity> list = getActionListByGoodsEntity(goods, itemmoney);
            //处理订单和赠品间的关系
            if (list != null && list.size() > 0) {
                int giftnum = 0;
                for (ActionEntity actionEntity : list) {
                    giftnum += 1;
                    String ghql = "from GiftEntity as a where a.status ='1' and a.stockNum > 0 " +
                            "and exists (select 1 from ActionGiftEntity as b where b.gift.id = a.id and b.action.id =" + actionEntity.getId() + ")";
                    List<GiftEntity> giftList = findHql(ghql);
                    for (GiftEntity gf : giftList) {
                        if (gf.getStockNum() > 0) {
                            gf.setStockNum(gf.getStockNum() - 1);
                            gf.setSaleNum(gf.getSaleNum() + 1);
                        }
                        OrderActionEntity orderActionEntity = new OrderActionEntity();
                        orderActionEntity.setGift(gf);
                        orderActionEntity.setAction(actionEntity);
                        orderActionEntity.setOrder(order);
                        orderActionEntity.setCreateDate(DateUtil.getCurrentTime());
                        orderActionEntity.setUpdateDate(DateUtil.getCurrentTime());
                        orderActionEntity.setStatus("1");
                        orderActionEntity.setSaleNum(giftnum);
                        save(orderActionEntity);
                        save(gf);
                    }
                    giftnum = 0;
                }
            }
        }
        deleteAllEntitie(cartList);//删除购物车
        map.put("code", 0);
        map.put("orderId", order.getId());
        return map;
    }

    @Override
    public Map<String, Object> getUserViewGoodsList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        int pageNo = 1;
        String pageNoStr = request.getParameter("pageNo");
        if (StringUtil.isNotEmpty(pageNoStr)) {
            pageNo = Integer.parseInt(pageNoStr);
        }

        int pageSize = 10;
        String pageSizeStr = request.getParameter("pageSize");
        if (StringUtil.isNotEmpty(pageSizeStr)) {
            pageSize = Integer.parseInt(pageSizeStr);
        }
        List<Object> params = new ArrayList<>();
        String userId = request.getParameter("userId");
        String hql = "from GoodsViewEntity as a where a.status = '1' and a.goods.isOnSale='1' and a.user.id=" + userId;
        String cateId = request.getParameter("cateId");
        if (StringUtil.isNotEmpty(cateId)) {
            hql = hql + " and a.cate.id= ? ";
            params.add(Long.valueOf(cateId));
        }
        String isDiscount = request.getParameter("isDiscount");
        if (StringUtil.isNotEmpty(isDiscount)) {
            hql = hql + " and a.isDiscount = ? ";
            params.add(isDiscount);
        }
        String keyword = request.getParameter("keyword");
        if (StringUtil.isNotEmpty(keyword)) {
            hql = hql + " and a.name like ? ";
            params.add("%" + keyword + "%");
        }
        hql = hql + " order by a.createDate desc";

        HqlQuery hqlQuery = new HqlQuery();
        hqlQuery.setCurPage(pageNo);
        hqlQuery.setQueryString(hql);
        hqlQuery.setPageSize(pageSize);
        hqlQuery.setParam(params.toArray());
        PageModel pm = commonDao.getPageList(hqlQuery, true);
        if (pm.getTotalRecords() < 1) {
            map.put("code", 1);//表示没有
            map.put("msg", "暂无数据");
            return map;
        }
        List<GoodsViewEntity> goodsList = (List<GoodsViewEntity>) pm.getList();
        List<Map<String, Object>> list = new ArrayList<>();
        for (GoodsViewEntity gv : goodsList) {
            Map<String, Object> item = new HashMap<>();
            GoodsEntity goods = gv.getGoods();
            item.put("id", goods.getId());
            item.put("imgUrl", goods.getImgUrl());
            item.put("cornerImg", goods.getCornerImg());
            item.put("name", goods.getName());
            item.put("name2", goods.getName2());
            item.put("price", goods.getPrice());
            item.put("oldPrice", goods.getOldPrice());
            item.put("saleNum", goods.getSaleNum());
            item.put("stockNum", goods.getStockNum());
            item.put("cornerImg", goods.getCornerImg());
            item.put("commentNum", goods.getCommentNum());
            if (StringUtil.isNotEmpty(goods.getIsDiscount()) && "1".equals(goods.getIsDiscount())) {
                item.put("price", goods.getPrice() * goods.getDiscount() / 10);
            }
            item.put("isHasSku", "0");
            String tempHql = "from GoodsSkuEntity as a where a.goods.id=" + goods.getId() + " and a.status = '1'";
            List<GoodsSkuEntity> skuList = findByQueryString(tempHql);
            if (skuList != null && !skuList.isEmpty()) {
                item.put("isHasSku", "1");
                GoodsSkuEntity sku = skuList.get(0);
                item.put("price", sku.getPrice());
                if (StringUtil.isNotEmpty(goods.getIsDiscount()) && "1".equals(goods.getIsDiscount())) {
                    item.put("price", sku.getPrice() * goods.getDiscount() / 10);
                }
                item.put("oldPrice", sku.getOldPrice());
                item.put("stockNum", sku.getStockNum());
                if (StringUtil.isNotEmpty(sku.getImgUrl())) {
                    item.put("imgUrl", sku.getImgUrl());
                }
            }
            list.add(item);
        }
        map.put("list", list);
        map.put("code", 0);
        map.put("totalPage", pm.getTotalPage());
        map.put("currPage", pm.getCurrPage());
        return map;
    }

    @Override
    public Map<String, Object> getUserCollectGoodsList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        int pageNo = 1;
        String pageNoStr = request.getParameter("pageNo");
        if (StringUtil.isNotEmpty(pageNoStr)) {
            pageNo = Integer.parseInt(pageNoStr);
        }

        int pageSize = 10;
        String pageSizeStr = request.getParameter("pageSize");
        if (StringUtil.isNotEmpty(pageSizeStr)) {
            pageSize = Integer.parseInt(pageSizeStr);
        }
        List<Object> params = new ArrayList<>();
        String userId = request.getParameter("userId");
        String hql = "from GoodsCollectEntity as a where a.status = '1' and a.goods.isOnSale='1' and a.user.id=" + userId;
        hql = hql + " order by a.createDate desc";

        HqlQuery hqlQuery = new HqlQuery();
        hqlQuery.setCurPage(pageNo);
        hqlQuery.setQueryString(hql);
        hqlQuery.setPageSize(pageSize);
        hqlQuery.setParam(params.toArray());
        PageModel pm = commonDao.getPageList(hqlQuery, true);
        if (pm.getTotalRecords() < 1 && StringUtil.isNotEmpty(pm.getTotalRecords())) {
            map.put("code", 1);//表示没有
            map.put("msg", "暂无数据");
            return map;
        }
        List<GoodsCollectEntity> goodsList = (List<GoodsCollectEntity>) pm.getList();
        List<Map<String, Object>> list = new ArrayList<>();
        for (GoodsCollectEntity gv : goodsList) {
            Map<String, Object> item = new HashMap<>();
            GoodsEntity goods = gv.getGoods();
            item.put("id", goods.getId());
            item.put("imgUrl", goods.getImgUrl());
            item.put("cornerImg", goods.getCornerImg());
            item.put("name", goods.getName());
            item.put("name2", goods.getName2());
            item.put("price", goods.getPrice());
            item.put("oldPrice", goods.getOldPrice());
            item.put("saleNum", goods.getSaleNum());
            item.put("stockNum", goods.getStockNum());
            item.put("cornerImg", goods.getCornerImg());
            item.put("commentNum", goods.getCommentNum());
            if (StringUtil.isNotEmpty(goods.getIsDiscount()) && "1".equals(goods.getIsDiscount())) {
                item.put("price", goods.getPrice() * goods.getDiscount() / 10);
            }
            item.put("isHasSku", "0");
            String tempHql = "from GoodsSkuEntity as a where a.goods.id=" + goods.getId() + " and a.status = '1'";
            List<GoodsSkuEntity> skuList = findByQueryString(tempHql);
            if (skuList != null && !skuList.isEmpty()) {
                item.put("isHasSku", "1");
                GoodsSkuEntity sku = skuList.get(0);
                item.put("price", sku.getPrice());
                if (StringUtil.isNotEmpty(goods.getIsDiscount()) && "1".equals(goods.getIsDiscount())) {
                    item.put("price", sku.getPrice() * goods.getDiscount() / 10);
                }
                item.put("oldPrice", sku.getOldPrice());
                item.put("stockNum", sku.getStockNum());
                if (StringUtil.isNotEmpty(sku.getImgUrl())) {
                    item.put("imgUrl", sku.getImgUrl());
                }
            }
            list.add(item);
        }

        map.put("list", list);
        map.put("code", 0);
        map.put("totalRecords", pm.getTotalRecords());
        map.put("totalPage", pm.getTotalPage());
        map.put("currPage", pm.getCurrPage());
        return map;
    }

    @Override
    public Map<String, Object> getOrderListyeji(HttpServletRequest request) {
        java.text.DecimalFormat df = new java.text.DecimalFormat("##0.00");
        Map<String, Object> map = new HashMap<String, Object>();
        String userId = request.getParameter("userId");
        if (StringUtil.isEmpty(userId)) {
            userId = request.getParameter("tuijianUserId");
        }
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        if (user == null) {
            map.put("code", 10);
            return map;
        }
        String pageNo = request.getParameter("pageNo");
        String pageSizeStr = request.getParameter("pageSize");
        int pageSize = 200;
        /*
        if (StringUtil.isNotEmpty(pageSizeStr)) {
            pageSize = Integer.parseInt(pageSizeStr);
        }
        */
        int curPage = 1;
        if (StringUtil.isNotEmpty(pageNo)) {
            curPage = Integer.parseInt(pageNo);
        }
        //String hql = "from OrderEntity as a where a.status = '1' ";

        String shql = "from OrderEntity as a where a.status = '1' and a.user.id in " +
                "(select b.id from WUserEntity as b where b.id in " +
                "(select c.id from WUserEntity as c where c.parentUser.id= " + userId + " ))";
        String hql = "from OrderEntity as a where a.status = '1' and a.orderState in ('1','2','3','4') and exists " +
                "(from WUserEntity as b where a.user.id = b.id and  b.id in " +
                "(select c.id from WUserEntity as c where c.parentUser.id= " + userId + " ))";

        /*
        if (StringUtil.isNotEmpty(userId)) {
            hql = hql + " and a.user.id=" + userId;
        }

        String tuijianUserId = request.getParameter("tuijianUserId");
        if (StringUtil.isNotEmpty(tuijianUserId)) {
            hql = hql + " and a.user.parentUser.id=" + tuijianUserId;
        }
        */
        String queryType = request.getParameter("queryType");
        if (StringUtil.isNotEmpty(queryType)) {
            if (queryType.equals("month")) {
                String nowDay = DateUtil.getTimeByCustomPattern(DateUtil.getCurrentTime(), "yyyy-MM");
                hql = hql + " and DATE_FORMAT(a.createDate,'%Y-%m') = '" + nowDay + "'";
            } else if ("day".equals(queryType)) {
                String nowDay = DateUtil.getTimeByCustomPattern(DateUtil.getCurrentTime(), DateUtil.yyyyMMdd);
                hql = hql + " and DATE_FORMAT(a.createDate,'%Y-%m-%d') = '" + nowDay + "'";
            }

        }
        List<Object> params = new ArrayList<>();
        String state = request.getParameter("state");
        if (!"-1".equals(state)) {
            hql += " and a.orderState = ? ";
            params.add(state);
        }
        String isSalesAfter = request.getParameter("isSalesAfter");
        if (StringUtil.isNotEmpty(isSalesAfter)) {
            hql += " and a.isSalesAfter = ? ";
            params.add(isSalesAfter);
        }
        hql += " order by a.createDate desc";
        HqlQuery hqlQuery = new HqlQuery();
        hqlQuery.setCurPage(curPage);
        hqlQuery.setPageSize(pageSize);
        hqlQuery.setQueryString(hql);
        hqlQuery.setParam(params.toArray());
        PageModel pm = commonDao.getPageList(hqlQuery, true);
        if (pm.getTotalRecords() < 1 && StringUtil.isNotEmpty(pm.getTotalRecords())) {
            map.put("code", 1);
            map.put("msg", "暂无数据");
            return map;
        }
        List<OrderEntity> orderList = (List<OrderEntity>) pm.getList();
        List<Map<String, Object>> list = new ArrayList<>();
        Double allMoney = 0D;
        for (OrderEntity order : orderList) {
            Map<String, Object> item = new HashMap<>();
            allMoney = allMoney + order.getMoney() + order.getCouponMoney();
            item.put("id", order.getId());
            item.put("time", DateUtil.getTimeByCustomPattern(order.getCreateDate(), DateUtil.yyyyMMddHHmmss));
            item.put("num", order.getNum());
            item.put("money", df.format(order.getMoney()));
            item.put("orderState", order.getOrderState());
            item.put("isRefund", order.getIsRefund());
            item.put("isSalesAfter", order.getIsSalesAfter());
            item.put("username", order.getUser().getName());
            item.put("usertell", order.getUser().getMobile());
            //获得订单的赠品
            String gift = getGiftByOrderId(order.getId());
            item.put("gift", StringUtil.isEmpty(gift) ? "无" : gift);
            String tempHql = "from OrderDetailEntity as a where a.sOrder.id=" + order.getId();
            List<OrderDetailEntity> odList = findByQueryString(tempHql);
            List<Map<String, Object>> childList = new ArrayList<>();
            for (OrderDetailEntity od : odList) {
                Map<String, Object> childMap = new HashMap<>();
                childMap.put("id", od.getId());
                childMap.put("name", od.getGoods().getName());
                childMap.put("imgUrl", od.getGoods().getImgUrl());
                childMap.put("price", df.format(od.getPrice()));
                childMap.put("sku", "");
                childMap.put("isHasSku", "0");
                if (od.getSku() != null) {
                    childMap.put("imgUrl", od.getSku().getImgUrl());
                    childMap.put("price", df.format(od.getPrice()));
                    childMap.put("parmasValuesJson", od.getSku().getParmasValuesJson());
                    childMap.put("isHasSku", "1");
                }
                childMap.put("num", od.getNum());
                childList.add(childMap);
            }
            item.put("childList", childList);
            list.add(item);
        }
        int num = orderList.size();
        map.put("allNum", num);
        map.put("allMoney", df.format(allMoney));
        map.put("code", 0);
        map.put("list", list);
        map.put("totalPage", pm.getTotalPage());
        map.put("currPage", pm.getCurrPage());
        return map;
    }

    @Override
    public Map<String, Object> getOrderList(HttpServletRequest request) {
        java.text.DecimalFormat df = new java.text.DecimalFormat("##0.00");
        Map<String, Object> map = new HashMap<String, Object>();
        String userId = request.getParameter("userId");
        if (StringUtil.isEmpty(userId)) {
            userId = request.getParameter("tuijianUserId");
        }
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        if (user == null) {
            map.put("code", 10);
            return map;
        }
        String pageNo = request.getParameter("pageNo");
        String pageSizeStr = request.getParameter("pageSize");
        int pageSize = 10;
        if (StringUtil.isNotEmpty(pageSizeStr)) {
            pageSize = Integer.parseInt(pageSizeStr);
        }
        int curPage = 1;
        if (StringUtil.isNotEmpty(pageNo)) {
            curPage = Integer.parseInt(pageNo);
        }
        //String hql = "from OrderEntity as a where a.status = '1' ";
        /*
        String hql = "from OrderEntity as a where a.status = '1' and a.user.id in " +
                "(select b.id from WUserEntity as b where b.id in " +
                "(select c.id from WUserEntity as c where c.parentUser.id= "+ userId +" ))";
         */
        String hql = "from OrderEntity as a where a.status = '1' and a.user.id =" + userId;
        /*
        if (StringUtil.isNotEmpty(userId)) {
            hql = hql + " and a.user.id=" + userId;
        }

        String tuijianUserId = request.getParameter("tuijianUserId");
        if (StringUtil.isNotEmpty(tuijianUserId)) {
            hql = hql + " and a.user.parentUser.id=" + tuijianUserId;
        }
        */
        String queryType = request.getParameter("queryType");
        if (StringUtil.isNotEmpty(queryType)) {
            if (queryType.equals("month")) {
                String nowDay = DateUtil.getTimeByCustomPattern(DateUtil.getCurrentTime(), "yyyy-MM");
                hql = hql + " and DATE_FORMAT(a.createDate,'%Y-%m') = '" + nowDay + "'";
            } else if ("day".equals(queryType)) {
                String nowDay = DateUtil.getTimeByCustomPattern(DateUtil.getCurrentTime(), DateUtil.yyyyMMdd);
                hql = hql + " and DATE_FORMAT(a.createDate,'%Y-%m-%d') = '" + nowDay + "'";
            }

        }
        List<Object> params = new ArrayList<>();
        String state = request.getParameter("state");
        if (!"-1".equals(state)) {
            hql += " and a.orderState = ? ";
            params.add(state);
        }
        String isSalesAfter = request.getParameter("isSalesAfter");
        if (StringUtil.isNotEmpty(isSalesAfter)) {
            hql += " and a.isSalesAfter = ? ";
            params.add(isSalesAfter);
        }
        hql += " order by a.createDate desc";
        HqlQuery hqlQuery = new HqlQuery();
        hqlQuery.setCurPage(curPage);
        hqlQuery.setPageSize(pageSize);
        hqlQuery.setQueryString(hql);
        hqlQuery.setParam(params.toArray());
        PageModel pm = commonDao.getPageList(hqlQuery, true);
        if (pm.getTotalRecords() < 1 && StringUtil.isNotEmpty(pm.getTotalRecords())) {
            map.put("totalPage", pm.getTotalPage());
            map.put("code", 1);
            map.put("msg", "暂无数据");
            return map;
        }
        List<OrderEntity> orderList = (List<OrderEntity>) pm.getList();
        List<Map<String, Object>> list = new ArrayList<>();
        Double allMoney = 0D;
        for (OrderEntity order : orderList) {
            Map<String, Object> item = new HashMap<>();
            allMoney = allMoney + order.getMoney() + order.getCouponMoney();
            item.put("id", order.getId());
            item.put("time", DateUtil.getTimeByCustomPattern(order.getCreateDate(), DateUtil.yyyyMMddHHmmss));
            item.put("num", order.getNum());
            item.put("money", df.format(order.getMoney()));
            item.put("orderState", order.getOrderState());
            item.put("isRefund", order.getIsRefund());
            item.put("isSalesAfter", order.getIsSalesAfter());
            //获得订单的赠品
            String gift = getGiftByOrderId(order.getId());
            item.put("gift", StringUtil.isEmpty(gift) ? "无" : gift);
            String tempHql = "from OrderDetailEntity as a where a.sOrder.id=" + order.getId();
            List<OrderDetailEntity> odList = findByQueryString(tempHql);
            List<Map<String, Object>> childList = new ArrayList<>();
            for (OrderDetailEntity od : odList) {
                Map<String, Object> childMap = new HashMap<>();
                childMap.put("id", od.getId());
                childMap.put("name", od.getGoods().getName());
                childMap.put("imgUrl", od.getGoods().getImgUrl());
                childMap.put("price", df.format(od.getPrice()));
                childMap.put("sku", "");
                childMap.put("isHasSku", "0");
                if (od.getSku() != null) {
                    childMap.put("imgUrl", od.getSku().getImgUrl());
                    childMap.put("price", df.format(od.getPrice()));
                    childMap.put("parmasValuesJson", od.getSku().getParmasValuesJson());
                    childMap.put("isHasSku", "1");
                }
                childMap.put("num", od.getNum());
                childList.add(childMap);
            }
            item.put("childList", childList);
            list.add(item);
        }
        int num = orderList.size();
        map.put("allNum", num);
        map.put("allMoney", df.format(allMoney));
        map.put("code", 0);
        map.put("list", list);
        map.put("totalPage", pm.getTotalPage());
        map.put("currPage", pm.getCurrPage());
        return map;
    }

    @Override
    public Map<String, Object> getOrderDetail(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        if (user == null) {
            map.put("code", 10);
            return map;
        }
        String orderId = request.getParameter("orderId");
        OrderEntity order = get(OrderEntity.class, Long.valueOf(orderId));
        map.put("isSalesAfter", order.getIsSalesAfter());
        if ("1".equals(order.getIsSalesAfter())) {
            map.put("salesAfterReason", order.getSalesAfterReason());
            map.put("salesAfterInfo", order.getSalesAfterInfo());
            map.put("salesAfterImgList", order.getSalesAfterImgList());
            map.put("salesAfterState", order.getSalesAfterState());
            map.put("applySalesAfterTime", DateUtil.getTimeByCustomPattern(order.getApplySalesAfterTime(), DateUtil.yyyyMMddHHmmss));
            map.put("checkSalesAfterTime", "无");
            map.put("teamHandleTime", "");
            map.put("salesAfterHandleBak", order.getSalesAfterHandleBak());
        }


        if (order.getDriver() != null) {
            map.put("driverName", order.getDriver().getName());
            map.put("driverMobile", order.getDriver().getMobile());
        }

        map.put("nick", order.getUser().getNick());
        map.put("headUrl", order.getUser().getHeadUrl());
        map.put("num", order.getNum());
        map.put("money", order.getMoney());
        map.put("bak", order.getBak());
        map.put("orderState", order.getOrderState());
        map.put("orderNo", order.getOrderNo());
        map.put("cancelReason", order.getCancelReason());
        map.put("address", order.getName() + order.getMobile() + order.getAddress());
        map.put("createDate", DateUtil.getTimeByCustomPattern(order.getCreateDate(), DateUtil.yyyyMMddHHmmss));
        map.put("cancleTime", "");
        if (order.getCancleTime() != null) {
            map.put("cancleTime", DateUtil.getTimeByCustomPattern(order.getCancleTime(), DateUtil.yyyyMMddHHmmss));
        }

        String tempHql = "from OrderDetailEntity as a where a.sOrder.id=" + order.getId();
        List<OrderDetailEntity> odList = findByQueryString(tempHql);
        List<Map<String, Object>> childList = new ArrayList<>();
        for (OrderDetailEntity od : odList) {
            Map<String, Object> childMap = new HashMap<>();
            childMap.put("id", od.getId());
            childMap.put("name", od.getGoods().getName());
            childMap.put("imgUrl", od.getGoods().getImgUrl());
            childMap.put("price", od.getPrice());
            childMap.put("sku", "");
            childMap.put("isHasSku", "0");
            if (od.getSku() != null) {
                childMap.put("imgUrl", od.getSku().getImgUrl());
                childMap.put("price", od.getPrice());
                childMap.put("parmasValuesJson", od.getSku().getParmasValuesJson());
                childMap.put("isHasSku", "1");
            }
            childMap.put("num", od.getNum());
            childMap.put("isRefund", "0");
            if (StringUtil.isNotEmpty(od.getIsRefund()) && "1".equals(od.getIsRefund())) {
                childMap.put("refundMoney", od.getRefoundMoney());
                childMap.put("refundTime", DateUtil.getTimeByCustomPattern(od.getRefundTime(), DateUtil.yyyyMMddHHmmss));
            }
            childList.add(childMap);
        }

        //得到赠品活动列表
        List<ActionEntity> list = getOldActionListByOrderDetail(odList);
        //处理订单和赠品间的关系
        if (list != null && list.size() > 0) {
            //String hqlg = "select id,order.id,actionId,gift,saleNum,COUNT(a.saleNum) as allnum from OrderActionEntity as a where a.status ='1' and a.order.id =" + orderId +"  group by a.gift.id";
            String sqlg = "select id,orderId,giftId,COUNT(saleNum) as allnum from shop_order_action where orderId = " + orderId +" group by giftId";
            List<Object[]> giftList = commonDao.findListbySql(sqlg);
            //List<Object[]> giftList = commonDao.findListbySql(sqlg);
            if (giftList != null && giftList.size() > 0) {
                List<Map<String, Object>> glist = new ArrayList<>();
                for (Object[] gf : giftList) {
                    BigInteger id = (BigInteger) (gf[0]);
                    BigInteger orderid = (BigInteger) gf[1];
                    BigInteger giftid = (BigInteger) gf[2];
                    BigInteger saleNum = (BigInteger) (gf[3]);
                    GiftEntity giftlists = get(GiftEntity.class, giftid.longValue());
                    Map<String, Object> item = new HashMap<>();
                    item.put("id", id);
                    item.put("imgUrl", giftlists.getImgUrl());
                    item.put("gname", giftlists.getName());
                    item.put("price", giftlists.getMoney());
                    item.put("saleNum", saleNum);
                    item.put("stockNum", giftlists.getStockNum());
                    glist.add(item);
                }
                map.put("giftList", glist);
            } else {
                map.put("gcode", '1');
            }
        }
        map.put("childList", childList);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> saveCancleOrder(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        if (user == null) {
            map.put("code", 10);
            return map;
        }
        String orderId = request.getParameter("orderId");
        OrderEntity order = get(OrderEntity.class, Long.valueOf(orderId));
        order.setOrderState("-1");
        order.setCancleTime(DateUtil.getCurrentTime());
        order.setCancelReason("未付款，用户手工取消");
        saveOrUpdate(order);
        String hql = "from OrderDetailEntity as a where a.sOrder.id=" + orderId;
        List<OrderDetailEntity> odList = findByQueryString(hql);
        for (OrderDetailEntity od : odList) {
            GoodsSkuEntity sku = od.getSku();
            GoodsEntity goods = od.getGoods();
            goods = get(GoodsEntity.class, goods.getId());
            if (sku != null) {
                sku = get(GoodsSkuEntity.class, sku.getId());
                sku.setStockNum(sku.getStockNum() + od.getNum());
                save(sku);
            }
            goods.setStockNum(goods.getStockNum() + od.getNum());
            save(goods);
        }
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> saveReceiveOrder(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        if (user == null) {
            map.put("code", 10);
            return map;
        }
        String orderId = request.getParameter("orderId");
        OrderEntity order = get(OrderEntity.class, Long.valueOf(orderId));
        order.setOrderState("3");
        order.setReceiveTime(DateUtil.getCurrentTime());
        saveOrUpdate(order);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> saveRefoundOrder(HttpServletRequest request) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void initCommentOrder(HttpServletRequest request) {
        String id = request.getParameter("orderId");
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        request.setAttribute("userId", userId);
        OrderEntity o = get(OrderEntity.class, Long.valueOf(id));
        String hql = "from OrderDetailEntity as a where a.sOrder.id=" + id;
        List<OrderDetailEntity> dList = findByQueryString(hql);
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (OrderDetailEntity d : dList) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("detail", d);
            if (StringUtil.isNotEmpty(d.getIsPost()) && d.getIsPost().equals("1")) {
                CommentEntity com = get(CommentEntity.class, d.getPostId());
                map.put("post", com);
            }
            mapList.add(map);
        }
        request.setAttribute("a", o);
        request.setAttribute("mapList", mapList);

    }

    @Override
    public Map<String, Object> saveOrderComment(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
		/*
		 * comType : 1,
				msgContent : textarea,
				otherId : goodsId,
				commentScore : commentScore,
				imgList : imgList,
				detailId : detailId,
				orderId : orderId
		 */
        String userId = request.getParameter("userId");
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        if (user == null) {
            map.put("code", 10);
            return map;
        }
        String orderId = request.getParameter("orderId");
        String detailId = request.getParameter("detailId");
        String comType = request.getParameter("comType");
        String msgContent = request.getParameter("msgContent");
        String otherId = request.getParameter("otherId");
        String commentScore = request.getParameter("commentScore");
        String imgList = request.getParameter("imgList");
        OrderEntity o = get(OrderEntity.class, Long.valueOf(orderId));
        OrderDetailEntity d = get(OrderDetailEntity.class, Long.valueOf(detailId));
        if (d.getPostId() != null) {
            map.put("code", 1);
            map.put("msg", "已经晒单，无法重复提交");
            return map;
        }
        int score = Integer.parseInt(commentScore);
        CommentEntity c = new CommentEntity();
        c.setSku(d.getSku().getParmasValuesJson());
        c.setCreateDate(DateUtil.getCurrentTime());
        c.setUpdateDate(DateUtil.getCurrentTime());
        c.setStatus("1");
        c.setComType(comType);
        c.setCommentScore(score);
        c.setImgList(imgList);
        c.setMsgContent(msgContent);
        c.setMsgTime(DateUtil.getTimeByCustomPattern(DateUtil.getCurrentTime(), DateUtil.yyyyMMddHHmmss));
        c.setOtherId(Long.valueOf(otherId));
        c.setRaiseNum(0);
        c.setReplyNum(0);
        c.setUser(user);
        c.setGoods(d.getSku().getGoods());
        save(c);

        d.setIsPost("1");
        d.setPostId(c.getId());
        d.setPostTime(DateUtil.getCurrentTime());
        saveOrUpdate(d);

        String hql = "from OrderDetailEntity as a where a.sorder.id=" + orderId;
        List<OrderDetailEntity> dList = findByQueryString(hql);
        boolean isAll = true;
        for (OrderDetailEntity od : dList) {
            if (StringUtil.isEmpty(od.getIsPost()) || "0".equals(od.getIsPost())) {
                isAll = false;
            }
        }
        if (isAll) {
            o.setOrderState("4");
            saveOrUpdate(o);
        }


        GoodsEntity g = d.getSku().getGoods();
        g.setCommentNum(g.getCommentNum());
        /*
         * 处理商品的好评度
         */
        if (g.getNiceCommentNum() == null)
            g.setNiceCommentNum(0);
        if (score > 3) {
            g.setNiceCommentNum(g.getNiceCommentNum() + 1);
        }
        Double niceComment = (double) (g.getNiceCommentNum() / g.getCommentNum());
        g.setNiceComment(niceComment);
        saveOrUpdate(g);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> loadCommentById(HttpServletRequest request) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<String, Object> addCommetPic(HttpServletRequest request) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<String, Object> getCateList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String cateId = request.getParameter("id");
        String hql = "from CateEntity as a where a.status = '1' and a.parentCate is null order by a.orderNum desc";
        if (StringUtil.isNotEmpty(cateId)) {
            hql = "from CateEntity as a where a.status = '1' and a.parentCate.id=" + cateId + " order by a.orderNum desc";
        }
        List<CateEntity> cateList = findByQueryString(hql);
        List<Map<String, Object>> list = new ArrayList<>();
        for (CateEntity cate : cateList) {
            Map<String, Object> item = new HashMap<>();
            item.put("id", cate.getId());
            item.put("name", cate.getName());
            item.put("imgUrl", cate.getImgUrl());
            list.add(item);
        }
        map.put("code", 0);
        map.put("list", list);
        return map;
    }

    @Override
    public Map<String, Object> saveCancleGoodsCollect(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        String userId = request.getParameter("userId");
        if (StringUtil.isEmpty(userId)) {
            map.put("code", 10);
            return map;
        }
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        if (user == null) {
            map.put("code", 10);
            return map;
        }
        String id = request.getParameter("id");
        String hql = "from GoodsCollectEntity as a where a.user.id=" + user.getId() + " and a.goods.id=" + id;
        List<GoodsCollectEntity> gcList = findByQueryString(hql);
        if (gcList == null || gcList.isEmpty()) {
            map.put("code", 1);
            return map;
        }
        GoodsCollectEntity gc = gcList.get(0);
        delete(gc);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> saveCancleAllGoods(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        String userId = request.getParameter("userId");
        if (StringUtil.isEmpty(userId)) {
            map.put("code", 10);
            return map;
        }
        WUserEntity user = get(WUserEntity.class, Long.valueOf(userId));
        if (user == null) {
            map.put("code", 10);
            return map;
        }
        String hql = "from GoodsCollectEntity as a where a.user.id=" + user.getId();
        List<GoodsCollectEntity> gcList = findByQueryString(hql);
        deleteAllEntitie(gcList);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> getUserTuijianList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        int pageNo = 1;
        String pageNoStr = request.getParameter("pageNo");
        if (StringUtil.isNotEmpty(pageNoStr)) {
            pageNo = Integer.parseInt(pageNoStr);
        }

        int pageSize = 10;
        String pageSizeStr = request.getParameter("pageSize");
        if (StringUtil.isNotEmpty(pageSizeStr)) {
            pageSize = Integer.parseInt(pageSizeStr);
        }
        List<Object> params = new ArrayList<>();
        String userId = request.getParameter("userId");
        String hql = "from WUserEntity as a where a.status = '1' and a.parentUser.id=" + userId;
        String queryType = request.getParameter("queryType");
        if (StringUtil.isNotEmpty(queryType)) {
            if (queryType.equals("month")) {
                String nowDay = DateUtil.getTimeByCustomPattern(DateUtil.getCurrentTime(), "yyyy-MM");
                hql = hql + " and DATE_FORMAT(a.createDate,'%Y-%m') = '" + nowDay + "'";
            } else if ("day".equals(queryType)) {
                String nowDay = DateUtil.getTimeByCustomPattern(DateUtil.getCurrentTime(), DateUtil.yyyyMMdd);
                hql = hql + " and DATE_FORMAT(a.createDate,'%Y-%m-%d') = '" + nowDay + "'";
            }

        }
        hql = hql + " order by a.createDate desc";

        HqlQuery hqlQuery = new HqlQuery();
        hqlQuery.setCurPage(pageNo);
        hqlQuery.setQueryString(hql);
        hqlQuery.setPageSize(pageSize);
        hqlQuery.setParam(params.toArray());
        PageModel pm = commonDao.getPageList(hqlQuery, true);
        if (pm.getTotalRecords() < 1 && StringUtil.isNotEmpty(pm.getTotalRecords())) {
            map.put("code", 1);//表示没有
            map.put("msg", "暂无数据");
            return map;
        }
        List<WUserEntity> userList = (List<WUserEntity>) pm.getList();
        List<Map<String, Object>> list = new ArrayList<>();
        for (WUserEntity gv : userList) {
            Map<String, Object> item = new HashMap<>();
            item.put("name", gv.getName());
            item.put("mobile", gv.getMobile());
            item.put("nick", gv.getNick());
            item.put("headUrl", gv.getHeadUrl());
            item.put("isAuth", gv.getIsAuth());
            item.put("address", gv.getAddress());
            list.add(item);
        }
        map.put("list", list);
        map.put("code", 0);
        map.put("totalPage", pm.getTotalPage());
        map.put("currPage", pm.getCurrPage());
        return map;
    }

    @Override
    public Map<String, Object> getNowBrandGiftActionGoodsList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        int pageNo = 1;
        String pageNoStr = request.getParameter("pageNo");
        if (StringUtil.isNotEmpty(pageNoStr)) {
            pageNo = Integer.parseInt(pageNoStr);
        }

        int pageSize = 10;
        String pageSizeStr = request.getParameter("pageSize");
        if (StringUtil.isNotEmpty(pageSizeStr)) {
            pageSize = Integer.parseInt(pageSizeStr);
        }

        String userId = request.getParameter("userId");
        WUserEntity user = null;
        boolean isAuth = false;
        if (StringUtil.isNotEmpty(userId)) {
            user = get(WUserEntity.class, Long.valueOf(userId));
            if (user.getIsAuth().equals("1"))
                isAuth = true;
        }

        Date now = DateUtil.getCurrentTime();

        //获取当前全部的品牌满赠活动
        String hqls = "from ActionEntity as a where a.status='1' and a.actionType='1' and a.beginTime <= ? and a.endTime >= ? ";
        List<ActionEntity> actionList = findHql(hqls, new Object[]{now, now});


        //获取当前全部的品牌满赠活动的商品
        Date nows = DateUtil.getCurrentTime();
        List<Object> params = new ArrayList<>();
        /*
        String hql = "from GoodsEntity as c where c.status = '1' " +
                "and exists(select 1 from ActionBrandEntity as b where b.status = '1' and c.brand.id = b.brand.id " +
                "and b.action.id in (select a.id from ActionEntity as a where a.status ='1' and a.actionType = '1' " +
                "and a.beginTime <= now() and a.endTime >= now() )) order by c.sort desc";

         */

        String ssql = "SELECT " +
                " a.id," +
                " a.imgUrl," +
                " a.cornerImg," +
                " a.name," +
                " a.name2," +
                " a.price," +
                " a.oldPrice," +
                " a.saleNum," +
                " a.stockNum," +
                " a.commentNum," +
                " c.conditionMoney" +
                " FROM " +
                " shop_goods a " +
                " LEFT JOIN shop_action_brand b ON a.brandId = b.brandId" +
                " LEFT JOIN shop_action c ON c.id = b.actionId " +
                " WHERE " +
                " a.status = '1' and b.status = '1' and c.status ='1' and c.actionType = '1' " +
                " AND c.beginTime <= now() AND c.endTime >= now()";

        /*
        String hql = "from GoodsEntity as c where c.status='1' and exists (select 1 from ActionBrandEntity as a where a.brand.id=c.brand.id and a.action.id in"
                + " (select b.id from ActionEntity as b where b.status='1' and b.actionType='1' and b.beginTime <= ? and b.endTime >= ? ))";
        */
        List<Object[]> goodsListss = commonDao.findListbySql(ssql);
        int totalPage = goodsListss.size() / pageSize;
        List<Object[]> goodsLists = commonDao.getListPageBySql(ssql,pageNo,pageSize);
        if (goodsLists.size() < 1) {
            map.put("code", 1);//表示没有
            map.put("msg", "暂无数据");
            return map;
        }

        List<Map<String, Object>> lists = new ArrayList<>();
        for (Object[] goods : goodsLists) {
            Map<String, Object> item = new HashMap<>();
            item.put("id", goods[0]);
            item.put("imgUrl", goods[1]);
            item.put("cornerImg", goods[2]);
            item.put("name", goods[3]);
            item.put("name2", goods[4]);
            item.put("price", goods[5]);
            if (!isAuth) {
                item.put("price", goods[5]);
            }
            item.put("oldPrice", goods[6]);
            item.put("saleNum", goods[7]);
            item.put("stockNum", goods[8]);
            item.put("commentNum",  goods[9]);
            item.put("conditionMoney",  goods[10]);
            item.put("isHasSku", "0");
            String tempHql = "from GoodsSkuEntity as a where a.goods.id=" + goods[0] + " and a.status = '1'";
            List<GoodsSkuEntity> skuList = findByQueryString(tempHql);
            if (skuList != null && !skuList.isEmpty()) {
                item.put("isHasSku", "1");
                GoodsSkuEntity sku = skuList.get(0);
                item.put("price", sku.getPrice());
                item.put("oldPrice", sku.getOldPrice());
                item.put("stockNum", sku.getStockNum());
                if (StringUtil.isNotEmpty(sku.getImgUrl())) {
                    item.put("imgUrl", sku.getImgUrl());
                }
            }
            lists.add(item);
        }
/*
        HqlQuery hqlQuery = new HqlQuery();
        hqlQuery.setCurPage(pageNo);
        hqlQuery.setQueryString(ssql);
        hqlQuery.setPageSize(pageSize);
        hqlQuery.setParam(params.toArray());
        PageModel pm = commonDao.getPageList(hqlQuery, true);
        if (pm.getTotalRecords() < 1) {
            map.put("code", 1);//表示没有
            map.put("msg", "暂无数据");
            return map;
        }
        List<GoodsEntity> goodsList = (List<GoodsEntity>) pm.getList();
        //List<GoodsEntity> goodsList = commonDao.getListByPage(hql, 1, 10, new Object[]{nows, nows});
        //List<GoodsEntity> goodsList = findHql(hql, new Object[]{nows, nows});
        List<Map<String, Object>> list = new ArrayList<>();


        for (GoodsEntity goods : goodsList) {
            Map<String, Object> item = new HashMap<>();
            item.put("id", goods.getId());
            item.put("imgUrl", goods.getImgUrl());
            item.put("cornerImg", goods.getCornerImg());
            item.put("name", goods.getName());
            item.put("name2", goods.getName2());
            item.put("price", goods.getPrice());
            if (!isAuth) {
                item.put("price", goods.getPrice());
            }
            item.put("oldPrice", goods.getOldPrice());
            item.put("saleNum", goods.getSaleNum());
            item.put("stockNum", goods.getStockNum());
            item.put("commentNum", goods.getCommentNum());
            item.put("cornerImg", goods.getCornerImg());
            item.put("isHasSku", "0");
            String tempHql = "from GoodsSkuEntity as a where a.goods.id=" + goods.getId() + " and a.status = '1'";
            List<GoodsSkuEntity> skuList = findByQueryString(tempHql);
            if (skuList != null && !skuList.isEmpty()) {
                item.put("isHasSku", "1");
                GoodsSkuEntity sku = skuList.get(0);
                item.put("price", sku.getPrice());
                item.put("oldPrice", sku.getOldPrice());
                item.put("stockNum", sku.getStockNum());
                if (StringUtil.isNotEmpty(sku.getImgUrl())) {
                    item.put("imgUrl", sku.getImgUrl());
                }
            }
            list.add(item);
        }
         */

        //获取当前全部的品牌满赠的赠品
        String hqlg = "from GiftEntity as a where a.status ='1' " +
                "and exists (select 1 from ActionGiftEntity as b where b.gift.id = a.id " +
                "and exists (select 1 from ActionEntity as c where c.status = '1' and c.actionType = '1' and b.action.id =c.id " +
                "and c.beginTime <= ? and c.endTime >= ? ))";
        List<GiftEntity> giftList = findHql(hqlg, new Object[]{nows, nows});
        //List<GiftEntity> giftList = findHql(hqlg, new Object[] {DateUtil.getCurrentTime(),DateUtil.getCurrentTime()});
        List<Map<String, Object>> glist = new ArrayList<>();
        for (GiftEntity gf : giftList) {
            Map<String, Object> item = new HashMap<>();
            item.put("id", gf.getId());
            item.put("imgUrl", gf.getImgUrl());
            item.put("gname", gf.getName());
            item.put("price", gf.getMoney());
            item.put("saleNum", gf.getSaleNum());
            item.put("stockNum", gf.getStockNum());
            glist.add(item);
        }

        map.put("code", 0);
        map.put("goodsList", lists);
        map.put("giftList", glist);
        int page = map.size();
        map.put("totalPage", totalPage);
        map.put("currPage", pageNo);
        return map;
    }

    @Override
    public Map<String, Object> getNowGoodsGiftActionGoodsList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
		/*String hql = "from ActionEntity as a where a.status='1' and a.actionType='1' and a.beginTime <= ? and a.endTime >= ? ";
		//获取当前全部的品牌满赠活动
		List<ActionEntity> actionList = findHql(hql, new Object[] {DateUtil.getCurrentTime(),DateUtil.getCurrentTime()});*/
        String userId = request.getParameter("userId");
        WUserEntity user = null;
        boolean isAuth = false;
        if (StringUtil.isNotEmpty(userId)) {
            user = get(WUserEntity.class, Long.valueOf(userId));
            if (user.getIsAuth().equals("1"))
                isAuth = true;
        }
        String sql = "SELECT " +
                " a.id," +
                " a.imgUrl," +
                " a.name," +
                " a.price," +
                " a.commentNum," +
                " c.giftType," +
                " c.giftNum," +
                " c.conditionMoney" +
                " FROM " +
                " shop_goods a " +
                " LEFT JOIN shop_action_goods b ON a.id = b.goodsId " +
                " LEFT JOIN shop_action c ON c.ID = b.actionId  " +
                " WHERE " +
                " c.STATUS = '1'  " +
                " AND c.actionType = '2'  " +
                " AND c.beginTime <= now() AND c.endTime >= now()";
        List<Object[]> goodsList = commonDao.findListbySql(sql);
        List<Map<String, Object>> list = new ArrayList<>();
        for (Object[] oo : goodsList) {
            Map<String, Object> item = new HashMap<>();
            item.put("id", oo[0]);
            item.put("imgUrl", oo[1]);
            item.put("name", oo[2]);
            item.put("price", oo[3]);
            item.put("commentNum", oo[4]);
            item.put("giftType", oo[5]);
            item.put("giftNum", oo[6]);
            item.put("conditionMoney", oo[7]);
            if (!isAuth) {
                item.put("price", "登陆后查看");
            }

            String tempHql = "from GoodsSkuEntity as a where a.goods.id=" + oo[0] + " and a.status = '1'";
            List<GoodsSkuEntity> skuList = findByQueryString(tempHql);
            if (skuList != null && !skuList.isEmpty()) {
                item.put("isHasSku", "1");
                GoodsSkuEntity sku = skuList.get(0);
                item.put("price", sku.getPrice());
                item.put("oldPrice", sku.getOldPrice());
                item.put("stockNum", sku.getStockNum());
                if (StringUtil.isNotEmpty(sku.getImgUrl())) {
                    item.put("imgUrl", sku.getImgUrl());
                }
            }
            String giftSql = "SELECT " +
                    " a.name ," +
                    " a.stockNum " +
                    "FROM " +
                    " shop_gift a  " +
                    " LEFT JOIN shop_action_gift b  on b.giftId =a.id " +
                    " LEFT JOIN shop_action  c on c.ID = b.actionId " +
                    " left JOIN shop_action_goods d on c.id = d.actionId " +
                    " left join shop_goods e on e.id = d.goodsId " +
                    " where    c.status = '1'  and goodsId ='" + oo[0] + "'";
            List<GiftEntity> giftList = commonDao.findListbySql(giftSql);
            item.put("giftList", giftList);//赠品列表
            list.add(item);
        }
        //获取当前全部的品牌满赠的赠品
        Date nows = DateUtil.getCurrentTime();
        String hqlg = "from GiftEntity as a where a.status ='1' " +
                "and exists (select 1 from ActionGiftEntity as b where b.gift.id = a.id " +
                "and exists (select 1 from ActionEntity as c where c.status = '1' and c.actionType = '2' and b.action.id =c.id " +
                "and c.beginTime <= ? and c.endTime >= ? ))";
        List<GiftEntity> giftList = findHql(hqlg, new Object[]{nows, nows});
        //List<GiftEntity> giftList = findHql(hqlg, new Object[] {DateUtil.getCurrentTime(),DateUtil.getCurrentTime()});
        List<Map<String, Object>> glist = new ArrayList<>();
        for (GiftEntity gf : giftList) {
            Map<String, Object> item = new HashMap<>();
            item.put("id", gf.getId());
            item.put("imgUrl", gf.getImgUrl());
            item.put("gname", gf.getName());
            item.put("price", gf.getMoney());
            item.put("saleNum", gf.getSaleNum());
            item.put("stockNum", gf.getStockNum());
            glist.add(item);
        }
        map.put("giftList", glist);
        map.put("code", 0);
        map.put("goodsList", list);
        return map;
    }

    @Override
    public Map<String, Object> getConfigData(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();

        ConfigEntity config = get(ConfigEntity.class, Long.valueOf(1));
        map.put("notice", config.getNotice());
        map.put("afterReason", config.getAfterReason());
        map.put("cancleOrderTime", config.getCancleOrderTime());
        map.put("cancleReason", config.getCancleReason());
        map.put("certPath", config.getCertPath());
        map.put("goodsRule", config.getGoodsRule());
        map.put("mchId", config.getMchId());
        map.put("mchSecret", config.getMchSecret());
        map.put("receiveDay", config.getReceiverDay());
        map.put("refoundReason", config.getRefoudReason());
        map.put("salesAfterDay", config.getSalesAfterDay());
        map.put("shareImgUrl", config.getShareImgUrl());
        map.put("shareTitle", config.getShareTitle());
        map.put("mobile", config.getMobile());
        map.put("xianzhi", config.getXianzhi());
        return map;
    }

    @Override
    public Map<String, Object> getNowHomeActionTips(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        //1:品牌满赠  2：商品满赠
        String hql = "from ActionEntity as a where a.actionType ='1' and a.beginTime <= ? and a.endTime >= ?";
        Date now = DateUtil.getCurrentTime();
        List<ActionEntity> brandActionlist = findHql(hql, new Object[]{now, now});//品牌满赠

        hql = "from ActionEntity as a where a.actionType ='2' and a.beginTime <= ? and a.endTime >= ?";
        List<ActionEntity> goodsActionlist = findHql(hql, new Object[]{now, now});//品牌满赠

        if ((brandActionlist == null || brandActionlist.isEmpty())
                && (goodsActionlist == null || goodsActionlist.isEmpty())) {
            map.put("code", 1);
            return map;//暂无号外活动，不弹窗
        }
        List<Map<String, Object>> list = new ArrayList<>();
        for (ActionEntity act : brandActionlist) {
            Map<String, Object> item = new HashMap<>();
            item.put("id", act.getId());
            item.put("name", act.getName());
            item.put("actType", "brand");//品牌满赠的
            list.add(item);
        }
        for (ActionEntity act : goodsActionlist) {
            Map<String, Object> item = new HashMap<>();
            item.put("id", act.getId());
            item.put("name", act.getName());
            item.put("actType", "goods");//商品满赠的
            list.add(item);
        }
        map.put("list", list);
        map.put("code", 0);
        return map;
    }

    //根据商品表，获得赠品活动列表
    private List<ActionEntity> getActionListByGoodsEntity(GoodsEntity goodsList, Double money) {
        List<ActionEntity> list = new ArrayList<>();
        //获得brandId-money列表和goodsId-money列表（brandId和goodsId没有重复的值）
        List<BrandMoneyDto> bList = new ArrayList<>();
        List<GoodsMoneyDTO> gList = new ArrayList<>();
        if (goodsList != null) {
            GoodsMoneyDTO goodsMoneyDTO = new GoodsMoneyDTO();
            goodsMoneyDTO.setGoodsId(goodsList.getId());
            goodsMoneyDTO.setMoney(money);
            gList = addGoodsMoneyDTO(gList, goodsMoneyDTO);
            BrandMoneyDto brandMoneyDto = new BrandMoneyDto();
            brandMoneyDto.setBrandId(goodsList.getBrand().getId());
            brandMoneyDto.setMoney(money);
            bList = addBrandMoneyDto(bList, brandMoneyDto);
        }

        Date now = DateUtil.getCurrentTime();
        String sql = "";

        //获得符合品牌满赠的活动
        if (bList.size() > 0) {
            for (BrandMoneyDto brandMoneyDto : bList) {
                sql = "select a.* from shop_action a LEFT JOIN shop_action_brand b on a.ID = b.actionId " +
                        "where a.`status`='1' and a.actionType='1' and a.beginTime<=now() and a.endTime>=now() " +
                        "and a.conditionMoney<=" + brandMoneyDto.getMoney() + " and b.brandId=" + brandMoneyDto.getBrandId();
                List<Object[]> list1 = commonDao.findListbySql(sql);
                if (list1 != null && list1.size() > 0) {
                    Object[] actionArr = list1.get(0);
                    list = addActionList(list, actionArr);
                }
            }
        }

        //获得符合商品满赠的活动
        if (gList.size() > 0) {
            for (GoodsMoneyDTO goodsMoneyDTO : gList) {
                sql = "select a.* from shop_action a LEFT JOIN shop_action_goods b on a.ID = b.actionId " +
                        "where a.`status`='1' and a.actionType='2' and a.beginTime<=now() and a.endTime>=now() " +
                        "and a.conditionMoney<=" + goodsMoneyDTO.getMoney() + " and b.goodsId=" + goodsMoneyDTO.getGoodsId();
                List<Object[]> list2 = commonDao.findListbySql(sql);
                if (list2 != null && list2.size() > 0) {
                    Object[] actionArr = list2.get(0);
                    list = addActionList(list, actionArr);
                }
            }
        }

        return list;
    }

    //根据订单详情列表，获得赠品活动列表
    private List<ActionEntity> getActionListByOrderDetail(List<OrderDetailEntity> odeList) {
        List<ActionEntity> list = new ArrayList<>();
        //获得brandId-money列表和goodsId-money列表（brandId和goodsId没有重复的值）
        List<BrandMoneyDto> bList = new ArrayList<>();
        List<GoodsMoneyDTO> gList = new ArrayList<>();
        if (odeList != null && odeList.size() > 0) {
            for (OrderDetailEntity orderDetailEntity : odeList) {
                GoodsMoneyDTO goodsMoneyDTO = new GoodsMoneyDTO();
                goodsMoneyDTO.setGoodsId(orderDetailEntity.getGoods().getId());
                goodsMoneyDTO.setMoney(orderDetailEntity.getMoney());
                gList = addGoodsMoneyDTO(gList, goodsMoneyDTO);
                BrandMoneyDto brandMoneyDto = new BrandMoneyDto();
                brandMoneyDto.setBrandId(orderDetailEntity.getGoods().getBrand().getId());
                brandMoneyDto.setMoney(orderDetailEntity.getMoney());
                bList = addBrandMoneyDto(bList, brandMoneyDto);
            }
        }

        Date now = DateUtil.getCurrentTime();
        String sql = "";

        //获得符合品牌满赠的活动
        if (bList.size() > 0) {
            for (BrandMoneyDto brandMoneyDto : bList) {
                sql = "select a.* from shop_action a LEFT JOIN shop_action_brand b on a.ID = b.actionId " +
                        "where a.`status`='1' and a.actionType='1' and a.beginTime<=now() and a.endTime>=now() " +
                        "and a.conditionMoney<=" + brandMoneyDto.getMoney() + " and b.brandId=" + brandMoneyDto.getBrandId();
                List<Object[]> list1 = commonDao.findListbySql(sql);
                if (list1 != null && list1.size() > 0) {
                    Object[] actionArr = list1.get(0);
                    list = addActionList(list, actionArr);
                }
            }
        }

        //获得符合商品满赠的活动
        if (gList.size() > 0) {
            for (GoodsMoneyDTO goodsMoneyDTO : gList) {
                sql = "select a.* from shop_action a LEFT JOIN shop_action_goods b on a.ID = b.actionId " +
                        "where a.`status`='1' and a.actionType='2' and a.beginTime<=now() and a.endTime>=now() " +
                        "and a.conditionMoney<=" + goodsMoneyDTO.getMoney() + " and b.goodsId=" + goodsMoneyDTO.getGoodsId();
                List<Object[]> list2 = commonDao.findListbySql(sql);
                if (list2 != null && list2.size() > 0) {
                    Object[] actionArr = list2.get(0);
                    list = addActionList(list, actionArr);
                }
            }
        }

        return list;
    }

    //根据订单详情列表，获得赠品历史活动列表
    private List<ActionEntity> getOldActionListByOrderDetail(List<OrderDetailEntity> odeList) {
        List<ActionEntity> list = new ArrayList<>();
        //获得brandId-money列表和goodsId-money列表（brandId和goodsId没有重复的值）
        List<BrandMoneyDto> bList = new ArrayList<>();
        List<GoodsMoneyDTO> gList = new ArrayList<>();
        if (odeList != null && odeList.size() > 0) {
            for (OrderDetailEntity orderDetailEntity : odeList) {
                GoodsMoneyDTO goodsMoneyDTO = new GoodsMoneyDTO();
                goodsMoneyDTO.setGoodsId(orderDetailEntity.getGoods().getId());
                goodsMoneyDTO.setMoney(orderDetailEntity.getMoney());
                gList = addGoodsMoneyDTO(gList, goodsMoneyDTO);
                BrandMoneyDto brandMoneyDto = new BrandMoneyDto();
                brandMoneyDto.setBrandId(orderDetailEntity.getGoods().getBrand().getId());
                brandMoneyDto.setMoney(orderDetailEntity.getMoney());
                bList = addBrandMoneyDto(bList, brandMoneyDto);
            }
        }

        Date now = DateUtil.getCurrentTime();
        String sql = "";

        //获得符合品牌满赠的活动
        if (bList.size() > 0) {
            for (BrandMoneyDto brandMoneyDto : bList) {
                sql = "select a.* from shop_action a LEFT JOIN shop_action_brand b on a.ID = b.actionId " +
                        "where a.`status`='1' and a.actionType='1' " +
                        "and a.conditionMoney<=" + brandMoneyDto.getMoney() + " and b.brandId=" + brandMoneyDto.getBrandId();
                List<Object[]> list1 = commonDao.findListbySql(sql);
                if (list1 != null && list1.size() > 0) {
                    Object[] actionArr = list1.get(0);
                    list = addActionList(list, actionArr);
                }
            }
        }

        //获得符合商品满赠的活动
        if (gList.size() > 0) {
            for (GoodsMoneyDTO goodsMoneyDTO : gList) {
                sql = "select a.* from shop_action a LEFT JOIN shop_action_goods b on a.ID = b.actionId " +
                        "where a.`status`='1' and a.actionType='2' " +
                        "and a.conditionMoney<=" + goodsMoneyDTO.getMoney() + " and b.goodsId=" + goodsMoneyDTO.getGoodsId();
                List<Object[]> list2 = commonDao.findListbySql(sql);
                if (list2 != null && list2.size() > 0) {
                    Object[] actionArr = list2.get(0);
                    list = addActionList(list, actionArr);
                }
            }
        }

        return list;
    }

    //将actionEntity添加到List<ActionEntity> list列表中，若有重复，不添加
    private List<ActionEntity> addActionList(List<ActionEntity> list, Object[] actionArr) {
        //将Object[] actionArr转为ActionEntity
        ActionEntity actionEntity = new ActionEntity();
        actionEntity.setId(Long.valueOf(String.valueOf(actionArr[0])));
        //actionEntity.setCreateDate(actionArr[1]);
        actionEntity.setStatus(String.valueOf(actionArr[2]));
        //actionEntity.setUpdateDate(actionArr[3]);
        actionEntity.setActionType(String.valueOf(actionArr[4]));
        //actionEntity.setBeginTime(actionArr[5]);
        actionEntity.setConditionMoney(Double.valueOf(String.valueOf(actionArr[6])));
        //actionEntity.setEndTime(actionArr[7]);
        actionEntity.setGiftNum(Integer.parseInt(String.valueOf(actionArr[8])));
        actionEntity.setGiftType(String.valueOf(actionArr[9]));
        actionEntity.setName(String.valueOf(actionArr[10]));

        if (list.size() > 0) {
            boolean isExect = false;
            for (ActionEntity entity : list) {
                if (entity.getId() == actionEntity.getId()) {
                    isExect = true;
                }
            }
            if (!isExect) {
                list.add(actionEntity);
            }
        } else {
            list.add(actionEntity);
        }
        return list;
    }

    //将goodsMoneyDTO添加到List<GoodsMoneyDTO>列表，若有重复的goodsId，则将money相加
    private List<GoodsMoneyDTO> addGoodsMoneyDTO(List<GoodsMoneyDTO> gList, GoodsMoneyDTO goodsMoneyDTO) {
        if (gList.size() > 0) {
            boolean isExect = false;
            for (GoodsMoneyDTO moneyDTO : gList) {
                if (moneyDTO.getGoodsId() == goodsMoneyDTO.getGoodsId()) {
                    moneyDTO.setMoney(moneyDTO.getMoney() + goodsMoneyDTO.getMoney());
                    isExect = true;
                    break;
                }
            }
            if (!isExect) {
                gList.add(goodsMoneyDTO);
            }
        } else {
            gList.add(goodsMoneyDTO);
        }
        return gList;
    }

    //将brandMoneyDto添加到List<BrandMoneyDto>列表，若有重复的brandId，则将money相加
    private List<BrandMoneyDto> addBrandMoneyDto(List<BrandMoneyDto> bList, BrandMoneyDto brandMoneyDto) {
        if (bList.size() > 0) {
            boolean isExect = false;
            for (BrandMoneyDto moneyDto : bList) {
                if (moneyDto.getBrandId() == brandMoneyDto.getBrandId()) {
                    moneyDto.setMoney(moneyDto.getMoney() + brandMoneyDto.getMoney());
                    isExect = true;
                    break;
                }
            }
            if (!isExect) {
                bList.add(brandMoneyDto);
            }
        } else {
            bList.add(brandMoneyDto);
        }
        return bList;
    }

    //根据订单id获得该订单的赠品
    /*
    private String getGiftByOrderId(Long orderId) {
        String gift = "";
        String hql = "from ActionGiftEntity as a where 1=1 and exists(select 1 from OrderActionEntity b where a.action.id = b.action.id and b.order.id=" + orderId + ")";
        List<ActionGiftEntity> oaList = findByQueryString(hql);
        if (oaList != null && oaList.size() > 0) {
            for (ActionGiftEntity actionGiftEntity : oaList) {
                gift = gift + actionGiftEntity.getGift().getName() + " "+actionGiftEntity.getGift().getSaleNum()+",";
            }
        }
        if (StringUtil.isNotEmpty(gift)) {
            gift = StringUtil.substringBeforeLast(gift, ",");
        }
        return gift;
    }*/
    //根据订单id获得该订单的赠品
    private String getGiftByOrderId(Long orderId) {
        String gift = "";
        String sqlg = "select giftId,COUNT(saleNum) as allnum from shop_order_action where orderId = " + orderId +" group by giftId";
        //String hql = "from OrderActionEntity as a where exists(select 1 from OrderEntity as b where a.order.id = b.id and a.order.id=" + orderId + ")";
        //sql = "from OrderActionEntity as a INNER JOIN OrderEntity as b ON a.order.id = b.id and a.order.id = "+ orderId +"";
        List<Object[]> oaList = commonDao.findListbySql(sqlg);
        //List<OrderActionEntity> oaList = findByQueryString(hql);
        if (oaList != null && oaList.size() > 0) {
            for (Object[] oa : oaList) {
                BigInteger giftid = (BigInteger)oa[0];
                GiftEntity giftlists = get(GiftEntity.class, giftid.longValue());
                //for (Object[] gf : giftList) {
                gift = gift + giftlists.getName() + " " + oa[1]  + ",";
            }
        }
        if (StringUtil.isNotEmpty(gift)) {
            gift = StringUtil.substringBeforeLast(gift, ",");
        }
        return gift;
    }
}