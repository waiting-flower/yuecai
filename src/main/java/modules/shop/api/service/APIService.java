package modules.shop.api.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.service.CommonService;

public interface APIService extends CommonService{

	Map<String, Object> getHomeSeckillList(HttpServletRequest request);

	Map<String, Object> getHomeBrandList(HttpServletRequest request);

	Map<String, Object> getHomeDiscountGoodsList(HttpServletRequest request);

	Map<String, Object> getGoodsList(HttpServletRequest request);

	Map<String, Object> saveReg(HttpServletRequest request);

	Map<String, Object> saveLogin(HttpServletRequest request);

	Map<String, Object> saveWorkUserData(HttpServletRequest request);

	Map<String, Object> getGoodsDetail(HttpServletRequest request);

	void saveGoodsView(HttpServletRequest request);

	Map<String, Object> getAddressList(HttpServletRequest request);

	Map<String, Object> saveAddress(HttpServletRequest request);

	Map<String, Object> deleteAddress(HttpServletRequest request);

	Map<String, Object> editDefault(HttpServletRequest request);

	Map<String, Object> setDefaultAddress(HttpServletRequest request);

	Map<String, Object> getAddressDetail(HttpServletRequest request);

	Map<String, Object> saveUserNick(HttpServletRequest request);

	Map<String, Object> saveUserPcode(HttpServletRequest request);

	Map<String, Object> saveUserPhoto(HttpServletRequest request);

	Map<String, Object> getUpToken(HttpServletRequest request);

	Map<String, Object> getCouponList(HttpServletRequest request);

	Map<String, Object> getUserCouponList(HttpServletRequest request);

	Map<String, Object> saveUserCoupon(HttpServletRequest request);

	Map<String, Object> saveGoodsToCart(HttpServletRequest request);

	Map<String, Object> getGoodsBuyDetail(HttpServletRequest request);

	Map<String, Object> deleteCart(HttpServletRequest request);

	Map<String, Object> getCartData(HttpServletRequest request);

	Map<String, Object> updateCart(HttpServletRequest request);

	Map<String, Object> getCartNum(HttpServletRequest request);

	Map<String, Object> saveGoodsCollect(HttpServletRequest request);

	Map<String, Object> getCartBuyData(HttpServletRequest request);

	Map<String, Object> createGoodsBuyOrder(HttpServletRequest request);

	Map<String, Object> createCartBuyOrder(HttpServletRequest request);

	Map<String, Object> getUserViewGoodsList(HttpServletRequest request);

	Map<String, Object> getUserCollectGoodsList(HttpServletRequest request);

	Map<String, Object> getOrderList(HttpServletRequest request);

	Map<String, Object> getOrderListyeji(HttpServletRequest request);

	Map<String, Object> getOrderDetail(HttpServletRequest request);

	Map<String, Object> saveCancleOrder(HttpServletRequest request);

	Map<String, Object> saveReceiveOrder(HttpServletRequest request);

	Map<String, Object> saveRefoundOrder(HttpServletRequest request);

	void initCommentOrder(HttpServletRequest request);

	Map<String, Object> saveOrderComment(HttpServletRequest request);

	Map<String, Object> loadCommentById(HttpServletRequest request);

	Map<String, Object> addCommetPic(HttpServletRequest request);

	Map<String, Object> getSeckillTimeList(HttpServletRequest request);

	Map<String, Object> getSecGoodsList(HttpServletRequest request);

	Map<String, Object> getCateList(HttpServletRequest request);

	Map<String, Object> saveCancleGoodsCollect(HttpServletRequest request);

	Map<String, Object> saveCancleAllGoods(HttpServletRequest request);

	Map<String, Object> getUserTuijianList(HttpServletRequest request);

	Map<String, Object> getNowBrandGiftActionGoodsList(HttpServletRequest request);

	Map<String, Object> getNowGoodsGiftActionGoodsList(HttpServletRequest request);

	Map<String, Object> getConfigData(HttpServletRequest request);

	Map<String, Object> getNowHomeActionTips(HttpServletRequest request);

}
