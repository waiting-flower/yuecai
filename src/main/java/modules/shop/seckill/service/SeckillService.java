package modules.shop.seckill.service;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface SeckillService extends CommonService{

	Map<String, Object> saveConfig(HttpServletRequest request);

	DataTableReturn loadSeckillActionByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveSeckillAction(HttpServletRequest request);

	Map<String, Object> deleteSeckillAction(HttpServletRequest request);

	DataTableReturn loadSeckillGoodsByPage(JQueryDataTables dataTable, HttpServletRequest request);

	DataTableReturn loadGoodsByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveSeckillGoodsSet(HttpServletRequest request);

	Map<String, Object> deleteSeckillGoods(HttpServletRequest request);

    Map<String,Object> saveSeckillEnd(HttpServletRequest request);
}
