package modules.shop.seckill.service.impl;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.HqlQuery;
import common.util.StringUtil;
import modules.shop.goods.entity.GoodsEntity;
import modules.shop.goods.entity.GoodsSkuEntity;
import modules.shop.seckill.entity.*;
import modules.shop.seckill.service.SeckillService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SeckillServiceImpl extends CommonServiceImpl implements SeckillService{

	@Override
	public Map<String, Object> saveConfig(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = request.getParameter("id");
		String openHour = request.getParameter("openHour");
		SeckillConfigEntity a = null;
		if(StringUtil.isNotEmpty(id)) {
			a = get(SeckillConfigEntity.class, Long.valueOf(id));
			a.setUpdateDate(DateUtil.getCurrentTime());
		}
		else {
			a = new SeckillConfigEntity();
			a.setCreateDate(DateUtil.getCurrentTime());
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
		}
		a.setOpenHour(openHour);
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public DataTableReturn loadSeckillActionByPage(JQueryDataTables dataTable, HttpServletRequest request) {
		StringBuffer hql = new StringBuffer(
				" from SeckillActionEntity as  a where 1 = 1  and a.status = '1' ");
		List<Object> params = new ArrayList<Object>();
		String name = request.getParameter("name");
		if (StringUtil.isNotEmpty(name)) {
			hql.append(" and a.name like ? ");
			params.add("%" + name + "%");
		}
		hql.append(" order by   a.createDate desc");
		HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
		int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
		hqlQuery.setCurPage(currPage + 1);
		hqlQuery.setPageSize(dataTable.getDisplayLength());
		DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
		List<SeckillActionEntity> pList = (List<SeckillActionEntity>) d.getAaData();
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		for (SeckillActionEntity p : pList) {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("id", p.getId());
			/*
			 * private String name;//秒杀活动名称
		private Integer limitBuy;//是否限制购买 1：表示限制购买  0：表示不限制购买
		private String openDay;
		private String openHours;
		private String isCanUseCoupon;//是否可以使用优惠券
			 */
			item.put("name", p.getName());
			item.put("limitBuy", p.getLimitBuy());
			item.put("beginTime", DateUtil.getTimeByCustomPattern(p.getBeginTime(), DateUtil.yyyyMMddHHmmss));
			item.put("endTime", DateUtil.getTimeByCustomPattern(p.getEndTime(), DateUtil.yyyyMMddHHmmss));
			item.put("openHours", p.getOpenHours());
			item.put("isCanUseCoupon", p.getIsCanUseCoupon());
			item.put("status", p.getStatus());
			item.put("goodsNum", p.getGoodsNum());
			item.put("saleNum", p.getSaleNum());
			item.put("actionState", p.getActionState());
			item.put("time",DateUtil.getTimeByCustomPattern( p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
			mapList.add(item);
		}
		d.setAaData(mapList);
		d.setsEcho(dataTable.getEcho());
		return d;
	}

	@Override
	public Map<String, Object> saveSeckillAction(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String limitBuy = request.getParameter("limitBuy");
		String beginTime = request.getParameter("beginTime");
		String endTime = request.getParameter("endTime");
		String openHours = request.getParameter("openHours");
		String isCanUseCoupon = request.getParameter("isCanUseCoupon");
		/*
		 * private String name;//秒杀活动名称
	private Integer limitBuy;//是否限制购买 1：表示限制购买  0：表示不限制购买
	private String openDay;
	private String openHours;
	private String isCanUseCoupon;//是否可以使用优惠券
		 */
		SeckillActionEntity a = null;
		if(StringUtil.isNotEmpty(id)) {
			a = get(SeckillActionEntity.class, Long.valueOf(id));
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
		}
		else {
			a = new SeckillActionEntity();
			a.setCreateDate(DateUtil.getCurrentTime());
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
			a.setGoodsNum(0);
			a.setSaleNum(0);
			a.setActionState("1");
		}
		a.setName(name);
		a.setBeginTime(DateUtil.parse(beginTime, DateUtil.yyyyMMddHHmmss));
		a.setEndTime(DateUtil.parse(endTime,DateUtil.yyyyMMddHHmmss));
		a.setOpenHours(openHours);
		a.setIsCanUseCoupon(isCanUseCoupon);
		a.setLimitBuy(limitBuy);
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> deleteSeckillAction(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		SeckillActionEntity a = get(SeckillActionEntity.class, Long.valueOf(id));
		if (a == null) { 
			map.put("code", 1);
			return map;
		}
		a.setStatus("0");
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public DataTableReturn loadSeckillGoodsByPage(JQueryDataTables dataTable, HttpServletRequest request) {
		StringBuffer hql = new StringBuffer(
				" from SeckillGoodsMapEntity as  a where 1 = 1  and a.status = '1' ");
		List<Object> params = new ArrayList<Object>();
		String seckillId = request.getParameter("seckillId");
		if (StringUtil.isNotEmpty(seckillId)) {
			hql.append(" and a.seckillAction.id = ? ");
			params.add(Long.valueOf(seckillId));
		}
		String name = request.getParameter("name");
		if (StringUtil.isNotEmpty(name)) {
			hql.append(" and a.goods.name like ? ");
			params.add("%" + name + "%");
		}
		hql.append(" order by a.createDate desc");
		HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
		int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
		hqlQuery.setCurPage(currPage + 1);
		hqlQuery.setPageSize(dataTable.getDisplayLength());
		DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
		List<SeckillGoodsMapEntity> pList = (List<SeckillGoodsMapEntity>) d.getAaData();
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		for (SeckillGoodsMapEntity p : pList) {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("id", p.getId());
			item.put("name", p.getGoods().getName());
			item.put("imgUrl", p.getGoods().getImgUrl());
			item.put("stockNum", p.getGoods().getStockNum());
			item.put("price", p.getGoods().getPrice());
			item.put("isOnSale", p.getGoods().getIsOnSale());
			item.put("goodsNum", p.getGoodsNum());
			item.put("state", "1");//进行中
			item.put("time",DateUtil.getTimeByCustomPattern( p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
			mapList.add(item);
		}
		d.setAaData(mapList);
		d.setsEcho(dataTable.getEcho());
		return d;
	}

	@Override
	public DataTableReturn loadGoodsByPage(JQueryDataTables dataTable, HttpServletRequest request) {
		StringBuffer hql = new StringBuffer(
				" from GoodsEntity as  a where 1 = 1  and a.status = '1' ");
		List<Object> params = new ArrayList<Object>();
		String name = request.getParameter("name");
		if (StringUtil.isNotEmpty(name)) {
			hql.append(" and a.name like ? ");
			params.add("%" + name + "%");
		}
		hql.append(" order by a.createDate desc");
		HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
		int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
		hqlQuery.setCurPage(currPage + 1);
		hqlQuery.setPageSize(dataTable.getDisplayLength());
		DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
		List<GoodsEntity> pList = (List<GoodsEntity>) d.getAaData();
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		for (GoodsEntity p : pList) {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("id", p.getId());
			item.put("name", p.getName());
			item.put("imgUrl", p.getImgUrl());
			item.put("stockNum", p.getStockNum());
			item.put("price", p.getPrice());
			item.put("groupState", "0");//未参与秒杀
			String tempHql = "from SeckillGoodsMapEntity as a where a.status='1' and a.goods.id= ? and a.seckillAction.endTime > ?";
			List<SeckillGoodsMapEntity> sggList = findHql(tempHql, new Object[] {p.getId(),DateUtil.getCurrentTime()});
			if(sggList != null && !sggList.isEmpty()) {
				item.put("groupState", "1");//已参与秒杀
			}
			item.put("time",DateUtil.getTimeByCustomPattern( p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
			mapList.add(item);
		}
		d.setAaData(mapList);
		d.setsEcho(dataTable.getEcho());
		return d;
	}

	@Override
	public Map<String, Object> saveSeckillGoodsSet(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String seckillId = request.getParameter("seckillId");
		String goodsId = request.getParameter("goodsId");
		String priceSet = request.getParameter("priceSet");
		
		SeckillActionEntity seckillAction = get(SeckillActionEntity.class, Long.valueOf(seckillId));
		seckillAction.setGoodsNum(seckillAction.getGoodsNum() + 1);
		saveOrUpdate(seckillAction);
		GoodsEntity goods = get(GoodsEntity.class, Long.valueOf(goodsId));
		goods.setSeckillId(Long.valueOf(seckillId));
		saveOrUpdate(goods);
		
		SeckillGoodsMapEntity gg = new SeckillGoodsMapEntity();
		gg.setCreateDate(DateUtil.getCurrentTime());
		gg.setUpdateDate(DateUtil.getCurrentTime());
		gg.setStatus("1");
		gg.setGoodsNum(0);
		gg.setSeckillAction(seckillAction);
		gg.setGoods(goods);
		save(gg);
		
		JSONObject priceItem = JSONObject.fromObject(priceSet);
	 
		SeckillGoodsPriceSetEntity sgps = new SeckillGoodsPriceSetEntity();
		sgps.setCreateDate(DateUtil.getCurrentTime());
		sgps.setUpdateDate(DateUtil.getCurrentTime());
		sgps.setStatus("1");
		if(StringUtil.isNotEmpty(priceItem.getString("discount"))) {
			sgps.setDiscount(Double.valueOf(priceItem.getString("discount")));
		}
		sgps.setGoods(goods);
		sgps.setSeckillGoodsMap(gg);
		sgps.setSeckillAction(seckillAction);
		if(priceItem.has("isOpenSkuPrice")) {
			sgps.setIsOpenSkuPrice(priceItem.getString("isOpenSkuPrice"));
		}
		else {
			sgps.setIsOpenSkuPrice("0");
		}
		if("1".equals(sgps.getIsOpenSkuPrice())) {
			sgps.setSkuPriceJson(priceItem.getJSONArray("skuArr").toString());
			JSONArray skuArr = priceItem.getJSONArray("skuArr");
			for(int j = 0; j < skuArr.size(); j++) {
				JSONObject skuItem = skuArr.getJSONObject(j);
				GoodsSkuEntity skuGoods = get(GoodsSkuEntity.class, Long.valueOf(skuItem.getString("skuId")));
				SeckillGoodsSkuPriceSetEntity skuPrice = new SeckillGoodsSkuPriceSetEntity();
				skuPrice.setCreateDate(DateUtil.getCurrentTime());
				skuPrice.setUpdateDate(DateUtil.getCurrentTime());
				skuPrice.setStatus("1");
				skuPrice.setSeckillGoodsMap(gg);
				skuPrice.setSeckillAction(seckillAction);
				skuPrice.setSku(skuGoods);
				skuPrice.setDiscount(Double.valueOf(skuItem.getString("discount")));
				save(skuPrice);
			}
		}
		save(sgps);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> deleteSeckillGoods(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		SeckillGoodsMapEntity a = get(SeckillGoodsMapEntity.class, Long.valueOf(id));
		if (a == null) { 
			map.put("code", 1);
			return map; 
		}

		a.setStatus("0");
		saveOrUpdate(a);

		GoodsEntity goods = a.getGoods();
		goods.setSeckillId(null);
		saveOrUpdate(goods);

		//更新秒杀商品价格表状态
		SeckillGoodsPriceSetEntity sec = get(SeckillGoodsPriceSetEntity.class, Long.valueOf(id));
		//String hqlss = "from SeckillGoodsPriceSetEntity as a where a.seckillGoods.id =19";
		//List<SeckillGoodsPriceSetEntity> secList = findByQueryString(hqlss);
		//SeckillGoodsPriceSetEntity sec = secList.get(0);
		sec.setStatus("0");
		saveOrUpdate(sec);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> saveSeckillEnd(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		String id = request.getParameter("id");
		SeckillActionEntity a = get(SeckillActionEntity.class,Long.valueOf(id));
		if (a == null) {
			map.put("code", 1);
			return map;
		}
		a.setStatus("0");
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

}
