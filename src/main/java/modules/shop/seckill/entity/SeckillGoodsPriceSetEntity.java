package modules.shop.seckill.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
import modules.shop.goods.entity.GoodsEntity;

/**
 * 秒杀活动减价设置
 * @author Administrator
 *
 */
@Entity
@Table(name = "shop_seckill_goods_price", schema = "")
public class SeckillGoodsPriceSetEntity extends CommonEntity{
	private SeckillGoodsMapEntity seckillGoodsMap;
	private SeckillActionEntity seckillAction;
	private GoodsEntity goods;
	private String isOpenSkuPrice;//是否开启分规格参数的设置拼图价 0表示不开启  1：表示开启
	private String skuPriceJson;//如果开启了的话，那么对赢的json数据
	private Double discount;//减价设置
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "seckillGoodsId")
	public SeckillGoodsMapEntity getSeckillGoodsMap() {
		return seckillGoodsMap;
	}
	public void setSeckillGoodsMap(SeckillGoodsMapEntity seckillGoodsMap) {
		this.seckillGoodsMap = seckillGoodsMap;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "seckillActionId")
	public SeckillActionEntity getSeckillAction() {
		return seckillAction;
	}
	public void setSeckillAction(SeckillActionEntity seckillAction) {
		this.seckillAction = seckillAction;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "goodsId")
	public GoodsEntity getGoods() {
		return goods;
	}
	public void setGoods(GoodsEntity goods) {
		this.goods = goods;
	}
	public String getIsOpenSkuPrice() {
		return isOpenSkuPrice;
	}
	public void setIsOpenSkuPrice(String isOpenSkuPrice) {
		this.isOpenSkuPrice = isOpenSkuPrice;
	}
	public String getSkuPriceJson() {
		return skuPriceJson;
	}
	public void setSkuPriceJson(String skuPriceJson) {
		this.skuPriceJson = skuPriceJson;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	
	
}
