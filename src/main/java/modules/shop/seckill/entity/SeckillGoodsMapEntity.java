package modules.shop.seckill.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
import modules.shop.goods.entity.GoodsEntity;

@Entity
@Table(name = "shop_seckill_goods", schema = "")
public class SeckillGoodsMapEntity extends CommonEntity{
	private SeckillActionEntity seckillAction;
	private GoodsEntity goods;
	private Integer goodsNum;//已秒杀件数量
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "seckillActionId")
	public SeckillActionEntity getSeckillAction() {
		return seckillAction;
	}
	public void setSeckillAction(SeckillActionEntity seckillAction) {
		this.seckillAction = seckillAction;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "goodsId")
	public GoodsEntity getGoods() {
		return goods;
	}
	public void setGoods(GoodsEntity goods) {
		this.goods = goods;
	}
	public Integer getGoodsNum() {
		return goodsNum;
	}
	public void setGoodsNum(Integer goodsNum) {
		this.goodsNum = goodsNum;
	}

}
