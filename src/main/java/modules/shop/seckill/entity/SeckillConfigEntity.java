package modules.shop.seckill.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import common.entity.CommonEntity;

/**
 * 秒杀活动配置
 * @author Administrator
 *
 */
@Entity
@Table(name = "shop_seckill_config", schema = "")
public class SeckillConfigEntity extends CommonEntity{
	private String openHour;//开放的时间段列表格式为   00:00~02:00,02:00~04:00

	public String getOpenHour() {
		return openHour;
	}

	public void setOpenHour(String openHour) {
		this.openHour = openHour;
	}
	
}
