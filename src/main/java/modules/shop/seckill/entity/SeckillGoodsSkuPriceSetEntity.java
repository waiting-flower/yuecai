package modules.shop.seckill.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
import modules.shop.goods.entity.GoodsSkuEntity;

/**
 * 规格的秒杀价格设置
 * @author Administrator
 *
 */
@Entity
@Table(name = "shop_seckill_goods_sku_price", schema = "")
public class SeckillGoodsSkuPriceSetEntity extends CommonEntity{
	private SeckillGoodsMapEntity seckillGoodsMap;
	private SeckillActionEntity seckillAction;
	private GoodsSkuEntity sku;
	private Double discount;//减价设置
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "seckillGoodsId")
	public SeckillGoodsMapEntity getSeckillGoodsMap() {
		return seckillGoodsMap;
	}
	public void setSeckillGoodsMap(SeckillGoodsMapEntity seckillGoodsMap) {
		this.seckillGoodsMap = seckillGoodsMap;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "seckillActionId")
	public SeckillActionEntity getSeckillAction() {
		return seckillAction;
	}
	public void setSeckillAction(SeckillActionEntity seckillAction) {
		this.seckillAction = seckillAction;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "skuId")
	public GoodsSkuEntity getSku() {
		return sku;
	}
	public void setSku(GoodsSkuEntity sku) {
		this.sku = sku;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	
}
