package modules.shop.seckill.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import common.entity.CommonEntity;

/**
 * 秒杀活动对象
 * @author Administrator
 *
 */
@Entity
@Table(name = "shop_seckill_action", schema = "")
public class SeckillActionEntity extends CommonEntity{
	private String name;//秒杀活动名称
	private Integer goodsNum;
	private Integer saleNum;
	private String limitBuy;//是否限制购买 1：表示限制购买  0：表示不限制购买
	private Date beginTime;
	private Date endTime;
	private String openHours;
	private String isCanUseCoupon;//是否可以使用优惠券
	private String actionState;//活动状态
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLimitBuy() {
		return limitBuy;
	}
	public void setLimitBuy(String limitBuy) {
		this.limitBuy = limitBuy;
	}
	public String getOpenHours() {
		return openHours;
	}
	public void setOpenHours(String openHours) {
		this.openHours = openHours;
	}
	public String getIsCanUseCoupon() {
		return isCanUseCoupon;
	}
	public void setIsCanUseCoupon(String isCanUseCoupon) {
		this.isCanUseCoupon = isCanUseCoupon;
	}
	public Integer getGoodsNum() {
		return goodsNum;
	}
	public void setGoodsNum(Integer goodsNum) {
		this.goodsNum = goodsNum;
	}
	public Integer getSaleNum() {
		return saleNum;
	}
	public void setSaleNum(Integer saleNum) {
		this.saleNum = saleNum;
	}
	public String getActionState() {
		return actionState;
	}
	public void setActionState(String actionState) {
		this.actionState = actionState;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	
}
