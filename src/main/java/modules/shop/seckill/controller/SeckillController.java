package modules.shop.seckill.controller;


import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.util.StringUtil;
import modules.shop.goods.entity.GoodsEntity;
import modules.shop.goods.entity.GoodsParamsEntity;
import modules.shop.goods.entity.GoodsSkuEntity;
import modules.shop.seckill.entity.*;
import modules.shop.seckill.service.SeckillService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "admin/shop/seckill")
public class SeckillController extends BaseController{
	
	@Autowired
	SeckillService service;
	
	/**
	 * 秒杀配置
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "config")
	public String config( HttpServletRequest request, HttpServletResponse response) {
		SeckillConfigEntity menu = service.get(SeckillConfigEntity.class, 1l);
		request.setAttribute("a", menu);
		return templatePath + "shop/seckill/config";
	}
	
	
	/**
	 * 保存一个秒杀配置
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveConfig")
	@ResponseBody
	public Map<String, Object> saveConfig(HttpServletRequest request) {
		return service.saveConfig(request);
	}
	
	
	/**
	 * 秒杀活动列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "seckillActionList")
	public String seckillActionList( HttpServletRequest request, HttpServletResponse response) {
		return templatePath + "shop/seckill/seckillActionList";
	}
	
	/**
	 * 秒杀活动详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "seckillActionInfo")
	public String seckillActionInfo( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			SeckillActionEntity menu = service.get(SeckillActionEntity.class, Long.valueOf(id));
			request.setAttribute("a", menu);
		} 
		SeckillConfigEntity config = service.get(SeckillConfigEntity.class,1l);
		String openHours = config.getOpenHour();
		request.setAttribute("openHours", openHours);
		return templatePath + "shop/seckill/seckillActionInfo";
	}
	
	
	/**
	 * 分页获取秒杀活动列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadSeckillActionByPage")
	@ResponseBody
	public DataTableReturn loadSeckillActionByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = service.loadSeckillActionByPage(dataTable, request);
		return d;
	}
	
	/**
	 * 保存一个秒杀活动
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveSeckillAction")
	@ResponseBody
	public Map<String, Object> saveSeckillAction(HttpServletRequest request) {
		return service.saveSeckillAction(request);
	}
	
	/**
	 * 删除一个秒杀活动
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteSeckillAction")
	@ResponseBody
	public Map<String, Object> deleteSeckillAction(HttpServletRequest request) {
		return service.deleteSeckillAction(request);
	}

	/**
	 * 结束一个秒杀活动
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveSeckillEnd")
	@ResponseBody
	public Map<String, Object> saveSeckillEnd(HttpServletRequest request) {
		return service.saveSeckillEnd(request);
	}
	
	
	
	/**
	 * 秒杀活动商品管理
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "seckillGoodsInfo")
	public String seckillGoodsInfo( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			SeckillActionEntity a = service.get(SeckillActionEntity.class, Long.valueOf(id));
			request.setAttribute("a", a); 
		} 
		return templatePath + "shop/seckill/seckillGoodsInfo";
	}
	
	
	/**
	 * 分页获取秒杀活动的商品列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadSeckillGoodsByPage")
	@ResponseBody
	public DataTableReturn loadSeckillGoodsByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = service.loadSeckillGoodsByPage(dataTable, request);
		return d;
	}
	
	
	/**
	 * 拼团活动  选择商品
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "goodsChoiceList")
	public String goodsChoiceList( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			SeckillActionEntity a = service.get(SeckillActionEntity.class, Long.valueOf(id));
			request.setAttribute("a", a); 
		} 
		return templatePath + "shop/seckill/goodsChoiceList";
	}
	
	/**
	 * 分页获取秒杀团活动要去选择的商品列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadGoodsByPage")
	@ResponseBody
	public DataTableReturn loadGoodsByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = service.loadGoodsByPage(dataTable, request);
		return d;
	}
	
	
	/**
	 * 拼团活动商品添加页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "goodsSet")
	public String goodsSet( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			SeckillActionEntity a = service.get(SeckillActionEntity.class, Long.valueOf(id));
			request.setAttribute("a", a); 
		} 
		String goodsId = request.getParameter("goodsId");
		if(StringUtil.isNotEmpty(goodsId)) {
			GoodsEntity goods = service.get(GoodsEntity.class, Long.valueOf(goodsId));
			request.setAttribute("goods", goods);
			
			String hql = "from GoodsParamsEntity as a where a.goods.id=" + goodsId;
			List<GoodsParamsEntity> paramsList = service.findByQueryString(hql);
			request.setAttribute("paramsList", paramsList);
			
			String hql2 = "from GoodsSkuEntity as a where a.status='1' and a.goods.id=" + goodsId;
			List<GoodsSkuEntity> skuList = service.findByQueryString(hql2);
			List<Map<String, Object>> mapList = new ArrayList<>();
			for(GoodsSkuEntity sku : skuList) {
				Map<String,Object> item = new HashMap<>();
				String paramsValusJson = sku.getParmasValuesJson();
				JSONObject itemJson = JSONObject.fromObject(paramsValusJson);
				List<Map<String, Object>> skuMapList = new ArrayList<>();
				for(int i = 0; i < paramsList.size(); i++) {
					Map<String,Object> skuItem = new HashMap<>();
					skuItem.put("canshuxm", paramsList.get(i).getParamsName());
					skuItem.put("canshuVal", itemJson.getString(paramsList.get(i).getParamsName()));
					skuMapList.add(skuItem);
				}
				item.put("skuList", skuMapList);
				item.put("price", sku.getPrice());
				item.put("stockNum", sku.getStockNum());
				item.put("oldPrice", sku.getOldPrice());
				item.put("imgUrl", sku.getImgUrl());
				item.put("skuId", sku.getId());
				mapList.add(item);
			}
			request.setAttribute("mapList", mapList);
		}
		return templatePath + "shop/seckill/goodsSet";
	}
	
	
	/**
	 * 保存一个秒杀活动的商品设置
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveSeckillGoodsSet")
	@ResponseBody
	public Map<String, Object> saveSeckillGoodsSet(HttpServletRequest request) {
		return service.saveSeckillGoodsSet(request);
	}
	
	/**
	 * 从秒杀活动中，移出一个商品
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteSeckillGoods")
	@ResponseBody
	public Map<String, Object> deleteSeckillGoods(HttpServletRequest request) {
		return service.deleteSeckillGoods(request);
	}
	
	
	/**
	 * 秒杀商品价格查看
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "goodsSetInfo")
	public String goodsSetInfo( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			
			SeckillGoodsMapEntity a = service.get(SeckillGoodsMapEntity.class, Long.valueOf(id));
			request.setAttribute("a", a); 
			GoodsEntity goods = service.get(GoodsEntity.class, a.getGoods().getId());
			SeckillActionEntity group = service.get(SeckillActionEntity.class, a.getSeckillAction().getId());
			request.setAttribute("goods", goods);
			request.setAttribute("group", group);
			
			String hql = "from GoodsParamsEntity as a where a.goods.id=" + goods.getId();
			List<GoodsParamsEntity> paramsList = service.findByQueryString(hql);
			request.setAttribute("paramsList", paramsList);
			
			hql = "from SeckillGoodsPriceSetEntity as a where  a.seckillGoodsMap.id=" + id;
			List<SeckillGoodsPriceSetEntity> priceList = service.findByQueryString(hql);
			request.setAttribute("priceList", priceList);
			
			hql = "from SeckillGoodsSkuPriceSetEntity as a where a.seckillGoodsMap.id=" + id;
			List<SeckillGoodsSkuPriceSetEntity> skuList = service.findByQueryString(hql);
			
			List<Map<String, Object>> mapList = new ArrayList<>();
			for(SeckillGoodsSkuPriceSetEntity sku : skuList) {
				Map<String,Object> item = new HashMap<>();
				String paramsValusJson = sku.getSku().getParmasValuesJson();
				JSONObject itemJson = JSONObject.fromObject(paramsValusJson);
				List<Map<String, Object>> skuMapList = new ArrayList<>();
				for(int i = 0; i < paramsList.size(); i++) {
					Map<String,Object> skuItem = new HashMap<>();
					skuItem.put("canshuxm", paramsList.get(i).getParamsName());
					skuItem.put("canshuVal", itemJson.getString(paramsList.get(i).getParamsName()));
					skuMapList.add(skuItem);
				}
				item.put("skuList", skuMapList);
				item.put("price", sku.getSku().getPrice());
				item.put("stockNum", sku.getSku().getStockNum());
				item.put("skuId", sku.getId());
				item.put("discount", sku.getDiscount());
				mapList.add(item);
			}
			request.setAttribute("mapList", mapList);
			
		} 
		return templatePath + "shop/seckill/goodsSetInfo";
	}
}
