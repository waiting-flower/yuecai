package modules.shop.order.service.impl;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.EShopUtil;
import common.util.HqlQuery;
import common.util.StringUtil;
import modules.pay.wx.util.ClientCustomSSL;
import modules.pay.wx.util.WxPayConfig;
import modules.shop.action.entity.ActionGiftEntity;
import modules.shop.driver.entity.DriverEntity;
import modules.shop.gift.entity.GiftEntity;
import modules.shop.goods.entity.ConfigEntity;
import modules.shop.goods.entity.GoodsEntity;
import modules.shop.goods.entity.GoodsSkuEntity;
import modules.shop.order.entity.OrderActionEntity;
import modules.shop.order.entity.OrderDetailEntity;
import modules.shop.order.entity.OrderEntity;
import modules.shop.order.service.OrderService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderServiceImpl extends CommonServiceImpl implements OrderService {
    @Override
    public DataTableReturn loadOrderByPage(JQueryDataTables dataTable, HttpServletRequest request) {
        StringBuffer hql = new StringBuffer(
                " from OrderEntity as  a where a.status = '1' and a.isSalesAfter = '0' ");
        List<Object> params = new ArrayList<Object>();
        String orderNo = request.getParameter("orderNo");
        if (StringUtil.isNotEmpty(orderNo)) {
            hql.append(" and a.orderNo like ? ");
            params.add("%" + orderNo + "%");
        }
        String orderState = request.getParameter("orderState");
        if (StringUtil.isNotEmpty(orderState)) {
            hql.append(" and a.orderState = ? ");
            params.add(orderState);
        }
        String userName = request.getParameter("userName");
        if (StringUtil.isNotEmpty(userName)) {
            hql.append(" and a.user.name like ? ");
            params.add("%" + userName + "%");
        }
        String mobile = request.getParameter("mobile");
        if (StringUtil.isNotEmpty(mobile)) {
            hql.append(" and a.mobile like ? ");
            params.add("%" + mobile + "%");
        }
        String userId = request.getParameter("userId");
        if (StringUtil.isNotEmpty(userId)) {
            hql.append(" and a.user.id = ? ");
            params.add(Long.valueOf(userId));
        }
        String driverId = request.getParameter("driverId");
        if (StringUtil.isNotEmpty(driverId)) {
            hql.append(" and a.driver.id = ? ");
            params.add(Long.valueOf(driverId));
        }


        String beginTime = request.getParameter("beginTime");
        if (StringUtil.isNotEmpty(beginTime)) {
            hql.append(" and a.createDate >= ? ");
            params.add(DateUtil.parse(beginTime, DateUtil.yyyyMMdd));
        }
        String endTime = request.getParameter("endTime");
        if (StringUtil.isNotEmpty(endTime)) {
            hql.append(" and a.createDate <= ? ");
            params.add(DateUtil.parse(endTime, DateUtil.yyyyMMdd));
        }

        String beginTime2 = request.getParameter("beginTime2");
        if (StringUtil.isNotEmpty(beginTime2)) {
            hql.append(" and a.createDate >= ? ");
            params.add(DateUtil.parse(beginTime2, DateUtil.yyyyMMddHHmmss));
        }
        String endTime2 = request.getParameter("endTime2");
        if (StringUtil.isNotEmpty(endTime2)) {
            hql.append(" and a.createDate <= ? ");
            params.add(DateUtil.parse(endTime2, DateUtil.yyyyMMddHHmmss));
        }

        hql.append(" order by  a.createDate desc");
        HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
        int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
        hqlQuery.setCurPage(currPage + 1);
        hqlQuery.setPageSize(dataTable.getDisplayLength());
        DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
        List<OrderEntity> pList = (List<OrderEntity>) d.getAaData();
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (OrderEntity p : pList) {
            Map<String, Object> item = new HashMap<String, Object>();
            item.put("id", p.getId());
			/*
		 * private TeamEntity team;//订单发货的团长
	private WUserEntity user;//订单下单的用户
	private String orderNo;//订单号码
	private Integer num;//商品的数量
	private Double money;//订单金额
	private String orderState;//订单状态  0待支付   1：待发货   2：已发货，团长未收货  3：团长已收货 待自提   4:已收货   -1：取消订单
	private String bak;//订单备注
	private String cancelReason;//订单取消原因 
	private String refoundNo;//退款单号
	private Date cancleTime;//订单取消时间
	private Date receiveTime;//用户订单接收时间
	private String isSalary;//是否计算了佣金了
	private String isRefund;//是否有售后服务，售后包含退款，退换货
	private Date refundTime;//提交退款的时间
	private Date handleTime;//处理退款完成时间
	private String isCanCancleOrder;//是否为可以取消的订单
	private String isSalesAfter;//是否有售后
	private String salesAfterReason;//售后的原因
	private String salesAfterInfo;//售后的说明
	private String salesAfterImgList;//售后的图片列表
	private String salesAfterState;//售后的状态  1：客服审核中 2：团长验货中  3：退货换货
		 */
            item.put("orderNo", p.getOrderNo());
            item.put("num", p.getNum());
            item.put("money", p.getMoney());
            item.put("orderState", p.getOrderState());
            item.put("nick", p.getUser().getNick());
            item.put("headUrl", p.getUser().getHeadUrl());
            item.put("address", p.getAddress());
            item.put("name", p.getName());
            item.put("mobile", p.getMobile());
            item.put("status", p.getStatus());
            item.put("driverName", "");
            if (p.getDriver() != null) {
                item.put("driverName", p.getDriver().getName() + p.getDriver().getMobile());
            }
            String tempHql = "from OrderDetailEntity as a where a.sOrder.id=" + p.getId();
            List<OrderDetailEntity> odList = findByQueryString(tempHql);
            String goodsName = "";
            for (OrderDetailEntity od : odList) {
                goodsName += od.getGoods().getName() + "<br>";
            }
            item.put("goodsName", goodsName);
            item.put("time", DateUtil.getTimeByCustomPattern(p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
            mapList.add(item);
        }
        d.setAaData(mapList);
        d.setsEcho(dataTable.getEcho());
        return d;
    }

    @Override
    public DataTableReturn loadSalesAfterOrderByPage(JQueryDataTables dataTable, HttpServletRequest request) {
        StringBuffer hql = new StringBuffer(
                " from OrderEntity as  a where 1 = 1  and a.status = '1' and a.isSalesAfter = '1' ");
        List<Object> params = new ArrayList<Object>();
        String orderNo = request.getParameter("orderNo");
        if (StringUtil.isNotEmpty(orderNo)) {
            hql.append(" and a.orderNo like ? ");
            params.add("%" + orderNo + "%");
        }
        String orderState = request.getParameter("orderState");
        if (StringUtil.isNotEmpty(orderState)) {
            hql.append(" and a.orderState = ? ");
            params.add(orderState);
        }
        String beginTime = request.getParameter("beginTime");
        if (StringUtil.isNotEmpty(beginTime)) {
            hql.append(" and a.createDate >= ? ");
            params.add(DateUtil.parse(beginTime, DateUtil.yyyyMMdd));
        }
        String endTime = request.getParameter("endTime");
        if (StringUtil.isNotEmpty(endTime)) {
            hql.append(" and a.createDate <= ? ");
            params.add(DateUtil.parse(endTime, DateUtil.yyyyMMdd));
        }

        hql.append(" order by  a.createDate desc");
        HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
        int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
        hqlQuery.setCurPage(currPage + 1);
        hqlQuery.setPageSize(dataTable.getDisplayLength());
        DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
        List<OrderEntity> pList = (List<OrderEntity>) d.getAaData();
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (OrderEntity p : pList) {
            Map<String, Object> item = new HashMap<String, Object>();
            item.put("id", p.getId());
			/*
		 * private TeamEntity team;//订单发货的团长
	private WUserEntity user;//订单下单的用户
	private String orderNo;//订单号码
	private Integer num;//商品的数量
	private Double money;//订单金额
	private String orderState;//订单状态  0待支付   1：待发货   2：已发货，团长未收货  3：团长已收货 待自提   4:已收货   -1：取消订单
	private String bak;//订单备注
	private String cancelReason;//订单取消原因 
	private String refoundNo;//退款单号
	private Date cancleTime;//订单取消时间
	private Date receiveTime;//用户订单接收时间
	private String isSalary;//是否计算了佣金了
	private String isRefund;//是否有售后服务，售后包含退款，退换货
	private Date refundTime;//提交退款的时间
	private Date handleTime;//处理退款完成时间
	private String isCanCancleOrder;//是否为可以取消的订单
	private String isSalesAfter;//是否有售后
	private String salesAfterReason;//售后的原因
	private String salesAfterInfo;//售后的说明
	private String salesAfterImgList;//售后的图片列表
	private String salesAfterState;//售后的状态  1：客服审核中 2：团长验货中  3：退货换货
		 */
            item.put("orderNo", p.getOrderNo());
            item.put("num", p.getNum());
            item.put("money", p.getMoney());
            item.put("orderState", p.getOrderState());
            item.put("nick", p.getUser().getNick());
            item.put("headUrl", p.getUser().getHeadUrl());
            item.put("address", p.getAddress());
            item.put("status", p.getStatus());
            item.put("salesAfterState", p.getSalesAfterState());
            item.put("applyTime", DateUtil.getTimeByCustomPattern(p.getApplySalesAfterTime(), DateUtil.yyyyMMddHHmmss));
            String tempHql = "from OrderDetailEntity as a where a.sOrder.id=" + p.getId();
            List<OrderDetailEntity> odList = findByQueryString(tempHql);
            String goodsName = "";
            for (OrderDetailEntity od : odList) {
                goodsName += od.getGoods().getName() + "<br>";
            }
            item.put("goodsName", goodsName);
            item.put("time", DateUtil.getTimeByCustomPattern(p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
            mapList.add(item);
        }
        d.setAaData(mapList);
        d.setsEcho(dataTable.getEcho());
        return d;
    }

    @Override
    public Map<String, Object> saveSalesAfterOrderCheck(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String id = request.getParameter("id");
        String state = request.getParameter("state");
        OrderEntity order = get(OrderEntity.class, Long.valueOf(id));
        if ("1".equals(state)) {
            //审核通过
            order.setSalesAfterState("2");
            saveOrUpdate(order);
        } else {
            //审核不通过
            order.setIsSalesAfter("0");
            saveOrUpdate(order);
        }
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> saveSalesAfterOrderResult(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String id = request.getParameter("id");
        String result = request.getParameter("result");
        OrderEntity order = get(OrderEntity.class, Long.valueOf(id));
        order.setSalesAfterHandleBak(result);
        saveOrUpdate(order);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> saveOrderSalesRefund(HttpServletRequest request) throws Exception {
        Map<String, Object> map = new HashMap<>();
        String id = request.getParameter("id");
        String refoundMoney = request.getParameter("refoundMoney");
        OrderDetailEntity orderDetail = get(OrderDetailEntity.class, Long.valueOf(id));

        OrderEntity order = orderDetail.getsOrder();
        if (StringUtil.isNotEmpty(orderDetail.getRefundNo())) {
            orderDetail.setRefundNo(EShopUtil.getOrderNo());
        }


        ConfigEntity config = get(ConfigEntity.class, 1l);
        Map<String, String> params = new HashMap<String, String>();
        params.put("appid", config.getAppid());
        params.put("mch_id", config.getMchId());
        params.put("nonce_str", WxPayConfig.generateNonceStr());
        params.put("out_trade_no", order.getOrderNo());// 订单号码
        int refondFee = (int) (Double.valueOf(refoundMoney) * 100);
        int allFee = (int) (order.getMoney() * 100);
        params.put("total_fee", String.valueOf(allFee));
        params.put("refund_fee", String.valueOf(refondFee));
        params.put("refund_desc", order.getSalesAfterReason());
        params.put("out_refund_no", orderDetail.getRefundNo());

        String signstr = WxPayConfig.generateSignature(params, config.getMchSecret());
        params.put("sign", signstr);// 通过签名算法计算得出的签名值，详见签名生成算法
        String str = WxPayConfig.mapToXml(params);
        System.out.println(str);
        String resultStr = ClientCustomSSL.doRefund(config.getAppid(), WxPayConfig.WX_REFUND_URL, str, config.getCertPath());
        Map<String, String> resultMap = WxPayConfig.xmlToMap(resultStr);
        if (resultMap.get("return_code").equals("SUCCESS")) {
            if (order.getRefundMoney() == null) order.setRefundMoney(0d);
            order.setRefundMoney(order.getRefundMoney() + Double.valueOf(refoundMoney));
            saveOrUpdate(order);
        }
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> saveOrderRefund(HttpServletRequest request) throws Exception {
        Map<String, Object> map = new HashMap<>();
        String id = request.getParameter("id");
        String refoundMoney = request.getParameter("refoundMoney");
        OrderEntity order = get(OrderEntity.class, Long.valueOf(id));
        order.setRefoundNo(EShopUtil.getOrderNo());

        ConfigEntity config = get(ConfigEntity.class, 1l);
        Map<String, String> params = new HashMap<String, String>();
        params.put("appid", config.getAppid());
        params.put("mch_id", config.getMchId());
        params.put("nonce_str", WxPayConfig.generateNonceStr());
        params.put("out_trade_no", order.getOrderNo());// 订单号码
        int refondFee = (int) (Double.valueOf(refoundMoney) * 100);
        int allFee = (int) (order.getMoney() * 100);
        params.put("total_fee", String.valueOf(allFee));
        params.put("refund_fee", String.valueOf(refondFee));
        params.put("refund_desc", order.getSalesAfterReason());
        params.put("out_refund_no", order.getRefoundNo());

        String signstr = WxPayConfig.generateSignature(params, config.getMchSecret());
        params.put("sign", signstr);// 通过签名算法计算得出的签名值，详见签名生成算法
        String str = WxPayConfig.mapToXml(params);
        System.out.println(str);
        String resultStr = ClientCustomSSL.doRefund(config.getAppid(), WxPayConfig.WX_REFUND_URL, str, config.getCertPath());
        Map<String, String> resultMap = WxPayConfig.xmlToMap(resultStr);
        if (resultMap.get("return_code").equals("SUCCESS")) {
            if (order.getRefundMoney() == null) order.setRefundMoney(0d);
            order.setRefundMoney(order.getRefundMoney() + Double.valueOf(refoundMoney));
            saveOrUpdate(order);
        }
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> saveOrderCancle(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String id = request.getParameter("id");
        OrderEntity order = get(OrderEntity.class, Long.valueOf(id));
        order.setOrderState("-1");
        order.setCancleTime(DateUtil.getCurrentTime());
        order.setCancelReason("后台关闭");
        saveOrUpdate(order);

        String hql = "from OrderDetailEntity as a where a.sOrder.id=" + id;
        List<OrderDetailEntity> odList = findByQueryString(hql);
        for (OrderDetailEntity od : odList) {
            GoodsSkuEntity sku = od.getSku();
            GoodsEntity goods = od.getGoods();
            goods = get(GoodsEntity.class, goods.getId());
            if (sku != null) {
                sku = get(GoodsSkuEntity.class, sku.getId());
                sku.setStockNum(sku.getStockNum() + od.getNum());
                save(sku);
            }
            goods.setStockNum(goods.getStockNum() + od.getNum());
            save(goods);
        }

        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> saveSalesAfterOrderComplete(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String id = request.getParameter("id");
        String result = request.getParameter("result");
        OrderEntity order = get(OrderEntity.class, Long.valueOf(id));
        order.setSalesAfterHandleBak(result);
        order.setSalesAfterState("4");
        saveOrUpdate(order);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> saveOrderFendan(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String id = request.getParameter("id");
        String driverId = request.getParameter("driverId");
        DriverEntity driver = get(DriverEntity.class, Long.valueOf(driverId));
        OrderEntity order = get(OrderEntity.class, Long.valueOf(id));
        order.setDriver(driver);
        order.setOrderState("2");
        order.setIsFendan("1");
        order.setFendanDay(DateUtil.getCurrentTimeByDefaultPattern());
        saveOrUpdate(order);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> saveOrderFendanRemove(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String id = request.getParameter("id");
        OrderEntity order = get(OrderEntity.class, Long.valueOf(id));
        order.setDriver(null);
        order.setOrderState("1");
        order.setIsFendan("0");
        saveOrUpdate(order);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> getNowAllDaifahuoOrderList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        StringBuffer hql = new StringBuffer(
                " from OrderEntity as  a where 1 = 1  and a.status = '1' and a.isSalesAfter = '0' and a.orderState = '1'");
        String beginTime2 = request.getParameter("beginTime");
        List<Object> params = new ArrayList<Object>();
        if (StringUtil.isNotEmpty(beginTime2)) {
            hql.append(" and a.createDate >= ? ");
            params.add(DateUtil.parse(beginTime2, DateUtil.yyyyMMddHHmmss));
        }
        String endTime2 = request.getParameter("endTime");
        if (StringUtil.isNotEmpty(endTime2)) {
            hql.append(" and a.createDate <= ? ");
            params.add(DateUtil.parse(endTime2, DateUtil.yyyyMMddHHmmss));
        }
        List<OrderEntity> orderList = findHql(hql.toString(), params.toArray());
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (OrderEntity p : orderList) {
            Map<String, Object> item = new HashMap<String, Object>();
            item.put("id", p.getId());
            item.put("orderNo", p.getOrderNo());
            item.put("num", p.getNum());
            item.put("money", p.getMoney());
            item.put("orderState", p.getOrderState());
            item.put("nick", p.getUser().getNick());
            item.put("headUrl", p.getUser().getHeadUrl());
            item.put("address", p.getAddress());
            item.put("name", p.getName());
            item.put("mobile", p.getMobile());
            item.put("status", p.getStatus());
            String tempHql = "from OrderDetailEntity as a where a.sOrder.id=" + p.getId();
            List<OrderDetailEntity> odList = findByQueryString(tempHql);
            String goodsName = "";
            for (OrderDetailEntity od : odList) {
                goodsName += od.getGoods().getName() + "<br>";
            }
            item.put("goodsName", goodsName);
            item.put("time", DateUtil.getTimeByCustomPattern(p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
            mapList.add(item);
        }
        map.put("list", mapList);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> getDriverOrderList(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String day = DateUtil.getCurrentTimeByDefaultPattern();
        String driverId = request.getParameter("driverId");
        StringBuffer hql = new StringBuffer(
                " from OrderEntity as  a where 1 = 1 and a.driver.id="
                        + driverId + "  and a.status = '1' and a.isSalesAfter = '0' and a.fendanDay = '"
                        + day + "'");
        List<OrderEntity> orderList = findByQueryString(hql.toString());
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (OrderEntity p : orderList) {
            Map<String, Object> item = new HashMap<String, Object>();
            item.put("id", p.getId());
            item.put("orderNo", p.getOrderNo());
            item.put("num", p.getNum());
            item.put("money", p.getMoney());
            item.put("orderState", p.getOrderState());
            item.put("nick", p.getUser().getNick());
            item.put("headUrl", p.getUser().getHeadUrl());
            item.put("address", p.getAddress());
            item.put("name", p.getName());
            item.put("mobile", p.getMobile());
            item.put("status", p.getStatus());
            String tempHql = "from OrderDetailEntity as a where a.sOrder.id=" + p.getId();
            List<OrderDetailEntity> odList = findByQueryString(tempHql);
            String goodsName = "";
            for (OrderDetailEntity od : odList) {
                goodsName += od.getGoods().getName() + "<br>";
            }
            item.put("goodsName", goodsName);
            item.put("time", DateUtil.getTimeByCustomPattern(p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
            mapList.add(item);
        }
        map.put("list", mapList);
        map.put("code", 0);
        return map;
    }

    @Override
    public void initPeisongdan(HttpServletRequest request, String id) {
        String date = DateUtil.getCurrentTimeByDefaultPattern();
        String hql = " from OrderEntity as a where a.fendanDay = '" + date + "' and a.driver.id=" + id;
        List<OrderEntity> orderList = findByQueryString(hql);//获取全部的订单
        request.setAttribute("orderList", orderList);
        List<Map<String, Object>> mapList = new ArrayList<>();
        for (OrderEntity order : orderList) {
            Map<String, Object> item = new HashMap<>();
            item.put("a", order);
            //得到该订单的赠品
            String gift = getGiftByOrderId(order.getId());
            item.put("gift", gift);
            String tempHql = "from OrderDetailEntity as a where a.sOrder.id=" + order.getId();
            List<OrderDetailEntity> odList = findByQueryString(tempHql);
            item.put("odList", odList);
            mapList.add(item);
        }
        request.setAttribute("mapList", mapList);
        request.setAttribute("id", id);
//		
//		Set<WUserEntity> userSet = new HashSet<>();
//		for(OrderEntity order : orderList) {
//			userSet.add(order.getUser());//全部的用户对象
//		}
//		for(WUserEntity user : userSet) {
//			int num = 0;
//			for(OrderEntity order : orderList) {
//				if(order.getUser().getId().equals(user.getId())) {
//					num = num + 1;
//				}
//			}
//			Map<String, Object> item = new HashMap<>();
//			item.put("user", user.getName());
//			item.put("mobile", user.getMobile());
//			item.put("num", num);
//			for(OrderEntity order : orderList) {
//				item.put("address", order.getAddress());
//				item.put("name", order.getName());
//				item.put("goodsNum", order.getNum());
//				item.put("money", order.getMoney());
//				String tempHql = "from OrderDetailEntity as a where a.sOrder.id=" + order.getId() + " and a.status='1'";
//				List<OrderDetailEntity> odList = findByQueryString(tempHql);
//				List<Map<String, Object>> childList = new ArrayList<>();
//				for(OrderDetailEntity od : odList) {
//					Map<String, Object> childMap = new HashMap<>();
//					childMap.put("goodsName", od.getGoods().getName());
//					childMap.put("goodsNum", od.getNum());
//					childList.add(childMap);
//				}
//			}
//			
//		}

    }

    @Override
    public void initFenjiandan(HttpServletRequest request, String id) {

        String date = DateUtil.getCurrentTimeByDefaultPattern();
        String gsql = "SELECT sum(saleNum),giftId from shop_order_action"
                + " where EXISTS(select 1 from shop_order b"
                + " where b.id = orderId and b.driverId=" + id
                + " and b.fendanDay = '" + date + "') GROUP BY giftId";
        List<Object[]> giftList = commonDao.findListbySql(gsql);
        List<Map<String, Object>> giftMapList = new ArrayList<>();


        String sql = "SELECT sum(num),goodsId,skuId,Id from shop_order_detail"
                + " where EXISTS(select 1 from shop_order b"
                + " where b.id = orderId and b.driverId=" + id
                + " and b.fendanDay = '" + date + "') GROUP BY goodsId ,skuId";
        List<Object[]> objList = commonDao.findListbySql(sql);
        List<Map<String, Object>> mapList = new ArrayList<>();

        for (Object[] objs : objList) {
            Map<String, Object> item = new HashMap<>();
            BigInteger goodsId = (BigInteger) objs[1];
            BigInteger skuId = (BigInteger) objs[2];
            BigDecimal num = (BigDecimal) objs[0];
            BigInteger orderId = (BigInteger) objs[3];
            OrderDetailEntity order = get(OrderDetailEntity.class, orderId.longValue());
            GoodsEntity goods = get(GoodsEntity.class, goodsId.longValue());
            item.put("name", goods.getName());
            item.put("sku", "");
            item.put("price", order.getPrice());
            item.put("money", num.intValue() * order.getPrice());
            if (skuId != null) {
                GoodsSkuEntity sku = get(GoodsSkuEntity.class, skuId.longValue());
                item.put("sku", sku.getParmasValuesJson().substring(sku.getParmasValuesJson().indexOf("单位") + 5, sku.getParmasValuesJson().length() - 2));
                item.put("price", order.getPrice());
                item.put("money", num.intValue() * order.getPrice());
                item.put("unit", sku.getUnit());
            }
            item.put("num", num.intValue());
            mapList.add(item);
        }

        for (Object[] objs : giftList) {
            Map<String, Object> item = new HashMap<>();
            BigDecimal num = (BigDecimal) objs[0];
            BigInteger giftId = (BigInteger) objs[1];
            GiftEntity gifts = get(GiftEntity.class, giftId.longValue());
            item.put("name", gifts.getName());
            item.put("sku", "");
            item.put("price", gifts.getMoney());
            item.put("money", num.intValue() * gifts.getMoney());
            item.put("num", num.intValue());
            giftMapList.add(item);
        }
        request.setAttribute("mapList", mapList);
        request.setAttribute("giftMapList", giftMapList);
    }

    @Override
    public void initDayindan(HttpServletRequest request) {
        String id = request.getParameter("id");
        String date = DateUtil.getCurrentTimeByDefaultPattern();
        String hql = " from OrderEntity as a where a.fendanDay = '" + date + "' and a.driver.id=" + id;
        List<OrderEntity> orderList = findByQueryString(hql);//获取全部的订单
        request.setAttribute("orderList", orderList);
        List<Map<String, Object>> mapList = new ArrayList<>();
        for (OrderEntity order : orderList) {
            Map<String, Object> item = new HashMap<>();
            item.put("a", order);
            String tempHql = "from OrderDetailEntity as a where a.sOrder.id=" + order.getId();
            //获得订单的赠品
            String gift = getGiftByOrderId(order.getId());
            item.put("gift", StringUtil.isEmpty(gift) ? "" : "【赠品：" + gift + "】");
            List<OrderDetailEntity> odList = findByQueryString(tempHql);
            item.put("odList", odList);

            //获得打印三联单相关信息
            int count = 5;//每联产品数
            Integer odPage = (odList.size() + count - 1) / count;
            item.put("odPage", odPage);
            if (odPage > 1) {
                List<Map<String, Object>> odPageInfo = new ArrayList<>();
                for (int i = 1; i <= odPage; i++) {
                    Map<String, Object> map = new HashMap<>();
                    int begin = count * (i - 1);
                    int end = count * i - 1;
                    if (end > odList.size() - 1) {
                        end = odList.size() - 1;
                    }
                    map.put("begin", begin);
                    map.put("end", end);
                    int num = 0;
                    Double money = 0.0;
                    for (int j = begin; j <= end; j++) {
                        num += odList.get(j).getNum();
                        money += odList.get(j).getMoney();
                    }
                    map.put("num", num);//每联产品数量之和
                    map.put("money", money);//每联产品货款之和
                    map.put("couponMoney", "0.0");//产品优惠金额
                    if (i == odPage) {
                        map.put("couponMoney", order.getCouponMoney());
                    }
                    odPageInfo.add(map);
                }
                item.put("odPageInfo", odPageInfo);
            }

            mapList.add(item);
        }
        request.setAttribute("mapList", mapList);
        request.setAttribute("id", id);
    }

    //根据订单id获得该订单的赠品
    @Override
	/*public String getGiftByOrderId(Long orderId) {
		String gift = "";
		String hql = "from ActionGiftEntity as a where 1=1 and exists(select 1 from OrderActionEntity b where a.action.id = b.action.id and b.order.id="+orderId+")";
		List<ActionGiftEntity> oaList = findByQueryString(hql);
		if(oaList!=null&&oaList.size()>0){
			for (ActionGiftEntity actionGiftEntity : oaList) {
				gift = gift + actionGiftEntity.getGift().getName()+" 1,";
			}
		}
		if(StringUtil.isNotEmpty(gift)){
			gift = StringUtil.substringBeforeLast(gift,",");
		}
		return gift;
	}*/
    //根据订单id获得该订单的赠品
    public String getGiftByOrderId(Long orderId) {
        String gift = "";
        String sqlg = "select giftId,COUNT(saleNum) as allnum from shop_order_action where orderId = " + orderId +" group by giftId";
        //String hql = "from OrderActionEntity as a where exists(select 1 from OrderEntity as b where a.order.id = b.id and a.order.id=" + orderId + ")";
        //sql = "from OrderActionEntity as a INNER JOIN OrderEntity as b ON a.order.id = b.id and a.order.id = "+ orderId +"";
        List<Object[]> oaList = commonDao.findListbySql(sqlg);
        //List<OrderActionEntity> oaList = findByQueryString(hql);
        if (oaList != null && oaList.size() > 0) {
            for (Object[] oa : oaList) {
                BigInteger giftid = (BigInteger)oa[0];
                GiftEntity giftlists = get(GiftEntity.class, giftid.longValue());
                //for (Object[] gf : giftList) {
                gift = gift + giftlists.getName() + " *" + oa[1]  + ",";
            }
        }
        if (StringUtil.isNotEmpty(gift)) {
            gift = StringUtil.substringBeforeLast(gift, ",");
        }
        return gift;
    }

    //根据订单id获得该订单的赠品列表
    public List getGiftListByOrderId(Long orderId) {
        String gift = "";
        String sql = "SELECT sum(saleNum),giftId,Id from shop_order_action"
                + " where orderId = " + orderId + "  GROUP BY giftId";
        List<Object[]> objList = commonDao.findListbySql(sql);
        List<Map<String, Object>> mapList = new ArrayList<>();
        if (objList != null && objList.size() > 0) {
            for (Object[] objs : objList) {
                Map<String, Object> item = new HashMap<>();
                BigDecimal num = (BigDecimal) objs[0];
                BigInteger giftId = (BigInteger) objs[1];
                BigInteger Id = (BigInteger) objs[2];
                GiftEntity gifts = get(GiftEntity.class, giftId.longValue());
                item.put("name", gifts.getName());
                item.put("sku", "");
                item.put("price", gifts.getMoney());
                item.put("money", "");
                item.put("num", num.intValue());
                mapList.add(item);
            }
        }
        return mapList;
    }

}
