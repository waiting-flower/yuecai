package modules.shop.order.service;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface OrderService extends CommonService{
	DataTableReturn loadOrderByPage(JQueryDataTables dataTable, HttpServletRequest request);

	DataTableReturn loadSalesAfterOrderByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveSalesAfterOrderCheck(HttpServletRequest request);

	Map<String, Object> saveSalesAfterOrderResult(HttpServletRequest request);

	Map<String, Object> saveOrderSalesRefund(HttpServletRequest request) throws Exception;

	Map<String, Object> saveOrderRefund(HttpServletRequest request) throws Exception;

	Map<String, Object> saveOrderCancle(HttpServletRequest request);

	Map<String, Object> saveSalesAfterOrderComplete(HttpServletRequest request);

	Map<String, Object> saveOrderFendan(HttpServletRequest request);

	Map<String, Object> saveOrderFendanRemove(HttpServletRequest request);

	Map<String, Object> getNowAllDaifahuoOrderList(HttpServletRequest request);

	Map<String, Object> getDriverOrderList(HttpServletRequest request);

	void initPeisongdan(HttpServletRequest request, String id);

	void initFenjiandan(HttpServletRequest request,String id);

	void initDayindan(HttpServletRequest request);

    String getGiftByOrderId(Long orderId);
}
