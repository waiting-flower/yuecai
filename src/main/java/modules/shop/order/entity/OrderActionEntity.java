package modules.shop.order.entity;

import common.entity.CommonEntity;
import modules.shop.action.entity.ActionEntity;
import modules.shop.gift.entity.GiftEntity;

import javax.persistence.*;

/**
 * 订单满赠情况表
 * @Author zhaoyingge
 * @Date 2019/4/26 17:03
 **/
@Entity
@Table(name = "shop_order_action", schema = "")
public class OrderActionEntity extends CommonEntity {
    private OrderEntity order;
    private ActionEntity action;
    private GiftEntity gift;

    private Integer saleNum; //赠送数量

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="orderId")
    public OrderEntity getOrder() {
        return order;
    }

    public void setOrder(OrderEntity order) {
        this.order = order;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="actionId")
    public ActionEntity getAction() {
        return action;
    }

    public void setAction(ActionEntity action) {
        this.action = action;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="giftId")
    public GiftEntity getGift() {
        return gift;
    }
    public void setGift(GiftEntity gift) {
        this.gift = gift;
    }

    public Integer getSaleNum() {
        return saleNum;
    }
    public void setSaleNum(Integer saleNum) {
        this.saleNum = saleNum;
    }
}
