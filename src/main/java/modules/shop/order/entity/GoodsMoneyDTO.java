package modules.shop.order.entity;

/**
 * @Author zhaoyingge
 * @Date 2019/4/27 9:07
 **/
public class GoodsMoneyDTO {
    private Long goodsId;
    private Double money;

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }
}
