package modules.shop.order.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
import modules.shop.goods.entity.GoodsEntity;
import modules.shop.goods.entity.GoodsSkuEntity;

/**
 * 订单详情数据
 * @author Administrator
 *
 */
@Entity 
@Table(name = "shop_order_detail", schema = "")
public class OrderDetailEntity extends CommonEntity{
	private OrderEntity sOrder;
	private GoodsEntity goods;
	private GoodsSkuEntity sku;
	private Integer num;
	private Double money;
	private Double price;
	private Double refoundMoney;//退款金额
	private String refundNo;
	private String isRefund;//是否有售后服务
	private Date refundTime;//提交售后的时间
	private Date handleTime;//处理完成时间
	private Long postId;
	private Date  postTime;
	private String isPost;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orderId")
	public OrderEntity getsOrder() {
		return sOrder;
	}
	public void setsOrder(OrderEntity sOrder) {
		this.sOrder = sOrder;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "goodsId")
	public GoodsEntity getGoods() {
		return goods;
	}
	public void setGoods(GoodsEntity goods) {
		this.goods = goods;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "skuId")
	public GoodsSkuEntity getSku() {
		return sku;
	}
	public void setSku(GoodsSkuEntity sku) {
		this.sku = sku;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getRefoundMoney() {
		return refoundMoney;
	}
	public void setRefoundMoney(Double refoundMoney) {
		this.refoundMoney = refoundMoney;
	}
	public String getIsRefund() {
		return isRefund;
	}
	public void setIsRefund(String isRefund) {
		this.isRefund = isRefund;
	}
	public Date getRefundTime() {
		return refundTime;
	}
	public void setRefundTime(Date refundTime) {
		this.refundTime = refundTime;
	}
	public Date getHandleTime() {
		return handleTime;
	}
	public void setHandleTime(Date handleTime) {
		this.handleTime = handleTime;
	}
	public String getRefundNo() {
		return refundNo;
	}
	public void setRefundNo(String refundNo) {
		this.refundNo = refundNo;
	}
	public Long getPostId() {
		return postId;
	}
	public void setPostId(Long postId) {
		this.postId = postId;
	}
	public Date getPostTime() {
		return postTime;
	}
	public void setPostTime(Date postTime) {
		this.postTime = postTime;
	}
	public String getIsPost() {
		return isPost;
	}
	public void setIsPost(String isPost) {
		this.isPost = isPost;
	}
	
	
}
