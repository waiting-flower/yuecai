package modules.shop.order.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
import modules.shop.driver.entity.DriverEntity;
import modules.shop.user.entity.WUserEntity;
/**
 * 订单对象
 * @author Administrator
 *
 */
@Entity
@Table(name = "shop_order", schema = "")
public class OrderEntity extends CommonEntity{
	private WUserEntity user;//订单下单的用户
	private String name;//姓名
	private String mobile;//手机号码
	private String address;//地址 
	private String orderNo;//订单号码
	private Integer num;//商品的数量
	private Double money;//订单金额
	private Double couponMoney;//优惠金额
	private String payType;//付款方式 ：0：货到付款  wx:微信  zfb：支付宝
	private String isFendan;//是否分单了
	private String fendanDay;//分单的日期
	private DriverEntity driver;
	private String orderState;//订单状态  0待支付   1：待发货   2：已发货  3：已收货，待评价   4:已完成   -1：取消订单  5：线下支付，待确认
	private String bak;//订单备注
	private String cancelReason;//订单取消原因 
	private Date sendTime;//订单发货时间
	private String refoundNo;//退款单号
	private Date cancleTime;//订单取消时间
	private Date receiveTime;//用户订单接收时间
	private String isRefund;//是否有售后服务，售后包含退款，退换货  0无退款
	private Double refundMoney;//退款金额
	private Date refundTime;//提交退款的时间
	private Date handleTime;//处理退款完成时间
	private String isSalesAfter;//是否有售后   0:无售后
	private String salesAfterReason;//售后的原因
	private String salesAfterInfo;//售后的说明
	private String salesAfterImgList;//售后的图片列表
	private String salesAfterState;//售后的状态  0：待处理 1：已完成
	private Date applySalesAfterTime;//申请售后的时间
	private String salesAfterHandleBak;//售后处理结果
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public WUserEntity getUser() {
		return user;
	}
	public void setUser(WUserEntity user) {
		this.user = user;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	public String getOrderState() {
		return orderState;
	}
	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}
	public String getBak() {
		return bak;
	}
	public void setBak(String bak) {
		this.bak = bak;
	}
	public String getCancelReason() {
		return cancelReason;
	}
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	public String getRefoundNo() {
		return refoundNo;
	}
	public void setRefoundNo(String refoundNo) {
		this.refoundNo = refoundNo;
	}
	public Date getCancleTime() {
		return cancleTime;
	}
	public void setCancleTime(Date cancleTime) {
		this.cancleTime = cancleTime;
	}
	public Date getReceiveTime() {
		return receiveTime;
	}
	public void setReceiveTime(Date receiveTime) {
		this.receiveTime = receiveTime;
	}
	public String getIsRefund() {
		return isRefund;
	}
	public void setIsRefund(String isRefund) {
		this.isRefund = isRefund;
	}
	public Double getRefundMoney() {
		return refundMoney;
	}
	public void setRefundMoney(Double refundMoney) {
		this.refundMoney = refundMoney;
	}
	public Date getRefundTime() {
		return refundTime;
	}
	public void setRefundTime(Date refundTime) {
		this.refundTime = refundTime;
	}
	public Date getHandleTime() {
		return handleTime;
	}
	public void setHandleTime(Date handleTime) {
		this.handleTime = handleTime;
	}
	public String getIsSalesAfter() {
		return isSalesAfter;
	}
	public void setIsSalesAfter(String isSalesAfter) {
		this.isSalesAfter = isSalesAfter;
	}
	public String getSalesAfterReason() {
		return salesAfterReason;
	}
	public void setSalesAfterReason(String salesAfterReason) {
		this.salesAfterReason = salesAfterReason;
	}
	public String getSalesAfterInfo() {
		return salesAfterInfo;
	}
	public void setSalesAfterInfo(String salesAfterInfo) {
		this.salesAfterInfo = salesAfterInfo;
	}
	public String getSalesAfterImgList() {
		return salesAfterImgList;
	}
	public void setSalesAfterImgList(String salesAfterImgList) {
		this.salesAfterImgList = salesAfterImgList;
	}
	public String getSalesAfterState() {
		return salesAfterState;
	}
	public void setSalesAfterState(String salesAfterState) {
		this.salesAfterState = salesAfterState;
	}
	public Date getApplySalesAfterTime() {
		return applySalesAfterTime;
	}
	public void setApplySalesAfterTime(Date applySalesAfterTime) {
		this.applySalesAfterTime = applySalesAfterTime;
	}
	public String getSalesAfterHandleBak() {
		return salesAfterHandleBak;
	}
	public void setSalesAfterHandleBak(String salesAfterHandleBak) {
		this.salesAfterHandleBak = salesAfterHandleBak;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getIsFendan() {
		return isFendan;
	}
	public void setIsFendan(String isFendan) {
		this.isFendan = isFendan;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "driverId")
	public DriverEntity getDriver() {
		return driver;
	}
	public void setDriver(DriverEntity driver) {
		this.driver = driver;
	}
	public Double getCouponMoney() {
		return couponMoney;
	}
	public void setCouponMoney(Double couponMoney) {
		this.couponMoney = couponMoney;
	}
	public String getFendanDay() {
		return fendanDay;
	}
	public void setFendanDay(String fendanDay) {
		this.fendanDay = fendanDay;
	}
	
	
}
