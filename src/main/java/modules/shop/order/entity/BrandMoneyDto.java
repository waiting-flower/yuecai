package modules.shop.order.entity;

/**
 * @Author zhaoyingge
 * @Date 2019/4/27 9:13
 **/
public class BrandMoneyDto {
    private Long brandId;
    private Double money;

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }
}
