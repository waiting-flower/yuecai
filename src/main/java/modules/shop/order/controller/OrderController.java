package modules.shop.order.controller;

import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.util.StringUtil;
import modules.shop.action.entity.ActionGiftEntity;
import modules.shop.driver.entity.DriverEntity;
import modules.shop.order.entity.OrderActionEntity;
import modules.shop.order.entity.OrderDetailEntity;
import modules.shop.order.entity.OrderEntity;
import modules.shop.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 订单管理web控制层
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "admin/shop/order")
public class OrderController extends BaseController{
	
	@Autowired
	OrderService service;

	/**
	 * 订单管理列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list")
	public String orderList( HttpServletRequest request, HttpServletResponse response) {
		return templatePath + "shop/order/orderList";
	}
	
	/**
	 * 订单详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "info")
	public String info( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			OrderEntity menu = service.get(OrderEntity.class, Long.valueOf(id));
			String hql = "from OrderDetailEntity as a where a.sOrder.id=" + id;
			List<OrderDetailEntity> detailList = service.findByQueryString(hql);
			request.setAttribute("detailList", detailList);
			request.setAttribute("a", menu);
			//获得订单的赠品
			request.setAttribute("gift", service.getGiftByOrderId(Long.valueOf(id)));
		}
		return templatePath + "shop/order/orderInfo";
	}
	
	
	/**
	 * 分页获取订单列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadOrderByPage")
	@ResponseBody
	public DataTableReturn loadOrderByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = service.loadOrderByPage(dataTable, request);
		return d;
	}
	
	
	
	/**
	 * 订单分单，同时发货
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveOrderFendan")
	@ResponseBody
	public Map<String, Object> saveOrderFendan(HttpServletRequest request) {
		return service.saveOrderFendan(request);
	}
	
	/**
	 * 移除一个分单
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveOrderFendanRemove")
	@ResponseBody
	public Map<String, Object> saveOrderFendanRemove(HttpServletRequest request) {
		return service.saveOrderFendanRemove(request);
	}
	
	/**
	 * 售后订单管理列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "salesAfterList")
	public String salesAfterList( HttpServletRequest request, HttpServletResponse response) {
		return templatePath + "shop/order/salesAfterList";
	}
	
	
	/**
	 * 分页获取售后订单列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadSalesAfterOrderByPage")
	@ResponseBody
	public DataTableReturn loadSalesAfterOrderByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = service.loadSalesAfterOrderByPage(dataTable, request);
		return d;
	}
	
	/**
	 * 售后订单详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "salesAfterInfo")
	public String salesAfterInfo( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			OrderEntity menu = service.get(OrderEntity.class, Long.valueOf(id));
			String hql = "from OrderDetailEntity as a where a.sOrder.id=" + id;
			List<OrderDetailEntity> detailList = service.findByQueryString(hql);
			request.setAttribute("detailList", detailList);
			request.setAttribute("a", menu);
		} 
		return templatePath + "shop/order/salesAfterInfo";
	}
	
	
	/**
	 * 保存一个售后订单的审核
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveSalesAfterOrderCheck")
	@ResponseBody
	public Map<String, Object> saveSalesAfterOrderCheck(HttpServletRequest request) {
		return service.saveSalesAfterOrderCheck(request);
	}
	
	/**
	 * 保存一个售后订单的操作处理结果
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveSalesAfterOrderResult")
	@ResponseBody
	public Map<String, Object> saveSalesAfterOrderResult(HttpServletRequest request) {
		return service.saveSalesAfterOrderResult(request);
	}
	
	
	/**
	 * 保存一个售后订单的审核
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveOrderSalesRefund")
	@ResponseBody
	public Map<String, Object> saveOrderSalesRefund(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		try {
			return service.saveOrderSalesRefund(request);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("code", 1);
			map.put("msg", "退款失败");
			return map;
		}
	}
	/**
	 * 整个订单的退款
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveOrderRefund")
	@ResponseBody
	public Map<String, Object> saveOrderRefund(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		try {
			return service.saveOrderRefund(request);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("code", 1);
			map.put("msg", "退款失败");
			return map;
		}
	}
	
	/**
	 * 主动关闭订单
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveOrderCancle")
	@ResponseBody
	public Map<String, Object> saveOrderCancle(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		try {
			return service.saveOrderCancle(request);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("code", 1);
			map.put("msg", "关闭失败");
			return map;
		}
	}
	
	/**
	 * 完成了售后了
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveSalesAfterOrderComplete")
	@ResponseBody
	public Map<String, Object> saveSalesAfterOrderComplete(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		try {
			return service.saveSalesAfterOrderComplete(request);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("code", 1);
			map.put("msg", "操作失败");
			return map;
		}
	}
	
	
	
	/**
	 *  分单管理
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "fendan")
	public String fendan( HttpServletRequest request, HttpServletResponse response) {
		String hql = "from DriverEntity as a where a.status='1'";
		List<DriverEntity> driverList = service.findByQueryString(hql);
		request.setAttribute("driverList", driverList);
		return templatePath + "shop/order/fendan";
	}
	
	
	/**
	 *  分单管理
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "getNowAllDaifahuoOrderList")
	@ResponseBody
	public Map<String, Object> getNowAllDaifahuoOrderList(HttpServletRequest request) {
		return service.getNowAllDaifahuoOrderList(request);
	}
	
	/**
	 *  分单管理
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "getDriverOrderList")
	@ResponseBody
	public Map<String, Object> getDriverOrderList(HttpServletRequest request) {
		return service.getDriverOrderList(request);
	}
	
	/**
	 * 售后订单详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "peisongdan")
	public String peisongdan( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		service.initPeisongdan(request,id);//司机id
		return templatePath + "shop/order/peisongdan";
	}
	
	
	/**
	 * 售后订单详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "fenjiandan")
	public String fenjiandan( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		service.initFenjiandan(request,id);//司机id
		return templatePath + "shop/order/fenjiandan";
	}
	
	
	/**
	 * 售后订单详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "dayindan")
	public String dayindan( HttpServletRequest request, HttpServletResponse response) {
		service.initDayindan(request);//司机id
		return templatePath + "shop/order/dayindan";
	}
	
}
