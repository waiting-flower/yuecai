package modules.shop.gift.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.HqlQuery;
import common.util.StringUtil;
import modules.shop.gift.entity.GiftEntity;
import modules.shop.gift.service.GiftService;
import modules.shop.goods.entity.BannerEntity;

@Service
public class GiftServiceImpl extends CommonServiceImpl implements GiftService{

	@Override
	public DataTableReturn loadGiftByPage(JQueryDataTables dataTable, HttpServletRequest request) {
		StringBuffer hql = new StringBuffer(
				" from GiftEntity as  a where 1 = 1  and a.status = '1' ");
		List<Object> params = new ArrayList<Object>();
		String name = request.getParameter("name");
		if (StringUtil.isNotEmpty(name)) {
			hql.append(" and a.name like ? ");
			params.add("%" + name + "%");
		}
		hql.append(" order by  a.createDate desc");
		HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
		int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
		hqlQuery.setCurPage(currPage + 1);
		hqlQuery.setPageSize(dataTable.getDisplayLength());
		DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
		List<GiftEntity> pList = (List<GiftEntity>) d.getAaData();
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		for (GiftEntity p : pList) {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("id", p.getId());
			/*
		 * private String name;//商品名称
	private String imgUrl;//赠品的图片
	private Double money;//赠品的金额
	private String imgList;
	private String info;
	private Integer saleNum;
	private Integer stockNum;
		 */
			item.put("name", p.getName());
			item.put("imgUrl", p.getImgUrl());
			item.put("money", p.getMoney());
			item.put("saleNum", p.getSaleNum());
			item.put("stockNum", p.getStockNum());
			item.put("status", p.getStatus());
			item.put("time",DateUtil.getTimeByCustomPattern( p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
			mapList.add(item);
		}
		d.setAaData(mapList);
		d.setsEcho(dataTable.getEcho());
		return d;
	}

	@Override
	public Map<String, Object> saveGift(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String imgUrl = request.getParameter("imgUrl");
		String info = request.getParameter("info");
		String money = request.getParameter("money");
		String imgList = request.getParameter("imgList");
		String stockNum = request.getParameter("stockNum");
		/*
		 * private String name;//商品名称
	private String imgUrl;//赠品的图片
	private Double money;//赠品的金额
	private String imgList;
	private String info;
	private Integer saleNum;
	private Integer stockNum;
		 */
		GiftEntity a = null;
		if(StringUtil.isNotEmpty(id)) {
			a = get(GiftEntity.class, Long.valueOf(id));
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
		}
		else {
			a = new GiftEntity();
			a.setCreateDate(DateUtil.getCurrentTime());
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
			a.setSaleNum(0);
		}
		a.setImgUrl(imgUrl);
		a.setName(name);
		a.setImgList(imgList);
		a.setInfo(info);
		a.setMoney(Double.valueOf(money));
		a.setStockNum(Integer.parseInt(stockNum));
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> deleteGift(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		GiftEntity a = get(GiftEntity.class, Long.valueOf(id));
		if (a == null) { 
			map.put("code", 1);
			return map;
		}
		a.setStatus("0");
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

}
