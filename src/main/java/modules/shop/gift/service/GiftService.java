package modules.shop.gift.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;

public interface GiftService extends CommonService{

	DataTableReturn loadGiftByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveGift(HttpServletRequest request);

	Map<String, Object> deleteGift(HttpServletRequest request);

}
