package modules.shop.gift.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.util.StringUtil;
import modules.shop.gift.entity.GiftEntity;
import modules.shop.gift.service.GiftService;
import modules.shop.goods.entity.BannerEntity;

/**
 * 赠品管理 web控制层
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "admin/shop/gift")
public class GiftController extends BaseController{
	
	@Autowired
	GiftService service;
	
	
	/**
	 * 赠品管理列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list")
	public String cateList( HttpServletRequest request, HttpServletResponse response) {
		return templatePath + "shop/gift/giftList";
	}
	
	/**
	 * 赠品详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "info")
	public String info( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			GiftEntity menu = service.get(GiftEntity.class, Long.valueOf(id));
			request.setAttribute("a", menu);
		} 
		return templatePath + "shop/gift/giftInfo";
	}
	
	
	/**
	 * 分页获取商城赠品列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadGiftByPage")
	@ResponseBody
	public DataTableReturn loadGiftByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = service.loadGiftByPage(dataTable, request);
		return d;
	}
	
	/**
	 * 保存一个商城赠品
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveGift")
	@ResponseBody
	public Map<String, Object> saveGift(HttpServletRequest request) {
		return service.saveGift(request);
	}
	
	/**
	 * 删除一个商城赠品
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteGift")
	@ResponseBody
	public Map<String, Object> deleteGift(HttpServletRequest request) {
		return service.deleteGift(request);
	}
}
