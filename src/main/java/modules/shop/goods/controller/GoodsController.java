package modules.shop.goods.controller;

import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.util.StringUtil;
import modules.shop.goods.entity.*;
import modules.shop.goods.service.GoodsService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 商品web层controller
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "admin/shop/goods")
public class GoodsController extends BaseController{
	
	
	@Autowired
	GoodsService service; 
	
	
	/**
	 * 商品列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list")
	public String list( HttpServletRequest request, HttpServletResponse response) {
		String hql = "from CateEntity as a where a.parentCate is null order by a.orderNum desc";
		List<CateEntity> cateList1 = service.findByQueryString(hql);
		
		hql = "from CateEntity as a where a.parentCate is not null order by a.orderNum desc,a.orderNum2 desc";
		List<CateEntity> cateList2 = service.findByQueryString(hql);
		
		request.setAttribute("cateList1", cateList1);
		request.setAttribute("cateList2", cateList2);
		
		return templatePath + "shop/goods/goodsList";
	}
	
	/**
	 * 特价折扣列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "discountList")
	public String discountList( HttpServletRequest request, HttpServletResponse response) {
		return templatePath + "shop/goods/discountList";
	}
	
	/**
	 * 商品详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "info")
	public String info( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			GoodsEntity menu = service.get(GoodsEntity.class, Long.valueOf(id));
			request.setAttribute("a", menu);
			
			String hql = "from GoodsParamsEntity as a where a.goods.id=" + id;
			List<GoodsParamsEntity> paramsList = service.findByQueryString(hql);
			request.setAttribute("paramsList", paramsList);
			
			String hql2 = "from GoodsSkuEntity as a where a.status='1' and a.goods.id=" + id;
			List<GoodsSkuEntity> skuList = service.findByQueryString(hql2);
			List<Map<String, Object>> mapList = new ArrayList<>();
			for(GoodsSkuEntity sku : skuList) {
				Map<String,Object> item = new HashMap<>();
				String paramsValusJson = sku.getParmasValuesJson();
				JSONObject itemJson = JSONObject.fromObject(paramsValusJson);
				List<Map<String, Object>> skuMapList = new ArrayList<>();
				for(int i = 0; i < paramsList.size(); i++) {
					Map<String,Object> skuItem = new HashMap<>();
					skuItem.put("canshuxm", paramsList.get(i).getParamsName());
					skuItem.put("canshuVal", itemJson.getString(paramsList.get(i).getParamsName()));
					skuMapList.add(skuItem);
				}
				item.put("skuList", skuMapList);
				item.put("price", sku.getPrice());
				item.put("stockNum", sku.getStockNum());
				item.put("oldPrice", sku.getOldPrice());
				item.put("imgUrl", sku.getImgUrl());
				item.put("skuId", sku.getId());
				item.put("disbuylimit", sku.getDisbuylimit());
				item.put("unit",sku.getUnit());
				mapList.add(item);
			}
			request.setAttribute("mapList", mapList);
		} 
		String hql = "from CateEntity as a where a.status = '1' order by a.orderNum2 desc,a.parentCate, a.orderNum desc";
		List<CateEntity> cateList = service.findByQueryString(hql);
		request.setAttribute("cateList", cateList);
		
		hql = "from BrandEntity as a where a.status = '1' order by a.orderNum desc";
		List<BrandEntity> brandList = service.findByQueryString(hql);
		request.setAttribute("brandList", brandList);
		return templatePath + "shop/goods/goodsInfo";
	}
	
	
	/**
	 * 分页获取商城商品列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadGoodsByPage")
	@ResponseBody
	public DataTableReturn loadGoodsByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = service.loadGoodsByPage(dataTable, request);
		return d;
	}
	
	/**
	 * 保存一个商城商品
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveGoods")
	@ResponseBody
	public Map<String, Object> saveGoods(HttpServletRequest request) {
		return service.saveGoods(request);
	}
	
	/**
	 * 删除一个商城商品
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteGoods")
	@ResponseBody
	public Map<String, Object> deleteGoods(HttpServletRequest request) {
		return service.deleteGoods(request);
	}
	
	/**
	 * 保存商品的热销推荐
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveGoodsRec")
	@ResponseBody
	public Map<String, Object> saveGoodsRec(HttpServletRequest request) {
		return service.saveGoodsRec(request);
	}
	
	/**
	 * 保存商品的特价折扣
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveGoodsDiscount")
	@ResponseBody
	public Map<String, Object> saveGoodsDiscount(HttpServletRequest request) {
		return service.saveGoodsDiscount(request);
	}

	/**
	 * 保存商品排序
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveGoodsSort")
	@ResponseBody
	public Map<String, Object> saveGoodsSort(HttpServletRequest request) {
		return service.saveGoodsSort(request);
	}
	
	/**
	 * 设置商品上架，下架
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveGoodsIsOnSale")
	@ResponseBody
	public Map<String, Object> saveGoodsIsOnSale(HttpServletRequest request) {
		return service.saveGoodsIsOnSale(request);
	}
}
