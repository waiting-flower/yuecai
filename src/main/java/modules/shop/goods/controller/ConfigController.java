package modules.shop.goods.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import common.controller.BaseController;
import modules.shop.goods.entity.ConfigEntity;
import modules.shop.goods.service.ConfigService;

/**
 * 轮播图片 banner web控制层controller
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "admin/shop/config")
public class ConfigController extends BaseController{
	
	@Autowired
	ConfigService service;
	
	/**
	 * 轮播详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "info")
	public String info( HttpServletRequest request, HttpServletResponse response) {
		ConfigEntity a = service.get(ConfigEntity.class, 1l);
		request.setAttribute("a", a);
		return templatePath + "shop/goods/config";
	}
	
	/**
	 * 保存商城配置
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveConfig")
	@ResponseBody
	public Map<String, Object> saveConfig(HttpServletRequest request) {
		return service.saveConfig(request);
	}
	
	
	/**
	 * 保存微信支付证书文件
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveUploadCertPath")
	@ResponseBody
	public Map<String, Object> saveUploadCertPath(@RequestParam("certFile") MultipartFile file,
			HttpServletRequest request) {
		return service.saveUploadCertPath(file,request);
	}
	
	
	
}
