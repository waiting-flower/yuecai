package modules.shop.goods.controller;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.TemplateExportParams;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import common.controller.BaseController;
import modules.shop.goods.service.DataService;
import modules.shop.order.entity.OrderDetailEntity;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "admin/shop/data")
public class DataController extends BaseController {

    @Autowired
    DataService service;


    /**
     * 动态统计
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "dongtai")
    public String dongtai(HttpServletRequest request, HttpServletResponse response) {
        service.initDongtaiData(request);
        return templatePath + "shop/data/dongtai";
    }

    /**
     * 二次分单统计
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "fendan")
    public String fendan(HttpServletRequest request, HttpServletResponse response) {
        return templatePath + "shop/data/fendan";
    }


    /**
     * 客户统计
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "kehu")
    public String kehu(HttpServletRequest request, HttpServletResponse response) {
        service.initKehuData(request);
        return templatePath + "shop/data/kehu";
    }


    /**
     * 商品统计
     *
     * @param request
     * @param response
     * @return
     */
    /*
    @RequestMapping(value = "goods")
    public String goods(HttpServletRequest request, HttpServletResponse response) {
        service.initGoodsData(request);
        return templatePath + "shop/data/goods";
    }
*/
    /**
     * 商品销售订单统计
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "goods")
    public String orders(HttpServletRequest request, HttpServletResponse response) {
        service.initOrdersData(request);
        return templatePath + "shop/data/goods";
    }


    /**
     * 订单统计
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "order")
    public String order(HttpServletRequest request, HttpServletResponse response) {
        service.initOrderData(request);
        return templatePath + "shop/data/order";
    }

    /**
     * 销售订单导出
     */
    @RequestMapping("order/export")
    public void exportData(HttpServletRequest request, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = service.initOrderExcelData(request);
        TemplateExportParams params = new TemplateExportParams("doc/template.xls");
        params.setHeadingRows(2);
        params.setHeadingStartRow(2);
        List resultList = Lists.newArrayList();
        for (Map l : list) {
            List<OrderDetailEntity> deataiList = (List<OrderDetailEntity>) l.get("deataiList");
            for (OrderDetailEntity od : deataiList) {
                Map rowMap = Maps.newHashMap();
                rowMap.put("date", l.get("date"));
                rowMap.put("userName", l.get("userName"));
                rowMap.put("couponMoney", l.get("couponMoney"));
                rowMap.put("code", od.getGoods().getGoodsNo());
                rowMap.put("name", od.getGoods().getName());
                rowMap.put("unit", od.getSku() != null ? od.getSku().getParmasValuesJson().substring(od.getSku().getParmasValuesJson().indexOf("单位")+5, od.getSku().getParmasValuesJson().length() - 2) : "");
                rowMap.put("num", od.getNum());
                rowMap.put("price", od.getPrice());
                rowMap.put("money", od.getMoney());
                resultList.add(rowMap);
            }
        }
        Map<String, Object> map = Maps.newHashMap();
        map.put("maplist", resultList);
        Workbook book = ExcelExportUtil.exportExcel(params, map);
        response.setContentType("APPLICATION/OCTET-STREAM");
        String fileName = "data.xls";
        response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
        response.flushBuffer();
        ServletOutputStream os = response.getOutputStream();
        book.write(os);
        os.close();
    }


    /**
     * 业务员统计
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "yewuyuan")
    public String yewuyuan(HttpServletRequest request, HttpServletResponse response) {
        service.initYewuyuanData(request);
        return templatePath + "shop/data/yewuyuan";
    }

    /**
     * 注册客户信息统计
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "user")
    public String user(HttpServletRequest request, HttpServletResponse response) {
        service.initUserData(request);
        return templatePath + "shop/data/user";
    }
}
