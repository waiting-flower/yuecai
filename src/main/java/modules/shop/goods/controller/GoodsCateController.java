package modules.shop.goods.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.util.StringUtil;
import modules.shop.goods.entity.CateEntity;
import modules.shop.goods.service.GoodsCateService;

/**
 * 商品分类web层controller
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "admin/shop/cate")
public class GoodsCateController extends BaseController{

	@Autowired
	GoodsCateService service;
	/**
	 * 商品分类列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list")
	public String cateList( HttpServletRequest request, HttpServletResponse response) {
		return templatePath + "shop/goods/cateList";
	}
	
	/**
	 * 商品分类详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "info")
	public String info( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			CateEntity menu = service.get(CateEntity.class, Long.valueOf(id));
			request.setAttribute("a", menu);
		} 
		String hql = "from CateEntity as a where a.status = '1' and a.parentCate is null order by a.orderNum desc";
		List<CateEntity> cateList = service.findByQueryString(hql);
		request.setAttribute("cateList", cateList);
		return templatePath + "shop/goods/cateInfo";
	}
	
	
	/**
	 * 分页获取商城分类列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadCateByPage")
	@ResponseBody
	public DataTableReturn loadCateByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = service.loadCateByPage(dataTable, request);
		return d;
	}
	
	/**
	 * 保存一个商城分类
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveCate")
	@ResponseBody
	public Map<String, Object> saveCate(HttpServletRequest request) {
		return service.saveCate(request);
	}
	
	/**
	 * 删除一个商城分类
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteCate")
	@ResponseBody
	public Map<String, Object> deleteCate(HttpServletRequest request) {
		return service.deleteCate(request);
	}
}
