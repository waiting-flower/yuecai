package modules.shop.goods.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.util.StringUtil;
import modules.shop.goods.entity.BannerEntity;
import modules.shop.goods.service.BannerService;

/**
 * 轮播图片 banner web控制层controller
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "admin/shop/banner")
public class BannerController extends BaseController{
	
	@Autowired
	BannerService service;
	
	/**
	 * 轮播管理列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list")
	public String cateList( HttpServletRequest request, HttpServletResponse response) {
		return templatePath + "shop/goods/bannerList";
	}
	
	/**
	 * 轮播详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "info")
	public String info( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			BannerEntity menu = service.get(BannerEntity.class, Long.valueOf(id));
			request.setAttribute("a", menu);
		} 
		return templatePath + "shop/goods/bannerInfo";
	}
	
	
	/**
	 * 分页获取商城轮播列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadBannerByPage")
	@ResponseBody
	public DataTableReturn loadBannerByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = service.loadBannerByPage(dataTable, request);
		return d;
	}
	
	/**
	 * 保存一个商城轮播
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveBanner")
	@ResponseBody
	public Map<String, Object> saveBanner(HttpServletRequest request) {
		return service.saveBanner(request);
	}
	
	/**
	 * 删除一个商城轮播
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteBanner")
	@ResponseBody
	public Map<String, Object> deleteCate(HttpServletRequest request) {
		return service.deleteBanner(request);
	}
}
