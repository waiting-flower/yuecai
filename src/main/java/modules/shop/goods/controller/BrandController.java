package modules.shop.goods.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.util.StringUtil;
import modules.shop.goods.entity.BrandEntity;
import modules.shop.goods.service.BrandService;

@Controller
@RequestMapping(value = "admin/shop/brand")
public class BrandController extends BaseController{
	
	@Autowired
	BrandService service;
	
	/**
	 * 商城品牌对象管理列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list")
	public String cateList( HttpServletRequest request, HttpServletResponse response) {
		return templatePath + "shop/brand/brandList";
	}
	
	/**
	 * 商城品牌对象详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "info")
	public String info( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			BrandEntity menu = service.get(BrandEntity.class, Long.valueOf(id));
			request.setAttribute("a", menu);
		} 
		return templatePath + "shop/brand/brandInfo";
	}
	
	
	/**
	 * 分页获取商城品牌对象列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadBrandByPage")
	@ResponseBody
	public DataTableReturn loadBrandByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = service.loadBrandByPage(dataTable, request);
		return d;
	}
	
	/**
	 * 保存一个商城轮播
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveBrand")
	@ResponseBody
	public Map<String, Object> saveBrand(HttpServletRequest request) {
		return service.saveBrand(request);
	}
	
	/**
	 * 删除一个商城品牌对象
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteBrand")
	@ResponseBody
	public Map<String, Object> deleteBrand(HttpServletRequest request) {
		return service.deleteBrand(request);
	}
}
