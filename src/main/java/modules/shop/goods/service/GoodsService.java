package modules.shop.goods.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;

public interface GoodsService extends CommonService{

	DataTableReturn loadGoodsByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveGoods(HttpServletRequest request);

	Map<String, Object> deleteGoods(HttpServletRequest request);

	Map<String, Object> saveGoodsRec(HttpServletRequest request);

	Map<String, Object> saveGoodsDiscount(HttpServletRequest request);

	Map<String, Object> saveGoodsSort(HttpServletRequest request);

	Map<String, Object> saveGoodsIsOnSale(HttpServletRequest request);

}
