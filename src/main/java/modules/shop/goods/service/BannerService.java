package modules.shop.goods.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;

public interface BannerService extends CommonService{

	DataTableReturn loadBannerByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveBanner(HttpServletRequest request);

	Map<String, Object> deleteBanner(HttpServletRequest request);

	Map<String, Object> getBannerList(HttpServletRequest request);

}
