package modules.shop.goods.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import common.service.CommonService;

public interface ConfigService extends CommonService{

	Map<String, Object> saveConfig(HttpServletRequest request);

	Map<String, Object> saveUploadCertPath(MultipartFile file,HttpServletRequest request);

}
