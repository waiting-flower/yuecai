package modules.shop.goods.service.impl;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.FileUtil;
import common.util.StringUtil;
import modules.shop.goods.entity.BannerEntity;
import modules.shop.goods.entity.ConfigEntity;
import modules.shop.goods.service.ConfigService;

@Service
public class ConfigServiceImpl extends CommonServiceImpl implements ConfigService{

	@Override
	public Map<String, Object> saveConfig(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		String goodsRule = request.getParameter("goodsRule");
		String cancleReason = request.getParameter("cancleReason");
		String refoudReason = request.getParameter("refoudReason");
		String afterReason = request.getParameter("afterReason");
		String cancleOrderTime = request.getParameter("cancleOrderTime");
		String salesAfterDay = request.getParameter("salesAfterDay");
		String receiverDay = request.getParameter("receiverDay");
		String appid = request.getParameter("appid");
		String appSecret = request.getParameter("appSecret");
		String notice = request.getParameter("notice");
		
		String mchId = request.getParameter("mchId");
		String mchSecret = request.getParameter("mchSecret");
		String certPath = request.getParameter("certPath");

		String shareImgUrl = request.getParameter("shareImgUrl");
		String shareTitle = request.getParameter("shareTitle");
		String mobile = request.getParameter("mobile");
		String xianzhi = request.getParameter("xianzhi");
		
		/*
		 * 	private String goodsRule;//商品说明
	private String cancleReason;//取消订单的原因
	private String refoudReason;//订单退款的原因
	private String afterReason;//售后的原因
	private Integer cancleOrderTime;//订单多久时间取消，单位为小时
	private String appid;//客户端小程序appid
	private String appSecret;//客户端小程序appsecret
	private String appidTeam;
	private String appSecretTeam;
	private String mchId;//商户id
	private String mchSecret;//商户支付秘钥
	private String certPath;//证书路径
		 */
		ConfigEntity a = null;
		if(StringUtil.isNotEmpty(id)) {
			a = get(ConfigEntity.class, Long.valueOf(id));
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
		}
		else {
			a = new ConfigEntity();
			a.setCreateDate(DateUtil.getCurrentTime());
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
		}
		a.setMobile(mobile);
		a.setXianzhi(xianzhi);
		a.setShareImgUrl(shareImgUrl);
		a.setShareTitle(shareTitle);
		if(StringUtil.isNotEmpty(receiverDay)) {
			a.setReceiverDay(Integer.parseInt(receiverDay));
		}
		if(StringUtil.isNotEmpty(salesAfterDay)) {
			a.setSalesAfterDay(Integer.parseInt(salesAfterDay));
		}
		a.setNotice(notice);
		a.setGoodsRule(goodsRule);
		a.setCancleReason(cancleReason);
		a.setRefoudReason(refoudReason);
		a.setAfterReason(afterReason);
		a.setCancleOrderTime(Integer.parseInt(cancleOrderTime));
		a.setAppid(appid);
		a.setAppSecret(appSecret);
		a.setMchId(mchId);
		a.setMchSecret(mchSecret);
		a.setCertPath(certPath);
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> saveUploadCertPath(MultipartFile file,HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", "1");
		map.put("msg", "上传成功");
		// 文件保存目录路径
		String savePath = "c:/cert/";
		String fileName = file.getOriginalFilename();

		String fileExt = FileUtil.getExtend(fileName);// 获取文件的扩展名
		if (!FileUtil.checkFileSize(file.getSize())) {
			map.put("code", "0");
			map.put("msg", "上传的文件过大");
			return map;
		}
		String tempFileName = FileUtil.generateFileName(fileExt);// 生成一个随机日期型的文件名称
		File targetFile = new File(savePath, tempFileName);
		if (!targetFile.exists()) {
			targetFile.mkdirs();
		}
		// 保存
		try {
			file.transferTo(targetFile);
			map.put("path", targetFile.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
			map.put("code", "0");
			map.put("msg", "上传的文件失败" + e.getMessage());
		}
		map.put("url", savePath + tempFileName);
		return map;
	}


}
