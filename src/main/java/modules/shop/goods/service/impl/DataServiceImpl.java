package modules.shop.goods.service.impl;

import cn.hutool.core.util.NumberUtil;
import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.StringUtil;
import modules.shop.goods.entity.BrandEntity;
import modules.shop.goods.entity.GoodsEntity;
import modules.shop.goods.entity.GoodsSkuEntity;
import modules.shop.goods.service.DataService;
import modules.shop.order.entity.OrderDetailEntity;
import modules.shop.order.entity.OrderEntity;
import modules.shop.user.entity.WUserEntity;
import org.apache.commons.lang.time.DateFormatUtils;
import org.jsoup.select.Evaluator;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.*;

@Service
public class DataServiceImpl extends CommonServiceImpl implements DataService {

    @Override
    public void initDongtaiData(HttpServletRequest request) {
        java.text.DecimalFormat df = new java.text.DecimalFormat("##0.00");
        String beginTime = request.getParameter("beginTime");
        String endTime = request.getParameter("endTime");
        request.setAttribute("beginTime", beginTime);
        request.setAttribute("endTime", endTime);
        String sql = "SELECT pro,city,county,count(1) from shop_user where `status` = '1'";
        if (StringUtil.isNotEmpty(beginTime)) {
            sql = sql + " and DATE_FORMAT(createDate,'%Y-%m-%d')  >= '" + beginTime + "'";
        }
        if (StringUtil.isNotEmpty(endTime)) {
            sql = sql + " and DATE_FORMAT(createDate,'%Y-%m-%d')  <= '" + endTime + "'";
        }
        sql += " GROUP BY pro,city,county";
        List<Object[]> objList = commonDao.findListbySql(sql);
        List<Map<String, Object>> mapList = new ArrayList<>();
        for (Object[] obj : objList) {
            Map<String, Object> item = new HashMap<>();
            String pro = (String) obj[0];
            String city = (String) obj[1];
            String county = (String) obj[2];

            item.put("pro", pro);
            item.put("city", city);
            item.put("county", county);
            item.put("userCount", obj[3]);
            item.put("jiazhikehu", 0);
            String tempHql = "SELECT SUM(o.money) from shop_user u,shop_order o "
                    + " where u.id = o.userId and u.pro = '" + pro + "'"
                    + " and u.city = '" + city + "'"
                    + " and u.county = '" + county + "'";
            if (StringUtil.isNotEmpty(beginTime)) {
                tempHql = tempHql + " and DATE_FORMAT(o.createDate,'%Y-%m-%d')  >= '" + beginTime + "'";
            }
            if (StringUtil.isNotEmpty(endTime)) {
                tempHql = tempHql + " and DATE_FORMAT(o.createDate,'%Y-%m-%d')  <= '" + endTime + "'";
            }
            List<Object> moneyList = commonDao.findListbySql(tempHql);
            item.put("money", moneyList.get(0));

            tempHql = "from OrderEntity as a where a.user.pro = ?"
                    + " and a.user.city = ?"
                    + " and a.user.county = ?"
                    + " and a.orderState in ('1','2','3','4')";
            if (StringUtil.isNotEmpty(beginTime)) {
                tempHql = tempHql + " and DATE_FORMAT(a.createDate,'%Y-%m-%d')  >= '" + beginTime + "'";
            }
            if (StringUtil.isNotEmpty(endTime)) {
                tempHql = tempHql + " and DATE_FORMAT(a.createDate,'%Y-%m-%d')  <= '" + endTime + "'";
            }
            List<OrderEntity> orderList = commonDao.findHql(tempHql, new Object[]{pro, city, county});
            Set<Long> userIdSet = new HashSet<>();
            for (OrderEntity order : orderList) {
                userIdSet.add(order.getUser().getId());
            }
            item.put("xiadankehu", userIdSet.size());
            mapList.add(item);
        }
        request.setAttribute("mapList", mapList);
    }

    @Override
    public void initKehuData(HttpServletRequest request) {
        String beginTime = request.getParameter("beginTime");
        String endTime = request.getParameter("endTime");
        Date now = new Date();
        //处理开始日期
        int end = 90;
        if (StringUtil.isNotEmpty(endTime)) {
            end = (int) DateUtil.getDayInterval(beginTime);
        }
        //处理结束日期
        int start = 0;
        if (StringUtil.isNotEmpty(beginTime)) {
            start = (int) DateUtil.getDayInterval(endTime);
        }
        if (StringUtil.isEmpty(beginTime)) {
            beginTime = DateUtil.convertToString(DateUtil.getPriorDay(now, end));
        }
        if (StringUtil.isEmpty(endTime)) {
            endTime = DateUtil.convertToString(now);
        }
        request.setAttribute("beginTime", beginTime);
        request.setAttribute("endTime", endTime);

        List<Map<String, Object>> mapList = new ArrayList<>();
        for (int i = start; i <= end; i++) {
            Map<String, Object> item = new HashMap<>();
            Date day = DateUtil.getAfterDay(now, 0 - i);
            String date = DateUtil.getTimeByCustomPattern(day, "yyyy-MM-dd");

            item.put("date", date);
            String sql = "select count(1) from shop_user_login_log where day='" + date + "'";
            List<Object> objList = commonDao.findListbySql(sql);
            if (objList != null && !objList.isEmpty()) {
                item.put("denglucishu", objList.get(0));
            } else {
                item.put("denglurenshu", 0);
            }


            sql = "select count(1) from shop_user_login_log where day='" + date + "' group by userId";
            objList = commonDao.findListbySql(sql);
            if (objList != null && !objList.isEmpty()) {
                item.put("denglurenshu", objList.get(0));
            } else {
                item.put("denglurenshu", 0);
            }


            sql = "SELECT COUNT(1) from shop_order where DATE_FORMAT(createDate,'%Y-%m-%d') = '" + date + "'";
            objList = commonDao.findListbySql(sql);
            item.put("xiadanrenshu", objList.get(0));
            if (objList != null && !objList.isEmpty()) {
                item.put("xiadanrenshu", objList.get(0));
            } else {
                item.put("xiadanrenshu", 0);
            }


            sql = "SELECT COUNT(1) from shop_user where DATE_FORMAT(createDate,'%Y-%m-%d') = '" + date + "'";
            objList = commonDao.findListbySql(sql);
            item.put("xinzengrenshu", objList.get(0));
            if (objList != null && !objList.isEmpty()) {
                item.put("xinzengrenshu", objList.get(0));
            } else {
                item.put("xinzengrenshu", 0);
            }


            item.put("xinkehuxiadanshu", 0);//新客户下单数

            sql = "SELECT count(1) from shop_user where DATE_FORMAT(createDate,'%Y-%m-%d') <= '" + date + "'";
            objList = commonDao.findListbySql(sql);
            item.put("leijikehushu", objList.get(0));
            if (objList != null && !objList.isEmpty()) {
                item.put("leijikehushu", objList.get(0));
            } else {
                item.put("leijikehushu", 0);
            }


            mapList.add(item);
        }
        request.setAttribute("mapList", mapList);


    }

    //lengsong
    @Override
    public void initOrdersData(HttpServletRequest request) {

        /*String hql = "from GoodsSkuEntity as a where a.status= '1'" +
                " and exists (select 1 from OrderDetailEntity as b where b.sku.id = a.id ";
                */
        String hql = "select a.id,a.orderId,a.goodsId,a.skuId,sum(a.num) as allnum from shop_order_detail as a  where exists(select b.* from shop_order as b where b.id = a.orderId and b.orderState in ('0','1','2','3','4')) and a.status= '1' ";
        String beginTime = request.getParameter("beginTime");
        String endTime = request.getParameter("endTime");
        request.setAttribute("beginTime", beginTime);
        request.setAttribute("endTime", endTime);

        if (StringUtil.isNotEmpty(beginTime)) {
            hql = hql + " and a.createDate >= '" + beginTime + "'";
        }
        if (StringUtil.isNotEmpty(endTime)) {
            hql = hql + " and a.createDate <= '" + endTime + "'";
        }
        hql += " group by a.skuId";
        List<Object[]> orderList = commonDao.findListbySql(hql);
        List<Map<String, Object>> mapList = new ArrayList<>();
        int index = 1;
        for (Object[] od : orderList) {
            BigInteger id = (BigInteger) od[0];
            BigInteger orderid = (BigInteger) od[1];
            BigInteger goodsid = (BigInteger) od[2];
            BigInteger skuid = (BigInteger) od[3];
            BigInteger allnum = new BigInteger(String.valueOf(od[4]));
            GoodsEntity goods = get(GoodsEntity.class, goodsid.longValue());
            if (skuid != null && !skuid.equals("null")) {
                GoodsSkuEntity skugoods = get(GoodsSkuEntity.class, skuid.longValue());
                Map<String, Object> item = new HashMap<>();
                item.put("no", index);
                item.put("orderid", orderid);
                //item.put("createDate", DateFormatUtils.format(od.getCreateDate(), "yyyy-MM-dd HH:mm"));
                item.put("goodsName", goods.getName());
                item.put("goodsNo", goods.getGoodsNo());
                item.put("tiaoxingma", goods.getTiaoxingma());
                item.put("sku", skugoods.getParmasValuesJson());
                item.put("danwei", skugoods.getUnit());
                item.put("num", allnum);
                item.put("price", skugoods.getPrice());
                item.put("money", skugoods.getPrice() * allnum.intValue());
                item.put("cate1", goods.getCate().getName());
                item.put("cate2", goods.getCate().getName());
                BrandEntity brand = goods.getBrand();
                if (brand != null) {
                    item.put("brand", brand.getName());
                } else {
                    item.put("brand", "无");
                }
                item.put("qudao", skugoods.getGoods().getName());
                index++;
                mapList.add(item);
            } else {
                Map<String, Object> item = new HashMap<>();
                item.put("no", index);
                item.put("orderid", orderid);
                //item.put("createDate", DateFormatUtils.format(od.getCreateDate(), "yyyy-MM-dd HH:mm"));
                item.put("goodsName", goods.getName());
                item.put("goodsNo", goods.getGoodsNo());
                item.put("tiaoxingma", goods.getTiaoxingma());
                item.put("sku", "");
                item.put("danwei", "");
                item.put("num", allnum);
                item.put("price", goods.getPrice());
                item.put("money", goods.getPrice() * goods.getSaleNum());
                item.put("cate1", goods.getCate().getName());
                item.put("cate2", goods.getCate().getParentCate().getName());
                BrandEntity brand = goods.getBrand();
                if (brand != null) {
                    item.put("brand", brand.getName());
                } else {
                    item.put("brand", "无");
                }
                item.put("qudao", goods.getName());
                index++;
                mapList.add(item);
            }
        }
        request.setAttribute("mapList", mapList);
    }

    @Override
    public void initGoodsData(HttpServletRequest request) {
        String hql = "from GoodsSkuEntity as a where a.status= '1' and a.goods.status = '1' and a.isNew ='1' ";
        String beginTime = request.getParameter("beginTime");
        String endTime = request.getParameter("endTime");
        request.setAttribute("beginTime", beginTime);
        request.setAttribute("endTime", endTime);

        if (StringUtil.isNotEmpty(beginTime)) {
            hql = hql + " and DATE_FORMAT(a.createDate,'%Y-%m-%d')  >= '" + beginTime + "'";
        }
        if (StringUtil.isNotEmpty(endTime)) {
            hql = hql + " and DATE_FORMAT(a.createDate,'%Y-%m-%d')  <= '" + endTime + "'";
        }
        hql += " order by a.createDate desc";
        List<GoodsSkuEntity> skuList = findByQueryString(hql);
        List<Map<String, Object>> mapList = new ArrayList<>();
        int index = 1;
        for (GoodsSkuEntity sku : skuList) {
            GoodsEntity goods = sku.getGoods();
            sku.setSaleNum(sku.getSaleNum() + 1);
            Map<String, Object> item = new HashMap<>();
            item.put("no", index);
            item.put("createDate", DateFormatUtils.format(sku.getCreateDate(), "yyyy-MM-dd HH:mm"));
            item.put("goodsName", sku.getGoods().getName());
            item.put("goodsNo", goods.getGoodsNo());
            item.put("tiaoxingma", goods.getTiaoxingma());
            item.put("sku", sku.getParmasValuesJson());
            item.put("danwei", sku.getUnit());
            item.put("num", sku.getSaleNum());
            item.put("money", sku.getPrice() * sku.getSaleNum());
            item.put("cate1", goods.getCate().getName());
            item.put("cate2", goods.getCate().getParentCate().getName());
            BrandEntity brand = goods.getBrand();
            if (brand != null) {
                item.put("brand", brand.getName());
            } else {
                item.put("brand", "无");
            }

            item.put("qudao", sku.getGoods().getName());
            index++;
            mapList.add(item);
        }
        request.setAttribute("mapList", mapList);
    }

    @Override
    public void initOrderData(HttpServletRequest request) {
        java.text.DecimalFormat df = new java.text.DecimalFormat("##0.00");
        String hql = "from OrderEntity as a where "
                + " a.status='1' and a.orderState in ('1','2','3','4')";
        String beginTime = request.getParameter("beginTime");
        String endTime = request.getParameter("endTime");
        List<Object> params = new ArrayList<>();
        if (StringUtil.isNotEmpty(beginTime)) {
            hql = hql + " and a.createDate >= ? ";
            params.add(DateUtil.parse(beginTime, DateUtil.yyyyMMdd));
        }
        if (StringUtil.isNotEmpty(endTime)) {
            hql = hql + " and a.createDate <= ? ";
            params.add(DateUtil.parse(endTime, DateUtil.yyyyMMdd));
        }
        request.setAttribute("beginTime", beginTime);
        request.setAttribute("endTime", endTime);

        hql = hql + " order by a.createDate desc";
        List<OrderEntity> orderList = findHql(hql, params.toArray());
        List<Map<String, Object>> mapList = new ArrayList<>();
        for (OrderEntity order : orderList) {
            Map<String, Object> item = new HashMap<>();
            item.put("date", DateUtil.getTimeByDefaultPattern(order.getCreateDate()));
            item.put("userName", order.getUser().getName());
            item.put("couponMoney", 0);
//			item.put("payType", order.getPayType());
            item.put("payType", "货到付款");
            item.put("address", order.getAddress());
            item.put("bak", order.getBak());
            item.put("num", order.getNum());
            item.put("money", df.format(order.getMoney()));
            item.put("price", df.format(order.getMoney() / order.getNum()));
            String tempHql = "from OrderDetailEntity as a where a.sOrder.id=" + order.getId();
            List<OrderDetailEntity> odList = findByQueryString(tempHql);
            String goodsName = "", goodsNo = "";
            for (OrderDetailEntity od : odList) {
                goodsName += od.getGoods().getName() + "<br>";
                goodsNo = od.getGoods().getGoodsNo();
            }
            item.put("goodsName", goodsName);
            item.put("goodsNo", goodsNo);
            mapList.add(item);
        }
        request.setAttribute("mapList", mapList);

    }

    @Override
    public List<Map<String, Object>> initOrderExcelData(HttpServletRequest request) {
        String hql = "from OrderEntity as a where "
                + " a.status='1' and a.orderState in ('1','2','3','4')";
        String beginTime = request.getParameter("beginTime");
        String endTime = request.getParameter("endTime");
        List<Object> params = new ArrayList<>();
        if (StringUtil.isNotEmpty(beginTime)) {
            hql = hql + " and a.createDate >= ? ";
            params.add(DateUtil.parse(beginTime, DateUtil.yyyyMMdd));
        }
        if (StringUtil.isNotEmpty(endTime)) {
            hql = hql + " and a.createDate <= ? ";
            params.add(DateUtil.parse(endTime, DateUtil.yyyyMMdd));
        }
        request.setAttribute("beginTime", beginTime);
        request.setAttribute("endTime", endTime);

        hql = hql + " order by a.createDate desc";
        List<OrderEntity> orderList = findHql(hql, params.toArray());
        List<Map<String, Object>> mapList = new ArrayList<>();
        for (OrderEntity order : orderList) {
            Map<String, Object> item = new HashMap<>();
            item.put("date", DateUtil.getTimeByDefaultPattern(order.getCreateDate()));
            item.put("userName", order.getUser().getName());
            item.put("couponMoney", order.getCouponMoney());
//			item.put("payType", order.getPayType());
            item.put("payType", "货到付款");
            item.put("address", order.getAddress());
            item.put("bak", order.getBak());
            item.put("num", order.getNum());
            item.put("money", order.getMoney());
            item.put("price", order.getMoney() / order.getNum());
            String tempHql = "from OrderDetailEntity as a where a.sOrder.id=" + order.getId();
            List<OrderDetailEntity> deataiList = findByQueryString(tempHql);
            item.put("deataiList", deataiList);
            mapList.add(item);
        }
        return mapList;

    }

    @Override
    public void initYewuyuanData(HttpServletRequest request) {
        java.text.DecimalFormat df = new java.text.DecimalFormat("##0.00");
        String hql = "from WUserEntity as a where a.isYewuyuan = '1' ";
        List<WUserEntity> userList = findByQueryString(hql);
        String beginTime = request.getParameter("beginTime");
        String endTime = request.getParameter("endTime");

        request.setAttribute("beginTime", beginTime);
        request.setAttribute("endTime", endTime);

        List<Map<String, Object>> mapList = new ArrayList<>();
        for (WUserEntity user : userList) {
            Map<String, Object> item = new HashMap<>();
            item.put("userId", user.getId());
            item.put("name", user.getNick());
            item.put("mobile", user.getMobile());
            Double allMoney = 0D;
            Double money = 0d;
            String tempHql = "from OrderEntity as a where a.user.parentUser.id=" + user.getId() + " and a.orderState in ('1','2','3','4')";
            /*String tempHql = "from OrderEntity as a where a.status = '1' and a.orderState in ('1','2','3','4') and a.user.id in " +
                    "(select b.id from WUserEntity as b where b.id in " +
                    "(select c.id from WUserEntity as c where c.parentUser.id= "+ user.getId() +" ))";

             */
            List<Object> params = new ArrayList<>();
            if (StringUtil.isNotEmpty(beginTime)) {
                tempHql = tempHql + " and a.createDate >= ? ";
                params.add(DateUtil.parse(beginTime, DateUtil.yyyyMMdd));
            }
            if (StringUtil.isNotEmpty(endTime)) {
                tempHql = tempHql + " and a.createDate <= ? ";
                params.add(DateUtil.parse(endTime, DateUtil.yyyyMMdd));
            }
            List<OrderEntity> orderList = findHql(tempHql, params.toArray());
            Set<Long> userIdSet = new HashSet<>();
            for (OrderEntity order : orderList) {
                userIdSet.add(order.getUser().getId());
                allMoney = allMoney + order.getMoney() + order.getCouponMoney();
                money = money + order.getMoney();
            }
            int num = orderList.size();
            item.put("allNum", num);
            item.put("allMoney", df.format(allMoney));
            item.put("money", df.format(money));
            item.put("userCount", userIdSet.size());

            item.put("kedanjia", 0);
            item.put("kedanjiaCoupon", 0);
            if (num > 0) {

                item.put("kedanjia", df.format(allMoney / num));
                item.put("kedanjiaCoupon", df.format(money / num));
            }

            mapList.add(item);
        }
        request.setAttribute("mapList", mapList);

    }

    @Override
    public void initUserData(HttpServletRequest request) {
        String hql = "from WUserEntity as a where a.status='1' ";
        String beginTime = request.getParameter("beginTime");
        String endTime = request.getParameter("endTime");
        List<Object> params = new ArrayList<>();
        if (StringUtil.isNotEmpty(beginTime)) {
            hql = hql + " and a.createDate >= ? ";
            params.add(DateUtil.parse(beginTime, DateUtil.yyyyMMdd));
        }
        if (StringUtil.isNotEmpty(endTime)) {
            hql = hql + " and a.createDate <= ? ";
            params.add(DateUtil.parse(endTime, DateUtil.yyyyMMdd));
        }
        request.setAttribute("beginTime", beginTime);
        request.setAttribute("endTime", endTime);

        hql = hql + " order by a.createDate desc";
        List<WUserEntity> userList = findHql(hql, params.toArray());
        request.setAttribute("userList", userList);
    }

}
