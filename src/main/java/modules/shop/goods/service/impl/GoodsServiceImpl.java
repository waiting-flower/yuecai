package modules.shop.goods.service.impl;


import com.alibaba.fastjson.JSON;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.HqlQuery;
import common.util.StringUtil;
import modules.shop.goods.entity.*;
import modules.shop.goods.service.GoodsService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GoodsServiceImpl extends CommonServiceImpl implements GoodsService{

	@Override
	public DataTableReturn loadGoodsByPage(JQueryDataTables dataTable, HttpServletRequest request) {
		StringBuffer hql = new StringBuffer(
				" from GoodsEntity as  a where 1 = 1 and a.status='1' ");
		List<Object> params = new ArrayList<Object>();
		String name = request.getParameter("name");
		if (StringUtil.isNotEmpty(name)) {
			hql.append(" and (a.name like ? or a.name2 like ? )");
			params.add("%" + name + "%");
			params.add("%" + name + "%");
		}
		String isDiscount = request.getParameter("isDiscount");
		if (StringUtil.isNotEmpty(isDiscount)) {
			hql.append(" and a.isDiscount = ? ");
			params.add(isDiscount);
		}
		String isRec = request.getParameter("isRec");
		if (StringUtil.isNotEmpty(isRec)) {
			hql.append(" and a.isRec = ? ");
			params.add(isRec);
		}
		String isOnSale = request.getParameter("isOnSale");
		if (StringUtil.isNotEmpty(isOnSale)) {
			hql.append(" and a.isOnSale = ? ");
			params.add(isOnSale);
		}
		String cate = request.getParameter("cate");
		if (StringUtil.isNotEmpty(cate)) {
			hql.append(" and a.cate.id = ? ");
			params.add(Long.valueOf(cate));
		}
		String sSortDir_0 = request.getParameter("sSortDir_0");
		String iSortCol_0 = request.getParameter("iSortCol_0");
		if(!"0".equals(iSortCol_0)) {
			hql.append(" order by a." + request.getParameter("mDataProp_" + iSortCol_0) + " " + sSortDir_0);
		}
		else{
			hql.append(" order by a.createDate desc");	
		}
		System.err.println(hql.toString());
		HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
		int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
		hqlQuery.setCurPage(currPage + 1);
		hqlQuery.setPageSize(dataTable.getDisplayLength());
		DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
		List<GoodsEntity> pList = (List<GoodsEntity>) d.getAaData();
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		for (GoodsEntity p : pList) {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("id", p.getId());
			item.put("name", p.getName());
			item.put("sort", p.getSort());
			item.put("price", p.getPrice());
			item.put("imgUrl", p.getImgUrl());
			item.put("status", p.getStatus());
			item.put("stockNum", p.getStockNum());
			item.put("lookNum", p.getLookNum());
			item.put("saleNum", p.getSaleNum());
			item.put("isRec", p.getIsRec());
			item.put("isOnSale", p.getIsOnSale());
			item.put("isDiscount", p.getIsDiscount());
			item.put("discount", p.getDiscount());
			item.put("cate", p.getCate().getName());
			item.put("time",DateUtil.getTimeByCustomPattern( p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
			mapList.add(item);
		}
		d.setAaData(mapList);
		d.setsEcho(dataTable.getEcho());
		return d;
	}

	@Override
	public Map<String, Object> saveGoods(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String name2 = request.getParameter("name2");
		String price = request.getParameter("price");
		String oldPrice = request.getParameter("oldPrice");
		String imgList = request.getParameter("imgList");
		String info = request.getParameter("info");
		String goodsType = request.getParameter("goodsType");
		String cornerImg = request.getParameter("cornerImg");
		String imgUrl = request.getParameter("imgUrl");
		String stockNum = request.getParameter("stockNum");
		String cate = request.getParameter("cate");
		String brand = request.getParameter("brand");
		String goodsNo = request.getParameter("goodsNo");
		String tiaoxingma = request.getParameter("tiaoxingma");
		String dislimit = request.getParameter("dislimit");
		
		GoodsEntity a = null;
		if(StringUtil.isNotEmpty(id)) {
			a = get(GoodsEntity.class, Long.valueOf(id));
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
			
			//删除之前生成的参数
			String hql = "from GoodsParamsEntity as a where a.goods.id=" + id;
			List<GoodsParamsEntity> paramsEntities = findByQueryString(hql);
			deleteAllEntitie(paramsEntities);
			
		}
		else {
			a = new GoodsEntity();
			a.setCreateDate(DateUtil.getCurrentTime());
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
			
			a.setLookNum(0);
			a.setSaleNum(0);
			a.setIsOnSale("1");
			a.setCommentNum(0);
			a.setNiceComment(100d);
			a.setNiceCommentNum(0);
			a.setScore(5D);
			a.setIsRec("0");
			a.setIsDiscount("0");
			a.setDiscount(10d);
			
		}
		a.setGoodsNo(goodsNo);
		a.setTiaoxingma(tiaoxingma);
		a.setDislimit(Integer.parseInt(dislimit));
		if(StringUtil.isNotEmpty(brand)) {
			a.setBrand(get(BrandEntity.class, Long.valueOf(brand)));
		}
		if(StringUtil.isNotEmpty(price)) {
			a.setPrice(Double.valueOf(price));
		}
		if(StringUtil.isNotEmpty(oldPrice)) {
			a.setOldPrice(Double.valueOf(oldPrice));
		}
		if(StringUtil.isNotEmpty(stockNum)) {
			a.setStockNum(Integer.parseInt(stockNum));
		}
		a.setCate(get(CateEntity.class, Long.valueOf(cate)));
		a.setName2(name2);
		if(StringUtil.isNotEmpty(imgList) && imgList.startsWith(",")) {
			imgList = imgList.substring(1, imgList.length());
		}
		a.setImgList(imgList);
		a.setInfo(info);
		a.setCornerImg(cornerImg);
		a.setGoodsType(goodsType);
		a.setImgUrl(imgUrl);
		a.setName(name);
		saveOrUpdate(a);
		
		
		String skuJson = request.getParameter("skuJson");
		String splitKey = request.getParameter("splitKey");
		String skuItem = request.getParameter("skuItem");
		String paramsItem = request.getParameter("paramsItem");
		
		if(StringUtil.isEmpty(skuItem)) {
			map.put("code", 0);
			return map;
		}
		
		a.setSkuJson(skuItem + splitKey + paramsItem);
		String paramsNameArr[] = skuItem.split(splitKey);
		String parmasValuesArr[] = paramsItem.split(splitKey);
		
		
		String goodsSkuStrArr[] = skuJson.split(splitKey);
		
		for(int i = 0; i < paramsNameArr.length;i++) {
			GoodsParamsEntity params = new GoodsParamsEntity();
			params.setCreateDate(DateUtil.getCurrentTime());
			params.setUpdateDate(DateUtil.getCurrentTime());
			params.setStatus("1");
			params.setGoods(a);
			params.setParamsName(paramsNameArr[i]);
			params.setParmasValues(parmasValuesArr[i]);
			save(params);
		}
		int allStockNum = 0;
		if(StringUtil.isNotEmpty(id)) {
			List<Long> existList = new ArrayList<>();
			for(int i = 0; i < goodsSkuStrArr.length;i++) {
				String goodsSku = goodsSkuStrArr[i];
				String itemArr[] = goodsSku.split("=,=");
				String skuId = "";
				if(itemArr.length > 6) {
					skuId = itemArr[6];
				}
				if(StringUtil.isNotEmpty(skuId)) {
					existList.add(Long.valueOf(skuId));
				}
			}
			String hql = "from GoodsSkuEntity as a where a.goods.id=" + id;
			List<GoodsSkuEntity> skuList = findByQueryString(hql);
			for(GoodsSkuEntity sku : skuList) {
				if(!existList.contains(sku.getId())) {
					sku.setStatus("0");
					saveOrUpdate(sku);
				}
			} 
		}
		
		
		for(int i = 0; i < goodsSkuStrArr.length;i++) {
			String goodsSku = goodsSkuStrArr[i];
			String itemArr[] = goodsSku.split("=,=");
			String skuId = "";
			if(itemArr.length > 6) {
				skuId = itemArr[6];
			}
			GoodsSkuEntity sku = null;
			if(StringUtil.isNotEmpty(skuId)) {
				sku = get(GoodsSkuEntity.class, Long.valueOf(skuId));
				sku.setUpdateDate(DateUtil.getCurrentTime());
			}
			else {
				sku = new GoodsSkuEntity();
				sku.setCreateDate(DateUtil.getCurrentTime());
				sku.setUpdateDate(DateUtil.getCurrentTime());
				sku.setStatus("1");
				sku.setSaleNum(0);
			}
			
			sku.setGoods(a);
			sku.setParmasValuesJson((itemArr[0]));
			sku.setImgUrl(itemArr[1]);
			sku.setPrice(Double.valueOf(itemArr[2]));
			sku.setStockNum(Integer.parseInt(itemArr[3]));
			sku.setDisbuylimit(Integer.parseInt(itemArr[5]));
			sku.setUnit(itemArr[0]);
			sku.setOldPrice(Double.valueOf(itemArr[4]));
			save(sku);
			if(i == 0) {
				a.setPrice(sku.getPrice());
				a.setOldPrice(sku.getOldPrice());
			}
			allStockNum = allStockNum + sku.getStockNum();
		}
		
		a.setSkuItem(skuItem);
		a.setStockNum(allStockNum);
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> deleteGoods(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		GoodsEntity a = get(GoodsEntity.class, Long.valueOf(id));
		if (a == null) { 
			map.put("code", 1);
			return map;
		}
		a.setStatus("0");
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> saveGoodsRec(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		GoodsEntity a = get(GoodsEntity.class, Long.valueOf(id));
		String state = request.getParameter("state");
		a.setIsRec(state);
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> saveGoodsSort(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		String id = request.getParameter("id");
		GoodsEntity a = get(GoodsEntity.class, Long.valueOf(id));
		String sort = request.getParameter("sort");
		Integer sorts = Integer.parseInt(sort);
		a.setSort(Integer.valueOf(sorts));
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> saveGoodsDiscount(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		GoodsEntity a = get(GoodsEntity.class, Long.valueOf(id));
		String state = request.getParameter("state");
		if("1".equals(state)) {
			String discount = request.getParameter("discount");
			a.setDiscount(Double.valueOf(discount));
		}
		a.setIsDiscount(state);
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> saveGoodsIsOnSale(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		GoodsEntity a = get(GoodsEntity.class, Long.valueOf(id));
		String isOnSale = request.getParameter("isOnSale");
		a.setIsOnSale(isOnSale);
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

}
