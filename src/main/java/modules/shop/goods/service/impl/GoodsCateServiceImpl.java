package modules.shop.goods.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.HqlQuery;
import common.util.StringUtil;
import modules.file.entity.FileCateEntity;
import modules.shop.goods.entity.CateEntity;
import modules.shop.goods.service.GoodsCateService;

@Service
public class GoodsCateServiceImpl extends CommonServiceImpl implements GoodsCateService{

	@Override
	public DataTableReturn loadCateByPage(JQueryDataTables dataTable, HttpServletRequest request) {
		StringBuffer hql = new StringBuffer(
				" from CateEntity as  a where status = 1 ");
		List<Object> params = new ArrayList<Object>();
		String name = request.getParameter("name");
		if (StringUtil.isNotEmpty(name)) {
			hql.append(" and a.name like ? ");
			params.add("%" + name + "%");
		}
		hql.append(" order by a.orderNum2 desc,a.parentCate,a.orderNum desc");
		HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
		int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
		hqlQuery.setCurPage(currPage + 1);
		hqlQuery.setPageSize(dataTable.getDisplayLength());
		DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
		List<CateEntity> pList = (List<CateEntity>) d.getAaData();
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		for (CateEntity p : pList) {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("id", p.getId());
			item.put("name", p.getName());
			item.put("imgUrl", p.getImgUrl());
			item.put("parentCate", "-");
			if(p.getParentCate() != null) {
				item.put("parentCate", p.getParentCate().getName());
				item.put("name", "&nbsp;&nbsp;&nbsp;&nbsp;├─" + p.getName());
			}
			item.put("orderNum", p.getOrderNum());
			item.put("status", p.getStatus());
			item.put("time",DateUtil.getTimeByCustomPattern( p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
			mapList.add(item);
		}
		d.setAaData(mapList);
		d.setsEcho(dataTable.getEcho());
		return d;
	}

	@Override
	public Map<String, Object> saveCate(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String parent = request.getParameter("parent");
		String orderNum = request.getParameter("orderNum");
		String imgUrl = request.getParameter("imgUrl");
		CateEntity a = null;
		if(StringUtil.isNotEmpty(id)) {
			a = get(CateEntity.class, Long.valueOf(id));
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
		}
		else {
			a = new CateEntity();
			a.setCreateDate(DateUtil.getCurrentTime());
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
		}
		if(StringUtil.isNotEmpty(parent)) {
			a.setParentCate(get(CateEntity.class, Long.valueOf(parent)));
			
			//二级分类
			a.setOrderNum(Integer.parseInt(orderNum));
			a.setOrderNum2(a.getParentCate().getOrderNum2());
		}
		else {
			a.setOrderNum(Integer.parseInt(orderNum));
			a.setOrderNum2(Integer.parseInt(orderNum));
		}
		
		
		a.setImgUrl(imgUrl);
		a.setName(name);
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> deleteCate(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		CateEntity a = get(CateEntity.class, Long.valueOf(id));
		if (a == null) { 
			map.put("code", 1);
			return map;
		}
		a.setStatus("0");
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

}
