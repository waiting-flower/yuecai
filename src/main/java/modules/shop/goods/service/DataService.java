package modules.shop.goods.service;

import javax.servlet.http.HttpServletRequest;

import common.service.CommonService;

import java.util.List;
import java.util.Map;

public interface DataService extends CommonService{

	void initDongtaiData(HttpServletRequest request);

	void initKehuData(HttpServletRequest request);

	void initGoodsData(HttpServletRequest request);

	void initOrdersData(HttpServletRequest request);

	void initOrderData(HttpServletRequest request);

	List<Map<String, Object>> initOrderExcelData(HttpServletRequest request);

	void initYewuyuanData(HttpServletRequest request);

	void initUserData(HttpServletRequest request);

}
