package modules.shop.goods.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;

public interface GoodsCateService extends CommonService{

	DataTableReturn loadCateByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveCate(HttpServletRequest request);

	Map<String, Object> deleteCate(HttpServletRequest request);

}
