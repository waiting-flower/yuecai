package modules.shop.goods.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;

public interface BrandService extends CommonService{

	DataTableReturn loadBrandByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveBrand(HttpServletRequest request);

	Map<String, Object> deleteBrand(HttpServletRequest request);

	Map<String, Object> getBrandList(HttpServletRequest request);

}
