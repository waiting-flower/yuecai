package modules.shop.goods.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import common.entity.CommonEntity;

/**
 * 轮播对象
 * @author Administrator
 *
 */
@Entity
@Table(name = "shop_banner", schema = "")
public class BannerEntity extends CommonEntity{
	private String imgUrl;//展示的图片
	private String title;//标题名称，只是方面后台管理
	private Integer orderNum;//权重，展示排序，越大越靠前
	private String info;//图文详情
	private String target;//如果不跳转，此值未none;如果是查看详情，那么此值为info;如果是跳转页面，此值为action
	private String pagePath;//跳转页面的路径
	private String pageOptions;//跳转页面时候的参数
	
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getPagePath() {
		return pagePath;
	}
	public void setPagePath(String pagePath) {
		this.pagePath = pagePath;
	}
	public String getPageOptions() {
		return pageOptions;
	}
	public void setPageOptions(String pageOptions) {
		this.pageOptions = pageOptions;
	}
	
	
	
}
