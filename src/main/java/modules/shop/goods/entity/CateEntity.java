package modules.shop.goods.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;

/**
 * 分类对象【只支持二级分类，暂未支持3级分类】
 * @author Administrator
 *
 */
@Entity
@Table(name = "shop_cate", schema = "")
public class CateEntity extends CommonEntity{
	private CateEntity cate;
	private String name;
	private String imgUrl;
	private Integer orderNum;
	private Integer orderNum2;
	private CateEntity parentCate;//上级分类
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public Integer getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parentId")
	public CateEntity getParentCate() {
		return parentCate;
	}
	public void setParentCate(CateEntity parentCate) {
		this.parentCate = parentCate;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cateId")
	public CateEntity getCate() {
		return cate;
	}
	public void setCate(CateEntity cate) {
		this.cate = cate;
	}
	public Integer getOrderNum2() {
		return orderNum2;
	}
	public void setOrderNum2(Integer orderNum2) {
		this.orderNum2 = orderNum2;
	}
	
	
}
