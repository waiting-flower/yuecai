package modules.shop.goods.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
import modules.shop.user.entity.WUserEntity;

/**
 * 商品评论对象
 * @author lpf0
 *
 */
@Entity
@Table(name = "shop_comment", schema = "")
public class CommentEntity extends CommonEntity{
	private WUserEntity user;
	private GoodsEntity goods;
	private String comType;//评论类型   1：商品评论  2：文章评论  3：商品评论的评论
	private String msgContent;//评论的内容
	private String msgTime;//评论的时间
	private Long otherId;//对应的其他对象ID  comType=1 对应为商品id  3对应为评论id
	private String replyToNick;//回复谁谁谁
	private Integer raiseNum;//其他人给该评论点赞的数量
	private Integer replyNum;//回复的数量
	private Integer commentScore;//评论商品的分数
	private String imgList;//晒单的图片列表
	private String sku;//对应的商品参数
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public WUserEntity getUser() {
		return user;
	}
	public void setUser(WUserEntity user) {
		this.user = user; 
	}
	public String getComType() {
		return comType;
	}
	public void setComType(String comType) {
		this.comType = comType;
	}
	public String getMsgContent() {
		return msgContent;
	}
	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}
	public String getMsgTime() {
		return msgTime;
	}
	public void setMsgTime(String msgTime) {
		this.msgTime = msgTime;
	}
	public Long getOtherId() {
		return otherId;
	}
	public void setOtherId(Long otherId) {
		this.otherId = otherId;
	}
	public Integer getRaiseNum() {
		return raiseNum;
	}
	public void setRaiseNum(Integer raiseNum) {
		this.raiseNum = raiseNum;
	}
	public String getImgList() {
		return imgList;
	}
	public void setImgList(String imgList) {
		this.imgList = imgList;
	}
	public Integer getCommentScore() {
		return commentScore;
	}
	public void setCommentScore(Integer commentScore) {
		this.commentScore = commentScore;
	}
	public Integer getReplyNum() {
		return replyNum;
	}
	public void setReplyNum(Integer replyNum) {
		this.replyNum = replyNum;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getReplyToNick() {
		return replyToNick;
	}
	public void setReplyToNick(String replyToNick) {
		this.replyToNick = replyToNick;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "goodsId")
	public GoodsEntity getGoods() {
		return goods;
	}
	public void setGoods(GoodsEntity goods) {
		this.goods = goods;
	}
	
	
	
}
