package modules.shop.goods.entity;

import common.entity.CommonEntity;

import javax.persistence.*;

/**
 * 商品销售属性
 * @author Administrator
 *
 */
@Entity
@Table(name = "shop_goods_sku", schema = "")
public class GoodsSkuEntity extends CommonEntity{
	private GoodsEntity goods;//商品
	private String skuName;
	private String paramsName;
	private String parmasValuesJson;//参数json对象 例如{"颜色" : "白色","尺码":"M","产地":"中国"}
	private String imgUrl;//显示的商品图片
	private Double price;//价格
	private Double oldPrice;//虚拟价格【只是展示用的】
	private Integer stockNum;//库存数量
	private Integer saleNum;//0销售数量
	private String unit;//单位
	private String isNew;
	private Integer disbuylimit;

	public String getIsNew() {
		return isNew;
	}

	public void setIsNew(String isNew) {
		this.isNew = isNew;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "goodsId")
	public GoodsEntity getGoods() {
		return goods;
	}
	public void setGoods(GoodsEntity goods) {
		this.goods = goods;
	}
	public String getSkuName() {
		return skuName;
	}
	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}
	public String getParamsName() {
		return paramsName;
	}
	public void setParamsName(String paramsName) {
		this.paramsName = paramsName;
	}
	public String getParmasValuesJson() {
		return parmasValuesJson;
	}
	public void setParmasValuesJson(String parmasValuesJson) {
		this.parmasValuesJson = parmasValuesJson;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Integer getStockNum() {
		return stockNum;
	}
	public void setStockNum(Integer stockNum) {
		this.stockNum = stockNum;
	}
	public Double getOldPrice() {
		return oldPrice;
	}
	public void setOldPrice(Double oldPrice) {
		this.oldPrice = oldPrice;
	}
	public Integer getSaleNum() {
		return saleNum;
	}
	public void setSaleNum(Integer saleNum) {
		this.saleNum = saleNum;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getDisbuylimit() {
		return disbuylimit;
	}
	public void setDisbuylimit(Integer disbuylimit) {
		this.disbuylimit = disbuylimit;
	}
}
