package modules.shop.goods.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;

/**
 * 商品对象
 * 参加特价折扣的，不能参加秒杀
 *
 * @author Administrator
 */
@Entity
@Table(name = "shop_goods", schema = "")
public class GoodsEntity extends CommonEntity {
    private CateEntity cate;
    private BrandEntity brand;
    private String name;//主标题
    private String name2;//副标题
    private String goodsNo;//商品编码
    private String tiaoxingma;//商品条形码
    private Double price;//售价
    private Double oldPrice;//吊牌价
    private String imgUrl;//商品封面图
    private String imgList;//商品轮播图，如果没有则取封面图
    private String info;//商品图片详情
    private String goodsType;//商品消费方式 1：普通商品  2：虚拟商品  3：到店消费
    private String cornerImg;//商品角标

    private Integer stockNum;//商品库存
    private String skuJson;//sku
    private String skuItem;

    private String isOnSale;//是否上架  1上架中  2已下架  0待上架,在仓库中

    private String isRec;//是否推荐热销
    private String isDiscount;//是否折扣商品
    private Double discount;//折扣的值
    private Integer dislimit;

    private Integer saleNum;//总销量
    private Integer lookNum;//查看次数
    private Integer commentNum;//评论数量
    private Double score;//用户的评分
    private Integer niceCommentNum;//商品好评数量
    private Double niceComment;//好评度 4分  5分为好评的用户   niceCommentNum/commentNum

    private Long seckillId;//秒杀活动ID
    private Integer sort;//秒杀活动ID

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(Double oldPrice) {
        this.oldPrice = oldPrice;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getImgList() {
        return imgList;
    }

    public void setImgList(String imgList) {
        this.imgList = imgList;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getIsOnSale() {
        return isOnSale;
    }

    public void setIsOnSale(String isOnSale) {
        this.isOnSale = isOnSale;
    }

    public String getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(String goodsType) {
        this.goodsType = goodsType;
    }

    public String getCornerImg() {
        return cornerImg;
    }

    public void setCornerImg(String cornerImg) {
        this.cornerImg = cornerImg;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public String getSkuJson() {
        return skuJson;
    }

    public void setSkuJson(String skuJson) {
        this.skuJson = skuJson;
    }

    public Integer getLookNum() {
        return lookNum;
    }

    public void setLookNum(Integer lookNum) {
        this.lookNum = lookNum;
    }

    public Integer getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(Integer commentNum) {
        this.commentNum = commentNum;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Integer getNiceCommentNum() {
        return niceCommentNum;
    }

    public void setNiceCommentNum(Integer niceCommentNum) {
        this.niceCommentNum = niceCommentNum;
    }

    public Double getNiceComment() {
        return niceComment;
    }

    public void setNiceComment(Double niceComment) {
        this.niceComment = niceComment;
    }

    public Integer getSaleNum() {
        return saleNum;
    }

    public void setSaleNum(Integer saleNum) {
        this.saleNum = saleNum;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cateId")
    public CateEntity getCate() {
        return cate;
    }

    public void setCate(CateEntity cate) {
        this.cate = cate;
    }

    public String getSkuItem() {
        return skuItem;
    }

    public void setSkuItem(String skuItem) {
        this.skuItem = skuItem;
    }

    public String getIsRec() {
        return isRec;
    }

    public void setIsRec(String isRec) {
        this.isRec = isRec;
    }

    public String getIsDiscount() {
        return isDiscount;
    }

    public void setIsDiscount(String isDiscount) {
        this.isDiscount = isDiscount;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "brandId")
    public BrandEntity getBrand() {
        return brand;
    }

    public void setBrand(BrandEntity brand) {
        this.brand = brand;
    }

    public Long getSeckillId() {
        return seckillId;
    }

    public void setSeckillId(Long seckillId) {
        this.seckillId = seckillId;
    }

    public String getGoodsNo() {
        return goodsNo;
    }

    public void setGoodsNo(String goodsNo) {
        this.goodsNo = goodsNo;
    }

    public String getTiaoxingma() {
        return tiaoxingma;
    }

    public void setTiaoxingma(String tiaoxingma) {
        this.tiaoxingma = tiaoxingma;
    }

    public Integer getDislimit() {
        return dislimit;
    }

    public void setDislimit(Integer dislimit) {
        this.dislimit = dislimit;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
