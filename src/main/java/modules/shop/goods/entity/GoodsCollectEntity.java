package modules.shop.goods.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
import modules.shop.user.entity.WUserEntity;


/**
 * 商品的收藏对象
 * @author Administrator
 *
 */
@Entity
@Table(name = "shop_goods_collect", schema = "")
public class GoodsCollectEntity  extends CommonEntity{
	private GoodsEntity goods;//商品
	private WUserEntity user;//用户
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "goodsId")
	public GoodsEntity getGoods() {
		return goods;
	}
	public void setGoods(GoodsEntity goods) {
		this.goods = goods;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public WUserEntity getUser() {
		return user;
	}
	public void setUser(WUserEntity user) {
		this.user = user;
	}
}
