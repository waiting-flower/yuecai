package modules.shop.goods.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import common.entity.CommonEntity;

/**
 * 商城配置对象
 * @author Administrator
 *
 */
@Entity 
@Table(name = "shop_config", schema = "")
public class ConfigEntity extends CommonEntity{
	private String notice;//首页跑马灯公告
	private String mobile;
	private String goodsRule;//商品说明
	private String cancleReason;//取消订单的原因
	private String refoudReason;//订单退款的原因
	private String afterReason;//售后的原因
	private Integer cancleOrderTime;//订单多久时间取消，单位为小时
	private Integer salesAfterDay;//售后后多少天提供售后
	private Integer receiverDay;//默认收货周期，天
	private String appid;//客户端小程序appid
	private String appSecret;//客户端小程序appsecret
	private String mchId;//商户id
	private String mchSecret;//商户支付秘钥
	private String certPath;//证书路径
	private String shareTitle;//分享的标题
	private String shareImgUrl;//分享的图片
	private String xianzhi;//订单购物金额限制
	public String getGoodsRule() {
		return goodsRule;
	}
	public void setGoodsRule(String goodsRule) {
		this.goodsRule = goodsRule;
	}
	public String getCancleReason() {
		return cancleReason;
	}
	public void setCancleReason(String cancleReason) {
		this.cancleReason = cancleReason;
	}
	public String getRefoudReason() {
		return refoudReason;
	}
	public void setRefoudReason(String refoudReason) {
		this.refoudReason = refoudReason;
	}
	public String getAfterReason() {
		return afterReason;
	}
	public void setAfterReason(String afterReason) {
		this.afterReason = afterReason;
	}
	public Integer getCancleOrderTime() {
		return cancleOrderTime;
	}
	public void setCancleOrderTime(Integer cancleOrderTime) {
		this.cancleOrderTime = cancleOrderTime;
	}
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getMchId() {
		return mchId;
	}
	public void setMchId(String mchId) {
		this.mchId = mchId;
	}
	public String getMchSecret() {
		return mchSecret;
	}
	public void setMchSecret(String mchSecret) {
		this.mchSecret = mchSecret;
	}
	public String getCertPath() {
		return certPath;
	}
	public void setCertPath(String certPath) {
		this.certPath = certPath;
	}
	public String getAppSecret() {
		return appSecret;
	}
	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}
	public String getShareTitle() {
		return shareTitle;
	}
	public void setShareTitle(String shareTitle) {
		this.shareTitle = shareTitle;
	}
	public String getShareImgUrl() {
		return shareImgUrl;
	}
	public void setShareImgUrl(String shareImgUrl) {
		this.shareImgUrl = shareImgUrl;
	}
	public Integer getSalesAfterDay() {
		return salesAfterDay;
	}
	public void setSalesAfterDay(Integer salesAfterDay) {
		this.salesAfterDay = salesAfterDay;
	}
	public Integer getReceiverDay() {
		return receiverDay;
	}
	public void setReceiverDay(Integer receiverDay) {
		this.receiverDay = receiverDay;
	}
	public String getNotice() {
		return notice;
	}
	public void setNotice(String notice) {
		this.notice = notice;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getXianzhi() {
		return xianzhi;
	}
	public void setXianzhi(String xianzhi) {
		this.xianzhi = xianzhi;
	}
}
