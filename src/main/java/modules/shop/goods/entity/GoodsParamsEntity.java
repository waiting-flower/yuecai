package modules.shop.goods.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;

/**
 * 商品规格属性
 * 例如：白色 LM
 * @author Administrator
 *
 */
@Entity
@Table(name = "shop_goods_params", schema = "")
public class GoodsParamsEntity extends CommonEntity{
	private GoodsEntity goods;//对应的商品
	private String paramsName;//规格项目名称 例如：颜色
	private String parmasValues;//规格具体的值  例如：白色，红色，黄色
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "goodsId")
	public GoodsEntity getGoods() {
		return goods;
	}
	public void setGoods(GoodsEntity goods) {
		this.goods = goods;
	}
	public String getParamsName() {
		return paramsName;
	}
	public void setParamsName(String paramsName) {
		this.paramsName = paramsName;
	}
	public String getParmasValues() {
		return parmasValues;
	}
	public void setParmasValues(String parmasValues) {
		this.parmasValues = parmasValues;
	}
	
	
}
