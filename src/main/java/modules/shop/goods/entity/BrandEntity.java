package modules.shop.goods.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import common.entity.CommonEntity;

/**
 * 品牌对象
 * @author Administrator
 *
 */
@Entity 
@Table(name = "shop_brand", schema = "")
public class BrandEntity extends CommonEntity{
	private String name;
	private String imgUrl;
	private Integer orderNum;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public Integer getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
	
}
