package modules.shop.indexad.entity;

import common.entity.CommonEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "index_ad", schema = "")
public class IndexAdEntity extends CommonEntity{
	private String content;//展示的图片
	private String remark;//标题名称，只是方面后台管理

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
