package modules.shop.indexad.service;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;
import modules.shop.indexad.entity.IndexAdEntity;

import javax.servlet.http.HttpServletRequest;

public interface IndexAdService extends CommonService {


    DataTableReturn loadAdByPage(JQueryDataTables dataTable, HttpServletRequest request);

    IndexAdEntity getCurrent();

}
