package modules.shop.indexad.service.impl;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.HqlQuery;
import modules.shop.indexad.entity.IndexAdEntity;
import modules.shop.indexad.service.IndexAdService;
import org.hibernate.Query;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IndexAdServiceImpl extends CommonServiceImpl implements IndexAdService {


    @Override
    public DataTableReturn loadAdByPage(JQueryDataTables dataTable, HttpServletRequest request) {
        StringBuffer hql = new StringBuffer(
                " from IndexAdEntity as  a where  a.status = '1' ");
        List<Object> params = new ArrayList<Object>();
        String title = request.getParameter("title");
        hql.append(" order by  a.createDate desc");
        HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
        int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
        hqlQuery.setCurPage(currPage + 1);
        hqlQuery.setPageSize(dataTable.getDisplayLength());
        DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
        List<IndexAdEntity> pList = (List<IndexAdEntity>) d.getAaData();
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (IndexAdEntity p : pList) {
            Map<String, Object> item = new HashMap<String, Object>();
            item.put("id", p.getId());
            item.put("content", p.getContent());
            item.put("status", p.getStatus());
            item.put("time", DateUtil.getTimeByCustomPattern(p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
            mapList.add(item);
        }
        d.setAaData(mapList);
        d.setsEcho(dataTable.getEcho());
        return d;
    }

    @Override
    public IndexAdEntity getCurrent() {
        StringBuffer hql = new StringBuffer(
                " from IndexAdEntity as  a order by createDate desc");
        Query query = commonDao.getSession().createQuery(hql.toString());
        query.setFirstResult(0);
        query.setMaxResults(1);
        List lists = query.list();
        if (!lists.isEmpty()) {
            return (IndexAdEntity) lists.get(0);
        }
        return null;
    }

}