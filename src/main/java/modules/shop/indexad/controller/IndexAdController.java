package modules.shop.indexad.controller;

import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.util.DateUtil;
import common.util.StringUtil;
import modules.shop.ad.service.AdService;
import modules.shop.indexad.entity.IndexAdEntity;
import modules.shop.indexad.service.IndexAdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * APP启动广告控制层
 *
 * @author Administrator
 */
@Controller
@RequestMapping(value = "admin/shop/indexad")
public class IndexAdController extends BaseController {

    @Autowired
    IndexAdService service;

    @Autowired
    AdService adService;

    /**
     * 轮播管理列表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "list")
    public String cateList(HttpServletRequest request, HttpServletResponse response) {
        return "admin/view/shop/indexad/adList";
    }

    /**
     * 轮播详情
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "info")
    public String info(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("id");
        if (StringUtil.isNotEmpty(id)) {
            IndexAdEntity menu = service.get(IndexAdEntity.class, Long.valueOf(id));
            request.setAttribute("a", menu);
        }
        return templatePath + "shop/indexad/adInfo";
    }


    /**
     * 分页获取商城轮播列表数据
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "loadAdByPage")
    @ResponseBody
    public DataTableReturn loadBannerByPage(HttpServletRequest request) {
        JQueryDataTables dataTable = new JQueryDataTables(request);
        DataTableReturn d = service.loadAdByPage(dataTable, request);
        return d;
    }

    /**
     * 保存一个商城轮播
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveAd")
    @ResponseBody
    public Map<String, Object> saveAd(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String id = request.getParameter("id");
        String content = request.getParameter("content");
        IndexAdEntity a = null;
        if (StringUtil.isNotEmpty(id)) {
            a = service.get(IndexAdEntity.class, Long.valueOf(id));
            a.setUpdateDate(DateUtil.getCurrentTime());
            a.setStatus("1");
        }else{
            a = new IndexAdEntity();
            a.setCreateDate(DateUtil.getCurrentTime());
            a.setUpdateDate(DateUtil.getCurrentTime());
            a.setStatus("1");
        }
        a.setContent(content);
        service.saveOrUpdate(a);
        map.put("code", 0);
        return map;
    }

    /**
     * 删除一个商城轮播
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "deleteAd")
    @ResponseBody
    public Map<String, Object> deleteAd(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String id = request.getParameter("id");
        IndexAdEntity a = service.get(IndexAdEntity.class, Long.valueOf(id));
        if (a == null) {
            map.put("code", 1);
            return map;
        }
        a.setStatus("0");
        service.saveOrUpdate(a);
        map.put("code", 0);
        return map;
    }
}
