package modules.shop.freight;

import common.entity.CommonEntity;

/**
 * 运费配置数据
 * @author Administrator
 *
 */
public class FreightConfigEntity extends CommonEntity{
	/*
	 * 订单运费金额固定
	 */
	private Double baseFreight;
	
	/*
	 * 满包邮设置：满多少元订单包邮
	 */
	private Double freeFreightOrderMoney;
	
}
