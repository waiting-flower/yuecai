package modules.shop.user.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
/**
 * 用户地址数据
 * @author Administrator
 *
 */
@Entity 
@Table(name = "shop_user_address", schema = "")
public class AddressEntity extends CommonEntity{
	private WUserEntity user;
	private String pro;
	private String city;
	private String county;
	private String name;
	private String mobile;
	private String info;
	private String isDefault;//是否为默认地址
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public WUserEntity getUser() {
		return user;
	}
	public void setUser(WUserEntity user) {
		this.user = user;
	}
	public String getPro() {
		return pro;
	}
	public void setPro(String pro) {
		this.pro = pro;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}
	
}
