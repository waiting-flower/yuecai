package modules.shop.user.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
/**
 * 平台用户对象
 * @author Administrator
 *
 */
@Entity 
@Table(name = "shop_user", schema = "")
public class WUserEntity extends CommonEntity{
	private String name;//店铺名称
	private String nick;//用户的昵称
	private String headUrl;
	private String mobile;//手机
	private String password;//密码
	private UserTypeEntity userType;
	
	private String isYewuyuan;
	private Date lastBuyDate;
	
	private String  pro;//省份
	private String city;//城市
	private String county;//区县
	private String address;//地址
	private String device;//设备类型
	private String regIp;//注册ip
	private Date regDate;//注册时间 
	private String lastLoginIp;//最后登录ip
	private Date lastLoginDate;//最后登录时间
	private Integer loginCount;//登录次数
	private String isAuth;//是否认证
	private String isDelete;//是否删除
	private String shareCode;//邀请码
	private String bak;//用户备注信息
	private WUserEntity parentUser;
	
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userTypeId")
	public UserTypeEntity getUserType() {
		return userType;
	}
	public void setUserType(UserTypeEntity userType) {
		this.userType = userType;
	}
	public String getPro() {
		return pro;
	}
	public void setPro(String pro) {
		this.pro = pro;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getRegIp() {
		return regIp;
	}
	public void setRegIp(String regIp) {
		this.regIp = regIp;
	}
	public Date getRegDate() {
		return regDate;
	}
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
	public String getLastLoginIp() {
		return lastLoginIp;
	}
	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}
	public Date getLastLoginDate() {
		return lastLoginDate;
	}
	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}
	public Integer getLoginCount() {
		return loginCount;
	}
	public void setLoginCount(Integer loginCount) {
		this.loginCount = loginCount;
	}
	public String getIsAuth() {
		return isAuth;
	}
	public void setIsAuth(String isAuth) {
		this.isAuth = isAuth;
	}
	public String getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}
	public String getShareCode() {
		return shareCode;
	}
	public void setShareCode(String shareCode) {
		this.shareCode = shareCode;
	}
	public String getBak() {
		return bak;
	}
	public void setBak(String bak) {
		this.bak = bak;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHeadUrl() {
		return headUrl;
	}
	public void setHeadUrl(String headUrl) {
		this.headUrl = headUrl;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parentUserId")  
	public WUserEntity getParentUser() {
		return parentUser;
	}
	public void setParentUser(WUserEntity parentUser) {
		this.parentUser = parentUser;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public Date getLastBuyDate() {
		return lastBuyDate;
	}
	public void setLastBuyDate(Date lastBuyDate) {
		this.lastBuyDate = lastBuyDate;
	}
	public String getIsYewuyuan() {
		return isYewuyuan;
	}
	public void setIsYewuyuan(String isYewuyuan) {
		this.isYewuyuan = isYewuyuan;
	}
	
	
	
}
