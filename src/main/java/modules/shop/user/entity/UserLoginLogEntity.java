package modules.shop.user.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;

/**
 * 用户登录日志
 * @author Administrator
 *
 */
@Entity 
@Table(name = "shop_user_login_log", schema = "")
public class UserLoginLogEntity extends CommonEntity{
	private WUserEntity user;//登录用户
	private String day;//登录日期
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public WUserEntity getUser() {
		return user;
	}
	public void setUser(WUserEntity user) {
		this.user = user;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	
}
