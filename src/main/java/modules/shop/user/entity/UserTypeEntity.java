package modules.shop.user.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import common.entity.CommonEntity;

/**
 * 用户类型
 * @author Administrator
 *
 */
@Entity 
@Table(name = "shop_user_type", schema = "")
public class UserTypeEntity extends CommonEntity{
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	 
}
