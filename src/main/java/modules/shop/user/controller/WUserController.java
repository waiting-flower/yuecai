package modules.shop.user.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.util.DateUtil;
import common.util.StringUtil;
import modules.shop.coupon.entity.CouponEntity;
import modules.shop.user.entity.UserTypeEntity;
import modules.shop.user.entity.WUserEntity;
import modules.shop.user.service.UserTypeService;
import modules.shop.user.service.WUserService;
import modules.system.entity.UserEntity;


@Controller
@RequestMapping(value = "admin/shop/user")
public class WUserController extends BaseController {

    @Autowired
    UserTypeService userTypeService;

    @Autowired
    WUserService service;

    /**
     * 店铺类型列表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "userTypeList")
    public String userTypeList(HttpServletRequest request, HttpServletResponse response) {
        return templatePath + "shop/user/userTypeList";
    }

    /**
     * 店铺类型详情
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "userTypeInfo")
    public String userTypeInfo(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("id");
        if (StringUtil.isNotEmpty(id)) {
            UserTypeEntity menu = userTypeService.get(UserTypeEntity.class, Long.valueOf(id));
            request.setAttribute("a", menu);
        }
        return templatePath + "shop/user/userTypeInfo";
    }


    /**
     * 分页获取店铺类型列表数据
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "loadUserTypeByPage")
    @ResponseBody
    public DataTableReturn loadUserTypeByPage(HttpServletRequest request) {
        JQueryDataTables dataTable = new JQueryDataTables(request);
        DataTableReturn d = userTypeService.loadUserTypeByPage(dataTable, request);
        return d;
    }

    /**
     * 保存一个店铺类型对象
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveUserType")
    @ResponseBody
    public Map<String, Object> saveUserType(HttpServletRequest request) {
        return userTypeService.saveUserType(request);
    }

    /**
     * 删除一个店铺类型对象
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "deleteUserType")
    @ResponseBody
    public Map<String, Object> deleteUserType(HttpServletRequest request) {
        return userTypeService.deleteUserType(request);
    }

    /**
     * 用户地址列表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "addressList")
    public String addressList(HttpServletRequest request, HttpServletResponse response) {
        return templatePath + "shop/user/addressList";
    }


    /**
     * 分页获取用户地址列表数据
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "loadAddressByPage")
    @ResponseBody
    public DataTableReturn loadAddressByPage(HttpServletRequest request) {
        JQueryDataTables dataTable = new JQueryDataTables(request);
        DataTableReturn d = service.loadAddressByPage(dataTable, request);
        return d;
    }


    /**
     * 用户列表
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "userList")
    public String userList(HttpServletRequest request, HttpServletResponse response) {
        return templatePath + "shop/user/userList";
    }

    /**
     * 用户详情
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "userInfo")
    public String userInfo(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("id");
        if (StringUtil.isNotEmpty(id)) {
            WUserEntity menu = service.get(WUserEntity.class, Long.valueOf(id));
            request.setAttribute("a", menu);
        }
        String hql = "from UserTypeEntity as a where a.status ='1' order by a.createDate desc";
        List<UserTypeEntity> userTypeList = service.findByQueryString(hql);
        request.setAttribute("userTypeList", userTypeList);
        return templatePath + "shop/user/userInfo";
    }

    /**
     * 用户可用优惠券
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "couponInfo")
    public String couponInfo(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("id");
        if (StringUtil.isNotEmpty(id)) {
            WUserEntity menu = service.get(WUserEntity.class, Long.valueOf(id));
            request.setAttribute("a", menu);
        }
        return templatePath + "shop/user/couponInfo";
    }

    /**
     * 用户历史订单
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "orderInfo")
    public String orderInfo(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("id");
        if (StringUtil.isNotEmpty(id)) {
            WUserEntity menu = service.get(WUserEntity.class, Long.valueOf(id));
            request.setAttribute("a", menu);
        }
        return templatePath + "shop/user/orderInfo";
    }

    /**
     * 发放优惠券
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "faInfo")
    public String faInfo(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("id");
        if (StringUtil.isNotEmpty(id)) {
            WUserEntity menu = service.get(WUserEntity.class, Long.valueOf(id));
            request.setAttribute("a", menu);
        }
        String hql = "from CouponEntity as a where a.status ='1' and a.endTime > ? order by a.createDate desc";
        List<CouponEntity> couponList = service.findHql(hql, new Object[]{DateUtil.getCurrentTime()});
        request.setAttribute("couponList", couponList);
        return templatePath + "shop/user/faInfo";
    }

    /**
     * 分页获取用户列表数据
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "loadUserByPage")
    @ResponseBody
    public DataTableReturn loadUserByPage(HttpServletRequest request) {
        JQueryDataTables dataTable = new JQueryDataTables(request);
        DataTableReturn d = service.loadUserByPage(dataTable, request);
        return d;
    }

    @RequestMapping(value = "loadUserCouponByPage")
    @ResponseBody
    public DataTableReturn loadUserCouponByPage(HttpServletRequest request) {
        JQueryDataTables dataTable = new JQueryDataTables(request);
        DataTableReturn d = service.loadUserCouponByPage(dataTable, request);
        return d;
    }

    /**
     * 保存一个用户对象
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveUser")
    @ResponseBody
    public Map<String, Object> saveUser(HttpServletRequest request) {
        return service.saveUser(request);
    }

    @RequestMapping(value = "saveUserCoupon")
    @ResponseBody
    public Map<String, Object> saveUserCoupon(HttpServletRequest request) {
        return service.saveUserCoupon(request);
    }

    /**
     * 删除一个用户对象
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "deleteUser")
    @ResponseBody
    public Map<String, Object> deleteUser(HttpServletRequest request) {
        return service.deleteUser(request);
    }

    /**
     * 修改推荐人推荐码
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "editTShareCode")
    @ResponseBody
    public Map<String, Object> editTShareCode(HttpServletRequest request) {
        return service.editTShareCode(request);
    }


}
