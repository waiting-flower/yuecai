package modules.shop.user.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Service;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.HqlQuery;
import common.util.StringUtil;
import modules.shop.coupon.entity.CouponEntity;
import modules.shop.coupon.entity.WUserCouponEntity;
import modules.shop.driver.entity.DriverEntity;
import modules.shop.order.entity.OrderEntity;
import modules.shop.user.entity.AddressEntity;
import modules.shop.user.entity.UserTypeEntity;
import modules.shop.user.entity.WUserEntity;
import modules.shop.user.service.WUserService;
import org.springframework.transaction.annotation.Transactional;

@Service
public class WUserServiceImpl extends CommonServiceImpl implements WUserService {

    @Override
    public DataTableReturn loadAddressByPage(JQueryDataTables jt, HttpServletRequest request) {
        StringBuffer hql = new StringBuffer(
                " from AddressEntity as  a where 1 = 1 ");
        List<Object> params = new ArrayList<Object>();
        String name = request.getParameter("name");
        if (StringUtil.isNotEmpty(name)) {
            hql.append(" and a.name like ? ");
            params.add("%" + name + "%");
        }
        String mobile = request.getParameter("mobile");
        if (StringUtil.isNotEmpty(mobile)) {
            hql.append(" and a.mobile like ? ");
            params.add("%" + mobile + "%");
        }
        hql.append(" and a.status = '1'");
        hql.append(" order by a.createDate desc");
        HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
        int currPage = jt.getDisplayStart() / jt.getDisplayLength();
        hqlQuery.setCurPage(currPage + 1);
        hqlQuery.setPageSize(jt.getDisplayLength());
        DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
        List<AddressEntity> pList = (List<AddressEntity>) d.getAaData();
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (AddressEntity p : pList) {
            Map<String, Object> item = new HashMap<String, Object>();
            item.put("id", p.getId());
            item.put("name", p.getUser().getName());
            item.put("nick", p.getUser().getNick());
            if (StringUtil.isEmpty(p.getUser().getName())) {
                item.put("name", "--");
            }
            if (StringUtil.isEmpty(p.getUser().getNick())) {
                item.put("nick", "--");
            }
            item.put("headUrl", p.getUser().getHeadUrl());
            item.put("address", p.getPro() + p.getCity() + p.getCounty() + p.getInfo());
            item.put("name", p.getName());
            item.put("mobile", p.getMobile());
            item.put("isDefault", p.getIsDefault());
            item.put("status", p.getStatus());
            item.put("time", DateUtil.getTimeByCustomPattern(p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
            mapList.add(item);
        }
        d.setAaData(mapList);
        d.setsEcho(jt.getEcho());
        return d;
    }

    @Override
    public DataTableReturn loadUserByPage(JQueryDataTables jt, HttpServletRequest request) {
        StringBuffer hql = new StringBuffer(
                " from WUserEntity as  a where 1 = 1 ");
        List<Object> params = new ArrayList<Object>();
        String tshareCode = request.getParameter("tshareCode");
        if (StringUtil.isNotEmpty(tshareCode)) {
            hql.append(" and a.parentUser.id in (select b.id from WUserEntity as b where b.shareCode = '" + tshareCode + "') ");
        } else {
            String name = request.getParameter("name");
            if (StringUtil.isNotEmpty(name)) {
                hql.append(" and a.name like ? ");
                params.add("%" + name + "%");
            }
            String shareCode = request.getParameter("shareCode");
            if (StringUtil.isNotEmpty(shareCode)) {
                hql.append(" and a.shareCode like ? ");
                params.add("%" + shareCode + "%");
            }
            String beginTime = request.getParameter("beginTime");
            if (StringUtil.isNotEmpty(beginTime)) {
                hql.append(" and a.createDate >= ? ");
                params.add(DateUtil.parse(beginTime, DateUtil.yyyyMMdd));
            }
            String endTime = request.getParameter("endTime");
            if (StringUtil.isNotEmpty(endTime)) {
                hql.append(" and a.createDate <= ? ");
                params.add(DateUtil.parse(endTime, DateUtil.yyyyMMdd));
            }
            String mobile = request.getParameter("mobile");
            if (StringUtil.isNotEmpty(mobile)) {
                hql.append(" and a.mobile like ? ");
                params.add("%" + mobile + "%");
            }
        }

        hql.append(" and a.status = '1'");
        hql.append(" order by a.createDate desc");
        HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
        int currPage = jt.getDisplayStart() / jt.getDisplayLength();
        hqlQuery.setCurPage(currPage + 1);
        hqlQuery.setPageSize(jt.getDisplayLength());
        DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
        List<WUserEntity> pList = (List<WUserEntity>) d.getAaData();
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (WUserEntity p : pList) {
            Map<String, Object> item = new HashMap<String, Object>();
			/*
			 * private String name;//店铺名称
	private String nick;//用户的昵称
	private String headUrl;
	private String mobile;//手机
	private String password;//密码
	private UserTypeEntity userType;
	private String  pro;//省份
	private String city;//城市
	private String county;//区县
	private String address;//地址
	private String device;//设备类型
	private String regIp;//注册ip
	private Date regDate;//注册时间 
	private String lastLoginIp;//最后登录ip
	private Date lastLoginDate;//最后登录时间
	private Integer loginCount;//登录次数
	private String isAuth;//是否认证
	private String isDelete;//是否删除
	private String shareCode;//邀请码
	private String bak;//用户备注信息
	private WUserEntity parentUser;
			 */
            item.put("id", p.getId());
            item.put("name", p.getName());//店铺名称
            item.put("password", p.getPassword());
            item.put("nick", p.getNick());
            item.put("headUrl", p.getHeadUrl());
            item.put("mobile", p.getMobile());
            item.put("userType", "-");
            if (p.getUserType() != null) {
                item.put("userType", p.getUserType().getName());
            }
            item.put("device", p.getDevice());
            item.put("regIp", p.getRegIp());
            item.put("regDate", DateUtil.getTimeByCustomPattern(p.getRegDate(), DateUtil.yyyyMMddHHmmss));
            item.put("lastLoginIp", p.getRegIp());
            item.put("lastLoginDate", DateUtil.getTimeByCustomPattern(p.getLastLoginDate(), DateUtil.yyyyMMddHHmmss));
            item.put("isAuth", p.getIsAuth());
            item.put("isDelete", p.getIsDelete());
            item.put("shareCode", p.getShareCode());
            item.put("bak", p.getBak());
            item.put("lastBuyDate", "-");
            if (p.getLastBuyDate() != null) {
                item.put("lastBuyDate", DateUtil.getTimeByCustomPattern(p.getLastBuyDate(), DateUtil.yyyyMMddHHmmss));
            }

            item.put("coupon", "0");
            String tempHql = "from WUserCouponEntity as a where a.user.id=" + p.getId() + " and a.coupon.endTime >= ?";
            List<WUserCouponEntity> wcpList = findHql(tempHql, new Object[]{DateUtil.getCurrentTime()});
            if (wcpList != null && !wcpList.isEmpty()) {
                item.put("coupon", wcpList.size());
            }
            item.put("orderSize", 0);
            tempHql = "from OrderEntity as a where a.user.id.id=" + p.getId();
            List<OrderEntity> orderList = findByQueryString(tempHql);
            if (orderList != null && !orderList.isEmpty()) {
                item.put("orderSize", orderList.size());
            }

            item.put("pro", "--");
            item.put("address", "--");
            if (StringUtil.isNotEmpty(p.getPro())) {
                item.put("pro", p.getPro());
                item.put("address", p.getAddress());
            }
            item.put("parentName", "-");
            item.put("parentShareCode", "-");
            if (p.getParentUser() != null) {
                item.put("parentName", p.getParentUser().getName());
                item.put("parentShareCode", p.getParentUser().getShareCode());
            }
            item.put("status", p.getStatus());
            item.put("time", DateUtil.getTimeByCustomPattern(p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
            mapList.add(item);
        }
        d.setAaData(mapList);
        d.setsEcho(jt.getEcho());
        return d;
    }

    @Override
    public Map<String, Object> saveUser(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        WUserEntity user = null;
        String id = request.getParameter("id");
        user = get(WUserEntity.class, Long.valueOf(id));
		/*
		 * id : id,
    			name : name,
    			pro : pro,
    			city :city,
    			county : county,
    			isYewuyuan : isYewuyuan,
    			isAuth : isAuth,
    			isDelete : isDelete,
    			userType : userType,
    			address :address,
    			bak: bak
		 */
        String name = request.getParameter("name");
        String pro = request.getParameter("pro");
        String city = request.getParameter("city");
        String county = request.getParameter("county");
        String isYewuyuan = request.getParameter("isYewuyuan");
        String isAuth = request.getParameter("isAuth");
        String isDelete = request.getParameter("isDelete");
        String userType = request.getParameter("userType");
        String address = request.getParameter("address");
        String bak = request.getParameter("bak");

        user.setName(name);
        user.setPro(pro);
        user.setCity(city);
        user.setCounty(county);
        user.setIsAuth(isAuth);
        user.setIsDelete(isDelete);
        user.setIsYewuyuan(isYewuyuan);
        user.setAddress(address);
        user.setUserType(get(UserTypeEntity.class, Long.valueOf(userType)));
        user.setBak(bak);
        saveOrUpdate(user);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> deleteUser(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String id = request.getParameter("id");
        WUserEntity a = get(WUserEntity.class, Long.valueOf(id));
        if (a == null) {
            map.put("code", 1);
            return map;
        }
        a.setStatus("0");
        saveOrUpdate(a);
        map.put("code", 0);
        return map;
    }

    @Override
    public Map<String, Object> editTShareCode(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String id = request.getParameter("id");
        String pUsercode = request.getParameter("parentUserCode");
        //当前用户
        WUserEntity user = get(WUserEntity.class, Long.valueOf(id));
        String hql = "from WUserEntity as a where a.shareCode = '"+pUsercode+"'";
        List<WUserEntity> pUser = commonDao.findByQueryString(hql);
        if (pUser.size() > 0) {
            user.setParentUser(pUser.get(0));
            saveOrUpdate(user);
            map.put("code", 0);
        }else {
            map.put("code", 1);
        }
        return map;
    }

    @Override
    public DataTableReturn loadUserCouponByPage(JQueryDataTables dataTable, HttpServletRequest request) {
        String userId = request.getParameter("userId");
        StringBuffer hql = new StringBuffer(
                " from WUserCouponEntity as  a where a.user.id="
                        + userId + " and a.isUsed = '0' ");
        List<Object> params = new ArrayList<Object>();
        String name = request.getParameter("name");
        if (StringUtil.isNotEmpty(name)) {
            hql.append(" and a.coupon.name like ? ");
            params.add("%" + name + "%");
        }
        hql.append(" order by a.createDate desc");
        HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
        int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
        hqlQuery.setCurPage(currPage + 1);
        hqlQuery.setPageSize(dataTable.getDisplayLength());
        DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
        List<WUserCouponEntity> pList = (List<WUserCouponEntity>) d.getAaData();
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (WUserCouponEntity wc : pList) {
            Map<String, Object> item = new HashMap<String, Object>();
            item.put("id", wc.getId());
            CouponEntity p = wc.getCoupon();
			/*
			 *  * private String name;//优惠券名称 10元优惠券
	private Double money;//优惠券金额  10
	private Double conditionMoney;//需要满足的条件金额 100
	private Date beginTime;//开始时间
	private Date endTime;//结束时间
	private Integer allNum;//总数量
	private Integer leftNum;//剩余数量
			 */
            item.put("name", p.getName());
            item.put("tips", p.getTips());
            item.put("money", p.getMoney());
            item.put("conditionMoney", p.getConditionMoney());
            item.put("beginTime", DateUtil.getTimeByCustomPattern(p.getBeginTime(), DateUtil.yyyyMMddHHmmss));
            item.put("endTime", DateUtil.getTimeByCustomPattern(p.getEndTime(), DateUtil.yyyyMMddHHmmss));
            item.put("allNum", p.getAllNum());//当前数量
            item.put("joinNum", p.getJoinNum());//已经领取数量
            item.put("status", p.getStatus());
            item.put("time", DateUtil.getTimeByCustomPattern(wc.getCreateDate(), DateUtil.yyyyMMddHHmmss));
            mapList.add(item);
        }
        d.setAaData(mapList);
        d.setsEcho(dataTable.getEcho());
        return d;
    }

    @Override
    public Map<String, Object> saveUserCoupon(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        WUserEntity user = null;
        String id = request.getParameter("id");
        user = get(WUserEntity.class, Long.valueOf(id));
        String coupon = request.getParameter("coupon");
        CouponEntity c = get(CouponEntity.class, Long.valueOf(coupon));
        WUserCouponEntity wc = new WUserCouponEntity();
        wc.setCreateDate(DateUtil.getCurrentTime());
        wc.setUpdateDate(DateUtil.getCurrentTime());
        wc.setStatus("1");
        wc.setCoupon(c);
        wc.setUser(user);
        wc.setIsUsed("0");
        save(wc);
        map.put("code", 0);
        return map;
    }

}
