package modules.shop.user.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;

public interface UserTypeService extends CommonService{

	DataTableReturn loadUserTypeByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveUserType(HttpServletRequest request);

	Map<String, Object> deleteUserType(HttpServletRequest request);

}
