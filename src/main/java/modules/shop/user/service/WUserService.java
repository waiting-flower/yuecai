package modules.shop.user.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;

public interface WUserService extends CommonService{

	DataTableReturn loadAddressByPage(JQueryDataTables dataTable, HttpServletRequest request);

	DataTableReturn loadUserByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveUser(HttpServletRequest request);

	Map<String, Object> deleteUser(HttpServletRequest request);

	Map<String, Object> editTShareCode(HttpServletRequest request);

	DataTableReturn loadUserCouponByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveUserCoupon(HttpServletRequest request);

}
