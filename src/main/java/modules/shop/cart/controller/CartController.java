package modules.shop.cart.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import common.controller.BaseController;

@Controller
@RequestMapping(value = "admin/shop/cart")
public class CartController extends BaseController{

}
