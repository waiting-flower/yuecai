package modules.shop.cart.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
import modules.shop.goods.entity.GoodsEntity;
import modules.shop.goods.entity.GoodsSkuEntity;
import modules.shop.user.entity.WUserEntity;

@Entity
@Table(name = "shop_cart", schema = "")
public class CartEntity extends CommonEntity{
	private WUserEntity user;
	private GoodsEntity goods;
	private GoodsSkuEntity sku;
	private Integer num;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public WUserEntity getUser() {
		return user;
	}
	public void setUser(WUserEntity user) {
		this.user = user;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "goodsId")
	public GoodsEntity getGoods() {
		return goods;
	}
	public void setGoods(GoodsEntity goods) {
		this.goods = goods;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "skuId")
	public GoodsSkuEntity getSku() {
		return sku;
	}
	public void setSku(GoodsSkuEntity sku) {
		this.sku = sku;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
}
