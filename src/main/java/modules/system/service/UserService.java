package modules.system.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;
import modules.system.entity.UserEntity;

public interface UserService extends CommonService{

	UserEntity workLogin(String userName, String password, HttpServletRequest request);

	Map<String, Object> saveReg(HttpServletRequest request);

	DataTableReturn loadUserByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveUser(HttpServletRequest request);

	Map<String, Object> deleteUser(HttpServletRequest request);

	Map<String, Object> saveUserOk(HttpServletRequest request);

}
