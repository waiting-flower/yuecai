package modules.system.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.service.CommonService;

public interface AdminService extends CommonService{

	void initHome(HttpServletRequest request);

	Map<String, Object> getUserData(HttpServletRequest request);

	Map<String, Object> getOrderData(HttpServletRequest request);

	Map<String, Object> getTeamData(HttpServletRequest request);

	Map<String, Object> getAfterData(HttpServletRequest request);

	Map<String, Object> getOrderBarList(HttpServletRequest request);

	Map<String, Object> getLastNewUser(HttpServletRequest request);

}
