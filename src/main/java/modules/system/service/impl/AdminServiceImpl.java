package modules.system.service.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.StringUtil;
import modules.shop.order.entity.OrderEntity;
import modules.shop.user.entity.WUserEntity;
import modules.system.entity.LoginLogEntity;
import modules.system.service.AdminService;

@Service
public class AdminServiceImpl extends CommonServiceImpl implements AdminService{

	@Override
	public void initHome(HttpServletRequest request) {
		String sql = "select count(1),sum(money) from shop_order where orderState in('1','2','3','4');";
		List<Object[]> list = commonDao.findListbySql(sql);
		request.setAttribute("orderCount", list.get(0)[0]);
		request.setAttribute("money", list.get(0)[1]);
		
		sql = "select count(1) from shop_goods where status='1'";
		List<Object> goodsList = commonDao.findListbySql(sql);
		request.setAttribute("goodsCount", goodsList.get(0));
		
		sql = "select count(1) from shop_user where status = '1'";
		List<Object> userList = commonDao.findListbySql(sql);
		request.setAttribute("userCount", userList.get(0));
		
		sql = "select count(1) from shop_driver where status = '1'";
		List<Object> teamList = commonDao.findListbySql(sql);
		request.setAttribute("teamCount", teamList.get(0));
		
		
		
		sql = "select count(1) from shop_order where orderState = '1'";
		List<Object> ordrDaiList = commonDao.findListbySql(sql);
		request.setAttribute("newUserCount", ordrDaiList.get(0));
		
		sql = "select count(1) from shop_order where orderState = '5'";
		List<Object> ordrDaiList2 = commonDao.findListbySql(sql);
		request.setAttribute("newQuerenCount", ordrDaiList2.get(0));
		
		
		
		sql = "select count(1) from shop_order where isSalesAfter = '1' and salesAfterState in('2','3')";
		List<Object> saleAfterList = commonDao.findListbySql(sql);
		request.setAttribute("newSalesAfterCount", saleAfterList.get(0)==null?0:saleAfterList.get(0));
		
		String hql = "from LoginLogEntity as a order by a.createDate desc";
		List<LoginLogEntity> loginlist = commonDao.getListByPage(hql, 1, 5, new Object[] {});
		request.setAttribute("loginList", loginlist);
	}

	@Override
	public Map<String, Object> getUserData(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		String queryType = request.getParameter("quertyType");
		if(StringUtil.isEmpty(queryType)) {
			queryType = "day";
		}
		if("day".equals(queryType)) {
			String today = DateUtil.getCurrentTimeByCustomPattern(DateUtil.yyyyMMdd);
			String preDay = DateUtil.getTimeByCustomPattern(DateUtil.getPreviousDay(DateUtil.getCurrentTime()), DateUtil.yyyyMMdd);
			String sql = "select count(1) from shop_user where DATE_FORMAT(regDate,'%Y-%m-%d') = '" + today + "'";
			List<Object> todayUserList = commonDao.findListbySql(sql);
			BigInteger nowData = (BigInteger) todayUserList.get(0);
			map.put("nowCount", nowData);
			
			sql = "select count(1) from shop_user where DATE_FORMAT(regDate,'%Y-%m-%d') = '" + preDay + "'";
			List<Object> preDayUserList = commonDao.findListbySql(sql);
			BigInteger preData = (BigInteger) preDayUserList.get(0);
			map.put("preCount", preData);
			if(preData.intValue() > nowData.intValue()) {
				map.put("orderType", "0");
				int diff = preData.intValue() - nowData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = Double.valueOf(diff)/Double.valueOf(preData.intValue());
					map.put("rate", rate);
				}
			}
			else {
				map.put("orderType", 1);
				int diff = nowData.intValue() - preData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = Double.valueOf(diff)/Double.valueOf(preData.intValue());
					map.put("rate", rate);
				}
				
			}
			
		}
		else if("month".equals(queryType)){
			String toMonth = DateUtil.getCurrentTimeByCustomPattern("yyyy-MM"); 
			String preMonth = DateUtil.getTimeByCustomPattern(DateUtil.getAfterMonth(DateUtil.getCurrentTime(), -1), "yyyy-MM");
			String sql = "select count(1) from shop_user where DATE_FORMAT(regDate,'%Y-%m') = '" + toMonth + "'";
			List<Object> todayUserList = commonDao.findListbySql(sql);
			BigInteger nowData = (BigInteger) todayUserList.get(0);
			map.put("nowCount", nowData);
			
			sql = "select count(1) from shop_user where DATE_FORMAT(regDate,'%Y-%m') = '" + preMonth + "'";
			List<Object> preDayUserList = commonDao.findListbySql(sql);
			BigInteger preData = (BigInteger) preDayUserList.get(0);
			map.put("preCount", preData);
			if(preData.intValue() > nowData.intValue()) {
				map.put("orderType", "0");
				int diff = preData.intValue() - nowData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = Double.valueOf(diff)/Double.valueOf(preData.intValue());
					map.put("rate", rate);
				}
			}
			else {
				map.put("orderType", 1);
				int diff = nowData.intValue() - preData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = Double.valueOf(diff)/Double.valueOf(preData.intValue());
					map.put("rate", rate);
				}
				
			}
		}
		else if("year".equals(queryType)) {
			String toYear = DateUtil.getCurrentTimeByCustomPattern("yyyy");
			String preYear = DateUtil.getTimeByCustomPattern(DateUtil.getAfterMonth(DateUtil.getCurrentTime(), -12), "yyyy");
			String sql = "select count(1) from shop_user where DATE_FORMAT(regDate,'%Y') = '" + toYear + "'";
			List<Object> todayUserList = commonDao.findListbySql(sql);
			BigInteger nowData = (BigInteger) todayUserList.get(0);
			map.put("nowCount", nowData);
			
			sql = "select count(1) from shop_user where DATE_FORMAT(regDate,'%Y') = '" + preYear + "'";
			List<Object> preDayUserList = commonDao.findListbySql(sql);
			BigInteger preData = (BigInteger) preDayUserList.get(0);
			map.put("preCount", preData);
			if(preData.intValue() > nowData.intValue()) {
				map.put("orderType", "0");
				int diff = preData.intValue() - nowData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = Double.valueOf(diff)/Double.valueOf(preData.intValue());
					map.put("rate", rate);
				}
			}
			else {
				map.put("orderType", 1);
				int diff = nowData.intValue() - preData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = Double.valueOf(diff)/Double.valueOf(preData.intValue());
					map.put("rate", rate);
				}
				
			}
		}
		
		return map;
	}

	@Override
	public Map<String, Object> getOrderData(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		String queryType = request.getParameter("quertyType");
		if(StringUtil.isEmpty(queryType)) {
			queryType = "day";
		}
		if("day".equals(queryType)) {
			String today = DateUtil.getCurrentTimeByCustomPattern(DateUtil.yyyyMMdd);
			String preDay = DateUtil.getTimeByCustomPattern(DateUtil.getPreviousDay(DateUtil.getCurrentTime()), DateUtil.yyyyMMdd);
			String sql = "select count(1) from shop_order where DATE_FORMAT(createDate,'%Y-%m-%d') = '" + today + "'";
			List<Object> todayUserList = commonDao.findListbySql(sql);
			BigInteger nowData = (BigInteger) todayUserList.get(0);
			map.put("nowCount", nowData);
			
			sql = "select count(1) from shop_user where DATE_FORMAT(createDate,'%Y-%m-%d') = '" + preDay + "'";
			List<Object> preDayUserList = commonDao.findListbySql(sql);
			BigInteger preData = (BigInteger) preDayUserList.get(0);
			map.put("preCount", preData);
			if(preData.intValue() > nowData.intValue()) {
				map.put("orderType", "0");
				int diff = preData.intValue() - nowData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = Double.valueOf(diff)/Double.valueOf(preData.intValue());
					map.put("rate", rate);
				}
			}
			else {
				map.put("orderType", 1);
				int diff = nowData.intValue() - preData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = Double.valueOf(diff)/Double.valueOf(preData.intValue());
					map.put("rate", rate);
				}
				
			}
			
		}
		else if("month".equals(queryType)){
			String toMonth = DateUtil.getCurrentTimeByCustomPattern("yyyy-MM");
			String preMonth = DateUtil.getTimeByCustomPattern(DateUtil.getAfterMonth(DateUtil.getCurrentTime(), -1), "yyyy-MM");
			String sql = "select count(1) from shop_order where DATE_FORMAT(createDate,'%Y-%m') = '" + toMonth + "'";
			List<Object> todayUserList = commonDao.findListbySql(sql);
			BigInteger nowData = (BigInteger) todayUserList.get(0);
			map.put("nowCount", nowData);
			
			sql = "select count(1) from shop_order where DATE_FORMAT(createDate,'%Y-%m') = '" + preMonth + "'";
			List<Object> preDayUserList = commonDao.findListbySql(sql);
			BigInteger preData = (BigInteger) preDayUserList.get(0);
			map.put("preCount", preData);
			if(preData.intValue() > nowData.intValue()) {
				map.put("orderType", "0");
				int diff = preData.intValue() - nowData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = Double.valueOf(diff)/Double.valueOf(preData.intValue());
					map.put("rate", rate);
				}
			}
			else {
				map.put("orderType", 1);
				int diff = nowData.intValue() - preData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = Double.valueOf(diff)/Double.valueOf(preData.intValue());
					map.put("rate", rate);
				}
				
			}
		}
		else if("year".equals(queryType)) {
			String toYear = DateUtil.getCurrentTimeByCustomPattern("yyyy");
			String preYear = DateUtil.getTimeByCustomPattern(DateUtil.getAfterMonth(DateUtil.getCurrentTime(), -12), "yyyy");
			String sql = "select count(1) from shop_order where DATE_FORMAT(createDate,'%Y') = '" + toYear + "'";
			List<Object> todayUserList = commonDao.findListbySql(sql);
			BigInteger nowData = (BigInteger) todayUserList.get(0);
			map.put("nowCount", nowData);
			
			sql = "select count(1) from shop_order where DATE_FORMAT(createDate,'%Y') = '" + preYear + "'";
			List<Object> preDayUserList = commonDao.findListbySql(sql);
			BigInteger preData = (BigInteger) preDayUserList.get(0);
			map.put("preCount", preData);
			if(preData.intValue() > nowData.intValue()) {
				map.put("orderType", "0");
				int diff = preData.intValue() - nowData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = (double) (diff/preData.intValue());
					map.put("rate", rate);
				}
			}
			else {
				map.put("orderType", 1);
				int diff = nowData.intValue() - preData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = (double) (diff/preData.intValue());
					map.put("rate", rate);
				}
				
			}
		}
		
		return map;
	}

	@Override
	public Map<String, Object> getTeamData(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		String queryType = request.getParameter("quertyType");
		if(StringUtil.isEmpty(queryType)) {
			queryType = "day";
		}
		if("day".equals(queryType)) {
			String today = DateUtil.getCurrentTimeByCustomPattern(DateUtil.yyyyMMdd);
			String preDay = DateUtil.getTimeByCustomPattern(DateUtil.getPreviousDay(DateUtil.getCurrentTime()), DateUtil.yyyyMMdd);
			String sql = "select count(1) from shop_driver where DATE_FORMAT(createDate,'%Y-%m-%d') = '" + today + "'";
			List<Object> todayUserList = commonDao.findListbySql(sql);
			BigInteger nowData = (BigInteger) todayUserList.get(0);
			map.put("nowCount", nowData);
			
			sql = "select count(1) from shop_driver where DATE_FORMAT(createDate,'%Y-%m-%d') = '" + preDay + "'";
			List<Object> preDayUserList = commonDao.findListbySql(sql);
			BigInteger preData = (BigInteger) preDayUserList.get(0);
			map.put("preCount", preData);
			if(preData.intValue() > nowData.intValue()) {
				map.put("orderType", "0");
				int diff = preData.intValue() - nowData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = (double) (diff/preData.intValue());
					map.put("rate", rate);
				}
			}
			else {
				map.put("orderType", 1);
				int diff = nowData.intValue() - preData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = (double) (diff/preData.intValue());
					map.put("rate", rate);
				}
				
			}
			
		}
		else if("month".equals(queryType)){
			String toMonth = DateUtil.getCurrentTimeByCustomPattern("yyyy-MM");
			String preMonth = DateUtil.getTimeByCustomPattern(DateUtil.getAfterMonth(DateUtil.getCurrentTime(), -1), "yyyy-MM");
			String sql = "select count(1) from shop_driver where DATE_FORMAT(createDate,'%Y-%m') = '" + toMonth + "'";
			List<Object> todayUserList = commonDao.findListbySql(sql);
			BigInteger nowData = (BigInteger) todayUserList.get(0);
			map.put("nowCount", nowData);
			
			sql = "select count(1) from shop_driver where DATE_FORMAT(createDate,'%Y-%m') = '" + preMonth + "'";
			List<Object> preDayUserList = commonDao.findListbySql(sql);
			BigInteger preData = (BigInteger) preDayUserList.get(0);
			map.put("preCount", preData);
			if(preData.intValue() > nowData.intValue()) {
				map.put("orderType", "0");
				int diff = preData.intValue() - nowData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = (double) (diff/preData.intValue());
					map.put("rate", rate);
				}
			}
			else {
				map.put("orderType", 1);
				int diff = nowData.intValue() - preData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = (double) (diff/preData.intValue());
					map.put("rate", rate);
				}
				
			}
		}
		else if("year".equals(queryType)) {
			String toYear = DateUtil.getCurrentTimeByCustomPattern("yyyy");
			String preYear = DateUtil.getTimeByCustomPattern(DateUtil.getAfterMonth(DateUtil.getCurrentTime(), -12), "yyyy");
			String sql = "select count(1) from shop_driver where DATE_FORMAT(createDate,'%Y') = '" + toYear + "'";
			List<Object> todayUserList = commonDao.findListbySql(sql);
			BigInteger nowData = (BigInteger) todayUserList.get(0);
			map.put("nowCount", nowData);
			
			sql = "select count(1) from shop_driver where DATE_FORMAT(createDate,'%Y') = '" + preYear + "'";
			List<Object> preDayUserList = commonDao.findListbySql(sql);
			BigInteger preData = (BigInteger) preDayUserList.get(0);
			map.put("preCount", preData);
			if(preData.intValue() > nowData.intValue()) {
				map.put("orderType", "0");
				int diff = preData.intValue() - nowData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = (double) (diff/preData.intValue());
					map.put("rate", rate);
				}
			}
			else {
				map.put("orderType", 1);
				int diff = nowData.intValue() - preData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = (double) (diff/preData.intValue());
					map.put("rate", rate);
				}
				
			}
		}
		
		return map;
	}

	@Override
	public Map<String, Object> getAfterData(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		String queryType = request.getParameter("quertyType");
		if(StringUtil.isEmpty(queryType)) {
			queryType = "day";
		}
		if("day".equals(queryType)) {
			String today = DateUtil.getCurrentTimeByCustomPattern(DateUtil.yyyyMMdd);
			String preDay = DateUtil.getTimeByCustomPattern(DateUtil.getPreviousDay(DateUtil.getCurrentTime()), DateUtil.yyyyMMdd);
			String sql = "select count(1) from shop_order where isSalesAfter = '1' and DATE_FORMAT(applySalesAfterTime,'%Y-%m-%d') = '" + today + "'";
			List<Object> todayUserList = commonDao.findListbySql(sql);
			BigInteger nowData = (BigInteger) todayUserList.get(0);
			map.put("nowCount", nowData); 
			
			sql = "select count(1) from shop_order where isSalesAfter = '1' and DATE_FORMAT(applySalesAfterTime,'%Y-%m-%d') = '" + preDay + "'";
			List<Object> preDayUserList = commonDao.findListbySql(sql);
			BigInteger preData = (BigInteger) preDayUserList.get(0);
			map.put("preCount", preData);
			if(preData.intValue() > nowData.intValue()) {
				map.put("orderType", "0");
				int diff = preData.intValue() - nowData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = (double) (diff/preData.intValue());
					map.put("rate", rate);
				}
			}
			else {
				map.put("orderType", 1);
				int diff = nowData.intValue() - preData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = (double) (diff/preData.intValue());
					map.put("rate", rate);
				}
				
			}
			
		}
		else if("month".equals(queryType)){
			String toMonth = DateUtil.getCurrentTimeByCustomPattern("yyyy-MM");
			String preMonth = DateUtil.getTimeByCustomPattern(DateUtil.getAfterMonth(DateUtil.getCurrentTime(), -1), "yyyy-MM");
			String sql = "select count(1) from shop_order where isSalesAfter = '1' and DATE_FORMAT(applySalesAfterTime,'%Y-%m') = '" + toMonth + "'";
			List<Object> todayUserList = commonDao.findListbySql(sql);
			BigInteger nowData = (BigInteger) todayUserList.get(0);
			map.put("nowCount", nowData);
			
			sql = "select count(1) from shop_order where isSalesAfter = '1' and DATE_FORMAT(applySalesAfterTime,'%Y-%m') = '" + preMonth + "'";
			List<Object> preDayUserList = commonDao.findListbySql(sql);
			BigInteger preData = (BigInteger) preDayUserList.get(0);
			map.put("preCount", preData);
			if(preData.intValue() > nowData.intValue()) {
				map.put("orderType", "0");
				int diff = preData.intValue() - nowData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = (double) (diff/preData.intValue());
					map.put("rate", rate);
				}
			}
			else {
				map.put("orderType", 1);
				int diff = nowData.intValue() - preData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = (double) (diff/preData.intValue());
					map.put("rate", rate);
				}
				
			}
		}
		else if("year".equals(queryType)) {
			String toYear = DateUtil.getCurrentTimeByCustomPattern("yyyy");
			String preYear = DateUtil.getTimeByCustomPattern(DateUtil.getAfterMonth(DateUtil.getCurrentTime(), -12), "yyyy");
			String sql = "select count(1) from shop_order where isSalesAfter = '1' and DATE_FORMAT(applySalesAfterTime,'%Y') = '" + toYear + "'";
			List<Object> todayUserList = commonDao.findListbySql(sql);
			BigInteger nowData = (BigInteger) todayUserList.get(0);
			map.put("nowCount", nowData);
			
			sql = "select count(1) from shop_order where isSalesAfter = '1' and DATE_FORMAT(applySalesAfterTime,'%Y') = '" + preYear + "'";
			List<Object> preDayUserList = commonDao.findListbySql(sql);
			BigInteger preData = (BigInteger) preDayUserList.get(0);
			map.put("preCount", preData);
			if(preData.intValue() > nowData.intValue()) {
				map.put("orderType", "0");
				int diff = preData.intValue() - nowData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = (double) (diff/preData.intValue());
					map.put("rate", rate);
				}
			}
			else {
				map.put("orderType", 1);
				int diff = nowData.intValue() - preData.intValue();
				if(preData.intValue() == 0) {
					map.put("rate", 0);
				}
				else {
					Double rate = (double) (diff/preData.intValue());
					map.put("rate", rate);
				}
				
			}
		}
		
		return map;
	}

	@Override
	public Map<String, Object> getOrderBarList(HttpServletRequest request) {

		Map<String, Object> map = new HashMap<>();
		String type = request.getParameter("type");
		Date now = DateUtil.getCurrentTime();
		Date begin = null;
		int len = 1;
		List<String> list = new ArrayList<>();
		if("7".equals(type)) {
			//7天统计
			len = 7;
			begin = DateUtil.getAfterDay(now, -7);
		}
		else if("30".equals(type)) {
			//30天统计
			len = 30;
			begin = DateUtil.getAfterDay(now, -30);
		}
		else if("90".equals(type)) {
			//90天统计
			len = 90;	
			begin = DateUtil.getAfterDay(now, -90);
		}
		StringBuffer tempSql = new StringBuffer();
		tempSql.append("SELECT ");
		tempSql.append("    od.date, ");
		tempSql.append("    od.userCount ");
		tempSql.append("FROM ");
		tempSql.append("    ( ");
		tempSql.append("        SELECT ");
		tempSql.append("            count(1) userCount, ");
		tempSql.append("            DATE_FORMAT(createDate,'%Y-%m-%d') date ");
		tempSql.append("        FROM ");
		tempSql.append("            shop_order  ");
		tempSql.append("        WHERE orderState > 0 and ");
		tempSql.append("            DATE_FORMAT(createDate,'%Y-%m-%d')>='" + DateUtil.getTimeByCustomPattern(begin, DateUtil.yyyyMMdd) 
				+ "' and DATE_FORMAT(createDate,'%Y-%m-%d')<='" + DateUtil.getTimeByCustomPattern(now, DateUtil.yyyyMMdd) + "' ");
		tempSql.append("        GROUP BY DATE_FORMAT(createDate,'%Y-%m-%d') ");
		for(int i = len - 1; i >= 0; i--) {
			String date = null;
			Date day = DateUtil.getAfterDay(now, 0 - i);
			date = DateUtil.getTimeByCustomPattern(day, "yyyy-MM-dd");
			list.add(date);
			tempSql.append(" UNION (SELECT 0, '" + date + "')");
		}
		tempSql.append("    ) AS od ");
		tempSql.append("ORDER BY od.date DESC ");
		System.out.println(tempSql.toString());
		List<Object[]> list2 = commonDao.findListbySql(tempSql.toString());
		map.put("list2", list2);
		List dataList = new ArrayList<>();
		for(Object[] obj : list2) {
			dataList.add(obj[1]);
		}
		map.put("dataList",dataList);
		map.put("list", list);
		return map;
	}

	@Override
	public Map<String, Object> getLastNewUser(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		String lastGetTime = request.getParameter("lastGetTime");
		map.put("code", 1);
		map.put("lastGetTime", DateUtil.getCurrentTimeByCustomPattern(DateUtil.yyyyMMddHHmmss));
		if(StringUtil.isNotEmpty(lastGetTime)){
			String hql = "from WUserEntity  as a where  a.createDate >= ? ";
			List<WUserEntity> appointEntities = commonDao.findHql(hql, new Object[]{DateUtil.parse(lastGetTime, DateUtil.yyyyMMddHHmmss)});
			if(appointEntities != null && !appointEntities.isEmpty()){
				map.put("code", 0);
			}
		} 
		else{
			map.put("code", 0);
			
		}
		return map;
	}

}
