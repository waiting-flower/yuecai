package modules.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.HqlQuery;
import common.util.StringUtil;
import modules.system.entity.MenuEntity;
import modules.system.entity.RoleEntity;
import modules.system.entity.RoleMenuMapEntity;
import modules.system.service.RoleService;
@Service
public class RoleServiceImpl extends CommonServiceImpl implements RoleService{

	@Override
	public DataTableReturn loadRoleByPage(JQueryDataTables dataTable, HttpServletRequest request) {
		StringBuffer hql = new StringBuffer(
				" from RoleEntity as  a where 1 = 1 ");
		List<Object> params = new ArrayList<Object>();
		String name = request.getParameter("name");
		if (StringUtil.isNotEmpty(name)) {
			hql.append(" and a.name like ? ");
			params.add("%" + name + "%");
		}
		HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
		int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
		hqlQuery.setCurPage(currPage + 1);
		hqlQuery.setPageSize(dataTable.getDisplayLength());
		DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
		List<RoleEntity> pList = (List<RoleEntity>) d.getAaData();
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		for (RoleEntity p : pList) {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("id", p.getId());
			item.put("name", p.getName());
			item.put("orderNum", p.getOrderNum());
			item.put("status", p.getStatus());
			item.put("note", p.getNote()); 
			item.put("time",DateUtil.getTimeByCustomPattern( p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
			mapList.add(item);
		}
		d.setAaData(mapList);
		d.setsEcho(dataTable.getEcho());
		return d;
	}

	@Override
	public Map<String, Object> saveRole(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String note = request.getParameter("note");
		String orderNum = request.getParameter("orderNum");
		RoleEntity role = null;
		if(StringUtil.isEmpty(id)) {
			role = new RoleEntity();
			role.setCreateDate(DateUtil.getCurrentTime());
			role.setUpdateDate(DateUtil.getCurrentTime());
			role.setStatus("1");
		}
		else {
			role = get(RoleEntity.class, Long.valueOf(id));
			role.setUpdateDate(DateUtil.getCurrentTime());
		}
		role.setName(name);
		role.setOrderNum(Integer.parseInt(orderNum));
		role.setNote(note);
		saveOrUpdate(role);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> deleteRole(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		String id = request.getParameter("id");
		RoleEntity role = get(RoleEntity.class, Long.valueOf(id));
		if (role == null) {
			map.put("code", 1);
			return map;
		}
		role.setStatus("0");
		saveOrUpdate(role);
		map.put("code", 0); 
		return map;
	}

	@Override
	public Map<String, Object> initRoleMenuTree(Long roleId) {
		Map<String, Object> map = new HashMap<>();
		String hql = "from MenuEntity as a where a.status = '1'  order by a.orderNum desc,a.orderNum2 desc,a.orderNum3 desc";
		List<MenuEntity> menuList = findByQueryString(hql);//全部的菜单列表
		String roleHql = "from RoleMenuMapEntity as a where a.role.id =" + roleId;
		List<RoleMenuMapEntity> roleMenuList = findByQueryString(roleHql);//改角色配置的菜单列表
		Set<Long> menuIdSet = new HashSet<>();
		for(RoleMenuMapEntity rm : roleMenuList) {
			menuIdSet.add(rm.getMenu().getId());
		}
		//最多支持4级菜单
		List<Map<String, Object>> list = new ArrayList<>();
		for(MenuEntity menu : menuList) {
			Map<String, Object> item = new HashMap<>();
			item.put("id", menu.getId());
			if(menu.getParentMenu() != null) {
				item.put("pId", menu.getParentMenu().getId());
				if(menu.getGradeMenu() == null) {
					item.put("open", true);
				}
			}
			else {
				item.put("open", true);
				item.put("pId", 0);
			}
			item.put("name", menu.getName());
			item.put("checked", false);
			if(menuIdSet.contains(menu.getId())) {
				item.put("checked", true);
			}
			list.add(item);
		}
		
		map.put("code", 0);
		map.put("list", list);
		return map;
	}

	@Override
	public Map<String, Object> saveRoleMenu(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		String roleId = request.getParameter("id");
		String roleHql = "from RoleMenuMapEntity as a where a.role.id =" + roleId;
		List<RoleMenuMapEntity> roleMenuList = findByQueryString(roleHql);//改角色配置的菜单列表
		deleteAllEntitie(roleMenuList);
		String menuIdArr = request.getParameter("menuIdArr");
		String menuIdStr[] = menuIdArr.split(",");
		RoleEntity role = get(RoleEntity.class, Long.valueOf(roleId));
		for(String id : menuIdStr) {
			MenuEntity menu = get(MenuEntity.class, Long.valueOf(id));
			RoleMenuMapEntity rm = new RoleMenuMapEntity();
			rm.setCreateDate(DateUtil.getCurrentTime());
			rm.setUpdateDate(DateUtil.getCurrentTime());
			rm.setStatus("1");
			rm.setRole(role);
			rm.setMenu(menu);
			save(rm);
		}
		map.put("code", 0); 
		return map;
	}

}
