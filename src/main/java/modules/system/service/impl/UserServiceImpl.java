package modules.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.HqlQuery;
import common.util.MD5Util;
import common.util.StringUtil;
import modules.system.entity.LoginLogEntity;
import modules.system.entity.RoleEntity;
import modules.system.entity.UserEntity;
import modules.system.service.UserService;

@Service(value = "sysUserService")
@Transactional(isolation = Isolation.READ_COMMITTED)
public class UserServiceImpl extends CommonServiceImpl implements UserService{

	@Override
	public UserEntity workLogin(String userName, String password, HttpServletRequest request) {
		String hql = "from UserEntity as a where   a.userName = ? and a.password = ? ";
		List<UserEntity> userList = commonDao.findHql(hql, new Object[]{userName,MD5Util.md5Encode(password)});
		if(userList != null && userList.size() > 0){
			UserEntity u = userList.get(0);
			u.setLoginCount(u.getLoginCount() + 1);
			if(StringUtil.isNotEmpty(u.getLoginIp())){
				u.setLastLoginIp(u.getLoginIp());
			}
			if(u.getLoginDate() != null){
				u.setLastLoginDate(u.getLoginDate());
			}
			u.setLoginDate(DateUtil.getCurrentTime());
			u.setLoginIp(request.getRemoteAddr());
			saveOrUpdate(u);
			
			
			LoginLogEntity log = new LoginLogEntity();
			log.setCreateDate(DateUtil.getCurrentTime());
			log.setUpdateDate(DateUtil.getCurrentTime());
			log.setStatus("1");
			log.setUserName(userName);
			log.setIp(request.getRemoteAddr());
			save(log);
			
			return u;
		}
		return null;
	}

	@Override
	public Map<String, Object> saveReg(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		String userName = request.getParameter("userName");
		String vercode = request.getParameter("vercode");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String randomPic = (String) request.getSession().getAttribute("randomPic");
		if(StringUtil.isEmpty(randomPic) || StringUtil.isEmpty(vercode) || !vercode.toLowerCase().equals(randomPic.toLowerCase())){
			map.put("code", "-1");
			return map;
		}
		String hql = "from UserEntity as a where a.userName = ? ";
		List<UserEntity> userList = findHql(hql, new Object[] {userName});
		if(userList != null && !userList.isEmpty()) {
			map.put("code", 1);
			map.put("msg", "您输入的用户名已被注册，请直接登录");
			return map;
		}
		
		hql = "from UserEntity as a where a.email = ? ";
		userList = findHql(hql, new Object[] {email});
		if(userList != null && !userList.isEmpty()) {
			map.put("code", 1);
			map.put("msg", "您输入的邮箱已被注册，请直接登录");
			return map;
		}
		UserEntity user = new UserEntity();
		user.setCreateDate(DateUtil.getCurrentTime());
		user.setUpdateDate(DateUtil.getCurrentTime());
		user.setStatus("1");
		user.setLoginCount(0);
		user.setUserName(userName);
		user.setPassword(MD5Util.md5Encode(password));
		user.setEmail(email);
		user.setName(name);
		saveOrUpdate(user);
		map.put("code", 0);
		return map;
	}

	@Override
	public DataTableReturn loadUserByPage(JQueryDataTables dataTable, HttpServletRequest request) {
		StringBuffer hql = new StringBuffer(
				" from UserEntity as  a where 1 = 1 ");
		List<Object> params = new ArrayList<Object>();
		String name = request.getParameter("name");
		if (StringUtil.isNotEmpty(name)) {
			hql.append(" and a.name like ? ");
			params.add("%" + name + "%");
		}
		String userName = request.getParameter("userName");
		if (StringUtil.isNotEmpty(userName)) {
			hql.append(" and a.userName like ? ");
			params.add("%" + name + "%");
		}
		String identity = request.getParameter("identity");
		if (StringUtil.isNotEmpty(identity)) {
			hql.append(" and a.identity = ? ");
			params.add(identity);
		}
		HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
		int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
		hqlQuery.setCurPage(currPage + 1);
		hqlQuery.setPageSize(dataTable.getDisplayLength());
		DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
		List<UserEntity> pList = (List<UserEntity>) d.getAaData();
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		for (UserEntity p : pList) {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("id", p.getId());
			item.put("name", p.getName());
			item.put("userName", p.getUserName());
			item.put("status", p.getStatus());
			item.put("password", p.getPassword()); 
			item.put("loginCount", p.getLoginCount()); 
			item.put("lastLoginIp", p.getLastLoginIp()); 
			item.put("lastLoginDate", "-");
			if(p.getLastLoginDate() != null) {
				item.put("lastLoginDate", DateUtil.getTimeByCustomPattern(p.getLastLoginDate(), DateUtil.yyyyMMddHHmmss)); 
			}
			
			item.put("email", p.getEmail());  
			item.put("time",DateUtil.getTimeByCustomPattern( p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
			mapList.add(item);
		}
		d.setAaData(mapList);
		d.setsEcho(dataTable.getEcho());
		return d;
	}

	@Override
	public Map<String, Object> saveUser(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		String id = request.getParameter("id");
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String role = request.getParameter("role");
		String coopId = request.getParameter("coopId");
		String accountId = request.getParameter("accountId");
		String identity = request.getParameter("identity");
		UserEntity user = null;
		if(StringUtil.isEmpty(id)) {
			UserEntity userList = findUniqueByProperty(UserEntity.class, "userName", userName);
			if(userList != null) {
				map.put("code", -1);
				return map;
			}
			userList = findUniqueByProperty(UserEntity.class, "email", email);
			if(userList != null) {
				map.put("code", -2);
				return map;
			}
			user = new UserEntity();
			user.setCreateDate(DateUtil.getCurrentTime());
			user.setUpdateDate(DateUtil.getCurrentTime());
			user.setStatus("1");
			user.setLoginCount(0);
			user.setPassword(MD5Util.md5Encode(password));//新增用户的时候，用户密码进行密文加密
		}
		else {
			user = get(UserEntity.class, Long.valueOf(id));
			user.setUpdateDate(DateUtil.getCurrentTime());
			String hql = "from UserEntity as a where a.userName = ?";
			List<UserEntity> userList = findHql(hql, new Object[] {userName});
			for(UserEntity ouser : userList) {
				if(!ouser.getId().equals(user.getId())) {
					map.put("code", "-1");
					return map;
				}
			}
			hql = "from UserEntity as a where a.email = ?";
			userList = findHql(hql, new Object[] {email});
			for(UserEntity ouser : userList) {
				if(!ouser.getId().equals(user.getId())) {
					map.put("code", "-2");
					return map;
				}
			}
		}
		user.setIdentity(identity);
		user.setRole(get(RoleEntity.class, Long.valueOf(role)));
		user.setEmail(email);
		user.setName(name);
		user.setUserName(userName);
		user.setPassword(MD5Util.md5Encode(password));//新增用户的时候，用户密码进行密文加密
		saveOrUpdate(user);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> deleteUser(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		String id = request.getParameter("id");
		UserEntity role = get(UserEntity.class, Long.valueOf(id));
		if (role == null) {
			map.put("code", 1);
			return map;
		}
		role.setStatus("0");
		saveOrUpdate(role);
		map.put("code", 0); 
		return map;
	}

	@Override
	public Map<String, Object> saveUserOk(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		String id = request.getParameter("id");
		UserEntity role = get(UserEntity.class, Long.valueOf(id));
		if (role == null) {
			map.put("code", 1);
			return map;
		}
		role.setStatus("1");
		saveOrUpdate(role);
		map.put("code", 0); 
		return map;
	}

}
