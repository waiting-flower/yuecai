package modules.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.impl.CommonServiceImpl;
import common.util.DateUtil;
import common.util.HqlQuery;
import common.util.StringUtil;
import modules.system.entity.MenuEntity;
import modules.system.service.MenuService;
@Service
public class MenuServiceImpl extends CommonServiceImpl implements MenuService{

	@Override
	public DataTableReturn loadMenuByPage(JQueryDataTables dataTable, HttpServletRequest request) {
		StringBuffer hql = new StringBuffer(
				" from MenuEntity as  a where 1 = 1 ");
		List<Object> params = new ArrayList<Object>();
		String name = request.getParameter("name");
		if (StringUtil.isNotEmpty(name)) {
			hql.append(" and a.name like ? ");
			params.add("%" + name + "%");
		}
		hql.append(" order by a.orderNum4 desc,a.orderNum3 desc,a.orderNum2 desc,a.orderNum desc");
		HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
		int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
		hqlQuery.setCurPage(currPage + 1);
		hqlQuery.setPageSize(dataTable.getDisplayLength());
		DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
		List<MenuEntity> pList = (List<MenuEntity>) d.getAaData();
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		for (MenuEntity p : pList) {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("id", p.getId());
			item.put("name", p.getName());
			item.put("parentMenu", "");
			if(p.getParentMenu() != null) {
				if(p.getGradeMenu() != null) {
					//三级
					item.put("parentMenu", p.getParentMenu().getName());
					item.put("name", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp├─" + p.getName());
				}
				else {
					//二级
					item.put("parentMenu", p.getParentMenu().getName());
					item.put("name", "&nbsp;&nbsp;&nbsp;&nbsp;├─" + p.getName());
				}
				
			}
			else {
				item.put("parent", "");
				item.put("name", p.getName());
			}
			item.put("url", p.getUrl());
			item.put("icon", p.getIcon());
			item.put("orderNum", p.getOrderNum());
			item.put("status", p.getStatus());

			item.put("time",DateUtil.getTimeByCustomPattern( p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
			mapList.add(item);
		}
		d.setAaData(mapList);
		d.setsEcho(dataTable.getEcho());
		return d;
	}

	@Override
	public Map<String, Object> saveMenu(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String orderNum = request.getParameter("orderNum");
		String parentMenu = request.getParameter("parentMenu");
		String icon = request.getParameter("icon");
		String url = request.getParameter("url");
		/*
			lookNum : lookNum,
			collectNum : collectNum,
			engName : engName,
			target :target,
			sponsorType : sponsorType,
			info : info,
			orderNum : orderNum
		 */
		MenuEntity a = null;
		if(StringUtil.isEmpty(id)) {
			a = new MenuEntity();
			a.setCreateDate(DateUtil.getCurrentTime());
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
			a.setIsHasChild("0");
		}
		else {
			a = get(MenuEntity.class, Long.valueOf(id));
			a.setUpdateDate(DateUtil.getCurrentTime());
		}
		a.setIcon(icon);
		a.setOrderNum(Integer.parseInt(orderNum));
		if(StringUtil.isNotEmpty(parentMenu)) {
			MenuEntity parent = get(MenuEntity.class, Long.valueOf(parentMenu));
			a.setParentMenu(parent);
			parent.setIsHasChild("1");
			saveOrUpdate(parent);
			if(parent.getParentMenu() != null) {
				//三级菜单
				a.setOrderNum(Integer.parseInt(orderNum));
				a.setOrderNum2(parent.getOrderNum2());
				a.setOrderNum3(parent.getParentMenu().getOrderNum3());
				a.setOrderNum4(parent.getParentMenu().getOrderNum4());
			}
			else {
				//二级菜单的
				a.setOrderNum(9999);
				a.setOrderNum2(Integer.parseInt(orderNum));
				a.setOrderNum3(parent.getOrderNum3());
				a.setOrderNum4(parent.getOrderNum4());
			}
			a.setGradeMenu(parent.getParentMenu());
		}
		else { //表示一级菜单的话
			if(StringUtil.isEmpty(id)) {
				//新增的话
				
				String hql = "from MenuEntity as a where a.parentMenu is null and a.gradeMenu is null order by a.orderNum4 desc";
				List<MenuEntity> menuList = findByQueryString(hql);
				if(menuList != null && !menuList.isEmpty()) {
					a.setOrderNum3(menuList.get(0).getOrderNum3() - 1 + Integer.parseInt(orderNum));
					a.setOrderNum2(menuList.get(0).getOrderNum2() - 1 + Integer.parseInt(orderNum));
					a.setOrderNum4(menuList.get(0).getOrderNum4() - 1 + Integer.parseInt(orderNum));
				} 
				else {
					a.setOrderNum3(9999 + Integer.parseInt(orderNum));
					a.setOrderNum2(9999 + Integer.parseInt(orderNum));
					a.setOrderNum4(9999 + Integer.parseInt(orderNum));
				}
			}
			else {
				a.setOrderNum3(9999 + Integer.parseInt(orderNum));
				a.setOrderNum2(9999 + Integer.parseInt(orderNum));
				a.setOrderNum4(9999 + Integer.parseInt(orderNum));
			}
			a.setOrderNum(Integer.parseInt(orderNum));
			
		}
		a.setUrl(url);
		a.setName(name);
		saveOrUpdate(a); 
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> deleteMenu(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		MenuEntity a = get(MenuEntity.class, Long.valueOf(id));
		if (a == null) { 
			map.put("code", 1);
			return map;
		}
		a.setStatus("0");
		saveOrUpdate(a);
		MenuEntity parentMenu = a.getParentMenu();
		if(parentMenu != null) {
			String hql = "from MenuEntity as a where a.parentMenu.id=" + id + " and a.status='1'";
			List<MenuEntity> menuList = findByQueryString(hql);
			if(menuList == null || menuList.isEmpty()) {
				parentMenu.setIsHasChild("0");
				saveOrUpdate(parentMenu);
			}
		}
		//同时删除该字菜单，已经孙子菜单
		String hql = "from MenuEntity as a where a.parentMenu.id=" + id;
		List<MenuEntity> parentList = findByQueryString(hql);
		for(MenuEntity menu : parentList) {
			menu.setStatus("0");
		}
		batchSave(parentList);
		
		hql = "from MenuEntity as a where a.gradeMenu.id=" + id;
		List<MenuEntity> gradeList = findByQueryString(hql);
		for(MenuEntity menu : gradeList) {
			menu.setStatus("0");
		}
		batchSave(gradeList);
		map.put("code", 0); 
		return map;
	}

}
