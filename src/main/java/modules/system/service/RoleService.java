package modules.system.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;

public interface RoleService extends CommonService{

	DataTableReturn loadRoleByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveRole(HttpServletRequest request);

	Map<String, Object> deleteRole(HttpServletRequest request);

	Map<String, Object> initRoleMenuTree(Long roleId);

	Map<String, Object> saveRoleMenu(HttpServletRequest request);

}
