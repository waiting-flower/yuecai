package modules.system.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;

public interface MenuService extends CommonService{

	DataTableReturn loadMenuByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveMenu(HttpServletRequest request);

	Map<String, Object> deleteMenu(HttpServletRequest request);

}
