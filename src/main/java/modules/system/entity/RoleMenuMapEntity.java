package modules.system.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
/**
 * 角色菜单映射
 * @author Administrator
 *
 */
@Entity
@Table(name = "sys_role_menu", schema = "")
public class RoleMenuMapEntity extends CommonEntity{
	private RoleEntity role;
	private MenuEntity menu;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "roleId")
	public RoleEntity getRole() {
		return role;
	}
	public void setRole(RoleEntity role) {
		this.role = role;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "menuId")
	public MenuEntity getMenu() {
		return menu;
	}
	public void setMenu(MenuEntity menu) {
		this.menu = menu;
	}
	
	
}
