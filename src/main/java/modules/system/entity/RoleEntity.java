package modules.system.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import common.entity.CommonEntity;
/**
 * 系统角色对象
 * @author Administrator
 *
 */
@Entity
@Table(name = "sys_role", schema = "")
public class RoleEntity extends CommonEntity{
	private String name; //角色名称
	private String note; //角色备注信息
	private Integer orderNum; //角色排序
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Integer getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
