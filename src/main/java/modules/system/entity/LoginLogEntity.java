package modules.system.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import common.entity.CommonEntity;

/**
 * 登录记录
 * @author Administrator
 *
 */
@Entity
@Table(name = "sys_user_login_log", schema = "")
public class LoginLogEntity extends CommonEntity{
	private String userName;
	private String ip;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	
}
