package modules.system.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
/**
 * 系统菜单对象
 * @author Administrator
 *
 */
@Entity
@Table(name = "sys_menu", schema = "")
public class MenuEntity extends CommonEntity{
	private String name;//菜单名称
	private String icon;//菜单的图标
	private String url;//菜单路径
	private String isHasChild;//是否有子菜单
	private Integer orderNum;//显示排序1
	private Integer orderNum2;
	private Integer orderNum3;
	private Integer orderNum4;
	private MenuEntity parentMenu;
	private MenuEntity gradeMenu;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parentMenuId")
	public MenuEntity getParentMenu() {
		return parentMenu;
	}
	public void setParentMenu(MenuEntity parentMenu) {
		this.parentMenu = parentMenu;
	}
	public Integer getOrderNum2() {
		return orderNum2;
	}
	public void setOrderNum2(Integer orderNum2) {
		this.orderNum2 = orderNum2;
	}
	public Integer getOrderNum3() {
		return orderNum3;
	}
	public void setOrderNum3(Integer orderNum3) {
		this.orderNum3 = orderNum3;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "gradeMenuId")
	public MenuEntity getGradeMenu() {
		return gradeMenu;
	}
	public void setGradeMenu(MenuEntity gradeMenu) {
		this.gradeMenu = gradeMenu;
	}
	public Integer getOrderNum4() {
		return orderNum4;
	}
	public void setOrderNum4(Integer orderNum4) {
		this.orderNum4 = orderNum4;
	}
	public String getIsHasChild() {
		return isHasChild;
	}
	public void setIsHasChild(String isHasChild) {
		this.isHasChild = isHasChild;
	}
	
}
