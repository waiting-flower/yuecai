package modules.system.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.util.StringUtil;
import modules.system.entity.MenuEntity;
import modules.system.entity.RoleEntity;
import modules.system.service.RoleService;

/**
 * 系统管理【角色管理】 web层controller
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "admin/role")
public class RoleController extends BaseController{
	@Autowired
	RoleService roleService;
	
	/**
	 * 菜单列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list")
	public String list( HttpServletRequest request, HttpServletResponse response) {
		System.err.println(templatePath);
		return templatePath + "system/roleList";
	}
	
	/**
	 * 角色详情编辑
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "info")
	public String info( HttpServletRequest request, HttpServletResponse response) {
		System.err.println(templatePath);
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			RoleEntity role = roleService.get(RoleEntity.class, Long.valueOf(id));
			request.setAttribute("a", role);
		}
		return templatePath + "system/roleInfo";
	}
	
	/**
	 * 分页获取角色列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadRoleByPage")
	@ResponseBody
	public DataTableReturn loadRoleByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = roleService.loadRoleByPage(dataTable, request);
		return d;
	}
	
	/**
	 * 保存一个角色
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveRole")
	@ResponseBody
	public Map<String, Object> saveRole(HttpServletRequest request) {
		return roleService.saveRole(request);
	}
	
	/**
	 * 删除一个角色
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteRole")
	@ResponseBody
	public Map<String, Object> deleteRole(HttpServletRequest request) {
		return roleService.deleteRole(request);
	}
	
	/**
	 * 角色详情编辑
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "roleSet")
	public String roleSet( HttpServletRequest request, HttpServletResponse response) {
		System.err.println(templatePath);
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			RoleEntity role = roleService.get(RoleEntity.class, Long.valueOf(id));
			request.setAttribute("a", role);
		}
		return templatePath + "system/roleSet";
	}
	
	
	/**
	 * 获取角色的菜单设置json数据
	 * 用于封装ztree
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadRoleSet")
	@ResponseBody
	public Map<String, Object> loadRoleSet(HttpServletRequest request) {
		Long roleId = Long.valueOf(request.getParameter("roleId"));
		return roleService.initRoleMenuTree(roleId);
	}
	
	/**
	 * 保存或者更新一个角色权限配置
	 * 
	 * @param a
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveRoleMenu")
	@ResponseBody
	public Map<String, Object> saveRoleMenu(HttpServletRequest request) {
		return roleService.saveRoleMenu(request);
	}
}
