package modules.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.mail.entity.MailEntity;
import common.mail.service.MailService;
import common.util.Constant;
import common.util.MD5Util;
import common.util.StringUtil;
import common.util.VerificationCodeUtil;
import modules.system.entity.MenuEntity;
import modules.system.entity.RoleEntity;
import modules.system.entity.UserEntity;
/**
 * 用户管理 web层controller
 * @author Administrator
 *
 */
import modules.system.service.UserService;
@Controller
@RequestMapping(value = "admin/user")
public class UserController extends BaseController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	MailService mailService;
	
	

	/**
	 * 用户列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list")
	public String list( HttpServletRequest request, HttpServletResponse response) {
		return templatePath + "system/userList";
	}
	
	/**
	 * 用户详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "info")
	public String info( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			UserEntity menu = userService.get(UserEntity.class, Long.valueOf(id));
			request.setAttribute("a", menu);
		}
		String coopId = request.getParameter("coopId");
		request.setAttribute("coopId", coopId);
		String hql = "from RoleEntity as a where a.status = '1'";
		List<RoleEntity> roleList = userService.findByQueryString(hql);
		request.setAttribute("roleList", roleList);
		return templatePath + "system/userInfo";
	}
	
	/**
	 * 用户详情【合作者】
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "infoForCoop")
	public String infoForCoop( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			UserEntity menu = userService.get(UserEntity.class, Long.valueOf(id));
			request.setAttribute("a", menu);
		}
		String coopId = request.getParameter("coopId");
		request.setAttribute("coopId", coopId);
		return templatePath + "system/userInfoForCoop";
	}
	
	
	/**
	 * 用户详情【合作者】
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "infoForAccount")
	public String infoForAccount( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			UserEntity menu = userService.get(UserEntity.class, Long.valueOf(id));
			request.setAttribute("a", menu);
		}
		String accountId = request.getParameter("accountId");
		request.setAttribute("accountId", accountId);
		return templatePath + "system/userInfoForAccount";
	}
	
	/**
	 * 分页获取用户列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadUserByPage")
	@ResponseBody
	public DataTableReturn loadUserByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = userService.loadUserByPage(dataTable, request);
		return d;
	}
	
	/**
	 * 保存一个用户
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveUser")
	@ResponseBody
	public Map<String, Object> saveUser(HttpServletRequest request) {
		return userService.saveUser(request);
	}
	
	/**
	 * 删除一个用户
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteUser")
	@ResponseBody
	public Map<String, Object> deleteUser(HttpServletRequest request) {
		return userService.deleteUser(request);
	}
	
	/**
	 * 恢复一个用户
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveUserOk")
	@ResponseBody
	public Map<String, Object> saveUserOk(HttpServletRequest request) {
		return userService.saveUserOk(request);
	}
	
	/**
	 * 用户登录
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "login")
	@ResponseBody
	public Map<String, Object> ajaxLogin(HttpServletRequest request,HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "1");
		String vercode = request.getParameter("vercode");
		String randomPic = (String) request.getSession().getAttribute("randomPic");
		if(StringUtil.isEmpty(randomPic) || StringUtil.isEmpty(vercode) || !vercode.toLowerCase().equals(randomPic.toLowerCase())){
			map.put("status", "-1");
			return map;
		}
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		UserEntity u = userService.workLogin(userName, password,request);
		if(u == null){
			map.put("status", "0");
			return map;
		}
		if(u.getStatus().equals("0")) {
			map.put("status", "2");
			return map;
		}
		
		request.getSession().setAttribute(Constant.ADMIN_USER_KEY, u);
		return map;
	}
	
	/**
	 * 用户注册
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "reg")
	@ResponseBody
	public Map<String, Object> reg(HttpServletRequest request,HttpServletResponse response) {
		return userService.saveReg(request);
	}
	
	
	@RequestMapping(value = "findpass")
	@ResponseBody
	public Map<String, Object> findpass(HttpServletRequest request,HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		String vercode = request.getParameter("vercode");
		String randomPic = (String) request.getSession().getAttribute("randomPic");
		if(StringUtil.isEmpty(randomPic) || StringUtil.isEmpty(vercode) || !vercode.toLowerCase().equals(randomPic.toLowerCase())){
			map.put("code", "-1");
			return map;
		}
		String userName = request.getParameter("userName"); 
		String email = request.getParameter("email"); 
		String hql = "from UserEntity as a where a.userName=? and a.email = ? ";
		List<UserEntity> userList = userService.findHql(hql, new Object[] {userName,email});
		if(userList == null || userList.isEmpty()) {
			map.put("code", 1);
			map.put("msg", "您输入的邮箱或者用户名有误");
			return map;
		}
		String[] letter = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
				"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
				"W", "X", "Y", "Z" };
		String sRand = "";
		Random random = new Random();
		for (int i = 0; i < 6; i++) {
			String tempRand = "";
			if (random.nextBoolean()) {
				tempRand = String.valueOf(random.nextInt(10));
				if (random.nextBoolean()) {
					sRand += random.nextInt(10);
				}
			} else {
				tempRand = letter[random.nextInt(25)];
				if (random.nextBoolean()) {// 随机将该字母变成小写
					tempRand = tempRand.toLowerCase();
				}
				if (random.nextBoolean()) {
					sRand += random.nextInt(10);
				}
			}
			sRand += tempRand;
		}
		UserEntity user = userList.get(0);
		user.setPassword(MD5Util.md5Encode(sRand));
		userService.saveOrUpdate(user);
		MailEntity mm = new MailEntity();  
	    mm.setFromAddress("lpf_991@sina.com");  
	    mm.setToAddresses(email);  
	    mm.setContent("您的新密码为：<br>" + sRand + "<br>请登录系统后，立即修改您的密码");  
	    mm.setSubject("找回密码");   
	    mailService.sendAttachMail(mm);
	    map.put("code", 0);
		return map;
	}
	
	
}
