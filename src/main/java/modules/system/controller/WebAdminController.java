
package modules.system.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.events.Comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import common.controller.BaseController;
import common.util.Constant;
import common.util.VerificationCodeUtil;
import modules.system.entity.MenuEntity;
import modules.system.entity.UserEntity;
import modules.system.service.AdminService;
import modules.system.service.RoleService;

@Controller
@RequestMapping(value = "/")
public class WebAdminController extends BaseController {

    @Autowired
    RoleService roleService;

    @Autowired
    AdminService adminService;

    /**
     * 登录页面
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "login")
    public String list(HttpServletRequest request, HttpServletResponse response) {
        return templatePath + "login";
    }

    /**
     * 注册页面
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "reg")
    public String reg(HttpServletRequest request, HttpServletResponse response) {
        return templatePath + "reg";
    }

    /**
     * 找回密码页面
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "findpass")
    public String findpass(HttpServletRequest request, HttpServletResponse response) {
        return templatePath + "findpass";
    }

    /**
     * 主页
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "home")
    public String home(HttpServletRequest request, HttpServletResponse response) {
        /**
         * 初始化首页数据
         */

        adminService.initHome(request);
        return templatePath + "home";
    }

    @RequestMapping(value = "getLastNewUser")
    @ResponseBody
    public Map<String, Object> getLastNewUser(HttpServletRequest request) {
        return adminService.getLastNewUser(request);
    }

    /**
     * 首页
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "index")
    public String index(HttpServletRequest request, HttpServletResponse response) {
        UserEntity user = (UserEntity) request.getSession().getAttribute(Constant.ADMIN_USER_KEY);
        if (user != null) {
			request.setAttribute("userList", user);//

            request.setAttribute("userinfo", user);
            String hql = "from MenuEntity as a where exists (select 1 from RoleMenuMapEntity as b where b.menu.id = a.id and b.role.id = "
                    + user.getRole().getId() + ") order by a.orderNum4 desc,a.orderNum3 desc, a.orderNum2 desc,a.orderNum desc";
            List<MenuEntity> menuList = roleService.findByQueryString(hql);
            List<MenuEntity> menuListP1 = new ArrayList<>();//所有的一级菜单
            List<MenuEntity> menuListP2 = new ArrayList<>();//所有的二级菜单
            List<MenuEntity> menuListP3 = new ArrayList<>();//所有的三级菜单
            for (MenuEntity menu : menuList) {
                if (menu.getParentMenu() == null) {
                    menuListP1.add(menu);
                } else {
                    if (menu.getGradeMenu() == null) {
                        menuListP2.add(menu);
                    } else {
                        menuListP3.add(menu);
                    }
                }
            }
            request.setAttribute("menuListP1", menuListP1);//最多三级菜单
            request.setAttribute("menuListP2", menuListP2);//最多三级菜单
            request.setAttribute("menuListP3", menuListP3);//最多三级菜单


        }

        return templatePath + "index";
    }

    /**
     * 获取后台登录页面的图片验证码
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "getRandomPic", method = RequestMethod.GET)
    public ByteArrayInputStream getRandomPic(HttpServletRequest request, HttpServletResponse response) {
        VerificationCodeUtil vcu = VerificationCodeUtil.Instance();
        request.getSession().setAttribute("randomPic", vcu.getVerificationCodeValue());
        try {
            ServletOutputStream out = response.getOutputStream();
            BufferedImage bi = ImageIO.read(vcu.getImage());
            ImageIO.write(bi, "jpeg", out);
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 获取用户新增情况
     *
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "getUserData")
    @ResponseBody
    public Map<String, Object> getUserData(HttpServletRequest request) throws Exception {
        return adminService.getUserData(request);
    }

    /**
     * 获取订单新增情况
     *
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "getOrderData")
    @ResponseBody
    public Map<String, Object> getOrderData(HttpServletRequest request) throws Exception {
        return adminService.getOrderData(request);
    }

    /**
     * 获取团长新增情况
     *
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "getTeamData")
    @ResponseBody
    public Map<String, Object> getTeamData(HttpServletRequest request) throws Exception {
        return adminService.getTeamData(request);
    }


    /**
     * 获取售后新增情况
     *
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "getAfterData")
    @ResponseBody
    public Map<String, Object> getAfterData(HttpServletRequest request) throws Exception {
        return adminService.getAfterData(request);
    }


    /**
     * 获取订单的列表统计【图表统计】
     *
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "getOrderBarList")
    @ResponseBody
    public Map<String, Object> getOrderBarList(HttpServletRequest request) throws Exception {
        return adminService.getOrderBarList(request);
    }

}

