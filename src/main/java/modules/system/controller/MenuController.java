package modules.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.util.StringUtil;
import modules.system.entity.MenuEntity;
import modules.system.service.MenuService;

/**
 * 系统管理【菜单管理】web层controller
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "admin/menu")
public class MenuController extends BaseController{

	@Autowired
	MenuService menuService;
	
	/**
	 * 菜单列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list")
	public String list( HttpServletRequest request, HttpServletResponse response) {
		System.err.println(templatePath);
		return templatePath + "system/menuList";
	}
	
	/**
	 * 菜单编辑列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "info")
	public String info( HttpServletRequest request, HttpServletResponse response) {
		System.err.println(templatePath);
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			MenuEntity menu = menuService.get(MenuEntity.class, Long.valueOf(id));
			request.setAttribute("a", menu);
		}
		String hql = "from MenuEntity as a where a.status = '1' order by a.orderNum4 desc,a.orderNum3 desc, a.orderNum2 desc,a.orderNum desc";
		List<MenuEntity> menuList = menuService.findByQueryString(hql);
		request.setAttribute("menuList", menuList);
		return templatePath + "system/menuInfo";
	}
	
	/**
	 * 分页获取菜单列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadMenuByPage")
	@ResponseBody
	public DataTableReturn loadColumnByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = menuService.loadMenuByPage(dataTable, request);
		return d;
	}
	
	/**
	 * 保存一个菜单
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveMenu")
	@ResponseBody
	public Map<String, Object> saveMenu(HttpServletRequest request) {
		return menuService.saveMenu(request);
	}
	
	/**
	 * 删除一个菜单
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteMenu")
	@ResponseBody
	public Map<String, Object> deleteMenu(HttpServletRequest request) {
		return menuService.deleteMenu(request);
	}
}
