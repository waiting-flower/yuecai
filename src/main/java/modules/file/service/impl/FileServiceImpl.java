package modules.file.service.impl;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.impl.CommonServiceImpl;
import common.util.Constant;
import common.util.DateUtil;
import common.util.HqlQuery;
import common.util.ImageUtil;
import common.util.StringUtil;
import modules.file.entity.FileCateEntity;
import modules.file.entity.FileEntity;
import modules.file.entity.FileUserCateEntity;
import modules.file.service.FileService;
import modules.file.util.FileConstant;
import modules.system.entity.MenuEntity;
import modules.system.entity.UserEntity;

@Service
public class FileServiceImpl extends CommonServiceImpl implements FileService{

	@Override
	public DataTableReturn loadFileByPage(JQueryDataTables dataTable, HttpServletRequest request) {
		StringBuffer hql = new StringBuffer(
				" from FileEntity as  a where 1 = 1 ");
		List<Object> params = new ArrayList<Object>();
		String name = request.getParameter("name");
		if (StringUtil.isNotEmpty(name)) {
			hql.append(" and a.name like ? ");
			params.add("%" + name + "%");
		}
		hql.append(" order by a.createDate desc");
		HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
		int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
		hqlQuery.setCurPage(currPage + 1);
		hqlQuery.setPageSize(dataTable.getDisplayLength());
		DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
		List<FileEntity> pList = (List<FileEntity>) d.getAaData();
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		/*
		 * private UserEntity user;//所属的用户
	private FileCateEntity cate;
	
	private String name;//系统生成的文件名称
	private String url;//系统生成的文件路径
	private String fileExt;//文件后缀
	private String fileType;//文件类型
	private String originalName;//文件原始名称
		 */
		for (FileEntity p : pList) {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("id", p.getId());
			item.put("name", p.getName());
			item.put("url", p.getUrl());
			item.put("fileExt", p.getFileExt());
			item.put("fileType", p.getFileType());
			item.put("originalName", p.getOriginalName());
			item.put("status", p.getStatus());
			item.put("time",DateUtil.getTimeByCustomPattern( p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
			mapList.add(item);
		}
		d.setAaData(mapList);
		d.setsEcho(dataTable.getEcho());
		return d;
	}

	@Override
	public Map<String, Object> saveFile(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> deleteFile(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DataTableReturn loadFileCateByPage(JQueryDataTables dataTable, HttpServletRequest request) {
		StringBuffer hql = new StringBuffer(
				" from FileCateEntity as  a where 1 = 1 ");
		List<Object> params = new ArrayList<Object>();
		String name = request.getParameter("name");
		if (StringUtil.isNotEmpty(name)) {
			hql.append(" and a.name like ? ");
			params.add("%" + name + "%");
		}
		hql.append(" order by a.createDate desc");
		HqlQuery hqlQuery = new HqlQuery(hql.toString(), params.toArray());
		int currPage = dataTable.getDisplayStart() / dataTable.getDisplayLength();
		hqlQuery.setCurPage(currPage + 1);
		hqlQuery.setPageSize(dataTable.getDisplayLength());
		DataTableReturn d = commonDao.loadDataByPage(hqlQuery, true);
		List<FileCateEntity> pList = (List<FileCateEntity>) d.getAaData();
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		for (FileCateEntity p : pList) {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("id", p.getId());
			item.put("name", p.getName());
			item.put("num", p.getNum());
			item.put("orderNum", p.getOrderNum());
			item.put("status", p.getStatus());
			item.put("time",DateUtil.getTimeByCustomPattern( p.getCreateDate(), DateUtil.yyyyMMddHHmmss));
			mapList.add(item);
		}
		d.setAaData(mapList);
		d.setsEcho(dataTable.getEcho());
		return d;
	}

	@Override
	public Map<String, Object> saveFileCate(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String orderNum = request.getParameter("orderNum");
		FileCateEntity a = null;
		if(StringUtil.isNotEmpty(id)) {
			a = get(FileCateEntity.class, Long.valueOf(id));
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
		}
		else {
			a = new FileCateEntity();
			a.setCreateDate(DateUtil.getCurrentTime());
			a.setUpdateDate(DateUtil.getCurrentTime());
			a.setStatus("1");
			a.setNum(0);
		}
		a.setName(name);
		a.setOrderNum(Integer.parseInt(orderNum));
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> deleteFileCate(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String id = request.getParameter("id");
		FileCateEntity a = get(FileCateEntity.class, Long.valueOf(id));
		if (a == null) { 
			map.put("code", 1);
			return map;
		}
		a.setStatus("0");
		saveOrUpdate(a);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> getAllFileCateList(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String hql = "from FileCateEntity as a where a.status='1' and a.parentCate is null order by a.orderNum desc";
		List<FileCateEntity> cateList = findByQueryString(hql);
		List<Map<String, Object>> list = new ArrayList<>();
		Map<String, Object> item = new HashMap<>();
		item.put("id", "");
		item.put("name","全部");
		list.add(item);
		for(FileCateEntity cate : cateList) {
			item = new HashMap<>();
			item.put("id", cate.getId());
			item.put("name",cate.getName());
			list.add(item);
		}
		map.put("code", 0);
		map.put("list", list);
		return map;
	}

	@Override
	public Map<String, Object> getFileCateListByParent(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		String cateId = request.getParameter("cateId");
		String hql = "from FileCateEntity as a where a.status='1' and a.parentCate.id =" + cateId + " order by a.orderNum desc";
		List<FileCateEntity> cateList = findByQueryString(hql);
		List<Map<String, Object>> list = new ArrayList<>();
		Map<String, Object> item = new HashMap<>();
		item.put("id", "");
		item.put("name","全部");
		list.add(item);
		for(FileCateEntity cate : cateList) {
			item = new HashMap<>();
			item.put("id", cate.getId());
			item.put("name",cate.getName());
			list.add(item);
		}
		map.put("code", 0);
		map.put("list", list);
		return map;
	}

	@Override
	public Map<String, Object> workUserFileCate(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		UserEntity user = getLoginUser(request);
		if(user == null) {
			map.put("code", Constant.NO_LOGIN_CODE);
			map.put("code", Constant.NO_LOGIN_MSG);
			return map;
		}
		String hql = "from FileUserCateEntity as a where a.status='1' and a.name='商品轮播图' and a.user.id=" + user.getId();
		List<FileUserCateEntity> cateList = findByQueryString(hql);
		if(cateList == null || cateList.isEmpty()) {
			FileUserCateEntity fuc = new FileUserCateEntity();
			fuc.init();
			fuc.setOrderNum(9);
			fuc.setUser(user);
			fuc.setName("商品轮播图");
			save(fuc);
			
			fuc = new FileUserCateEntity();
			fuc.init();
			fuc.setOrderNum(9);
			fuc.setUser(user);
			fuc.setName("商品封面图");
			save(fuc);
			
			fuc = new FileUserCateEntity();
			fuc.init();
			fuc.setOrderNum(9);
			fuc.setUser(user);
			fuc.setName("商品详情图");
			save(fuc);
		}
		hql = "from FileUserCateEntity as a where a.status='1'  and a.user.id=" + user.getId() + " order by a.orderNum desc";
		cateList = findByQueryString(hql);
		
		List<Map<String, Object>> list = new ArrayList<>();
		Map<String, Object> item = new HashMap<>();
		item.put("id", "");
		item.put("name","全部");
		list.add(item);
		for(FileUserCateEntity cate : cateList) {
			item = new HashMap<>();
			item.put("id", cate.getId());
			item.put("name",cate.getName());
			item.put("orderNum", cate.getOrderNum());
			list.add(item);
		}
		map.put("code", 0);
		map.put("list", list);
		return map;
	}

	@Override
	public Map<String, Object> saveUserFileCate(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		UserEntity user = getLoginUser(request);
		if(user == null) {
			map.put("code", Constant.NO_LOGIN_CODE);
			map.put("code", Constant.NO_LOGIN_MSG);
			return map;
		}
		String name = request.getParameter("name");
		String id = request.getParameter("id");
		FileUserCateEntity fuc = null;
		if(StringUtil.isEmpty(id)) {
			fuc = new FileUserCateEntity();
			String hql = "from FileUserCateEntity as a where a.user.id=" + user.getId() + " and a.name='" + name + "' and a.status='1'";
			List<FileUserCateEntity> fucList = findByQueryString(hql);
			if(fucList != null && !fucList.isEmpty()) {
				map.put("code", 1);
				map.put("msg", "该分类名称已经存在，请重新输入");
				return map;
			}
			fuc.init();
			fuc.setOrderNum(1);
			fuc.setUser(user);
			fuc.setName(name);
			save(fuc);
		}
		else {
			fuc = get(FileUserCateEntity.class, Long.valueOf(id));
			fuc.setName(name);
			String hql = "from FileUserCateEntity as a where a.user.id=" + user.getId() + " and a.name='" + name + "' and a.status='1'";
			List<FileUserCateEntity> fucList = findByQueryString(hql);
			if(fucList != null && !fucList.isEmpty()) {
				for(FileUserCateEntity fuc2 : fucList) {
					if(!fuc2.getId().equals(fuc.getId())) {
						map.put("code", 1);
						map.put("msg", "该分类名称已经存在，请重新输入");
						return map;
					}
				}
			}
			save(fuc);
		}
		map.put("id", fuc.getId());
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> deleteUserFileCate(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		UserEntity user = getLoginUser(request);
		if(user == null) {
			map.put("code", Constant.NO_LOGIN_CODE);
			map.put("code", Constant.NO_LOGIN_MSG);
			return map;
		}
		String id = request.getParameter("id");
		FileUserCateEntity fuc = get(FileUserCateEntity.class, Long.valueOf(id));
		fuc.setStatus("0");
		saveOrUpdate(fuc);
		
		String hql = "from FileEntity as a where a.userCate.id=" + id + " and a.status='1'";
		List<FileEntity> fileList = findByQueryString(hql);
		for(FileEntity file : fileList) {
			file.setUserCate(null);
		}
		batchSave(fileList);
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> getFileListByCate(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		UserEntity user = getLoginUser(request);
		if(user == null) {
			map.put("code", Constant.NO_LOGIN_CODE);
			map.put("code", Constant.NO_LOGIN_MSG);
			return map;
		}
		String cateId = request.getParameter("cateId");
		String parentCateId = request.getParameter("parentCateId");
		String hql = "from FileEntity as a where a.user is null  and a.status='1'";
		if(StringUtil.isNotEmpty(parentCateId)) {
			hql = hql + " and a.cate.parentCate.id=" + parentCateId;
		}
		hql = hql + "  order by a.createDate desc";
		if(StringUtil.isNotEmpty(cateId)) {
			hql = "from FileEntity as a where a.user is null and a.cate.id=" + cateId + " and a.status='1' order by a.createDate desc";
		}
		List<FileEntity> fileList = findByQueryString(hql);
		List<Map<String, Object>> list = new ArrayList<>();
		for(FileEntity file : fileList) {
			Map<String, Object> item = new HashMap<>();
			item.put("id", file.getId());
			item.put("url", file.getUrl());
			list.add(item);
		}
		map.put("code", 0);
		map.put("list", list);
		return map;
	}

	@Override
	public Map<String, Object> getFileListByUserCate(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		UserEntity user = getLoginUser(request);
		if(user == null) {
			map.put("code", Constant.NO_LOGIN_CODE);
			map.put("code", Constant.NO_LOGIN_MSG);
			return map;
		}
		String cateId = request.getParameter("cateId");
		String hql = "from FileEntity as a where  a.status='1' and a.userCate.id="
				+ cateId + "  order by a.createDate desc";
		if(StringUtil.isEmpty(cateId)) {
			hql = "from FileEntity as a where a.status='1' and a.user.id=" + user.getId() + " order by a.createDate desc";
		}
		List<FileEntity> fileList = findByQueryString(hql);
		List<Map<String, Object>> list = new ArrayList<>();
		for(FileEntity file : fileList) {
			Map<String, Object> item = new HashMap<>();
			item.put("id", file.getId());
			item.put("url", file.getUrl());
			list.add(item);
		}
		map.put("code", 0);
		map.put("list", list);
		return map;
	}

	@Override
	public Map<String, Object> deleteUserFile(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		UserEntity user = getLoginUser(request);
		if(user == null) {
			map.put("code", Constant.NO_LOGIN_CODE);
			map.put("code", Constant.NO_LOGIN_MSG);
			return map;
		}
		String id = request.getParameter("id");
		FileEntity file = get(FileEntity.class, Long.valueOf(id));
		file.setStatus("0");
		saveOrUpdate(file); 
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> deleteUserFileByIds(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		UserEntity user = getLoginUser(request);
		if(user == null) {
			map.put("code", Constant.NO_LOGIN_CODE);
			map.put("code", Constant.NO_LOGIN_MSG);
			return map;
		}
		String ids = request.getParameter("ids");
		String[] idArr = ids.split(",");
		for(String id : idArr) {
			FileEntity file = get(FileEntity.class, Long.valueOf(id));
			file.setStatus("0");
			saveOrUpdate(file); 
		}
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> saveUserFileMoveByIds(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		UserEntity user = getLoginUser(request);
		if(user == null) {
			map.put("code", Constant.NO_LOGIN_CODE);
			map.put("code", Constant.NO_LOGIN_MSG);
			return map;
		}
		String cateId = request.getParameter("cateId");
		FileUserCateEntity cate = get(FileUserCateEntity.class, Long.valueOf(cateId));
		String ids = request.getParameter("ids");
		String[] idArr = ids.split(",");
		for(String id : idArr) {
			FileEntity file = get(FileEntity.class, Long.valueOf(id));
			file.setUserCate(cate);
			saveOrUpdate(file); 
		}
		map.put("code", 0);
		return map;
	}

	@Override
	public Map<String, Object> saveFileCutForPic(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>(); 
		UserEntity user = getLoginUser(request);
		if(user == null) {
			map.put("code", Constant.NO_LOGIN_CODE);
			map.put("code", Constant.NO_LOGIN_MSG);
			return map;
		}
		String fileId = request.getParameter("fileId");
		String imgUrl = request.getParameter("imgUrl");
		String xStr = request.getParameter("x");
		String yStr = request.getParameter("y");
		String wStr = request.getParameter("w");
		String hStr = request.getParameter("h"); 
		String showWidth = request.getParameter("showWidth");
		String showHeight = request.getParameter("showHeight");
		
		InputStream is = ImageUtil.cutWebUrlPic(imgUrl, Double.valueOf(xStr), Double.valueOf(yStr),
				Double.valueOf(wStr), Double.valueOf(hStr), 
				Double.valueOf(showWidth), Double.valueOf(showHeight));
		Configuration cfg = new Configuration(Zone.zone0());
		UploadManager uploadManager = new UploadManager(cfg);
		// ...生成上传凭证，然后准备上传
		Auth auth = Auth.create(FileConstant.QINIU_AK, FileConstant.QINIU_SK);
		String upToken = auth.uploadToken(FileConstant.QINIU_bucket);
		// 默认不指定key的情况下，以文件内容的hash值作为文件名
		String key = null;
		Response res;
		try {
			res = uploadManager.put(is, key,
					upToken, null, null);
			// 解析上传成功的结果
			DefaultPutRet putRet = new Gson().fromJson(
					res.bodyString(), DefaultPutRet.class);
			String returnUrl = FileConstant.QINIU_DOMAIN + putRet.key;
			System.out.println("文件上传成功：" + returnUrl);
			map.put("url", returnUrl);
			if(StringUtil.isNotEmpty(fileId)) {
				FileEntity file = get(FileEntity.class, Long.valueOf(fileId));
				FileEntity newFile = new FileEntity();
				newFile.setCreateDate(DateUtil.getCurrentTime());
				newFile.setUpdateDate(DateUtil.getCurrentTime());
				newFile.setStatus("1");
				newFile.setFileExt(file.getFileExt());
				newFile.setFileType(file.getFileType());
				newFile.setName(file.getName());
				newFile.setOriginalName(file.getOriginalName());
				newFile.setUserCate(file.getUserCate());
				newFile.setUser(file.getUser());
				newFile.setUrl(returnUrl);
				save(newFile);
				map.put("id", newFile.getId());
			}
		} catch (QiniuException e) {
			e.printStackTrace();
		}
		map.put("code", 0);
		return map;
	}

}
