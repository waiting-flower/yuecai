package modules.file.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.service.CommonService;

public interface FileService extends CommonService{

	DataTableReturn loadFileByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveFile(HttpServletRequest request);

	Map<String, Object> deleteFile(HttpServletRequest request);

	DataTableReturn loadFileCateByPage(JQueryDataTables dataTable, HttpServletRequest request);

	Map<String, Object> saveFileCate(HttpServletRequest request);

	Map<String, Object> deleteFileCate(HttpServletRequest request);

	Map<String, Object> getAllFileCateList(HttpServletRequest request);

	Map<String, Object> getFileCateListByParent(HttpServletRequest request);

	Map<String, Object> workUserFileCate(HttpServletRequest request);

	Map<String, Object> saveUserFileCate(HttpServletRequest request);

	Map<String, Object> deleteUserFileCate(HttpServletRequest request);

	Map<String, Object> getFileListByCate(HttpServletRequest request);

	Map<String, Object> getFileListByUserCate(HttpServletRequest request);

	Map<String, Object> deleteUserFile(HttpServletRequest request);

	Map<String, Object> deleteUserFileByIds(HttpServletRequest request);

	Map<String, Object> saveUserFileMoveByIds(HttpServletRequest request);

	Map<String, Object> saveFileCutForPic(HttpServletRequest request);

}
