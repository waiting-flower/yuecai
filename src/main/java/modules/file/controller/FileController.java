package modules.file.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;

import common.controller.BaseController;
import common.entity.DataTableReturn;
import common.entity.JQueryDataTables;
import common.util.Constant;
import common.util.DateUtil;
import common.util.FileUtil;
import common.util.StringUtil;
import modules.file.entity.FileCateEntity;
import modules.file.entity.FileEntity;
import modules.file.entity.FileUserCateEntity;
import modules.file.service.FileService;
import modules.file.util.FileConstant;
import modules.system.entity.UserEntity;

/**
 * 文件web层controller
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "admin/file")
public class FileController extends BaseController{

	@Autowired
	FileService fileService;
	
	/**
	 * 文件列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "list")
	public String list( HttpServletRequest request, HttpServletResponse response) {
		return templatePath + "file/fileList";
	}
	
	/**
	 * 文件详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "info")
	public String info( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			FileEntity menu = fileService.get(FileEntity.class, Long.valueOf(id));
			request.setAttribute("a", menu);
		} 
		String hql = "from FileCateEntity as a where a.status='1' order by a.orderNum desc";
		List<FileCateEntity> cateList = fileService.findByQueryString(hql);
		request.setAttribute("cateList", cateList);
		return templatePath + "file/fileInfo";
	}
	
	
	/**
	 * 分页获取文件列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadFileByPage")
	@ResponseBody
	public DataTableReturn loadFileByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = fileService.loadFileByPage(dataTable, request);
		return d;
	}
	
	/**
	 * 保存一个文件
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveFile")
	@ResponseBody
	public Map<String, Object> saveFile(HttpServletRequest request) {
		return fileService.saveFile(request);
	}
	
	/**
	 * 删除一个文件
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteFile")
	@ResponseBody
	public Map<String, Object> deleteFile(HttpServletRequest request) {
		return fileService.deleteFile(request);
	}
	
	
	
	/**
	 * 文件分类列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "cate/list")
	public String cateList( HttpServletRequest request, HttpServletResponse response) {
		return templatePath + "file/cateList";
	}
	
	/**
	 * 文件分类详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "cate/info")
	public String cateInfo( HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		if (StringUtil.isNotEmpty(id)) {
			FileCateEntity menu = fileService.get(FileCateEntity.class, Long.valueOf(id));
			request.setAttribute("a", menu);
		} 
		return templatePath + "file/cateInfo";
	}
	
	
	/**
	 * 分页获取文件分类列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "loadFileCateByPage")
	@ResponseBody
	public DataTableReturn loadFileCateByPage(HttpServletRequest request) {
		JQueryDataTables dataTable = new JQueryDataTables(request);
		DataTableReturn d = fileService.loadFileCateByPage(dataTable, request);
		return d;
	}
	
	/**
	 * 保存一个文件分类
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveFileCate")
	@ResponseBody
	public Map<String, Object> saveFileCate(HttpServletRequest request) {
		return fileService.saveFileCate(request);
	}
	
	/**
	 * 删除一个文件分类
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteFileCate")
	@ResponseBody
	public Map<String, Object> deleteFileCate(HttpServletRequest request) {
		return fileService.deleteFileCate(request);
	}
	
	
	///////////////////////////暴露给其他的上传接口//////////////////////////////
	
	/**
	 * 暴露出去的上传接口
	 * @param request
	 * @param response
	 * @param file
	 * @return
	 * @throws QiniuException
	 * @throws IOException
	 */
	@RequestMapping(value = "uploadNormalImg", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> uploadNormalImg(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam("file") MultipartFile file) throws QiniuException, IOException {
		Map<String, Object> map = new HashMap<>();
		// 构造一个带指定Zone对象的配置类
		Configuration cfg = new Configuration(Zone.zone0());
		UploadManager uploadManager = new UploadManager(cfg);
		// ...生成上传凭证，然后准备上传
		Auth auth = Auth.create(FileConstant.QINIU_AK, FileConstant.QINIU_SK);
		String upToken = auth.uploadToken(FileConstant.QINIU_bucket);
		// 默认不指定key的情况下，以文件内容的hash值作为文件名
		String key = null;
		
		Response res = uploadManager.put(file.getInputStream(), key,
				upToken, null, null);
		// 解析上传成功的结果
		DefaultPutRet putRet = new Gson().fromJson(
				res.bodyString(), DefaultPutRet.class);
		String returnUrl = FileConstant.QINIU_DOMAIN + putRet.key;
		System.out.println("文件上传成功：" + returnUrl);
		String originName = file.getOriginalFilename();
		String fileExt = FileUtil.getExtend(originName);
		map.put("code", 0);
		map.put("url", returnUrl);
		return map;
	}
	
	
	/**
	 * 暴露出去的上传接口
	 * @param request
	 * @param response
	 * @param file
	 * @return
	 * @throws QiniuException
	 * @throws IOException
	 */
	@RequestMapping(value = "uploadUserImg", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> uploadUserImg(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam("file") MultipartFile file) throws QiniuException, IOException {
		Map<String, Object> map = new HashMap<>();
		UserEntity user = fileService.getLoginUser(request);
		if(user == null) {
			map.put("code", Constant.NO_LOGIN_CODE);
			map.put("code", Constant.NO_LOGIN_MSG);
			return map;
		}
		
		
		// 构造一个带指定Zone对象的配置类
		Configuration cfg = new Configuration(Zone.zone0());
		UploadManager uploadManager = new UploadManager(cfg);
		// ...生成上传凭证，然后准备上传
		Auth auth = Auth.create(FileConstant.QINIU_AK, FileConstant.QINIU_SK);
		String upToken = auth.uploadToken(FileConstant.QINIU_bucket);
		// 默认不指定key的情况下，以文件内容的hash值作为文件名
		String key = null;
		
		Response res = uploadManager.put(file.getInputStream(), key,
				upToken, null, null);
		// 解析上传成功的结果
		DefaultPutRet putRet = new Gson().fromJson(
				res.bodyString(), DefaultPutRet.class);
		String returnUrl = FileConstant.QINIU_DOMAIN + putRet.key;
		System.out.println("文件上传成功：" + returnUrl);
		String originName = file.getOriginalFilename();
		String fileExt = FileUtil.getExtend(originName);
		
		FileEntity f = new FileEntity();
		f.setCreateDate(DateUtil.getCurrentTime());
		f.setUpdateDate(DateUtil.getCurrentTime());
		f.setStatus("1");
		f.setFileExt(fileExt);
		f.setName(putRet.key);
		f.setOriginalName(originName);
		f.setFileExt(fileExt);
		f.setFileType("image");
		f.setUrl(returnUrl);
		f.setUser(user);
		f.setUserCate(null);
		String cateId = request.getParameter("cateId");
		if(StringUtil.isNotEmpty(cateId)) {
			f.setUserCate(fileService.get(FileUserCateEntity.class, Long.valueOf(cateId)));
		}
		fileService.save(f);
		
		map.put("code", 0);
		map.put("id", f.getId());
		map.put("url", returnUrl);
		return map;
	}
	/**
	 * 暴露出去的上传图片接口
	 * @param request
	 * @param response
	 * @param file
	 * @return
	 * @throws QiniuException
	 * @throws IOException
	 */
	@RequestMapping(value = "/uploadToQiniu", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> uploadToQiniu(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam("file") MultipartFile[] files) throws QiniuException, IOException {
		Map<String, Object> map = new HashMap<>();
		// 构造一个带指定Zone对象的配置类
		Configuration cfg = new Configuration(Zone.zone0());
		UploadManager uploadManager = new UploadManager(cfg);
		// ...生成上传凭证，然后准备上传
		Auth auth = Auth.create(FileConstant.QINIU_AK, FileConstant.QINIU_SK);
		String upToken = auth.uploadToken(FileConstant.QINIU_bucket);
		// 默认不指定key的情况下，以文件内容的hash值作为文件名
		String key = null;
		
		List<Map<String, Object>> list = new ArrayList<>();
		for (MultipartFile file : files) {
			Response res = uploadManager.put(file.getInputStream(), key,
					upToken, null, null);
			// 解析上传成功的结果
			DefaultPutRet putRet = new Gson().fromJson(
					res.bodyString(), DefaultPutRet.class);
			String returnUrl = FileConstant.QINIU_DOMAIN + putRet.key;
			System.out.println("文件上传成功：" + returnUrl);
			String originName = file.getOriginalFilename();
			String fileExt = FileUtil.getExtend(originName);
			
			/*
			 * private UserEntity user;//所属的用户
	private FileCateEntity cate;
	
	private String name;//系统生成的文件名称
	private String url;//系统生成的文件路径
	private String fileExt;//文件后缀
	private String fileType;//文件类型
	private String originalName;//文件原始名称
			 */
			FileCateEntity cate = fileService.get(FileCateEntity.class, Long.valueOf(request.getParameter("cate")));
			cate.setNum(cate.getNum() + 1);
			fileService.saveOrUpdate(cate);
			
			FileEntity f = new FileEntity();
			f.setCreateDate(DateUtil.getCurrentTime());
			f.setUpdateDate(DateUtil.getCurrentTime());
			f.setStatus("1");
			f.setFileExt(fileExt);
			f.setName(putRet.key);
			f.setOriginalName(originName);
			f.setFileExt(fileExt);
			f.setFileType("image");
			f.setUrl(returnUrl);
			f.setCate(cate);
			fileService.save(f);
			
			Map<String, Object> item = new HashMap<>();
			item.put("id", f.getId());
			item.put("url", returnUrl);
			list.add(item);
		}
		map.put("code", 0);
		map.put("list", list);
		return map;
	}
}
