package modules.file.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import common.controller.BaseController;
import modules.file.service.FileService;

/**
 * 文件管理，前端web控制层
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "web/file")
public class FileWebController extends BaseController {
	
	@Autowired
	FileService service;
	
	/**
	 * 获取到全部的文件分类列表
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "getAllFileCateList")
	@ResponseBody
	public Map<String, Object> getAllFileCateList(HttpServletRequest request) {
		return service.getAllFileCateList(request);
	}
	
	
	/**
	 * 根据一级分类，获取到全部的二级分类列表
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "getFileCateListByParent")
	@ResponseBody
	public Map<String, Object> getFileCateListByParent(HttpServletRequest request) {
		return service.getFileCateListByParent(request);
	}
	
	/**
	 * 获取用户自己创建的图片分类对象
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "workUserFileCate")
	@ResponseBody
	public Map<String, Object> workUserFileCate(HttpServletRequest request) {
		return service.workUserFileCate(request);
	}

	/**
	 * 新建或者更新一个用户文件分类对象
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveUserFileCate")
	@ResponseBody
	public Map<String, Object> saveUserFileCate(HttpServletRequest request) {
		return service.saveUserFileCate(request);
	}
	

	/**
	 * 删除一个用户文件分类
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteUserFileCate")
	@ResponseBody
	public Map<String, Object> deleteUserFileCate(HttpServletRequest request) {
		return service.deleteUserFileCate(request);
	}
	
	
	/**
	 * 根据系统分类获取到对应的图片
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "getFileListByCate")
	@ResponseBody
	public Map<String, Object> getFileListByCate(HttpServletRequest request) {
		return service.getFileListByCate(request);
	}
	
	/**
	 * 根据用户分类获取到对应的图片
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "getFileListByUserCate")
	@ResponseBody
	public Map<String, Object> getFileListByUserCate(HttpServletRequest request) {
		return service.getFileListByUserCate(request);
	}
	
	/**
	 * 删除一个图片
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteUserFile")
	@ResponseBody
	public Map<String, Object> deleteUserFile(HttpServletRequest request) {
		return service.deleteUserFile(request);
	}
	
	/**
	 * 删除多个图片
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "deleteUserFileByIds")
	@ResponseBody
	public Map<String, Object> deleteUserFileByIds(HttpServletRequest request) {
		return service.deleteUserFileByIds(request);
	}
	
	/**
	 * 移动多个图片到一个分组里面去
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveUserFileMoveByIds")
	@ResponseBody
	public Map<String, Object> saveUserFileMoveByIds(HttpServletRequest request) {
		return service.saveUserFileMoveByIds(request);
	}
	
	
	/**
	 * 裁剪一个图片，并生成一个新图片，并将新图片的地址返回
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveFileCutForPic")
	@ResponseBody
	public Map<String, Object> saveFileCutForPic(HttpServletRequest request) {
		return service.saveFileCutForPic(request);
	}
	
	
}
