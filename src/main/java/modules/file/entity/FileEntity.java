package modules.file.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
import modules.system.entity.UserEntity;

/**
 * 系统文件对象
 * @author Administrator
 *
 */
@Entity
@Table(name = "sys_file", schema = "")
public class FileEntity extends CommonEntity{
	private UserEntity user;//所属的用户
	private FileCateEntity cate;
	private FileUserCateEntity userCate;
	private String name;//系统生成的文件名称
	private String url;//系统生成的文件路径
	private String fileExt;//文件后缀
	private String fileType;//文件类型
	private String originalName;//文件原始名称
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public UserEntity getUser() {
		return user;
	}
	public void setUser(UserEntity user) {
		this.user = user;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cateId")
	public FileCateEntity getCate() {
		return cate;
	}
	public void setCate(FileCateEntity cate) {
		this.cate = cate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getFileExt() {
		return fileExt;
	}
	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getOriginalName() {
		return originalName;
	}
	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userCateId")
	public FileUserCateEntity getUserCate() {
		return userCate;
	}
	public void setUserCate(FileUserCateEntity userCate) {
		this.userCate = userCate;
	}
	
	
}
