package modules.file.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
import modules.system.entity.UserEntity;
/**
 * 文件所属用户的分类对象
 * <br>
 * 只支持一级分类
 * @author Administrator
 *
 */
@Entity
@Table(name = "sys_file_user_cate", schema = "")
public class FileUserCateEntity extends CommonEntity{
	private UserEntity user;//所属用户
	private String name;//分类名称
	private Integer orderNum;//显示排序
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public UserEntity getUser() {
		return user;
	}
	public void setUser(UserEntity user) {
		this.user = user;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
	
}
