package modules.file.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import common.entity.CommonEntity;
/**
 * 系统图库分类对象
 * 最多支持二级分类
 * @author Administrator
 *
 */
@Entity
@Table(name = "sys_file_cate", schema = "")
public class FileCateEntity extends CommonEntity{
	private String name;
	private Integer num;//文件数量
	private Integer orderNum;
	private FileCateEntity parentCate;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parentCateId")
	public FileCateEntity getParentCate() {
		return parentCate;
	}
	public void setParentCate(FileCateEntity parentCate) {
		this.parentCate = parentCate;
	}
	public Integer getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
	
}
