package common.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class CookieUtil {
	
	/**
	 * 根据key获取cookie
	 * @param request
	 * @param key
	 * @return
	 */
	public static Cookie getCookieByKey(HttpServletRequest request,String key){
		Cookie[] cookies = request.getCookies();
		for(Cookie cookieTemp : cookies){
			String cookieIdentity = cookieTemp.getName();
			if(cookieIdentity.equals(key))
				return cookieTemp;
		}
		return null;
	}
}
