package common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


/**
 * 商城平台工具类
 * 
 * @author lpf0
 *
 */
public class EShopUtil {

	private static int orderCount = 0;

	/**
	 * 获取订单号码
	 * 
	 * @return
	 */
	public synchronized static String getOrderNo() {
		if (orderCount > 1000) {
			orderCount = 1;
		}
		StringBuffer sb = new StringBuffer();
		String timeVal = DateUtil
				.getCurrentTimeByCustomPattern("yyyyMMddhhmmssSSS");
//		System.out.println(timeVal);
		sb.append(timeVal);
		for (int i = 0; i < (4 - String.valueOf(orderCount).length()); i++) {
			sb.append("0");
		}
		sb.append(orderCount);
		orderCount++;
		System.out.println("订单号码为" + sb.toString());
		return sb.toString();
	}

	/**
	 * 根据订单金额，随机返回积分给用户
	 * 100元订单  积分为：5-10
	 * 100元以下订单 积分为 0-5
	 * @param money
	 * @return
	 */
	public static Integer randomOrderPoint(Double money){
		if(money == 1D){
			return 8;
		}
		else{
			if(money > 100D){
				int d = (int) (money/100);
				Random r = new Random();
				return r.nextInt(5*d) + 5*d;
			}
			else{
				Random r = new Random();
				return r.nextInt(5) + 1;
			}
		}
	}
	/**
	 * 获取验证码
	 * 
	 * @return
	 */
	public static String yzm() {
		int[] array = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		Random rand = new Random();
		for (int i = 10; i > 1; i--) {
			int index = rand.nextInt(i);
			int tmp = array[index];
			array[index] = array[i - 1];
			array[i - 1] = tmp;
		}
		int result = 0;
		for (int i = 0; i < 6; i++)
			result = result * 10 + array[i];
		String t = String.valueOf(result);
		StringBuffer sBuffer = new StringBuffer();
		if (t.length() < 6) {
			for (int i = 0; i < (6 - t.length()); i++) {
				sBuffer.append("0");
			}
		}
		sBuffer.append(t);
		return sBuffer.toString();
	}

	/**
	 * SMS短信发送接口地址
	 */
	private final static String SMS_URL = "http://web.1xinxi.cn//asmx/smsservice.aspx";

	private final static String SMS_NAME = "18017637638";
	private final static String SMS_PWD = "C31E322FF5169DDC6F50BF12ACBF";
	private final static String SMS_SIGN = "一元天下";
	private final static String SMS_TYPE = "pt";
	/** 返回消息：系统异常 */
	private final static String SMS_RETURN_MSG_OK = "0";
	/** 返回消息：含有敏感词汇 */
	private final static String SMS_RETURN_MSG_1 = "1";
	/** 返回消息：余额不足 */
	private final static String SMS_RETURN_MSG_2 = "2";
	/** 返回消息：没有号码 */
	private final static String SMS_RETURN_MSG_3 = "3";
	/** 返回消息：包含sql语句 */
	private final static String SMS_RETURN_MSG_4 = "4";
	/** 返回消息：账号不存在 */
	private final static String SMS_RETURN_MSG_10 = "10";
	/** 返回消息：账号注销 */
	private final static String SMS_RETURN_MSG_11 = "11";
	/** 返回消息：账号停用 */
	private final static String SMS_RETURN_MSG_12 = "12";
	/** 返回消息：IP鉴权失败 */
	private final static String SMS_RETURN_MSG_13 = "13";
	/** 返回消息：格式错误 */
	private final static String SMS_RETURN_MSG_14 = "14";
	/** 返回消息：系统异常 */
	private final static String SMS_RETURN_MSG_SYS_ERROR = "-1";

	/**
	 * 发送短信验证码
	 * 
	 * @param mobile
	 * @param yzm
	 * @return
	 */
	public synchronized static String sendSms(String mobile, String yzm) {
		String msg = "您的验证码如下：" + yzm + "，请妥善保管验证码";
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", SMS_NAME);
		map.put("pwd", SMS_PWD);
		map.put("content", msg);
		map.put("mobile", mobile);
		map.put("sign", SMS_SIGN);
		map.put("type", SMS_TYPE);
		String url = SMS_URL;
		String returnJson = sendPostRequest(url, "name=" + SMS_NAME + "&pwd="
				+ SMS_PWD + "&content=" + msg + "&mobile=" + mobile + "&type="
				+ SMS_TYPE + "&sign=" + SMS_SIGN);
		System.out.println(returnJson);
		String var[] = returnJson.split(",");
		return var[0];
	}

	
	/**
	 *  post方式请求
	 * 
	 */
	public static String sendPostRequest(String httpsUrl, String param) {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(httpsUrl);
			// 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			// 发送请求参数
			if (null != param) {
				OutputStream outputStream = conn.getOutputStream();
				// 注意编码格式，防止中文乱码
				outputStream.write(param.getBytes("UTF-8"));
				outputStream.flush();
				outputStream.close();
			}
			// flush输出流的缓冲
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(
					conn.getInputStream(), "utf-8"));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送 POST 请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

}
