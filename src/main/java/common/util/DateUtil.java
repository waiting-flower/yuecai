package common.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 日期工具类
 * 
 * @author lpf0
 * 
 */
public class DateUtil {

	private static String defaultDatePattern = "yyyy-MM-dd";
	public final static String DEFAULT_DATE_FORMAT = "yyyy年MM月dd日";
	public final static String DATETIME_FORMAT_HHMMSS = "yyyy年MM月dd日 HH时mm分ss秒";
	public static final String yyMMdd = "yy-MM-dd"; // 短日期格式
	public static final String yyyyMMdd = "yyyy-MM-dd"; // 长日期格式
	public static final String HHmmss = "HH:mm:ss"; // 时间格式
	public static final String yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss"; // 长日期时间格式
	public static final String yyMMddHHmmss = "yy-MM-dd HH:mm:ss"; // 短日期时间格式
	public static final String PATTERN_SSS = "yyyy-MM-dd HH:mm:ss.SSS";

	/**
	 * 获取当前时间之前或之后几分钟 minute
	 */
	public static String getTimeByMinute(Date date, int minute) {

		Calendar calendar = getCalendar(date);

		calendar.add(Calendar.MINUTE, minute);

		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar
				.getTime());

	}

	/**
	 * 返回一个指定日期的Calendar实例
	 * 
	 * @param date
	 * @return
	 */
	public static Calendar getCalendar(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;
	}

	/**
	 * 返回当天的Calendar实例
	 * 
	 * @return
	 */
	public static Calendar getCurrentCalendar() {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;
	}

	/**
	 * 返回当前日期
	 * 
	 * @return date 当前日期
	 */
	public static Date getCurrentTime() {
		String curr = DateUtil.getTimeByCustomPattern(new Date(),
				"yyyy-MM-dd HH:mm:ss.SSS");
		return DateUtil.parse(curr, "yyyy-MM-dd HH:mm:ss.SSS");
	}

	/**
	 * 按照默认格式返回当前日期
	 * 
	 * @return date 当前日期
	 */
	public static String getCurrentTimeByDefaultPattern() {
		return new SimpleDateFormat(defaultDatePattern).format(new Date());
	}

	/**
	 * 返回指定日期的默认格式字符串输出
	 * 
	 * @param date
	 *            指定日期
	 * @return
	 */
	public static String getTimeByDefaultPattern(Date date) {
		return new SimpleDateFormat(defaultDatePattern).format(date);
	}

	/**
	 * 返回指定日期的默认格式字符串输出
	 * 
	 * @param date
	 *            指定日期
	 * @return
	 */
	public static String getTimeByCustomPattern(Date date, String pattern) {
		return new SimpleDateFormat(pattern).format(date);
	}

	/**
	 * 按照自定义格式返回当前日期
	 * 
	 * @return date 当前日期
	 */
	public static String getCurrentTimeByCustomPattern(String pattern) {
		return new SimpleDateFormat(pattern).format(new Date());
	}

	/**
	 * 判断日期是否属于自然季度的末尾月
	 * 
	 * @param date
	 *            日期
	 * @return
	 */
	public static boolean isEndOfSeason(Date date) {
		Calendar calendar = getCalendar(date);
		int month = calendar.get(Calendar.MONTH);
		if (month % 3 == 2) {
			return true;
		}
		return false;
	}

	/**
	 * 返回月
	 * 
	 * @return
	 */
	public static int getMonth(Date date) {
		Calendar calendar = getCalendar(date);
		return calendar.get(Calendar.MONTH) + 1;
	}

	/**
	 * 返回日
	 * 
	 * @return
	 */
	public static int getDay(Date date) {
		Calendar calendar = getCalendar(date);
		return calendar.get(Calendar.DATE) + 1;
	}

	/**
	 * 返回年
	 * 
	 * @param date
	 * @return
	 */
	public static int getYear(Date date) {
		Calendar calendar = getCalendar(date);
		return calendar.get(Calendar.YEAR);
	}
	
	/**
	 * 返回小时
	 * @param date
	 * @return
	 */
	public static int getHour(Date date) {
		Calendar calendar = getCalendar(date);
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * 返回某月的天数
	 * 
	 * @return
	 */
	public static int getMonthLength(Date date) {
		Calendar calendar = getCalendar(date);
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
	
	

	/**
	 * 返回该月可能的最大日期
	 * 
	 * @param date
	 * @return
	 */
	public static Date getActualMaximumDate(Date date) {
		Calendar calendar = getCalendar(date);
		int actualMaximumDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendar.set(Calendar.DAY_OF_MONTH, actualMaximumDay);
		return calendar.getTime();
	}

	public static void main(String[] arg) {

		/*Date d = new Date();
		System.out
				.println(getDateBetween(d, DateUtil.getFirstDayOfThisMonth(d)));
		Date d2 = DateUtil.getFirstDayOfThisMonth(d);
		System.out.println(getMonth(d));
		Long t = getDateBetween(d2, getActualMaximumDate(d));
		System.out.println(t);*/
		//String dateStr="2019-03-19";
		//System.out.println(getDayInterval(dateStr));
        String begin = "8:00";
        int hour = Integer.parseInt(StringUtil.substringBefore(begin,":"));
        Calendar calendar1 = getCalendar(new Date());
		Calendar calendar = Calendar.getInstance();
		calendar.set(calendar1.get(Calendar.YEAR),calendar1.get(Calendar.MONTH),calendar1.get(Calendar.DATE),hour,0,0);
		Date date =calendar.getTime();
        System.out.println(convertToString(date,yyyyMMddHHmmss));
	}

    /**
     * 根据时分，获得当天，该时分的时间
     * @param hourStr 格式：HH:MM
     * @return
     */
	public static Date getDateFromTimeStr(String hourStr){
        int h = Integer.parseInt(StringUtil.substringBefore(hourStr,":"));
        int m = Integer.parseInt(StringUtil.substringAfter(hourStr,":"));
        Calendar calendar1 = getCalendar(new Date());
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar1.get(Calendar.YEAR),calendar1.get(Calendar.MONTH),calendar1.get(Calendar.DATE),h,m,0);
        return calendar.getTime();
    }

    /**
     * 根据时分，获得当天，该时分的时间
     * @param hourStr 格式：HH:MM
     * @return
     */
    public static String getDateStrFromTimeStr(String hourStr){
        StringBuffer sb = new StringBuffer();
        sb.append(convertToString(new Date()));
        sb.append(" ");
        sb.append(hourStr);
        sb.append(":00");
        return sb.toString();
    }

	/**
	 * 获得date前month个月的月底
	 * 
	 * @param date
	 * @param month
	 * @return
	 */

	public static Date getActualMaximumDate(Date date, int month) {
		Calendar calendar = getCalendar(date);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - month);
		int actualMaximumDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendar.set(Calendar.DAY_OF_MONTH, actualMaximumDay);
		return calendar.getTime();

	}

	/**
	 * 返回该月可能的最小日期
	 * 
	 * @param date
	 * @return
	 */
	public static Date getActualMinimumDate(Date date) {
		Calendar calendar = getCalendar(date);
		int actualMininumDay = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
		calendar.set(Calendar.DAY_OF_MONTH, actualMininumDay);
		return calendar.getTime();
	}

	/**
	 * 获得date前month个月的1日
	 * 
	 * @param date
	 * @param month
	 * @return
	 */
	public static Date getActualMinimumDate(Date date, int month) {
		Calendar calendar = getCalendar(date);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - month);
		int actualMininumDay = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
		calendar.set(Calendar.DAY_OF_MONTH, actualMininumDay);
		return calendar.getTime();
	}

	/**
	 * 获得date当月的指定的某一天
	 * 
	 * @param date
	 * @param indexDay
	 * @return
	 */
	public static Date getSpecifyDate(Date date, int indexDay) {
		Calendar calendar = getCalendar(date);
		int actualMininumDay = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
		calendar.set(Calendar.DAY_OF_MONTH, actualMininumDay + indexDay - 1);
		return calendar.getTime();
	}

	//
	// /**
	// * 将object转换为date
	// *
	// * @param object
	// * @return
	// */
	// public static Date convertToDate(Object object) {
	// if (object instanceof Date) {
	// return (Date) object;
	// } else if (object instanceof String) {
	// Long temp = StringHelper.convertToLong((String) object);
	// if (temp != null) {
	// return new Date(temp.longValue());
	// } else {
	// return null;
	// }
	// } else {
	// return null;
	// }
	// }

	/**
	 * 将日期转换成YearMonth <br>
	 * pattern : yyyyMM
	 * 
	 * @param date
	 */
	public static String getYearMonth(Date date) {
		return new SimpleDateFormat("yyyyMM").format(date);
	}

	/**
	 * 获取上个月的日期对象(上个月1号)
	 * 
	 * @param date
	 */
	public static Date getPriorMonthDate(Date date) {
		Calendar calendar = getCalendar(date);
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		Calendar newCalendar = Calendar.getInstance();
		if (month == 0) {
			year--;
			month = 11;
		} else {
			month--;
		}
		newCalendar.set(year, month, 1);
		return newCalendar.getTime();
	}

	/**
	 * 获取传过来的月份的上一个季度末的日期
	 * @param
	 * @param date
	 * @return Date
	 */
	public static Date getpriorMonthDateByMonth(Date date) {
		Calendar calendar = getCalendar(date);
		int month = calendar.get(Calendar.MONTH) + 1;
		int year = calendar.get(Calendar.YEAR);
		Calendar newCalendar = Calendar.getInstance();
		if (month == 4 || month == 5 || month == 6) {
			newCalendar.set(year, 3, 0);
			return newCalendar.getTime();
		} else if (month == 7 || month == 8 || month == 9) {
			newCalendar.set(year, 6, 0);
			return newCalendar.getTime();
		} else if (month == 10 || month == 11 || month == 12) {
			newCalendar.set(year, 9, 0);
			return newCalendar.getTime();
		} else {
			year--;
			newCalendar.set(year, 12, 0);
			return newCalendar.getTime();
		}
	}

	public static Date getNextSeason(Date date) {
		Calendar calendar = getCalendar(date);
		Calendar newCalendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);

		int newMonth = month + 4;

		newCalendar.set(year, newMonth, 0);
		Date nextSeasonFirstDay = newCalendar.getTime();
		return getActualMaximumDate(nextSeasonFirstDay);
	}

	/**
	 * jun.cao 绩效津贴中结算月的算法
	 * 
	 * @param date
	 * @return date
	 */
	public static Date getPerforMancebonusMonthDateByMonth(Date date) {
		Calendar calendar = getCalendar(date);
		int month = calendar.get(Calendar.MONTH) + 1;
		int year = calendar.get(Calendar.YEAR);
		Calendar newCalendar = Calendar.getInstance();
		if (month == 3 || month == 4 || month == 5) {
			newCalendar.set(year, 3, 0);
			return newCalendar.getTime();
		} else if (month == 6 || month == 7 || month == 8) {
			newCalendar.set(year, 6, 0);
			return newCalendar.getTime();
		} else if (month == 9 || month == 10 || month == 11) {
			newCalendar.set(year, 9, 0);
			return newCalendar.getTime();
		} else {
			year--;
			newCalendar.set(year, 12, 0);
			return newCalendar.getTime();
		}

	}

	/**
	 * 去掉日期的时、分、秒，如果没有指定日期，则返回当前日期
	 * 
	 * @param date
	 * @return
	 * @author ya.zhao
	 */
	public static Date getSimpleDate(Date date) {
		if (date == null) {
			date = new Date();
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}

	public static Date getLastMonth(Date date) {
		Calendar calendar = getCalendar(date);
		int month = calendar.get(Calendar.MONTH);
		calendar.set(Calendar.MONTH, month - 1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		return calendar.getTime();
	}

	public static Date getNextMonth(Date date) {
		Calendar calendar = getCalendar(date);
		int month = calendar.get(Calendar.MONTH);
		calendar.set(Calendar.MONTH, month + 1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		return getActualMaximumDate(calendar.getTime());
	}

	/**
	 * 获取日期指定月份后的一天
	 * @param date
	 * @param num
	 * @return
	 */
	public static Date getAfterMonth(Date date,int num) {
		Calendar calendar = getCalendar(date);
		calendar.add(Calendar.MONTH, num);
		return calendar.getTime();
	}

	public static Date parse(String str) throws ParseException {
		return parse(str, defaultDatePattern);
	}

	public static Date parse(String str, String pattern) {
		try {
			return new SimpleDateFormat(pattern).parse(str);
		} catch (ParseException e) {
			throw new RuntimeException("日期格式转换错误", e);
		}
	}

	/**
	 * 获得前months月的最后一天
	 * 
	 * @param date
	 * @param months
	 * @return
	 */
	public static Date parse(Date date, int months) {
		String str = getTimeByCustomPattern(date, "yyyy-MM");
		Date d = parse(str, "yyyy-MM");
		Calendar calendar = getCalendar(d);
		int month = calendar.get(Calendar.MONTH);
		calendar.set(Calendar.MONTH, (month - months));
		return calendar.getTime();
	}

	/**
	 * 获得前months月的最后一天
	 * 
	 * @param date
	 * @param months
	 * @return
	 */
	public static Date getLastDay(Date date, int months) {
		Calendar calendar = getCalendar(date);
		int month = calendar.get(Calendar.MONTH);
		calendar.set(Calendar.MONTH, (month - months));
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		return getActualMaximumDate(calendar.getTime());
	}

	/**
	 * 获得前months月的最后一天
	 * 
	 * @param date
	 * @param months
	 * @return
	 */
	public static Date getFirstDay(Date date, int months) {
		Calendar calendar = getCalendar(date);
		int month = calendar.get(Calendar.MONTH);
		calendar.set(Calendar.MONTH, (month - months));
		calendar.set(Calendar.DAY_OF_MONTH, 1);

		return getActualMinimumDate(calendar.getTime());

	}

	/**
	 * 两个月间隔的月份
	 * 
	 * @param beginDate
	 *            开始日期
	 * @param endDate
	 *            结束日期
	 * @return
	 */
	public static int getInterval(Date beginDate, Date endDate) {
		int b = getMonth(beginDate);
		int e = getMonth(endDate);
		int by = getYear(beginDate);
		int ey = getYear(endDate);
		return e - b + 12 * (ey - by);
	}

	public static long getDayInterval(Date beginDate, Date endDate) {
		String beginStr = getTimeByCustomPattern(beginDate, yyyyMMddHHmmss);
		String endStr = getTimeByCustomPattern(endDate, yyyyMMddHHmmss);
		long begin = parse(beginStr, yyyyMMddHHmmss).getTime();
		long end = parse(endStr, yyyyMMddHHmmss).getTime();
		long days = (end - begin) / (long) (1000 * 3600 * 24);
		if (days < 0) {
			days = 0;
		}
		return days;
	}

	/**
	 * 获得两个日期字符串间隔的天数
	 * @param beginStr 格式yyyy-MM-dd HH:mm:ss
	 * @param endStr 格式yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static long getDayInterval(String beginStr, String endStr) {
		long begin = parse(beginStr, yyyyMMddHHmmss).getTime();
		long end = parse(endStr, yyyyMMddHHmmss).getTime();
		long days = (end - begin) / (long) (1000 * 3600 * 24);
		if (days < 0) {
			days = 0;
		}
		return days;
	}

	/**
	 * 获得dateStr距离现在的天数
	 * @param dateStr 格式yyyy-MM-dd
	 * @return
	 */
	public static long getDayInterval(String dateStr) {
		long begin = parse(dateStr, yyyyMMdd).getTime();
		long end = System.currentTimeMillis();
		long days = (end - begin) / (long) (1000 * 3600 * 24);
		if(days<0){
			days=0;
		}
		return days;
	}

	/**
	 * 返回前一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getPreviousDay(Date date) {
		Calendar calendar = getCalendar(date);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		calendar.set(Calendar.DATE, day - 1);
		return calendar.getTime();
	}

	public static boolean isEndQuarter(Date date) {
		boolean retval = false;
		Calendar calendar = getCalendar(date);
		int month = calendar.get(Calendar.MONTH);
		if ((month + 1) % 3 == 0) {
			retval = true;
		}
		return retval;
	}

	public static boolean isMidYear(Date date) {
		boolean retval = false;
		Calendar calendar = getCalendar(date);
		int month = calendar.get(Calendar.MONTH);
		if ((month + 1) % 6 == 0) {
			retval = true;
		}
		return retval;
	}

	public static boolean isEndYear(Date date) {
		boolean retval = false;
		Calendar calendar = getCalendar(date);
		int month = calendar.get(Calendar.MONTH);
		if ((month + 1) % 12 == 0) {
			retval = true;
		}
		return retval;
	}

	/**
	 * 日期型转换为字符串
	 * 
	 * @param date
	 * @return
	 */
	public static String convertToString(Date date) {
		return new SimpleDateFormat("yyyy-MM-dd").format(date);
	}

	public static String convertToString(Date date,String fromat) {
		return new SimpleDateFormat(fromat).format(date);
	}

	/**
	 * java.util.date 转换为 java.sql.date
	 * 
	 * @param date
	 * @return
	 */
	public static java.sql.Date convertToSqlDate(Date date) {
		return new java.sql.Date(date.getTime());
	}

	/**
	 * 
	 * @return
	 */
	public static String getCurrentTimeByFullPattern() {
		return new SimpleDateFormat(getDateTimePattern()).format(new Date());
	}

	public static String getDateTimePattern() {
		return defaultDatePattern + " HH:mm:ss.SSS";
	}

	/**
	 * Long形字符串转化日期
	 * 
	 * @param exeDate
	 * @return
	 */
	public static String convertToDateString(Object exeDate) {
		String exeDateStr = "";
		if (exeDate != null && exeDate instanceof String) {
			long dataValue = Long.parseLong((String) exeDate);
			Date date = new Date(dataValue);
			exeDateStr = getTimeByCustomPattern(date, "yyyy-MM-dd");
		}
		return exeDateStr;
	}

	// /**
	// * 获得当前月到前yearSize年的月份
	// *
	// * @param yearSize
	// * 年数
	// * @return 月份的集合
	// */
	// public static List getAllMonthsEndToday(int yearSize) {
	// String pattern = "yyyy-MM";
	// List monthsList = new ArrayList();
	// int i = 1;
	// Date priorDate = new Date();
	//
	// while (i <= yearSize * 12) {
	// CommisionMonth cm = new CommisionMonth();
	// cm.setCommisionMonth(DateHelper.getTimeByCustomPattern(priorDate,
	// pattern));
	// monthsList.add(cm);
	// priorDate = DateHelper.getPriorMonthDate(priorDate);
	// i++;
	// }
	// return monthsList;
	// }

	/**
	 * 获得指定日期的前days天日期
	 * 
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date getPriorDay(Date date, int days) {
		long curTime = date.getTime();
		long priorTime = curTime - 1000 * 60 * 60 * 24 * (long) days;
		return new Date(priorTime);
	}

	/**
	 * 获得指定日期的后days天日期
	 * 
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date getAfterDay(Date date, int days) {
		long curTime = date.getTime();
		long afterTime = curTime + 1000 * 60 * 60 * 24 * (long) days;
		return new Date(afterTime);
	}

	/**
	 * 获得指定日期的前hours小时的日期
	 * 
	 * @param date
	 * @param hours
	 * @return
	 */
	public static Date getPriorHour(Date date, int hours) {
		long curTime = date.getTime();
		long priorTime = curTime - 1000 * 60 * 60 * (long) hours * 1;
		return new Date(priorTime);
	}

	/**
	 * 返回指定日期的下一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getNextDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_YEAR,
				calendar.get(Calendar.DAY_OF_YEAR) + 1);
		return calendar.getTime();
	}

	/**
	 * 返回指定日期的前一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getLastDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_YEAR,
				calendar.get(Calendar.DAY_OF_YEAR) - 1);
		return calendar.getTime();
	}

	/**
	 * 获得当月的第一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getFirstDayOfThisMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		return calendar.getTime();
	}

	/**
	 * 获得上月的第一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getFirstDayOfLastMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		return calendar.getTime();
	}

	/**
	 * 返回当月的最后一天
	 * 
	 * @param strDate
	 *            格式（'YYYY-MM'）
	 * @return
	 */
	public static Date getLastDayOfThisMonth(String strDate) throws Exception {

		try {
			Date date = new SimpleDateFormat("yyyy-MM").parse(strDate.trim());
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.set(Calendar.MONTH, (calendar.get(Calendar.MONTH) + 1));

			calendar.set(Calendar.DAY_OF_YEAR,
					(calendar.get(Calendar.DAY_OF_YEAR) - 1));
			return calendar.getTime();
		} catch (ParseException e) {
			throw new Exception("日期格式错误！");
		}

	}

	/**
	 * 判断日期是否为下半月（16日以后）
	 * 
	 * @param date
	 * @return
	 */
	public static boolean isSecondHalfMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		if (day > 15) {
			return true;
		} else {
			return false;
		}
	}

	public static Date getYearFirstDate(Date date) {
		Calendar calendar = getCalendar(date);
		int minDay = calendar.getActualMinimum(Calendar.DAY_OF_YEAR);
		calendar.set(Calendar.DAY_OF_YEAR, minDay);
		return calendar.getTime();
	}

	public static Date getYearLastDate(Date date) {
		Calendar calendar = getCalendar(date);
		int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_YEAR);
		calendar.set(Calendar.DAY_OF_YEAR, maxDay);
		return calendar.getTime();
	}

	/**
	 * 去掉日期的时、分、秒，毫秒如果没有指定日期，则返回当前日期
	 * 
	 * @param date
	 * @return
	 * @author ya.zhao
	 */
	public static Date getSimpleDateNoNanos(Date date) {
		if (date == null) {
			date = new Date();
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Timestamp time = new Timestamp(calendar.getTime().getTime());
		time.setNanos(0);
		return new Date(time.getTime());
	}

	/**
	 * 获取季度第一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getSeasonFirstDay(Date date) {
		Date result = null;
		if (date == null)
			return result;
		Calendar calendar = Calendar.getInstance();
		int month = getMonth(date);
		int year = getYear(date);
		if (month >= 1 && month <= 3) {
			calendar.set(year, 0, 1);
		} else if (month >= 4 && month <= 6) {
			calendar.set(year, 3, 1);
		} else if (month >= 7 && month <= 9) {
			calendar.set(year, 6, 1);
		} else {
			calendar.set(year, 9, 1);
		}
		return calendar.getTime();
	}

	/**
	 * 获取季度最后一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getSeasonLastDay(Date date) {
		return getActualMaximumDate(getSeasonFirstDay(date), -2);
	}

	/**
	 * 获取半年度第一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getHalfYearFirstDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		int month = getMonth(date);
		int year = getYear(date);
		if (month >= 1 && month <= 6) {
			calendar.set(year, 0, 1);
		} else {
			calendar.set(year, 6, 1);
		}
		return calendar.getTime();
	}

	/**
	 * 获取半年度最后一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getHalfYearLastDay(Date date) {
		return getActualMaximumDate(getHalfYearFirstDay(date), -5);
	}

	/**
	 * 获得两个日期的天数差值
	 * 
	 * @author gjx
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static Long getDateBetween(Date date1, Date date2) {
		Long t = date1.getTime() - date2.getTime();
		return Math.abs(t / 1000 / 3600 / 24);
	}

	/**
	 * 将日期转换为字符串 如2012-03-08 16:37:05 现在时间2012-03-08 16:47:05
	 * 
	 * @param date
	 * @return 10分前
	 */
	public static String getOneMonthDateFormat(Date date) {
		String str = DateUtil.getTimeByDefaultPattern(date);
		Date nowDate = new Date();
		int s = (int) ((nowDate.getTime() - date.getTime()) / 1000);
		int d = s / 86400;
		if (s < 0 || d >= 31) {
			return str;
		}
		str = s < 60 ? "刚刚" : (s / 60) < 60 ? (s / 60) + "分钟前"
				: (s / 3600) < 24 ? (s / 3600) + "小时前" : d == 1 ? "昨天"
						: d < 7 ? d + "天前" : (d / 7) < 5 ? (d / 7) + "周前" : str;
		return str;
	}

	 /**
     * 获取当前日期是星期几<br>
     * 
     * @param dt
     * @return 当前日期是星期几
     */
    public static String getWeekOfDate(Date dt) {
        String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);

        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;

        return weekDays[w];
    }
	/*
	 * 取本周7天的第一天（周一的日期）
	 */
	public static String getNowWeekBegin() {
		int mondayPlus;
		Calendar cd = Calendar.getInstance();
		// 获得今天是一周的第几天，星期日是第一天，星期二是第二天......
		int dayOfWeek = cd.get(Calendar.DAY_OF_WEEK) - 1; // 因为按中国礼拜一作为第一天所以这里减1
		if (dayOfWeek == 1) {
			mondayPlus = 0;
		} else {
			mondayPlus = 1 - dayOfWeek;
		}
		GregorianCalendar currentDate = new GregorianCalendar();
		currentDate.add(GregorianCalendar.DATE, mondayPlus);
		Date monday = currentDate.getTime();

		DateFormat df = DateFormat.getDateInstance();
		String preMonday = df.format(monday);

		return preMonday + " 00:00:00";

	}
}
