package common.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;

/**
 * 文件操作工具类
 * 
 * @author 李鹏飞
 *
 */
public class FileUtil {
	private static final Logger logger = Logger.getLogger(FileUtil.class);

	/**
	 * 最大允许的上传文件大小
	 */
	static long maxSize = 1024 * 1024 * 200;

	/**
	 * 支持的文件扩展名集合
	 */
	static Map<String, String> extMap = new HashMap<String, String>();
	static {
		extMap.put("image", "gif,jpg,jpeg,png,bmp");
		extMap.put("flash", "swf,flv");
		extMap.put("media", "swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb");
		extMap.put("dayinDoc", "doc,docx,pdf");
		extMap.put("file","gif,jpg,jpeg,png,bmp,doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar,gz,bz2,pdf,mp4,flv");
	}
	/**
	 * 按行读取文件内容，返回内容list数组
	 * 
	 * @param fileName
	 * @return
	 */
	public static List<String> readFileContentByLine(String fileName) {
		return readFileByLineAndEncoding(fileName, "UTF-8");
	}

	public static List<String> readFileByLineAndEncoding(String fileName,
			String encoding) {
		List<String> list = null;
		BufferedReader bf = null;
		InputStreamReader isr = null;
		FileInputStream fis = null;
		try {
			list = new ArrayList<String>();
			File file = new File(fileName);
			fis = new FileInputStream(file);
			if (encoding == null || "".equals(encoding))
				isr = new InputStreamReader(fis);
			else
				isr = new InputStreamReader(fis, encoding);
			bf = new BufferedReader(isr);
			String tempVal = null;
			while ((tempVal = bf.readLine()) != null) {
				list.add(tempVal);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				bf.close();
				isr.close();
				fis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	/**
	 * 以增量的方式将内容按行写入文件
	 * 
	 * @param fileName
	 * @param content
	 */
	public synchronized static void addContentToFile(String fileName, String content) {
		try {
			createNewFile(fileName);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			BufferedWriter bw = null;
			FileOutputStream fos = new FileOutputStream(fileName, true);
			bw = new BufferedWriter(new OutputStreamWriter(fos, "UTF-8"));
			bw.write(content);
			bw.newLine();
			bw.flush();
			bw.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	/**
	 * 创建一个新的文件
	 * 
	 * @param fileName
	 * @throws IOException
	 */
	public static void createNewFile(String fileName) throws IOException {
		File myFilePath = new File(fileName);
		if (!myFilePath.exists()) {
			myFilePath.createNewFile();
		}
	}
	/**
	 * 检查文件目录是否存在，不存在的话创建
	 * @param path
	 * @return
	 */
	public static boolean checkDir(String path) {
		try {
			// 检查目录
			File uploadDir = new File(path);
			if (!uploadDir.isDirectory()) {
				uploadDir.mkdirs();
				// out.println(getError("上传目录不存在。"));
				// return;
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * 生成一个文件名称，由日期与随机数组成
	 * 
	 * @param extend
	 * @return
	 */
	public static String generateFileName(String extend) {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		String newFileName = df.format(new Date()) + "_"
				+ new Random().nextInt(1000) + "." + extend;
		return newFileName;
	}

	/**
	 * 检查文件大小
	 * 
	 * @param fileSize
	 * @return
	 */
	public static boolean checkFileSize(long fileSize) {
		if (fileSize > maxSize) {
			logger.info("上传文件大小超过限制。");
			return false;
		}
		return true;
	}

	/**
	 * 检查上传文件是否为系统支持的上传文件
	 * 
	 * @param dir
	 *            上传什么类型的文件 image:图片 flash：flash文件
	 * @param fileName
	 * @return
	 */
	public static boolean checkExtendIsEffectByExt(String dirName, String fileExt) {
		if (StringUtil.isEmpty(dirName)) {
			dirName = "image";
		}
		if (!Arrays.<String> asList(extMap.get(dirName).split(",")).contains(
				fileExt.toLowerCase())) {
			logger.info("上传文件扩展名是不允许的扩展名。\n只允许" + extMap.get(dirName) + "格式。");
			return false;
		}
		return true;
	}
	/**
	 * 检查上传文件是否为系统支持的上传文件
	 * 
	 * @param dir
	 *            上传什么类型的文件 image:图片 flash：flash文件
	 * @param fileName
	 * @return
	 */
	public static boolean checkExtendIsEffect(String dirName, String fileName) {
		if (StringUtil.isEmpty(dirName)) {
			dirName = "image";
		}
		String fileExt = getExtend(fileName).toLowerCase();
		if (!Arrays.<String> asList(extMap.get(dirName).split(",")).contains(
				fileExt)) {
			logger.info("上传文件扩展名是不允许的扩展名。\n只允许" + extMap.get(dirName) + "格式。");
			return false;
		}
		return true;
	}

	/**
	 * 获取文件扩展名
	 * 
	 * @param filename
	 * @return
	 */
	public static String getExtend(String filename) {
		return getExtend(filename, "");
	}

	/**
	 * 获取文件扩展名
	 * 
	 * @param filename
	 * @return
	 */
	public static String getExtend(String filename, String defExt) {
		if ((filename != null) && (filename.length() > 0)) {
			int i = filename.lastIndexOf('.');

			if ((i > 0) && (i < (filename.length() - 1))) {
				return (filename.substring(i + 1)).toLowerCase();
			}
		}
		return defExt.toLowerCase();
	}

	/**
	 * 获取文件名称[不含后缀名]
	 * 
	 * @param
	 * @return String
	 */
	public static String getFilePrefix(String fileName) {
		int splitIndex = fileName.lastIndexOf(".");
		return fileName.substring(0, splitIndex).replaceAll("\\s*", "");
	}

	/**
	 * 获取文件名称[不含后缀名] 不去掉文件目录的空格
	 * 
	 * @param
	 * @return String
	 */
	public static String getFilePrefix2(String fileName) {
		int splitIndex = fileName.lastIndexOf(".");
		return fileName.substring(0, splitIndex);
	}

	/**
	 * 文件复制 方法摘要：这里一句话描述方法的用途
	 *
	 * @param
	 * @return void
	 */
	public static void copyFile(String inputFile, String outputFile)
			throws FileNotFoundException {
		File sFile = new File(inputFile);
		File tFile = new File(outputFile);
		FileInputStream fis = new FileInputStream(sFile);
		FileOutputStream fos = new FileOutputStream(tFile);
		int temp = 0;
		byte[] buf = new byte[10240];
		try {
			while ((temp = fis.read(buf)) != -1) {
				fos.write(buf, 0, temp);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fis.close();
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 判断文件是否为图片<br>
	 * <br>
	 * 
	 * @param filename
	 *            文件名<br>
	 *            判断具体文件类型<br>
	 * @return 检查后的结果<br>
	 * @throws Exception
	 */
	public static boolean isPicture(String filename) {
		// 文件名称为空的场合
		if (StringUtil.isEmpty(filename)) {
			// 返回不和合法
			return false;
		}
		// 获得文件后缀名
		// String tmpName = getExtend(filename);
		String tmpName = filename;
		// 声明图片后缀名数组
		String imgeArray[][] = { { "bmp", "0" }, { "dib", "1" },
				{ "gif", "2" }, { "jfif", "3" }, { "jpe", "4" },
				{ "jpeg", "5" }, { "jpg", "6" }, { "png", "7" },
				{ "tif", "8" }, { "tiff", "9" }, { "ico", "10" } };
		// 遍历名称数组
		for (int i = 0; i < imgeArray.length; i++) {
			// 判断单个类型文件的场合
			if (imgeArray[i][0].equals(tmpName.toLowerCase())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断文件是否为DWG<br>
	 * <br>
	 * 
	 * @param filename
	 *            文件名<br>
	 *            判断具体文件类型<br>
	 * @return 检查后的结果<br>
	 * @throws Exception
	 */
	public static boolean isDwg(String filename) {
		// 文件名称为空的场合
		if (StringUtil.isEmpty(filename)) {
			// 返回不和合法
			return false;
		}
		// 获得文件后缀名
		String tmpName = getExtend(filename);
		// 声明图片后缀名数组
		if (tmpName.equals("dwg")) {
			return true;
		}
		return false;
	}

	/**
	 * 删除指定的文件
	 * 
	 * @param strFileName
	 *            指定绝对路径的文件名
	 * @return 如果删除成功true否则false
	 */
	public static boolean delete(String strFileName) {
		File fileDelete = new File(strFileName);

		if (!fileDelete.exists() || !fileDelete.isFile()) {
			LogUtil.info("错误: " + strFileName + "不存在!");
			return false;
		}

		LogUtil.info("--------成功删除文件---------" + strFileName);
		return fileDelete.delete();
	}

	public static String deleteFileName = ".svn";
	public static String deleteDirectory = "D:/wokspace/yytx";

	public static void deleteFile(File file) {
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			for (File f : files) {
				deleteFile(f);
			}
		}
		System.out.println("delete file: " + file.getPath() + "/"
				+ file.getName());
		file.delete();
	}

	public static void deleteDestFile(File file) {
		if (file.isDirectory()) {
			if (file.getName().equals(deleteFileName)) {
				deleteFile(file);
			} else {
				File[] files = file.listFiles();
				for (File f : files) {
					deleteDestFile(f);
				}
			}
		}
	}

	public static void main(String[] args) {
		File file = new File(deleteDirectory);
		deleteDestFile(file);
	}
}
