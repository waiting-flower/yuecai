package common.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/**
 * 二维码生成和读的工具类
 *
 */
public class QrCodeUtil {
    
    /**
     * 生成包含字符串信息的二维码图片
     * @param outputStream 文件输出流路径
     * @param content 二维码携带信息
     * @param qrCodeSize 二维码图片大小
     * @param imageFormat 二维码的格式
     * @throws WriterException 
     * @throws IOException 
     */
    public static boolean createQrCode(OutputStream outputStream, String content, int qrCodeSize, String imageFormat) throws WriterException, IOException{  
            //设置二维码纠错级别ＭＡＰ
            Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<EncodeHintType, ErrorCorrectionLevel>();  
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);  // 矫错级别  
            QRCodeWriter qrCodeWriter = new QRCodeWriter();  
            //创建比特矩阵(位矩阵)的QR码编码的字符串  
            BitMatrix byteMatrix = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE, qrCodeSize, qrCodeSize, hintMap);  
            // 使BufferedImage勾画QRCode  (matrixWidth 是行二维码像素点)
            int matrixWidth = byteMatrix.getWidth();  
            BufferedImage image = new BufferedImage(matrixWidth-200, matrixWidth-200, BufferedImage.TYPE_INT_RGB);  
            image.createGraphics();  
            Graphics2D graphics = (Graphics2D) image.getGraphics();  
            graphics.setColor(Color.WHITE);  
            graphics.fillRect(0, 0, matrixWidth, matrixWidth);  
            // 使用比特矩阵画并保存图像
            graphics.setColor(Color.BLACK);  
            for (int i = 0; i < matrixWidth; i++){
                for (int j = 0; j < matrixWidth; j++){
                    if (byteMatrix.get(i, j)){
                        graphics.fillRect(i-100, j-100, 1, 1);  
                    }
                }
            }
            return ImageIO.write(image, imageFormat, outputStream);  
    }  
      
    
    public static BufferedImage createQRCode(final String url,int width,int height) throws WriterException, IOException {  
        BufferedImage image = null;  
        //二维码图片输出流  
        OutputStream out = null;  
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();  
        Map hints = new HashMap();  
        // 设置编码方式  
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");  
        // 设置QR二维码的纠错级别（H为最高级别）具体级别信息  
        /*hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);*/  
        BitMatrix bitMatrix = multiFormatWriter.encode(url, BarcodeFormat.QR_CODE, width, height,hints);  
        bitMatrix = updateBit(bitMatrix,10);  
        image = MatrixToImageWriter.toBufferedImage(bitMatrix);  
        return image;  
    }  
  
    /**  
     * 自定义白边边框宽度  
     *  
     * @param matrix  
     * @param margin  
     * @return  
     */  
    private static BitMatrix updateBit(final BitMatrix matrix, final int margin) {  
        int tempM = margin * 2;  
        //获取二维码图案的属性  
        int[] rec = matrix.getEnclosingRectangle();  
        int resWidth = rec[2] + tempM;  
        int resHeight = rec[3] + tempM;  
        // 按照自定义边框生成新的BitMatrix  
        BitMatrix resMatrix = new BitMatrix(resWidth, resHeight);  
        resMatrix.clear();  
        //循环，将二维码图案绘制到新的bitMatrix中  
        for (int i = margin; i < resWidth - margin; i++) {  
            for (int j = margin; j < resHeight - margin; j++) {  
                if (matrix.get(i - margin + rec[0], j - margin + rec[1])) {  
                    resMatrix.set(i, j);  
                }  
            }  
        }  
        return resMatrix;  
    }  
  
}
