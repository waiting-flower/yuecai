package common.util;

public class Constant {
	/**
	 * 后台用户的session key值
	 */
	public final static String ADMIN_USER_KEY = "user";
	
	/**
	 * 登录已过期，或者您尚未登录
	 */
	public final static String NO_LOGIN_CODE = "10";
	
	/**
	 * 登录已过期，或者您尚未登录
	 */
	public final static String NO_LOGIN_MSG = "登录已过期，或者您尚未登录";
	
	
	/**
	 * 图片类型的文件  ，jpg，png，gif等
	 */
	public final static String FILE_TYPE_IMAGE = "image";
	
	/**
	 * word的文件，例如，doc，ppt等
	 */
	public final static String FILE_TYPE_WORD = "word";
	
	/**
	 * 压缩文件
	 */ 
	public final static String FILE_TYPE_ZIP = "zip";
	
	/**
	 * 系统登录用户身份型：正常的授权用户 1
	 */
	public final static String USER_IDENTITY_NORMAL = "1";
	
	/**
	 * 合作商的用户：2
	 */
	public final static String USER_IDENTITY_SPECIAL_2 = "2";
	
	public static boolean is_spider_goods = false;
	
	public static boolean is_create_task = false;
}
