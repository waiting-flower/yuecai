package common.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;


/**
 * 验证码类，主要生成几种不同类型的验证码 <br>
 * <strong> 第一种：简单验证码，4位随机数字 <br>
 * 第二种：英文字符加数字的验证码 <br>
 * 第三种：像铁路订票系统一样的验证码，肆+？=21 </strong>
 *
 * @author 李鹏飞
 *
 */
public class VerificationCodeUtil {
    private ByteArrayInputStream image;// 图像
    private String str;// 验证码
    private static final int WIDTH = 95;
    private static final int HEIGHT = 35;

    private int x;
    private int y;

    public static void main(String[] arg) {
        VerificationCodeUtil vcu = VerificationCodeUtil.Instance();
        //System.err.println(vcu.getVerificationCodeValue());
    }

    /**
     * 功能：获取一个验证码类的实例
     *
     * @return
     */
    public static VerificationCodeUtil Instance() {
        return new VerificationCodeUtil();
    }

    public VerificationCodeUtil(char vcChar, String code) {
        initVeriCode(vcChar, code);
    }
    public VerificationCodeUtil(String needC, String fullC) {
        initVeriCodeForWap(needC, fullC);
    }

    private Color getColor() {
        int R = (int) (Math.random() * 255);
        int G = (int) (Math.random() * 255);
        int B = (int) (Math.random() * 255);
        return new Color(R, G, B);
    }

    /**
     * 随机产生干扰点
     *
     * @param width
     * @param height
     * @param many
     * @param g
     */
    private void createRandomPoint(int width, int height, int many, Graphics g) {
        Random rand = new Random();
        for (int i = 0; i < many; i++) {
            int x = rand.nextInt(width);
            int y = rand.nextInt(height);
            g.setColor(getColor());
            g.drawOval(x, y, 1, 1);
        }
    }

    /**
     * 随机产生干扰线条
     *
     * @param width
     * @param height
     * @param minMany
     * @param g
     */
    private void createRandomLine(int width, int height, int minMany, Graphics g) {
        Random rand = new Random();
        for (int i = 0; i < rand.nextInt(minMany) + 5; i++) {
            int x1 = rand.nextInt(width) % 15;
            int y1 = rand.nextInt(height);
            int x2 = (int) (rand.nextInt(width) % 40 + width * 0.7);
            int y2 = rand.nextInt(height);
            g.setColor(getColor());
            g.drawLine(x1, y1, x2, y2);
        }
    }

    /**
     * 生成wap版本验证码需要的图片
     * @param needC  1个汉字
     * @param fullC  8个汉字
     */
    private void initVeriCodeForWap(String needC,String fullC){
        int width = 260;// 260/4= 65
        int height = 138;// 69
        BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        Graphics2D g = (Graphics2D) image.getGraphics();
        Random random = new Random();
        String picPath = VerificationCodeUtil.class.getClassLoader()
                .getResource("images/" + (random.nextInt(7) + 1) + ".jpg")
                .getPath(); // 读取本地图片，做背景图片

//		try {
//			g.drawImage(ImageIO.read(new File(picPath)), 0, 0, width, height,
//					null);
//		} catch (IOException e) {
//			e.printStackTrace();
//		} // 将背景图片从高度0开始

        g.setColor(Color.white); // 设置颜色
        g.drawRect(0, 0, width - 1, height - 1); // 画边框
        g.setFont(new Font("宋体", Font.BOLD, 20)); // 设置字体

        for(int i = 0; i < fullC.length(); i++){
            String nowC = String.valueOf(fullC.charAt(i));
            g.setColor(new Color(random.nextInt(50) + 200,
                    random.nextInt(150) + 100, random.nextInt(50) + 200));
            int mod = i % 2;
            int x = 0;//每一个字的 X 、Y坐标
            int y = 0;
            if(mod == 0){//偶数，绘制在第一行  基数绘制在第二行
                x = (i / 2) * 65 + random.nextInt(30);
                y = 20 + random.nextInt(40);
            }
            else{
                x = (i - 1) / 2 * 65 + random.nextInt(30);
                y = 70 + random.nextInt(40);
            }
            g.drawString(nowC, x, y);
            if (nowC.equals(needC)) {
                this.setX(x);
                this.setY(y);
            }
        }

        createRandomPoint(300, 100, 200, g);
        createRandomLine(300, 100, 5, g);
        g.dispose();
        this.setImage(drawImage(image));
    }


    private void initVeriCode(char myChar, String code) {
        int height = 132; // 图片高
        int width = 338; // 图片宽
        BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        Graphics2D g = (Graphics2D) image.getGraphics();
        Random random = new Random();
        String picPath = VerificationCodeUtil.class.getClassLoader()
                .getResource("images/" + (random.nextInt(7) + 1) + ".jpg")
                .getPath(); // 读取本地图片，做背景图片

        try {
            g.drawImage(ImageIO.read(new File(picPath)), 0, 0, width, height,
                    null);
        } catch (IOException e) {
            e.printStackTrace();
        } // 将背景图片从高度0开始

        g.setColor(Color.white); // 设置颜色
        g.drawRect(0, 0, width - 1, height - 1); // 画边框
        g.setFont(new Font("宋体", Font.BOLD, 24)); // 设置字体
        String charArr[] = code.split(",");// 3个成语集合
//		System.out.println("字符串长度为" + charArr.length);
        int preY = 0;
        for (int i = 0; i < charArr.length; i++) {
//			System.out.println("处理第" + i + "个字符串" + charArr[i]);
            g.setColor(new Color(random.nextInt(50) + 200,
                    random.nextInt(150) + 100, random.nextInt(50) + 200));
            // g.rotate(30 * Math.PI / 180); // 旋转30度
            // x坐标值
            int a = random.nextInt(248);
            // y坐标值
            int b = random.nextInt(45) + 44 * i + 15;
            if ((b - preY) < 20) {
                b = b + 20;
            }
            preY = b;
            if (b > 120) {
                b = 117;
            }
            char[] charVal = charArr[i].toCharArray();
            int tempInex = 0;
            for (char c : charVal) {
                int tempX = tempInex * 22 + a;
//				System.out
//						.println("开始画：" + c + "\n坐标X=" + tempX + "——坐标Y=" + b);
                g.drawString(String.valueOf(c), tempX, b);
                tempInex++;
//				System.err.println("c==" + c);
//				System.err.println("myChar==" + myChar);
                if (c == myChar) {
//					System.err.println("字符匹配成功" + c + "——0" + myChar);
                    this.setX(tempX);
                    this.setY(b);
                }
            }
        }
        createRandomPoint(300, 100, 200, g);
        createRandomLine(300, 100, 5, g);
        g.dispose();
        this.setImage(drawImage(image));
    }

    private VerificationCodeUtil() {
        BufferedImage image = new BufferedImage(WIDTH, HEIGHT,
                BufferedImage.TYPE_INT_RGB);
        int randomNum = new Random().nextInt(3);
        if (randomNum == 0) {
            initNumVerificationCode(image);
        } else if (randomNum == 1) {
            initNumVerificationCode(image);
        } else {
            initNumVerificationCode(image);
        }
    }

    /**
     * 功能：设置第一种验证码的属性
     */
    public void initNumVerificationCode(BufferedImage image) {

        Random random = new Random(); // 生成随机类
        Graphics g = initImage(image, random);
        String sRand = "";
        for (int i = 0; i < 4; i++) {
            String rand = String.valueOf(random.nextInt(10));
            sRand += rand;
            // 将认证码显示到图象中
            g.setFont(new Font("Times New Roman", Font.BOLD, 24));
            g.setColor(new Color(20 + random.nextInt(110), 20 + random
                    .nextInt(110), 20 + random.nextInt(110)));
            // 调用函数出来的颜色相同，可能是因为种子太接近，所以只能直接生成
            g.drawString(rand, 20 * i + 6, 16 + random.nextInt(10));
        }
        this.setStr(sRand);/* 赋值验证码 */
        // 图象生效
        g.dispose();
        this.setImage(drawImage(image));
    }

    /**
     * 功能：设置第二种验证码属性
     */
    public void initLetterAndNumVerificationCode(BufferedImage image) {

        Random random = new Random(); // 生成随机类
        Graphics g = initImage(image, random);
        String[] letter = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
                "W", "X", "Y", "Z" };
        String sRand = "";
        for (int i = 0; i < 4; i++) {
            String tempRand = "";
            if (random.nextBoolean()) {
                tempRand = String.valueOf(random.nextInt(10));
            } else {
                tempRand = letter[random.nextInt(25)];
                if (random.nextBoolean()) {// 随机将该字母变成小写
                    tempRand = tempRand.toLowerCase();
                }
            }
            sRand += tempRand;
            g.setColor(new Color(20 + random.nextInt(10), 20 + random
                    .nextInt(110), 20 + random.nextInt(110)));
            g.setFont(new Font("Times New Roman", Font.BOLD, 24));
            g.drawString(tempRand, 20 * i + 6, 16 + random.nextInt(10));
        }
        this.setStr(sRand);/* 赋值验证码 */
        g.dispose(); // 图象生效
        this.setImage(drawImage(image));
    }

    /**
     * 功能：设置第三种验证码属性 即：肆+？=24
     */
    public void initChinesePlusNumVerificationCode(BufferedImage image) {
        // String[] cn = { "一", "二", "三", "四", "五", "六", "七", "八", "九", "十" };
        // String[] cn = { "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖", "拾" };
        Random random = new Random(); // 生成随机类
        Graphics g = initImage(image, random);
        int sumValue = random.nextInt(1000) + 99;
        int y = random.nextInt(100);
        this.setStr(String.valueOf(sumValue - y));
        g.setFont(new Font("楷体", Font.PLAIN, 18));// 设定字体
        g.setColor(new Color(20 + random.nextInt(10), 20 + random.nextInt(110),
                20 + random.nextInt(110)));
        g.drawString(String.valueOf(y), 2, 16);
        g.drawString("+", 20, 16);
        g.drawString("?", 28, 16);
        g.drawString("=", 41, 16);
        g.drawString(String.valueOf(sumValue), 53, 16);
        g.dispose(); // 图象生效
        this.setImage(drawImage(image));

    }

    public Graphics initImage(BufferedImage image, Random random) {
        Graphics g = image.getGraphics(); // 获取图形上下文
        g.setColor(getRandColor(200, 250));// 设定背景色
        g.fillRect(0, 0, WIDTH, HEIGHT);
        g.setFont(new Font("Times New Roman", Font.PLAIN, 28));// 设定字体
        g.setColor(getRandColor(160, 200)); // 随机产生165条干扰线，使图象中的认证码不易被其它程序探测到
        for (int i = 0; i < 165; i++) {
            int x = random.nextInt(WIDTH);
            int y = random.nextInt(HEIGHT);
            int xl = random.nextInt(12);
            int yl = random.nextInt(12);
            g.drawLine(x, y, x + xl, y + yl);
        }
        return g;
    }

    public ByteArrayInputStream drawImage(BufferedImage image) {
        ByteArrayInputStream input = null;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            ImageOutputStream imageOut = ImageIO
                    .createImageOutputStream(output);
            ImageIO.write(image, "JPEG", imageOut);
            imageOut.close();
            input = new ByteArrayInputStream(output.toByteArray());
        } catch (Exception e) {
            System.out.println("验证码图1片产生出现错误：" + e.toString());
        }
        return input;
    }

    /*
     * 功能：给定范围获得随机颜色
     */
    private Color getRandColor(int fc, int bc) {
        Random random = new Random();
        if (fc > 255)
            fc = 255;
        if (bc > 255)
            bc = 255;
        int r = fc + random.nextInt(bc - fc);
        int g = fc + random.nextInt(bc - fc);
        int b = fc + random.nextInt(bc - fc);
        return new Color(r, g, b);
    }

    /**
     * 功能：获取验证码的字符串值
     *
     * @return
     */
    public String getVerificationCodeValue() {
        return this.getStr();
    }

    /**
     * 功能：取得验证码图片
     *
     * @return
     */
    public ByteArrayInputStream getImage() {
        return this.image;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public void setImage(ByteArrayInputStream image) {
        this.image = image;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
