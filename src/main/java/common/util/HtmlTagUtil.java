package common.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 类功能简介：html标签工具类<br>
 * 可以简单处理html标签
 * 功能1：将一篇复杂的文章，取出前N个文字
 * 功能2：取出该文章中第一幅图片
 * @author lpf0
 *
 */
public class HtmlTagUtil {
	
	/**
	 * 去除字符串中的回车、空格、制表符
	 * @param str
	 * @return
	 */
	public static String conversString(String str) {
		String result = null;
		Pattern p = Pattern.compile("\\s*|\t|\r|\n");
	    Matcher m = p.matcher(str);
	    result = m.replaceAll("");
		return result;
	}
	
	/**
	 * 获取一篇文章的文字简介，去除html元素
	 * @param content
	 * @return
	 */
	public static String getSimContent(String content){
		/**
		 * 替换HTML标签
		 */
		content = content.replaceAll("\\&[a-zA-Z]{0,9};", "").replaceAll("<[^>]*>", "\n\t");
		content = conversString(content);
		if(content != null && content.length() > 90)
			content = content.substring(0, 89) + "...";
		return content;
	}
	
	public static final Pattern PATTERN = Pattern.compile(
			"<img\\s+(?:[^>]*)src\\s*=\\s*([^>]+)", Pattern.CASE_INSENSITIVE
					| Pattern.MULTILINE);

	/**
	 * 取出该文章中第一幅图片
	 * @param html
	 * @return
	 */
	public static String getFirstImgByHtml(String html){
		List<String> list = getImgSrc(html);
		if(list.isEmpty()){
			return "";
		}
		else {
			return list.get(0);
		}
	}
	/**
	 * 得到文章中全部的图片
	 * @param html
	 * @return
	 */
	public static List<String> getImgSrc(String html) {
		Matcher matcher = PATTERN.matcher(html);
		List<String> list = new ArrayList<String>();
		while (matcher.find()) {
			String group = matcher.group(1);
			if (group == null) {
				continue;
			}
			// 这里可能还需要更复杂的判断,用以处理src="...."内的一些转义符
			if (group.startsWith("'")) {
				list.add(group.substring(1, group.indexOf("'", 1)));
			} else if (group.startsWith("\"")) {
				list.add(group.substring(1, group.indexOf("\"", 1)));
			} else {
				list.add(group.split("\\s")[0]);
			}
		}
		return list;
	}
	
	public static void main(String[] arg){
		String htmlVal = "<div>safsf";
		Pattern p =  Pattern.compile("<(!|/)?\\w+( ((.|\n)*?\"\")?)? *>", Pattern.CASE_INSENSITIVE
				| Pattern.MULTILINE);
		
		Matcher matcher = p.matcher(htmlVal);
		System.out.println(matcher.find());
	}
}

