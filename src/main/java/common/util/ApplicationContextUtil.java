package common.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 
 * @author  李朋飞
 *
 */
public class ApplicationContextUtil{

	private static ApplicationContext context;

	/**
	 * 根据id获取bean对象
	 * @param beanName
	 * @return
	 */
	public static Object getBeanById(String beanName) {
		if(context == null)
			context = new ClassPathXmlApplicationContext("classpath*:/spring/*.xml");
		return context.getBean(beanName);
	}
	

	public static ApplicationContext getContext() {
		return context;
	}
}
