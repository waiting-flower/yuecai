package common.test.hunli;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import common.util.FileUtil;
import common.util.StringUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class Hunlicehua {
	/*
	 * 价格，区域，商家名，商家口号，到店礼，
	 * 优惠礼，支付礼，服务价格最低价，服务价格最高价，案例，套系，星级，商家略缩图（230x180）
	 */
	String jiage;
	String quyu;
	String shangjia;
	String shangjiakouhao;//或者简介
	String daodianli;
	String youhuili;
	String zhifuli;
	String zuidijia;
	String zuigaojia;
	String shangjiatu;
	String address;
	String email;//商家email
	List anliList;
	
	static String quyuArr = "全部,渝中区,渝北区,江北区,南岸区,巴南区,北碚区,沙坪坝区,大渡口区,九龙坡区,万盛区,万州区,涪陵区,黔江区,江津区,合川区,永川区,南川区";
	static String quyuArrList[] = quyuArr.split(",");
	static List<String> quyuList = new ArrayList<String>();
	
	static String shangjiaFilePath = "e:/json/shangjia.json";
	static String shangjiaIdFilePath = "e:/json/shangjiaId.json";
	static String anliFilePath = "e:/json/anli.json";
	static String pinglunListFilePath = "e:/json/pinglun.json";
	
	
	static String anlilistFilePath = "e:/json/套餐.json";
	
	static String xinniangFilePath = "e:/json/新娘说.json";
	static String xinniangPinglunFilePath = "e:/json/新娘说评论.json";
	
	
	static String miyueFilePath = "e:/json/蜜月游.json";

	
	static String JIUDIAN_FilePath = "e:/json/酒店.json";
	static String JIUDIAN_PINGLUN_FilePath = "e:/json/酒店评论.json";

	/*
	 * 婚纱摄影
	 */
	static String HUNSHASHEYING_FilePath = "e:/json/婚纱摄影.json";
	static String HUNSHASHEYING_PINGLUN_FilePath = "e:/json/婚纱摄影评论.json";
	
	/*
	 * 婚品商城
	 */
	static String SHOP_FilePath = "e:/json/商城商品.json";
	static String SHOP_PINGLUN_FilePath = "e:/json/商城商品评论.json";
	
	
	/*
	 * 婚纱策划套餐
	 */
	static String  CEHUATAOCAN_FilePath = "e:/json/婚礼策划套餐.json";
	
	
	
	/*
	 * 婚纱策划套餐
	 */
	static String  B1_FilePath = "e:/json/婚礼摄影商家.json";
	
	/*
	 * 婚纱策划套餐
	 */
	static String  B2_FilePath = "e:/json/婚礼摄像商家.json";
	
	
	/*
	 * 婚纱策划套餐
	 */
	static String  B3_FilePath = "e:/json/婚礼跟妆商家.json";
	
	/*
	 * 婚纱策划套餐
	 */
	static String  B4_FilePath = "e:/json/婚礼司仪商家.json";
	
	static String  AA1 = "e:/json/婚礼摄影套餐.json";
	static String  BB1 = "e:/json/婚礼摄影评论.json";
	
	static String  AA2 = "e:/json/婚礼摄像套餐.json";
	static String  BB2 = "e:/json/婚礼摄像评论.json";
	
	static String  AA3 = "e:/json/婚礼跟妆套餐.json";
	static String  BB3 = "e:/json/婚礼跟妆评论.json";
	
	static String  AA4 = "e:/json/婚礼司仪套餐.json";
	static String  BB4 = "e:/json/婚礼司仪评论.json";
	
 	
	public static void main(String[] arg) throws Exception{
		try {
			hunshasheying();
  		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 婚纱摄影
	 * @throws Exception
	 */
	public static void hunshasheying() throws Exception{
		for(int pageNo = 0; pageNo <= 20; pageNo++){
			String url = "http://m.hunliji.com/p/wedding/index.php/home/APIMerchant/"
					+ "WorkList?city=4&sort%5Bkey%5D=default&sort%5Border%5D=desc&property=11&commodity_type=0&page=" + pageNo +"&per_page=20";
			String result = HTTPRULConnect(url);
			JSONArray resultArray = JSONObject.fromObject(result).getJSONArray("works");
			for(int index = 0; index < resultArray.size(); index++){
				JSONObject hunshaJSON = resultArray.getJSONObject(index);
				String id = hunshaJSON.getString("id");
				String detailUrl = "http://m.hunliji.com/p/wedding/index.php/home/APISetMeal/info?id=" + id + "&is_mark=1";
				JSONObject detailJsonObject = JSONObject.fromObject(HTTPRULConnect(detailUrl)).getJSONObject("data").getJSONObject("work");
				if(!detailJsonObject.containsKey("pay_all_gift")){
					continue;
				}
				Map<String, Object> map = new HashMap<String, Object>();
				String title = detailJsonObject.getString("title");
				String cover_path = detailJsonObject.getString("cover_path");
				String collectors_count = detailJsonObject.getString("collectors_count");
				String comments_count = detailJsonObject.has("comments_count") ? detailJsonObject.getString("comments_count") : "";
				String watch_count = detailJsonObject.getString("watch_count");
				String sales_count = detailJsonObject.getString("sales_count");
				String actual_price = detailJsonObject.getString("actual_price");
				String market_price = detailJsonObject.getString("market_price");
				String describe = detailJsonObject.getString("describe");
				String order_gift = null;
				if(detailJsonObject.containsKey("order_gift")){
					order_gift = detailJsonObject.getString("order_gift");
				}
				String pay_all_gift = detailJsonObject.getString("pay_all_gift");
				String show_price = detailJsonObject.getString("show_price");
				
				map.put("id", id);
				map.put("title", title);
				map.put("cover_path", cover_path);
				map.put("collectors_count", collectors_count);
				map.put("comments_count", comments_count);
				map.put("watch_count", watch_count);
				map.put("sales_count", sales_count);
				map.put("actual_price", actual_price);
				map.put("market_price", market_price);
				map.put("describe", describe);
				map.put("order_gift", order_gift);
				map.put("pay_all_gift", pay_all_gift);
				map.put("show_price", show_price);
				 
				
				JSONArray photosArr = detailJsonObject.getJSONArray("detail_photos");
				List<String> picsList = new ArrayList<String>();
				for(int i = 0; i < photosArr.size(); i++){
					JSONObject item = photosArr.getJSONObject(i);
					String img = item.getString("img");
					picsList.add(img);
				}
				map.put("picsList", picsList);
				
				List<Map<String, Object>> mealInfoList = new ArrayList<Map<String,Object>>();
				JSONArray mealInfoValueArr = detailJsonObject.getJSONArray("mealInfoValue");
				for(int i = 0; i < mealInfoValueArr.size();i++){
					JSONObject item = mealInfoValueArr.getJSONObject(i);
					
					Map<String, Object> itemMap = new HashMap<String, Object>();
					
					String mealTitle = item.getString("title");
					String mealId = item.getString("id");
					
					
					itemMap.put("mealId", mealId);
					itemMap.put("mealTitle", mealTitle);
					
					JSONArray childArr = item.getJSONArray("children");
					for(int j = 0; j < childArr.size(); j++){
						JSONObject child = childArr.getJSONObject(j);
						String childTitle = child.getString("title");
						String childVal = child.getJSONArray("field_value").toString();
						itemMap.put(childTitle, childVal);
	 				}
					mealInfoList.add(itemMap);
				}

				map.put("mealInfoList", mealInfoList);
				
				
				//merchant
				JSONObject merchantJson = detailJsonObject.getJSONObject("merchant");
				String merchantId = merchantJson.getString("id");
				String merchantName = merchantJson.getString("name");
				map.put("merchantId", merchantId);
				map.put("merchantName", merchantName);
				
				FileUtil.addContentToFile(AA4, JSONObject.fromObject(map).toString());
				
				String commentUrl = "http://m.hunliji.com/p/wedding/index.php/Home/APIOrderComment/"
						+ "merchantCommentList?merchant_id=" + merchantId + "&tag_id=0&page=1&per_page=20";
				JSONArray listJsonArray = JSONObject.fromObject(HTTPRULConnect(commentUrl)).getJSONObject("data").getJSONArray("list");
				
				for(int i = 0; i < listJsonArray.size(); i++){
					JSONObject comentJsonObject = listJsonArray.getJSONObject(i);
					Map<String, Object> itemMap = new HashMap<String, Object>();
					String comentId = comentJsonObject.getString("id");
					String content = comentJsonObject.getString("content");
					String authorNick = comentJsonObject.getJSONObject("author").getString("nick");
					String authorId = comentJsonObject.getJSONObject("author").getString("id");
					String authorPath = comentJsonObject.getJSONObject("author").getString("avatar");
					
					itemMap.put("comentId", comentId);
					itemMap.put("content", content);
					itemMap.put("authorNick", authorNick);
					itemMap.put("authorId", authorId);
					itemMap.put("authorPath", authorPath);
					itemMap.put("huashaId", id);
					itemMap.put("mendianId", merchantId);
					
					JSONArray picArray = comentJsonObject.getJSONArray("comment_photos");
					List<String> photoList = new ArrayList<String>();
					for(int j = 0; j < picArray.size(); j++){
						JSONObject item = picArray.getJSONObject(j);
						photoList.add(item.getString("photo_path"));
						
					}
					itemMap.put("photoList", photoList);
					FileUtil.addContentToFile(BB4, JSONObject.fromObject(itemMap).toString());
				}
			}
			
			
		}
	}
	
	
	public static void hunlisiyi() throws Exception{
		for(int N = 1; N <= 3; N++){
			String url = "http://m.hunliji.com/p/wedding/index.php/home/APIMerchant/merchantV2?city=4&property=11&category_id=0&area_id=0&sort=default&page=" + N + "&per_page=20&bond_sign=0&is_grade=0";
			JSONArray listJsonArray = JSONObject.fromObject(HTTPRULConnect(url)).getJSONObject("data").getJSONObject("normal_merchant").getJSONArray("list");
			for(int i = 0; i < listJsonArray.size(); i++){
				
				Map<String, Object> map = new HashMap<String, Object>();
				
				JSONObject item = listJsonArray.getJSONObject(i);
				String detailId = item.getString("id");
				String detailurl = "http://m.hunliji.com/p/wedding/index.php/home/APIMerchant/detailMerchantV2?mer_id=" + detailId;
				JSONObject json = JSONObject.fromObject(HTTPRULConnect(detailurl)).getJSONObject("data");
				String name = json.getString("name");
				String address = json.getString("address");
				String logo_path = json.getString("logo_path");
				String cover_path = json.getString("cover_path");
				String email = json.getString("email");
				String des = json.getString("des");
				String phone = json.getString("phone");
				String works_count = json.getString("works_count");
				String active_works_pcount = json.getString("active_works_pcount");
				String active_cases_pcount = json.getString("active_cases_pcount");
				String pro_price = json.getString("pro_price");
				String contact_phones = json.getJSONArray("contact_phones").toString();
				
				map.put("id", detailId);
				map.put("name", name);
				map.put("address", address);
				map.put("logo_path", logo_path);
				map.put("cover_path", cover_path);
				map.put("email", email);
				map.put("des", des);
				map.put("phone", phone);
				map.put("works_count", works_count);
				map.put("active_works_pcount", active_works_pcount);
				map.put("active_cases_pcount", active_cases_pcount);
				map.put("pro_price", pro_price);
				map.put("contact_phones", contact_phones);
				JSONObject aaaJsonObject = json.getJSONObject("privilege").getJSONObject("items").getJSONObject("shop_gift");
				String shop_gift = "";
				if(!aaaJsonObject.isNullObject()){
					shop_gift = aaaJsonObject.getString("value");
				}
				
				map.put("shop_gift", shop_gift);
				
				List<Map<String, Object>> promiseMapList = new ArrayList<Map<String,Object>>();
				
				JSONObject ssssJsonObject = json.getJSONObject("privilege").getJSONObject("items").getJSONObject("merchant_promise");
				if(!ssssJsonObject.isNullObject()){
					JSONArray proJsonArray = ssssJsonObject.getJSONArray("value");
					for(int M = 0; M < proJsonArray.size();M++){
						JSONObject pro = proJsonArray.getJSONObject(M);
						Map<String, Object> itemMap = new HashMap<String, Object>();
						itemMap.put("name", pro.getString("main"));
						itemMap.put("detail", pro.getString("detail"));
						promiseMapList.add(itemMap);
					}
				}
				
				
				map.put("promiseMapList", promiseMapList);
				
				
				List<String> imgList = new ArrayList<String>();
				if(!json.get("real_photos").toString().equals("null")){
					JSONArray real_photosArr = json.getJSONArray("real_photos");
					for(int M = 0; M < real_photosArr.size(); M++){
						imgList.add(real_photosArr.getJSONObject(M).getString("img"));
					}
					map.put("imgList", imgList);
				}
				
				FileUtil.addContentToFile(B4_FilePath, JSONObject.fromObject(map).toString());
			}
		}
	}
	
	public static void hunligenzhuang() throws Exception{
		for(int N = 1; N <= 7; N++){
			String url = "http://m.hunliji.com/p/wedding/index.php/home/APIMerchant/merchantV2?city=4&property=9&category_id=0&area_id=0&sort=default&page=" + N + "&per_page=20&bond_sign=0&is_grade=0";
			JSONArray listJsonArray = JSONObject.fromObject(HTTPRULConnect(url)).getJSONObject("data").getJSONObject("normal_merchant").getJSONArray("list");
			for(int i = 0; i < listJsonArray.size(); i++){
				
				Map<String, Object> map = new HashMap<String, Object>();
				
				JSONObject item = listJsonArray.getJSONObject(i);
				String detailId = item.getString("id");
				String detailurl = "http://m.hunliji.com/p/wedding/index.php/home/APIMerchant/detailMerchantV2?mer_id=" + detailId;
				JSONObject json = JSONObject.fromObject(HTTPRULConnect(detailurl)).getJSONObject("data");
				String name = json.getString("name");
				String address = json.getString("address");
				String logo_path = json.getString("logo_path");
				String cover_path = json.getString("cover_path");
				String email = json.getString("email");
				String des = json.getString("des");
				String phone = json.getString("phone");
				String works_count = json.getString("works_count");
				String active_works_pcount = json.getString("active_works_pcount");
				String active_cases_pcount = json.getString("active_cases_pcount");
				String pro_price = json.getString("pro_price");
				String contact_phones = json.getJSONArray("contact_phones").toString();
				
				map.put("id", detailId);
				map.put("name", name);
				map.put("address", address);
				map.put("logo_path", logo_path);
				map.put("cover_path", cover_path);
				map.put("email", email);
				map.put("des", des);
				map.put("phone", phone);
				map.put("works_count", works_count);
				map.put("active_works_pcount", active_works_pcount);
				map.put("active_cases_pcount", active_cases_pcount);
				map.put("pro_price", pro_price);
				map.put("contact_phones", contact_phones);
				JSONObject aaaJsonObject = json.getJSONObject("privilege").getJSONObject("items").getJSONObject("shop_gift");
				String shop_gift = "";
				if(!aaaJsonObject.isNullObject()){
					shop_gift = aaaJsonObject.getString("value");
				}
				
				map.put("shop_gift", shop_gift);
				
				List<Map<String, Object>> promiseMapList = new ArrayList<Map<String,Object>>();
				
				JSONObject ssssJsonObject = json.getJSONObject("privilege").getJSONObject("items").getJSONObject("merchant_promise");
				if(!ssssJsonObject.isNullObject()){
					JSONArray proJsonArray = ssssJsonObject.getJSONArray("value");
					for(int M = 0; M < proJsonArray.size();M++){
						JSONObject pro = proJsonArray.getJSONObject(M);
						Map<String, Object> itemMap = new HashMap<String, Object>();
						itemMap.put("name", pro.getString("main"));
						itemMap.put("detail", pro.getString("detail"));
						promiseMapList.add(itemMap);
					}
				}
				
				
				map.put("promiseMapList", promiseMapList);
				
				
				List<String> imgList = new ArrayList<String>();
				if(!json.get("real_photos").toString().equals("null")){
					JSONArray real_photosArr = json.getJSONArray("real_photos");
					for(int M = 0; M < real_photosArr.size(); M++){
						imgList.add(real_photosArr.getJSONObject(M).getString("img"));
					}
					map.put("imgList", imgList);
				}
				
				FileUtil.addContentToFile(B3_FilePath, JSONObject.fromObject(map).toString());
			}
		}
	}
	
	public static void hunlishexiang() throws Exception{
		for(int N = 1; N <= 13; N++){
			String url = "http://m.hunliji.com/p/wedding/index.php/home/APIMerchant/merchantV2?city=4&property=2&category_id=0&area_id=0&sort=default&page=" + N + "&per_page=20&bond_sign=0&is_grade=0";
			JSONArray listJsonArray = JSONObject.fromObject(HTTPRULConnect(url)).getJSONObject("data").getJSONObject("normal_merchant").getJSONArray("list");
			for(int i = 0; i < listJsonArray.size(); i++){
				
				Map<String, Object> map = new HashMap<String, Object>();
				
				JSONObject item = listJsonArray.getJSONObject(i);
				String detailId = item.getString("id");
				String detailurl = "http://m.hunliji.com/p/wedding/index.php/home/APIMerchant/detailMerchantV2?mer_id=" + detailId;
				JSONObject json = JSONObject.fromObject(HTTPRULConnect(detailurl)).getJSONObject("data");
				String name = json.getString("name");
				String address = json.getString("address");
				String logo_path = json.getString("logo_path");
				String cover_path = json.getString("cover_path");
				String email = json.getString("email");
				String des = json.getString("des");
				String phone = json.getString("phone");
				String works_count = json.getString("works_count");
				String active_works_pcount = json.getString("active_works_pcount");
				String active_cases_pcount = json.getString("active_cases_pcount");
				String pro_price = json.getString("pro_price");
				String contact_phones = json.getJSONArray("contact_phones").toString();
				
				map.put("id", detailId);
				map.put("name", name);
				map.put("address", address);
				map.put("logo_path", logo_path);
				map.put("cover_path", cover_path);
				map.put("email", email);
				map.put("des", des);
				map.put("phone", phone);
				map.put("works_count", works_count);
				map.put("active_works_pcount", active_works_pcount);
				map.put("active_cases_pcount", active_cases_pcount);
				map.put("pro_price", pro_price);
				map.put("contact_phones", contact_phones);
				JSONObject aaaJsonObject = json.getJSONObject("privilege").getJSONObject("items").getJSONObject("shop_gift");
				String shop_gift = "";
				if(!aaaJsonObject.isNullObject()){
					shop_gift = aaaJsonObject.getString("value");
				}
				
				map.put("shop_gift", shop_gift);
				
				List<Map<String, Object>> promiseMapList = new ArrayList<Map<String,Object>>();
				
				JSONObject ssssJsonObject = json.getJSONObject("privilege").getJSONObject("items").getJSONObject("merchant_promise");
				if(!ssssJsonObject.isNullObject()){
					JSONArray proJsonArray = ssssJsonObject.getJSONArray("value");
					for(int M = 0; M < proJsonArray.size();M++){
						JSONObject pro = proJsonArray.getJSONObject(M);
						Map<String, Object> itemMap = new HashMap<String, Object>();
						itemMap.put("name", pro.getString("main"));
						itemMap.put("detail", pro.getString("detail"));
						promiseMapList.add(itemMap);
					}
				}
				
				
				map.put("promiseMapList", promiseMapList);
				
				
				List<String> imgList = new ArrayList<String>();
				if(!json.get("real_photos").toString().equals("null")){
					JSONArray real_photosArr = json.getJSONArray("real_photos");
					for(int M = 0; M < real_photosArr.size(); M++){
						imgList.add(real_photosArr.getJSONObject(M).getString("img"));
					}
					map.put("imgList", imgList);
				}
				
				FileUtil.addContentToFile(B2_FilePath, JSONObject.fromObject(map).toString());
			}
		}
	}
	
	public static void hunlisheying() throws Exception{
		for(int N = 0; N < 4; N++){
			String url = "http://m.hunliji.com/p/wedding/index.php/home/APIMerchant/merchantV2?city=4&property=7&category_id=0&area_id=0&sort=default&page=" + N + "&per_page=20&bond_sign=0&is_grade=0";
			JSONArray listJsonArray = JSONObject.fromObject(HTTPRULConnect(url)).getJSONObject("data").getJSONObject("normal_merchant").getJSONArray("list");
			for(int i = 0; i < listJsonArray.size(); i++){
				
				Map<String, Object> map = new HashMap<String, Object>();
				
				JSONObject item = listJsonArray.getJSONObject(i);
				String detailId = item.getString("id");
				String detailurl = "http://m.hunliji.com/p/wedding/index.php/home/APIMerchant/detailMerchantV2?mer_id=" + detailId;
				JSONObject json = JSONObject.fromObject(HTTPRULConnect(detailurl)).getJSONObject("data");
				String name = json.getString("name");
				String address = json.getString("address");
				String logo_path = json.getString("logo_path");
				String cover_path = json.getString("cover_path");
				String email = json.getString("email");
				String des = json.getString("des");
				String phone = json.getString("phone");
				String works_count = json.getString("works_count");
				String active_works_pcount = json.getString("active_works_pcount");
				String active_cases_pcount = json.getString("active_cases_pcount");
				String pro_price = json.getString("pro_price");
				String contact_phones = json.getJSONArray("contact_phones").toString();
				
				map.put("id", detailId);
				map.put("name", name);
				map.put("address", address);
				map.put("logo_path", logo_path);
				map.put("cover_path", cover_path);
				map.put("email", email);
				map.put("des", des);
				map.put("phone", phone);
				map.put("works_count", works_count);
				map.put("active_works_pcount", active_works_pcount);
				map.put("active_cases_pcount", active_cases_pcount);
				map.put("pro_price", pro_price);
				map.put("contact_phones", contact_phones);
				JSONObject aaaJsonObject = json.getJSONObject("privilege").getJSONObject("items").getJSONObject("shop_gift");
				String shop_gift = "";
				if(!aaaJsonObject.isNullObject()){
					shop_gift = aaaJsonObject.getString("value");
				}
				
				map.put("shop_gift", shop_gift);
				
				List<Map<String, Object>> promiseMapList = new ArrayList<Map<String,Object>>();
				
				JSONObject ssssJsonObject = json.getJSONObject("privilege").getJSONObject("items").getJSONObject("merchant_promise");
				if(!ssssJsonObject.isNullObject()){
					JSONArray proJsonArray = ssssJsonObject.getJSONArray("value");
					for(int M = 0; M < proJsonArray.size();M++){
						JSONObject pro = proJsonArray.getJSONObject(M);
						Map<String, Object> itemMap = new HashMap<String, Object>();
						itemMap.put("name", pro.getString("main"));
						itemMap.put("detail", pro.getString("detail"));
						promiseMapList.add(itemMap);
					}
				}
				
				
				map.put("promiseMapList", promiseMapList);
				
				
				List<String> imgList = new ArrayList<String>();
				if(!json.get("real_photos").toString().equals("null")){
					JSONArray real_photosArr = json.getJSONArray("real_photos");
					for(int M = 0; M < real_photosArr.size(); M++){
						imgList.add(real_photosArr.getJSONObject(M).getString("img"));
					}
					map.put("imgList", imgList);
				}
				
				FileUtil.addContentToFile(B1_FilePath, JSONObject.fromObject(map).toString());
			}
		}
	}
	
	/**
	 * 商城
	 * @throws Exception
	 */
	public static void shop() throws Exception{
		for(int pageNo = 20;pageNo <= 800; pageNo++){
			String url = "http://m.hunliji.com/p/wedding/index.php/home/APISubPageShop/product_listV2?pid=0&page=" + pageNo + "&per_page=10";
			JSONArray listJSONArray = JSONObject.fromObject(HTTPRULConnect(url)).getJSONObject("data").getJSONArray("list");
			for(int index = 0; index < listJSONArray.size(); index++){
				JSONObject detailJSON = listJSONArray.getJSONObject(index);
				String detailid = detailJSON.getString("id");
				String detailURL = "http://m.hunliji.com/p/wedding/index.php/shop/APIShopProduct/info?id=" + detailid + "&cid=0&is_mark=1";
				JSONObject goodsJSON = JSONObject.fromObject(HTTPRULConnect(detailURL)).getJSONObject("data");
				
				Map<String, Object> map = new HashMap<String, Object>();
				
				String id = goodsJSON.getString("id");
				String category_id = goodsJSON.getString("category_id");
				String title = goodsJSON.getString("title");
				String coverImg = goodsJSON.getJSONObject("cover_image").getString("img");
				String sold_count = goodsJSON.getString("sold_count");
				String collectors_count = goodsJSON.getString("collectors_count");
				String comments_count = goodsJSON.getString("comments_count");
				String actual_price = goodsJSON.getString("actual_price");
				String market_price = goodsJSON.getString("market_price");
				String sale_price = goodsJSON.getString("sale_price");
				String merchantId = goodsJSON.getJSONObject("merchant").getString("id");
				String merchantName = goodsJSON.getJSONObject("merchant").getString("name");
				String merchant_logo_path = goodsJSON.getJSONObject("merchant").getString("logo_path");
				String shipping_fee = goodsJSON.getString("shipping_fee");
				String describe = goodsJSON.getString("describe");
				
				map.put("id", id);
				map.put("category_id", category_id);
				map.put("title", title);
				map.put("coverImg", coverImg);
				map.put("sold_count", sold_count);
				map.put("collectors_count", collectors_count);
				map.put("comments_count", comments_count);
				map.put("actual_price", actual_price);
				map.put("market_price", market_price);
				map.put("sale_price", sale_price);
				map.put("merchantId", merchantId);
				map.put("merchantName", merchantName);
				map.put("merchant_logo_path", merchant_logo_path);
				map.put("shipping_fee", shipping_fee);
				map.put("describe", describe);
				
				JSONObject cateJsonObject = goodsJSON.getJSONObject("category").getJSONObject("root");
				String category_name_root = cateJsonObject.getString("name");
				String category_name = goodsJSON.getJSONObject("category").getString("name");
				String category_img_path = goodsJSON.getJSONObject("category").getString("cover_image");
				
				
				map.put("category_name_root", category_name_root);
				map.put("category_name", category_name);
				map.put("category_img_path", category_img_path);
				
				
				JSONArray imgListArray = goodsJSON.getJSONArray("header_photos");
				List<String> imgList = new ArrayList<String>();
				for(int i = 0; i < imgListArray.size(); i++){
					imgList.add(imgListArray.getJSONObject(i).getString("img"));
				}
				map.put("imgList", imgList);
				
				
				JSONArray detailImgListArray = goodsJSON.getJSONArray("detail_photos");
				List<String> detailImgList = new ArrayList<String>();
				for(int i = 0; i < detailImgListArray.size(); i++){
					detailImgList.add(detailImgListArray.getJSONObject(i).getString("img"));
				}
				map.put("detailImgList", detailImgList);
				
				
				JSONArray skuListArray = goodsJSON.getJSONArray("skus");
				List<Map<String, Object>> skuMapList = new ArrayList<Map<String,Object>>();
				for(int i = 0; i < skuListArray.size(); i++){
					Map<String, Object> itemMap = new HashMap<String, Object>();
					JSONObject sku = skuListArray.getJSONObject(i);
					itemMap.put("skuId", sku.getString("id"));
					itemMap.put("skuName", sku.getString("name"));
					itemMap.put("actual_price", sku.getString("actual_price"));
					itemMap.put("market_price", sku.getString("market_price"));
					itemMap.put("show_price", sku.getString("show_price"));
					itemMap.put("show_num", sku.getString("show_num"));
					skuMapList.add(itemMap);
				}
				map.put("skuMapList", skuMapList);
				
				FileUtil.addContentToFile(SHOP_FilePath, JSONObject.fromObject(map).toString());
				
				int commentsCount = Integer.parseInt(comments_count);
				int page = commentsCount/10;
				if (page < 1) {
					page = 1;
				}
				page = 1;
				for(int N = 0; N < page; N++){
					String comentURL = "http://m.hunliji.com/p/wedding/index.php/Shop/APIShopComment/list?product_id=" + id + "&page=" + N + "&per_page=10";
					JSONArray listJSONArr = JSONObject.fromObject(HTTPRULConnect(comentURL)).getJSONObject("data").getJSONArray("list");
					for(int M = 0; M < listJSONArr.size(); M++){
						Map<String, Object> comMap = new HashMap<String, Object>();
						JSONObject itemJSON = listJSONArr.getJSONObject(M);
						String commentId = itemJSON.getString("id");
						String content = itemJSON.getString("content");
						String user_id = itemJSON.getString("user_id");
						String merchant_id = itemJSON.getString("merchant_id");
						String product_id = itemJSON.getString("product_id");
						String userNick = itemJSON.getJSONObject("user").getString("nick");
						String userImg = itemJSON.getJSONObject("user").getString("avatar");
						String skuName = itemJSON.getJSONObject("sku").getString("name");
						
						comMap.put("commentId", commentId);
						comMap.put("content", content);
						comMap.put("user_id", user_id);
						comMap.put("merchant_id", merchant_id);
						comMap.put("userNick", userNick);
						comMap.put("userImg", userImg);
						comMap.put("skuName", skuName);
						comMap.put("product_id", product_id);
						
						if(itemJSON.containsKey("photos") && !itemJSON.isNullObject()){
							List<String> comImgList = new ArrayList<String>();
							if(!itemJSON.get("photos").toString().equals("null")){
								JSONArray comArr = itemJSON.getJSONArray("photos");
								for(int i = 0; i < comArr.size(); i++){
									comImgList.add(comArr.getJSONObject(i).getString("img"));
								}
							}
							comMap.put("comImgList", comImgList);
						}
						FileUtil.addContentToFile(SHOP_PINGLUN_FilePath, JSONObject.fromObject(comMap).toString());
					}
				}
				
			}
		}
	}
	
	/**
	 * 婚礼策划套餐
	 * @throws Exception
	 */
	public static void hunlicehuaCaotan() throws Exception{
		for(int pageNo = 0; pageNo < 80; pageNo++){
			String url = "http://m.hunliji.com/p/wedding/index.php/home/APIMerchant/"
					+ "WorkList?city=4&sort%5Bkey%5D=default&sort%5Border%5D=desc&property=2&commodity_type=0&page=" + pageNo + "&per_page=20";
			JSONArray listJsonArr = JSONObject.fromObject(HTTPRULConnect(url)).getJSONArray("works");
			for(int index = 0; index < listJsonArr.size(); index++){
				Map<String, Object> map = new HashMap<String, Object>();
				JSONObject item = listJsonArr.getJSONObject(index);
				String id = item.getString("id");
				System.out.println(id);
				String describe = item.getString("describe");
				String cover_path = item.getString("cover_path");				
				String title = item.getString("title");
				String collectors_count = item.getString("collectors_count");
				String actual_price = item.getString("actual_price");
				String market_price = item.getString("market_price");
				String merchant_id = item.getString("merchant_id");
				String merchantName = item.getJSONObject("merchant").getString("name");
				
				map.put("id", id);
				map.put("describe", describe);
				map.put("cover_path", cover_path);
				map.put("title", title);
				map.put("collectors_count", collectors_count);
				map.put("actual_price", actual_price);
				map.put("market_price", market_price);
				map.put("merchant_id", merchant_id);
				map.put("merchantName", merchantName);
				
				
				String detailUrl = "http://m.hunliji.com/p/wedding/index.php/home/APISetMeal/info?id=" + id + "&is_mark=1";
				JSONObject detailJSON = JSONObject.fromObject(HTTPRULConnect(detailUrl)).getJSONObject("data").getJSONObject("work");
				
 
				
				System.out.println(detailJSON.get("detail_photos"));
				if(!detailJSON.containsKey("detail_photos") || detailJSON.get("detail_photos").toString().equals("null")){
					continue;
				}
				try {
					JSONArray detail_photosArray = detailJSON.getJSONArray("detail_photos");
					List<String> detailImgList = new ArrayList<String>();
					for(int i = 0; i < detail_photosArray.size(); i++){
						JSONObject itemJSON = detail_photosArray.getJSONObject(i);
						String img = itemJSON.getString("img");
						detailImgList.add(img);
					}
					map.put("detailImgList", detailImgList);
				} catch (Exception e) {
					continue;
				}
				
 
				
				
				JSONArray mealInfoValueArr = detailJSON.getJSONArray("mealInfoValue");
				if(mealInfoValueArr.size() < 1){
					continue;
				}
 				List<Map<String, Object>> itemList = new ArrayList<Map<String,Object>>();
				for(int i = 0; i < mealInfoValueArr.size(); i++){
					JSONObject itemJSON = mealInfoValueArr.getJSONObject(i);
					
					
					Map<String, Object> itemMap = new HashMap<String, Object>();
					
					String itemId = itemJSON.getString("id");
 					String itemTitle = itemJSON.getString("title");
 					
					
					List<Map<String, Object>> itemMapList = new ArrayList<Map<String, Object>>();
					JSONArray childrenArr = itemJSON.getJSONArray("children");
					for(int j = 0; j < childrenArr.size();j++){
						JSONObject childJsonObject = childrenArr.getJSONObject(j);
						Map<String, Object> childMap = new HashMap<String, Object>();
						
						childMap.put("title", childJsonObject.getString("title"));
						childMap.put("field_name", childJsonObject.getString("field_name"));
						childMap.put("field_value", childJsonObject.getJSONArray("field_value").toString());						 						
						itemMapList.add(childMap);
					}
				 
					
					
					itemMap.put("itemId", itemId);
					itemMap.put("itemTitle", itemTitle);
 					itemMap.put("itemMapList", itemMapList);
					itemList.add(itemMap);
				}
				
				map.put("itemList", itemList);
				
				FileUtil.addContentToFile(CEHUATAOCAN_FilePath, JSONObject.fromObject(map).toString());
				
			}
		}
	}
	
	
	/**
	 * 酒店
	 * @throws Exception
	 */
	public static void jiudian() throws Exception{
		for(int pageNo = 1; pageNo <= 40; pageNo++){
			String url = "https://m.daoxila.com/chongqing/HunYan-List/Page-" + pageNo + "?test=dxl";
			String result = HTTPRULConnect(url);
			JSONObject jsonObject = JSONObject.fromObject(result);
			JSONObject hotelListJsonObject = jsonObject.getJSONObject("hotelList");
			JSONArray entitiesArray = hotelListJsonObject.getJSONArray("entities");
			for(int i = 0; i < entitiesArray.size();i++){
				JSONObject itemJSON = entitiesArray.getJSONObject(i);
				System.err.println(itemJSON.toString());
				String id = itemJSON.getString("id");
				String name = itemJSON.getString("name");
				String region = itemJSON.getString("region");
				String address = itemJSON.getString("address");
				String coverPath = itemJSON.getString("cover");
				String priceMin = itemJSON.getString("priceMin");
				String priceMax = itemJSON.getString("priceMax");
				String deskMax = itemJSON.getString("deskMax");
 
				String urlBase = itemJSON.getString("url");
				
				String commnetViewUrl = "https://m.daoxila.com/index/jsonpnew/index?act=comments&shopId=" + id + "&reviewType=1&callback=jQuery2100344028164166438_1513137747024&offset=0&limit=12";
				String commentResult = HTTPRULConnect(commnetViewUrl);
				commentResult = commentResult.replace("jQuery2100344028164166438_1513137747024(", "");
				commentResult = commentResult.substring(0, commentResult.length() - 1);
				System.out.println(commentResult);
				JSONObject comentJsonObject = JSONObject.fromObject(commentResult).getJSONObject("detail");
				JSONArray comArr = comentJsonObject.getJSONArray("albums");
				
				
				for(int comIndex = 0; comIndex < comArr.size(); comIndex++){
					JSONObject comItem = comArr.getJSONObject(comIndex);
					Map<String, Object> comMap = new HashMap<String, Object>();
					
					String comname = comItem.getString("name");
					String img = comItem.getString("img");
					String content = comItem.getString("content");
					String main_score = comItem.getString("main_score");
					String jiudianId = id;
					
					comMap.put("name", comname);
					comMap.put("img", img);
					comMap.put("content", content);
					comMap.put("main_score", main_score);
					comMap.put("jiudianId", jiudianId);
 					FileUtil.addContentToFile(JIUDIAN_PINGLUN_FilePath, JSONObject.fromObject(comMap).toString());		
				}
				
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", id);
				map.put("name", name);
				map.put("region", region);
				map.put("address", address);
				map.put("coverPath", coverPath);
				map.put("priceMin", priceMin);
				map.put("priceMax", priceMax);
				map.put("deskMax", deskMax);
 				
//				JSONArray hallsJsonArray = itemJSON.getJSONArray("halls");
				
//				for(int j = 0; j < hallsJsonArray.size(); j++){
//					JSONObject hallJson = hallsJsonArray.getJSONObject(j);
//					String hallId = hallJson.getString("id");
//					String hallName = hallJson.getString("name");
//					String hallCover = hallJson.getString("cover");		
//					Map<String, Object> itemMap = new HashMap<String, Object>();
//					itemMap.put("hallId", hallId);
//					itemMap.put("hallName", hallName);
//					itemMap.put("hallCover", hallCover);
//					hallsList.add(itemMap);
//				}
//				
				String viewUrl = "https://chongqing.daoxila.com/HunYan/" + urlBase + "-Info";
				Document doc = getDOC(viewUrl);
				
				Element desEle = doc.select("div.hotel_msg").first();
				
				Element icon9Ele = doc.select("li.icon9").first();
				Element icon9aEle = doc.select("li.icon9a").first();
				Element icon9bEle = doc.select("li.icon9b").first();
				
				String des = desEle.html();
				
				map.put("jinchangfei", icon9Ele.text());
				map.put("kaipingfei", icon9aEle.text());
				map.put("fuwufei", icon9bEle.text());
				
				map.put("des", des);
				
				
				Elements peizhiListEle = doc.select("div.allocation_left > ul > li");
				List<String> peizhiList = new ArrayList<String>();
				for(Element e : peizhiListEle){
					peizhiList.add(e.text());
				}
				map.put("peizhiList", peizhiList);
				
				
				List<Map<String, Object>> hallsList = new ArrayList<Map<String,Object>>();
				
				
				
				String imglist = doc.select("div.pic_three > ul").first().attr("abs:imglist");
				map.put("imglist", imglist);
				
				Elements hallsEles = doc.select("div.ballroom_box");
				for(Element e : hallsEles){
					Map<String, Object> itemMap = new HashMap<String, Object>();
					String hallImg = e.select("div.ballroom_img").first().select("img").attr("abs:src");
					String hassName = e.select("div.ballroom_info").first().select("div.name_item > ul > li").first().text();
					String hassFee = e.select("div.ballroom_info").first().select("div.name_item > ul > li").get(1).text();
					Elements canshuEles = e.select("div.ballroom_info").first().select("div.discounts_item").select("li.canshu_item").select("ul > li");
					for(Element canshu : canshuEles){
						String canshuKey = canshu.html().split("：")[0];
						String canshuName = canshu.select("span").first().text();
						itemMap.put(canshuKey, canshuName);
					}
					
					
					itemMap.put("hallImg", hallImg);
					itemMap.put("hassName", hassName);
					itemMap.put("hassFee", hassFee);
					hallsList.add(itemMap);
					
				}
				map.put("hallsList", hallsList);
				
				
				List<Map<String, Object>> menuList = new ArrayList<Map<String,Object>>();
				Elements menuEles = doc.select("div.menuboxs > ul > li");
				for(Element menuEle : menuEles){
					Map<String, Object> menuMap = new HashMap<String, Object>();
					String menuName = "婚姻套餐";
					String menuTitle = menuEle.select("div.title").first().text();
					String menuContent = menuEle.select("div.list16").first().text();
					String price = menuEle.select("div.price").first().text();
					menuMap.put("menuName", menuName);
					menuMap.put("menuTitle", menuTitle);
					menuMap.put("menuContent", menuContent);
					menuMap.put("price", price);
					menuList.add(menuMap);
				}
				
				map.put("menuList", menuList);
				
				String jiudian = JSONObject.fromObject(map).toString();
				FileUtil.addContentToFile(JIUDIAN_FilePath, jiudian);
				 
				
			}//end for list
		}//end for pageNo
	}
	public static Document getDOC(String url) throws Exception{
		Document doc = Jsoup
				.connect(url)
				.userAgent(
						"Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Mobile Safari/537.36")
				.cookie("a", "898Z1Jh-UBQYKtjNrJLWFIyXc5-ifhUWWQ-Ygx1")
				.timeout(8000).get();
		return doc;
	}
	
	public static void miyueDetail(String url) throws Exception{
 		Document doc = Jsoup
				.connect(url)
				.userAgent(
						"Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Mobile Safari/537.36")
				.cookie("a", "898Z1Jh-UBQYKtjNrJLWFIyXc5-ifhUWWQ-Ygx1")
				.timeout(8000).get();
 		
 		
 		Map<String, Object> map = new HashMap<String, Object>();
 		
		Element priceele = doc.select("div.price > strong").get(0);
		System.out.println(priceele.html());
		
		Element titleEle = doc.select("div.product-info > h3").first();
		System.out.println(titleEle.text());
		
		Element picEle = doc.select("div.product-img").first();
		
		Element chufaEle = doc.select("div.product-info > p").first();

		Elements txtEle = doc.select("div.mod");
		String id = url.split("-")[1];
		if(txtEle.size() < 2){
			return;
		}
		String miyue = txtEle.get(0).html();
		String jiudian = txtEle.get(1).html();
		String cankaoxingcheng = txtEle.get(2).html();	 		
		String place = chufaEle.html();		
		String picPath = picEle.select("img").first().attr("abs:src");		
		String title = titleEle.text();
		String price = priceele.html();
		
		map.put("id", id);
		map.put("miyue", miyue);
		map.put("jiudian", jiudian);
		map.put("cankaoxingcheng", cankaoxingcheng);
		map.put("place", place);
		map.put("picPath", picPath);
		map.put("title", title);
		map.put("price", price);
		
		Elements picsListEle = doc.select("div.pic-list > ul > li").select("img");
		
		List<String> picList = new ArrayList<String>();
		for(Element element : picsListEle){
			String src = element.attr("abs:src");
			picList.add(src);
		}
		

		Element miyuesss = doc.select("div.focus").get(0);		
		Elements miyuPicList = miyuesss.select("div.bd > ul > li").select("img");
		List<String> miyuePicsList = new ArrayList<String>();
		for(Element element : miyuPicList){
			String src = element.attr("abs:src");
			miyuePicsList.add(src);
		}
		
		Element jiudiansss = doc.select("div.focus").get(1);
		Elements jiudianPicList = jiudiansss.select("div.bd > ul > li").select("img");
		List<String> jiudianPicsList = new ArrayList<String>();
		for(Element element : jiudianPicList){
			String src = element.attr("abs:src");
			jiudianPicsList.add(src);
		}
				
		Element jingdiansss = doc.select("div.focus").get(2);		
		Elements jingdianPicList = jingdiansss.select("div.bd > ul > li").select("img");
		List<String> jingdianPicsList = new ArrayList<String>();
		for(Element element : jingdianPicList){
			String src = element.attr("abs:src");
			jingdianPicsList.add(src);
		}
		
		map.put("miyuePicsList", miyuePicsList);
		map.put("jiudianPicList", jiudianPicsList);
		map.put("jingdianPicList",jingdianPicsList);
		
		String result = JSONObject.fromObject(map).toString();
		System.out.println(result);
		FileUtil.addContentToFile(miyueFilePath, result);
		
	}
	/**
	 * 蜜月游
	 */
	public static void miyueyou() throws Exception{
		String urlList = "https://m.daoxila.com/MiYue/Tours-137,https://m.daoxila.com/MiYue/Tours-138,https://m.daoxila.com/MiYue/Tours-139,https://m.daoxila.com/MiYue/Tours-132,https://m.daoxila.com/MiYue/Tours-133,https://m.daoxila.com/MiYue/Tours-134,https://m.daoxila.com/MiYue/Tours-135,https://m.daoxila.com/MiYue/Tours-136,https://m.daoxila.com/MiYue/Tours-126,https://m.daoxila.com/MiYue/Tours-125,https://m.daoxila.com/MiYue/Tours-124,https://m.daoxila.com/MiYue/Tours-131,https://m.daoxila.com/MiYue/Tours-130,https://m.daoxila.com/MiYue/Tours-96,https://m.daoxila.com/MiYue/Tours-127,https://m.daoxila.com/MiYue/Tours-128,https://m.daoxila.com/MiYue/Tours-129,https://m.daoxila.com/MiYue/Tours-81,https://m.daoxila.com/MiYue/Tours-85,https://m.daoxila.com/MiYue/Tours-78,https://m.daoxila.com/MiYue/Tours-83,https://m.daoxila.com/MiYue/Tours-89,https://m.daoxila.com/MiYue/Tours-87,https://m.daoxila.com/MiYue/Tours-90,https://m.daoxila.com/MiYue/Tours-106,https://m.daoxila.com/MiYue/Tours-102,https://m.daoxila.com/MiYue/Tours-105,https://m.daoxila.com/MiYue/Tours-103,https://m.daoxila.com/MiYue/Tours-7";
		for(String itemUrl : urlList.split(",")){
			miyueDetail(itemUrl);
		}
		
	}
	
	/**
	 * 新娘说
	 * @throws Exception 
	 */
	public static void xinnianshuo() throws Exception{
		//取100页
		for(int pageNo = 1; pageNo <= 100; pageNo++){
			String  url = "http://m.hunliji.com/p/wedding/Home/APICommunitySetup/RecommendThreads?city=&page=" + pageNo + "&per_page=15";
			JSONObject jsonObject = JSONObject.fromObject(HTTPRULConnect(url));
			JSONObject dataJson = jsonObject.getJSONObject("data");
			JSONArray listJsonArray = dataJson.getJSONArray("list");
			for(int i = 0; i < listJsonArray.size(); i++){
				
				Map<String, Object> map = new HashMap<String, Object>();
				
				JSONObject item = listJsonArray.getJSONObject(i);
				String id = item.getString("id");
				String entity_id = item.getString("entity_id");
				
				map.put("id", id);
				map.put("entity_id", entity_id);
				
				JSONObject entityJSON = item.getJSONObject("entity");
				String title = entityJSON.getString("title");
				String post_count = entityJSON.getString("post_count");
				String have_pics = entityJSON.getString("have_pics");
				
				map.put("title", title);
				map.put("post_count", post_count);
				map.put("have_pics", have_pics);
				
				JSONObject postJson = entityJSON.getJSONObject("post");
				String content = postJson.getString("message");
				String praised_count = postJson.getString("praised_count");
				
				map.put("content", content);
				map.put("praised_count", praised_count);
				
				List<String> picsList = new ArrayList<String>();
				if(have_pics.equals("true")){
					JSONArray picsJSONArray = postJson.getJSONArray("pics");
					for(int j = 0; j < picsJSONArray.size(); j++){
						JSONObject picJson = picsJSONArray.getJSONObject(j);
						String imgPath = picJson.getString("path");
						picsList.add(imgPath);
					}
				}
				map.put("picsList", picsList);
				JSONObject authorJson = entityJSON.getJSONObject("author");
				String authorNick = authorJson.getString("nick");
				String authorId = authorJson.getString("id");
				String authorImg = authorJson.getString("img");
				
				map.put("authorNick", authorNick);
				map.put("authorId", authorId);
				map.put("authorImg", authorImg);
				
				String tips = JSONObject.fromObject(map).toString();
				FileUtil.addContentToFile(xinniangFilePath, tips);
				
				
				String commentUrl = "http://m.hunliji.com/p/wedding/Home/APICommunityPost/PostListV2?thread_id=" + id + "&page=1&per_page=20";
				JSONObject jjjJsonObject = JSONObject.fromObject(HTTPRULConnect(commentUrl));
				JSONObject dataJsonObject = jjjJsonObject.getJSONObject("data");
				if(dataJsonObject == null || dataJsonObject.isNullObject()){
					continue;
				}
				JSONArray commentJsonArray = dataJsonObject.getJSONArray("list");
				
				Map<String, Object> comMap = new HashMap<String, Object>();
				
				for(int cIndex = 0; cIndex < commentJsonArray.size(); cIndex++){
					JSONObject comItem = commentJsonArray.getJSONObject(cIndex);
					String comId = comItem.getString("id");
					String user_id = comItem.getString("user_id");
					String message = comItem.getString("message");
					String praised_count_item = comItem.getString("praised_count");
					String created_at = comItem.getString("created_at");
					String nick = comItem.getJSONObject("author").getString("nick");
					String img = comItem.getJSONObject("author").getString("img");
					String specialty = comItem.getJSONObject("author").getString("specialty");
					String wedding_status = comItem.getJSONObject("author").getString("wedding_status");
					comMap.put("comId", comId);
					comMap.put("user_id", user_id);
					comMap.put("message", message);
					comMap.put("praised_count_item", praised_count_item);
					comMap.put("created_at", created_at);
					comMap.put("nick", nick);
					comMap.put("img", img);
					comMap.put("specialty", specialty);
					comMap.put("wedding_status", wedding_status);
 					comMap.put("xinniangshuoId", id);
 					List<String> picsList2 = new ArrayList<String>();
					if(!comItem.get("pics").toString().equals("null")){

						JSONArray picsArray = comItem.getJSONArray("pics");
						
						for(int jjj = 0; jjj < picsArray.size(); jjj++){
							picsList2.add(picsArray.getJSONObject(jjj).getString("path"));
						}
						
					}
					
					comMap.put("picsList", picsList2);
					
					
					FileUtil.addContentToFile(xinniangPinglunFilePath, JSONObject.fromObject(comMap).toString());
				}
			}
		}
	}
	
	/**
	 * 商家套餐
	 */
	public static void taocaochuli(){
		for(int pageNo = 1; pageNo <= 81;pageNo++){
			String url = "http://m.hunliji.com/p/wedding"
					+ "/index.php/home/APIMerchant/WorkList?city=4&"
					+ "sort%5Bkey%5D=default&sort%5Border%5D=desc&property=2&commodity_type=0&page=" + pageNo + "&per_page=20";
			String result = "";
			try {
				result = HTTPRULConnect(url);
				JSONArray works = JSONObject.fromObject(result).getJSONArray("works"); 
				for(int index = 0; index < works.size(); index++){
					Map<String, Object> map = new HashMap<String, Object>();
					JSONObject item = works.getJSONObject(index);
					String id = item.getString("id");
					String describe = item.getString("describe");
					String cover_path = item.getString("cover_path");
					String title = item.getString("title");
					String actual_price = item.getString("actual_price");
					String market_price = item.getString("market_price");
					String merchant_id = item.getString("merchant_id");
					
					map.put("id", id);
					map.put("describe", describe);
					map.put("cover_path", cover_path);
					map.put("title", title);
					map.put("actual_price", actual_price);
					map.put("market_price", market_price);
					map.put("merchant_id", merchant_id);
					
					String itemUrl = "http://m.hunliji.com/p/wedding/index.php/home/APIMerchant/GetMealsInfo/id/?id=" + id;
					String itemResult = HTTPRULConnect(itemUrl);
					JSONObject jjjJsonObject = JSONObject.fromObject(itemResult);
					JSONObject workJson = jjjJsonObject.getJSONObject("work");
					String purchase_notes = workJson.getString("purchase_notes");
					
					map.put("purchase_notes", purchase_notes);
					System.err.println(workJson);
					JSONArray work_items = workJson.getJSONArray("work_items");
					System.out.println("work_items=" + work_items);
					if(work_items == null || work_items.size() == 0){
						continue;
					}
					List<Map<String, Object>> workList = new ArrayList<Map<String,Object>>();
					for(int i = 0; i < work_items.size();i++){
						JSONObject workItem = work_items.getJSONObject(i);
						Map<String, Object> itemMap = new HashMap<String, Object>();
						String name = workItem.getString("name");
						String qudescribe = workItem.getString("describe");
						String itemId = workItem.getString("itemId");
						itemMap.put("name", name);
						itemMap.put("qudescribe", qudescribe);
						itemMap.put("itemId", itemId);
						
						JSONArray images = workItem.getJSONArray("images");
						List<String> imgList = new ArrayList<String>();
						if(images != null){
							for(int j = 0; j < images.size(); j++){
								imgList.add(images.getJSONObject(j).getString("image_path"));
							}
							itemMap.put("imgList", imgList);
						}
						
						workList.add(itemMap);
					}
					map.put("workList", workList);
					FileUtil.addContentToFile(anlilistFilePath, JSONObject.fromObject(map).toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
		}
		
	}
	
	/**
	 * 商家评论
	 */
	public static void shangjiapinglun(){
		try {
			List<String> list = FileUtil.readFileContentByLine(shangjiaIdFilePath);
			for(String id : list){
				String url = "http://m.hunliji.com/p/wedding/index.php/home/APIMerchant/detailMerchantV2?mer_id=" + id;
				JSONObject detailJson = JSONObject.fromObject(HTTPRULConnect(url)).getJSONObject("data");
				int comCount = detailJson.getInt("comments_count");//评论数量
				int totalPage = comCount/10;
				if(comCount % 10 == 0){
					totalPage = totalPage + 1;
				}
				for(int page = 1; page <= totalPage; page++){
					url = "http://m.hunliji.com/p/wedding/index.php/Home/APIOrderComment/merchantCommentList?merchant_id=" 
							+ id +"&tag_id=0&page=" + page + "&per_page=10";
					String result = HTTPRULConnect(url);
					JSONArray commentList = JSONObject.fromObject(result).getJSONObject("data").getJSONArray("list");
					for(int i = 0; i < commentList.size(); i++){
						JSONObject com = commentList.getJSONObject(i);
						String comId = com.getString("id");
						String content = com.getString("content");
						String user_id = com.getString("user_id");
						String rating = com.getString("rating");
						String nick = com.getJSONObject("author").getString("nick");
						String avatar = com.getJSONObject("author").getString("avatar");
						JSONArray comment_photos = com.getJSONArray("comment_photos");
						String imgList = "";
						if(comment_photos != null){
							for(int j = 0; j < comment_photos.size(); j++){
								JSONObject img = comment_photos.getJSONObject(j);
								imgList = imgList + "," + img.getString("photo_path");
							}
						}
						
						String insertSQL = "INSERT INTO sl_wedding_plan_comment VALUES ('" + comId
								+ "', '" +nick
								+ "', '" + avatar
								+ "', '1"
								+ "', '5"
								+ "', '" + content
								+ "', '" + imgList
								+ "', '" + user_id
								+ "', '" + id
								+ "', '1511354622');";
						FileUtil.addContentToFile(pinglunListFilePath, insertSQL);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String HTTPRULConnect(String url) throws Exception{
		URL realUrl = new URL(url);
		HttpURLConnection conn = null;
		conn = (HttpURLConnection) realUrl.openConnection();
		conn.setRequestMethod("GET");
		conn.setUseCaches(false);
		conn.setDoOutput(true);
		conn.setReadTimeout(20000);
		conn.setConnectTimeout(20000);
		conn.setInstanceFollowRedirects(false);
		conn.setRequestProperty(
				"user_agent",
				"Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12A365 MicroMessenger/5.4.1 NetType/WIFI");
		int code = conn.getResponseCode();
		System.out.println(code);
		InputStream is = conn.getInputStream();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				is, "UTF-8"));
		StringBuffer buffer = new StringBuffer();
		String line = "";
		while ((line = in.readLine()) != null) {
			buffer.append(line);
		}
		String result = buffer.toString();
		System.out.println(result);
		return result;
	}
	
	
	public static void anlie() throws Exception{
		String url = "http://m.hunliji.com/p/wedding/index.php/home/APIMerchant/GetMealsInfo/id/?id=13871";
		URL realUrl = new URL(url);
		HttpURLConnection conn = null;
		conn = (HttpURLConnection) realUrl.openConnection();
		conn.setRequestMethod("GET");
		conn.setUseCaches(false);
		conn.setDoOutput(true);
		conn.setReadTimeout(20000);
		conn.setConnectTimeout(20000);
		conn.setInstanceFollowRedirects(false);
		conn.setRequestProperty(
				"user_agent",
				"Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12A365 MicroMessenger/5.4.1 NetType/WIFI");
		int code = conn.getResponseCode();
		System.out.println(code);
		InputStream is = conn.getInputStream();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				is, "UTF-8"));
		StringBuffer buffer = new StringBuffer();
		String line = "";
		while ((line = in.readLine()) != null) {
			buffer.append(line);
		}
		String result = buffer.toString();
		System.out.println(result);
		JSONObject workJSON = JSONObject.fromObject(result).getJSONObject("work");
		String id = workJSON.getString("id");
		String title = workJSON.getString("title");
		String cover_path = workJSON.getString("cover_path");
		String actual_price = workJSON.getString("actual_price");
		String market_price = workJSON.getString("market_price");
		String describe = workJSON.getString("describe");
		String purchase_notes = workJSON.getString("purchase_notes");
		String special_choice = workJSON.getString("special_choice");
		String merchantId = workJSON.getJSONObject("merchant").getString("id");
		String merchantName = workJSON.getJSONObject("merchant").getString("name");
		// + "?imageView2/1/w/1920/h/600"
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public static void ttt() throws Exception{
		String quyuArrList[] = quyuArr.split(",");
		for(String q :quyuArrList){
			quyuList.add(q);
		}
		for(int i = 0; i <= 13; i++){
			handle(i);
		}
	}
	/**
	 * 总共13页
	 * @param pageNo
	 * @throws Exception
	 */
	public static void handle(int pageNo) throws Exception{
		String url = "http://m.hunliji.com/p/wedding/index.php"
				+ "/home/APIMerchant/merchantV2?city=4&property=2"
				+ "&category_id=0&area_id=0&sort=default"
				+ "&page=" + pageNo + "&per_page=20&bond_sign=0&is_grade=0";
		URL realUrl = new URL(url);
		HttpURLConnection conn = null;
		conn = (HttpURLConnection) realUrl.openConnection();
		conn.setRequestMethod("GET");
		conn.setUseCaches(false);
		conn.setDoOutput(true);
		conn.setReadTimeout(20000);
		conn.setConnectTimeout(20000);
		conn.setInstanceFollowRedirects(false);
		conn.setRequestProperty(
				"user_agent",
				"Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12A365 MicroMessenger/5.4.1 NetType/WIFI");
		int code = conn.getResponseCode();
		System.out.println(code);
		InputStream is = conn.getInputStream();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				is, "UTF-8"));
		StringBuffer buffer = new StringBuffer();
		String line = "";
		while ((line = in.readLine()) != null) {
			buffer.append(line);
		}
		String result = buffer.toString();
		System.out.println(result);
		JSONObject listJson = JSONObject.fromObject(result);
		JSONObject dataJson = listJson.getJSONObject("data");
		JSONObject normal_merchantJson = dataJson.getJSONObject("normal_merchant");
		JSONArray arrayList = normal_merchantJson.getJSONArray("list");
		System.out.println(arrayList.toString());
		for(int i = 0; i < arrayList.size();i++){
			JSONObject item = arrayList.getJSONObject(i);
			String id = item.getString("id");
			String name = item.getString("name");
			String address = item.getString("address");
			String des = item.getString("des");
			String email = item.getString("email");
			String shop_phone = item.getString("shop_phone");
			String phone = item.getString("phone");
			String active_works_pcount = item.getString("active_works_pcount");//套餐
			String active_cases_pcount = item.getString("active_cases_pcount");//案例
			String feed_count = item.getString("feed_count");
			String daodianli = "";
			try {
				daodianli = item.getJSONObject("privilege").getJSONObject("items").getJSONObject("shop_gift").getString("value");
			} catch (Exception e) {
				System.err.println("无到店里" );
				daodianli = "";
			}
			
			String logo_path = item.getString("logo_path");
			String score = item.getJSONObject("merchant_comment").getString("score");
			
			String cover_path = item.getString("cover_path");
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			System.err.println(item.get("real_photos"));
			if(item.get("real_photos") != null
					&& !item.get("real_photos").toString().equals("null")){
				JSONArray real_photosList = item.getJSONArray("real_photos");
				List<String> imgList = new ArrayList<String>();
				for(int j = 0; j < real_photosList.size(); j++){
					JSONObject photo = real_photosList.getJSONObject(j);
					String img = photo.getString("img");
					imgList.add(img);
				}
				map.put("imgList", imgList);
			}
			if(StringUtil.isEmpty(cover_path)){
				cover_path = logo_path;
			}
			map.put("id", id);
			map.put("name", name);
			map.put("address", address);
			map.put("des", des);
			map.put("email", email);
			map.put("shop_phone", shop_phone);
			map.put("phone", phone);
			map.put("active_works_pcount", active_works_pcount);
			map.put("active_cases_pcount", active_cases_pcount);
			map.put("daodianli", daodianli);
			map.put("logo_path", logo_path + "?imageView2/1/w/230/h/180");
			map.put("score", score);
			map.put("cover_path", cover_path + "?imageView2/1/w/1920/h/600");
			map.put("feed_count", feed_count);
			String content = JSONObject.fromObject(map).toString();
//			String filePath = "e:/json/" + id + ".json";
			FileUtil.addContentToFile(shangjiaFilePath, content); 
			FileUtil.addContentToFile(shangjiaIdFilePath, id);
			int regionId = 0;
			String quyuId = "其他";
			int rrId = 19;
			for(String quString : quyuList){
				if(address.contains(quString)){
					quyuId = quString;
					rrId = regionId + 1;
					break;
				}
				regionId++;
			}
			String sql = "INSERT INTO `sl_wedding_plan` VALUES ('" + 
			id + "', '" 
			+ name + "', '"
			+ map.get("logo_path") +  "', '"
			+ map.get("cover_path") + "', '" 
			+ map.get("cover_path") + "', '" 
			+ address + "', '" 
			+ StringUtil.replaceBlank(des) + "', '" 
			+ active_cases_pcount + "', '" 
			+ active_works_pcount + "', '" + quyuId + " ', '5', '" 
			+ feed_count + "', '0', '0', '0', '" + rrId + "', '1', '1', '1512903686');";
			
			FileUtil.addContentToFile(anliFilePath, sql); 
		}
	}
}
