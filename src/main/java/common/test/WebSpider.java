package common.test;
import java.io.BufferedReader;  
import java.io.FileWriter;  
import java.io.IOException;  
import java.io.InputStreamReader;  
import java.io.PrintWriter;  
import java.net.MalformedURLException;  
import java.net.URL;  
import java.net.URLConnection;  
import java.util.regex.Matcher;  
import java.util.regex.Pattern;  
import java.io.IOException;  
import org.apache.http.HttpEntity;  
import org.apache.http.HttpResponse;  
import org.apache.http.client.ClientProtocolException;  
import org.apache.http.client.HttpClient;  
import org.apache.http.client.methods.HttpGet;  
import org.apache.http.impl.client.DefaultHttpClient;  
import org.apache.http.util.EntityUtils;  

public class WebSpider {  
	public static String getContentFormUrl(String url) {  
        /* 实例化一个HttpClient客户端 */  
        HttpClient client = new DefaultHttpClient();  
        HttpGet getHttp = new HttpGet(url);  
  
        String content = null;  
  
        HttpResponse response;  
        try {  
            /* 获得信息载体 */  
            response = client.execute(getHttp);  
            HttpEntity entity = response.getEntity();  
  
            VisitedUrlQueue.addElem(url);  
  
            if (entity != null) {  
                /* 转化为文本信息 */  
                content = EntityUtils.toString(entity);  
  
                /* 判断是否符合下载网页源代码到本地的条件 */  
                if (FunctionUtils.isCreateFile(url)  
                        && FunctionUtils.isHasGoalContent(content) != -1) {  
                    FunctionUtils.createFile(  
                            FunctionUtils.getGoalContent(content), url);  
                }  
            }  
  
        } catch (ClientProtocolException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        } finally {  
            client.getConnectionManager().shutdown();  
        }  
  
        return content;  
    }  
    public static void main(String[] args) {  
    	String str = getContentFormUrl("https://45.60.15.111/Html/84/10691.html");
    	System.out.println(str);
//        URL url = null;  
//        URLConnection urlconn = null;  
//        BufferedReader br = null;  
//        PrintWriter pw = null;  
//        String regex = "http://[\\w+\\.?/?]+\\.[A-Za-z]+";  
//        Pattern p = Pattern.compile(regex);  
//        try {  
//            url = new URL("https://www.296hk.com");  
//            urlconn = url.openConnection();  
//            pw = new PrintWriter(new FileWriter("e:/url.txt"), true);//这里我们把收集到的链接存储在了E盘底下的一个叫做url的txt文件中  
//            br = new BufferedReader(new InputStreamReader(  
//                    urlconn.getInputStream()));  
//            String buf = null;  
//            while ((buf = br.readLine()) != null) {  
////                Matcher buf_m = p.matcher(buf);  
////                while (buf_m.find()) {  
////                    pw.println(buf_m.group());  
////                }  
//            	 pw.println(buf);  
//            }  
//            System.out.println("获取成功！");  
//        } catch (MalformedURLException e) {  
//            e.printStackTrace();  
//        } catch (IOException e) {  
//            e.printStackTrace();  
//        } finally {  
//            try {  
//                br.close();  
//            } catch (IOException e) {  
//                e.printStackTrace();  
//            }  
//            pw.close();  
//        }  
    }  
}  