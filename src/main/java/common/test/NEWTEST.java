package common.test;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import common.timer.util.AikucunPostUtil;

public class NEWTEST {
	public static void main(String[] args) throws Exception {
		// String name = "78001、 LILY/大衣-¥149\n尺码
		// XS(150/78A)/S(155/80A)/M(160/84A)/L(165/88A)/XL(170/92A) \n款式
		// 长袖翻领大衣【45%羊毛/桔红/市场价1599】\n款号 115420F1503906";
		// String arr[] = name.split("\\n");
		// System.out.println(arr[0]);
		// System.out.println(arr[1]);
		// System.out.println(arr[2]);
		// System.out.println(arr[3]);
		test3();
//		 String result = AikucunPostUtil.PostRequest("https://miniapp.rrrdai.com/miniapp/activity/activity?viplevel=1&createtime=2018-06-27%2012:22:56&unionid=oXZ2O0qj17_9JB3wzWqLpi4tIV4s", "");
//		 System.err.println(result); 
//		 byte[] b = result.getBytes("GBK"); 
//		  String n = new String(b,"GBK"); 
//		  System.err.println(n);
	}

	static void test3() throws Exception {
		String getUrl = "https://miniapp.rrrdai.com/miniapp/activity/preActivity?viplevel=1&createtime=1530073376000&unionid=oXZ2O0qj17_9JB3wzWqLpi4tIV4s";
		System.out.println(getUrl);
		URL realUrl = new URL(getUrl);
		HttpURLConnection conn = null;
		conn = (HttpURLConnection) realUrl.openConnection();
		conn.setRequestMethod("POST");
		conn.setUseCaches(false);
		conn.setDoOutput(true);
		conn.setReadTimeout(30000);
		conn.setConnectTimeout(30000);
		conn.setInstanceFollowRedirects(false);
		conn.setRequestProperty("Accept", "*");
		conn.setRequestProperty("Accept-Encoding", "br, gzip, deflate,compress");
		conn.setRequestProperty("Accept-Language", "zh-cn");
		conn.setRequestProperty("Connection", "keep-alive");
		conn.setRequestProperty("User-Agent",
				"Mozilla/5.0 (iPhone; CPU iPhone OS 11_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15F79 MicroMessenger/6.6.7 NetType/WIFI Language/zh_CN");
		int code = conn.getResponseCode();
		System.err.println(code);
		if (code == 200) {
			InputStream is = conn.getInputStream();
			BufferedInputStream bis = new BufferedInputStream(is);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(bis));
			StringBuffer buffer = new StringBuffer();
			String line = "";
			while ((line = in.readLine()) != null) {
				buffer.append(line);
			}
			String result = buffer.toString();
			System.out.println(result);

		} else {
			System.out.println("失败");
		}
	}

	static void test2() throws Exception {
		String getUrl = "https://miniapp.rrrdai.com/miniapp/product/getList?liveid=2c9089c2646863c401646e202f9264d1&pageSize=1000&pageNo=0&unionid=oXZ2O0qj17_9JB3wzWqLpi4tIV4s&entrance=1";
		System.out.println(getUrl);
		URL realUrl = new URL(getUrl);
		HttpURLConnection conn = null;
		conn = (HttpURLConnection) realUrl.openConnection();
		conn.setRequestMethod("POST");
		conn.setUseCaches(false);
		conn.setDoOutput(true);
		conn.setReadTimeout(30000);
		conn.setConnectTimeout(30000);
		conn.setInstanceFollowRedirects(false);
		conn.setRequestProperty("Accept", "*");
		conn.setRequestProperty("Accept-Encoding", "br, gzip, deflate");
		conn.setRequestProperty("Accept-Language", "zh-cn");
		conn.setRequestProperty("Connection", "keep-alive");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("User-Agent",
				"Mozilla/5.0 (iPhone; CPU iPhone OS 11_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15F79 MicroMessenger/6.6.7 NetType/WIFI Language/zh_CN");
		int code = conn.getResponseCode();
		System.err.println(code);
		if (code == 200) {
			InputStream is = conn.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(is, "GB2312"));
			StringBuffer buffer = new StringBuffer();
			String line = "";
			while ((line = in.readLine()) != null) {
				buffer.append(line);
			}
			String result = buffer.toString();
			System.out.println(result);

		} else {
			System.out.println("失败");
		}
	}

	public static String PostRequest(String URL, String obj) {
		String jsonString = "";
		try {
			// 创建连接
			URL url = new URL(URL);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true); 
			connection.setRequestMethod("POST"); // 设置请求方法
			connection.setRequestProperty("Charsert", "UTF-8"); // 设置请求编码
			connection.setUseCaches(false);
			connection.setInstanceFollowRedirects(true);
			connection.setRequestProperty("Content-Type", "application/json");

			connection.connect();

			// POST请求
			PrintWriter out = new PrintWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
			out.println(obj);
			out.flush();
			out.close();

			// 读取响应
			if (connection.getResponseCode() == 200) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String lines;
				StringBuffer sb = new StringBuffer("");
				while ((lines = reader.readLine()) != null) {
					lines = new String(lines.getBytes(), "UTF-8");
					sb.append(lines);
				}
				jsonString = sb.toString();

				reader.close();
			} // 返回值为200输出正确的响应信息

			if (connection.getResponseCode() == 400) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
				String lines;
				StringBuffer sb = new StringBuffer("");
				while ((lines = reader.readLine()) != null) {
					lines = new String(lines.getBytes(), "UTF-8");
					sb.append(lines);
				}
				jsonString = sb.toString();
				reader.close();
			} // 返回值错误，输出错误的返回信息
				// 断开连接
			connection.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonString;
	}

	static void test1() throws Exception {
		String getUrl = "https://miniapp.rrrdai.com/miniapp/activity/activity?viplevel=1&createtime=2018-08-06%2012:22:56&unionid=oXZ2O0qj17_9JB3wzWqLpi4tIV4s";
		System.out.println(getUrl);
		URL realUrl = new URL(getUrl);
		HttpURLConnection conn = null;
		conn = (HttpURLConnection) realUrl.openConnection();
		conn.setRequestMethod("POST");
		conn.setUseCaches(false);
		conn.setDoOutput(true);
		conn.setReadTimeout(30000);
		conn.setConnectTimeout(30000);
		conn.setInstanceFollowRedirects(false);
		conn.setRequestProperty("Accept", "*");
		conn.setRequestProperty("Accept-Encoding", "br, gzip, deflate");
		conn.setRequestProperty("Accept-Language", "zh-cn");
		conn.setRequestProperty("Charsert", "UTF-8"); // 设置请求编码

		conn.setRequestProperty("Connection", "keep-alive");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("User-Agent",
				"Mozilla/5.0 (iPhone; CPU iPhone OS 11_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15F79 MicroMessenger/6.6.7 NetType/WIFI Language/zh_CN");

		conn.connect();
		int code = conn.getResponseCode();
		String obj = null;
		PrintWriter out = new PrintWriter(new OutputStreamWriter(conn.getOutputStream(), "utf-8"));
		out.println(obj);
		System.err.println(code);
		System.err.println(obj);
		if (code == 200) {
			InputStream is = conn.getInputStream();
			// System.out.println(toString(is));
			BufferedReader in = new BufferedReader(new InputStreamReader(is, "gbk"));
			StringBuffer buffer = new StringBuffer();
			String line = "";
			while ((line = in.readLine()) != null) {
				buffer.append(line);
			}
			String result = buffer.toString();
			System.out.println(result);

		} else {
			System.out.println("失败");
		}
	}

	public static String toString(InputStream is) {

		try {
			ByteArrayOutputStream boa = new ByteArrayOutputStream();
			int len = 0;
			byte[] buffer = new byte[1024];

			while ((len = is.read(buffer)) != -1) {
				boa.write(buffer, 0, len);
			}
			is.close();
			boa.close();
			byte[] result = boa.toByteArray();

			String temp = new String(result);

			// 识别编码
			if (temp.contains("utf-8")) {
				return new String(result, "utf-8");
			} else if (temp.contains("gb2312")) {
				return new String(result, "gb2312");
			} else {
				return new String(result, "utf-8");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
