package common.test;
/**
 * @author liuyazhuang
 *
 */
public class UrlDataHanding implements Runnable {
	/**
	 * 下载对应页面并分析出页面对应的URL放在未访问队列中。
	 * 
	 * @param url
	 */
	public void dataHanding(String url) {
		String content = DownloadPage.getContentFormUrl(url);
		System.out.println(content);
		HrefOfPage.getHrefOfContent(content);
	}

	public void run() {
		while (!UrlQueue.isEmpty()) {
			dataHanding(UrlQueue.outElem());
		}
	}
}