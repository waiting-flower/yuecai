package common.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.ModelAndView;


public class BaseController {
	
	@Value("${templatePath}")
	protected String templatePath;
	
	/**
	 * 非法的请求参数返回页面
	 * @param request
	 * @return
	 */
	public ModelAndView errorParam(HttpServletRequest request){
		return new ModelAndView("/errorParam");
	}
	protected String basePath = "";
	protected String adminBasePath;
	/**
	 * 商城wap端
	 * @param request
	 */
	protected void initShopWapController(HttpServletRequest request){
		this.basePath = request.getContextPath() + "/web/";
		this.adminBasePath = request.getContextPath() + "/wap/";
		request.setAttribute("basePath", basePath);
		request.setAttribute("skinPath", adminBasePath);
		this.templatePath = "wap/html/";
	}
}
