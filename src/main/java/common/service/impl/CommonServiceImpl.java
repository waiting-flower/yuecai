package common.service.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.dao.CommonDao;
import common.service.CommonService;
import common.util.Constant;
import modules.system.entity.UserEntity;

@Service("commonService")
@Transactional(readOnly = false,rollbackFor = Exception.class)  
public class CommonServiceImpl implements CommonService{
	
	protected CommonDao commonDao;
	
	public <T> Serializable save(T entity) {
		return getCommonDao().save(entity);
	}

	
	public <T> void saveOrUpdate(T entity) {
		getCommonDao().saveOrUpdate(entity);

	}

	
	public <T> void delete(T entity) {
		getCommonDao().delete(entity);

	}

	/**
	 * 删除实体集合
	 * 
	 * @param <T>
	 * @param entities
	 */
	public <T> void deleteAllEntitie(Collection<T> entities) {
		getCommonDao().deleteAllEntitie(entities);
	}

	/**
	 * 根据实体名获取对象
	 */
	public <T> T get(Class<T> class1, Serializable id) {
		return (T) getCommonDao().get(class1, id);
	}

	/**
	 * 根据实体名返回全部对象
	 * 
	 * @param <T>
	 * @param hql
	 * @param size
	 * @return
	 */
	public <T> List<T> getList(Class clas) {
		return getCommonDao().loadAll(clas);
	}

	/**
	 * 根据实体名获取对象
	 */
	public <T> T getEntity(Class entityName, Serializable id) {
		return (T) getCommonDao().getEntity(entityName, id);
	}

	/**
	 * 根据实体名称和字段名称和字段值获取唯一记录
	 * 
	 * @param <T>
	 * @param entityClass
	 * @param propertyName
	 * @param value
	 * @return
	 */
	public <T> T findUniqueByProperty(Class<T> entityClass,
			String propertyName, Object value) {
		return (T) getCommonDao().findUniqueByProperty(entityClass, propertyName, value);
	}

	/**
	 * 按属性查找对象列表.
	 */
	public <T> List<T> findByProperty(Class<T> entityClass,
			String propertyName, Object value) {

		return getCommonDao().findByProperty(entityClass, propertyName, value);
	}
	@Override
	public <T> void batchSave(List<T> entitys) {
		getCommonDao().batchSave(entitys);
		
	}
	/**
	 * 加载全部实体
	 * 
	 * @param <T>
	 * @param entityClass
	 * @return
	 */
	public <T> List<T> loadAll(final Class<T> entityClass) {
		return getCommonDao().loadAll(entityClass);
	}

	/**
	 * 删除实体主键ID删除对象
	 * 
	 * @param <T>
	 * @param entities
	 */
	public <T> void deleteEntityById(Class entityName, Serializable id) {
		getCommonDao().deleteEntityById(entityName, id);
	}

	/**
	 * 更新指定的实体
	 * 
	 * @param <T>
	 * @param pojo
	 */
	public <T> void updateEntitie(T pojo) {
		getCommonDao().updateEntitie(pojo);

	}

	
	public CommonDao getCommonDao() {
		return commonDao;
	}

	@Resource
	public void setCommonDao(CommonDao commonDao) {
		this.commonDao = commonDao;
	}


	@Override
	public <T> List<T> findByQueryString(String hql) {
		return commonDao.findByQueryString(hql);
	}


	@Override
	public <T> List<T> findHql(String hql, Object... param) {
		return this.commonDao.findHql(hql, param);
	}


	@Override
	public UserEntity getLoginUser(HttpServletRequest request) {
		UserEntity user = (UserEntity) request.getSession().getAttribute(Constant.ADMIN_USER_KEY);
		return user;
	}
	
	

}
