package common.timer.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


/**
 * 爬取爱库存商品的任务对象
 * @author Administrator
 *
 */
@Entity
@Table(name = "shop_sys_goods_task", schema = "")
public class GoodsTaskEntity{
	private Long id;
	private Date createDate;
	private Date updateDate;
	private String aikucunId;
	private Long actionId;
	private Integer executeNum;
	private Integer goodsNum;// 获取到的商品数量
	private String taskState;//0未执行  1：已执行
	private Date beginTime;//任务执行开始时间
	private Date endTime;//任务执行结束时间
	public String getAikucunId() {
		return aikucunId;
	}
	public void setAikucunId(String aikucunId) {
		this.aikucunId = aikucunId;
	}
	public Integer getGoodsNum() {
		return goodsNum;
	}
	public void setGoodsNum(Integer goodsNum) {
		this.goodsNum = goodsNum;
	}
	public String getTaskState() {
		return taskState;
	}
	public void setTaskState(String taskState) {
		this.taskState = taskState;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Long getActionId() {
		return actionId;
	}
	public void setActionId(Long actionId) {
		this.actionId = actionId;
	}
	public Integer getExecuteNum() {
		return executeNum;
	}
	public void setExecuteNum(Integer executeNum) {
		this.executeNum = executeNum;
	}
	@Id
	@GeneratedValue(generator = "paymentableGenerator") 
	@GenericGenerator(name = "paymentableGenerator", strategy = "increment")
	@Column(name ="ID",nullable=false,length=32)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
}
