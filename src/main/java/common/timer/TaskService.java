package common.timer;

import common.service.CommonService;

public interface TaskService extends CommonService{
	public void work();
}
