package common.timer.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AikucunPostUtil {
	/**
	 * 抓取url，并返回无乱码的字符串
	 * @param URL
	 * @param obj
	 * @return
	 */
	public static String PostRequest(String URL, String obj) {
		String jsonString = "";
		try {
			// 创建连接
			URL url = new URL(URL);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST"); // 设置请求方法
//			connection.setRequestProperty("Charsert", "UTF-8"); // 设置请求编码
//			connection.setUseCaches(false);
//			connection.setInstanceFollowRedirects(true);
//			connection.setRequestProperty("Content-Type", "application/json");

			connection.connect();

			// POST请求
			PrintWriter out = new PrintWriter(new OutputStreamWriter(connection.getOutputStream()));
			out.println(obj);
			out.flush();
			out.close();

			// 读取响应
			if (connection.getResponseCode() == 200) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String lines;
				StringBuffer sb = new StringBuffer("");
				while ((lines = reader.readLine()) != null) {
					lines = new String(lines.getBytes(), "utf-8");
					sb.append(lines);
				}
				jsonString = sb.toString();

				reader.close();
			} // 返回值为200输出正确的响应信息

			if (connection.getResponseCode() == 400) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
				String lines;
				StringBuffer sb = new StringBuffer("");
				while ((lines = reader.readLine()) != null) {
					lines = new String(lines.getBytes(), "utf-8");
					sb.append(lines);
				}
				jsonString = sb.toString();
				reader.close();
			} // 返回值错误，输出错误的返回信息
				// 断开连接
			connection.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonString;
	}
}
