package common.system.listener;


import java.math.BigDecimal;
import java.util.List;

import javax.servlet.ServletContextEvent;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import common.service.CommonService;


/**
 * 系统初始化监听器,在系统启动时运行,进行一些初始化工作
 * @author laien
 *
 */
public class InitListener  implements javax.servlet.ServletContextListener {

	
	public void contextDestroyed(ServletContextEvent arg0) {
		
	}

	/**
	 * 系统启动时候对系统进行一些初始化的操作，监听开始
	 */
	public void contextInitialized(ServletContextEvent event) {
		System.out.println("init listenter");
	}

}
