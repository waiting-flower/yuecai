package common.entity;

import java.util.Map;
/**
 * 用于ajax返回json的实体类
 * @author lpf0
 *
 */
public class JsonEntity {
	/**
	 * 返回的json  ，用map进行存储
	 */
	private Map<String, Object> map;

	public void setJsonValue(String key,Object object){
		map.put(key, object);
	}
	
	
	public Map<String, Object> getMap() {
		return map;
	}

	public void setMap(Map<String, Object> map) {
		this.map = map;
	} 
}
