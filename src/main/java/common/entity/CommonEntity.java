package common.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;

import common.util.DateUtil;
@MappedSuperclass
public abstract class CommonEntity {
	private Long id;
	private Date createDate;//创建时间
	private Date updateDate;//最后更新时间
	private String status;//当前状态，有效无效
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator") 
	@GenericGenerator(name = "paymentableGenerator", strategy = "increment")
	@Column(name ="ID",nullable=false,length=32)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
 
	public void init() {
		this.setCreateDate(DateUtil.getCurrentTime());
		this.setUpdateDate(DateUtil.getCurrentTime());
		this.setStatus("1");
	}
}

