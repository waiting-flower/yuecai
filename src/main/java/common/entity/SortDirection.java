package common.entity;
/**
 * jquery datatable 排序定义
 * asc 升序<br>
 * desc 降序
 * @author lpf0
 *
 */
public enum SortDirection {
	/**
	 * 升序
	 */
	asc, 
	/**
	 * 降序
	 */
	desc
}
