var isEditPic = true;
var currPicId = "";
var editCateIndex;
var nowPicCateId = "";
var imgList = [];
var parentCateId = "";
$(function() {
	$(".ant-menu-item").click(function(){
		//顶部的tab菜单点击事件
		$(".ant-menu-item").removeClass("ant-menu-item-selected");
		$(this).addClass("ant-menu-item-selected");
		var sitem = $(this).attr("nz-menu-item");
		if(sitem == "self"){
			$(".content").css("width","700px");
			$("#subMenuCate_div").hide();
			$("#subMenuCate_div").empty();
			$(".lpf_normal").show(); 
			initUserPicCate(); 
			initPicData();  
			$(".left_menu_ul").addClass("normal").removeClass("system");
		}
		else if(sitem == "sys"){
			$(".lpf_footer .ng-star-inserted").hide();
			initSysPicCate(); 
			$(".left_menu_ul").addClass("system").removeClass("normal");
			initSysPicData();  
		}
	});
	//获取系统图片
	var initSysPicData = function(cateId){
		$("#tupian_box").empty();
		$.getJSON(reqPath + "web/file/getFileListByCate",{
			parentCateId : parentCateId,
			cateId : cateId
		},function(json){
			if (json.list.length < 1) {
                $("#noPic_tips").show();
            } else {
                $("#noPic_tips").hide();
                $.each(json.list, function(i, item) {
                    var ui = '<div itemId="' + item.id + '"  class="img_wrap normal">' + '<img src="' + item.url + '">' + '</div> ';
                    var G = $(ui);
                    $("#tupian_box").append(G);
                });
            }
		});
	}
	//获取系统图片分类
	var initSysPicCate = function(){
		$.getJSON(reqPath + "web/file/getAllFileCateList",function(json){
			console.log(json);
			$(".left_menu_ul").empty();
			$.each(json.list,function(i,item){
				var classZ = "";
                if (i == 0) {
                    classZ = "active";
                }
				var ui = '<li itemId="' + item.id + '" class="' + classZ + '"><span>' + item.name + '</span></li>';
				$(".left_menu_ul").append(ui); 
			}); 
		});
	}
	//获取用户的图片分类
    var initUserPicCate = function() {
        $.post(reqPath + "web/file/workUserFileCate", {}, function(json) {
            console.log(json);
            $(".left_menu_ul").empty();
            $.each(json.list, function(i, item) {
                var ui = "";
                if (item.orderNum == 1) {
                    ui = '<li class="ng-star-inserted" itemId="' + item.id + '">' 
                    + '<i itemId="' + item.id + '" name="' + item.name 
                    + '" class="anticon anticon-edit edit-cate fa fa-pencil"></i>' 
                    + '<span>' + item.name + '</span>' 
                    + '<i class="anticon anticon-close-circle delete-cate fa fa-times"></i>' 
                    + '</li>';
                } else {
                    var classZ = "";
                    if (i == 0) {
                        classZ = "active";
                    }
                    ui = '<li itemId="' + item.id + '" class="' + classZ + '"><span>' + item.name + '</span></li>';
                }
                var G = $(ui);
                G.hover(function() {
                    $(this).find(".anticon").show();
                }, function() {
                    $(this).find(".anticon").hide();
                });
                G.find(".anticon-edit").click(function(event) {
                	event.stopPropagation();
                    var itemId = $(this).attr("itemId");
                    var name = $(this).attr("name");
                    $("#pic_cateName").val(name);
                    $("#pic_cateName").attr("itemId", itemId);
                    editCateIndex = layer.open({
                        title: "编辑分组",
                        area: ['400px', '160px'],
                        type: 1,
                        content: $('#editCateDiv')//这里content是一个DOM，注意：最好该元素要存放在body最外层，否则可能被其它的相对元素所影响
                    });
                });
                G.find(".anticon-close-circle").click(function(event) {
                	event.stopPropagation();
                    layer.confirm("确定要删除当前组？(组内图片不会删除)", {
                        icon: 3,
                        title: '温馨提示'
                    }, function(index) {
                        $.post(reqPath + "web/file/deleteUserFileCate", {
                            id: item.id
                        }, function(json) {
                            console.log(json);
                            if (json.code == 0) {
                                $(".left_menu_ul").find("li[itemId='" + item.id + "']").remove();
                                layer.close(index);
                            }
                        }, "json");
                    });
                });
                $(".left_menu_ul").append(G);
            });
        }, "json");
    }
    $("#btnCanclePicCate").click(function() {
        layer.close(editCateIndex);
    });
    $("#btnSavePicCate").click(function() {
        var name = $("#pic_cateName").val();
        var id = $("#pic_cateName").attr("itemId");
        $.post(reqPath + "web/file/saveUserFileCate", {
            id: id,
            name: name
        }, function(json) {
            if (json.code == "0") {
                if (id == "") {
                    var ui = '<li class="ng-star-inserted" itemId="' + json.id + '">' + '<i itemId="' + json.id + '" name="' + name + '" class="anticon anticon-edit edit-cate fa fa-pencil"></i>' + '<span>' + name + '</span>' + '<i class="anticon anticon-close-circle delete-cate fa fa-times"></i>' + '</li>';
                    var G = $(ui);
                    G.hover(function() {
                        $(this).find(".anticon").show();
                    }, function() {
                        $(this).find(".anticon").hide();
                    });
                    G.find(".anticon-edit").click(function(event) {
                    	event.stopPropagation();
                        var itemId = $(this).attr("itemId");
                        var name = $(this).attr("name");
                        $("#pic_cateName").val(name);
                        $("#pic_cateName").attr("itemId", itemId);
                        editCateIndex = layer.open({
                            title: "编辑分组",
                            area: ['400px', '160px'],
                            type: 1,
                            content: $('#editCateDiv')//这里content是一个DOM，注意：最好该元素要存放在body最外层，否则可能被其它的相对元素所影响
                        });
                    });
                    var ttId = json.id;
                    G.find(".anticon-close-circle").click(function(event) {
                    	event.stopPropagation(); 
                        layer.confirm("确定要删除当前组？(组内图片不会删除)", {
                            icon: 3,
                            title: '温馨提示'
                        }, function(index) {
                            $.post(reqPath + "web/file/deleteUserFileCate", {
                                id: ttId
                            }, function(json) {
                                console.log(json);
                                if (json.code == 0) {
                                    $(".left_menu_ul").find("li[itemId='" + ttId + "']").remove();
                                    layer.close(index);
                                }
                            }, "json");
                        });
                    });
                    $(".left_menu_ul").append(G);
                    $("#btnCanclePicCate").click();
                } else {
                    layer.msg("修改成功", {
                        time: 1
                    }, function() {
                        $(".left_menu_ul").find("i[itemId='" + id + "']").attr("name", name);
                        $(".left_menu_ul").find("i[itemId='" + id + "']").parent().find("span").html(name);
                        $("#btnCanclePicCate").click();
                    });
                }

            } else if (json.code == 1) {
                layer.msg(json.msg);
            }
        }, "json");
    });
    $("#addNewCate").click(function() {
        $("#pic_cateName").val("");
        $("#pic_cateName").attr("itemId", "");
        editCateIndex = layer.open({
            title: "新增分组",
            area: ['400px', '160px'],
            type: 1,
            content: $('#editCateDiv')//这里content是一个DOM，注意：最好该元素要存放在body最外层，否则可能被其它的相对元素所影响
        });
    });
    initUserPicCate();

    var initPicData = function(cateId) {
    	$("#tupian_box").empty();  
        $.getJSON(reqPath + "web/file/getFileListByUserCate", {
            cateId: cateId
        }, function(json) {
            imgList = json.imgList;
            if (json.list.length < 1) {
                $("#noPic_tips").show();
            } else {
                $("#noPic_tips").hide();
                $.each(json.list, function(i, item) {
                    var ui = '<div itemId="' + item.id + '"  class="img_wrap normal">' + '<img src="' + item.url + '">' + '<span class="delete" itemId="' + item.id + '">删除</span>' + '</div> ';
                    var G = $(ui);
                    G.find(".delete").click(function(event) {
                    	event.stopPropagation();
                        var itemId = $(this).attr("itemId");
                        layer.confirm("是否确认删除该图片", function(index) {
                            $.getJSON(reqPath + "web/file/deleteUserFile", {
                                id: itemId
                            }, function(result) {
                                if (result.code == 0) {
                                    layer.msg("删除成功", {
                                        time: 600
                                    }, function() {
                                        G.remove();
                                        if ($("#tupian_box").find(".img_wrap").length < 1) {
                                            $("#noPic_tips").show();
                                        }
                                    });
                                }
                            })
                        });
                    });
                    $("#tupian_box").append(G);
                });
            }
        });
    };
    initPicData();

    $("#batchHandleCate").click(function() {
        //批量处理
        $.post(reqPath + "web/file/workUserFileCate", {}, function(json) {
            $("#moveCateSelect").empty();
            $.each(json.list, function(i, item) {
                $("#moveCateSelect").append("<option value='" + item.id + "'>" + item.name + "</option>");
            });
        }, "json");
        $(".lpf_footer .lpf_normal").hide();
        $("#piliangChuliBox").show();

        $("#tupian_box").find(".img_wrap").removeClass("normal").addClass("piliangChuli");
    });
     
    //左侧的分类点击事件【用户图片库的时候】
    $("body").on('click', ".left_menu_ul.normal li", function() {
    	 $(".left_menu_ul li").removeClass("active");
         $(this).addClass("active");
         $("#tupian_box").html("");
         $("#nowPicCateId").val($(this).attr("itemId"));
         initPicData($(this).attr("itemId"));
    });
    
    //左侧的分类点击事件【系统图片库的时候】
    $("body").on('click', ".left_menu_ul.system li", function() {
    	 $(".left_menu_ul li").removeClass("active");
         $(this).addClass("active");
         $("#tupian_box").empty();
         var itemId = $(this).attr("itemId");
         if(itemId == ""){
        	 $(".content").css("width","700px");
        	 $("#subMenuCate_div").hide();
        	 parentCateId = ""; 
        	 initSysPicData("");
         } 
         else{
        	 $(".content").css("width","600px");
        	 initSysPicSubCate($(this).attr("itemId"));
        	 $("#subMenuCate_div").show(); 
        	 parentCateId = itemId;
        	 initSysPicData(""); 
         } 
         //去获取二级分类
    });
    //二级分类点击事件
    $("body").on('click', "#subMenuCate_div li", function() {
   	 	$("#subMenuCate_div li").removeClass("active");
        $(this).addClass("active");
        $("#tupian_box").empty();
        var itemId = $(this).attr("itemId");
        $("#tupian_box").html("");
        initSysPicData($(this).attr("itemId"));
    });
    
    var initSysPicSubCate = function(cateId){
    	//获取系统图片分类的二级分类
		$.getJSON(reqPath + "web/file/getFileCateListByParent",{cateId : cateId},function(json){
			console.log(json);
			$("#subMenuCate_div").empty();
			$.each(json.list,function(i,item){
				var classZ = "";
                if (i == 0) {
                    classZ = "active";
                }
				var ui = '<li itemId="' + item.id + '" class="' + classZ + '"><span>' + item.name + '</span></li>';
				$("#subMenuCate_div").append(ui); 
			});  
		});
	}
    
    $("#cancleCropImg").click(function(){
    	//关闭裁剪框
    	$("#tupiancaijian_box").hide(); 
    });
    $(".cancleCropImg").click(function(){
    	//关闭裁剪框
    	$("#tupiancaijian_box").hide(); 
    });
    
    //改变裁剪比例操作 
    $("input:radio[name='crop_rate']").on('ifChecked', function(event){
        var cropRate = $(this).val();
        if(cropRate == "1"){
        	//原始比例 
        	if(imgWidth < imgHeight){ 
    			var dWidth = imgWidth/imgHeight*450; 
    			jcropApi.setOptions({
               		aspectRatio : dWidth/450,
               		setSelect: [0,0,dWidth,450]
        		}); 
    		}
    		else{
    			jcropApi.setOptions({ 
               		aspectRatio : 450,
               		setSelect: [0,0,450,450*imgHeight/imgWidth]
        		});  
    		}
        }
        else if(cropRate == "2"){//1:1
        	jcropApi.setOptions({
           		aspectRatio : 1
    		});  
        }
        else if(cropRate == "3"){//4:3
        	jcropApi.setOptions({
           		aspectRatio : 4/3
    		}); 
        }
        else if(cropRate == "4"){//3:4
        	jcropApi.setOptions({
           		aspectRatio : 3/4
    		});
        }
        else if(cropRate == "5"){//16:9
        	jcropApi.setOptions({
           		aspectRatio : 16/9
    		});
        }
        else if(cropRate == "6"){//自由
        	jcropApi.setOptions({
           		aspectRatio : 0
    		});
        } 
    }); 
    var imgWidth = 0;
    var imgHeight = 0;
    var currImgUrl = "";
    
    //确认裁剪
    $("#confirmCropImg").click(function(){
    	console.log(); 
    	console.log(jcropApi.getWidgetSize());  
    	console.log(jcropApi.getScaleFactor());
    	var selectObj = jcropApi.tellSelect();
    	$.getJSON(reqPath + "web/file/saveFileCutForPic",{
    		imgUrl : currImgUrl, 
    		x : selectObj.x,
    		y : selectObj.y,
    		w : selectObj.w,
    		h : selectObj.h,
    		showWidth : jcropApi.getWidgetSize()[0],
    		showHeight : jcropApi.getWidgetSize()[1] 
    	},function(json){
    		console.log(json);
    		if(json.code == "0"){
    			console.log(json.url); 
    			sucCheckImg(json.url);
    		}
    		
    	});
    	
    });
    
    $("#notCropImg").click(function(){
    	//不裁剪
    	$("#manageImgBox").fadeOut(); 
    	$("#tupiancaijian_box").hide();
    	sucCheckImg(currImgUrl);
    });
    $(".closeMangeBox").click(function(){
    	$("#manageImgBox").fadeOut(); 
    	$("#tupiancaijian_box").hide();  
    });
    //普通模式下面的图片点击事件
    $("body").on('click', ".img_wrap.normal", function() {
    	var itemId = $(this).attr("itemId");
    	var imgUrl = $(this).find("img").attr("src");
    	currPicId = itemId;
    	currImgUrl = imgUrl;
    	$("#crop_img").attr("src",imgUrl);    
    	$("input[name='crop_rate'][value='1']").iCheck('check');
    	console.log("用户点击了图片");  
    	$("#tupiancaijian_box").show();
    	//450 450
    	$("<img/>").attr("src", imgUrl).load(function() {
    		var w = this.width;
    		imgWidth = w;
    		var h = this.height;
    		imgHeight = h;
    		//如果是长方形的，动态div宽度不需要调整，如果是短边长方形，那么需要调整
    		if(w < h){
    			var dWidth = w/h*450; 
    			$("#tupiancaijian_box").find(".image-cropper-inner").css("width",dWidth + "px");
    			jcropApi.setImage(imgUrl,function(){
    				console.log("设置新图片成功");
        			jcropApi.setOptions({
                   		aspectRatio : dWidth/450,
                   		setSelect: [0,0,dWidth,450]
            		});  
        		});
    		}
    		else{
    			jcropApi.setImage(imgUrl,function(){
    				console.log("设置新图片成功");
        			jcropApi.setOptions({ 
                   		aspectRatio : 450,
                   		setSelect: [0,0,450,450*h/w]
            		});  
        		}); 
    		}
    	});
    });
    $("body").on('click', ".piliangChuli", function() {
        var _this = $(this);
        if (_this.hasClass("selected-img")) {
            _this.removeClass("selected-img");
        } else {
            _this.addClass("selected-img");
        }
        $("#selectPicCount").html($("#tupian_box").find(".selected-img").length);
    });
    //取消批量操作
    $("#btnCanclePiliangchuli").click(function() {
        $(".lpf_footer .lpf_normal").show();
        $("#piliangChuliBox").hide();
        $("#selectPicCount").html(0);
        $("#tupian_box").find(".img_wrap").addClass("normal");
        $("#tupian_box").find(".img_wrap").removeClass("piliangChuli");
        $("#tupian_box").find(".img_wrap").removeClass("selected-img");
        $("#tupian_box").find(".piliangChuli").unbind("click");
    });
    $("#btnMoveBatchFile").click(function(){
    	var imgArr = $("#tupian_box").find(".selected-img");
    	if(imgArr.length < 1){
    		layer.msg("请选择您想移动的图片");
    		return false;
    	}
    	var targetCateId = $("#moveCateSelect").val();
    	if(targetCateId == ""){
    		layer.msg("请选择一个具体的图片分组");
    		return false; 
    	}
    	var nowPicCateId = $("#nowPicCateId").val();
    	if(nowPicCateId == targetCateId){
    		layer.msg("目标分组不能与当前图片分组相同");
    		return false;
    	}
    	var ids = [];
    	for(var i = 0; i < imgArr.length; i++){
    		var item = $(imgArr[i]);
    		var id = item.find("span").attr("itemId");
    		ids.push(id);
    	}
    	layer.confirm("是否确认移动选中的图片", function(index) {
            $.getJSON(reqPath + "web/file/saveUserFileMoveByIds", {
                ids: ids.join(",") ,
                cateId : targetCateId
            }, function(result) {
                if (result.code == 0) {
                    layer.msg("移动成功", {
                        time: 600
                    }, function() {
                    	if(nowPicCateId != ""){
                    		$("#tupian_box").find(".selected-img").remove();
                          	 $("#selectPicCount").html(0);
                               if ($("#tupian_box").find(".img_wrap").length < 1) {
                                   $("#noPic_tips").show();
                               }
                    	}
                    });
                }
            })
        });
    });
    //批量操作，删除文件
    $("#btnDeleteBatchFile").click(function(){
    	var imgArr = $("#tupian_box").find(".selected-img");
    	if(imgArr.length < 1){
    		layer.msg("请选择您想删除的图片");
    		return false;
    	}
    	var ids = [];
    	for(var i = 0; i < imgArr.length; i++){
    		var item = $(imgArr[i]);
    		var id = item.find("span").attr("itemId");
    		ids.push(id);
    	}
    	 layer.confirm("是否确认删除选中的图片", function(index) {
             $.getJSON(reqPath + "web/file/deleteUserFileByIds", {
                 ids: ids.join(",")  
             }, function(result) {
                 if (result.code == 0) {
                     layer.msg("删除成功", {
                         time: 600
                     }, function() {
                    	 $("#tupian_box").find(".selected-img").remove();
                    	 $("#selectPicCount").html(0);
                         if ($("#tupian_box").find(".img_wrap").length < 1) {
                             $("#noPic_tips").show();
                         }
                     });
                 }
             })
         });
    });
    layui.use(['upload', 'layer'], function() {
        var picFileUpload = layui.upload;
        console.log("nowPicCateId=" + nowPicCateId);
        //多图片上传
        picFileUpload.render({
            elem: '#uploadBtnForPic',
            url: reqPath + 'admin/file/uploadUserImg',
            data: {  
                cateId: function() {
                    return $("#nowPicCateId").val();
                }
            },
            multiple: true,
            before: function(obj) {
                layer.load();
            },
            allDone: function(obj) {
                console.log(obj);
                console.log(obj.total);
                console.log(obj.successful);
                console.log(obj.aborted);
                layer.closeAll('loading');
                layer.msg("上传成功");
            },
            done: function(res) {
                console.log(res);
                $("#noPic_tips").hide();
                var ui = '<div itemId="' + res.id + '" class="img_wrap normal">' 
                		+ '<img src="' + res.url + '">' 
                		+ '<span itemId="' + res.id 
                		+ '" class="delete">删除</span>' 
                		+ '</div> ';
                var G = $(ui);  
                G.find(".delete").click(function(event) {
                	event.stopPropagation();
                    var itemId = $(this).attr("itemId");
                    layer.confirm("是否确认删除该图片", function(index) {
                        $.getJSON(reqPath + "web/file/deleteUserFile", {
                            id: itemId
                        }, function(result) {
                            if (result.code == 0) {
                                layer.msg("删除成功", {
                                    time: 600
                                }, function() {
                                    G.remove();
                                    if ($("#tupian_box").find(".img_wrap").length < 1) {
                                        $("#noPic_tips").show();
                                    }
                                });
                            }
                        })
                    });
                });
                $("#tupian_box").append(G);
            }
        });
        
    });//end layui
});
