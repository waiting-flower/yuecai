$(function(){
	var data = {
		skuItem : []	
	};
	if(nowGoodsSkuItem != ""){ 
		data.skuItem = nowGoodsSkuItem.split("###=,=###");
	}
	$(".tagsinput").tagsinput(); 
	//判断sku规格是否设置完成
	var checkSkuSetOk = function(){
		var checkOk = true;
		var skuItem = data.skuItem;
		if(skuItem.length < 1){
			return true;
		}
		
		var tagsinputArr = $(".tagsinput");
		var len = tagsinputArr.length;
		var ui = "";
		if(len == 1){
			if($(tagsinputArr[0]).val() == ""){
				return false;  
			}
		}
		else if(len == 2){
			var skuNameArr1 = $(tagsinputArr[0]).val().split(",");
			var skuNameArr2 = $(tagsinputArr[1]).val().split(",");
			if($(tagsinputArr[0]).val() == "" || $(tagsinputArr[1]).val() == ""){
				return false;  
			}
		}
		else if(len == 3){
			var skuNameArr1 = $(tagsinputArr[0]).val().split(",");
			var skuNameArr2 = $(tagsinputArr[1]).val().split(",");
			var skuNameArr3 = $(tagsinputArr[2]).val().split(","); 
			if($(tagsinputArr[0]).val() == "" || $(tagsinputArr[1]).val() == "" || $(tagsinputArr[2]).val() == ""){
				return false;  
			}
		} 
		var inputValArr = $("#sku_body_tr").find(".price");
		for(var i = 0; i < inputValArr.length; i++){
			var item = $(inputValArr[i]);  
			if(item.val() == ""){
				return false;
			}
		}
		return checkOk;
	}
	
	//点击按钮，打开规格弹窗
	$("#btnToAddGoodsSku").click(function(){
		$("#sku_modal").show();
	});
	//关闭规格弹窗
	$("#cancleCheckSku").click(function(){
		$("#sku_modal").hide();  
	});
	//确认选择规格弹窗
	$("#confirmCheckSku").click(function(){
		//确认选好了规格参数了
		var skuStr = $("#sku_choiced").html();
		if(skuStr == ""){
			layer.msg("请选择规格");
			return false;
		}
		var skuItem = skuStr.split(",");
		data.skuItem = skuItem;
		var ui = "";
		for(var i = 0; i < skuItem.length; i++){
			//判断是否存在
			ui = ui + '<li>' +
				     '	<div class="selected-specifications-item">' +
					 '		<span>' + skuItem[i] + '</span> ' +
					 '		<i skuItem="' + skuItem[i] + '" class="fa fa-times"></i>' +
					 '	</div>' + 
					 '	<div class="specifications-param-container clearfix">' +
					 '		<input type="text" skuName="' + skuItem[i] + '" skuId="sku_' + skuItem[i] + 
					 '	" class="form-control tagsinput" placeholder="新增标签">' +
					 '	</div>' + 
					 '</li>';  
		}
		var G = $(ui);
		$("#goods_sku_div_box ul").html(G); 
		$(".tagsinput").tagsinput();
		freshTr();
		$("#sku_body_tr").html("");  
		$("#sku_modal").hide(); 
		
	}); 
	
	var freshTr = function(){
		var skuItem = data.skuItem;
		if(skuItem.length > 0){
			$("#sku_table").show();
			$("#basePriceDiv").hide();
		}
		else{
			$("#sku_table").hide();
			$("#basePriceDiv").show();
		}  
		//获取全部的规格参数 
		var ui = "";
		for(var i = 0; i < skuItem.length;i++){
			ui = ui + "<th>" + skuItem[i] + "</th>";
		}
		ui = ui + '<th>图片</th>' 
        	+ '<th>销售价格<input type="text" placeholder="统一设置销售价格" class="form-control price tongyijiage"></th>'
        	+ '<th>库存<input type="text" placeholder="统一设置库存" class="form-control price tongyikucun"></th>'
        	+ '<th>虚拟价格<input type="text" placeholder="统一设置虚拟价格" class="form-control price tongyixunijiage"></th>'
			+ '<th>折扣商品数量限制<input type="text" placeholder="统一设置折数量限制" class="form-control price tongyidisbuylimit"></th>';
		$("#sku_tr").html(ui);
		$("#sku_body_tr").html("");  
		refreshTd(); 
	}
	$("body").on('click', ".sku_img_box", function() {
		showPicModal();
		var _this = $(this);
		sucCheckImg = function(url){
			_this.html("<img src='" + url + "'>"); 
		}; 
	});
	
	$("body").on('change', ".tongyijiage", function() {
		var val = $(this).val();
		$(".jiage").val(val);  
	});
	$("body").on('change', ".tongyikucun", function() {
		var val = $(this).val();
		$(".kucun").val(val);  
	}); 
	$("body").on('change', ".tongyixunijiage", function() {
		var val = $(this).val();
		$(".xunijiage").val(val);  
	});
	$("body").on('change', ".tongyidisbuylimit", function() {
		var val = $(this).val();
		$(".disbuylimit").val(val);
	});

	$("body").on('itemAdded', ".tagsinput", function(event) {
		var tagsinputArr = $(".tagsinput");
		for(var i = 0; i < tagsinputArr.length; i++){
			var tagItem = $(tagsinputArr[i]);
			console.log(tagItem.attr("skuName"));
			if(tagItem.val() == ""){
				return false;
			}
		} 
		refreshTd();
	});
	$("body").on('itemRemoved', ".tagsinput", function(event) {
		console.log(event); 
		var skuParams = event.item;
		var tagsinputArr = $(".tagsinput"); 
		for(var i = 0; i < tagsinputArr.length; i++){
			var tagItem = $(tagsinputArr[i]);
			console.log(tagItem.attr("skuName"));
			if(tagItem.val() == ""){
				$("#sku_body_tr").html(""); 
				return false;
			}
		} 
		removeSkuItem(skuParams); 
	});
	 
	//删除一个sku的标签之后
	var removeSkuItem = function(skuParams){
		var tagsinputArr = $(".tagsinput");
		var len = tagsinputArr.length;
		if(len == 1){
			var trArr = $("#sku_body_tr").find("tr");
			for(var index = 0; index < trArr.length; index++){
				var trItem = $(trArr[index]);
				if($(trItem.find("td")[0]).html() == skuParams){
					trItem.remove(); 
				}  
			}
		}
		else if(len == 2){
			var trArr = $("#sku_body_tr").find("tr");
			for(var index = 0; index < trArr.length; index++){
				var trItem = $(trArr[index]);
				if($(trItem.find("td")[0]).html() == skuParams 
						|| $(trItem.find("td")[1]).html() == skuParams){
					trItem.remove();  
				} 
			}
		}
		else if(len == 3){
			var trArr = $("#sku_body_tr").find("tr");
			for(var index = 0; index < trArr.length; index++){
				var trItem = $(trArr[index]);
				if($(trItem.find("td")[0]).html() == skuParams 
						|| $(trItem.find("td")[1]).html() == skuParams
						|| $(trItem.find("td")[2]).html() == skuParams){
					trItem.remove(); 
				} 
			}
		}
	}
	//添加一个sku的标签之后
	var refreshTd = function(){
		var tagsinputArr = $(".tagsinput");
		var len = tagsinputArr.length;
		var ui = "";
		if(len == 1){
			if($(tagsinputArr[0]).val() == ""){
				return false;  
			}
			var skuNameArr = $(tagsinputArr[0]).val().split(",");
			console.log(skuNameArr); 
			for(var i = 0; i < skuNameArr.length; i++){
				//判断这个标签是否存在于已经有的table中
				var isExists = false;
				var trArr = $("#sku_body_tr").find("tr");
				for(var index = 0; index < trArr.length; index++){
					var trItem = $(trArr[index]);
					if($(trItem.find("td")[0]).html() == skuNameArr[i]){
						isExists = true; 
						break; 
					}
				}
				if(isExists) continue;
				ui = ui + '<tr><td>' + skuNameArr[i] + '</td>'   
				+ '<td><div class="sku_img_box"></div></td>'
				+ '<td><input type="text" class="form-control price jiage">'
				+ '<input type="hidden" value="" class="skuId"></td>'
				+ '<td><input type="text" class="form-control price kucun"></td>'  
				+ '<td><input type="text" class="form-control price xunijiage"></td>'
				+ '<td><input type="text" class="form-control price disbuylimit"></td></tr>'
			} 
			
		}
		else if(len == 2){
			var skuNameArr1 = $(tagsinputArr[0]).val().split(",");
			var skuNameArr2 = $(tagsinputArr[1]).val().split(",");
			if($(tagsinputArr[0]).val() == "" || $(tagsinputArr[1]).val() == ""){
				return false;  
			}
			console.log(skuNameArr1); 
			for(var i = 0; i < skuNameArr1.length; i++){
				for(var j = 0; j < skuNameArr2.length; j++){
					//判断是否存在了
					var isExists = false;
					var trArr = $("#sku_body_tr").find("tr");
					for(var index = 0; index < trArr.length; index++){
						var trItem = $(trArr[index]);
						if($(trItem.find("td")[0]).html() == skuNameArr1[i]
							&& $(trItem.find("td")[1]).html() == skuNameArr2[j]){
							isExists = true;
							break; 
						}
					}
					if(isExists) continue;
					ui = ui + '<tr><td>' + skuNameArr1[i] + '</td>'  
					+ '<td>' + skuNameArr2[j] + '</td>'   
					+ '<td><div class="sku_img_box"></div></td>'
					+ '<td><input type="text" class="form-control price jiage"><input type="hidden" value="" class="skuId"></td>'
					+ '<td><input type="text" class="form-control price kucun"></td>'  
					+ '<td><input type="text" class="form-control price xunijiage"></td>'
					+ '<td><input type="text" class="form-control price disbuylimit"></td></tr>'
				}
			} 
		}
		else if(len == 3){
			var skuNameArr1 = $(tagsinputArr[0]).val().split(",");
			var skuNameArr2 = $(tagsinputArr[1]).val().split(",");
			var skuNameArr3 = $(tagsinputArr[2]).val().split(","); 
			if($(tagsinputArr[0]).val() == "" || $(tagsinputArr[1]).val() == "" || $(tagsinputArr[2]).val() == ""){
				return false;  
			}
			console.log(skuNameArr1); 
			for(var i = 0; i < skuNameArr1.length; i++){
				for(var j = 0; j < skuNameArr2.length; j++){
					for(var k = 0; k < skuNameArr3.length; k++){
						//判断是否存在了
						var isExists = false;
						var trArr = $("#sku_body_tr").find("tr");
						for(var index = 0; index < trArr.length; index++){
							var trItem = $(trArr[index]);
							if($(trItem.find("td")[0]).html() == skuNameArr1[i]
								&& $(trItem.find("td")[1]).html() == skuNameArr2[j]
								&& $(trItem.find("td")[2]).html() == skuNameArr3[k]){
								isExists = true;
								break; 
							}
						}
						if(isExists) continue;
						
						ui = ui + '<tr><td>' + skuNameArr1[i] + '</td>'  
						+ '<td>' + skuNameArr2[j] + '</td>' 
						+ '<td>' + skuNameArr3[k] + '</td>'  
						+ '<td><div class="sku_img_box"></div></td>'
						+ '<td><input type="text" class="form-control price jiage"><input type="hidden" value="" class="skuId"></td>'
						+ '<td><input type="text" class="form-control price kucun"></td>'  
						+ '<td><input type="text" class="form-control price xunijiage"></td>'
						+ '<td><input type="text" class="form-control price disbuylimit"></td></tr>'
					}
				}
			} 
		} 
		$("#sku_body_tr").append(ui); 
	}
	
	//获取规格配置，提交时候的参数
	var getSubmitSkuJson = function(){
		var skuItem = data.skuItem;
		var skuLen = skuItem.length;//总共几个规格
		
		var trArr = $("#sku_body_tr").find("tr");//总共有多少个选项
		var skuJson = {};
		var skuJsonSubmitStr = "";
		var skuJsonSubmitArr = []; 
		if(skuLen == 1){//一个规格
			for(var i = 0; i < trArr.length; i++){
				var item = $(trArr[i]);  
				skuJson[skuItem[0]] = $(item.find("td")[0]).html().trim();
				var imgUrl = item.find(".sku_img_box img").attr("src");
				if(imgUrl == undefined || imgUrl == ""){
					imgUrl = $("#imgUrl").val();
				}
				var jiage = item.find(".jiage").val();
				var kucun = item.find(".kucun").val();
				var xunijiage = item.find(".xunijiage").val();
				var disbuylimit = item.find(".disbuylimit").val();
				var skuId = item.find(".skuId").val();
				var ui = JSON.stringify(skuJson) + "=,=" + imgUrl + "=,=" + jiage + "=,=" + kucun 
					+ "=,=" + xunijiage + "=,=" + disbuylimit + "=,=" +skuId;
				skuJsonSubmitArr.push(ui);
			}
		}
		else if(skuLen == 2){//2个规格
			for(var i = 0; i < trArr.length; i++){
				var item = $(trArr[i]);
				skuJson[skuItem[0]] = $(item.find("td")[0]).html().trim();
				skuJson[skuItem[1]] = $(item.find("td")[1]).html().trim();
				var imgUrl = item.find(".sku_img_box img").attr("src");
				if(imgUrl == undefined || imgUrl == ""){
					imgUrl = $("#imgUrl").val();  
				}
				var jiage = item.find(".jiage").val();
				var kucun = item.find(".kucun").val();
				var xunijiage = item.find(".xunijiage").val();
				var disbuylimit = item.find(".disbuylimit").val();
				var skuId = item.find(".skuId").val();
				var ui = JSON.stringify(skuJson) + "=,=" + imgUrl + "=,=" 
					+ jiage + "=,=" + kucun + "=,=" + xunijiage + "=,=" + disbuylimit + "=,=" +skuId;
				skuJsonSubmitArr.push(ui);
			}
		}
		else if(skuLen == 3){//三个规格
			for(var i = 0; i < trArr.length; i++){
				var item = $(trArr[i]);
				skuJson[skuItem[0]] = $(item.find("td")[0]).html().trim();
				skuJson[skuItem[1]] = $(item.find("td")[1]).html().trim();
				skuJson[skuItem[2]] = $(item.find("td")[2]).html().trim();
				var imgUrl = item.find(".sku_img_box img").attr("src");
				if(imgUrl == undefined || imgUrl == ""){  
					imgUrl = $("#imgUrl").val(); 
				}
				var jiage = item.find(".jiage").val();
				var kucun = item.find(".kucun").val();
				var xunijiage = item.find(".xunijiage").val();
				var disbuylimit = item.find(".disbuylimit").val();
				var skuId = item.find(".skuId").val();
				console.log("skuId=" + skuId);   
				var ui = JSON.stringify(skuJson) + "=,=" + imgUrl + "=,=" 
					+ jiage + "=,=" + kucun + "=,=" + xunijiage + "=,=" + disbuylimit + "=,=" +skuId;
				skuJsonSubmitArr.push(ui);  
			}
		}
		var skuJsonSubmitStr = skuJsonSubmitArr.join("###=,=###");
		console.log("skuJsonSubmitStr=" + skuJsonSubmitStr);
		return skuJsonSubmitStr; 
	}
	
	//添加规格
	$("#tianjiaguige_customer").click(function(){
		$("#customerTags").val("");
		$("#showBox_tianjiaguige").show(); 
		$("#tianjiaguige_customer").hide();
		$("#confirm_tianjiaguige_customer").show();
	});
	$("#confirm_tianjiaguige_customer").click(function(){
		var itemArr = $(".specifications-show-item");
		var tagsVal = $("#customerTags").val();
		if(tagsVal == ""){
			layer.msg("请输入自定义规格名称");
			$("#customerTags").focus(); 
			return false; 
		}
		if(tagsVal.indexOf(",") != -1){
			layer.msg("规格名称中不能输入英文逗号");
			$("#customerTags").focus(); 
			return false; 
		}
		var hasKey = false;
		for(var i = 0; i < itemArr.length; i++){
			if($(itemArr[i]).html().trim() == tagsVal){
				hasKey = true;
				break; 
			}
		}
		if(hasKey){
			layer.msg("您输入的规格名称已经存在，请不要重复输入");
			$("#customerTags").focus();
			return false;
		} 
		$("#showBox_tianjiaguige").before('<div class="specifications-show-item ng-star-inserted">' 
				+ tagsVal + '</div>');
		
		$("#tianjiaguige_customer").show(); 
		$("#confirm_tianjiaguige_customer").hide();
		$("#showBox_tianjiaguige").hide(); 
	});
	 
	//规格的选择
	$("body").on('click', ".specifications-show-item", function() {
		var _this = $(this);
		if(_this.hasClass("active")){
			//取消选中
			$(this).removeClass("active");
			var sku_choiced = $("#sku_choiced").html();
			var skuItem = sku_choiced.split(",");
			var newItem = [];
			for(var i = 0; i < skuItem.length; i++){
				if(skuItem[i] == _this.html().trim()){
					continue;   
				}
				newItem.push(skuItem[i]);
			} 
			$("#sku_choiced").html(newItem.join(","));
		}
		else{
			var len = $(".specifications-show-item.active").length;
			if(len >= 3){
				layer.msg("规格最多可以设置3个");
				return false;
			}
			$(this).addClass("active"); 
			var sku_choiced = $("#sku_choiced").html();
			if(sku_choiced != ""){
				sku_choiced = sku_choiced + ",";
			} 
			$("#sku_choiced").html(sku_choiced + _this.html().trim());
		}
		
	});
	
	$("body").on('click', ".selected-specifications-item i.fa-times",function(){
		var skuItem = $(this).attr("skuItem");
		$(this).parent().parent().remove();  
		var sitem = data.skuItem;
		var newItemArr = [];
		for(var i = 0; i < sitem.length; i++){
			if(skuItem == sitem[i]){
				continue;
			}
			newItemArr.push(sitem[i]);
		}
		data.skuItem = newItemArr;  
		freshTr(); 
	});
	$("body").on('click', ".closeBtn", function(){
		var url = $(this).parent().find("img").attr("src");
		var imgList = $("#imgList").val();
		var imgArr = imgList.split(",");
		var newImgArr = [];
		for(var i = 0; i < imgArr.length; i++){
			if(imgArr[i] == url){
				continue;
			}
			newImgArr.push(imgArr[i]);
		} 
		$("#imgList").val(newImgArr.join(","));
		$(this).parent().remove();  
	}); 

	
	var clickCheckbox = document.querySelector('.js-switch_3');
	var lunboBox = document.querySelector('.js-switch_lunbo');

	new Switchery(clickCheckbox, {
    	color: "#1AB394", 
    	size: 'small'   
    }); 
	new Switchery(lunboBox, {
    	color: "#1AB394", 
    	size: 'small'   
    });  
	lunboBox.onchange = function(){
		if(lunboBox.checked){
			$("#shangpin_lunbo_div_box").show();
		}  
		else{
			$("#shangpin_lunbo_div_box").hide();    
		}
	}; 
	clickCheckbox.onchange = function() {
		//是否有图片的角标
		if(clickCheckbox.checked){
			$("#corner_img_box").show();
		}
		else{
			$("#corner_img_box").hide(); 
		}
	}; 
	
	//角标图片的选项radio
	$("input:radio[name='cornerType']").on('ifChecked', function(event){
		var cornerType = $(this).val(); 
		if(cornerType == "zidingyi"){
			$("#jiaobiao_img_div_box").show(); 
			$("#jiaobiao_tupian").find("img").attr("src","");
		}
		else{
			$("#jiaobiao_img_div_box").hide();  
			$("#jiaobiao_tupian").find("img").attr("src",cornerImg[cornerType]);
			$("#cornerImg").val(cornerImg[cornerType]);
		} 
    });
	
	
	//角标自定义图片
	$("#jiaobiao_img_div_box").click(function(){
		showPicModal(); 
		sucCheckImg = function(url){
			$("#jiaobiao_tupian").find("img").attr("src",url);
			$("#cornerImg").val(url);
			hidePicModal();   
		} 
	});
	
	var cornerImg = {
			"xinpin" : "http://youji.7working.com/xinpin.png",
		"hot" : "http://youji.7working.com/hot.png",
		"new" : "http://youji.7working.com/new.png",
		"baokuan" : "http://youji.7working.com/baokuan.png",
		"zidingyi" : "" 
	};
	
	//商品主图上传
	$("#uploadGoodsZhutu").click(function(){
		showPicModal(); 
		sucCheckImg = function(url){
			$("#goods_imgUrl_div_box").html('<img src="' + url + '">');
			$("#imgUrl").val(url);
			hidePicModal();   
		}   
	});
	
	//轮播图片上传
	$("#btnAddLunboPic").click(function(){
		showPicModal(); 
		sucCheckImg = function(url){
			var ui = '<div class="add-pic goods_lunbo m-r">'     
	        		+ '<img src="' + url + '">' 
					+ '<span class="closeBtn">×</span> ' 
					+ '</div> ';
			var G = $(ui); 
			$("#btnAddLunboPic").before(G); 
			hidePicModal();    
			var imgList = $("#imgList").val();
			var imgArr = imgList.split(",");
			imgArr.push(url); 
			$("#imgList").val(imgArr.join(","));
		}   
	}); 
	
	var pl = parent.layer;
	var pw = parent.currDialogWindow;
	$("#btnSave").click(function(){
		var id = $("#id").val();
		var name = $("#name").val();
		var name2 = $("#name2").val();
		var cornerImg = $("#cornerImg").val();
		var imgUrl = $("#imgUrl").val(); 
		var imgList = $("#imgList").val(); 
		var cate = $("#cate").val();
		var brand = $("#brand").val();
		var info = editor.html();
		
		var goodsType = $("#goodsType").val();
		
		var price = $("#price").val();
		var oldPrice = $("#oldPrice").val();
		var stockNum = $("#stockNum").val();
		
		if(name == null || name == ""){ 
			layer.alert("请输入商品名称");
			return false; 
		}
		if(imgUrl == ""){
			$("#imgUrl").focus();
			layer.msg("请上传商品封面主图");
			return false; 
		}
		if(cate == null || cate == ""){
			$("#cate").focus();
			layer.msg("请选择商品分类");
			return false;
		}
		
		if(brand == null || brand == ""){
			$("brand").focus();
			layer.msg("请选择商品品牌");
			return false;
		}
		
		//判断角标是否开启
		var isCornerImg = clickCheckbox.checked;
		if(isCornerImg && cornerImg == ""){
			layer.msg("请选择或者上传角标图片哦");
			return false; 
		}
		
		var isLunbo = lunboBox.checked;
		if(isLunbo && imgList == ""){
			layer.msg("请上传商品轮播图片");
			$("#shangpin_lunbo_div_box").focus();
			return false;
		}
		
		
		
		if(info == ""){ 
			layer.msg("商品图文详情不能为空，请重新设置");
			$("#info").focus();
			return false;
		}
		//此时去拼接规格参数等数据
		var skuItem = data.skuItem;
		if(skuItem.length < 1){//说明没有设置规格参数
			if(price == ""){
				layer.msg("请设置商品价格");
				return false;
			}
			if(oldPrice == ""){ 
				layer.msg("请设置商品虚拟价格");
				return false;
			}
			if(stockNum == ""){
				layer.msg("请设置商品库存");
				return false;
			}
		}
		if(!checkSkuSetOk()){
			layer.msg("规格参数尚未设置完成，请设置完成后重新提交");
			$("#goods_sku_div_box").focus();
			return false; 
		}
		
		var paramsItem = [];
		var tagsinputArr = $(".tagsinput");
		for(var i = 0; i < tagsinputArr.length; i++){
			paramsItem.push($(tagsinputArr[i]).val());
		}
		//接下来获取每一个具体的配置数据
		var skuJSONStr = getSubmitSkuJson();
		console.log(skuJSONStr); 
		var vData = {
				id : id,
				name : name, 
				name2 : name2, 
				imgUrl : imgUrl, 
				imgList : imgList, 
				info : info, 
				cate : cate,
				brand : brand,
				goodsType : goodsType,
				cornerImg : cornerImg, 
				price : price,
				oldPrice : oldPrice,
				stockNum : stockNum,
				splitKey : "###=,=###",
				skuItem : skuItem.join("###=,=###"),
				paramsItem : paramsItem.join("###=,=###"),
				skuJson : skuJSONStr,
				goodsNo : $("#goodsNo").val(),
				tiaoxingma :$("#tiaoxingma").val(),
				dislimit :$("#dislimit").val()
		};
		
		$.post(basePath + "/admin/shop/goods/saveGoods",vData,function(json){ 
			if(json.code == 0){  
				var index = pl.getFrameIndex(window.name); //先得到当前iframe层的索引
				pl.close(index); //再执行关闭 
				pl.alert("操作成功"); 
				pw.$("#queryBtn").click();
			}  
			else{   
				layer.alert("操作失败");
			}
		},"json");
	});
	$("#btnCancle").click(function(){
		//关闭操作
		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
		parent.layer.close(index); //再执行关闭  
	}); 
});