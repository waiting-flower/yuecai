<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>文件分类</title>
    <link rel="shortcut icon" href="favicon.ico"> 
   	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
 
</head> 

<body class="gray-bg"> 
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
    	<div class="col-sm-12">  
    		<div class="ibox float-e-margins" style="height:100%;">
                <div class="ibox-content"> 
                	<form class="form-horizontal">
					    <div class="form-group"> 
					        <label class="col-sm-3 col-md-3 control-label">合作商姓名：</label>
					        <div class="col-sm-6 col-md-6">
					        	<input type="hidden" value="${a.id }" id="id">  
					            <input type="text" id="name" value="${a.name }" class="form-control" placeholder="请输入合作商姓名">
					        </div> 
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 col-md-3  control-label">联系方式：</label>
					        <div class="col-sm-6 col-md-6">
					            <input type="text" id="mobile" value="${a.mobile }" class="form-control" placeholder="请输入联系方式">
					        </div> 
					    </div> 
					    <div class="form-group">
					        <label class="col-sm-3 col-md-3  control-label">公司名称：</label>
					        <div class="col-sm-6 col-md-6">
					            <input type="text" id="company" value="${a.company }" class="form-control" placeholder="请输入公司名称">
					        </div> 
					    </div> 
					    <div class="form-group">
					        <label class="col-sm-3 col-md-3  control-label">合作商级别：</label>
					        <div class="col-sm-6 col-md-6">
					            <select class="form-control"  id="level"  >  
									<option value="">请选择合作商级别</option>
									<option value="6000元基础版合作商">6000元基础版合作商</option>
									<option value="12000高级加盟合作商">12000高级加盟合作商</option>
								</select>
					        </div> 
					    </div> 
					    <div class="form-group"> 
								<label class="col-sm-3 control-label">所在区域:</label>
								<div class="col-sm-3">
									<select class="form-control" name="pro" id="pro"  >  
								        <option value="">请选择省份</option>
								      </select>
								</div>
								<div class="col-sm-3">
									 <select class="form-control" name="city" id="city"> 
								      	<option value=''>请选择城市</option>
								      </select> 
								</div> 
								<div class="col-sm-3">
									 <select class="form-control" name="country" id="country"> 
								      	<option value=''>请选择区/县</option>
								      </select>  
								</div>
						</div> 
					    <div class="form-group draggable ui-draggable">
                            <div class="col-sm-12 col-sm-offset-3"> 
                                <button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;提交</button>
                                <button class="btn btn-white" type="button" id="btnCancle">取消</button>
                            </div>
                        </div>
					</form> 
                </div>  
            </div> 
    	</div> 
    </div>
</div>

<script src="https://cdn.staticfile.org/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script>   
    $(document).ready(function () {
    	var id = "${a.id}";
    	$("#pro").change(function(){
    		var proName = $(this).find("option:selected").text(); 

    		if(proName != "请选择省份"){ 
    			$.getJSON("${pageContext.request.contextPath}/admin/static/js/address.js",function(json){
    				var len = json.length;
    				for(var i = 0; i < len; i++){
    					var proItem = json[i];
    					if(proItem.name == proName){
    						var cityArr = proItem.city;
    						$("#city").empty(); 
    						$("#city").append("<option value=''>请选择城市</option>"); 
    						$.each(cityArr,function(i,item){
    							var option = "<option src='" + item.id + "'>" + item.name + "</option>"; 
    							$("#city").append(option); 
    						});
    						break;
    					}
    				}
    				
    			});
    		}
    	});
    	$("#city").change(function(){ 
    		var proName = $("#pro").find("option:selected").text();
    		var cityName = $(this).find("option:selected").text(); 
     
    		if(proName != "请选择省份" && cityName != "请选择城市"){ 
    			$.getJSON("${pageContext.request.contextPath}/admin/static/js/address.js",function(json){
    				var len = json.length;
    				for(var i = 0; i < len; i++){
    					var proItem = json[i];
    					
    					if(proItem.name == proName){
    						console.log(proItem.name);
    						var cityArr = proItem.city;
    						for(var j = 0; j < cityArr.length; j++){
    							var cityItem = cityArr[j];
    							if(cityItem.name == cityName){
    								console.log(cityItem.name); 
    								var arr = cityItem.district;
    								$("#country").empty();  
    								$("#country").append("<option value=''>请选择区县</option>"); 
    								$.each(arr,function(i,item){
    									var option = "<option src='" + item.id + "'>" + item.name + "</option>"; 
    									$("#country").append(option); 
    								});
    							}
    						}
    						break;
    					}
    				}
    				
    			});
    		}
    	});
    	if(id == ''){ 
    		
    		//新的 
    		$.getJSON("${pageContext.request.contextPath}/admin/static/js/address.js",function(json){
    			console.log(json); 
    			$.each(json,function(i,item){ 
    				var option = "<option src='" + item.id + "'>" + item.name + "</option>"; 
    				$("#pro").append(option); 
    			});
    			
    		});
    	}
    	else{
    		var proName = "${a.pro}";
    		var cityName = "${a.city}";
    		var countryName = "${a.country}";  
    		//新的 
    		$.getJSON("${pageContext.request.contextPath}/admin/static/js/address.js",function(json){
    			for(var i = 0; i < json.length; i++){
    				var item = json[i];
    				if(item.name == proName){ 
    					var option = "<option selected src='" + item.id + "'>" + item.name + "</option>"; 
    					$("#pro").append(option);
    				}
    				else{
    					var option = "<option src='" + item.id + "'>" + item.name + "</option>"; 
    					$("#pro").append(option);
    				}
    				 
    				if(item.name == proName){
    					for(var j = 0; j < item.city.length; j++){
    						var item2 = item.city[j]; 
    						if(item2.name == cityName){ 
    							var option2 = "<option selected src='" + item2.id + "'>" + item2.name + "</option>"; 
    							$("#city").append(option2); 
    						}
    						else{
    							var option2 = "<option src='" + item2.id + "'>" + item2.name + "</option>"; 
    							$("#city").append(option2); 
    						}
    						
    						if(item2.name == cityName){   
    							for(var k = 0; k < item2.district.length; k++){
    								var item3 = item2.district[k]; 
    								if(item3.name == countryName){  
    									var option3 = "<option selected src='" + item3.id + "'>" + item3.name + "</option>"; 
    									$("#country").append(option3); 
    								}
    								else{
    									var option3 = "<option src='" + item3.id + "'>" + item3.name + "</option>"; 
    									$("#country").append(option3); 
    								}
    							}
    						}
    					}
    				}
    			}     
    		});
    	}
    	$("#btnSave").click(function(){
    		var id = $("#id").val();
    		var name = $("#name").val();
    		var mobile = $("#mobile").val();
    		var company = $("#company").val();
    		var pro = $("#pro").val();
    		var city = $("#city").val();
    		var county = $("#country").val();
    		var level = $("#level").val();
    		if(name == null || name == ""){ 
    			layer.alert("请输入合作商姓名");
    			return false; 
    		}
    		if(mobile == null || mobile == ""){ 
    			layer.alert("请输入合作商姓名");
    			return false; 
    		}
    		if(company == null || company == ""){ 
    			layer.alert("请输入合作商姓名");
    			return false; 
    		}
    		if(pro == null || pro == ""){ 
    			layer.alert("请选择省份");
    			return false; 
    		}
    		if(city == null || city == ""){ 
    			layer.alert("请选择城市");
    			return false; 
    		}
    		if(county == null || county == ""){ 
    			layer.alert("请选择区域");
    			return false; 
    		}
    		if(level == null || level == ""){ 
    			layer.alert("请选择合作商级别");
    			return false; 
    		}
    		$.post("${pageContext.request.contextPath}/admin/coop/saveCooperation",{
    			id : id,
    			name : name, 
    			mobile : mobile,
    			company : company,
    			pro : pro,
    			city : city,
    			county : county,
    			level : level 
    		},function(json){
    			if(json.code == 0){ 
    				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
    				parent.layer.close(index); //再执行关闭 
    				parent.layer.alert("操作成功");
    				parent.currDialogWindow.$("#queryBtn").click();
    			}
    			else{   
    				layer.alert("操作失败");
    			}
    		},"json");
    	});
		$("#btnCancle").click(function(){
			//关闭操作
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭  
		});      
    });
</script>
</body>
</html>
