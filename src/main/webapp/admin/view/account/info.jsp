<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>微信账号</title>
    <link rel="shortcut icon" href="favicon.ico"> 
   	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
 
</head> 

<body class="gray-bg"> 
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
    	<div class="col-sm-12">  
    		<div class="ibox float-e-margins" style="height:100%;">
                <div class="ibox-content"> 
                	<form class="form-horizontal">
					    <div class="form-group"> 
					        <label class="col-sm-3 col-md-3 control-label">账号名称：</label>
					        <div class="col-sm-6 col-md-6">
					        	<input type="hidden" value="${a.id }" id="id">  
					            <input type="text" id="name" value="${a.name }" class="form-control" placeholder="请输入账号名称">
					        </div> 
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 col-md-3  control-label">APPID：</label>
					        <div class="col-sm-6 col-md-6">
					            <input type="text" id="appid" value="${a.appid }" class="form-control" placeholder="请输入APPID">
					        </div> 
					    </div> 
					    <div class="form-group">
					        <label class="col-sm-3 col-md-3  control-label">微信平台秘钥：</label>
					        <div class="col-sm-6 col-md-6">
					            <input type="text" id="secret" value="${a.secret }" class="form-control" placeholder="请输入appsecret">
					        </div> 
					    </div> 
					    <div class="form-group">
					        <label class="col-sm-3 col-md-3  control-label">token凭证：</label>
					        <div class="col-sm-6 col-md-6">
					            <input type="text" id="token" value="${a.token }" class="form-control" placeholder="请输入微信平台token凭证">
					        </div> 
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 col-md-3  control-label">微信支付商户id：</label>
					        <div class="col-sm-6 col-md-6">
					            <input type="text" id="mhId" value="${a.mhId }" class="form-control" placeholder="请输入微信支付商户ID">
					        </div> 
					    </div> 
					    <div class="form-group">
					        <label class="col-sm-3 col-md-3  control-label">微信支付的API秘钥：</label>
					        <div class="col-sm-6 col-md-6">
					            <input type="text" id="apiSecret" value="${a.apiSecret }" class="form-control" placeholder="请输入微信支付apiSecret">
					        </div> 
					    </div> 
					    <div class="form-group">
					        <label class="col-sm-3 col-md-3  control-label">微信支付的证书位置：</label>
					        <div class="col-sm-6 col-md-6">
					            <input type="text" id="certPath" value="${a.certPath }" class="form-control" placeholder="需要用到退款的必须上传">
					        </div> 
					    </div> 
					    <div class="form-group">
					        <label class="col-sm-3 col-md-3  control-label">微信支付的pkcs8公钥：</label>
					        <div class="col-sm-6 col-md-6">
					            <input type="text" id="pkcs8" value="${a.pkcs8 }" class="form-control" placeholder="使用在线打款，例如打款到银行卡，需要提交该字段">
					        </div> 
					    </div> 
					    <div class="form-group draggable ui-draggable">
                            <div class="col-sm-12 col-sm-offset-3"> 
                                <button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;提交</button>
                                <button class="btn btn-white" type="button" id="btnCancle">取消</button>
                            </div>
                        </div>
					</form> 
                </div>  
            </div> 
    	</div> 
    </div>
</div>

<script src="https://cdn.staticfile.org/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script>   
    $(document).ready(function () {
    	$("#btnSave").click(function(){
    		var id = $("#id").val();
    		var name = $("#name").val();
    		var appid = $("#appid").val();
    		var secret = $("#secret").val();
    		var token = $("#token").val();
    		var mhId = $("#mhId").val();
    		var apiSecret = $("#apiSecret").val();
    		var certPath = $("#certPath").val();
    		var pkcs8 = $("#pkcs8").val();
    		if(name == null || name == ""){ 
    			layer.alert("请输入账号名称");
    			return false; 
    		}
    		if(appid == null || appid == ""){ 
    			layer.alert("请输入appid");
    			return false; 
    		}
    		if(secret == null || secret == ""){ 
    			layer.alert("请输入secret");
    			return false; 
    		} 
    		$.post("${pageContext.request.contextPath}/admin/account/saveAccount",{
    			id : id,
    			name : name, 
    			token : token,
    			appid : appid,
    			secret : secret,
    			mhId : mhId,
    			apiSecret : apiSecret,
    			certPath : certPath,
    			pkcs8 : pkcs8
    		},function(json){
    			if(json.code == 0){ 
    				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
    				parent.layer.close(index); //再执行关闭 
    				parent.layer.alert("操作成功");
    				parent.currDialogWindow.$("#queryBtn").click();
    			}
    			else{   
    				layer.alert("操作失败");
    			}
    		},"json");
    	});
		$("#btnCancle").click(function(){
			//关闭操作
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭  
		});      
    });
</script>
</body>
</html>
