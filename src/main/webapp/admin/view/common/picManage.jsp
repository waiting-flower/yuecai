<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<link href="${pageContext.request.contextPath}/admin/static/js/app/jcrop/jquery.Jcrop.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/admin/static/css/pic.css?v=4.1.0" rel="stylesheet">
<link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
<div id="manageImgBox" style="display:none;">
	<div class="lpf_modal_mask"></div>   
	<div class="lpf_modal">  
	<div class="lpf_dialog"> 
		<div class="lpf_dialog_content">
			<button aria-label="Close" class="ant-modal-close closeMangeBox ng-star-inserted">
                <span class="ant-modal-close-x fa fa-times"></span>
             </button> 
             <div class="ant-modal-header ng-star-inserted">
              <div class="ant-modal-title" id="nzModal3">
			      <ul class="menu ant-menu ant-menu-root ant-menu-light ant-menu-horizontal ng-star-inserted" nz-menu="">
			        <li nz-menu-item="self" class="ant-menu-item ant-menu-item-selected">
			    		我的图片
			    	</li>
			        <li nz-menu-item="sys" class="ant-menu-item">
			    		图片库
			    	</li>
			      </ul>
              </div>
            </div>
            <!-- body开始 -->
            <div class="lpf_modal_body">
            	<!-- 左侧开始 -->
            	<div class="left">
            		<ul class="cate left_menu_ul normal"> 
            			<!-- 
            			<li class="active"><span>全部</span></li>
				    	<li class="ng-star-inserted"> 
				            <i class="anticon anticon-edit edit-cate fa fa-pencil"></i>
				            <span>34</span> 
				            <i class="anticon anticon-close-circle delete-cate fa fa-times"></i>
				     	</li>
            			 -->
            		</ul> 
            	</div>
            	<!-- 左侧结束 -->
            	
            	<ul class="sub-cate ng-star-inserted" id="subMenuCate_div" style="display:none;">
			        <li class="active">全部</li>
			    </ul>
            	
            	<!-- 右侧开始 -->
            	<div class="right">
            		<div class="img_container"> 
            			<div class="content">
            				<div class="img_star_inserted" id="tupian_box">
            					<%for(int i=0;i<0;i++){ %>
            					<div class="img_wrap">
            						<img src="https://img.zhichiwangluo.com/zcimgdir/album/file_5bfd0d2656148.png">
            						<span class="delete">删除</span>
            					</div> 
            					<%} %> 
            				</div>
            				<div class="empty_cate img_star_inserted" style="display:none;" id="noPic_tips">此分组暂无图片！</div>
            			</div>
            		</div>
            	</div> 
            	<!-- 右侧结束 -->
            </div>
            <!-- body结束 -->
            <!-- footer开始 -->
            <div class="lpf_footer"> 
            	<div class="lpf_normal ng-star-inserted">
            		<button class="upload-btn ant-btn ant-btn-primary" id="uploadBtnForPic"
            			 style="color: #fff;background-color: #108ee9;border-color: #108ee9;">上传至当前组</button>
			        <button id="addNewCate" class="ant-btn ant-btn-default">新建分组 </button>
			        <button id="batchHandleCate" 
			        	class="ant-btn ant-btn-default">批量处理 </button>
            	</div>  
				<div class="clearfix ng-star-inserted" id="piliangChuliBox" style="display:none;">
					<button class="f-r ant-btn ant-btn-default" id="btnCanclePiliangchuli">
						取消</button>
					<button class="f-r mg-r-10 ant-btn ant-btn-default" id="btnDeleteBatchFile">
						删除选中的图片</button>
					<button class="f-r ant-btn ant-btn-primary" id="btnMoveBatchFile">
						确定</button>
					<nz-select
						class="w-120 f-r ng-tns-c24-34 ant-select ant-select-enabled ng-untouched ng-pristine ng-valid">
						<div
							class="ng-tns-c24-34 ant-select-selection ant-select-selection--single"
							cdkoverlayorigin="" tabindex="0">   
							<select class="form-control m-b" 
								style="height:30px;line-height:30px;font-size:12px;" 
								id="moveCateSelect">
	                   		</select> 
						</div>
					</nz-select> 
					<span class="f-r mg-r-10 select-num" style="margin-top: 5px !important;">已选中 <em id="selectPicCount">0</em> 张 移动到</span>
				</div>
			</div>
            <!-- footer结束 --> 
		</div>
	</div>
</div>

</div>
<div id="editCateDiv">
	<div class="ibox float-e-margins" style="height:100%;">
		<div class="ibox-content">  
			<label class="col-sm-4 control-label">分组名称：</label>
			<div class="col-sm-6">
				<input type="text" itemId="" id="pic_cateName" value="" class="form-control" placeholder="请输入分组名称">
			</div>	         
			<div class="col-sm-8 col-sm-offset-4" style="margin-top:20px;"> 
            	<button class="btn btn-primary " type="button" id="btnSavePicCate"><i class="fa fa-check"></i>&nbsp;提交</button>
          		<button class="btn btn-white" type="button" id="btnCanclePicCate">取消</button>
        	</div>    
		</div>
	</div> 
</div>
<style>
#editCateDiv{
	display:none;  
}
</style>
<!-- 图片裁剪弹出框开始 --> 
<div id="tupiancaijian_box" style="display:none;">
	<div class="ant-modal-mask"></div>
	<div class="ant-modal-wrap">
		<div class="ant-modal">
			<div class="ant-modal-content">
				<button aria-label="Close" class="ant-modal-close ng-star-inserted cancleCropImg">
	                <span class="ant-modal-close-x fa fa-times"></span> 
	            </button>   
	            <div class="ant-modal-header ng-star-inserted" style="padding:13px 16px !important;">
	              	<div class="ant-modal-title" id="nzModal5">
	            	图片裁剪 
	              	</div>
	            </div>
	            <div class="ant-modal-body">
	            	<div class="modal-container">
	            		<!-- 左侧裁剪框开始 -->
	            		<div class="image-cropper">
	            			<!-- 动态宽度div -->
	            			<div class="image-cropper-inner">
	            				<div class="img_crop_box">   
	            					<div class="cropper-outer" style="width:100%;height:100%;">
	            						<img id="crop_img" src="http://zhuanmi.xzyouyou.com/Fuz5cHk7r66sVmgQRIbZCXV8_TiA" class="source_img">
	            					</div> 
	            				</div> 
	            			</div> 
	            		</div>
	            		<!-- 左侧裁剪框结束 -->
	            		
	            		<!-- 右侧选项框开始 -->
	            		<div class="crop-options">
							<div class="radio i-checks" style="margin-bottom:20px;">
								<label style="padding-left:10px;font-size:14px;"> 
									<input type="radio" checked value="1" name="crop_rate">
									<i></i> &nbsp;&nbsp;&nbsp;&nbsp;原图比例
								</label> 
							</div> 
							<div class="radio i-checks" style="margin-bottom:20px;">
								<label style="padding-left:10px;font-size:14px;"> 
									<input type="radio" value="2" name="crop_rate"> 
									<i></i> &nbsp;&nbsp;&nbsp;&nbsp;1:1
								</label>
							</div>
							<div class="radio i-checks" style="margin-bottom:20px;">
								<label style="padding-left:10px;font-size:14px;"> 
									<input type="radio" value="3" name="crop_rate"> 
									<i></i> &nbsp;&nbsp;&nbsp;&nbsp;4:3
								</label> 
							</div>
							<div class="radio i-checks" style="margin-bottom:20px;">
								<label style="padding-left:10px;font-size:14px;"> 
									<input type="radio" value="4" name="crop_rate">
									<i></i> &nbsp;&nbsp;&nbsp;&nbsp;3:4
								</label>
							</div>
							<div class="radio i-checks" style="margin-bottom:20px;">
								<label style="padding-left:10px;font-size:14px;"> 
									<input type="radio" value="5" name="crop_rate">
									<i></i> &nbsp;&nbsp;&nbsp;&nbsp;16:9
								</label>
							</div>
							<div class="radio i-checks" style="margin-bottom:20px;">
								<label style="padding-left:10px;font-size:14px;"> 
									<input type="radio" value="6" name="crop_rate">
									<i></i> &nbsp;&nbsp;&nbsp;&nbsp;自由拖动
								</label> 
							</div>
						</div>
	            		<!-- 右侧选项框结束 -->
	            	</div>
	            </div>
	            <!-- body结束 --> 
	            <div class="ant-modal-footer ng-star-inserted">
				    <div _ngcontent-c31="" class="ng-star-inserted">
				      	<button style="color: #fff;background-color: #108ee9;margin-right:10px;border-color: #108ee9;"
				      		 class="ant-btn ant-btn-primary" id="notCropImg">不裁剪 </button>
				      	<button class="ant-btn" id='confirmCropImg' style="margin-right:10px;color:#108ee9;border-color:#108ee9;">裁剪 </button>
				      	<button class="ant-btn" id="cancleCropImg" style="color:#108ee9;border-color:#108ee9;">取消 </button>
				    </div>    
            	</div>
			</div>
		</div>
	</div>
</div>
<!-- 图片裁剪弹出框结束 -->

<input type="hidden" value="" id="nowPicCateId">
<script> 
var reqPath = "${pageContext.request.contextPath}/";
var showPicModal = function(){
	$("#manageImgBox").show(); 
} 
var hidePicModal = function(){
	$("#tupiancaijian_box").hide();
	$("#manageImgBox").hide(); 
} 
</script>
<script src="${pageContext.request.contextPath}/admin/static/js/app/picManage.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/app/jcrop/jquery.Jcrop.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script>
	var jcropApi; 
    $(document).ready(function () {
        $(".i-checks").iCheck({checkboxClass: "icheckbox_square-green", radioClass: "iradio_square-green",})
        $('#crop_img').Jcrop({ 
        	 allowSelect: false,
        	  baseClass: 'jcrop', 
        	  addClass :"source_img2"
   		}, function() {
			//debugger;
        	jcropApi = this;
        });
    });
</script>