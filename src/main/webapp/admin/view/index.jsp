<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<!-- Mirrored from www.zi-han.net/theme/hplus/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:16:41 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title>后台管理系统</title>
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->

    <link rel="shortcut icon" href="favicon.ico">
    <link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/animate.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/app.css?v=4.1.0" rel="stylesheet">
</head>

<body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
    <div id="wrapper">
        <!--左侧导航开始-->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="nav-close"><i class="fa fa-times-circle"></i>
            </div>
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element" style="position:relative;">
                            <span class="head_img_box"><img alt="image" class="img-circle" src="${pageContext.request.contextPath}/admin/static/img/profile_small.jpg" /></span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear">
                                    <span class="block m-t-xs"><strong class="font-bold">${userList.name }</strong></span>
                                    <span class="text-muted text-xs block">${userList.userName }<b class="caret"></b></span>
                                </span>
                            </a>
                            <!--
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">

                                <li><a class="J_menuItem" href="form_avatar.html">修改头像</a>
                                </li>
                                <li><a class="J_menuItem" href="profile.html">个人资料</a>
                                </li>
                                <li><a class="J_menuItem" href="contacts.html">联系我们</a>
                                </li>
                                <li><a class="J_menuItem" href="mailbox.html">信箱</a>
                                </li>

                                <li class="divider"></li>
                                <li><a href="login.html">安全退出</a>
                                </li>
                            </ul>
                            -->
                        </div>
                        <div class="logo-element">悦采科技</div>
                    </li>
                    <c:forEach items="${menuListP1 }" var="item">
                    	<li>
                    		<a href="#"><i class="${item.icon }"></i>
                    			<span class="nav-label">${item.name }</span>
                    			<c:if test="${item.isHasChild== '1' }">
                    			<span class="fa arrow"></span>
                    			</c:if>
                    		</a>
                    		<c:if test="${item.isHasChild== '1' }">
                    		<ul class="nav nav-second-level">
	                    		<c:forEach items="${menuListP2 }" var="item2">
	                    			<c:if test="${item2.parentMenu.id==item.id }">
	                    			<li>
	                    				<a class="J_menuItem" href="${item2.url }">
	                    					<i class="${item2.icon }"></i>
	                    					${item2.name }
	                    					<c:if test="${item2.isHasChild== '1' }">
	                    					<span class="fa arrow"></span>
	                    					</c:if>
	                    				</a>
	                    				<c:if test="${item2.isHasChild== '1' }">
	                    				<ul class="nav nav-third-level">
	                    				<c:forEach items="${menuListP3 }" var="item3">
	                    					<c:if test="${item3.parentMenu.id==item2.id }">
		                    				<li>
	                                        <a class="J_menuItem" href="${item3.url }"><i class="${item3.icon }"></i>${item3.name }</a>
	                                    	</li>
	                                    	</c:if>
	                    				</c:forEach>
	                    				</ul>
	                    				</c:if>
	                            	</li>
	                            	</c:if>
                    			</c:forEach>
                    		</ul>
                    		</c:if>
                    	</li>
                    </c:forEach>
                </ul>
            </div>
        </nav>
        <!--左侧导航结束-->
        <!--右侧部分开始-->
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row content-tabs">
                <button class="roll-nav roll-left J_tabLeft"><i class="fa fa-backward"></i>
                </button>
                <nav class="page-tabs J_menuTabs">
                    <div class="page-tabs-content">
                        <a href="javascript:;" class="active J_menuTab" data-id="index_v1.html">首页</a>
                    </div>
                </nav>
                <button class="roll-nav roll-right J_tabRight"><i class="fa fa-forward"></i>
                </button>
                <div class="btn-group roll-nav roll-right">
                    <button class="dropdown J_tabClose" data-toggle="dropdown">关闭操作<span class="caret"></span>
                    </button>
                    <ul role="menu" class="dropdown-menu dropdown-menu-right">
                        <li class="J_tabShowActive"><a>定位当前选项卡</a>
                        </li>
                        <li class="divider"></li>
                        <li class="J_tabCloseAll"><a>关闭全部选项卡</a>
                        </li>
                        <li class="J_tabCloseOther"><a>关闭其他选项卡</a>
                        </li>
                    </ul>
                </div>
                <a class="right-sidebar-toggle roll-nav roll-right" aria-expanded="false">
                	<i class="fa fa-tasks"></i> 主题
                </a>
                <a href="login.html" class="roll-nav roll-right J_tabExit"><i class="fa fa fa-sign-out"></i> 退出</a>
            </div>
            <div class="row J_mainContent" id="content-main">
                <iframe class="J_iframe" name="iframe0" width="100%" height="100%" src="home" frameborder="0" data-id="index_v1.html" seamless></iframe>
            </div>
            <div class="footer">
            	<div class="pull-left" style="color:#999;">为了您能获得最佳的使用体验，请使用谷歌浏览器</div>
                <div class="pull-right">&copy; 2017~2018 <a href="javascript:;" target="_blank">某公司</a>
                </div>
            </div>
        </div>
        <!--右侧部分结束-->
        <!--右侧边栏开始-->
        <div id="right-sidebar">
            <div class="sidebar-container">

                <div id="tab-1" class="tab-pane active">
                        <div class="sidebar-title">
                            <h3> <i class="fa fa-comments-o"></i> 主题设置</h3>
                            <small><i class="fa fa-tim"></i> 你可以从这里选择和预览主题的布局和样式，这些设置会被保存在本地，下次打开的时候会直接应用这些设置。</small>
                        </div>
                        <div class="skin-setttings">
                            <div class="title">主题设置</div>
                            <div class="setings-item">
                                <span>收起左侧菜单</span>
                                <div class="switch">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="collapsemenu">
                                        <label class="onoffswitch-label" for="collapsemenu">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="setings-item">
                                <span>
                        		固定宽度
                    			</span>

                                <div class="switch">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="boxedlayout" class="onoffswitch-checkbox" id="boxedlayout">
                                        <label class="onoffswitch-label" for="boxedlayout">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="title">皮肤选择</div>
                            <div class="setings-item default-skin nb">
                                <span class="skin-name ">
                         <a href="#" class="s-skin-0">
                             	默认皮肤
                         </a>
                    </span>
                            </div>
                            <div class="setings-item blue-skin nb">
                                <span class="skin-name ">
                        <a href="#" class="s-skin-1">
                            	蓝色主题
                        </a>
                    </span>
                            </div>
                            <div class="setings-item yellow-skin nb">
                                <span class="skin-name ">
                        <a href="#" class="s-skin-3">
                           	 黄色/紫色主题
                        </a>
                    </span>
                            </div>
                        </div>
                    </div>


            </div>
        </div>
        <!--右侧边栏结束-->
    </div>

    <audio id="tips_music" src="${pageContext.request.contextPath}/admin/assets/8868.mp3"></audio>

    <script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
    <script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="${pageContext.request.contextPath}/admin/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="${pageContext.request.contextPath}/admin/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
    <script src="${pageContext.request.contextPath}/admin/static/js/hplus.min.js?v=4.1.0"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/admin/static/js/contabs.min.js"></script>
    <script src="${pageContext.request.contextPath}/admin/static/js/plugins/pace/pace.min.js"></script>
    <script type="text/javascript">
    	var currDialogWindow;
    	function flyAlert(msg){
    		layer.alert(msg);
    	}
    	function flyMsg(msg){
    		layer.msg(msg);
    	}
    	function flyConfirm(msg,suc){
    		layer.confirm(msg,function(index){
    			suc();
        	});
    	}
    	function openDialog(title,url,area,type,obj){
    		var tempArea;
    		if(area){
    			tempArea = area;
    		}
    		else{
    			tempArea = ['800px', '600px'];
    		}
    		currDialogWindow = obj;
    		var index = layer.open({
    			type: 2,
    			title: title,
    			content: url,
    			area: tempArea
    		});
    		if(type == '1'){
    			layer.full(index);
    		}
    	}

    	//操作cookie的对象
    	var cookie = {
    		set: function(name, value) {
    			var Days = 30;
    			var exp = new Date();
    			exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
    			document.cookie = name + '=' + escape(value) + ';expires=' + exp.toGMTString();
    		},
    		get: function(name) {
    			var arr, reg = new RegExp('(^| )' + name + '=([^;]*)(;|$)');
    			if(arr = document.cookie.match(reg)) {
    				return unescape(arr[2]);
    			} else {
    				return null;
    			}
    		},
    		del: function(name) {
    			var exp = new Date();
    			exp.setTime(exp.getTime() - 1);
    			var cval = getCookie(name);
    			if(cval != null) {
    				document.cookie = name + '=' + cval + ';expires=' + exp.toGMTString();
    			}
    		}
    	};


    	var tips_music = document.getElementById("tips_music");
    	var lastGetTime = cookie.get("lastGetTime");
    	if(!lastGetTime || lastGetTime == undefined) { //如果没有记录值，则设置时间0开始播放
        	lastGetTime = "";
        }
        var orderNewsTip = function(){
        	console.log("定时请求最新注册用户");
        	$.getJSON("${pageContext.request.contextPath}/getLastNewUser.do?lastGetTime=" + lastGetTime,function(json){
        		console.log(json);
        		if(json.code == 0){
        			tips_music.play();
        		}
        		else{
        			console.log("无最新注册用户需要处理");
        		}
        		lastGetTime = json.lastGetTime;
    			cookie.set("lastGetTime",lastGetTime);
        	});
        }
        orderNewsTip();
    </script>
</body>
</html> 
