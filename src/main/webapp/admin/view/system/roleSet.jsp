<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>角色权限设置</title>

    <link rel="shortcut icon" href="favicon.ico"> 
       <link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/admin/static/js/plugins/ztree/bootstrapStyle.css" type="text/css">
</head> 

<body class="gray-bg"> 
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
    	<div class="col-sm-12">  
    		<div class="ibox float-e-margins" style="height:100%;">
                <div class="ibox-content"> 
                	<form class="form-horizontal">
					    <div class="form-group"> 
								<div class="col-md-12">
									<input type="hidden" value="${a.id }" id="id">
									<ul id="treeDemo" class="ztree"></ul>
									
								</div>
						</div>
                        <div class="form-group"> 
                        	<div class="col-sm-12 col-sm-offset-3"> 
                                <button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;提交</button>
                                <button class="btn btn-white" type="button" id="btnCancle">取消</button>
                            </div>   
		               </div>  
					</form> 
                </div> 
            </div> 
    	</div> 
    </div>
</div>

<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/static/js/plugins/ztree/jquery.ztree.core.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/static/js/plugins/ztree/jquery.ztree.excheck.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/static/js/plugins/ztree/jquery.ztree.exedit.js"></script>
<script>   
$(function(){
	var setting = {
            view: { 
                selectedMulti: false
            },
            check: {
                enable: true
            },
            data: {
                simpleData: {
                    enable: true 
                }
            },
            edit: {
                enable: false 
            }
        };  
	$.getJSON("${pageContext.request.contextPath}/admin/role/loadRoleSet?roleId=${a.id}",function(json){
		console.log(json); 
		$.fn.zTree.init($("#treeDemo"), setting, json.list);
	});
	$("#btnCancle").click(function(){
		//关闭操作
		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
		parent.layer.close(index); //再执行关闭  
	});       
	$("#btnSave").click(function(){ 
		var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
		var nodes = treeObj.getCheckedNodes(true);
		var len = nodes.length; 
		var menuIdArr = [];
		for(var i = 0; i < len; i++){
			var node = nodes[i];  
			menuIdArr.push(node.id);
		}
		var id = $("#id").val();
		$.post("${pageContext.request.contextPath}/admin/role/saveRoleMenu",{
			id : id,
			menuIdArr : menuIdArr.join(",") 
		},function(json){
			if(json.code == 0){ 
				layer.alert("操作成功");
				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
				parent.layer.close(index); //再执行关闭 
				parent.layer.alert("操作成功");
			}
			else{ 
				layer.alert("操作失败");
			}
		},"json");
	}); 
}); 
</script>
</body>
</html>
