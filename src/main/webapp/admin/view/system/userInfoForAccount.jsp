<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>用户编辑</title>
    <link rel="shortcut icon" href="favicon.ico"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
 
</head> 

<body class="gray-bg"> 
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
    	<div class="col-sm-12">  
    		<div class="ibox float-e-margins" style="height:100%;">
                <div class="ibox-content"> 
                	<form class="form-horizontal">
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">用户名：</label>
					        <div class="col-sm-6">
					        	<input type="hidden" value="${a.id }" id="id">  
					        	<input type="hidden" value="${accountId}" id="accountId">  
					            <input type="text" id="userName" value="${a.userName }" class="form-control" placeholder="请输入用户名">
					        </div> 
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 control-label">姓名：</label>
					        <div class="col-sm-6">
					            <input type="text" id="name" value="${a.name }" class="form-control" placeholder="请输入姓名">
					        </div> 
					    </div> 
					    <div class="form-group">
					        <label class="col-sm-3 control-label">邮箱地址：</label>
					        <div class="col-sm-6"> 
					            <input type="text" id="email" value="${a.email }" class="form-control" placeholder="请输入邮箱地址">
					        </div> 
					    </div> 
					    <c:if test="${empty a }">
					    <div class="form-group">
					        <label class="col-sm-3 control-label">登录密码：</label>
					        <div class="col-sm-6">
					           	<input type="password" id="password" value="" class="form-control" placeholder="请输入密码">
					        </div> 
					    </div> 
					    <div class="form-group">
					        <label class="col-sm-3 control-label">确认密码：</label>
					        <div class="col-sm-6">
					            <input type="password" id="password2" value="" class="form-control" placeholder="请再次输入密码">
					        </div> 
					    </div> 
					    </c:if>
					    <div class="form-group draggable ui-draggable">
                            <div class="col-sm-12 col-sm-offset-3"> 
                                <button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;提交</button>
                                <button class="btn btn-white" type="button" id="btnCancle">取消</button>
                            </div>
                        </div>
					</form> 
                </div> 
            </div> 
    	</div> 
    </div>
</div>

<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script>   
    $(document).ready(function () {
    	var id = "${a.id}";
    	$("#btnSave").click(function(){
    		var id = $("#id").val();
    		var name = $("#name").val();
    		var userName = $("#userName").val();
    		var email = $("#email").val();
    		var password = $("#password").val();
    		var password2 = $("#password2").val();
    		var accountId = $("#accountId").val();
    		if(name == null || name == ""){ 
    			layer.alert("请输入用户姓名");
    			return false; 
    		}
    		if(userName == null || userName == ""){ 
    			layer.alert("请输入用户名");
    			return false; 
    		}
    		if(email == null || email == ""){ 
    			layer.alert("请输入邮箱地址");
    			return false; 
    		}
    		var pattern = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    		if(!pattern.test(email)){
    			layer.msg("请检查email格式是否正确");
    			return false;
    		} 
    		if("" == id){
    			if(password == null || password == ""){ 
        			layer.alert("请输入密码");
        			return false; 
        		}
        		if(password2 == null || password2 == ""){ 
        			layer.alert("请再次输入密码");
        			return false; 
        		}
        		if(password != password2){
        			layer.alert("您两次输入的密码不一致");
        			return false;
        		}
    		}
    		
    		$.post("${pageContext.request.contextPath}/admin/user/saveUser",{
    			id : id,
    			name : name, 
    			userName : userName,
    			email : email,
    			password : password,
    			role : 2,
    			accountId : accountId,
    			identity : 3 
    		},function(json){
    			if(json.code == 0){ 
    				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
    				parent.layer.close(index); //再执行关闭 
    				parent.layer.alert("操作成功");
    				parent.currDialogWindow.$("#queryBtn").click();
    			}
    			else if(json.code == -1){ 
    				layer.alert("您输入的用户名已经存在，请重新输入");
    			}
    			else if(json.code == -2){ 
    				layer.alert("您输入的Email账号已经存在，请重新输入");
    			}
    			else{   
    				layer.alert("操作失败");
    			}
    		},"json");
    	});
		$("#btnCancle").click(function(){
			//关闭操作
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭  
		});      
    });
</script>
</body>
</html>
