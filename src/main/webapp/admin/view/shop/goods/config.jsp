<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>商城配置</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/tagsinput/bootstrap-tagsinput.css"
          rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/layui/css/layui.css?v=4.1.0"
          rel="stylesheet">
    <style type="text/css">
        .bootstrap-tagsinput .label {
            font-size: 12px;
        }

        .layui-upload-img {
            width: 92px;
            height: 92px;
            margin: 0 10px 10px 0;
        }
    </style>
    <link href="${pageContext.request.contextPath}/admin/static/css/app.css?v=4.1.0" rel="stylesheet">
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>商城配置</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">最低金额限制：</label>
                            <div class="col-sm-6">
                                <input type="text" style="width:100%;" id="xianzhi" value="${a.xianzhi}"
                                       class="form-control" placeholder="商城金额限制">
                                <span class="help-block" style="color:#FFB800;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">首页跑马灯公告：</label>
                            <div class="col-sm-6">
                                <input type="hidden" value="${a.id }" id="id">
                                <textarea id="notice" style="height:100px;" name="notice"
                                          class="form-control">${a.notice}</textarea>
                                <span class="help-block" style="color:#FFB800;">首页跑马灯公告</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">客服电话：</label>
                            <div class="col-sm-6">
                                <input type="text" style="width:100%;" id="mobile" value="${a.mobile }"
                                       class="form-control" placeholder="客服电话">
                                <span class="help-block" style="color:#FFB800;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">商品说明：</label>
                            <div class="col-sm-6">
                                <textarea id="goodsRule" style="height:100px;" name="goodsRule"
                                          class="form-control">${a.goodsRule }</textarea>
                                <span class="help-block" style="color:#FFB800;">商品详情页面，商品说明</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">退款原因：</label>
                            <div class="col-sm-6">
                                <input type="text" style="width:100%;" id="refoudReason" value="${a.refoudReason }"
                                       class="form-control tagsinput" placeholder="请输入退款原因">
                                <span class="help-block" style="color:#FFB800;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">退换货原因：</label>
                            <div class="col-sm-6">
                                <input type="text" style="width:100%;" id="afterReason" value="${a.afterReason }"
                                       class="form-control tagsinput" placeholder="请输入退换货原因">
                                <span class="help-block" style="color:#FFB800;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">取消订单原因：</label>
                            <div class="col-sm-6">
                                <input type="text" style="width:100%;" id="cancleReason" value="${a.cancleReason }"
                                       class="form-control tagsinput" placeholder="请输入取消订单原因">
                                <span class="help-block" style="color:#FFB800;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">订单超时时间：</label>
                            <div class="col-sm-6">
                                <input type="text" style="width:100%;" id="cancleOrderTime"
                                       value="${a.cancleOrderTime }"
                                       class="form-control" placeholder="订单超时取消时间">
                                <span class="help-block" style="color:#FFB800;">单位为小时</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">自动收货周期：</label>
                            <div class="col-sm-6">
                                <input type="text" style="width:100%;" id="receiverDay" value="${a.receiverDay }"
                                       class="form-control" placeholder="自动收货周期，团长收货后，多少天，用户自动收货">
                                <span class="help-block" style="color:#FFB800;">单位为天</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">收货后售后有效期：</label>
                            <div class="col-sm-6">
                                <input type="text" style="width:100%;" id="salesAfterDay" value="${a.salesAfterDay }"
                                       class="form-control" placeholder="收货后，多少天内，可以申请售后">
                                <span class="help-block" style="color:#FFB800;">单位为天</span>
                            </div>
                        </div>
                        <div class="form-group" style="display:none;">
                            <label class="col-sm-3 control-label">客户端小程序appid：</label>
                            <div class="col-sm-6">
                                <input type="text" style="width:100%;" id="appid" value="${a.appid }"
                                       class="form-control" placeholder="客户端小程序appid">
                                <span class="help-block" style="color:#FFB800;"></span>
                            </div>
                        </div>
                        <div class="form-group" style="display:none;">
                            <label class="col-sm-3 control-label">客户端小程序appsecret：</label>
                            <div class="col-sm-6">
                                <input type="text" style="width:100%;" id="appSecret" value="${a.appSecret }"
                                       class="form-control" placeholder="客户端小程序appsecret">
                                <span class="help-block" style="color:#FFB800;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">商户id：</label>
                            <div class="col-sm-6">
                                <input type="text" style="width:100%;" id="mchId" value="${a.mchId }"
                                       class="form-control" placeholder="商户id">
                                <span class="help-block" style="color:#FFB800;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">支付秘钥：</label>
                            <div class="col-sm-6">
                                <input type="text" style="width:100%;" id="mchSecret" value="${a.mchSecret }"
                                       class="form-control" placeholder="请输入支付秘钥">
                                <span class="help-block" style="color:#FFB800;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">支付证书目录：</label>
                            <div class="col-sm-3">
                                <input type="text" disabled style="width:100%;" id="certPath" value="${a.certPath }"
                                       class="form-control" placeholder="支付证书文件路径">
                                <span class="help-block" style="color:#FFB800;"></span>
                            </div>
                            <div class="col-sm-3">
                                <button class="btn btn-success" type="button" id="btnUpload">
                                    <i class="fa fa-file"></i>&nbsp;上传
                                </button>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">分享标题：</label>
                            <div class="col-sm-6">
                                <input type="text" style="width:100%;" id="shareTitle" value="${a.shareTitle }"
                                       class="form-control" placeholder="分享标题">
                                <span class="help-block" style="color:#FFB800;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">分享图片：</label>
                            <div class="col-sm-6">
                                <input type="hidden" id="shareImgUrl" value="${a.shareImgUrl }" class="form-control">
                                <div class="goods_row_wrap m-t m-b">
                                    <div id="goods_imgUrl_div_box" class="add-pic corner-pic m-r">
                                        <c:if test="${empty a.shareImgUrl }">分享图片</c:if>
                                        <c:if test="${not empty a.shareImgUrl }">
                                            <img src="${a.shareImgUrl }">
                                        </c:if>
                                    </div>
                                    <div id="uploadGoodsZhutu"
                                         class="add-pic add-pic-btn add-corner-pic ng-star-inserted">
                                        <i class="fa fa-plus"></i>
                                        上传分享图片
                                    </div>
                                    <span class="pic-txt m-l">
                						推荐使用1000*800像素的jpg图片
              						</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group draggable ui-draggable">
                            <div class="col-sm-12 col-sm-offset-3">
                                <button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;提交
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/tagsinput/bootstrap-tagsinput.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layui/layui.js"></script>
<script>
    var sucCheckImg;
    layui.use(['upload'], function () {
        var upload = layui.upload;
        //执行实例
        var uploadInst = upload.render({
            elem: '#btnUpload' //绑定元素
            , field: "certFile"
            , accept: "file"
            , url: '${pageContext.request.contextPath}/admin/shop/config/saveUploadCertPath' //上传接口
            , done: function (res) {
                //上传完毕回调
                $("#certPath").val(res.url);
            }
            , error: function () {
                //请求异常回调
            }
        });
        $("#uploadGoodsZhutu").click(function () {
            showPicModal();
            sucCheckImg = function (url) {
                $("#goods_imgUrl_div_box").html('<img src="' + url + '">');
                $("#shareImgUrl").val(url);
                hidePicModal();
            }
        });
    });
    $(document).ready(function () {
        $(".tagsinput").tagsinput();

        $("#btnSave").click(function () {
            var id = $("#id").val();
            var notice = $("#notice").val();
            var goodsRule = $("#goodsRule").val();
            var cancleReason = $("#cancleReason").val();
            var refoudReason = $("#refoudReason").val();
            var afterReason = $("#afterReason").val();
            var cancleOrderTime = $("#cancleOrderTime").val();
            var salesAfterDay = $("#salesAfterDay").val();
            var receiverDay = $("#receiverDay").val();
            var appid = $("#appid").val();
            var appSecret = $("#appSecret").val();
            var mchId = $("#mchId").val();
            var mchSecret = $("#mchSecret").val();
            var certPath = $("#certPath").val();
            var shareImgUrl = $("#shareImgUrl").val();
            var shareTitle = $("#shareTitle").val();
            var mobile = $("#mobile").val();
            var xianzhi = $("#xianzhi").val();
            if (cancleOrderTime == null || cancleOrderTime == "") {
                layer.alert("请输入订单超时时间");
                return false;
            }
            $.post("${pageContext.request.contextPath}/admin/shop/config/saveConfig", {
                id: id,
                notice: notice,
                goodsRule: goodsRule,
                cancleReason: cancleReason,
                refoudReason: refoudReason,
                afterReason: afterReason,
                cancleOrderTime: cancleOrderTime,
                receiverDay: receiverDay,
                salesAfterDay: salesAfterDay,
                appid: appid,
                appSecret: appSecret,
                mchId: mchId,
                mchSecret: mchSecret,
                certPath: certPath,
                shareTitle: shareTitle,
                shareImgUrl: shareImgUrl,
                mobile: mobile,
                xianzhi: xianzhi
            }, function (json) {
                if (json.code == 0) {
                    layer.alert("操作成功");
                } else {
                    layer.alert("操作失败");
                }
            }, "json");
        });
        $("#btnCancle").click(function () {
            //关闭操作
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            parent.layer.close(index); //再执行关闭
        });

    });
</script>
<jsp:include page="../../common/picManage.jsp"></jsp:include>
</body>
</html>
