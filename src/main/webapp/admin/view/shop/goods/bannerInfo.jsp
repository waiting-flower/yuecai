<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>商城轮播</title>
    <link rel="shortcut icon" href="favicon.ico"> 
   	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/layui/css/layui.css?v=4.1.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/app.css?v=4.1.0" rel="stylesheet"> 
    <style type="text/css">
    .layui-upload-img {
	    width: 92px;
	    height: 92px;
	    margin: 0 10px 10px 0;
	} 
    </style>
   	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/admin/static/plugins/kindeditor/themes/default/default.css">
	<script charset="utf-8" src="${pageContext.request.contextPath}/admin/static/plugins/kindeditor/kindeditor-all-min.js"></script>
	<script charset="utf-8" src="${pageContext.request.contextPath}/admin/static/plugins/kindeditor/lang/zh-CN.js"></script>
 	<script type="text/javascript">
		var imgArr = [];//初始化图片数组
		var editor;
		KindEditor.ready(function(K) {
			editor = K.create('textarea[id="content"]',{
				allowFileManager : true,
				uploadJson : '${pageContext.request.contextPath}/admin/view/common/upload_json.jsp',
		        fileManagerJson : '${pageContext.request.contextPath}/admin/view/common/file_manager_json.jsp'
			});
		});
	</script>
</head> 

<body class="gray-bg"> 
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
    	<div class="col-sm-12">  
    		<div class="ibox float-e-margins" style="height:100%;">
                <div class="ibox-content"> 
                	<form class="form-horizontal">
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">轮播标题：</label>
					        <div class="col-sm-6">
					        	<input type="hidden" value="${a.id }" id="id">  
					            <input type="text" id="title" value="${a.title }"
					            	 class="form-control" placeholder="请输入轮播标题">
					        </div>
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 control-label">轮播图片：</label>
					        <div class="col-sm-9"> 
					            <input type="hidden" id="imgUrl" value="${a.imgUrl }" class="form-control" placeholder="值越大越靠前">
					            <div class="goods_row_wrap m-t m-b"> 
					        		<div id="goods_imgUrl_div_box" class="add-pic corner-pic m-r">
                						<c:if test="${empty a.imgUrl }">轮播图片</c:if> 
                						<c:if test="${not empty a.imgUrl }">
                						<img src="${a.imgUrl }">
                						</c:if>
              						</div>   
              						<div id="uploadGoodsZhutu" class="add-pic add-pic-btn add-corner-pic ng-star-inserted">
						                <i class="fa fa-plus"></i>
						                	上传轮播图片  
						            </div> 
						            <span class="pic-txt m-l">
                						推荐使用800*350像素的jpg图片
              						</span>   
					        	</div>    
					        </div> 
					    </div> 
					    <div class="form-group">
					        <label class="col-sm-3 control-label">点击事件：</label>
					        <div class="col-sm-3"> 
					            <select class="form-control" id="target">
					            	<option value="none">无</option>
					            	<option value="info">轮播详情</option>
					            	<option value="action">小程序页面</option>
					            </select> 
					        </div> 
					    </div>
					    <div id="xcx_div_box" style="display:none;">
						    <div class="form-group">
						        <label class="col-sm-3 control-label">小程序页面路径：</label>
						        <div class="col-sm-3">
						            <input type="text" id="pagePath" value="${a.pagePath }" 
						            	class="form-control" placeholder="请输入小程序页面路径，例如/pages/index/index">
						        </div> 
						    </div> 
						    <div class="form-group">
						        <label class="col-sm-3 control-label">小程序页面参数：</label>
						        <div class="col-sm-3">
						            <input type="text" id="pageOptions"
						            	 value="${a.pageOptions }" class="form-control" placeholder="请输入小程序页面参数">
						        </div> 
						    </div> 
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 control-label">显示排序：</label>
					        <div class="col-sm-3">
					            <input type="text" id="orderNum" value="${a.orderNum }" class="form-control" placeholder="值越大越靠前">
					        </div> 
					    </div>  
					    <div class="form-group" id="bannerInfo"> 
					        <label class="col-sm-3 control-label">轮播详情：</label>
					        <div class="col-sm-6"> 
					            <textarea class="form-control" style="height:450px;" id="content">${a.info }</textarea>
					        </div> 
					    </div>
					    <div class="form-group draggable ui-draggable">
                            <div class="col-sm-12 col-sm-offset-3"> 
                                <button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;提交</button>
                                <button class="btn btn-white" type="button" id="btnCancle">取消</button>
                            </div>
                        </div>
					</form> 
                </div> 
            </div> 
    	</div> 
    </div>
</div>

<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layui/layui.js"></script>
<script>   
var sucCheckImg;
layui.use(['layer'], function(){
});  	
    $(document).ready(function () {
    	//轮播图片上传 
    	$("#uploadGoodsZhutu").click(function(){
    		showPicModal(); 
    		sucCheckImg = function(url){
    			$("#goods_imgUrl_div_box").html('<img src="' + url + '">');
    			$("#imgUrl").val(url);
    			hidePicModal();   
    		}   
    	});
    	var target = "${a.target}";
    	if(target != "info"){
    		$("#bannerInfo").hide();  
    	} 
    	if(target == "action"){
    		$("#xcx_div_box").show();  
    	} 
    	$("#target").change(function(){
    		var target = $(this).val();
    		if(target == "action"){
    			$("#xcx_div_box").show();
    			$("#bannerInfo").hide();
    		}
    		else if(target == "info"){
    			$("#bannerInfo").show();
    			$("#xcx_div_box").hide();
    		}
    		else{
    			$("#bannerInfo").hide();
    			$("#xcx_div_box").hide();
    		} 
    	});
    	if(target != ""){
    		$("#target").val(target);
    	} 
  		var pl = parent.layer;
  		var pw = parent.currDialogWindow;
    	$("#btnSave").click(function(){
    		var id = $("#id").val();
    		var title = $("#title").val();
    		var target = $("#target").val();
    		var pagePath = $("#pagePath").val();
    		var pageOptions = $("#pageOptions").val();
    		var info = editor.html();
    		var orderNum = $("#orderNum").val();
    		var imgUrl = $("#imgUrl").val();
    		if(title == null || title == ""){ 
    			layer.alert("请输入轮播标题");
    			return false; 
    		}
    		if(imgUrl == null || imgUrl == ""){ 
    			layer.alert("请上传轮播图片");
    			return false; 
    		}
    		if(orderNum == null || orderNum == ""){
    			orderNum = 1;
    		}
    		if(target == "info"){
    			if(info == ""){
    				layer.msg("请输入轮播图文描述");
    				return false;
    			}
    		}
    		else if(target == "action"){
    			if(pagePath == ""){
    				layer.msg("请输入小程序页面路径");
    				return false;
    			}
    			if(pageOptions == ""){
    				layer.msg("请输入小程序页面路径");
    				return false;
    			}
    		} 
    		$.post("${pageContext.request.contextPath}/admin/shop/banner/saveBanner",{
    			id : id,
    			title : title, 
    			orderNum : orderNum,
    			target : target,
    			imgUrl : imgUrl,
    			info : info,
    			pagePath : pagePath,
    			pageOptions : pageOptions
    		},function(json){
    			if(json.code == 0){  
    				var index = pl.getFrameIndex(window.name); //先得到当前iframe层的索引
    				pl.close(index); //再执行关闭 
    				pl.alert("操作成功");
    				pw.$("#queryBtn").click();
    			}  
    			else{   
    				layer.alert("操作失败");
    			}
    		},"json");
    	});
		$("#btnCancle").click(function(){
			//关闭操作
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭  
		}); 
    	
    });
</script>
<jsp:include page="../../common/picManage.jsp"></jsp:include>
</body>
</html>
