<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>商城商品管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/layui/css/layui.css?v=4.1.0"
          rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">

    <!-- Data Tables -->
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/dataTables/dataTables.bootstrap.css"
          rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form role="form" class="form-inline">
                        <div class="input-group" style="margin-top:-5px;">
                            <select class="form-control m-b" name="state" id="isOnSale">
                                <option value="">商品状态</option>
                                <option value='1'>上架中</option>
                                <option value='2'>已下架</option>
                            </select>
                        </div>
                        <div class="input-group" style="margin-top:-5px;">
                            <select class="form-control m-b" name="isDiscount" id="isDiscount">
                                <option value="">是否折扣</option>
                                <option value='1'>折扣商品</option>
                                <option value='0'>无折扣</option>
                            </select>
                        </div>
                        <div class="input-group" style="margin-top:-5px;">
                            <select class="form-control m-b" name="isRec" id="isRec">
                                <option value="">是否热销</option>
                                <option value='1'>热销推荐</option>
                                <option value='0'>非热销</option>
                            </select>
                        </div>
                        <div class="input-group " style="margin-top:-5px;">
                            <select class="form-control m-b" name="state" id="cate">
                                <option value="">请选择商品类别</option>
                                <c:forEach items="${cateList1 }" var="item">
                                    <option value='${item.id }' disabled>${item.name }</option>
                                    <c:forEach items="${cateList2 }" var="item2">
                                        <c:if test="${item2.parentCate.id==item.id }">
                                            <option value='${item2.id }'>
                                                &nbsp;&nbsp;&nbsp;&nbsp;├─${item2.name }</option>
                                        </c:if>
                                    </c:forEach>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="input-group ">
                            <input type="text" class="form-control" id="name"
                                   placeholder="请输入商品名称"> <span
                                class="input-group-btn ">
												<button type="button" class="btn btn-primary" id="queryBtn">搜索</button>
											</span>
                        </div>
                        <button type="button" class="btn btn-w-m btn-success"
                                id="addBtn">
                            <i class="fa fa-plus"></i>&nbsp;&nbsp;发布新商品
                        </button>
                        <button type="button" class="btn btn-w-m btn-info"
                                id="shaixuanBtn" style="display: none;">
                            <i class="fa fa-angle-down"></i>&nbsp;&nbsp;筛选
                        </button>
                    </form>
                    <div id="collapseOne" class="panel-collapse collapse ">
                        <div class="panel-body">Bootstrap相关优质项目推荐
                            这些项目或者是对Bootstrap进行了有益的补充，或者是基于Bootstrap开发的
                        </div>
                    </div>
                    <table id="table1"
                           class="table table-striped table-bordered table-hover dataTables-example"
                           style="width: 100%">
                        <thead>
                        <tr>
                            <th>商品名称</th>
                            <th>商品图片</th>
                            <th>所属类目</th>
                            <th>排序</th>
                            <th>价格</th>
                            <th>浏览量</th>
                            <th>总销量</th>
                            <th>总库存</th>
                            <th>是否热销</th>
                            <th>特价折扣</th>
                            <th>商品状态</th>
                            <th>更新时间</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layui/layui.js"></script>

<script>
    layui.use('element', function () {
        var $ = layui.jquery
            , element = layui.element; //Tab的切换功能，切换事件监听等，需要依赖element模块
    });
    var dataTable;
    var _this = this;
    $(document).ready(function () {
        var laguage = {
            "sLengthMenu": "每页显示 _MENU_ 条记录",
            "sZeroRecords": "抱歉， 没有找到",
            "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
            "sInfoEmpty": "没有数据",
            "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
            "oPaginate": {
                "sFirst": "首页",
                "sPrevious": "前一页",
                "sNext": "后一页",
                "sLast": "尾页"
            },
            "sZeroRecords": "您请求的内容暂时没有数据",
            "sProcessing": "数据加载中..."
        };
        var aoColumns = [{
            "mData": "name"
        }, {
            "mData": "imgUrl",
            "mRender": function (data, type, full) {
                return '<img src="' + data + '" style="width:20px;height:20px;">'
            }
        }, {
            "mData": "cate"
        }, {
            "mData": "sort",
            "mRender": function (data, type, full) {
                return '<div style="text-align: center" type="text" ondblclick="editsort(' + full.id + ')" class="">' + data + '</div>'
            }
        }, {
            "mData": "price"
        }, {
            "mData": "lookNum"
        }, {
            "mData": "saleNum"
        }, {
            "mData": "stockNum"
        }, {
            "mData": "isRec",
            "mRender": function (data, type, full) {
                if (data == "1") {
                    //热销推荐
                    return '<button onclick="cancleRecInfo(' + full.id + ')" class="btn btn-primary btn-xs"><i class="fa fa-heart"></i>热销</button>'
                } else {
                    return '<button onclick="setRecInfo(' + full.id + ')" class="btn btn-danger btn-xs"><i class="fa fa-heart-o"></i>否</button>'
                }
            }
        }, {
            "mData": "isDiscount",
            "mRender": function (data, type, full) {
                if (data == "1") {
                    //热销推荐
                    return '<button onclick="cancleDiscountInfo(' + full.id + ')" class="btn btn-primary btn-xs"><i class="fa fa-circle"></i>折扣' + full.discount + '</button>'
                } else {
                    return '<button onclick="setDiscountInfo(' + full.id + ')" class="btn btn-danger btn-xs"><i class="fa fa-circle-o"></i>无折扣</button>'
                }
            }
        }, {
            "mData": "isOnSale",
            "mRender": function (data, type, full) {
                if (data == "1") {
                    //热销推荐
                    return '上架中<br>' +
                        '<button onclick="setGoodsXiajia(' + full.id + ')" class="btn btn-warning btn-xs"><i class="fa fa-times"></i>下架</button>'
                } else {
                    return '已下架<br>' +
                        '<button onclick="setGoodsShangjia(' + full.id + ')" class="btn btn-warning btn-xs"><i class="fa fa-times"></i>上架</button>'
                }
            }
        }, {
            "mData": "time"
        }, {
            "mData": "id",
            "mRender": function (data, type, full) {
                return '<button onclick="deleteInfo(' + data + ')" class="btn btn-danger btn-xs"><i class="fa fa-times"></i>删除</button>'
                    + '<button onclick="editInfo(' + data + ')" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i>编辑</button>';
            }
        }];
        dataTable = $('#table1').dataTable({
            "dom": 'rt<"bottom"p>l<"clear">i<"clear">',
            "aoColumns": aoColumns,
            "oLaguage": laguage,
            "bServerSide": true,
            "bFilter": false,
            "bProcessing": true,
            "bSort": true,
            "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1, 2, 7, 8, 9, 10, 11]}],
            /* 使用post方式 */
            "iDisplayLength": 10,
            "sAjaxSource": '${pageContext.request.contextPath}/admin/shop/goods/loadGoodsByPage',
            "fnServerParams": function (aoData) {  //查询条件
                aoData.push(
                    {"name": "name", "value": $("#name").val()},
                    {"name": "isOnSale", "value": $("#isOnSale").val()},
                    {"name": "cate", "value": $("#cate").val()},
                    {"name": "isRec", "value": $("#isRec").val()},
                    {"name": "isDiscount", "value": $("#isDiscount").val()}
                );
            },
            "fnServerData": function (sSource, aoData, fnCallback) {
                $.ajax({
                    "dataType": 'json',
                    "type": "post",
                    "url": sSource,
                    "data": aoData,
                    "success": function (json) {
                        $("#iTotalRecords").html(json.iTotalRecords);
                        fnCallback(json);
                        $(".dataTables-example").css("width", "100%");
                    }
                });
            }
        });
        $("#queryBtn").click(function () {
            dataTable.fnDraw();
        });
        $("#addBtn").click(function () {
            parent.openDialog("发布商品", "${pageContext.request.contextPath}/admin/shop/goods/info", ['800px', '550px'], 1, _this);
        });
        $("#shaixuanBtn").click(function () {
            $("#collapseOne").toggleClass("in");
            if ($("#collapseOne").hasClass("in")) {
                $("#shaixuanBtn i").addClass("fa-angle-up").removeClass("fa-angle-down");
            } else {
                $("#shaixuanBtn i").removeClass("fa-angle-up").addClass("fa-angle-down");
            }
        });
    });

    function editInfo(data) {
        parent.openDialog("编辑商品", "${pageContext.request.contextPath}/admin/shop/goods/info?id=" + data, ['800px', '550px'], 1, _this);
    }

    function deleteInfo(data) {
        parent.flyConfirm('确认要删除该商品吗吗，删除之后无法恢复，请谨慎操作？', function (index) {
            $.post("${pageContext.request.contextPath}/admin/shop/goods/deleteGoods?id=" + data, function (json) {
                if (json.code == 0) {
                    parent.flyMsg("删除成功");
                    $("#queryBtn").click();
                } else {
                    parent.flyMsg("删除失败");
                }
            }, "json");
        });
    }

    function editsort(data) {
        layer.prompt({title: '请输入商品的排序，数字越大排序越靠前。', formType: 2}, function (text, index) {
            layer.close(index);
            var sort = text;
            $.post("${pageContext.request.contextPath}/admin/shop/goods/saveGoodsSort?id=" + data + "&sort=" + sort, function (json) {
                if (json.code == 0) {
                    parent.flyMsg("设置成功");
                    $("#queryBtn").click();
                } else {
                    parent.flyMsg("设置失败");
                }
            }, "json");
        });
    }

    function cancleRecInfo(data) {
        parent.flyConfirm('确认要取消设置该商品的热销推荐吗？', function (index) {
            $.post("${pageContext.request.contextPath}/admin/shop/goods/saveGoodsRec?state=0&id=" + data, function (json) {
                if (json.code == 0) {
                    parent.flyMsg("设置成功");
                    $("#queryBtn").click();
                } else {
                    parent.flyMsg("设置失败");
                }
            }, "json");
        });
    }

    function setRecInfo(data) {
        parent.flyConfirm('确认要设置该商品为热销推荐吗？', function (index) {
            $.post("${pageContext.request.contextPath}/admin/shop/goods/saveGoodsRec?state=1&id=" + data, function (json) {
                if (json.code == 0) {
                    parent.flyMsg("设置成功");
                    $("#queryBtn").click();
                } else {
                    parent.flyMsg("设置失败");
                }
            }, "json");
        });
    }

    function cancleDiscountInfo(data) {
        parent.flyConfirm('确认要取消设置该商品的特价折扣吗？', function (index) {
            $.post("${pageContext.request.contextPath}/admin/shop/goods/saveGoodsDiscount?state=0&id=" + data, function (json) {
                if (json.code == 0) {
                    parent.flyMsg("设置成功");
                    $("#queryBtn").click();
                } else {
                    parent.flyMsg("设置失败");
                }
            }, "json");
        });
    }

    function setDiscountInfo(data) {
        layer.prompt({title: '请输入商品的折扣，9表示9折，7.4表示74折', formType: 2}, function (text, index) {
            layer.close(index);
            var discount = text;
            $.post("${pageContext.request.contextPath}/admin/shop/goods/saveGoodsDiscount?state=1&id=" + data + "&discount=" + discount, function (json) {
                if (json.code == 0) {
                    parent.flyMsg("设置成功");
                    $("#queryBtn").click();
                } else {
                    parent.flyMsg("设置失败");
                }
            }, "json");
        });
    }

    function setGoodsXiajia(data) {
        parent.flyConfirm('确认下架该商品吗？', function (index) {
            $.post("${pageContext.request.contextPath}/admin/shop/goods/saveGoodsIsOnSale?isOnSale=2&id=" + data, function (json) {
                if (json.code == 0) {
                    parent.flyMsg("下架成功");
                    $("#queryBtn").click();
                } else {
                    parent.flyMsg("下架失败");
                }
            }, "json");
        });
    }

    function setGoodsShangjia(data) {
        parent.flyConfirm('确认上架该商品吗？', function (index) {
            $.post("${pageContext.request.contextPath}/admin/shop/goods/saveGoodsIsOnSale?isOnSale=1&id=" + data, function (json) {
                if (json.code == 0) {
                    parent.flyMsg("上架成功");
                    $("#queryBtn").click();
                } else {
                    parent.flyMsg("上架失败");
                }
            }, "json");
        });
    }
</script>
</body>
</html>
