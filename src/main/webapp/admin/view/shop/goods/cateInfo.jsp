<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>商城分类</title>
    <link rel="shortcut icon" href="favicon.ico"> 
   	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/layui/css/layui.css?v=4.1.0" rel="stylesheet"> 
    <style type="text/css">
    .layui-upload-img {
	    width: 92px;
	    height: 92px;
	    margin: 0 10px 10px 0;
	} 
    </style>
</head> 

<body class="gray-bg"> 
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
    	<div class="col-sm-12">  
    		<div class="ibox float-e-margins" style="height:100%;">
                <div class="ibox-content"> 
                	<form class="form-horizontal">
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">分类名称：</label>
					        <div class="col-sm-6">
					        	<input type="hidden" value="${a.id }" id="id">  
					            <input type="text" id="name" value="${a.name }" class="form-control" placeholder="请输入分类名称">
					        </div>
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 control-label">分类图标：</label>
					        <div class="col-sm-6">
					            <input type="hidden" id="imgUrl" value="${a.imgUrl }" class="form-control" placeholder="值越大越靠前">
					            <div class="layui-upload">
								  <button type="button" class="layui-btn" id="uploadBtn">上传图片</button>
								  <div class="layui-upload-list">
								    <img class="layui-upload-img" src="${a.imgUrl }" id="demo1">
								    <p id="demoText"></p>
								  </div>
								</div>   
					        </div> 
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 control-label">上级分类：</label>
					        <div class="col-sm-6">
					            <select class="form-control" id="parent">
					            	<option value="">请选择上级分类</option>
									<c:forEach items="${cateList}" var="item">
									<c:if test="${a.parentCate.id == item.id }">
									<option value="${item.id }" selected>${item.name }</option>
									</c:if> 
									<c:if test="${a.parentCate.id != item.id }">
									<option value="${item.id }">${item.name }</option>
									</c:if>
									</c:forEach> 
					            </select>
					        </div> 
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 control-label">显示排序：</label>
					        <div class="col-sm-6">
					            <input type="text" id="orderNum" value="${a.orderNum }" class="form-control" placeholder="值越大越靠前">
					        </div> 
					    </div> 
					    <div class="form-group draggable ui-draggable">
                            <div class="col-sm-12 col-sm-offset-3"> 
                                <button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;提交</button>
                                <button class="btn btn-white" type="button" id="btnCancle">取消</button>
                            </div>
                        </div>
					</form> 
                </div> 
            </div> 
    	</div> 
    </div>
</div>

<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layui/layui.js"></script>
<script>   
layui.use(['upload','layer'], function(){
	  var upload = layui.upload;
	  var layer = layui.layer;
	  //多图片上传
	  upload.render({
	    elem: '#uploadBtn'
	    ,url: '${pageContext.request.contextPath}/admin/file/uploadNormalImg'
	    ,data:{cate:$("#cate").val()}
	    ,multiple: false
	  	,before: function(obj){
	      //预读本地文件示例，不支持ie8
	      obj.preview(function(index, file, result){
	        $('#demo1').attr('src', result); //图片链接（base64）
	      });
	    }
	    ,allDone: function(obj){ //当文件全部被提交后，才触发
	    	console.log(obj);
	        console.log(obj.total); //得到总文件数
	        console.log(obj.successful); //请求成功的文件数
	        console.log(obj.aborted); //请求失败的文件数
	        layer.closeAll('loading'); //关闭loading 
	        layer.msg("上传成功");
	    }
	    ,done: function(res){
	    	console.log(res);
	    	layer.msg("上传成功");
	    	$("#imgUrl").val(res.url); 
	      //上传完毕
	      
	    }
	  }); 
	}); 
	
    $(document).ready(function () {
  		var pl = parent.layer;
  		var pw = parent.currDialogWindow;
    	$("#btnSave").click(function(){
    		var id = $("#id").val();
    		var name = $("#name").val();
    		var orderNum = $("#orderNum").val();
    		var imgUrl = $("#imgUrl").val();
    		var parent = $("#parent").val();
    		if(name == null || name == ""){ 
    			layer.alert("请输入分类名称");
    			return false; 
    		}
    		if(orderNum == null || orderNum == ""){
    			orderNum = 1;
    		}
    		$.post("${pageContext.request.contextPath}/admin/shop/cate/saveCate",{
    			id : id,
    			name : name, 
    			orderNum : orderNum,
    			parent:parent,
    			imgUrl : imgUrl
    		},function(json){
    			if(json.code == 0){  
    				var index = pl.getFrameIndex(window.name); //先得到当前iframe层的索引
    				pl.close(index); //再执行关闭 
    				pl.alert("操作成功");
    				pw.$("#queryBtn").click();
    			}  
    			else{   
    				layer.alert("操作失败");
    			}
    		},"json");
    	});
		$("#btnCancle").click(function(){
			//关闭操作
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭  
		}); 
    	
    });
</script>
</body>
</html>
