<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>商品编辑</title>
    <link rel="shortcut icon" href="favicon.ico"> 
   	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/layui/css/layui.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/app.css?v=4.1.0" rel="stylesheet">
    <style type="text/css">
    .layui-upload-img {
	    width: 92px;
	    height: 92px;
	    margin: 0 10px 10px 0;
	} 
    </style>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/admin/static/plugins/kindeditor/themes/default/default.css">
	<script charset="utf-8" src="${pageContext.request.contextPath}/admin/static/plugins/kindeditor/kindeditor-all-min.js"></script>
	<script charset="utf-8" src="${pageContext.request.contextPath}/admin/static/plugins/kindeditor/lang/zh-CN.js"></script>
 	<script type="text/javascript">
		var imgArr = [];//初始化图片数组
		var editor;
		KindEditor.ready(function(K) {
			editor = K.create('textarea[id="content"]',{
				allowFileManager : true,
				uploadJson : '${pageContext.request.contextPath}/admin/view/common/upload_json.jsp',
		        fileManagerJson : '${pageContext.request.contextPath}/admin/view/common/file_manager_json.jsp'
			});
		});
	</script>
</head> 

<body class="gray-bg"> 
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
    	<div class="col-sm-12">  
    		<div class="ibox float-e-margins" style="height:100%;">
                <div class="ibox-content"> 
                	<form class="form-horizontal">
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">商品名称：</label>
					        <div class="col-sm-6">
					        	<input type="hidden" value="${a.id }" id="id">  
					            <input type="text" id="name" value="${a.name }" class="form-control" placeholder="例如：力士(LUX)沐浴乳套装 幽莲魅肤1kg+樱沁恬韵1kg送幽莲魅肤350g+恒永慕爱100gx2">
					        </div>
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">商品副标题：</label>
					        <div class="col-sm-6">
					            <input type="text" id="name2" value="${a.name2 }" class="form-control" placeholder="例如：2件包邮">
					        </div>
					    </div>
					     <div class="form-group"> 
					        <label class="col-sm-3 control-label">商品编码：</label>
					        <div class="col-sm-6">
					            <input type="text" id="goodsNo" value="${a.goodsNo }" class="form-control" placeholder="请输入商品编码">
					        </div>
					    </div>
					     <div class="form-group"> 
					        <label class="col-sm-3 control-label">商品条形码：</label>
					        <div class="col-sm-6">
					            <input type="text" id="tiaoxingma" value="${a.tiaoxingma }" class="form-control" placeholder="请输入商品条形码">
					        </div>
					    </div>
						<div class="form-group">
							<label class="col-sm-3 control-label">折扣商品限购数量：<span class="text text-danger">(必填)</span></label>
							<div class="col-sm-6">
								<input type="text" id="dislimit" value="${a.dislimit }" class="form-control" placeholder="请输入折扣商品限购数量">
								<span class="help-block" style="color:#FFB800;">输入1：表示折扣商品用户每次只能购买1件；0表示不限制</span>
							</div>
						</div>
					    <div class="form-group" > 
					        <label class="col-sm-3 control-label">商品类型：</label>
					        <div class="col-sm-3"> 
					            <select class="form-control" id="goodsType">
					            	<option value="1">实物商品</option>
					            </select>
					        </div>
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">所属分类：</label>
					        <div class="col-sm-3"> 
					            <select class="form-control" id="cate">
					            	<option value="">请选择商品分类</option>
									<c:forEach items="${cateList}" var="item">
										<c:if test="${empty a }">
											<c:if test="${empty item.parentCate }">	
											<option value="${item.id }" disabled>${item.name }</option>
											</c:if> 
											<c:if test="${not empty item.parentCate }">	
											<option value="${item.id }">&nbsp;&nbsp;&nbsp;├─${item.name }</option>
											</c:if>
										</c:if> 
										<c:if test="${not empty a}">
											 <c:if test="${a.cate.id == item.id }">
											 	<option value="${item.id }" selected>
												&nbsp;&nbsp;&nbsp;├─${item.name }
												</option>
											</c:if>
											<c:if test="${a.cate.id != item.id }">
											 	<option value="${item.id }">
											 	<c:if test="${empty item.parentCate }">	
											 	${item.name } 
											 	</c:if>
											 	<c:if test="${not empty item.parentCate }">	
											 	&nbsp;&nbsp;&nbsp;├─${item.name } 
											 	</c:if>
												</option>
											</c:if> 
										</c:if>   
										</c:forEach> 
					            </select>
					        </div>
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">所属品牌：</label>
					        <div class="col-sm-3"> 
					            <select class="form-control" id="brand">
					            	<option value="">请选择商品所属品牌</option>
									<c:forEach items="${brandList}" var="item">
										<c:if test="${empty a }">
										<option value="${item.id }">${item.name }</option>
										</c:if> 
										<c:if test="${not empty a}">
											 <c:if test="${a.brand.id == item.id }">
											 	<option value="${item.id }" selected>${item.name }</option>
											</c:if>
											<c:if test="${a.cate.id != item.id }">
											 	<option value="${item.id }" >${item.name }</option>
											</c:if> 
										</c:if>   
										</c:forEach> 
					            </select>
					        </div>
					    </div>
					    <div class="form-group" style="display:none;"> 
					        <label class="col-sm-3 control-label">商品角标：</label>
					        <div class="col-sm-6">
					        	<c:if test="${not empty a.cornerImg }">
					        	<input type="checkbox" checked class="js-switch_3"/>
					        	</c:if>
					        	<c:if test="${ empty a.cornerImg }">
					        	<input type="checkbox" class="js-switch_3"/>
					        	</c:if>
					        	<span>用于展示在商品列表左上角，吸引关注</span>
					        </div>      
					        <input type="hidden" value="${a.cornerImg }"  id="cornerImg">
					        <div class="col-sm-6 col-sm-offset-3" id="corner_img_box" style="display:none;">
					        	<div class="goods_row_wrap m-t m-b"> 
					        		<div class="add-pic corner-pic m-r">
                						商品图片 
						                <div class="corner-pic-item" id="jiaobiao_tupian"> 
						                	<c:if test="${not empty a.cornerImg }">
						                   	<img src="${a.cornerImg }" class="ng-star-inserted">
						                   	</c:if> 
						                   	<c:if test="${empty a.cornerImg }">
						                   	<img src="http://youji.7working.com/xinpin.png" class="ng-star-inserted">
						                   	</c:if>
						                </div> 
              						</div>  
              						<div id="jiaobiao_img_div_box" style="display:none;" class="add-pic add-pic-btn add-corner-pic ng-star-inserted">
							        	<i class="fa fa-plus"></i>更换角标 
							      	</div> 
							      	<span class="pic-txt m-l">推荐使用72*36像素的png图片 </span> 	
					        	</div>
					        	<div class="radio radio-inline i-checks" style="margin-bottom:20px;">
									<label style="font-size:14px;">  
										<input type="radio" value="xinpin" checked name="cornerType">
										<i></i>新品
									</label> 
								</div>
								<div class="radio radio-inline i-checks" style="margin-bottom:20px;">
									<label style="font-size:14px;"> 
										<input type="radio" value="hot" name="cornerType">
										<i></i>HOT
									</label> 
								</div> 
								<div class="radio radio-inline i-checks" style="margin-bottom:20px;">
									<label style="font-size:14px;"> 
										<input type="radio" value="new" name="cornerType">
										<i></i>NEW
									</label> 
								</div>
								<div class="radio radio-inline i-checks" style="margin-bottom:20px;">
									<label style="font-size:14px;"> 
										<input type="radio" value="baokuan" name="cornerType">
										<i></i>爆款
									</label>  
								</div>
								<div class="radio radio-inline i-checks" style="margin-bottom:20px;">
									<label style="font-size:14px;"> 
										<input type="radio" value="zidingyi" name="cornerType">
										<i></i>自定义
									</label> 
								</div>
					        </div> 
					    </div> 
					    <div class="form-group">
					        <label class="col-sm-3 control-label">商品封面图片：</label>
					        <div class="col-sm-6">
					            <input type="hidden" id="imgUrl" value="${a.imgUrl }" class="form-control" placeholder="值越大越靠前">
					            <div class="goods_row_wrap m-t m-b"> 
					        		<div id="goods_imgUrl_div_box" class="add-pic corner-pic m-r">
                						<c:if test="${empty a.imgUrl }">商品图片</c:if> 
                						<c:if test="${not empty a.imgUrl }">
                						<img src="${a.imgUrl }">
                						</c:if>
              						</div>   
              						<div id="uploadGoodsZhutu" class="add-pic add-pic-btn add-corner-pic ng-star-inserted">
						                <i class="fa fa-plus"></i>
						                	上传商品主图 
						            </div> 
						            <span class="pic-txt m-l">
                						推荐使用800*800像素的jpg图片
              						</span>   
					        	</div>   
					        </div> 
					    </div>
					    
					    <div class="form-group">
					        <label class="col-sm-3 control-label">商品轮播图片：</label>
					        <div class="col-sm-6">
					        	<c:if test="${not empty a.imgList }">
					        	<input type="checkbox" checked class="js-switch_lunbo"/>
					        	</c:if> 
					        	<c:if test="${empty a.imgList }">
					        	<input type="checkbox" class="js-switch_lunbo"/>
					        	</c:if>
					        	<span>不开启，则使用商品封面图片作为展示</span>
					        </div>       
					        <input type="hidden" value="${a.imgList}" id="imgList">  
					        <div class="col-sm-9 col-sm-offset-3" style="display:${not empty a.imgList ?'block':'none' };"
					        	 id="shangpin_lunbo_div_box">
					            <div class="goods_row_wrap m-t m-b"> 
					            	<c:set var="string1" value="${a.imgList}" />
									<c:set value="${ fn:split(string1, ',') }" var="imgList" />
					            	<c:forEach items="${imgList }" var="item">
					            	<div class="add-pic goods_lunbo m-r">     
					        			<img src="${item }"> 
										<span class="closeBtn">×</span> 
									</div>
					            	</c:forEach>
              						<div style="margin-top:-5px;" id="btnAddLunboPic" class="add-pic add-pic-btn add-corner-pic ">
						                <i class="fa fa-plus"></i> 添加 
						            </div>  
						            <span class="pic-txt m-l">
                						推荐使用800*800像素的jpg图片
              						</span>   
					        	</div>   
					        </div> 
					    </div>
					    
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">商品规格：<span class="text text-danger">(必填)</span></label>
					        <div class="col-sm-3"> 
					            <span id="btnToAddGoodsSku" class="addSku_span"><i class="fa fa-plus"></i>添加规格</span>
					            <div class="wenxintip">温馨提示：请在设置完成规格之后，再去设置价格等属性，否则会导致之前的数据消失.</div>
					            <div class="wenxintip">修改库存，价格请在商品列表页中操作.</div>
					        </div>   
					        <div class="col-sm-6 col-sm-offset-3" id="goods_sku_div_box">
					        	<ul class="selected-specifications-list">
					        		<c:forEach items="${paramsList}" var="item" varStatus="status">
					        		<li> 
					        			<div class="selected-specifications-item">
					        				<span>${item.paramsName }</span> 
					        				<i class="fa fa-times"></i>
					        			</div>
					        			<div class="specifications-param-container clearfix">
					        				<input type="text" value="${item.parmasValues}"
					        					skuName="${item.paramsName }"
					        					skuId="${item.id }"
					        					 class="form-control tagsinput" placeholder="请设置标签">
					        			</div>  
					        		</li>  
					        		</c:forEach>
					        	</ul> 
					        	<table class="table table-bordered" id="sku_table" style="display:${not empty paramsList?'block':'none'};">
			                        <thead>
			                        <tr id="sku_tr">
			                            <c:forEach items="${paramsList}" var="item" varStatus="status">
											<th>${item.paramsName}</th>
										</c:forEach> 
			                            <th>图片</th>
			                            <th>销售价格<input type="text" value="${item.good.price }" class="form-control tongyijiage price"></th>
			                            <th>库存<input type="text" value="${item.good.stockNum }" class="form-control tongyikucun price"></th>
										<th>虚拟价格<input type="text" value="${item.good.oldPrice }" class="form-control tongyixunijiage price"></th>
			                            <th>折扣商品数量限制<input type="text" value="${item.good.dislimit }" class="form-control tongyidisbuylimit price"></th>
			                        </tr>
			                        </thead>
			                        <tbody id="sku_body_tr"> 
			                        	<c:forEach items="${mapList}" var="item" varStatus="status">
				                        <tr>
				                            <c:forEach items="${item.skuList}" var="item2" varStatus="status2">
										    	<td>${item2.canshuVal }</td>
						 					</c:forEach>   
				                            <td><div class="sku_img_box"><img src="${item.imgUrl }"></div></td>
				                            <td> 
				                            	<input type="hidden" value="${item.skuId}" class="skuId">
				                            	<input type="text" value="${item.price }" class="form-control price jiage">
				                            </td>  
				                            <td><input type="text" value="${item.stockNum }" class="form-control price kucun"></td>
				                            <td><input type="text" value="${item.oldPrice }" class="form-control price xunijiage"></td>
											<td><input type="text" value="${item.disbuylimit }" class="form-control price disbuylimit"></td>
				                        </tr>  
				                        </c:forEach>   
			                        </tbody>
			                    </table>
					        </div>
					    </div> 
					    <div id="basePriceDiv" style="display:${empty paramsList?'block':'none'}">
					    	<div class="form-group"> 
						        <label class="col-sm-3 control-label">吊牌价：</label>
						        <div class="col-sm-3">
						            <input type="text" id="oldPrice" value="${a.oldPrice }" 
						            	class="form-control" placeholder="商品吊牌价，只做展示用途">
						        </div>
						    </div>
						    <div class="form-group"> 
						        <label class="col-sm-3 control-label">商品价格：</label>
						        <div class="col-sm-3">
						            <input type="text" id="price" value="${a.price }"
						            	 class="form-control" placeholder="实际结算价格，如果使用了规格，那么此参数无需设置">
						        </div>
						    </div> 
						    <div class="form-group"> 
						        <label class="col-sm-3 control-label">商品库存：</label>
						        <div class="col-sm-3">
						            <input type="text" id="stockNum" value="${a.stockNum }"
						             class="form-control" placeholder="库存数量，如果使用了规格，那么此参数无需设置">
						        </div> 
						    </div>
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 control-label">商品详情：</label>
					        <div class="col-sm-6"> 
					            <textarea class="form-control" style="height:450px;" id="content">${a.info }</textarea>
					        </div> 
					    </div>
					    <div class="form-group draggable ui-draggable">
                            <div class="col-sm-12 col-sm-offset-3"> 
                                <button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;提交</button>
                                <button class="btn btn-white" type="button" id="btnCancle">取消</button>
                            </div>
                        </div>
					</form> 
                </div> 
            </div> 
    	</div> 
    </div>
</div>

<!-- 规格选择框开始框 -->
<div id="sku_modal" style="display:none;">
<div class="ant-modal-mask"></div>
	<div class="ant-modal-wrap">
		<div class="ant-modal" style="width: 550px; top: 100px;">
			<div class="ant-modal-content">
				<button aria-label="Close"
					class="ant-modal-close ng-star-inserted cancleCropImg">
					<span class="ant-modal-close-x fa fa-times"></span>
				</button>
				<div class="ant-modal-header ng-star-inserted"
					style="padding: 13px 16px !important;">
					<div class="ant-modal-title" id="nzModal5">选择规格，顺序为点击排序</div>
				</div>
				<div class="ant-modal-body">
					<div class="clearfix ng-star-inserted">
						<div class="normal-specifications clearfix">
							<h4>常规规格</h4>
							<div class="specifications clearfix">
								<div class="specifications-show-item ng-star-inserted ">
									单位</div>
								<div class="specifications-show-item ng-star-inserted ">
									颜色</div>
								<div class="specifications-show-item ng-star-inserted ">
									口味</div>
								<div class="specifications-show-item ng-star-inserted">容量
								</div>
								<div class="specifications-show-item ng-star-inserted">套餐
								</div>
								<div class="specifications-show-item ng-star-inserted">种类
								</div>
								<div class="specifications-show-item ng-star-inserted">尺寸
								</div>
								<div class="specifications-show-item ng-star-inserted">重量
								</div>
								<div class="specifications-show-item ng-star-inserted">型号
								</div>
								<div class="specifications-show-item ng-star-inserted">款式
								</div>
							</div>
						</div>

						<div class="custom-specifications clearfix">
							<h4>
								<i class="anticon anticon-edit ng-star-inserted"></i> 我的规格
							</h4>
							<div class="specifications clearfix">
								<div class="form-group" id="showBox_tianjiaguige"
									style='display: none;'>
									<input type="text" id="customerTags" class="form-control tags">
								</div>
								<div class="addSku_span add-tags-btn" id="tianjiaguige_customer">
									<i class="fa fa-plus"></i> 添加规格
								</div>

								<div class="addSku_span add-tags-btn" style="display: none;"
									id="confirm_tianjiaguige_customer">
									<i class="fa fa-plus"></i> 确定添加
								</div>
							</div>
						</div>

					</div>
					<div class="sku_xuanzhong_result">
						已选：<span id="sku_choiced"></span>
					</div>
				</div>
				<!-- body结束 -->
				<div class="ant-modal-footer ng-star-inserted">
					<div _ngcontent-c31="" class="ng-star-inserted">
						<button
							style="color: #fff; background-color: #108ee9; margin-right: 10px; border-color: #108ee9;"
							class="ant-btn ant-btn-primary" id="confirmCheckSku">确定
						</button>
						<button class="ant-btn" id="cancleCheckSku"
							style="color: #108ee9; border-color: #108ee9;">取消</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	
<!-- 规格选择结束框 -->

<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layui/layui.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/switchery/switchery.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/tagsinput/bootstrap-tagsinput.js"></script>
<script>   
var sucCheckImg;
var nowGoodsSkuItem = "${a.skuItem}";
var goodsType = "${a.goodsType}";
if(goodsType != ""){
	$("#goodsType").val(goodsType); 
}
var basePath = "${pageContext.request.contextPath}";
layui.use(['layer'], function(){
});  
</script>
<script src="${pageContext.request.contextPath}/admin/static/js/app/goodsInfo.js"></script>
<jsp:include page="../../common/picManage.jsp"></jsp:include>
</body>
</html>
