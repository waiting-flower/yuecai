<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>商城赠品编辑</title>
    <link rel="shortcut icon" href="favicon.ico"> 
   	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
     <link href="${pageContext.request.contextPath}/admin/static/css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/layui/css/layui.css?v=4.1.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/app.css?v=4.1.0" rel="stylesheet"> 
    <style type="text/css">
    .layui-upload-img {
	    width: 92px;
	    height: 92px;
	    margin: 0 10px 10px 0;
	} 
    </style>
     <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/admin/static/plugins/kindeditor/themes/default/default.css">
	<script charset="utf-8" src="${pageContext.request.contextPath}/admin/static/plugins/kindeditor/kindeditor-all-min.js"></script>
	<script charset="utf-8" src="${pageContext.request.contextPath}/admin/static/plugins/kindeditor/lang/zh-CN.js"></script>
 	<script type="text/javascript">
		var imgArr = [];//初始化图片数组
		var editor;
		KindEditor.ready(function(K) {
			editor = K.create('textarea[id="content"]',{
				allowFileManager : true,
				uploadJson : '${pageContext.request.contextPath}/admin/view/common/upload_json.jsp',
		        fileManagerJson : '${pageContext.request.contextPath}/admin/view/common/file_manager_json.jsp'
			});
		});
	</script> 
</head> 

<body class="gray-bg"> 
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
    	<div class="col-sm-12">  
    		<div class="ibox float-e-margins" style="height:100%;">
                <div class="ibox-content"> 
                	<form class="form-horizontal">
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">赠品名称：</label>
					        <div class="col-sm-6"> 
					        	<input type="hidden" value="${a.id }" id="id">  
					            <input type="text" id="name" value="${a.name }"
					            	 class="form-control" placeholder="请输入赠品名称">
					        </div>
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 control-label">赠品图片：</label>
					        <div class="col-sm-9"> 
					            <input type="hidden" id="imgUrl" value="${a.imgUrl }" class="form-control" placeholder="值越大越靠前">
					            <div class="goods_row_wrap m-t m-b"> 
					        		<div id="goods_imgUrl_div_box" class="add-pic corner-pic m-r">
                						<c:if test="${empty a.imgUrl }">赠品图片</c:if> 
                						<c:if test="${not empty a.imgUrl }">
                						<img src="${a.imgUrl }">
                						</c:if>
              						</div>   
              						<div id="uploadGoodsZhutu" class="add-pic add-pic-btn add-corner-pic ng-star-inserted">
						                <i class="fa fa-plus"></i>
						                	上传品牌赠品图片  
						            </div> 
						            <span class="pic-txt m-l">
                						推荐使用800*800像素的jpg图片
              						</span>    
					        	</div>    
					        </div> 
					    </div> 
					    <div class="form-group">
					        <label class="col-sm-3 control-label">赠品轮播图片：</label>
					        <div class="col-sm-6">
					        	<c:if test="${not empty a.imgList }">
					        	<input type="checkbox" checked class="js-switch_lunbo"/>
					        	</c:if> 
					        	<c:if test="${empty a.imgList }">
					        	<input type="checkbox" class="js-switch_lunbo"/>
					        	</c:if>
					        	<span>不开启，则使用商品封面图片作为展示</span>
					        </div>       
					        <input type="hidden" value="${a.imgList}" id="imgList">  
					        <div class="col-sm-9 col-sm-offset-3" style="display:${not empty a.imgList ?'block':'none' };"
					        	 id="shangpin_lunbo_div_box">
					            <div class="goods_row_wrap m-t m-b"> 
					            	<c:set var="string1" value="${a.imgList}" />
									<c:set value="${ fn:split(string1, ',') }" var="imgList" />
					            	<c:forEach items="${imgList }" var="item">
					            	<div class="add-pic goods_lunbo m-r">     
					        			<img src="${item }"> 
										<span class="closeBtn">×</span> 
									</div>
					            	</c:forEach>
              						<div style="margin-top:-5px;" id="btnAddLunboPic" class="add-pic add-pic-btn add-corner-pic ">
						                <i class="fa fa-plus"></i> 添加 
						            </div>  
						            <span class="pic-txt m-l">
                						推荐使用800*800像素的jpg图片
              						</span>   
					        	</div>   
					        </div> 
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 control-label">赠品价格：</label>
					        <div class="col-sm-3">
					            <input type="text" id="money" value="${a.money }"
					            	 class="form-control" placeholder="请输入赠品的显示售价">
					        </div> 
					    </div> 
					    <div class="form-group">
					        <label class="col-sm-3 control-label">商品库存：</label>
					        <div class="col-sm-3">
					            <input type="text" id="stockNum" value="${a.stockNum }"
					            	 class="form-control" placeholder="商品库存，只能输入正整数">
					        </div> 
					    </div> 
					    <div class="form-group">
					        <label class="col-sm-3 control-label">商品详情：</label>
					        <div class="col-sm-6"> 
					            <textarea class="form-control" style="height:450px;" id="content">${a.info }</textarea>
					        </div> 
					    </div>
					    <div class="form-group draggable ui-draggable">
                            <div class="col-sm-12 col-sm-offset-3"> 
                                <button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;提交</button>
                                <button class="btn btn-white" type="button" id="btnCancle">取消</button>
                            </div>
                        </div>
					</form> 
                </div> 
            </div> 
    	</div> 
    </div>
</div>

<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layui/layui.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/switchery/switchery.js"></script>
<script>   
var sucCheckImg;
layui.use(['layer'], function(){
});  	
    $(document).ready(function () {
    	var lunboBox = document.querySelector('.js-switch_lunbo');
    	new Switchery(lunboBox, {
        	color: "#1AB394", 
        	size: 'small'   
        });
    	lunboBox.onchange = function(){
    		if(lunboBox.checked){
    			$("#shangpin_lunbo_div_box").show();
    		}  
    		else{
    			$("#shangpin_lunbo_div_box").hide();    
    		}
    	}; 
    	
    	//封面图片上传 
    	$("#uploadGoodsZhutu").click(function(){
    		showPicModal(); 
    		sucCheckImg = function(url){
    			$("#goods_imgUrl_div_box").html('<img src="' + url + '">');
    			$("#imgUrl").val(url);
    			hidePicModal();   
    		}   
    	});
  		var pl = parent.layer;
  		var pw = parent.currDialogWindow;
    	$("#btnSave").click(function(){
    		var id = $("#id").val();
    		var name = $("#name").val();
    		var imgUrl = $("#imgUrl").val();
    		var imgList = $("#imgList").val();
    		var money = $("#money").val();
    		var stockNum = $("#stockNum").val();
    		var info = editor.html();
    		if(name == null || name == ""){ 
    			layer.alert("请输入赠品名称");
    			return false;  
    		}
    		if(imgUrl == null || imgUrl == ""){ 
    			layer.alert("请上传赠品图片");
    			return false; 
    		}
    		if(money == null || money == ""){ 
    			layer.alert("请输入赠品价格");
    			return false; 
    		}
    		if(stockNum == null || stockNum == ""){ 
    			layer.alert("请输入库存");
    			return false; 
    		}
    		if(info == null || info == ""){ 
    			layer.alert("请输入赠品详情");
    			return false; 
    		}
    		$.post("${pageContext.request.contextPath}/admin/shop/gift/saveGift",{
    			id : id,
    			name : name, 
    			imgList : imgList,
    			imgUrl : imgUrl,
    			stockNum : stockNum,
    			money : money,
    			info : info 
    		},function(json){
    			if(json.code == 0){  
    				var index = pl.getFrameIndex(window.name); //先得到当前iframe层的索引
    				pl.close(index); //再执行关闭 
    				pl.alert("操作成功");
    				pw.$("#queryBtn").click();
    			}  
    			else{   
    				layer.alert("操作失败");
    			}
    		},"json");
    	});
		$("#btnCancle").click(function(){
			//关闭操作
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭  
		}); 
    	
    });
</script>
<jsp:include page="../../common/picManage.jsp"></jsp:include>
</body>
</html>
