<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>秒杀活动商品价格设置</title>
    <link rel="shortcut icon" href="favicon.ico"> 
   	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/layui/css/layui.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
    <style type="text/css">
    .tuanzhang{
    	display:none; 
    }
    </style> 
</head> 

<body class="gray-bg"> 
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
    	<div class="col-sm-12">  
    		<div class="ibox float-e-margins" style="height:100%;">
                <div class="ibox-content"> 
                	<form class="form-horizontal">
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">商品名称：</label>
					        <div class="col-sm-6">
					        	<input type="hidden" value="${a.id }" id="id">    
					        	<label style="line-height:35px;font-weight:normal;">${goods.name }  ——价格：${goods.price }</label>
					        </div>    
					    </div> 
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">商品图片：</label>
					        <div class="col-sm-6"> 
					            <img alt="" src="${goods.imgUrl }" style="width:80px;height:80px;border:1px solid #e5e5e5;">
					        </div>
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">秒杀减价设置：</label>
					        <div class="col-sm-6"> 
								<table class="table table-bordered" id="sku_table">
			                        <thead>
				                        <tr id="sku_tr">
				                            <th>秒杀减价设置</th>
				                            <!--<th>分规格设置</th>-->
				                        </tr> 
			                        </thead>
			                        <tbody id="group_person_set_body_tr"> 
				                        <tr>
				                            <td>
				                            	<input type="text" style="width:120px;" 
				                            	value="" class="form-control discount">
				                            </td>
											<!--
				                            <td>    
				                            	<c:if test="${not empty paramsList }">
				                            	<button class="btn btn-success btn-sm setSkuGroupPrice normal"
				                            	 type="button"
				                            	 isOpenSkuPrice="0" 
				                            		 personNum="1" >设置</button>
				                            		</c:if>
				                            </td>
				                            -->
				                        </tr>   
			                        </tbody>
			                    </table>
					        </div>
					    </div>
					    <div class="form-group">   
					        <div class="col-sm-6 col-sm-offset-3"> 
								<div id="personNum_div_1" style="display:none;">
									<label>秒杀商品分规格秒杀减价设置</label>
									<table class="table table-bordered" id="sku_table">
				                        <thead> 
				                        <tr id="sku_tr">
				                            <c:forEach items="${paramsList}" var="item" varStatus="status">
												<th>${item.paramsName}</th>
											</c:forEach> 
				                            <th>价格</th>
				                            <th>减价设置</th>
				                            <th class="tuanzhang">团长优惠</th>
				                        </tr> 
				                        </thead>
				                        <tbody class="sku_body_tr"> 
				                        	<c:forEach items="${mapList}" var="item" varStatus="status">
					                        <tr itemId="${item.skuId}">
					                            <c:forEach items="${item.skuList}" var="item2" varStatus="status2">
											    	<td>${item2.canshuVal }</td>
							 					</c:forEach> 
					                            <td>${item.price}</td>
					                            <td> 
					                            	<input type="text" style="width:120px;"
					                            	 value="" class="form-control discount">
					                            </td>
					                        </tr>  
					                        </c:forEach>    
				                        </tbody>
				                    </table>
								</div>
					        </div>
					    </div>
					    <div class="form-group draggable ui-draggable">
                            <div class="col-sm-12 col-sm-offset-3"> 
                                <button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;提交</button>
                                <button class="btn btn-white" type="button" id="btnCancle">取消</button>
                            </div>
                        </div>
					</form> 
                </div> 
            </div> 
    	</div> 
    </div>
</div>

<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layui/layui.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/tagsinput/bootstrap-tagsinput.js"></script>

<script> 
layui.use(['layer','laydate'], function(){
	var layer = layui.layer;
	var laydate = layui.laydate;
	laydate.render({ 
		elem: '#beginTime'
		,type: 'datetime'
	});
	laydate.render({ 
		elem: '#endTime'
		,type: 'datetime'
	}); 
	$(".i-checks").iCheck({checkboxClass: "icheckbox_square-green", radioClass: "iradio_square-green",})
	$("body").on('click', ".setSkuGroupPrice.normal", function(){
		var personNum = $(this).attr("personNum");
		$(this).html("取消设置"); 
		$(this).attr("isOpenSkuPrice","1");
		$("#personNum_div_" + personNum).show(); 
		$(this).addClass("openSkuPrice").removeClass("normal"); 
	});
	$("body").on('click', ".setSkuGroupPrice.openSkuPrice", function(){
		var personNum = $(this).attr("personNum"); 
		$(this).removeClass("openSkuPrice").addClass("normal");
		$(this).html("设置");   
		$(this).attr("isOpenSkuPrice","0");
		$("#personNum_div_" + personNum).hide(); 
	})
});     
	
    $(document).ready(function () {
  		var pl = parent.layer;
  		var pw = parent.currDialogWindow;
    	$("#btnSave").click(function(){ 
    		var seckillId = $("#id").val();//秒杀活动ID
    		var goodsId = "${goods.id}";//对应的商品ID
    		var vData = {
    			seckillId : seckillId,
    			goodsId : goodsId
    		}; 
    		var trArr = $("#group_person_set_body_tr tr"); 
    		
			var trItem = $(trArr[0]);
			var isOpenSkuPrice = trItem.find(".setSkuGroupPrice").attr("isOpenSkuPrice");
			var discount = trItem.find(".discount").val();
			
			
			var priceSetJsonStr = {
					seckillId : seckillId,
	    			goodsId : goodsId,
	    			isOpenSkuPrice : isOpenSkuPrice,
	    			discount : discount
			};
			
			if(isOpenSkuPrice == "0" && discount == ""){
				layer.msg("请输入秒杀商品的减价设置");
				return false;  
			}
			if(isOpenSkuPrice == "1"){
				//判定规格是否完成设置价格
				var skuArr = [];
				var skuDiscountArr = $("#personNum_div_1").find(".discount");
				for(var j = 0; j < skuDiscountArr.length; j++){
					if($(skuDiscountArr[j]).val() == ""){
						layer.msg("请设置分规格减价设置");
    					return false;
					}
				}
				var skuItemArr = $("#personNum_div_1").find(".sku_body_tr tr");
				for(var j = 0; j < skuItemArr.length; j++){
					var skuItem = $(skuItemArr[j]);
					var skuId = skuItem.attr("itemId");
					var discount = skuItem.find(".discount").val();
					skuArr.push({ 
    					skuId : skuId,
    					discount : discount
    				});
				}
				priceSetJsonStr.skuArr = skuArr;
			} 
			
    		console.log(priceSetJsonStr);  
    		var priceSet = JSON.stringify(priceSetJsonStr);
    		vData.priceSet = priceSet;
    		console.log(vData); 
    		$.post("${pageContext.request.contextPath}/admin/shop/seckill/saveSeckillGoodsSet",vData,function(json){
    			if(json.code == 0){  
    				var index = pl.getFrameIndex(window.name); //先得到当前iframe层的索引
    				pl.close(index); //再执行关闭 
    				pl.alert("操作成功"); 
    				pw.$("#queryBtn").click();
    			}  
    			else{   
    				layer.alert("操作失败");
    			}
    		},"json");
    	});
		$("#btnCancle").click(function(){
			//关闭操作
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭  
		}); 
    	
    });
</script>
</body>
</html>
