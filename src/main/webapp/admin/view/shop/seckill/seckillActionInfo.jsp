<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>秒杀活动编辑</title>
    <link rel="shortcut icon" href="favicon.ico"> 
   	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/layui/css/layui.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/tagsinput/bootstrap-tagsinput.css" rel="stylesheet"> 
</head> 

<body class="gray-bg"> 
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
    	<div class="col-sm-12">  
    		<div class="ibox float-e-margins" style="height:100%;">
                <div class="ibox-content"> 
                	<form class="form-horizontal">
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">秒杀活动名称：</label>
					        <div class="col-sm-6">
					        	<input type="hidden" value="${a.id }" id="id">  
					            <input type="text" maxLength="10" id="name" value="${a.name }"
					            	 class="form-control" placeholder="请输入秒杀活动名称">
					        </div> 
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">开始时间：</label>
					        <div class="col-sm-6">
					            <input type="text" id="beginTime" value="<fmt:formatDate value="${a.beginTime}" pattern="yyyy-MM-dd HH:mm:ss" />" 
					            class="form-control" placeholder="秒杀活动开始的时间">
					        </div>
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">结束时间：</label>
					        <div class="col-sm-6"> 
					            <input type="text" id="endTime" value="<fmt:formatDate value="${a.endTime}" pattern="yyyy-MM-dd HH:mm:ss" />" class="form-control"
					            	 placeholder="不输入则表示长期">
					        </div>
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">开启时段：</label>
					        <div class="col-sm-6">
					        	<c:set var="string1" value="${openHours}" />
								<c:set value="${ fn:split(string1, ',') }" var="openHours"/> 
								<c:forEach items="${openHours }" var="item"  varStatus="status">
								<div class="checkbox checkbox-success"> 
                                    <input id="checkbox${status.index }" type="checkbox" name="openHours" value="${item }">
                                    <label for="checkbox${status.index }">
                                        ${item }
                                    </label>
                                </div>
								</c:forEach>
					        </div>
					    </div>
					    <div class="form-group">  
					        <label class="col-sm-3 control-label">每位用户每次秒杀时可购买件数：</label>
					        <div class="col-sm-3"> 
					            <input type="number" id="limitBuy" value="${a.limitBuy }" 
					            	class="form-control" placeholder="每位用户秒杀时可购买件数">
					           	<span class="help-block" style="color:#FFB800;">输入1：表示用户每次团只能购买1件，0表示不限制</span>
					        </div>
					    </div>
					    <div class="form-group">  
					        <label class="col-sm-3 control-label">是否可使用优惠券：</label>
					        <div class="col-sm-3"> 
					            <div class="radio-inline i-checks" style="margin-bottom:20px;">
									<label style="font-size:14px;"> 
										<input type="radio" value="0" checked name="isCanUseCoupon">
										<i></i>不可以使用优惠券 
									</label> 
								</div> 
								<div class="radio-inline i-checks" style="margin-bottom:20px;">
									<label style="font-size:14px;"> 
										<input type="radio" value="1" name="isCanUseCoupon">
										<i></i>可以使用优惠券
									</label>  
								</div>
					        </div>
					    </div>
					    <div class="form-group draggable ui-draggable">
                            <div class="col-sm-12 col-sm-offset-3"> 
                                <button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;提交</button>
                                <button class="btn btn-white" type="button" id="btnCancle">取消</button>
                            </div>
                        </div>
					</form> 
                </div> 
            </div> 
    	</div> 
    </div>
</div>

<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layui/layui.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/tagsinput/bootstrap-tagsinput.js"></script>

<script> 
layui.use(['layer','laydate'], function(){
	$(".i-checks").iCheck({checkboxClass: "icheckbox_square-green", radioClass: "iradio_square-green",})
	var layer = layui.layer;
	var laydate = layui.laydate;
	laydate.render({  
		elem: '#beginTime'
		,type: 'datetime'
	});
	laydate.render({ 
		elem: '#endTime'
		,type: 'datetime'
	}); 
	var thisOpenHour = "${a.openHours}";
	var thisOpenHourArr = thisOpenHour.split(",");
	for(var i = 0; i < thisOpenHourArr.length; i++){
		var item = thisOpenHourArr[i];
		$("input:checkbox[name='openHours'][value='" + item + "']").attr("checked",true);  
	}
});  
	
    $(document).ready(function () {
  		var pl = parent.layer;
  		var pw = parent.currDialogWindow;
    	$("#btnSave").click(function(){
    		var id = $("#id").val();
    		var name = $("#name").val();
    		var beginTime = $("#beginTime").val();
    		var endTime = $("#endTime").val();
    		var openHours = $("#openHours").val();
    		var openHoursArr = [];
    		$('input:checkbox[name=openHours]:checked').each(function(k){
    			openHoursArr.push($(this).val());
    		});  
    		var limitBuy = $("#limitBuy").val();
    		var isCanUseCoupon = $("input[name='isCanUseCoupon'][checked]").val();
    		if(name == null || name == ""){ 
    			layer.msg("请输入拼团活动名称");
    			return false; 
    		}
    		if(openHoursArr.length < 1){
    			layer.msg("请选择开启时段");
    			return false;
    		}
    		if(beginTime == ""){
    			layer.msg("请输入秒杀活动开始时间");
    			return false;
    		}
    		if(endTime == ""){
    			endTime = "2099-12-31 00:00:00";
    		}
    		if(endTime < beginTime){
    			layer.msg("结束时间不能早于开始时间");
    			return false;
    		}
    		if(limitBuy == ""){
    			limitBuy = 0;
    		}
    		
    		var vData = {
        			id : id,
        			name : name,
        			isCanUseCoupon : isCanUseCoupon,  
        			beginTime : beginTime,
        			endTime :endTime,
        			limitBuy : limitBuy,
        			openHours : openHoursArr.join(",")
        	};  
    		$.post("${pageContext.request.contextPath}/admin/shop/seckill/saveSeckillAction",vData,function(json){
    			if(json.code == 0){  
    				var index = pl.getFrameIndex(window.name); //先得到当前iframe层的索引
    				pl.close(index); //再执行关闭 
    				pl.alert("操作成功"); 
    				pw.$("#queryBtn").click();
    			}  
    			else{   
    				layer.alert("操作失败");
    			}
    		},"json");
    	});
		$("#btnCancle").click(function(){
			//关闭操作
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭  
		}); 
    	
    });
</script>
</body>
</html>
