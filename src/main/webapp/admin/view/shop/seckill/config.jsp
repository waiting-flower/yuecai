<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>秒杀配置</title>
    <link rel="shortcut icon" href="favicon.ico">
	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
  	<link href="${pageContext.request.contextPath}/admin/static/js/plugins/tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
	<style type="text/css">
	.bootstrap-tagsinput .label{
		font-size:12px;  
	}
	</style>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row"> 
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
            	<div class="ibox-title">
                    <h5>秒杀配置</h5>
                </div> 
					<div class="ibox-content">
						<form class="form-horizontal">
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">秒杀时间段：</label>
					        <div class="col-sm-6"> 
					        	<input type="hidden" value="${a.id }" id="id">  
					            <input type="text" style="width:100%;" id="openHour" value="${a.openHour }"
					            	 class="form-control tagsinput" placeholder="请输入时间段设置">
					            <span class="help-block" style="color:#FFB800;">拼团时间段，格式如下00:00~02:00,所有输入均需要英文输入</span>
					        </div>
					    </div>
					    <div class="form-group draggable ui-draggable"> 
                            <div class="col-sm-12 col-sm-offset-3"> 
                                <button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;提交</button>
                            </div>
                        </div>
					</form>
					</div>
            </div>
        </div>
    </div>
</div> 
<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/tagsinput/bootstrap-tagsinput.js"></script> 
<script> 
$(document).ready(function () {
	$(".tagsinput").tagsinput(); 
	
	$("#btnSave").click(function(){
		var id = $("#id").val();
		var openHour = $("#openHour").val();
		if(openHour == null || openHour == ""){ 
			layer.alert("请输入秒杀时段");
			return false; 
		}
		$.post("${pageContext.request.contextPath}/admin/shop/seckill/saveConfig",{
			id : id,
			openHour : openHour
		},function(json){
			if(json.code == 0){  
				layer.alert("操作成功");
			}  
			else{   
				layer.alert("操作失败");
			}
		},"json");
	});
	$("#btnCancle").click(function(){
		//关闭操作
		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
		parent.layer.close(index); //再执行关闭  
	}); 
	
});
</script>
</body>
</html>
