<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>秒杀活动商品选择</title>
    <link rel="shortcut icon" href="favicon.ico">
	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
	
    <!-- Data Tables -->
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row"> 
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
            	<div class="ibox-title">
                    <h5>${a.name }——商品选择</h5>
                </div>  
					<div class="ibox-content">
						<form role="form" class="form-inline">
							<div class="input-group">
								<input type="text" class="form-control" id="name" placeholder="请输入商品名称">
								<span class="input-group-btn"> 
									<button type="button" class="btn btn-primary" id="queryBtn">搜索</button>
								</span>
							</div>      
						</form>
					</div>
					<div class="ibox-content">  
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="width:100%">
                        <thead>
                        <tr>
                            <th>商品图片</th>
							<th>商品名称</th>
							<th>库存</th>
							<th>价格</th> 
							<th>参与秒杀活动状态</th>
							<th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 
<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
 
<script> 
	var dataTable;
	var _this = this; 
    $(document).ready(function () {
    	var laguage = {
    			"sLengthMenu" : "每页显示 _MENU_ 条记录",
    			"sZeroRecords" : "抱歉， 没有找到",
    			"sInfo" : "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
    			"sInfoEmpty" : "没有数据",
    			"sInfoFiltered" : "(从 _MAX_ 条数据中检索)",
    			"oPaginate" : {
    				"sFirst" : "首页",
    				"sPrevious" : "前一页",
    				"sNext" : "后一页",
    				"sLast" : "尾页"
    			},
    			"sZeroRecords" : "您请求的内容暂时没有数据",
    			"sProcessing" : "数据加载中..."
    		};  
    	var aoColumns = [{
    		"mData" : "imgUrl",
    		"mRender" : function(data,type,full){
    			return '<img src="' + data + '" style="width:60px;height:60px;">'
    		} 
    	}, {
    		"mData" : "name"  
    	}, {
    		"mData" : "stockNum" 
    	}, {
    		"mData" : "price"  
    	}, {
    		"mData" : "groupState",
    		"mRender" : function(data,type,full){
    			if(data == "1"){ 
    				return '<span class="label label-default">已加入秒杀活动</span>';
    			} 
    			return '<span class="label label-danger">未加入秒杀活动</span>';    		}  
    	}, {
    		"mData" : "id", 
    		"mRender" : function(data,type,full){    
    			if(full.groupState == "0"){ 
    				return '<button onclick="editInfo(' + data + ')" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i>添加</button>';
    			}
    			return "";
    		}        
    	} ]; 
        dataTable = $('.dataTables-example').dataTable({
        	"dom": 't<"bottom"p>lir<"clear">',
    		"aoColumns" : aoColumns,    
    		"oLaguage" : laguage,
    		"bServerSide" : true, 
    		"bFilter" : false,  
    		"bProcessing" : true,
    		"bSort" : false, 
    		/* 使用post方式 */ 
    		"iDisplayLength" :10,     
    	     "sAjaxSource": '${pageContext.request.contextPath}/admin/shop/seckill/loadGoodsByPage',   
    	     "fnServerParams": function (aoData) {  //查询条件
    				aoData.push( 
    						{ "name": "name", "value": $("#name").val()}
    	            );
    	      },
    	     "fnServerData" : function(sSource, aoData, fnCallback) {  
    	    	 $.ajax({
    					"dataType" : 'json',
    					"type" : "post",
    					"url" : sSource,
    					"data" : aoData,
    					"success" : function(json){
    						$("#iTotalRecords").html(json.iTotalRecords);
    						fnCallback(json);
    					}
    				}); 
    	     }  
    	});
    	//
    	$("#queryBtn").click(function() {
            dataTable.fnDraw();
    	});
    });  
    function editInfo(data){
    	location.href = "${pageContext.request.contextPath}/admin/shop/seckill/goodsSet?id=${a.id}&goodsId=" + data; 
    }
</script>
</body>
</html>
