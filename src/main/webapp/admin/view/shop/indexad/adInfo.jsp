<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>启动广告</title>
	<link rel="shortcut icon" href="favicon.ico">
	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/admin/static/js/plugins/layui/css/layui.css?v=4.1.0"
		  rel="stylesheet">
	<link href="${pageContext.request.contextPath}/admin/static/css/app.css?v=4.1.0" rel="stylesheet">
	<style type="text/css">
		.layui-upload-img {
			width: 92px;
			height: 92px;
			margin: 0 10px 10px 0;
		}
	</style>
	<link rel="stylesheet" type="text/css"
		  href="${pageContext.request.contextPath}/admin/static/plugins/kindeditor/themes/default/default.css">
	<script charset="utf-8"
			src="${pageContext.request.contextPath}/admin/static/plugins/kindeditor/kindeditor-all-min.js"></script>
	<script charset="utf-8"
			src="${pageContext.request.contextPath}/admin/static/plugins/kindeditor/lang/zh-CN.js"></script>
	<script type="text/javascript">
		var imgArr = [];//初始化图片数组
		var editor;
		KindEditor.ready(function (K) {
			editor = K.create('textarea[id="content"]', {
				allowFileManager: true,
				uploadJson: '${pageContext.request.contextPath}/admin/view/common/upload_json.jsp',
				fileManagerJson: '${pageContext.request.contextPath}/admin/view/common/file_manager_json.jsp'
			});
		});
	</script>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInUp">
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox float-e-margins" style="height:100%;">
				<div class="ibox-content">
					<form class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-3 control-label">公告内容：</label>
							<div class="col-sm-6">
								<input type="hidden" value="${a.id }" id="id">
								<textarea class="form-control" style="height:450px;"
										  id="content">${a.content}</textarea>
							</div>
						</div>
						<div class="form-group draggable ui-draggable">
							<div class="col-sm-12 col-sm-offset-3">
								<button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;提交
								</button>
								<button class="btn btn-white" type="button" id="btnCancle">取消</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layui/layui.js"></script>
<script>
	var sucCheckImg;
	layui.use(['layer'], function () {
	});
	$(document).ready(function () {
		//轮播图片上传
		$("#uploadGoodsZhutu").click(function () {
			showPicModal();
			sucCheckImg = function (url) {
				$("#goods_imgUrl_div_box").html('<img src="' + url + '">');
				$("#imgUrl").val(url);
				hidePicModal();
			}
		});
		var pl = parent.layer;
		var pw = parent.currDialogWindow;
		$("#btnSave").click(function () {
			var content=editor.html();
			var id = $("#id").val();
			if (content == null || content == "") {
				layer.alert("请输入内容");
				return false;
			}


			$.post("${pageContext.request.contextPath}/admin/shop/indexad/saveAd", {
				id: id,
				content: editor.html()
			}, function (json) {
				if (json.code == 0) {
					var index = pl.getFrameIndex(window.name); //先得到当前iframe层的索引
					pl.close(index); //再执行关闭
					pl.alert("操作成功");
					pw.$("#queryBtn").click();
				} else {
					layer.alert("操作失败");
				}
			}, "json");
		});
		$("#btnCancle").click(function () {
			//关闭操作
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭
		});

	});
</script>
<jsp:include page="../../common/picManage.jsp"></jsp:include>
</body>
</html>
