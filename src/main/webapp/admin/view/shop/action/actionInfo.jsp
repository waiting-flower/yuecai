<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>商城满赠活动</title>
    <link rel="shortcut icon" href="favicon.ico">
   	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/chosen/chosen.css" rel="stylesheet">

    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/layui/css/layui.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/app.css?v=4.1.0" rel="stylesheet">
    <style type="text/css">
    .layui-upload-img {
	    width: 92px;
	    height: 92px;
	    margin: 0 10px 10px 0;
	}
    </style>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
    	<div class="col-sm-12">
    		<div class="ibox float-e-margins" style="height:100%;">
                <div class="ibox-content">
                	<form class="form-horizontal">
					    <div class="form-group">
					        <label class="col-sm-3 control-label">满赠活动名称：</label>
					        <div class="col-sm-6">
					        	<input type="hidden" value="${a.id}" id="id">
					        	<input type="hidden" value="${goodsIds}" id="goodsIds">
					        	<input type="hidden" value="${giftIds}" id="giftIds">
					            <input type="text" id="name" value="${a.name}"
					            	 class="form-control" placeholder="请输入满赠活动名称">
					        </div>
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 control-label">活动类型：</label>
					        <div class="col-sm-3">
					            <div class="radio-inline i-checks" style="margin-bottom:20px;">
									<label style="font-size:14px;">
										<input type="radio" value="1" checked name="actionType">
										<i></i>品牌满赠
									</label>
								</div>
								<div class="radio-inline i-checks" style="margin-bottom:20px;">
									<label style="font-size:14px;">
										<input type="radio" value="2" name="actionType">
										<i></i>商品满赠
									</label>
								</div>
								<button id="btnGoods" style="margin-top:-15px;display:none;" class="btn btn-xs btn-primary" type="button">选择商品</button>
					        </div>
					    </div>
					    <div class="form-group" id="brandDiv">
					    	<div class="col-sm-3 col-sm-offset-3">
					    		<select data-placeholder="请选择品牌" class="chosen-select"
					    		multiple style="width:350px;" id="brandList">
                                	<option value="">请选择品牌</option>
                                	<c:forEach items="${brandList }" var="item">
                                	<option value="${item.id }">${item.name }</option>
                                	</c:forEach>
                                </select>
					    	</div>
					    </div>


					    <div class="form-group" id="goodsDiv" style="display:none;">
					    	<div class="col-sm-3 col-sm-offset-3">
					    		<ul id="goodsList_box" style="border:1px solide #e5e5e5;background:#f7f7f7;padding:10px;">
					    			<!--
					    			<li>
					    				<span class="list-group-item-heading">品牌名称</span>
					    				<button class="btn btn-xs btn-warning" style="margin-left:10px;" type="button">删除</button>
					    			</li>
					    			 -->
					    			 <c:forEach items="${goodsList }" var="item">
					    			 <li>
					    				<span class="list-group-item-heading">${item.goods.name }</span>
					    				<button itemId="${item.goods.id}" class="deleteGoods btn btn-xs btn-warning" style="margin-left:10px;" type="button">删除</button>
					    			</li>
					    			 </c:forEach>
					    		</ul>
					    	</div>
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 control-label">满赠条件金额：</label>
					        <div class="col-sm-3">
					            <input type="text" id="conditionMoney" value="${a.conditionMoney}"
					            	 class="form-control" placeholder="请输入达标赠送金额，例如满200赠送某某商品">
					        </div>
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 control-label">赠品类型：</label>
					        <div class="col-sm-3">
					            <div class="radio-inline i-checks" style="margin-bottom:20px;">
									<label style="font-size:14px;">
										<input type="radio" value="1" checked name="giftType">
										<i></i>任选N件
									</label>
								</div>
								<div class="radio-inline i-checks" style="margin-bottom:20px;">
									<label style="font-size:14px;">
										<input type="radio" value="2" name="giftType">
										<i></i>指定一件
									</label>
								</div>
					        </div>
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 control-label">输入任选赠品数量：</label>
					        <div class="col-sm-3">
					            <input type="text" id="giftNum" value="${a.giftNum}"
					            	 class="form-control" placeholder="请输入任选赠品数量">
					        </div>
					        <div class="col-sm-2">
					        	<button id="btnGift" style="margin-top:5px;" class="btn btn-xs btn-primary" type="button">选择赠品</button>
					        </div>
					    </div>
					    <div class="form-group" id="giftDiv" >
					    	<div class="col-sm-3 col-sm-offset-3">

					    		<ul id="giftList_box" style="border:1px solide #e5e5e5;background:#f7f7f7;padding:10px;">
					    		 <c:forEach items="${giftList }" var="item">
					    			 <li>
					    				<span class="list-group-item-heading">${item.gift.name }</span>
					    				<button itemId="${item.gift.id }" class="deleteGift btn btn-xs btn-warning" style="margin-left:10px;" type="button">删除</button>
					    			</li>
					    			 </c:forEach>
					    		</ul>
					    	</div>
					    </div>
					    <div class="form-group">
					        <label class="col-sm-3 control-label">开始时间：</label>
					        <div class="col-sm-3">
					            <input type="text" id="beginTime" value="${a.beginTime }"
					            	 class="form-control" placeholder="请输入开始时间">
					        </div>
					    </div>

					    <div class="form-group">
					        <label class="col-sm-3 control-label">结束时间：</label>
					        <div class="col-sm-3">
					            <input type="text" id="endTime" value="${a.endTime }"
					            	 class="form-control" placeholder="请输入结束时间">
					        </div>
					    </div>
					    <div class="form-group draggable ui-draggable">
                            <div class="col-sm-12 col-sm-offset-3">
                                <button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;提交</button>
                                <button class="btn btn-white" type="button" id="btnCancle">取消</button>
                            </div>
                        </div>
					</form>
                </div>
            </div>
    	</div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layui/layui.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/chosen/chosen.jquery.js"></script>

<script>
function addGoods(goodsList){
	var goodsIds = $("#goodsIds").val();
	var goodsIdArr = null;
	if(goodsIds == ""){
		goodsIdArr = [];
	}
	else{
		goodsIdArr = goodsIds.split(",")
	}
	console.log(goodsList);
	for(var i = 0; i < goodsList.length; i++){
		var goodsItem = goodsList[i];
		var goodsItemid = goodsItem.id;
		console.log("goodsItemid==="+goodsItemid);
		var name = goodsItem.name;
		var isExists = false;
		for(var j = 0; j < goodsIdArr.length; j++){
			if(goodsIdArr[j] == goodsItemid){
				isExists = true;
				break;
			}
		}
		if(!isExists){
			goodsIdArr.push(goodsItemid);
			console.log("goodsIdArr==="+JSON.stringify(goodsIdArr));
			$("#goodsList_box").append('<li><span class="list-group-item-heading">'
						+ name + '</span><button itemId="'
						+ goodsItemid + '" class="deleteGoods btn btn-xs btn-warning" style="margin-left:10px;" type="button">删除</button></li>')
		}
	}
	$("#goodsIds").val(goodsIdArr.join(","));
}

function addGift(goodsList){
	var giftIds = $("#giftIds").val();
	var goodsIdArr = null;
	console.log(giftIds);
	if(giftIds == ""){
		goodsIdArr = [];
	}
	else{
		goodsIdArr = giftIds.split(",")
	}
	console.log(goodsList);
	for(var i = 0; i < goodsList.length; i++){
		var goodsItem = goodsList[i];
		var goodsItemid = goodsItem.id;
		var name = goodsItem.name;
		var isExists = false;
		for(var j = 0; j < goodsIdArr.length; j++){
			if(goodsIdArr[j] == goodsItemid){
				isExists = true;
				break;
			}
		}
		if(!isExists){
			goodsIdArr.push(goodsItemid);
			$("#giftList_box").append('<li><span class="list-group-item-heading">'
						+ name + '</span><button itemId="'
						+ goodsItemid + '" class="deleteGift btn btn-xs btn-warning" style="margin-left:10px;" type="button">删除</button></li>')
		}
	}
	console.log(goodsIdArr)
	$("#giftIds").val(goodsIdArr.join(","));
}
var _this = this;
var sucCheckImg;
layui.use(['layer','laydate'], function(){
	var laydate = layui.laydate;
	var layer = layui.layer;
	laydate.render({
		elem: '#beginTime'
		,type : 'datetime'
		,format: 'yyyy-MM-dd HH:mm:ss'
	});
	laydate.render({
		elem: '#endTime'
		,type : 'datetime'
		,format: 'yyyy-MM-dd HH:mm:ss'
	});
});
    $(document).ready(function () {

    	$(".chosen-select").chosen({});
    	$("input:radio[name='actionType']").on('ifChecked', function(event){
    		var actionType = $(this).val();
    		if(actionType == "1"){
    			$("#brandDiv").show();
    			$("#goodsDiv").hide();
    			$("#btnGoods").hide();
    			$("input:radio[name='actionType'][value=1]").prop("checked",true);
    		}
    		else{
    			$("#brandDiv").hide();
    			$("#goodsDiv").show();
    			$("#btnGoods").show();
    			$("input:radio[name='actionType'][value=2]").prop("checked",true);
    		}
        });
    	$("input:radio[name='giftType']").on('ifChecked', function(event){
    		var giftType = $(this).val();
    		if(giftType == "1"){
    			$("#giftNum").val(2);
    			$("#giftNum").removeAttr("disabled","disabled");
    			$("input:radio[name='giftType'][value=1]").prop("checked",true);
    		}
    		else{
    			$("#giftNum").val(1);
    			$("#giftNum").attr("disabled","disabled");
    			$("input:radio[name='giftType'][value=2]").prop("checked",true);
    		}
        });
    	$("#btnGoods").click(function(){
    		//商品选择
    		var index = layer.open({
    			type: 2,
    			title: "满赠活动|添加商品",
    			content: "${pageContext.request.contextPath}/admin/shop/action/goodsChoiceList?id=${a.id}",
    			area: ['800px', '550px']
    		});
    		layer.full(index);
    	});
    	$("#btnGift").click(function(){
    		var giftType = $("#giftType").val();
    		var maxLen = 9999;
    		if(giftType == "2"){
    			maxLen = 1;
    		}
    		//商品选择
    		var index = layer.open({
    			type: 2,
    			title: "满赠活动|赠品选择",
    			content: "${pageContext.request.contextPath}/admin/shop/action/giftChoiceList?id=${a.id}&maxLen=" + maxLen,
    			area: ['800px', '550px']
    		});
    		layer.full(index);
    	});
    	$("body").on('click', ".deleteGoods", function(){
    		var goodsId = $(this).attr("itemId");
			console.log("goodsId==="+JSON.stringify(goodsId))
    		var goodsIds = $("#goodsIds").val();
    		var goodsIdArr = null;
    		if(goodsIds == ""){
    			goodsIdArr = [];
    		}
    		else{
    			goodsIdArr = goodsIds.split(",")
    		}
    		var newGoodsArr = [];
    		for(var j = 0; j < goodsIdArr.length; j++){
    			if(goodsIdArr[j] == goodsId){
    				continue;
    			}
    			newGoodsArr.push(goodsIdArr[j]);
    		}
    		$("#goodsIds").val(newGoodsArr.join(","));
			$(this).parent().valueOf()
    		$(this).parent().remove();

    	});

    	$("body").on('click', ".deleteGift", function(){
    		var goodsId = $(this).attr("itemId");
    		var giftIds = $("#giftIds").val();
    		var goodsIdArr = null;
    		if(goodsIds == ""){
    			goodsIdArr = [];
    		}
    		else{
    			goodsIdArr = giftIds.split(",")
    		}
    		var newGoodsArr = [];
    		for(var j = 0; j < goodsIdArr.length; j++){
    			if(goodsIdArr[j] == goodsId){
    				continue;
    			}
    			newGoodsArr.push(goodsIdArr[j]);
    		}
    		$("#giftIds").val(newGoodsArr.join(","));
    		$(this).parent().remove();
    	});
    	var giftType = "${a.giftType}";
    	var actionType = "${a.actionType}";
    	if(actionType != ""){
    		if("1" == actionType){
    			var brandIds = "${brandIds}";
    			var brandArr = brandIds.split(",");
    			for(var i = 0; i < brandArr.length; i++){
    				$("#brandList").find("option[value = '" + brandArr[i] + "']").attr("selected","selected");
    			}
    	    	$('.chosen-select').trigger('chosen:updated');
    		}
    		else {
    			$("#brandDiv").hide();
    			$("#goodsDiv").show();
    			$("#btnGoods").show();
    			$("input:radio[name='actionType'][value=2]").prop("checked",true);
    		}

    	}

    	if(giftType != "" && "2" == giftType){
    		$("#giftNum").val(1);
			$("#giftNum").attr("disabled","disabled");
			$("input:radio[name='giftType'][value=2]").prop("checked",true);
    	}
    	//轮播图片上传
    	$("#uploadGoodsZhutu").click(function(){
    		showPicModal();
    		sucCheckImg = function(url){
    			$("#goods_imgUrl_div_box").html('<img src="' + url + '">');
    			$("#imgUrl").val(url);
    			hidePicModal();
    		}
    	});

  		var pl = parent.layer;
  		var pw = parent.currDialogWindow;
    	$("#btnSave").click(function(){
    		var id = $("#id").val();
    		var name = $("#name").val();
    		var actionType = $("input[name='actionType']:checked").val();
    		var giftType = $("input[name='giftType']:checked").val();

    		var brandList = $("#brandList").val();
    		var goodsIds = $("#goodsIds").val();
    		var conditionMoney = $("#conditionMoney").val();
    		var giftIds = $("#giftIds").val();
    		var beginTime = $("#beginTime").val();
    		var endTime = $("#endTime").val();
    		var giftNum = $("#giftNum").val();
    		if(name == null || name == ""){
    			layer.alert("请输入满赠活动名称");
    			return false;
    		}
    		if(actionType == "1"){
    			if(brandList == "" || brandList == null || brandList == "null"){
    				layer.alert("请选择品牌");
    				return false;
    			}
    		}
    		else{
    			if(goodsIds == ""){
    				layer.alert("请选择商品");
    				return false;
    			}
    		}
    		if(conditionMoney == null || conditionMoney == ""){
    			layer.alert("请输入满足赠品金额");
    			return false;
    		}
    		if(giftIds == ""){
				layer.alert("请选择赠品");
				return false;
			}
    		if(giftType == "1"){
    			if(giftNum == ""){
    				layer.alert("请输入赠品数量");
        			return false;
    			}
    			if(giftIds.split(",").length < parseInt(giftNum)){
					layer.msg("赠品数量没有选择完整哦");
					return false;
				}
    		}
    		else{
				if(giftIds.split(",").length > 1){
					layer.msg("您只能选择一个赠品哦");
					return false;
				}
			}

    		if(beginTime == null || beginTime == ""){
    			layer.alert("请输入开始时间");
    			return false;
    		}
    		if(endTime == null || endTime == ""){
    			endTime = "2099-12-31 23:23:59";
    		}

    		$.post("${pageContext.request.contextPath}/admin/shop/action/saveAction",{
    			id : id,
    			name : name,
    			actionType : actionType,
    			brandList : brandList != null ? brandList.join(","):"",
    			goodsIds : goodsIds,
    			giftIds : giftIds,
    			giftType : giftType,
    			giftNum : giftNum,
    			beginTime : beginTime,
    			endTime : endTime,
    			conditionMoney : conditionMoney
    		},function(json){
    			if(json.code == 0){
    				var index = pl.getFrameIndex(window.name); //先得到当前iframe层的索引
    				pl.close(index); //再执行关闭
    				pl.alert("操作成功");
    				pw.$("#queryBtn").click();
    			}
    			else{
    				layer.alert("操作失败");
    			}
    		},"json");
    	});
		$("#btnCancle").click(function(){
			//关闭操作
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭
		});

    });
</script>
<jsp:include page="../../common/picManage.jsp"></jsp:include>
</body>
</html>
