<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>用户管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/datapicker/datepicker3.css"
          rel="stylesheet">
    <!-- Data Tables -->
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/dataTables/dataTables.bootstrap.css"
          rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>用户管理</h5>
                </div>
                <div class="ibox-content">
                    <form role="form" id="formDiv" class="form-inline">
                        <div class="input-group date" style="margin-top:-5px;">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" id="beginTime" class="form-control" value="" placeholder="注册起始时间">
                        </div>
                        <div class="input-group date" style="margin-top:-5px;">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" id="endTime" class="form-control" value="" placeholder="注册结束时间">
                        </div>
                        <div class="input-group" style="margin-top:-5px;">
                            <input type="text" id="shareCode" class="form-control" value="" placeholder="推荐码">
                        </div>
                        <div class="input-group" style="margin-top:-5px;">
                            <input type="text" id="tshareCode" class="form-control" value="" placeholder="推荐人推荐码">
                        </div>
                        <div class="input-group" style="margin-top:-5px;">
                            <input type="text" id="mobile" class="form-control" value="" placeholder="手机号码">
                        </div>
                        <div class="input-group">
                            <input type="text" class="form-control" id="name" placeholder="请输入用户名称">
                            <span class="input-group-btn">
									<button type="button" class="btn btn-primary" id="queryBtn">搜索</button>
								</span>
                        </div>
                    </form>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="width:100%">
                        <thead>
                        <tr>
                            <th>用户账号</th>
                            <th>用户密码</th>
                            <th>店铺名</th>
                            <th>店铺类型</th>
                            <th>推荐码</th>
                            <th>业务员</th>
                            <th>推荐人推荐码</th>
                            <th>注册地区</th>
                            <th>详细地址</th>
                            <th>账号状态</th>
                            <th>认证状态</th>
                            <th>登录设备</th>
                            <th>可用优惠</th>
                            <th>历史订单</th>
                            <th>最后下单时间</th>
                            <th>注册时间</th>
                            <th>最后登录时间</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/datapicker/bootstrap-datepicker.js"></script>


<script>
    var dataTable;
    var _this = this;
    $(document).ready(function () {
        $("#formDiv .input-group.date").datepicker({
            todayBtn: "linked",
            keyboardNavigation: !1,
            forceParse: !1,
            calendarWeeks: !0,
            autoclose: !0
        });

        var laguage = {
            "sLengthMenu": "每页显示 _MENU_ 条记录",
            "sZeroRecords": "抱歉， 没有找到",
            "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
            "sInfoEmpty": "没有数据",
            "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
            "oPaginate": {
                "sFirst": "首页",
                "sPrevious": "前一页",
                "sNext": "后一页",
                "sLast": "尾页"
            },
            "sZeroRecords": "您请求的内容暂时没有数据",
            "sProcessing": "数据加载中..."
        };
        var aoColumns = [{
            "mData": "mobile"
        }, {
            "mData": "password"
        }, {
            "mData": "name"
        }, {
            "mData": "userType"
        }, {
            "mData": "shareCode"
        }, {
            "mData": "parentName"
        }, {
            "mData": "parentShareCode",
            "mRender": function (data, type, full) {
                return '<div style="text-align: center" type="text" ondblclick="editTShareCode(' + full.id + ')" class="">' + data + '</div>'
            }
        }, {
            "mData": "pro"
        }, {
            "mData": "address"
        }, {
            "mData": "isDelete",
            "mRender": function (data, type, full) {
                if (data == "0") {
                    return '<button onclick="javascript:;" class="btn btn-primary btn-xs"><i class="fa fa-check"></i>启用</button>'
                } else {
                    return '<button onclick="setRecInfo(' + full.id + ')" class="btn btn-danger btn-xs"><i class="fa fa-times"></i>禁用</button>'
                }
            }
        }, {
            "mData": "isAuth",
            "mRender": function (data, type, full) {
                if (data == "1") {
                    //热销推荐
                    return '<button onclick="javascript:;" class="btn btn-primary btn-xs"><i class="fa fa-check"></i>已认证</button>'
                } else {
                    return '<button onclick="setRecInfo(' + full.id + ')" class="btn btn-danger btn-xs"><i class="fa fa-times"></i>未认证</button>'
                }
            }
        }, {
            "mData": "device"
        }, {
            "mData": "coupon",
            "mRender": function (data, type, full) {
                return data + "<br>"
                    + '<button onclick="couponInfo(' + full.id + ')" class="btn btn-success btn-xs"><i class="fa fa-money"></i>可用优惠券</button>'
            }
        }, {
            "mData": "orderSize",
            "mRender": function (data, type, full) {
                return data + "<br>"
                    + '<button onclick="orderInfo(' + full.id + ')" class="btn btn-warning btn-xs"><i class="fa fa-list"></i>历史订单</button>'
                    + '<br><button onclick="faInfo(' + full.id + ')" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i>发放优惠券</button>'
            }
        }, {
            "mData": "lastBuyDate"
        }, {
            "mData": "regDate"
        }, {
            "mData": "lastLoginDate"
        }, {
            "mData": "id",
            "mRender": function (data, type, full) {
                return '<button onclick="deleteInfo(' + data + ')" class="btn btn-danger btn-xs"><i class="fa fa-times"></i>删除</button>'
                    + '<button onclick="editInfo(' + data + ')" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i>编辑</button>';
            }
        }];
        dataTable = $('.dataTables-example').dataTable({
            "dom": 't<"bottom"p>lir<"clear">',
            "aoColumns": aoColumns,
            "oLaguage": laguage,
            "bServerSide": true,
            "bFilter": false,
            "bProcessing": true,
            "bSort": false,
            /* 使用post方式 */
            "iDisplayLength": 10,
            "sAjaxSource": '${pageContext.request.contextPath}/admin/shop/user/loadUserByPage',
            "fnServerParams": function (aoData) {  //查询条件
                aoData.push(
                    {"name": "name", "value": $("#name").val()},
                    {"name": "beginTime", "value": $("#beginTime").val()},
                    {"name": "endTime", "value": $("#endTime").val()},
                    {"name": "mobile", "value": $("#mobile").val()},
                    {"name": "shareCode", "value": $("#shareCode").val()},
                    {"name": "tshareCode", "value": $("#tshareCode").val()}
                );
            },
            "fnServerData": function (sSource, aoData, fnCallback) {
                $.ajax({
                    "dataType": 'json',
                    "type": "post",
                    "url": sSource,
                    "data": aoData,
                    "success": function (json) {
                        $("#iTotalRecords").html(json.iTotalRecords);
                        fnCallback(json);
                    }
                });
            }
        });
        //
        $("#queryBtn").click(function () {
            dataTable.fnDraw();
        });
    });

    function editInfo(data) {
        parent.openDialog("编辑用户信息", "${pageContext.request.contextPath}/admin/shop/user/userInfo?id=" + data, ['800px', '250px'], 1, _this);
    }

    function couponInfo(data) {
        parent.openDialog("可用优惠券信息", "${pageContext.request.contextPath}/admin/shop/user/couponInfo?id=" + data, ['800px', '250px'], 1, _this);
    }

    function orderInfo(data) {
        parent.openDialog("用户历史订单", "${pageContext.request.contextPath}/admin/shop/user/orderInfo?id=" + data, ['800px', '250px'], 1, _this);
    }

    function faInfo(data) {
        parent.openDialog("发放优惠券", "${pageContext.request.contextPath}/admin/shop/user/faInfo?id=" + data, ['800px', '250px'], 1, _this);
    }

    function deleteInfo(data) {
        parent.flyConfirm('确认要删除该用户，删除之后无法恢复，请谨慎操作？', function (index) {
            $.post("${pageContext.request.contextPath}/admin/shop/user/deleteUser?id=" + data, function (json) {
                if (json.code == 0) {
                    parent.flyMsg("删除成功");
                    $("#queryBtn").click();
                } else {
                    parent.flyMsg("删除失败");
                }
            }, "json");
        });
    }
    function editTShareCode(data) {
        layer.prompt({title: '请重新输入推荐人推荐码。', formType: 2}, function (text, index) {
            layer.close(index);
            var parentUserCode = text;
            $.post("${pageContext.request.contextPath}/admin/shop/user/editTShareCode?id=" + data + "&parentUserCode=" + parentUserCode, function (json) {
                if (json.code == 0) {
                    parent.flyMsg("设置成功");
                    $("#queryBtn").click();
                } else {
                    parent.flyMsg("设置失败");
                }
            }, "json");
        });
    }
</script>
</body>
</html>
