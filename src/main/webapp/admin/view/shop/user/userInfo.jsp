<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>用户信息编辑</title>
    <link rel="shortcut icon" href="favicon.ico"> 
   	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/layui/css/layui.css?v=4.1.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/app.css?v=4.1.0" rel="stylesheet"> 
    <style type="text/css">
    .layui-upload-img {
	    width: 92px;
	    height: 92px;
	    margin: 0 10px 10px 0;
	} 
    </style>
</head> 

<body class="gray-bg"> 
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
    	<div class="col-sm-12">  
    		<div class="ibox float-e-margins" style="height:100%;">
                <div class="ibox-content"> 
                	<form class="form-horizontal">
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">用户手机号码：</label>
					        <div class="col-sm-6"> 
					        	<input type="hidden" value="${a.id }" id="id">  
					            <input type="text" id="mobile" disabled value="${a.mobile }"
					            	 class="form-control" placeholder="请输入店铺类型名称">
					        </div> 
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">店铺名称：</label>
					        <div class="col-sm-6"> 
					            <input type="text" id="name" value="${a.name }"
					            	 class="form-control" placeholder="请输入店铺名称">
					        </div>
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">选择店铺类型：</label>
					        <div class="col-sm-6">
					            <select class="form-control" id="userType">
					            	<option value="">请选择店铺类型</option>
									<c:forEach items="${userTypeList}" var="item">
									<c:if test="${a.userType.id == item.id }">
									<option value="${item.id }" selected>${item.name }</option>
									</c:if> 
									<c:if test="${a.userType.id != item.id }">
									<option value="${item.id }">${item.name }</option>
									</c:if>
									</c:forEach>
					            </select>
					        </div>
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">用户昵称：</label>
					        <div class="col-sm-6"> 
					            <input type="text" id="nick" disabled value="${a.nick }"
					            	 class="form-control" placeholder="请输入用户昵称">
					        </div>
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">所在地区：</label>
					        <div class="col-md-2">
								<select class="form-control" name="pro" id="pro"  >  
									<option value="">请选择省份</option>
								</select>
							</div>
							<div class="col-md-2">
								<select class="form-control" name="city" id="city"> 
									<option value=''>请选择城市</option>
								</select> 
							</div>  
							<div class="col-md-2">
								<select class="form-control" name="country" id="country"> 
									<option value=''>请选择区/县</option>
								</select>  
							</div>
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">详细地址：</label>
					        <div class="col-sm-6"> 
					            <input type="text" id="address" value="${a.address }"
					            	 class="form-control" placeholder="请输入详细地址">
					        </div>
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">认证状态：</label>
					        <div class="col-md-2">
								<select class="form-control" name="isAuth" id="isAuth"  >  
									<option value="0">未认证</option>
									<option value="1">已认证</option>
								</select>
							</div>
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">是否禁用：</label>
					        <div class="col-md-2">
								<select class="form-control" name="isDelete" id="isDelete"  >  
									<option value="0">未禁用</option>
									<option value="1">已禁用</option>
								</select>
							</div>
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">是否业务员：</label>
					        <div class="col-md-2">
								<select class="form-control" name="isYewuyuan" id="isYewuyuan"  >  
									<option value="0">非业务员</option>
									<option value="1">是业务员</option>
								</select>
							</div>
					    </div>
						<div class="form-group">
							<label class="col-sm-3 control-label">推荐人推荐码：</label>
							<div class="col-sm-6">
								<input type="text" id="code" value="${a.name }"
									   class="form-control" placeholder="推荐人推荐码">
							</div>
						</div>
					     <div class="form-group"> 
					        <label class="col-sm-3 control-label">备注信息：</label>
					        <div class="col-md-6">
								<textarea class="form-control" id="bak"  cols="">${a.bak }</textarea>
							</div>
					    </div>
					    <div class="form-group draggable ui-draggable">
                            <div class="col-sm-12 col-sm-offset-3"> 
                                <button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;提交</button>
                                <button class="btn btn-white" type="button" id="btnCancle">取消</button>
                            </div>
                        </div>
					</form> 
                </div> 
            </div> 
    	</div> 
    </div>
</div>

<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layui/layui.js"></script>
<script>   
var sucCheckImg;
layui.use(['layer'], function(){
});  	
    $(document).ready(function () {
    	var id = "${a.id}";
    	var isDelete = "${a.isDelete}";
    	var isAuth = "${a.isAuth}";
    	var isYewuyuan = "${a.isYewuyuan}";

    	$("#isDelete").val(isDelete);
    	$("#isAuth").val(isAuth); 
    	if(isYewuyuan){
    		$("#isYewuyuan").val(isYewuyuan);
    	}
    	$("#pro").change(function(){
    		var proName = $(this).find("option:selected").text(); 

    		if(proName != "请选择省份"){ 
    			$.getJSON("${pageContext.request.contextPath}/admin/static/js/address.js",function(json){
    				var len = json.length;
    				for(var i = 0; i < len; i++){
    					var proItem = json[i];
    					if(proItem.name == proName){
    						var cityArr = proItem.city;
    						$("#city").empty(); 
    						$("#city").append("<option value=''>请选择城市</option>"); 
    						$.each(cityArr,function(i,item){
    							var option = "<option src='" + item.id + "'>" + item.name + "</option>"; 
    							$("#city").append(option); 
    						});
    						break;
    					}
    				}
    				
    			});
    		}
    	});
    	$("#city").change(function(){ 
    		var proName = $("#pro").find("option:selected").text();
    		var cityName = $(this).find("option:selected").text(); 
     
    		if(proName != "请选择省份" && cityName != "请选择城市"){ 
    			$.getJSON("${pageContext.request.contextPath}/admin/static/js/address.js",function(json){
    				var len = json.length;
    				for(var i = 0; i < len; i++){
    					var proItem = json[i];
    					
    					if(proItem.name == proName){
    						console.log(proItem.name);
    						var cityArr = proItem.city;
    						for(var j = 0; j < cityArr.length; j++){
    							var cityItem = cityArr[j];
    							if(cityItem.name == cityName){
    								console.log(cityItem.name); 
    								var arr = cityItem.district;
    								$("#country").empty();  
    								$("#country").append("<option value=''>请选择区县</option>"); 
    								$.each(arr,function(i,item){
    									var option = "<option src='" + item.id + "'>" + item.name + "</option>"; 
    									$("#country").append(option); 
    								});
    							}
    						}
    						break;
    					}
    				}
    				
    			});
    		}
    	});
    	if(id == ''){ 
    		
    		//新的 
    		$.getJSON("${pageContext.request.contextPath}/admin/static/js/address.js",function(json){
    			console.log(json); 
    			$.each(json,function(i,item){ 
    				var option = "<option src='" + item.id + "'>" + item.name + "</option>"; 
    				$("#pro").append(option); 
    			});
    			
    		});
    	}
    	else{
    		var proName = "${a.pro}";
    		var cityName = "${a.city}";
    		var countryName = "${a.county}";  
    		//新的 
    		$.getJSON("${pageContext.request.contextPath}/admin/static/js/address.js",function(json){
    			for(var i = 0; i < json.length; i++){
    				var item = json[i]; 
    				if(item.name == proName){ 
    					var option = "<option selected src='" + item.id + "'>" + item.name + "</option>"; 
    					$("#pro").append(option);
    				}
    				else{
    					var option = "<option src='" + item.id + "'>" + item.name + "</option>"; 
    					$("#pro").append(option);
    				}
    				 
    				if(item.name == proName){
    					for(var j = 0; j < item.city.length; j++){
    						var item2 = item.city[j]; 
    						if(item2.name == cityName){ 
    							var option2 = "<option selected src='" + item2.id + "'>" + item2.name + "</option>"; 
    							$("#city").append(option2); 
    						}
    						else{
    							var option2 = "<option src='" + item2.id + "'>" + item2.name + "</option>"; 
    							$("#city").append(option2); 
    						}
    						
    						if(item2.name == cityName){   
    							for(var k = 0; k < item2.district.length; k++){
    								var item3 = item2.district[k]; 
    								if(item3.name == countryName){  
    									var option3 = "<option selected src='" + item3.id + "'>" + item3.name + "</option>"; 
    									$("#country").append(option3); 
    								}
    								else{
    									var option3 = "<option src='" + item3.id + "'>" + item3.name + "</option>"; 
    									$("#country").append(option3); 
    								}
    							}
    						}
    					}
    				}
    			}     
    		});
    	}
  		var pl = parent.layer;
  		var pw = parent.currDialogWindow;
    	$("#btnSave").click(function(){
    		var id = $("#id").val();
    		var name = $("#name").val();
    		var pro = $("#pro").val();
    		var city = $("#city").val();
    		var county = $("#country").val();
    		var address = $("#address").val();
    		var bak = $("#bak").val();
    		var isYewuyuan = $("#isYewuyuan").val();
    		var isAuth = $("#isAuth").val();
    		var isDelete = $("#isDelete").val();
    		var userType = $("#userType").val();
    		if(name == null || name == ""){ 
    			layer.alert("请输入店铺名称");
    			return false; 
    		}
    		$.post("${pageContext.request.contextPath}/admin/shop/user/saveUser",{
    			id : id,
    			name : name,
    			pro : pro,
    			city :city,
    			county : county,
    			isYewuyuan : isYewuyuan,
    			isAuth : isAuth,
    			isDelete : isDelete,
    			userType : userType,
    			address :address,
    			bak: bak
    		},function(json){
    			if(json.code == 0){  
    				var index = pl.getFrameIndex(window.name); //先得到当前iframe层的索引
    				pl.close(index); //再执行关闭 
    				pl.alert("操作成功");
    				pw.$("#queryBtn").click();
    			}  
    			else{    
    				layer.alert("操作失败");
    			}
    		},"json");
    	});
		$("#btnCancle").click(function(){
			//关闭操作
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭  
		}); 
    	
    });
</script>
</body>
</html>
