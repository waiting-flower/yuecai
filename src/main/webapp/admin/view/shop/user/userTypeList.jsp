<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>用户类型管理</title>
    <link rel="shortcut icon" href="favicon.ico">
	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
	
    <!-- Data Tables -->
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row"> 
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
            	<div class="ibox-title">
                    <h5>店铺类型</h5>
                </div> 
					<div class="ibox-content">
						<form role="form" class="form-inline">
							<div class="input-group">
								<input type="text" class="form-control" id="title" placeholder="请输入店铺类型名称">
								<span class="input-group-btn"> 
									<button type="button" class="btn btn-primary" id="queryBtn">搜索</button>
								</span>
							</div>      
							<button type="button" class="btn btn-w-m btn-success" id="addBtn">
								<i class="fa fa-plus"></i>&nbsp;&nbsp;添加新店铺类型
							</button> 
						</form> 
					</div>
					<div class="ibox-content">  
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="width:100%">
                        <thead>
                        <tr>
                            <th>店铺类型名称</th>
							<th>创建时间</th>
							<th>操作</th>
                        </tr> 
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 
<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
 
<script> 
	var dataTable;
	var _this = this; 
    $(document).ready(function () {
    	var laguage = {
    			"sLengthMenu" : "每页显示 _MENU_ 条记录",
    			"sZeroRecords" : "抱歉， 没有找到",
    			"sInfo" : "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
    			"sInfoEmpty" : "没有数据",
    			"sInfoFiltered" : "(从 _MAX_ 条数据中检索)",
    			"oPaginate" : {
    				"sFirst" : "首页",
    				"sPrevious" : "前一页",
    				"sNext" : "后一页",
    				"sLast" : "尾页"
    			},
    			"sZeroRecords" : "您请求的内容暂时没有数据",
    			"sProcessing" : "数据加载中..."
    		};  
    	var aoColumns = [{
    		"mData" : "name"
    	}, {  
    		"mData" : "time"  
    	}, { 
    		"mData" : "id", 
    		"mRender" : function(data,type,full){     
    			return '<button onclick="deleteInfo(' + data + ')" class="btn btn-danger btn-xs"><i class="fa fa-times"></i>删除</button>'
    			+ '<button onclick="editInfo(' + data + ')" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i>编辑</button>'; 
    		}       
    	} ]; 
        dataTable = $('.dataTables-example').dataTable({
        	"dom": 't<"bottom"p>lir<"clear">',
    		"aoColumns" : aoColumns,   
    		"oLaguage" : laguage,
    		"bServerSide" : true,  
    		"bFilter" : false,  
    		"bProcessing" : true, 
    		"bSort" : false, 
    		/* 使用post方式 */ 
    		"iDisplayLength" :10,     
    	     "sAjaxSource": '${pageContext.request.contextPath}/admin/shop/user/loadUserTypeByPage',   
    	     "fnServerParams": function (aoData) {  //查询条件
    				aoData.push( 
    						{ "name": "name", "value": $("#name").val()}
    	            );
    	      },
    	     "fnServerData" : function(sSource, aoData, fnCallback) {  
    	    	 $.ajax({
    					"dataType" : 'json',
    					"type" : "post",
    					"url" : sSource,
    					"data" : aoData,
    					"success" : function(json){
    						$("#iTotalRecords").html(json.iTotalRecords);
    						fnCallback(json);
    					}
    				}); 
    	     }  
    	});
    	//
    	$("#queryBtn").click(function() {
    		dataTable.fnDraw();
    	});
        $("#addBtn").click(function(){    
        	parent.openDialog("新增店铺类型","${pageContext.request.contextPath}/admin/shop/user/userTypeInfo",['800px', '250px'],2,_this);
        });   
    });    
    function editInfo(data){    
    	parent.openDialog("编辑店铺类型","${pageContext.request.contextPath}/admin/shop/user/userTypeInfo?id=" + data,['800px', '250px'],2,_this);
    } 
    function deleteInfo(data){
    	parent.flyConfirm('确认要删除该店铺类型，删除之后无法恢复，请谨慎操作？',function(index){   
    		$.post("${pageContext.request.contextPath}/admin/shop/user/deleteUserType?id=" + data,function(json){
    			if(json.code == 0){
    				parent.flyMsg("删除成功");  
    				$("#queryBtn").click(); 
    			}
    			else{ 
    				parent.flyMsg("删除失败");
    			} 
    		},"json"); 
    	});  
    }
</script>
</body>
</html>
