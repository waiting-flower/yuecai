<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>分单管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/tagsinput/bootstrap-tagsinput.css"
          rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/dataTables/dataTables.bootstrap.css"
          rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/layui/css/layui.css?v=4.1.0"
          rel="stylesheet">
    <script type="text/javascript"
            src="http://api.map.baidu.com/api?v=2.0&ak=QNo49R0NgQ473BxbBvDtE0DUnaf69A9x"></script>
    <style type="text/css">
        .bootstrap-tagsinput .label {
            font-size: 12px;
        }

        .layui-upload-img {
            width: 92px;
            height: 92px;
            margin: 0 10px 10px 0;
        }
    </style>
    <link href="${pageContext.request.contextPath}/admin/static/css/app.css?v=4.1.0" rel="stylesheet">
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        分单完成后，订单状态自动变更为【已发货，待收货】 。
        操作方法：选中司机之后，可以获取到司机当前配送路线以及订单信息。<br>
        确认分配订单后，地图会增加显示新分配的订单
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>司机选择</h5>
                    </div>
                    <div class="ibox-content">
                        <form role="form" id="formDiv" class="form-inline">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">请选择司机：</label>
                                <div class="col-sm-4">
                                    <select class="form-control" name="state" id="driverId">
                                        <option value="0">请选择司机列表</option>
                                        <c:forEach items="${driverList }" var="item">
                                            <option value='${item.id }'>${item.name }-${item.mobile }-${item.licenseNo }</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <button class="btn btn-primary " style="margin-left:20px;" type="button"
                                            id="btnChoice"><i class="fa fa-check"></i>&nbsp;选此司机
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>司机目前订单列表</h5>
                        <button type="button" class="btn btn-xs btn-danger pull-right" onclick="deleteAll()">
                            <i class="fa fa-trash-o"></i>&nbsp;&nbsp;一键删除
                        </button>
                        <button type="button" class="btn btn-xs btn-primary pull-right" id="showPeisong">
                            <i class="fa fa-trunk"></i>&nbsp;&nbsp;配送单
                        </button>
                        <button type="button" class="btn btn-xs btn-success pull-right" id="showFenjian">
                            <i class="fa fa-plus"></i>&nbsp;&nbsp;分拣单
                        </button>
                    </div>
                    <div class="ibox-content" style="max-height:300px;overflow-y:auto;">
                        <table id="driverTable"
                               class="table table-striped table-bordered table-hover dataTables-example"
                               style="width:100%">
                            <thead>
                            <tr>
                                <th>司机姓名</th>
                                <th>商品名称</th>
                                <th>商品数量</th>
                                <th>收货地址</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>待分单列表
                        </h5>
                        <button type="button" class="btn btn-xs btn-success pull-right" id="showMap">
                            <i class="fa fa-plus"></i>&nbsp;&nbsp;地图显示
                        </button>
                    </div>
                    <div class="ibox-content" style="max-height:400px;overflow-y:auto;">
                        <div class="col-sm-10" style="padding-left:0px;">
                            <div class="input-group date" style="margin-top:-5px;">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" id="beginTime" class="form-control" value="" placeholder="开始时间">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" id="endTime" class="form-control" value="" placeholder="结束时间">
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <button type="button" class="btn btn-xs btn-primary" id="queryBtn">
                                <i class="fa fa-search"></i>&nbsp;&nbsp;查询
                            </button>
                        </div>
                        <table style="margin-top:12px;" id="orderTable"
                               class="table table-striped table-bordered table-hover dataTables-example"
                               style="width:100%">
                            <thead>
                            <tr>
                                <th>商品名称</th>
                                <th>所属用户</th>
                                <th>收货信息</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>地图信息
                    </h5>
                </div>
                <div class="ibox-content" style="max-height:500px;overflow-y:auto;">
                    <div id="allmap" style="height:500px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layui/layui.js"></script>

<script>
    layui.use(['layer', 'laydate'], function () {
        var layer = layui.layer;
        var laydate = layui.laydate;
        //日期时间选择器
        laydate.render({
            elem: '#beginTime'
            , type: 'datetime'
        });
        //日期时间选择器
        laydate.render({
            elem: '#endTime'
            , type: 'datetime'
        });
    });
    var stateMap = {
        "0": "待付款",
        "1": "待发货",
        "2": "已发货",
        "3": "已收货，待评价",
        "4": "已完成",
        "-1": "已取消",
        "5": "货到付款，待确认"
    };
    var dataTable;
    var _this = this;
    var nowDriverId = "";
    $(document).ready(function () {
        $("#formDiv .input-group.date").datepicker({
            todayBtn: "linked",
            keyboardNavigation: !1,
            forceParse: !1,
            calendarWeeks: !0,
            autoclose: !0
        });
        var laguage = {
            "sLengthMenu": "每页显示 _MENU_ 条记录",
            "sZeroRecords": "抱歉， 没有找到",
            "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
            "sInfoEmpty": "没有数据",
            "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
            "oPaginate": {
                "sFirst": "首页",
                "sPrevious": "前一页",
                "sNext": "后一页",
                "sLast": "尾页"
            },
            "sZeroRecords": "您请求的内容暂时没有数据",
            "sProcessing": "数据加载中..."
        };
        var aoColumns = [{
            "mData": "goodsName"
        }, {
            "mData": "nick",
            "mRender": function (data, type, full) {
                return '<img src="' + full.headUrl + '" style="width:45px;height:45px;"><br>' + data;
            }
        }, {
            "mData": "name",
            "mRender": function (data, type, full) {
                return name + "<br>" + full.mobile + "<br>" + full.address;
            }
        }, {
            "mData": "id",
            "mRender": function (data, type, full) {
                return '<button onclick="fenpeiInfo(' + data + ')" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i>分配此订单</button>';
            }
        }];
        dataTable = $('#orderTable').dataTable({
            "dom": 't<"bottom"p>lir<"clear">',
            "aoColumns": aoColumns,
            "oLaguage": laguage,
            "bServerSide": true,
            "bFilter": false,
            "bProcessing": true,
            "bSort": false,
            /* 使用post方式 */
            "iDisplayLength": 10,
            "sAjaxSource": '${pageContext.request.contextPath}/admin/shop/order/loadOrderByPage',
            "fnServerParams": function (aoData) {  //查询条件
                aoData.push(
                    {"name": "orderNo", "value": $("#orderNo").val()},
                    {"name": "orderState", "value": 1},
                    {"name": "beginTime2", "value": $("#beginTime").val()},
                    {"name": "endTime2", "value": $("#endTime").val()}
                );
            },
            "fnServerData": function (sSource, aoData, fnCallback) {
                $.ajax({
                    "dataType": 'json',
                    "type": "post",
                    "url": sSource,
                    "data": aoData,
                    "success": function (json) {
                        $("#iTotalRecords").html(json.iTotalRecords);
                        fnCallback(json);
                    }
                });
            }
        });
        //
        $("#queryBtn").click(function () {
            dataTable.fnDraw(false);
            map.clearOverlays();
            var beginTime = $("#beginTime").val();
            var endTime = $("#endTime").val();
            $.getJSON("${pageContext.request.contextPath}/admin/shop/order/getNowAllDaifahuoOrderList", {
                beginTime: beginTime,
                endTime: endTime
            }, function (json) {
                var list = json.list;
                console.log(list);
                $.each(list, function (i, item) {
                    myGeo.getPoint(item.address, function (point) {
                        if (point) {
                            map.centerAndZoom(point, 16);
                            var myIcon = new BMap.Icon("http://youji.7working.com/FpWEAySHe0mZ7_RY47myLVn7NK92", new BMap.Size(48, 48));
                            var marker2 = new BMap.Marker(point, {icon: myIcon});  // 创建标注
                            marker2.addEventListener("click", choiceClickOrder);

                            function choiceClickOrder() {
                                var p = marker2.getPosition();  //获取marker的位置
                                if (nowDriverId == "") {
                                    layer.msg("请选择一个司机之后再进行此操作");
                                    return false;
                                }
                                var name = $("#driverId").find("option:selected").text();
                                parent.flyConfirm('确认要将该订单分单给司机' + name + '吗？', function (index) {
                                    $.post("${pageContext.request.contextPath}/admin/shop/order/saveOrderFendan?id=" + item.id + "&driverId=" + nowDriverId, function (json) {
                                        if (json.code == 0) {
                                            parent.flyMsg("分单成功");
                                            dataTable.fnDraw(false);
                                            dataTable2.fnDraw(false);
                                            map.removeOverlay(marker2);
                                        } else {
                                            parent.flyMsg("分单失败");
                                        }
                                    }, "json");
                                });
                            }

                            map.addOverlay(marker2);
                        } else {
                            alert("您选择地址没有解析到结果!");
                        }
                    }, "北京市");
                });
            });
        });


    });

    var dataTable2;
    var _this2 = this;
    $(document).ready(function () {
        var laguage2 = {
            "sLengthMenu": "每页显示 _MENU_ 条记录",
            "sZeroRecords": "抱歉， 没有找到",
            "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
            "sInfoEmpty": "没有数据",
            "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
            "oPaginate": {
                "sFirst": "首页",
                "sPrevious": "前一页",
                "sNext": "后一页",
                "sLast": "尾页"
            },
            "sZeroRecords": "您请求的内容暂时没有数据",
            "sProcessing": "数据加载中..."
        };
        var aoColumns2 = [{
            "mData": "driverName"
        }, {
            "mData": "goodsName"
        }, {
            "mData": "num"
        }, {
            "mData": "address"
        }, {
            "mData": "id",
            "mRender": function (data, type, full) {
                return '<button onclick="deleteInfo(' + data + ')" class="btn btn-danger btn-xs"><i class="fa fa-times"></i>删除</button>';
            }
        }];
        dataTable2 = $('#driverTable').dataTable({
            "dom": 't<"bottom"p>r<"clear">',
            "aoColumns": aoColumns2,
            "oLaguage": laguage2,
            "bServerSide": true,
            "bFilter": false,
            "bProcessing": true,
            "bSort": false,
            /* 使用post方式 */
            "iDisplayLength": 1000,
            "sAjaxSource": '${pageContext.request.contextPath}/admin/shop/order/loadOrderByPage',
            "fnServerParams": function (aoData) {  //查询条件
                aoData.push(
                    {"name": "driverId", "value": $("#driverId").val()}
                );
            },
            "fnServerData": function (sSource, aoData, fnCallback) {
                $.ajax({
                    "dataType": 'json',
                    "type": "post",
                    "url": sSource,
                    "data": aoData,
                    "success": function (json) {
                        $("#iTotalRecords").html(json.iTotalRecords);
                        fnCallback(json);
                    }
                });
            }
        });
    });

    //百度地图API功能
    // 百度地图API功能
    var map = new BMap.Map("allmap", {minZoom: 4, maxZoom: 16});
    var point = new BMap.Point(116.331398, 39.897445);
    map.centerAndZoom(point, 12);
    map.enableScrollWheelZoom(true);
    // 创建地址解析器实例
    var myGeo = new BMap.Geocoder();


    $("#showMap").click(function () {
        map.clearOverlays();
        var beginTime = $("#beginTime").val();
        var endTime = $("#endTime").val();
        $.getJSON("${pageContext.request.contextPath}/admin/shop/order/getNowAllDaifahuoOrderList", {
            beginTime: beginTime,
            endTime: endTime
        }, function (json) {
            var list = json.list;
            console.log(list);
            $.each(list, function (i, item) {
                myGeo.getPoint(item.address, function (point) {
                    if (point) {
                        map.centerAndZoom(point, 16);
                        var myIcon = new BMap.Icon("http://youji.7working.com/FpWEAySHe0mZ7_RY47myLVn7NK92", new BMap.Size(48, 48));
                        var marker2 = new BMap.Marker(point, {icon: myIcon});  // 创建标注
                        map.addOverlay(marker2);
                    } else {
                        alert("您选择地址没有解析到结果!");
                    }
                }, "北京市");
            });
        });
    });

    function deleteInfo(data) {
        parent.flyConfirm('确认从该司机的配送订单列表中移除该订单吗？', function (index) {
            $.post("${pageContext.request.contextPath}/admin/shop/order/saveOrderFendanRemove?id=" + data, function (json) {
                if (json.code == 0) {
                    parent.flyMsg("操作成功");
                    dataTable.fnDraw(false);
                    dataTable2.fnDraw(false);
                } else {
                    parent.flyMsg("操作失败");
                }
            }, "json");
        });
    }

    function deleteAll() {
        parent.flyConfirm('确认清空该司机的所有配送订单吗？', function (index) {
            var table = $('#driverTable').dataTable();
            var nTrs = table.fnGetNodes();//fnGetNodes获取表格所有行，nTrs[i]表示第i行tr对象
            for (var i = 0; i < nTrs.length; i++) {
                var tid = table.fnGetData(nTrs[i]).id;
                $.post("${pageContext.request.contextPath}/admin/shop/order/saveOrderFendanRemove?id=" + tid, function () {
                    parent.flyMsg("操作成功");
                    table.fnDraw(false);
                });
            }
        });
    }

    function fenpeiInfo(data) {
        if (nowDriverId == "") {
            layer.msg("请选择一个司机之后再进行此操作");
            return false;
        }
        var name = $("#driverId").find("option:selected").text();
        parent.flyConfirm('确认要将该订单分单给司机' + name + '吗？', function (index) {
            $.post("${pageContext.request.contextPath}/admin/shop/order/saveOrderFendan?id=" + data + "&driverId=" + nowDriverId, function (json) {
                if (json.code == 0) {
                    parent.flyMsg("分单成功");
                    dataTable.fnDraw(false);
                    dataTable2.fnDraw(false);
                    map.clearOverlays();
                    $.getJSON("${pageContext.request.contextPath}/admin/shop/order/getDriverOrderList?driverId=" + nowDriverId, function (json) {
                        var list = json.list;
                        console.log(list);
                        $.each(list, function (i, item) {
                            myGeo.getPoint(item.address, function (point) {
                                if (point) {
                                    map.centerAndZoom(point, 16);
                                    var myIcon = new BMap.Icon("http://www.yuecaikeji.cn/wap/images/Fub15kAbs44i-ST9d35kEzxSg_3c.png", new BMap.Size(50, 43));
                                    var marker2 = new BMap.Marker(point, {icon: myIcon});  // 创建标注
                                    map.addOverlay(marker2);
                                } else {
                                    alert("您选择地址没有解析到结果!");
                                }
                            }, "北京市");
                        });
                    });
                } else {
                    parent.flyMsg("分单失败");
                }
            }, "json");
        });
    }

    $("#btnChoice").click(function () {
        var id = $("#driverId").val();
        if (id == "0") {
            layer.msg("请选择一个司机后继续操作");
            return false;
        }
        nowDriverId = id;
        dataTable2.fnDraw(false);
        $.getJSON("${pageContext.request.contextPath}/admin/shop/order/getDriverOrderList?driverId=" + nowDriverId, function (json) {
            var list = json.list;
            map.clearOverlays();
            $.each(list, function (i, item) {
                myGeo.getPoint(item.address, function (point) {
                    if (point) {
                        map.centerAndZoom(point, 16);
                        var myIcon = new BMap.Icon("http://www.yuecaikeji.cn/wap/images/Fub15kAbs44i-ST9d35kEzxSg_3c.png", new BMap.Size(50, 43));
                        var marker2 = new BMap.Marker(point, {icon: myIcon});  // 创建标注
                        map.addOverlay(marker2);
                    } else {
                        alert("您选择地址没有解析到结果!");
                    }
                }, "北京市");
            });
        });

    });
    $("#showPeisong").click(function () {
        if (nowDriverId == "") {
            layer.msg("请选择一个司机之后再进行此操作");
            return false;
        }
        parent.openDialog("配送单详情", "${pageContext.request.contextPath}/admin/shop/order/peisongdan?id=" + nowDriverId, ['800px', '550px'], 1, _this);
    });
    $("#showFenjian").click(function () {
        if (nowDriverId == "") {
            layer.msg("请选择一个司机之后再进行此操作");
            return false;
        }
        parent.openDialog("分拣单详情", "${pageContext.request.contextPath}/admin/shop/order/fenjiandan?id=" + nowDriverId, ['800px', '550px'], 1, _this);
    });
</script>
</body>
</html>
