<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>打印单</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">

    <!-- Data Tables -->
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
<c:forEach items="${mapList }" var="item" varStatus="listStatus">
    <%--<div class="col-sm-12">
        <button class="btn btn-info pull-right" onclick="myPrint(document.getElementById('target_${item.a.id}'))">打 印</button>
        <div id="dayin_${item.a.id }" style="width:100%;padding:15px;height:auto;margin-bottom:15px;border:1px solid #e5e5e5;">
            <div style="font-size:18px;font-weight:bold;color:blue;text-align:center;">
                物流配送单
                <span style="float:right;font-size:14px;font-weight:normal;">购买日期：${item.a.createDate }</span>
            </div>
            <div style="color:blue;margin-top:10px;font-size:14px;text-align:left;">
                <span style="display:inline-block;width:33%;">收货单位：${item.a.user.name }</span>
                <span style="display:inline-block;width:33%;">电话：${item.a.user.mobile }</span>
                <span style="display:inline-block;width:30%;">客户编码：${item.a.user.id }</span>
            </div>
            <div style="color:blue;margin-top:10px;font-size:14px;text-align:left;">
                <span style="display:inline-block;width:60%;">地址：${item.a.address }</span>
                <span style="display:inline-block;width:30%;">单据编号：${item.a.orderNo }</span>
            </div>
            <table border="1" cellspacing="0" cellpadding="0" style="width:100%;margin-top:10px;color:blue;">
                <tr>
                    <td align="center">货品编码</td>
                    <td align="center">货品名称</td>
                    <td align="center">规格</td>
                    <td align="center">数量</td>
                    <td align="center">单位</td>
                    <td align="center">单价</td>
                    <td align="center">货款</td>
                </tr>
                <c:forEach items="${item.odList }" var="d">
                    <tr>
                        <td>${d.goods.goodsNo }</td>
                        <td>${d.goods.name }</td>
                        <td>${d.sku.parmasValuesJson }</td>
                        <td  align="center">${d.num }</td>
                        <td>${d.sku.unit }</td>
                        <td align="right">${d.price }</td>
                        <td align="right">${d.money }</td>
                    </tr>
                </c:forEach>
                <tr>
                    <td align="center">优惠金额</td>
                    <td align="center" colspan="2">${item.a.couponMoney }</td>
                    <td align="center">${item.a.num }</td>
                    <td align="center" colspan="2"></td>
                    <td align="center">${item.a.money }</td>
                </tr>
                <tr>
                    <td align="center">备注</td>
                    <td align="center" colspan="2">${item.a.bak }${item.gift}</td>
                    <td align="center">支付状态：</td>
                    <td align="center">未支付</td>
                    <td align="center">应收额</td>
                    <td align="center">${item.a.money }</td>
                </tr>
            </table>
            <div style="color:blue;margin-top:10px;font-size:14px;text-align:left;">
                <span style="display:inline-block;width:33%;">司机：${item.a.driver.name }</span>
                <span style="display:inline-block;width:33%;">司机电话：${item.a.driver.mobile }</span>
                <span style="display:inline-block;width:30%;">客服电话：0535 408 0200</span>
            </div>
        </div>
    </div>--%>
    <c:if test="${item.odPage==1}">
        <%--<div id="target_${item.a.id }" style="width:99%;height:auto;border:1px solid #e5e5e5;margin-left: 20px;">--%>
        <div class="col-sm-12" style="width:95%;height:auto;border:1px solid #e5e5e5;margin-left:50px;<c:if test="${listStatus.count==1}"> margin-top:49px;</c:if><c:if test="${listStatus.count>1}"> margin-top:98px;</c:if>">
            <div style="font-size:14px;font-weight:bold;color:blue;text-align:center;">
                物流配送单
                <span style="float:right;font-size:14px;font-weight:normal;">购买日期：${item.a.createDate }</span>
            </div>
            <div style="color:blue;font-size:14px;text-align:left;">
                <span style="display:inline-block;width:33%;">收货单位：【${item.a.user.name }】--${item.a.name }</span>
                <span style="display:inline-block;width:34%;">电话：${item.a.mobile }</span>
                <span style="display:inline-block;width:29%;">客户编码：${item.a.user.id }</span>
            </div>
            <div style="color:blue;font-size:14px;text-align:left;">
                <span style="display:inline-block;width:50%;">地址：${item.a.address }</span>
                <span style="display:inline-block;width:49%;">单据编号：${item.a.orderNo }</span>
            </div>
            <table border="1" cellspacing="0" cellpadding="0" style="width:100%;color:blue;font-size:14px;">
                <tr>
                    <td align="center">货品编码</td>
                    <td align="center">货品名称</td>
                    <td align="center">数量</td>
                    <td align="center">单位</td>
                    <td align="center">单价</td>
                    <td align="center">货款</td>
                </tr>
                <c:forEach items="${item.odList }" var="d">
                    <tr>
                        <td>&nbsp;${d.goods.goodsNo }</td>
                        <td>&nbsp;${d.goods.name }</td>
                        <td  align="center">${d.num }</td>
                        <td align="center">${fn:substring(d.sku.parmasValuesJson,fn:indexOf(d.sku.parmasValuesJson,":") +2,fn:length(d.sku.parmasValuesJson) -2)}</td>
                        <td align="center">${d.price }</td>
                        <td align="center">${d.money }</td>
                    </tr>
                </c:forEach>
                <c:if test="${fn:length(item.odList) < 5}">
                    <c:forEach var="i" begin="${fn:length(item.odList)+1}" end="5" step="1">
                        <tr>
                            <td>&ensp; </td>
                            <td>&ensp; </td>
                            <td>&ensp; </td>
                            <td>&ensp; </td>
                            <td>&ensp; </td>
                            <td>&ensp; </td>
                        </tr>
                    </c:forEach>
                </c:if>
                <tr>
                    <td align="center">优惠金额</td>
                    <td align="center" >${item.a.couponMoney }</td>
                    <td align="center">${item.a.num }</td>
                    <td align="center" colspan="2"></td>
                    <td align="center">${item.a.money }</td>
                </tr>
                <tr>
                    <td align="center">备注</td>
                    <td align="center" >${item.a.bak }${item.gift}</td>
                    <td align="center">支付状态：</td>
                    <td align="center">未支付</td>
                    <td align="center">应收额</td>
                    <td align="center">${item.a.money }</td>
                </tr>
            </table>
            <div style="color:blue;font-size:14px;text-align:left;">
                <span style="display:inline-block;width:33%;">司机：${item.a.driver.name }</span>
                <span style="display:inline-block;width:33%;">司机电话：${item.a.driver.mobile }</span>
                <span style="display:inline-block;width:30%;">客服电话：0535 408 0200</span>
            </div>
        </div>
        <%--</div>--%>
    </c:if>
    <c:if test="${item.odPage>1}">
        <%--<div id="target_${item.a.id }" style="width: 99%;font-size: 14px;margin-left:20px;">--%>
        <c:forEach items="${item.odPageInfo}" var="pageInfo" varStatus="state">
            <div class="col-sm-12" style="width:95%;height:auto;border:1px solid #e5e5e5;margin-left:50px;<c:if test="${listStatus.count==1&&state.count==1}"> margin-top:49px;</c:if><c:if test="${listStatus.count>1||(listStatus.count==1&&state.count>1)}"> margin-top:98px;</c:if>">
                    <%--<div style="width:100%;">--%>
                <div style="font-size:14px;font-weight:bold;color:blue;text-align:center;">
                    物流配送单（${state.count}/${item.odPage}）
                    <span style="float:right;font-weight:normal;">购买日期：${item.a.createDate }</span>
                </div>
                <div style="color:blue;font-size:14px;text-align:left;">
                    <span style="display:inline-block;width:33%;">收货单位：${item.a.user.name }</span>
                    <span style="display:inline-block;width:34%;">电话：${item.a.user.mobile }</span>
                    <span style="display:inline-block;width:29%;">客户编码：${item.a.user.id }</span>
                </div>
                <div style="color:blue;font-size:14px;margin-top:5px;text-align:left;">
                    <span style="display:inline-block;width:50%;">地址：${item.a.address }</span>
                    <span style="display:inline-block;width:49%;">单据编号：${item.a.orderNo }</span>
                </div>
                <table border="1" cellspacing="0" cellpadding="0" style="width:100%;font-size:14px;color:blue;">
                    <tr>
                        <td align="center">货品编码</td>
                        <td align="center">货品名称</td>
                        <td align="center">规格</td>
                        <td align="center">数量</td>
                        <td align="center">单位</td>
                        <td align="center">单价</td>
                        <td align="center">货款</td>
                    </tr>
                    <c:forEach items="${item.odList}" begin="${pageInfo.begin}" end="${pageInfo.end}" var="d">
                        <tr>
                            <td>${d.goods.goodsNo }</td>
                            <td>${d.goods.name }</td>
                            <td>${d.sku.parmasValuesJson }</td>
                            <td  align="center">${d.num }</td>
                            <td>${d.sku.unit }</td>
                            <td align="right">${d.price }</td>
                            <td align="right">${d.money }</td>
                        </tr>
                    </c:forEach>
                    <c:if test="${state.count==item.odPage && item.odPage*5-fn:length(item.odList)-1>0}">
                        <c:forEach var="i" begin="${fn:length(item.odList)+1}" end="${item.odPage*5}" step="1">
                            <tr>
                                <td>&ensp; </td>
                                <td>&ensp; </td>
                                <td>&ensp; </td>
                                <td>&ensp; </td>
                                <td>&ensp; </td>
                                <td>&ensp; </td>
                                <td>&ensp; </td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    <tr>
                        <td align="center">优惠金额</td>
                        <td align="center" colspan="2">${pageInfo.couponMoney}</td>
                        <td align="center">${pageInfo.num}</td>
                        <td align="center" colspan="2"></td>
                        <td align="center">${pageInfo.money}</td>
                    </tr>
                    <tr>
                        <td align="center">备注</td>
                        <td align="center" colspan="2">${item.a.bak }${item.gift}</td>
                        <td align="center">支付状态：</td>
                        <td align="center">未支付</td>
                        <td align="center">应收额</td>
                        <td align="center">${item.a.money }</td>
                    </tr>
                </table>
                <div style="color:blue;font-size:14px;text-align:left;">
                    <span style="display:inline-block;width:33%;">司机：${item.a.driver.name }</span>
                    <span style="display:inline-block;width:33%;">司机电话：${item.a.driver.mobile }</span>
                    <span style="display:inline-block;width:30%;">客服电话：0535 408 0200</span>
                </div>
                    <%--</div>--%>
            </div>
        </c:forEach>
        <%--</div>--%>
    </c:if>
</c:forEach>
<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>

<script src="${pageContext.request.contextPath}/admin/static/js/ts.js?v=1.0.0"></script>

<script>
    function myPrint(obj){
        var newWindow=window.open("打印窗口","_blank");//打印窗口要换成页面的url
        var docStr = obj.innerHTML;
        newWindow.document.write(docStr);
        newWindow.document.close();
        newWindow.print();
        newWindow.close();
    }

    var idTmr;
    function  getExplorer() {
        var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
        var isOpera = userAgent.indexOf("Opera") > -1; //判断是否Opera浏览器
        var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera; //判断是否IE浏览器
        var isEdge = userAgent.indexOf("Windows NT 6.1; Trident/7.0;") > -1 && !isIE; //判断是否IE的Edge浏览器
        var isFF = userAgent.indexOf("Firefox") > -1; //判断是否Firefox浏览器
        var isSafari = userAgent.indexOf("Safari") > -1 && userAgent.indexOf("Chrome") == -1; //判断是否Safari浏览器
        var isChrome = userAgent.indexOf("Chrome") > -1 && userAgent.indexOf("Safari") > -1; //判断Chrome浏览器

        //ie
        if (isIE || !!window.ActiveXObject || "ActiveXObject" in window) {
            return 'ie';
        }
        //firefox
        else if (isFF) {
            return 'Firefox';
        }
        //Chrome
        else if(isChrome){
            return 'Chrome';
        }
        //Opera
        else if(isOpera){
            return 'Opera';
        }
        //Safari
        else if(isSafari){
            return 'Safari';
        }
    }
    function export_excel(tableid) {
        if(getExplorer()=='ie'){
            var curTbl = document.getElementById(tableid);
            var oXL;
            try {
                oXL = new ActiveXObject("Excel.Application"); //创建AX对象excel
            } catch (e) {
                alert("无法启动Excel!\n\n如果您确信您的电脑中已经安装了Excel，" + "那么请调整IE的安全级别。\n\n具体操作：\n\n" + "工具 → Internet选项 → 安全 → 自定义级别 → 对没有标记为安全的ActiveX进行初始化和脚本运行 → 启用");
                return false;
            }

            var oWB = oXL.Workbooks.Add();
            var oSheet = oWB.ActiveSheet;
            var Lenr = curTbl.rows.length;
            for (i = 0; i < Lenr; i++) {
                var Lenc = curTbl.rows(i).cells.length;
                for (j = 0; j < Lenc; j++) {
                    oSheet.Cells(i + 1, j + 1).value = curTbl.rows(i).cells(j).innerText;
                }
            }
            oXL.Visible = true;

            //导出剪切板上的数据为excel
//        var curTbl = document.getElementById(tableid);
//        var oXL = new ActiveXObject("Excel.Application"); //创建AX对象excel
//        var oWB = oXL.Workbooks.Add();
//        var xlsheet = oWB.Worksheets(1);
//        var sel = document.body.createTextRange();
//        sel.moveToElementText(curTbl);
//        sel.collapse(true);
//        sel.select();
//        sel.execCommand("Copy");
//        xlsheet.Paste();
//        oXL.Visible = true;
//
//        try {
//            var fname = oXL.Application.GetSaveAsFilename("Excel.xls", "Excel Spreadsheets (*.xls), *.xls");
//        } catch (e) {
//            console.log("Nested catch caught " + e);
//        } finally {
//            oWB.SaveAs(fname);
//            oWB.Close(savechanges = false);
//            oXL.Quit();
//            oXL = null;
//            idTmr = window.setInterval("Cleanup();", 1);
//        }
        }else{
            tableToExcel(tableid)
        }
    }
    function Cleanup() {
        window.clearInterval(idTmr);
        CollectGarbage();
    }

    var tableToExcel = (function() {
        var uri = 'data:application/vnd.ms-excel;base64,',
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            },
            // 下面这段函数作用是：将template中的变量替换为页面内容ctx获取到的值
            format = function(s, c) {
                return s.replace(/{(\w+)}/g,
                    function(m, p) {
                        return c[p];
                    }
                )
            };
        return function(table, name) {
            if (!table.nodeType) {
                table = document.getElementById(table)
            }
            // 获取表单的名字和表单查询的内容
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML};
            // format()函数：通过格式操作使任意类型的数据转换成一个字符串
            // base64()：进行编码
            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = "data.xls";
            document.getElementById("dlink").click();

        }
    })()
</script>
</body>
</html>
