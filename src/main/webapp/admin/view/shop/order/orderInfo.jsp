<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>订单详情</title>
    <link rel="shortcut icon" href="favicon.ico"> 
   	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
     <link href="${pageContext.request.contextPath}/admin/static/css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/layui/css/layui.css?v=4.1.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/app.css?v=4.1.0" rel="stylesheet"> 
    <style type="text/css">
    .layui-upload-img {
	    width: 92px;
	    height: 92px;
	    margin: 0 10px 10px 0;
	} 
    </style>
</head> 

<body class="gray-bg"> 
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
    	<div class="col-sm-12">  
    		<div class="ibox float-e-margins" style="height:100%;">
    			<div class="ibox-title">
                    <h5>订单信息 </h5>
                </div>
                <div class="ibox-content"> 
                	<form class="form-horizontal">
                		<div class="form-group"> 
							<div class="col-sm-3">  
								<p class="text-left " style="display:inline-block;">订单状态：
								<label class="label-primary">
								<c:choose>
									<c:when test="${a.orderState == '0'}">待付款</c:when>
									<c:when test="${a.orderState == '1'}">待发货</c:when>
									<c:when test="${a.orderState == '2'}">已发货，待收货</c:when>
									<c:when test="${a.orderState == '3'}">已收货，待评价</c:when>
									<c:when test="${a.orderState == '4'}">已完成</c:when>
									<c:otherwise>已取消</c:otherwise> 
								</c:choose>
								</label> 
								</p> 
							</div>
							<div class="col-sm-3">  
								是否有售后：${a.isRefund == "1"?"有售后":"无售后" }
							</div> 
					    </div> 
					    <div class="form-group"> 
							<div class="col-sm-3"> 
								订单号码：${a.orderNo }
							</div>
							<div class="col-sm-3"> 
								下单时间：${a.createDate} 
							</div>
							<div class="col-sm-3"> 
								发货时间：${a.sendTime }
							</div> 
							<div class="col-sm-3"> 
								实收金额：${a.money }
							</div>
						</div>
					</form> 
                </div> 
                <div class="ibox-title">
                    <h5>买家信息</h5>
                </div>
                <div class="ibox-content"> 
                	<form class="form-horizontal">
                		<div class="form-group"> 
							<div class="col-sm-3"> 
								头像：<img src="${a.user.headUrl }" style="width:35px;height:35px;border-radius:50%;">
							</div>
							<div class="col-sm-3"> 
								昵称：${a.user.nick} 
							</div>
							<div class="col-sm-3"> 
								店铺名称：${a.user.name }
							</div> 
							<div class="col-sm-3"> 
								店铺地址：${a.user.address }
							</div>
						</div>
						<div class="form-group">
							<table class="table table-bordered">
		                        <thead>
			                        <tr> 
			                        	<th>商品图片</th>
			                            <th>商品</th>
			                            <th>规格</th>
			                            <th>单价（元）</th>
			                            <th>购买数量</th>
			                            <th>发货数量</th>
			                            <th>小计</th>
			                            <th>状态</th>
			                            <th>操作</th>
			                        </tr>
		                        </thead>
		                        <tbody>
		                        	<c:forEach items="${detailList }" var="item">
			                        <tr>  
			                        	<td><img src="${item.goods.imgUrl}" style="width:40px;height:40px;"></td>
			                            <td style="word-break: break-all;max-width:200px;">${item.goods.name }</td>
			                            <td>${item.sku.parmasValuesJson }</td>   
			                            <td>${item.price }</td>
			                            <td>${item.num }</td>
			                            <td>${item.num }</td>
			                            <td>${item.money }</td>
			                            <td>${item.money }</td>
			                            <td>${item.money }</td>
			                        </tr>
			                        </c:forEach>
		                        </tbody>
		                    </table>
						</div>
					</form>
                </div>
                
                <div class="ibox-title">
                    <h5>配送信息</h5>
                </div>
                <div class="ibox-content">
                	<form class="form-horizontal">
                		<div class="form-group"> 
							<div class="col-sm-3"> 
								姓名：${a.name }
							</div>
							<div class="col-sm-3"> 
								联系电话：${a.mobile }
							</div>
							<div class="col-sm-6"> 
								地址：${a.address }
							</div> 
						</div>
					</form> 
                </div> 
                
            </div> 
    	</div> 
    </div>
</div>

<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layui/layui.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/switchery/switchery.js"></script>
<script>   
var sucCheckImg;
layui.use(['layer'], function(){
});  	
    $(document).ready(function () {
    	var lunboBox = document.querySelector('.js-switch_lunbo');
    	new Switchery(lunboBox, {
        	color: "#1AB394", 
        	size: 'small'   
        });
    	lunboBox.onchange = function(){
    		if(lunboBox.checked){
    			$("#shangpin_lunbo_div_box").show();
    		}  
    		else{
    			$("#shangpin_lunbo_div_box").hide();    
    		}
    	}; 
    	
    	//封面图片上传 
    	$("#uploadGoodsZhutu").click(function(){
    		showPicModal(); 
    		sucCheckImg = function(url){
    			$("#goods_imgUrl_div_box").html('<img src="' + url + '">');
    			$("#imgUrl").val(url);
    			hidePicModal();   
    		}   
    	});
  		var pl = parent.layer;
  		var pw = parent.currDialogWindow;
    	$("#btnSave").click(function(){
    		var id = $("#id").val();
    		var name = $("#name").val();
    		var imgUrl = $("#imgUrl").val();
    		var imgList = $("#imgList").val();
    		var money = $("#money").val();
    		var stockNum = $("#stockNum").val();
    		var info = editor.html();
    		if(name == null || name == ""){ 
    			layer.alert("请输入赠品名称");
    			return false;  
    		}
    		if(imgUrl == null || imgUrl == ""){ 
    			layer.alert("请上传赠品图片");
    			return false; 
    		}
    		if(money == null || money == ""){ 
    			layer.alert("请输入赠品价格");
    			return false; 
    		}
    		if(stockNum == null || stockNum == ""){ 
    			layer.alert("请输入库存");
    			return false; 
    		}
    		if(info == null || info == ""){ 
    			layer.alert("请输入赠品详情");
    			return false; 
    		}
    		$.post("${pageContext.request.contextPath}/admin/shop/gift/saveGift",{
    			id : id,
    			name : name, 
    			imgList : imgList,
    			imgUrl : imgUrl,
    			stockNum : stockNum,
    			money : money,
    			info : info 
    		},function(json){
    			if(json.code == 0){  
    				var index = pl.getFrameIndex(window.name); //先得到当前iframe层的索引
    				pl.close(index); //再执行关闭 
    				pl.alert("操作成功");
    				pw.$("#queryBtn").click();
    			}  
    			else{   
    				layer.alert("操作失败");
    			}
    		},"json");
    	});
		$("#btnCancle").click(function(){
			//关闭操作
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭  
		}); 
    	
    });
</script>
<jsp:include page="../../common/picManage.jsp"></jsp:include>
</body>
</html>
