<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>订单管理</title>
    <link rel="shortcut icon" href="favicon.ico">
	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/admin/static/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- Data Tables -->
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row"> 
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
            	<div class="ibox-title">
                    <h5>售后订单</h5>
                </div> 
					<div class="ibox-content">
						<form role="form" id="formDiv" class="form-inline">
							<div class="input-group" style="margin-top:-5px;">
				               <select class="form-control m-b" name="state" id="orderState">
			                                    <option value="">售后状态</option>
			                                    <option value='1'>待审核</option>
			                                    <option value='2'>客服已审核，待团长验货</option>
			                                    <option value='3'>团长已验货</option>
			                                    <option value='4'>售后处理完成</option>
			                                </select>             
				            </div>
				            <div class="input-group date" style="margin-top:-5px;">
	                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                            <input type="text" id="beginTime" class="form-control" value=""  placeholder="开始时间">
	                        </div>
	                        <div class="input-group date" style="margin-top:-5px;">
	                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                            <input type="text" id="endTime" class="form-control" value=""  placeholder="结束时间">
	                        </div>
							<div class="input-group">
								<input type="text" class="form-control" id="orderNo" placeholder="请输入订单号码">
								<span class="input-group-btn"> 
									<button type="button" class="btn btn-primary" id="queryBtn">搜索</button>
								</span>
							</div>      
						</form> 
					</div>
					<div class="ibox-content">  
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="width:100%">
                        <thead>
                        <tr>
                            <th>订单号码</th>
							<th>售后状态</th>
							<th>订单金额</th>
							<th>商品数量</th>
							<th>商品名称</th>
							<th>所属用户</th> 
							<th>所属团长</th>
							<th>提交售后时间</th>
							<th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 
<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
 <script src="${pageContext.request.contextPath}/admin/static/js/plugins/datapicker/bootstrap-datepicker.js"></script>
 
<script> 
	var stateMap = {
	      "1": "待客服审核",
	      "2": "客服已审核，待团长验货",
	      "3": "团长已验货",
	      "4": "售后已完成" 
	    };
	var dataTable;
	var _this = this; 
    $(document).ready(function () {
    	$("#formDiv .input-group.date").datepicker({
	        todayBtn: "linked",
	        keyboardNavigation: !1,
	        forceParse: !1,
	        calendarWeeks: !0,
	        autoclose: !0
	    });
    	var laguage = {
    			"sLengthMenu" : "每页显示 _MENU_ 条记录",
    			"sZeroRecords" : "抱歉， 没有找到",
    			"sInfo" : "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
    			"sInfoEmpty" : "没有数据",
    			"sInfoFiltered" : "(从 _MAX_ 条数据中检索)",
    			"oPaginate" : {
    				"sFirst" : "首页",
    				"sPrevious" : "前一页",
    				"sNext" : "后一页",
    				"sLast" : "尾页"
    			},
    			"sZeroRecords" : "您请求的内容暂时没有数据",
    			"sProcessing" : "数据加载中..."
    		};  
    	var aoColumns = [{
    		"mData" : "orderNo"
    	}, {  
    		"mData" : "salesAfterState",
    		"mRender" : function(data,type,full){
    			return stateMap[data]; 
    		}
    	}, {  
    		"mData" : "money"  
    	}, {  
    		"mData" : "num"  
    	}, {
    		"mData" : "goodsName" 
    	}, {  
    		"mData" : "nick",
    		"mRender" : function(data,type,full){   
    			return '<img src="' + full.headUrl + '" style="width:45px;height:45px;"><br>' + data;
    		}    
    	}, {  
    		"mData" : "team",
    		"mRender" : function(data,type,full){
    			return data + "<br>" + full.address;
    		}
    	}, {  
    		"mData" : "applyTime"  
    	}, { 
    		"mData" : "id", 
    		"mRender" : function(data,type,full){   
    			return '<button onclick="editInfo(' + data + ')" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i>详情</button>'; 
    		}       
    	} ]; 
        dataTable = $('.dataTables-example').dataTable({
        	"dom": 't<"bottom"p>lir<"clear">',
    		"aoColumns" : aoColumns,   
    		"oLaguage" : laguage,
    		"bServerSide" : true,  
    		"bFilter" : false,  
    		"bProcessing" : true, 
    		"bSort" : false, 
    		/* 使用post方式 */ 
    		"iDisplayLength" :10,     
    	     "sAjaxSource": '${pageContext.request.contextPath}/admin/shop/order/loadSalesAfterOrderByPage',   
    	     "fnServerParams": function (aoData) {  //查询条件
    				aoData.push( 
    						{ "name": "orderNo", "value": $("#orderNo").val()},
    						{ "name": "orderState", "value": $("#orderState").val()},
    						{ "name": "beginTime", "value": $("#beginTime").val()},
    						{ "name": "endTime", "value": $("#endTime").val()}
    	            );
    	      },
    	     "fnServerData" : function(sSource, aoData, fnCallback) {  
    	    	 $.ajax({
    					"dataType" : 'json',
    					"type" : "post",
    					"url" : sSource,
    					"data" : aoData,
    					"success" : function(json){
    						$("#iTotalRecords").html(json.iTotalRecords);
    						fnCallback(json);
    					}
    				}); 
    	     }  
    	});
    	//
    	$("#queryBtn").click(function() {
    		dataTable.fnDraw();
    	});
    });   
    function editInfo(data){    
    	parent.openDialog("售后订单详情","${pageContext.request.contextPath}/admin/shop/order/salesAfterInfo?id=" + data,['800px', '550px'],1,_this);
    } 
</script>
</body>
</html>
