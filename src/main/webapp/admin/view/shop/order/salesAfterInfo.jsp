<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>订单详情</title>
    <link rel="shortcut icon" href="favicon.ico"> 
   	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
     <link href="${pageContext.request.contextPath}/admin/static/css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/layui/css/layui.css?v=4.1.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/app.css?v=4.1.0" rel="stylesheet"> 
    <style type="text/css">
    .layui-upload-img {
	    width: 92px;
	    height: 92px;
	    margin: 0 10px 10px 0;
	} 
    </style>
</head> 

<body class="gray-bg"> 
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
    	<div class="col-sm-12">  
    		<div class="ibox float-e-margins" style="height:100%;">
    			<div class="ibox-title">
                    <h5>订单信息 </h5>
                </div>
                <div class="ibox-content"> 
                	<form class="form-horizontal">
                		<div class="form-group"> 
							<div class="col-sm-3">  
								<p class="text-left " style="display:inline-block;">订单状态：
								<label class="label-primary">
								<c:choose>
									<c:when test="${a.orderState == '0'}">待付款</c:when>
									<c:when test="${a.orderState == '1'}">待发货</c:when>
									<c:when test="${a.orderState == '2'}">已发货，团长未收货</c:when>
									<c:when test="${a.orderState == '3'}">团长已收货 待自提</c:when>
									<c:when test="${a.orderState == '4'}">用户已收货</c:when>
									<c:otherwise>已取消</c:otherwise> 
								</c:choose>
								</label> 
								</p>
							</div>
							<div class="col-sm-3">  
								是否有售后：${a.isSalesAfter == "1"?"有售后":"无售后" }
							</div>  
					    </div> 
					    <div class="form-group"> 
							<div class="col-sm-3"> 
								订单号码：${a.orderNo }
							</div>
							<div class="col-sm-3"> 
								下单时间：${a.createDate} 
							</div>
							<div class="col-sm-3"> 
								发货时间：${a.sendTime }
							</div> 
							<div class="col-sm-3"> 
								实收金额：${a.money }
							</div>
						</div>
					</form> 
                </div> 
                <div class="ibox-title">
                    <h5>售后信息 </h5>
                </div>
                <div class="ibox-content"> 
                	<form class="form-horizontal">
                		<div class="form-group"> 
							<div class="col-sm-3">  
								<p class="text-left " style="display:inline-block;">售后状态：
								<label class="label-primary">
								<c:choose>
									<c:when test="${a.salesAfterState == '1'}">等待客服审核</c:when>
									<c:when test="${a.salesAfterState == '2'}">已审核，等待团长验货</c:when>
									<c:when test="${a.salesAfterState == '3'}">团长已验货</c:when>
									<c:when test="${a.salesAfterState == '4'}">售后完成</c:when>
									<c:otherwise>等待客服审核</c:otherwise> 
								</c:choose>
								</label> 
								</p>
							</div>
							<div class="col-sm-3">  
								是否有售后：${a.isSalesAfter == "1"?"有售后":"无售后" }
							</div>  
					    </div> 
					    <div class="form-group"> 
							<div class="col-sm-3"> 
								订单号码：${a.orderNo }
							</div>
							<div class="col-sm-3"> 
								售后提交时间：${a.applySalesAfterTime} 
							</div>
							<div class="col-sm-3"> 
								客服审核时间：${a.checkSalesAfterTime }
							</div> 
							<div class="col-sm-3"> 
								团长验货时间：${a.teamHandleTime }
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">  
								售后结果反馈：${a.salesAfterHandleBak }
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">  
								申请售后原因：${a.salesAfterReason }
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">  
								申请售后说明：${a.salesAfterInfo }
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">  
								用户提交售后图片：<c:set var="string1" value="${a.salesAfterImgList}" />
									<c:set value="${ fn:split(string1, ',') }" var="imgList" />
								<c:forEach items="${imgList }" var="item">
								<img src="${item }" class="salesImg layui-upload-img"> 
								</c:forEach>
							</div>  
						</div>
						<c:if test="${a.salesAfterState == '1'}">
						<div class="form-group">  
							<div class="col-sm-9 col-sm-offset-3">  
								<button class="btn btn-primary " type="button" id="checkOk">
									<i class="fa fa-check"></i>&nbsp;审核通过</button>
                                <button class="btn btn-warning" type="button" id="checkNotOk">审核不通过</button>
							</div> 
						</div>
						</c:if>
						<c:if test="${a.salesAfterState == '3'}">
						<div class="form-group"> 
							<div class="col-sm-3"> 
								售后处理结果
							</div>  
							<div class="col-sm-9">  
								<input type="text" id="salesAfterHandleBak" value=""
					            	 class="form-control" placeholder="请输入品牌名称">
							</div> 
						</div>
						<div class="form-group"> 
							<div class="col-sm-9 col-sm-offset-3">  
								<button class="btn btn-primary " type="button" id="completeShouhou">
									<i class="fa fa-check"></i>&nbsp;完成售后</button>
							</div> 
						</div>
						</c:if>
						<div class="form-group">  
							<div class="col-sm-9 col-sm-offset-3">  
								<button class="btn btn-warning" style="margin-left:20px;" type="button" id="btnRefound">
									<i class="fa fa-money"></i>&nbsp;退款</button>
							</div>
						</div>
						
					</form> 
                </div> 
                <div class="ibox-title">
                    <h5>买家信息</h5>
                </div>
                <div class="ibox-content"> 
                	<form class="form-horizontal">
                		<div class="form-group"> 
							<div class="col-sm-3"> 
								头像：<img src="${a.user.headUrl }" style="width:35px;height:35px;border-radius:50%;">
							</div>
							<div class="col-sm-3"> 
								昵称：${a.user.nick} 
							</div>
							<div class="col-sm-3"> 
								消费金额：${a.user.consumption }
							</div> 
							<div class="col-sm-3"> 
								订单数：${a.user.orderCount }
							</div>
						</div>
						<div class="form-group">
							<table class="table table-bordered">
		                        <thead>
			                        <tr> 
			                        	<th>商品图片</th>
			                            <th>商品</th>
			                            <th>规格</th>
			                            <th>单价（元）</th>
			                            <th>购买数量</th>
			                            <th>发货数量</th>
			                            <th>小计</th>
			                            <th>状态</th>
			                            <th>操作</th>
			                        </tr>
		                        </thead>
		                        <tbody>
		                        	<c:forEach items="${detailList }" var="item">
			                        <tr>  
			                        	<td><img src="${item.goods.imgUrl}" style="width:40px;height:40px;"></td>
			                            <td style="word-break: break-all;max-width:200px;">${item.goods.name }</td>
			                            <td>${item.sku.parmasValuesJson }</td>   
			                            <td>${item.price }</td>
			                            <td>${item.num }</td>
			                            <td>${item.num }</td>
			                            <td>${item.money }</td>
			                            <td>${item.isRefund=='1'?"有退款":"无退款" }</td>
			                            <td> 
			                            	<!-- 只能处理团长验货后的订单  -->
			                            	<c:if test="${a.salesAfterState == '3' or a.salesAfterState == '4' }">
			                            	<c:if test="${item.isRefund == '1' }">
			                            		已退款${item.refoundMoney }
			                            	</c:if> 
			                            	<c:if test="${item.isRefund == '0' or empty item.isRefund }">   
			                            		<button itemId="${item.id }" class="btn btn-warning btnRefund btn-xs" type="button" >
													<i class="fa fa-money"></i>&nbsp;退款</button> 
			                            	</c:if> 
			                            	</c:if>
			                            </td>
			                        </tr>
			                        </c:forEach>
		                        </tbody>
		                    </table>
						</div>
					</form>
                </div>
                
                <div class="ibox-title">
                    <h5>团长信息</h5>
                </div>
                <div class="ibox-content">
                	<form class="form-horizontal">
                		<div class="form-group"> 
							<div class="col-sm-3"> 
								姓名：${a.team.name }
							</div>
							<div class="col-sm-3"> 
								联系电话：${a.team.mobile }
							</div>
							<div class="col-sm-6"> 
								地址：${a.team.address }
							</div> 
						</div>
					</form> 
                </div> 
                
            </div> 
    	</div> 
    </div>
</div>

<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layui/layui.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/switchery/switchery.js"></script>
<script>   
var sucCheckImg;
layui.use(['layer'], function(){
	var layer = layui.layer; 
});  	
    $(document).ready(function () {
    	$(".salesImg").click(function(){
    		var imgList = "${a.salesAfterImgList}";
    		var arr = imgList.split(",");
    		var len = arr.length;
    		var data = [];
    		for(var i = 0; i < len; i++){
    			var item = arr[i];
    			data.push({
    				 
    				      "alt": "图片名",
    				      "pid": 666, //图片id
    				      "src": item, //原图地址
    				      "thumb": "" //缩略图地址
    				     
    			})
    		}
    		var json = { 
    				  "title": "用户图片", //相册标题
    				  "id": 123, //相册id
    				  "start": 0, //初始显示的图片序号，默认0
    				  "data": data
    				}
    		layer.photos({
    		    photos: json
    		    ,anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
    		 });
    	}); 
    	//封面图片上传 
    	$("#uploadGoodsZhutu").click(function(){
    		showPicModal(); 
    		sucCheckImg = function(url){
    			$("#goods_imgUrl_div_box").html('<img src="' + url + '">');
    			$("#imgUrl").val(url);
    			hidePicModal();   
    		}   
    	});
  		var pl = parent.layer;
  		var pw = parent.currDialogWindow;
  		$("#checkOk").click(function(){
  			parent.flyConfirm('确认审核通过该售后申请吗？',function(index){    
        		$.post("${pageContext.request.contextPath}/admin/shop/order/saveSalesAfterOrderCheck?state=1&id=${a.id}",function(json){
        			if(json.code == 0){ 
        				parent.flyMsg("操作成功"); 
        				pw.$("#queryBtn").click();
        				location.reload();
        			} 
        			else{ 
        				parent.flyMsg("操作失败");
        			} 
        		},"json"); 
        	});  
  		});
  		$("#checkNotOk").click(function(){
  			parent.flyConfirm('确认审核不通过该售后申请吗？',function(index){    
        		$.post("${pageContext.request.contextPath}/admin/shop/order/saveSalesAfterOrderCheck?state=0&id=${a.id}",function(json){
        			if(json.code == 0){ 
        				parent.flyMsg("操作成功");  
        				pw.$("#queryBtn").click();
        				location.reload();
        			} 
        			else{ 
        				parent.flyMsg("操作失败");
        			} 
        		},"json"); 
        	});  
  		});
  		$("#completeShouhou").click(function(){
  			parent.flyConfirm('确认已经完成了该订单的售后申请了吗？',function(index){   
  				var result = $("#salesAfterHandleBak").val(); 
        		$.post("${pageContext.request.contextPath}/admin/shop/order/saveSalesAfterOrderComplete",{
        			result : result,
        			id : "${a.id}"
        		},function(json){
        			if(json.code == 0){ 
        				parent.flyMsg("操作成功");  
        				pw.$("#queryBtn").click();
        				location.reload();
        			} 
        			else{ 
        				parent.flyMsg("操作失败");
        			} 
        		},"json"); 
        	});  
  		});
  		$("#btnRefound").click(function(){
  			layer.confirm('确认要退款该订单吗？',function(index){    
  				layer.close(index);  
  				layer.prompt({
  				  formType: 0,
  				  value: '0',
  				  title: '请输入退款金额，只能输入数字'
  				},function(value, index, elem){
  					$.post("${pageContext.request.contextPath}/admin/shop/order/saveOrderRefund?id=${a.id}&refoundMoney=" + value,function(json){
  	        			if(json.code == 0){ 
  	        				parent.flyMsg("操作成功");  
  	        				pw.$("#queryBtn").click();
  	        				location.reload();
  	        			} 
  	        			else{ 
  	        				parent.flyMsg("操作失败");
  	        			} 
  	        		},"json"); 
  				});
        	});    
  		}); 
  		$(".btnRefound").click(function(){
  			layer.confirm('确认要退款该子订单吗？',function(index){    
  				layer.prompt({
  				  formType: 0,
  				  value: '0',
  				  title: '请输入退款金额，只能输入数字'
  				},function(value, index, elem){
  					$.post("${pageContext.request.contextPath}/admin/shop/order/saveOrderSalesRefund?id=${a.id}&refoundMoney=" + value,function(json){
  	        			if(json.code == 0){ 
  	        				parent.flyMsg("操作成功");  
  	        				pw.$("#queryBtn").click();
  	        				location.reload();
  	        			} 
  	        			else{ 
  	        				parent.flyMsg("操作失败");
  	        			} 
  	        		},"json"); 
  				});
        	});  
  		});
		$("#btnCancle").click(function(){
			//关闭操作
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭  
		}); 
    	
    });
</script>
<jsp:include page="../../common/picManage.jsp"></jsp:include>
</body>
</html>
