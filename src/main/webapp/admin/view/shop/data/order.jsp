<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>客户统计</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/datapicker/datepicker3.css"
          rel="stylesheet">

    <!-- Data Tables -->
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/dataTables/dataTables.bootstrap.css"
          rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>订单统计
                    </h5>
                </div>
                <div class="ibox-content">
                    <form role="form" id="formDiv" class="form-inline">
                        <div class="input-group date" style="margin-top:-5px;">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" id="beginTime" class="form-control" value="${beginTime }"
                                   placeholder="起始时间">
                        </div>
                        <div class="input-group date" style="margin-top:-5px;">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" id="endTime" class="form-control" value="${endTime }" placeholder="结束时间">
                        </div>
                        <div class="input-group">
								<span class="input-group-btn"> 
									<button type="button" class="btn btn-primary" id="queryBtn">统计</button>
								</span>
                        </div>
                    </form>
                    <div class="pull-right">
                        <button type="button"
                                onclick="export_excel()"
                                class="btn btn-w-m btn-success" id="exportBtn">
                            <i class="fa fa-plus"></i>&nbsp;&nbsp;导出到excel
                        </button>
                        <a id="dlink" style="display: none;"></a>

                    </div>
                    <table class="table table-hover" id="table2excel">
                        <thead>
                        <tr>
                            <th>制单日期</th>
                            <th>店铺名称</th>
                            <th>优惠金额</th>
                            <th>付款方式</th>
                            <th>收货地址</th>
                            <th>订单备注</th>
                            <th>商品数量</th>
                            <th>金额</th>

                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${mapList }" var="item">
                            <tr>
                                <td>${item.date }</td>
                                <td>${item.userName }</td>
                                <td>0</td>
                                <td>货到付款</td>
                                <td>${item.address }</td>
                                <td>${item.bak }</td>
                                <td>${item.num }</td>
                                <td>${item.money }</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<script src="${pageContext.request.contextPath}/admin/static/js/ts.js?v=1.0.0"></script>

<script>
    $(function () {
        $("#formDiv .input-group.date").datepicker({
            todayBtn: "linked",
            keyboardNavigation: !1,
            forceParse: !1,
            calendarWeeks: !0,
            autoclose: !0
        });
        $("#queryBtn").click(function () {
            var beginTime = $("#beginTime").val();
            var endTime = $("#endTime").val();
            location.href = "${pageContext.request.contextPath}/admin/shop/data/order?beginTime=" + beginTime + "&endTime=" + endTime;
        })
    });
    var idTmr;

    function export_excel() {
        var b = $("#beginTime").val();
        var e = $("#endTime").val();
        window.open('/admin/shop/data/order/export?beginTime=' + b + '&endTime=' + e);
    }

</script>
</body>
</html>
