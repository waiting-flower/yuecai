<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>业务员统计</title> 
    <link rel="shortcut icon" href="favicon.ico">
	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/admin/static/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
	
    <!-- Data Tables -->
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row"> 
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
            	<div class="ibox-title">
                    <h5>业务员销售统计
                    </h5> 
                </div> 
					<div class="ibox-content">
						<form role="form" id="formDiv" class="form-inline">
							<div class="input-group date" style="margin-top:-5px;">
	                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                            <input type="text" id="beginTime" class="form-control" value="${beginTime }"  placeholder="起始时间">
	                        </div>
	                        <div class="input-group date" style="margin-top:-5px;">
	                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                            <input type="text" id="endTime" class="form-control" value="${endTime }"  placeholder="结束时间">
	                        </div>
							<div class="input-group"> 
								<span class="input-group-btn"> 
									<button type="button" class="btn btn-primary" id="queryBtn">统计</button>
								</span>
							</div>      
						</form>   
						<div class="pull-right">
							<button type="button"   
							onclick="export_excel('table2excel')"
							 class="btn btn-w-m btn-success" id="exportBtn"> 
									<i class="fa fa-plus"></i>&nbsp;&nbsp;导出到excel 
							</button>
							<a id="dlink" style="display: none;"></a>
							
						</div>    
                    <table class="table table-hover" id="table2excel">
                        <thead>
	                        <tr>
	                            <th>业务员编号</th>
	                            <th>业务员名称</th>
	                            <th>业务员手机号码</th>
	                            <th>总金额</th>
	                            <th>减去优惠券后的金额</th>
	                            <th>订单数</th>
	                            <th>交易用户</th>
	                            <th>客单价</th>
	                            <th>客单价（减去优惠金额）</th>
	                        </tr>
                        </thead> 
                        <tbody>
                        <c:forEach items="${mapList }" var="item">
	                        <tr>  
	                            <td>${item.userId }</td>
	                            <td>${item.mobile }</td>
	                            <td>${item.mobile }</td>
	                            <td>${item.allMoney }</td>
	                            <td>${item.money }</td>
	                            <td>${item.allNum }</td>
	                            <td>${item.userCount }</td>
	                            <td>${item.kedanjia }</td>
	                            <td>${item.kedanjiaCoupon }</td>
	                            <td></td> 
	                            <td></td>
	                        </tr>    
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 
<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
   <script src="${pageContext.request.contextPath}/admin/static/js/plugins/datapicker/bootstrap-datepicker.js"></script>
 
<script src="${pageContext.request.contextPath}/admin/static/js/ts.js?v=1.0.0"></script>
 
<script>
$(function(){
	$("#formDiv .input-group.date").datepicker({
        todayBtn: "linked",
        keyboardNavigation: !1,
        forceParse: !1,
        calendarWeeks: !0,
        autoclose: !0
    }); 
	$("#queryBtn").click(function(){   
		var beginTime = $("#beginTime").val();
		var endTime = $("#endTime").val();
		location.href = "${pageContext.request.contextPath}/admin/shop/data/yewuyuan?beginTime=" + beginTime + "&endTime=" + endTime;
	})
});

var idTmr;
function  getExplorer() {
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
    var isOpera = userAgent.indexOf("Opera") > -1; //判断是否Opera浏览器
    var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera; //判断是否IE浏览器
    var isEdge = userAgent.indexOf("Windows NT 6.1; Trident/7.0;") > -1 && !isIE; //判断是否IE的Edge浏览器
    var isFF = userAgent.indexOf("Firefox") > -1; //判断是否Firefox浏览器
    var isSafari = userAgent.indexOf("Safari") > -1 && userAgent.indexOf("Chrome") == -1; //判断是否Safari浏览器
    var isChrome = userAgent.indexOf("Chrome") > -1 && userAgent.indexOf("Safari") > -1; //判断Chrome浏览器

    //ie
    if (isIE || !!window.ActiveXObject || "ActiveXObject" in window) {
        return 'ie';
    }
    //firefox
    else if (isFF) {
        return 'Firefox';
    }
    //Chrome
    else if(isChrome){
        return 'Chrome';
    }
    //Opera
    else if(isOpera){
        return 'Opera';
    }
    //Safari
    else if(isSafari){
        return 'Safari';
    }
}
function export_excel(tableid) {
    if(getExplorer()=='ie'){
        var curTbl = document.getElementById(tableid);
        var oXL;
        try {
            oXL = new ActiveXObject("Excel.Application"); //创建AX对象excel
        } catch (e) {
            alert("无法启动Excel!\n\n如果您确信您的电脑中已经安装了Excel，" + "那么请调整IE的安全级别。\n\n具体操作：\n\n" + "工具 → Internet选项 → 安全 → 自定义级别 → 对没有标记为安全的ActiveX进行初始化和脚本运行 → 启用");
            return false;
        }

        var oWB = oXL.Workbooks.Add();
        var oSheet = oWB.ActiveSheet;
        var Lenr = curTbl.rows.length;
        for (i = 0; i < Lenr; i++) {
            var Lenc = curTbl.rows(i).cells.length;
            for (j = 0; j < Lenc; j++) {
                oSheet.Cells(i + 1, j + 1).value = curTbl.rows(i).cells(j).innerText;
            }
        }
        oXL.Visible = true;

        //导出剪切板上的数据为excel
//        var curTbl = document.getElementById(tableid);
//        var oXL = new ActiveXObject("Excel.Application"); //创建AX对象excel
//        var oWB = oXL.Workbooks.Add();
//        var xlsheet = oWB.Worksheets(1);
//        var sel = document.body.createTextRange();
//        sel.moveToElementText(curTbl);
//        sel.collapse(true);
//        sel.select();
//        sel.execCommand("Copy");
//        xlsheet.Paste();
//        oXL.Visible = true;
//
//        try {
//            var fname = oXL.Application.GetSaveAsFilename("Excel.xls", "Excel Spreadsheets (*.xls), *.xls");
//        } catch (e) {
//            console.log("Nested catch caught " + e);
//        } finally {
//            oWB.SaveAs(fname);
//            oWB.Close(savechanges = false);
//            oXL.Quit();
//            oXL = null;
//            idTmr = window.setInterval("Cleanup();", 1);
//        }
    }else{
        tableToExcel(tableid)
    }
}
function Cleanup() {
    window.clearInterval(idTmr);
    CollectGarbage();
} 

var tableToExcel = (function() {
    var uri = 'data:application/vnd.ms-excel;base64,',
    template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
    base64 = function(s) {
        return window.btoa(unescape(encodeURIComponent(s)))
    },
    // 下面这段函数作用是：将template中的变量替换为页面内容ctx获取到的值
    format = function(s, c) {
        return s.replace(/{(\w+)}/g,
            function(m, p) {
                return c[p];
            }
        )
    };
    return function(table, name) {
        if (!table.nodeType) {
            table = document.getElementById(table)
        }
        // 获取表单的名字和表单查询的内容
        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML};
        // format()函数：通过格式操作使任意类型的数据转换成一个字符串
        // base64()：进行编码
        document.getElementById("dlink").href = uri + base64(format(template, ctx));
        document.getElementById("dlink").download = "data.xls";
        document.getElementById("dlink").click();  

    }
})()
</script>
</body>
</html>
