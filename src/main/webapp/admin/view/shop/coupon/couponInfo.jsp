<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>商城优惠券</title>
    <link rel="shortcut icon" href="favicon.ico"> 
   	<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/layui/css/layui.css?v=4.1.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/app.css?v=4.1.0" rel="stylesheet"> 
    <style type="text/css">
    .layui-upload-img {
	    width: 92px;
	    height: 92px;
	    margin: 0 10px 10px 0;
	} 
    </style>
</head> 

<body class="gray-bg"> 
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
    	<div class="col-sm-12">  
    		<div class="ibox float-e-margins" style="height:100%;">
                <div class="ibox-content"> 
                	<form class="form-horizontal">
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">优惠券名称：</label>
					        <div class="col-sm-6"> 
					        	<input type="hidden" value="${a.id }" id="id">  
					            <input type="text" id="name" value="${a.name}"
					            	 class="form-control" placeholder="请输入优惠券名称">
					        </div>
					    </div> 
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">优惠券说明：</label>
					        <div class="col-sm-6">  
					            <input type="text" id="tips" value="${a.tips}"
					            	 class="form-control" placeholder="请输入优惠券说明">
					        </div>
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">优惠券数量：</label>
					        <div class="col-sm-3">  
					            <input type="text" id="allNum" value="${a.allNum}"
					            	 class="form-control" placeholder="请输入优惠券数量">
					        </div>
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">优惠券金额：</label>
					        <div class="col-sm-3">  
					            <input type="text" id="money" value="${a.money}"
					            	 class="form-control" placeholder="请输入优惠券金额">
					        </div>
					    </div>
					    <div class="form-group"> 
					        <label class="col-sm-3 control-label">使用条件金额：</label>
					        <div class="col-sm-3">  
					            <input type="text" id="conditionMoney" value="${a.conditionMoney}"
					            	 class="form-control" placeholder="请输入优惠券使用起始金额">
					        </div>
					    </div>
					    
					    <div class="form-group">
					        <label class="col-sm-3 control-label">开始时间：</label>
					        <div class="col-sm-3">
					            <input type="text" id="beginTime" value="${a.beginTime }"
					            	 class="form-control" placeholder="请输入开始时间">
					        </div> 
					    </div> 
					    
					    <div class="form-group">
					        <label class="col-sm-3 control-label">结束时间：</label>
					        <div class="col-sm-3">
					            <input type="text" id="endTime" value="${a.endTime }"
					            	 class="form-control" placeholder="请输入结束时间">
					        </div> 
					    </div>  
					    <div class="form-group draggable ui-draggable">
                            <div class="col-sm-12 col-sm-offset-3"> 
                                <button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;提交</button>
                                <button class="btn btn-white" type="button" id="btnCancle">取消</button>
                            </div>
                        </div>
					</form> 
                </div> 
            </div> 
    	</div> 
    </div>
</div>

<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layui/layui.js"></script>
<script>   
var sucCheckImg;
layui.use(['layer','laydate'], function(){
	var laydate = layui.laydate; 
	var layer = layui.layer;
	laydate.render({ 
		elem: '#beginTime'
		,type : 'datetime'
		,format: 'yyyy-MM-dd HH:mm:ss'
	});
	laydate.render({ 
		elem: '#endTime' 
		,type : 'datetime'
		,format: 'yyyy-MM-dd HH:mm:ss' 
	});
});  	
    $(document).ready(function () {
    	//轮播图片上传 
    	$("#uploadGoodsZhutu").click(function(){
    		showPicModal(); 
    		sucCheckImg = function(url){
    			$("#goods_imgUrl_div_box").html('<img src="' + url + '">');
    			$("#imgUrl").val(url);
    			hidePicModal();   
    		}   
    	});
  		var pl = parent.layer;
  		var pw = parent.currDialogWindow;
    	$("#btnSave").click(function(){
    		var id = $("#id").val();
    		var name = $("#name").val();
    		var tips = $("#tips").val();
    		var allNum = $("#allNum").val();
    		var beginTime = $("#beginTime").val();
    		var endTime = $("#endTime").val();
    		var conditionMoney = $("#conditionMoney").val();
    		var money = $("#money").val();
    		if(name == null || name == ""){ 
    			layer.alert("请输入优惠券名称");
    			return false; 
    		}
    		if(tips == null || tips == ""){ 
    			layer.alert("请输入使用说明");
    			return false; 
    		}
    		if(money == null || money == ""){
    			layer.alert("请输入优惠券金额");
    			return false; 
    		}
    		if(conditionMoney == null || conditionMoney == ""){
    			conditionMoney = 0;
    		}
    		if(allNum == null || allNum == ""){
    			layer.alert("请输入优惠券数量");
    			return false;
    		}
    		$.post("${pageContext.request.contextPath}/admin/shop/coupon/saveCoupon",{
    			id : id,
    			name : name, 
    			tips : tips,
    			allNum : allNum,
    			beginTime : beginTime,
    			endTime : endTime,
    			money : money,
    			conditionMoney : conditionMoney
    		},function(json){
    			if(json.code == 0){  
    				var index = pl.getFrameIndex(window.name); //先得到当前iframe层的索引
    				pl.close(index); //再执行关闭 
    				pl.alert("操作成功");
    				pw.$("#queryBtn").click();
    			}  
    			else{   
    				layer.alert("操作失败");
    			}
    		},"json");
    	});
		$("#btnCancle").click(function(){
			//关闭操作
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭  
		}); 
    	
    });
</script>
<jsp:include page="../../common/picManage.jsp"></jsp:include>
</body>
</html>
