<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">


<!-- Mirrored from www.zi-han.net/theme/hplus/login_v2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:49 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>用户注册</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/animate.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/login.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/app.css" rel="stylesheet">
    <!--[if lt IE 9]>  
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <script> 
        if(window.top!==window.self){window.top.location=window.location};
    </script>
    <style type="text/css">
    body.signin{
    	color:#555; 
    } 
    </style>

</head>

<body class="signin">
    <div class="signinpanel">
        <div class="row">
            <div class="col-sm-7"> 
                <div class="signin-info" style="color:#fff;">
                    <div class="logopanel m-b">
                        <h1>商城</h1>
                    </div> 
                    <div class="m-b"></div>
                    <h4>欢迎使用 <strong>某某 后台管理系统</strong></h4>
                    <ul class="m-b">
                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 优势一</li>
                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 优势二</li>
                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 优势三</li>
                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 优势四</li>
                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 优势五</li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-5">
                <form class="form-horizontal">
                    <h4 class="no-margins">用户注册：</h4>
                    <p class="m-t-md">注册xx系统后台用户</p> 
                    <div class="input-group flex"> 
                        <input type="text" class="form-control flex1" id="userName" value="" placeholder="用户名">
                        <span class="input-group-addon input_icon">
                    		<span class="fa fa-user"></span> 
                    	</span>    
                    </div>   
                    <div class="input-group flex"> 
                        <input type="text" class="form-control flex1" id="name" value="" placeholder="您的姓名">
                        <span class="input-group-addon input_icon">
                    		<span class="fa fa-info"></span> 
                    	</span>     
                    </div>   
                    <div class="input-group flex"> 
                        <input type="password" class="form-control flex1" id="password" value="" placeholder="密码">
                        <span class="input-group-addon input_icon">
                    		<span class="fa fa-key"></span> 
                    	</span>    
                    </div> 
                    <div class="input-group flex">  
                        <input type="password" class="form-control flex1" id="password2" value="" placeholder="确认密码">
                        <span class="input-group-addon input_icon">
                    		<span class="fa fa-key"></span> 
                    	</span>    
                    </div> 
                   	<div class="input-group flex">  
                        <input type="text" class="form-control flex1" id="email" value="" placeholder="邮箱地址，用于找回密码">
                        <span class="input-group-addon input_icon">
                    		<span class="fa fa-envelope-o"></span> 
                    	</span>    
                    </div> 
                    <div class="form-group m-b">   
                        <div class="col-sm-12">  
                        	<input type="text" style="display:inline-block;width:calc(100% - 80px);"
                        		 id="vercode" required="" placeholder="验证码" class="form-control">
                    		<img    
                    		style="width:70px;height:32px;"
                    		src="${pageContext.request.contextPath}/getRandomPic" id="randpic">
                    	</div>     
                    </div> 	   
                    <button type="button" id="btnReg" class="btn btn-primary btn-block">立即注册</button>
                    <p class="text-muted text-center m-t"> 
		                <small>已经有账户了？</small> 
		                <a href="${pageContext.request.contextPath}/login">点此登录</a>
		            </p> 
                </form>   
            </div>
        </div>  
        <div class="signup-footer">
            <div class="pull-right">
                &copy; 2018~2019 All Rights Reserved. 某某公司
            </div>
        </div>
    </div>
   	<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
    <script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
    <script type="text/javascript">
    	$(document).keyup(function(event){
		  if(event.keyCode ==13){
			  $("#btnLogin").trigger("click");
		  } 
		}); 
	$("#randpic").click(function(){
		//验证码图片点击事件
		//获取当前的时间作为参数，无具体意义     
		var timenow = new Date().getTime(); 
		$(this).attr("src","${pageContext.request.contextPath}/getRandomPic?d=" + timenow);
	});
	$("#btnReg").click(function(){
		var u = $("#userName").val();
		var name = $("#name").val();
		var p = $("#password").val();
		var p2 = $("#password2").val();
		var email = $("#email").val(); 
		var vercode = $("#vercode").val();
		var pattern = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		if(u == "" || u == null || u == '输入账号'){
			layer.msg('用户不能为空');
			return false;
		}
		if(p == "" || p == null || p == "输入密码"){
			layer.msg('密码不能为空');
			return false;
		} 
		if(p2 == "" || p2 == null || p2 == "输入密码"){
			layer.msg('请确认您的密码');
			return false;
		} 
		if(p != p2){
			layer.msg("您两次输入的密码不一致");
			return false;
		}
		if(email == "" || email == null){
			layer.msg('邮箱不能为空');
			return false;
		} 
		if(!pattern.test(email)){
			layer.msg('邮箱格式不正确');
			return false;
		}
		if(vercode == "" || vercode == null || vercode == "验证码" || vercode == '输入验证码'){
			layer.msg('验证码不能为空');
			return false; 
		}    
		$.post("${pageContext.request.contextPath}/admin/user/reg",{
			"userName" : u,
			"password" : p,
			"name" : name,
			"email" : email,
			"vercode" : vercode
		}, function(json){
			if(json.code == "0"){
				location.href = "${pageContext.request.contextPath}/login";
			}
			else if(json.code == "-1"){
				layer.alert('验证码不正确'); 
			}
			else{ 
				layer.alert(json.msg);
			}
		},"json");
	});  
    </script>
</body>
</html>
