<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>系统文件对象</title>
    <link rel="shortcut icon" href="favicon.ico"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min.css?v=4.1.0" rel="stylesheet"> 
    <link href="${pageContext.request.contextPath}/admin/static/js/plugins/layui/css/layui.css?v=4.1.0" rel="stylesheet"> 
    <style type="text/css">
    .layui-upload-img {
	    width: 92px;
	    height: 92px;
	    margin: 0 10px 10px 0;
	} 
    </style>
</head>  
 
<body class="gray-bg"> 
<div class="wrapper wrapper-content animated fadeInUp">
    <div class="row">
    	<div class="col-sm-12">  
    		<div class="ibox float-e-margins" style="height:100%;">
                <div class="ibox-content"> 
                	<form class="form-horizontal">
					    <div class="form-group">  
					        <label class="col-sm-3 control-label">选择所属分类：</label>
					        <div class="col-sm-6">
					            <select class="form-control" id="cate">
									<c:forEach items="${cateList}" var="item">
									<c:if test="${a.cate.id == item.id }">
									<option value="${item.id }" selected>${item.name }</option>
									</c:if> 
									<c:if test="${a.cate.id != item.id }">
									<option value="${item.id }">${item.name }</option>
									</c:if>
									</c:forEach> 
					            </select>
					        </div>
					    </div>
					    <div class="form-group">   
					    	<label class="col-sm-3 control-label">选择图片：</label>
					        <div class="col-sm-9">
								<div class="layui-upload">
									<button type="button" class="layui-btn" id="uploadBtn">选择图片</button>
									<blockquote class="layui-elem-quote layui-quote-nm"
											style="margin-top: 10px;">
										<div class="layui-upload-list" id="yulanDiv"></div>
									</blockquote>
								</div>
								<input type="hidden" value="" id="imgList">
							</div> 
					    </div> 
					    <div class="form-group draggable ui-draggable">
                            <div class="col-sm-12 col-sm-offset-3"> 
                                <button class="btn btn-primary " type="button" id="btnSave"><i class="fa fa-check"></i>&nbsp;上传</button>
                                <button class="btn btn-white" type="button" id="btnCancle">取消</button>
                            </div>
                        </div>
					</form> 
                </div> 
            </div> 
    	</div> 
    </div>
</div>

<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/content.min.js?v=1.0.0"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layui/layui.js"></script>
<script>    
	
    $(document).ready(function () {
    	layui.use(['upload','layer'], function(){
    		  var upload = layui.upload;
    		  var layer = layui.layer;
    		  //多图片上传
    		  upload.render({
    		    elem: '#uploadBtn'
    		    ,url: '${pageContext.request.contextPath}/admin/file/uploadToQiniu'
    		    ,data:{cate:$("#cate").val()}
    		    ,multiple: true
    		    ,auto: false //选择文件后不自动上传
    		    ,bindAction: '#btnSave' //指向一个按钮触发上传
    		   	,choose: function(obj){
    		   		obj.preview(function(index, file, result){
        		        $('#yulanDiv').append('<img src="'+ result +'" alt="'+ file.name +'" class="layui-upload-img">')
        		      }); 
    		   	}
    		    ,before: function(obj){ 
    		      layer.load();
    		    }
    		    ,allDone: function(obj){ //当文件全部被提交后，才触发
    		    	console.log(obj);
    		        console.log(obj.total); //得到总文件数
    		        console.log(obj.successful); //请求成功的文件数
    		        console.log(obj.aborted); //请求失败的文件数
    		        layer.closeAll('loading'); //关闭loading 
    		        layer.msg("上传成功");
    		    }
    		    ,done: function(res){
    		    	console.log(res); 
    		      //上传完毕
    		      
    		    }
    		  }); 
    	});
    	$("#btnSave").click(function(){
    		var id = $("#id").val();
    		var cate = $("#cate").val();
    		if(cate == null || cate == ""){ 
    			layer.alert("请选择文件分类");
    			return false;  
    		}
    		if(1 == 0){  
				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
				parent.layer.close(index); //再执行关闭 
				parent.layer.alert("操作成功");
				parent.currDialogWindow.$("#queryBtn").click();
			}
    	});
		$("#btnCancle").click(function(){
			//关闭操作
			var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭  
		});      
    });
</script>
</body>
</html>
