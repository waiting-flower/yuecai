<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="renderer" content="webkit">

<meta http-equiv="Cache-Control" content="no-siteapp" />
<link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
<link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
<link href="${pageContext.request.contextPath}/admin/static/css/animate.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/admin/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
<link href="${pageContext.request.contextPath}/admin/static/css/app.css?v=4.1.0" rel="stylesheet">
<title>首页样式1</title>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content">
	<!-- 顶部的小部件统计开始 -->
    <div class="row">
    	<div class="col-sm-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>用户</h5>
                    <div class="pull-right">
                        <div class="btn-group">
                            <button type="button" queryType="day"  class="btnUser btn btn-xs btn-white active">天</button>
                            <button type="button" queryType="month" class="btnUser btn btn-xs btn-white">月</button>
                            <button type="button" queryType="year" class="btnUser btn btn-xs btn-white">年</button>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins" id="newUser"></h1>
                    <div class="stat-percent font-bold text-success"  id="newRate"><em></em>% <i class="fa "></i>
                    </div>
                    <small>新增用户</small>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>订单</h5>
                    <div class="pull-right">
                        <div class="btn-group">
                            <button type="button" queryType="day"  class="btnOrder btn btn-xs btn-white active">天</button>
                            <button type="button" queryType="month" class="btnOrder btn btn-xs btn-white">月</button>
                            <button type="button" queryType="year" class="btnOrder btn btn-xs btn-white">年</button>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins" id="newOrder"></h1>
                    <div class="stat-percent font-bold text-info" id="orderRate"><em></em>% <i class="fa"></i>
                    </div>
                    <small>新订单</small>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>司机</h5>
                    <div class="pull-right">
                        <div class="btn-group">
                            <button type="button" queryType="day"  class="btnTeam btn btn-xs btn-white active">天</button>
                            <button type="button" queryType="month" class="btnTeam btn btn-xs btn-white">月</button>
                            <button type="button" queryType="year" class="btnTeam btn btn-xs btn-white">年</button>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins" id="newTeam"></h1>
                    <div class="stat-percent font-bold text-navy" id="teamRate"><em></em>% <i class="fa "></i>
                    </div>
                    <small>新司机</small>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="pull-right">
                        <div class="btn-group">
                            <button type="button" queryType="day"  class="btnAfter btn btn-xs btn-white active">天</button>
                            <button type="button" queryType="month" class="btnAfter btn btn-xs btn-white">月</button>
                            <button type="button" queryType="year" class="btnAfter btn btn-xs btn-white">年</button>
                        </div>
                    </div> 
                    <h5>售后</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins" id="newAfter"></h1>
                    <div class="stat-percent font-bold text-danger" id="afterRate"><em></em>% <i class="fa"></i>
                    </div> 
                    <small>新售后</small>
                </div>
            </div>
        </div>
    </div>
    <!-- 小部件统计结束 -->
    <div class="row">
        <div class="col-sm-9"> 
        	<div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>店铺详情</h5>
                </div>
                <div class="ibox-content">
                	<div class="row">
                		<div class="col-sm-3"> 
				            <div class="widget-head-color-box navy-bg p-sm text-center">
				                <div class="m-b-md">
				                    <h2 class="font-bold no-margins">山东APP</h2>
				                </div>   
				                <img src="http://youji.7working.com/Fi2K_53VMVO-H_kl-zgJp3YQ8vGe" style="width:100%;" class="img-circle circle-border m-b-md" alt="profile">
				                <div>
				                    <span>${teamCount }司机</span> | <span>${userCount }用户</span> | <span>${orderCount} 订单</span>
				                </div>
				            </div> 
				        </div> 
				        <div class="col-sm-9">  
				        	<div class="widget-head-color-box  widget-text-box  p-xl">
				        		<div class="row">
				        			<div class='col-sm-12'>
				        				<p><label>商城名称</label>：山东APP</p>
				        			</div>
				        			<div class='col-sm-4'>
				        				<p><label>注册时间</label>：2019-01-01</p>
				        			</div>  
				        			<div class='col-sm-4'>
				        				<p><label>商品数量</label>：${goodsCount }</p>
				        			</div>
				        			<div class='col-sm-4'>
				        				<p><label>订单总数</label>：${orderCount }</p>
				        			</div> 
				        			<div class='col-sm-4'>
				        				<p><label>成交总额</label>：${money}</p>
				        			</div>
				        			<div class='col-sm-4'>
				        				<p><label>司机数量</label>：${teamCount }</p>
				        			</div>
				        			<div class='col-sm-4'>
				        				<p><label>用户总数</label>：${userCount }</p>
				        			</div>   
				        			<div class="col-sm-4">  
							            <div class="widget style1 navy-bg">
							                <div class="row">  
							                    <div class="col-xs-12 text-right">
							                    	<i class="fa fa-users"></i>
							                        <span> 待确认订单 </span>
							                        <h2 class="font-bold">${newQuerenCount }</h2>
							                    </div>
							                </div> 
							            </div>
							        </div> 
							        <div class="col-sm-4">
							            <div class="widget style1 lazur-bg">
							                <div class="row">
							                    <div class="col-xs-12 text-right"> 
							                    	 <i class="fa fa-shopping-cart"></i>
							                        <span> 待发货订单 </span>
							                        <h2 class="font-bold">${newUserCount }</h2>
							                    </div>
							                </div>
							            </div> 
							        </div> 
							        <div class="col-sm-4"> 
							            <div class="widget style1 blue-bg">
							                <div class="row"> 
							                    <div class="col-xs-12 text-right">
							                    	<i class="fa fa-money"></i>
							                        <span> 售后处理 </span>  
							                        <h2 class="font-bold">${newSalesAfterCount}</h2>
							                    </div> 
							                </div> 
							            </div>
							        </div> 
				        		</div> 
				            </div>
				        </div>
                	</div>
                </div>
            </div>
        </div>
        <div class="col-sm-3"> 
        	<div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>登录记录【最新5条】</h5> 
                </div> 
                <div class="ibox-content"> 
                	<table class="table table-hover">
                        <thead>
                        <tr>
                            <th>时间</th>
                            <th>ip</th>
                        </tr>
                        </thead> 
                        <tbody>
                        <c:forEach items="${loginList }" var="item">
	                        <tr>  
	                            <td>${item.createDate }</td>
	                            <td>${item.ip }</td> 
	                        </tr> 
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
    	<div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>订单</h5>
                    <div class="pull-right">
                        <div class="btn-group">
                             <button type="button" queryType="7"  class="btnBar btn btn-xs btn-white active">7天</button>
                            <button type="button" queryType="30" class="btnBar btn btn-xs btn-white">30天</button>
                            <button type="button" queryType="90" class="btnBar btn btn-xs btn-white">90天</button>
                        </div> 
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="chart_bars_vertical" style="width: 100%;height:500px;"></div> 
                        </div>
                    </div>
                </div>

            </div>
        </div>
    
    </div>
     
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/static/js/echarts.min.js">
</script>
<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
<script type="text/javascript">
	$(function(){
		var getData3 = function(type){ 
			$.getJSON("${pageContext.request.contextPath}/getOrderBarList?type=" + type,function(json){
				console.log(json.dataList); 
				var myChart = echarts.init(document.getElementById('chart_bars_vertical'));
				var option = {
						tooltip: {
					        trigger: 'axis',
					        axisPointer: {
					            animation: false
					        }
					    },
					    xAxis: { 
					        type: 'category',
					        data: json.list
					    },
					    yAxis: {
					        type: 'value'
					    }, 
					    series: [{
					    	name : "成交订单数量",
					        data: json.dataList,
					        type: 'line'
					    }]
					};

				myChart.setOption(option);
			});
		} 
		getData3(7);
		$(".btnBar").click(function(){
			var queryType = $(this).attr("queryType")
			$(".btnBar").removeClass("active");
			$(this).addClass("active"); 
			getData3(queryType);  
		});
		var init = function(){      
			$.getJSON("${pageContext.request.contextPath}/getUserData?quertyType=day",function(json){
				console.log(json); 
				$("#newUser").html(json.nowCount);
				$("#newRate em").html(parseFloat(json.rate).toFixed(2)*100); 
				if(json.orderType == "1"){   
					//升 
					$("#newRate").find(".fa").addClass("fa-level-up");
				}
				else{ 
					$("#newRate").find(".fa").addClass("fa-level-down");
				}
			});
			$(".btnUser").click(function(){
				var queryType = $(this).attr("queryType")
				$(".btnUser").removeClass("active");
				$(this).addClass("active");   
				$.getJSON("${pageContext.request.contextPath}/getUserData?quertyType=" + queryType,function(json){
					console.log(json); 
					$("#newUser").html(json.nowCount);
					$("#newRate em").html(parseFloat(json.rate).toFixed(2)*100); 
					if(json.orderType == "1"){ 
						//升  
						$("#newRate").find("i").addClass("fa-level-up");
					}
					else{   
						$("#newRate").find("i").addClass("fa-level-down");
					}
				});
			});
			
			
			$.getJSON("${pageContext.request.contextPath}/getOrderData?quertyType=day",function(json){
				console.log(json); 
				$("#newOrder").html(json.nowCount);
				$("#orderRate em").html(parseFloat(json.rate).toFixed(2)*100); 
				if(json.orderType == "1"){  
					//升  
					$("#orderRate").find(".fa").addClass("fa-level-up");
				}
				else{ 
					$("#orderRate").find(".fa").addClass("fa-level-down");
				}
			});
			
			$(".btnOrder").click(function(){
				var queryType = $(this).attr("queryType")
				$(".btnOrder").removeClass("active");
				$(this).addClass("active");   
				$.getJSON("${pageContext.request.contextPath}/getOrderData?quertyType=" + queryType,function(json){
					console.log(json); 
					$("#newOrder").html(json.nowCount);
					$("#orderRate em").html(parseFloat(json.rate).toFixed(2)*100); 
					if(json.orderType == "1"){  
						//升  
						$("#orderRate").find(".fa").addClass("fa-level-up");
					}
					else{ 
						$("#orderRate").find(".fa").addClass("fa-level-down");
					} 
				});
			});
			
			$.getJSON("${pageContext.request.contextPath}/getTeamData?quertyType=day",function(json){
				console.log(json); 
				$("#newTeam").html(json.nowCount);
				$("#teamRate em").html(parseFloat(json.rate).toFixed(2)*100); 
				if(json.orderType == "1"){  
					//升  
					$("#teamRate").find(".fa").addClass("fa-level-up");
				}
				else{ 
					$("#teamRate").find(".fa").addClass("fa-level-down");
				} 
			});
			
			$(".btnTeam").click(function(){
				var queryType = $(this).attr("queryType")
				$(".btnTeam").removeClass("active");
				$(this).addClass("active");    
				$.getJSON("${pageContext.request.contextPath}/getTeamData?quertyType=" + queryType,function(json){
					console.log(json); 
					$("#newTeam").html(json.nowCount);
					$("#teamRate em").html(parseFloat(json.rate).toFixed(2)*100);  
					if(json.orderType == "1"){  
						//升  
						$("#teamRate").find(".fa").addClass("fa-level-up");
					}
					else{    
						$("#teamRate").find(".fa").addClass("fa-level-down");
					} 
				});
			});
			
			
			$.getJSON("${pageContext.request.contextPath}/getAfterData?quertyType=day",function(json){
				console.log(json); 
				$("#newAfter").html(json.nowCount);
				$("#afterRate em").html(parseFloat(json.rate).toFixed(2)*100); 
				if(json.orderType == "1"){  
					//升  
					$("#afterRate").find(".fa").addClass("fa-level-up");
				}
				else{ 
					$("#afterRate").find(".fa").addClass("fa-level-down");
				}   
			});
			
			$(".btnAfter").click(function(){
				var queryType = $(this).attr("queryType")
				$(".btnAfter").removeClass("active");
				$(this).addClass("active");    
				$.getJSON("${pageContext.request.contextPath}/getAfterData?quertyType=" + queryType,function(json){
					console.log(json); 
					$("#newAfter").html(json.nowCount);
					$("#afterRate em").html(parseFloat(json.rate).toFixed(2)*100);  
					if(json.orderType == "1"){  
						//升  
						$("#afterRate").find(".fa").addClass("fa-level-up");
					}
					else{    
						$("#afterRate").find(".fa").addClass("fa-level-down");
					} 
				});
			});
		}
		init();    
	});
</script>
</body>
</html>