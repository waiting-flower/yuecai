<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">


<!-- Mirrored from www.zi-han.net/theme/hplus/login_v2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:49 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>用户登录</title>
    <link href="${pageContext.request.contextPath}/admin/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/animate.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin/static/css/login.min.css" rel="stylesheet">
    <!--[if lt IE 9]> 
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <script>
        if(window.top!==window.self){window.top.location=window.location};
    </script>
    <style type="text/css">
    body.signin{
    	color:#555; 
    } 
    </style>

</head>

<body class="signin">
    <div class="signinpanel">
        <div class="row">
            <div class="col-sm-7"> 
                <div class="signin-info" style="color:#fff;">
                    <div class="logopanel m-b">
                        <h1>商城</h1>
                    </div> 
                    <div class="m-b"></div>
                    <h4>欢迎使用 <strong>某某 后台管理系统</strong></h4>
                    <ul class="m-b">
                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 优势一</li>
                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 优势二</li>
                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 优势三</li>
                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 优势四</li>
                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 优势五</li>
                    </ul>
                    <strong>还没有账号？ <a href="${pageContext.request.contextPath}/reg">立即注册&raquo;</a></strong>
                </div>
            </div>
            <div class="col-sm-5">
                <form class="form-horizontal">
                    <h4 class="no-margins">登录：</h4>
                    <p class="m-t-md">登录到山东app后台管理系统</p>
                    <input type="text" class="form-control uname" id="userName" required="" placeholder="用户名" value="" />
                    <input type="password" class="form-control pword" id='password' required="" placeholder="密码"  value=""/>
                    <div class="form-group m-b"> 
                        <div class="col-sm-12">  
                        	<input type="text" style="display:inline-block;width:calc(100% - 80px);"
                        		 id="vercode" required="" placeholder="验证码" class="form-control">
                    		<img    
                    		style="width:70px;height:32px;"
                    		src="${pageContext.request.contextPath}/getRandomPic" id="randpic">
                    	</div>     
                    </div> 	  
                               
                    <a href="${pageContext.request.contextPath}/findpass">忘记密码了？</a>
                    <button type="button" id="btnLogin" class="btn btn-success btn-block">登录</button>
                </form>  
            </div>
        </div>  
        <div class="signup-footer">
            <div class="pull-right">
                &copy; 2018~2019 All Rights Reserved. 某某公司
            </div>
        </div>
    </div>
   	<script src="${pageContext.request.contextPath}/admin/static/js/jquery.min.js?v=2.1.4"></script>
    <script src="${pageContext.request.contextPath}/admin/static/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="${pageContext.request.contextPath}/admin/static/js/plugins/layer/layer.min.js"></script>
    <script type="text/javascript">
    	$(document).keyup(function(event){
		  if(event.keyCode ==13){
			  $("#btnLogin").trigger("click");
		  } 
		}); 
	$("#randpic").click(function(){
		//验证码图片点击事件
		//获取当前的时间作为参数，无具体意义     
		var timenow = new Date().getTime(); 
		$(this).attr("src","${pageContext.request.contextPath}/getRandomPic?d=" + timenow);
	});
	$("#btnLogin").click(function(){
		var u = $("#userName").val();
		var p = $("#password").val();
		var vercode = $("#vercode").val();
		if(u == "" || u == null || u == '输入账号'){
			layer.msg('用户不能为空');
			return false;
		}
		if(p == "" || p == null || p == "输入密码"){
			layer.msg('密码不能为空');
			return false;
		} 
		if(vercode == "" || vercode == null || vercode == "验证码" || vercode == '输入验证码'){
			layer.msg('验证码不能为空');
			return false; 
		}    
		$.post("${pageContext.request.contextPath}/admin/user/login",{
			"userName" : u,
			"password" : p,
			"vercode" : vercode
		}, function(json){
			if(json.status == "1"){
				location.href = "${pageContext.request.contextPath}/index";
			}
			else if(json.status == "-1"){
				layer.alert('验证码不正确'); 
			}
			else if(json.status == "2"){ 
				layer.alert('您的账号因为违规操作已经被管理员禁用，请联系工作人员进行解禁操作'); 
			} 
			else{ 
				layer.alert('用户名或者密码错误');
			}
		},"json");
	});  
    </script>
</body>
</html>
