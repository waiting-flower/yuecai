<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>购物车</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/cartList.css?v=161012" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath }js/cartListFun.js" language="javascript" type="text/javascript"></script>
<style type="text/css">
.cart_store_name{
	width:94%; 
	background:#f7f7f7;
	color:#ef4238;
	height:30px;
	line-height:30px;
	padding:5px 3%;
}
</style>
</head>
<body fnav="0"  id="loadingPicBlock"  class="g-acc-bg">


	<div class="g-Cart-list marginB">
		<ul id="cartBody"> 
			<div class="loading clearfix">
				<b></b>正在加载
			</div>
		</ul>
		<div id="divNone" class="empty" style="display: none;">
			<s></s>购物车为空
		</div>
	</div>
	 <div id="mycartpay" class="g-Total-bt g-car-new" style="display:none ;">
            <dl>
                <dt class="gray6"> 
                    <p class="money-total">合计:<em class="orange" id="allPrice"></em>元</p>
                    <p class="pro-total">共<em id="goodsNum"></em>个商品</p>
                </dt> 
                <dd> 
                    <a href="javascript:;" id="a_payment" class="orangeBtn w_account">去结算</a>
                </dd>
            </dl>
        </div>
         
	<input id="hidPageType" type="hidden" value="4" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script> 
		$(".f_car a").addClass("hover");
	</script>
</body>
</html>
