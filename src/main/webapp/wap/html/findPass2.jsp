<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>找回密码</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />

<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/app.css?v=151209" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath }js/findPassFun.js" language="javascript" type="text/javascript"></script>

</head>
<body class="g-acc-bg">
	 
	<div class="loginBg">
		<div class="card">
			<div class="title">
	        	<h4>忘记密码</h4>
	        </div>
	        <label><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">密码</font></font></label>
	        <input class="form-control" type="password" id="password" name="login-username" placeholder="请输入您要重置的密码">
			<label><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">确认密码</font></font></label>
	        <input type="password" id="password2" name="password2" placeholder="请确认您填写的密码">
	        <label><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">手机号码</font></font></label>
	        <input type="text" id="mobile" name="mobile" placeholder="请填写您账户注册的手机号码">
	        <label><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">短信验证码</font></font></label>
	        <input type="text" id="snsCode" name="snsCode" placeholder="请输入您收到的短信验证码">
	        <div class="line" style="margin-top:33px;">
	        	<a href="javascript:;" id="btnYzm" class="btnSns">获取验证码</a>
	        </div>
	        <div class="line" style="margin-top:13px;"> 
	        	<a href="javascript:;" id="btnSave" class="btnLogin">提交验证</a>
	        </div> 
		</div>
		<div class="line"> 
	        	<a href="${pageContext.request.contextPath}/shopWap.html?login" id="btnLogin" class="btnForget">登录</a>
	        </div> 
	</div>
	
	<jsp:include page="../html/footer.jsp"></jsp:include>
		<input id="hidPageType" type="hidden" value="-1" /> 
	<script>     
		$(".footer").hide(); 
	</script>
</body>
</html>
