<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>${cate.name}商户列表</title> 
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath }js/storeListFun.js?v=160903" language="javascript" type="text/javascript"></script>

</head>
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	<input type="hidden" value="${cate.id}" id="cateId" > 
	<div class="store_list" id="storeList" style="margin-bottom:55px;">
		<!-- 
		<div class="store_item" onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?storeDetail'">
			<div class="store_img_box">
				<img alt="" src="http://img20.aihuju.com/seller_imgs/seller_logo/logo_thumb/1517854389217371035.jpg">
			</div>
			<div class="content_box">
				<div class="name">我的店铺的名称</div>
				<span class="num">已经有200人关注</span>
				<span class="juli">为2323</span>
			</div>
			<div class="op_box">
				<div class="not_guanzhu guanzhubtn">
					<i class="fa fa-heart"></i>
					关注
				</div>
			</div> 
		</div> 
		<div class="store_item" onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?storeDetail'">
			<div class="store_img_box">
				<img alt="" src="http://img20.aihuju.com/seller_imgs/seller_logo/logo_thumb/1517854389217371035.jpg">
			</div>
			<div class="content_box">
				<div class="name">我的店铺的名称</div>
				<span class="num">已经有200人关注</span>
				<span class="juli">距离您239km</span>
			</div>
			<div class="op_box"> 
				<div class="yi_guanzhu guanzhubtn">
					已关注
				</div>
			</div>
		</div>
		 -->  
		 <div id="noneTipDiv"></div>
	</div>
	
	<input id="hidPageType" type="hidden" value="4" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script> 
	</script>
</body>
</html>
