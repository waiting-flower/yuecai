<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>搜索结果</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath }js/searchResultFun.js?v=160903" language="javascript" type="text/javascript"></script>
<style type="text/css">
    .goods_list{  
    	float:left; 
    }
    .search_prolist_item {
	    display: block;
	    position: relative;
	    padding: 3px 0 2px;
	}
	.search_prolist_item_inner {
	    position: relative;
	    padding-left: 120px;
	}
	.search_prolist_cover {
	    position: absolute;
	    left: 0;
	    top: 0;
	    width: 120px;
	    height: 120px; 
	    border-radius:5px; 
	    overflow: hidden;
	}
	.search_prolist_cover img {
	    width: 100%;
	}
	.search_prolist_info {
	    min-height: 120px;
	    padding-bottom: 8px;
	    box-sizing: border-box;
	    padding: 0 10px;
	    overflow: hidden;
	    line-height: 1;
	}
	.search_prolist_title {
	    overflow: hidden;
	    text-overflow: ellipsis;
	    display: -webkit-box;
	    -webkit-line-clamp: 2;
	    -webkit-box-orient: vertical;
	    height: 34px;
	    word-break: break-all;
	    margin-top: 8px;
	    color: #333;
	    line-height: 1.36;
	}
	.search_prolist_price {
	    margin-top: 13px;
	    display: inline-block;
	    height: 18px;
	    color: #e93b3d;
    	min-width: 40px;
	}
	.search_prolist_price strong {
	    font-size: 12px;
	    font-weight: 400;
	}
	.search_prolist_price strong span{
	    font-size: 12px;
	    font-weight: 400;
	    font-size: 18px; 
	}
	.search_prolist_price strong b { 
	    font-weight: 400;
	    font-size: 18px;
	}
	.mod_tag {
	    display: inline-block;
	    vertical-align: middle;
	    margin-top: -2px;
	    margin-right: 5px;
	    height: 14px;
	}
	.mod_tag img {
	    display: block;
	    height: 100%;
	}
	.search_prolist_line { 
	    display: inline-block;
	    margin-left: 2px;
	}
	.search_prolist_other {
	    margin-top: 8px;
	    font-size: 12px;
	}
	.search_prolist_comment{ 
	    font-size: 12px;
	    color: #999;
	}
    </style> 
</head>
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	
	<div>
        <input name="hdSortID" type="hidden" id="hdSortID" value="0" />
        <!--商品分类-->
        <input name="hdOrderFlag" type="hidden" id="hdOrderFlag" value="10" />
        <!--商品排序-->
        <input name="hdBrandID" type="hidden" id="hdBrandID" value="0" />
        <!--品牌ID-->
        <input name="hdNvgtID" type="hidden" id="hdNvgtID" />
        <!--品牌ID-->
        <input name="hdIsPreview" type="hidden" id="hdIsPreview" />
        <!--是否为预览-->

        <div class="pro-s-box thin-bor-bottom" id="divSearch">
            <div class="box">
                <div class="border">
                    <div class="border-inner"></div>
                </div>
                <div class="input-box"> 
                    <i class="s-icon"></i>
                    <input type="text" placeholder="搜索您喜欢的商品" id="txtSearch" value="${keyword}">
                    <i class="c-icon" id="btnClearInput" style="display: none"></i>
                </div>
            </div>
            <a href="javascript:;" class="s-btn" id="btnSearch">搜索</a>
        </div>
        <div class="search-info" style="display: none;">
            <div class="hot">
                <p class="title">热门搜索</p>
                <ul id="ulSearchHot" class="hot-list clearfix">
                    <c:forEach items="${ keyList }" var="item"  varStatus="status">
                    <li wd="${item }"><a class="items">${item }</a>
                    </li> 
                    </c:forEach>
                </ul> 
            </div>
            <div class="history" style="display: none">
                <p class="title">历史记录</p>
                <div class="his-inner" id="divSearchHotHistory"></div>
            </div>
        </div> 

        <div class="all-list-wrapper"> 
            <div class="good-list-wrapper" style="padding-left:0px;" >
                <div class="good-menu thin-bor-bottom">
                    <ul class="good-menu-list" id="ulOrderBy">
                        <li  class="current sp" stype="goods"><a href="javascript:;">商品</a>
                        </li> 
                        <li class="dp" stype="store"><a href="javascript:;">店铺</a>
                        </li>
                        <!--价值(由高到低30,由低到高31)-->
                    </ul>   
                </div>        
               	<div id="goodsResult" style="width:100%;height:35px;background:#f8f8f8;color:#666;line-height:35px;font-size:12px;border-bottom:1px solid #dedede;">
               		<span style="color:#FF5722;margin-left:15px;">“${keyword }”</span>&nbsp;共为您找到 
               		<span style="margin:0px 8px;font-size:16px;color:#01AAED;" id="goodsCount">0</span>件宝贝
               	</div> 
               	<div id="storeResult" style="display:none;width:100%;height:35px;background:#f8f8f8;color:#666;line-height:35px;font-size:12px;border-bottom:1px solid #dedede;">
               		<span style="color:#FF5722;margin-left:15px;">“${keyword }”</span>&nbsp;共为您找到 
               		<span style="margin:0px 8px;font-size:16px;color:#01AAED;" id="storeCount">0</span>家店铺
               	</div>  
				<div class="good-list-inner marginB" style="padding-left:0px;margin-top:40px;">
					<div class="good-list-box" id="loadingPicBlock">
						<div class="goods_list" style="width:100%;"  id="goodsList">
							<ul id="ulGoodsList">  
							</ul>  
						</div> 
						<div class="store_list" id="storeList" style="display:none;">
						</div>  
						<div id="divLoading" class="loading clearfix">
							<b></b>正在加载
						</div>  
						<div id="noneTipDiv" style="float:left;text-align:center;width:100%;"></div>
					</div>
					
				</div> 
			</div>
    </div>


    <input id="hidPageType" type="hidden" value="-2" />
    <input id="hidIsHttps" type="hidden" value="1" />
    <input id="hidSiteVer" type="hidden" value="v47" />
    <input id="hidWxDomain" type="hidden" value="https://m.1yyg.com" />
    <input id="hidOpenID" type="hidden" value="" />
    <jsp:include page="../html/footer.jsp"></jsp:include>
    </div>
</body>
</html>
