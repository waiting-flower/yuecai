<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>地址详情</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />

<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/member.css?v=151209" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath }js/userAddressDetailFun.js" language="javascript" type="text/javascript"></script>

</head>
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	<input type="hidden" id="orderId" value="${orderId }">
	<input type="hidden" id="tradeNo" value="${tradeNo }">
	<input type="hidden" id="scoreGoodsId" value="${scoreGoodsId }">
		<input type="hidden" id="num" value="${num }">
	
	<input type="hidden" id="op" value="${op }">
	<input name="hidUserID" type="hidden" id="hidUserID" value="${hidUserID}" />
	<input type="hidden" id="addrId" value="${a.id}">
	<div class="addrAddBox">  
		<div class="userDetail" style="margin-top:0px;padding:0 5%;margin-left:0;border:none;"> 
			<div class="tit rec">收货地址</div>	
			<div class="row">
				<span class="label">姓名：</span>
				<div class="info">
					<input type="text" id="name" value="${a.name }" class="input" placeholder="请输入收货人姓名">
				</div>
			</div> 
			<div class="row">
				<span class="label">手机：</span>
				<div class="info"> 
					<input type="text" id="mobile" value="${a.mobile }" class="input" placeholder="请输入收货人手机号码">
				</div>
			</div> 
			<div class="row">
				<span class="label">省份：</span>
				<div class="info">
					<span class="year">
						<div class="num" id="ANAME">请选择省份</div> 
						<i class="s-icon fa fa-angle-down"></i> 
						<select id="pro" class="opt">
							<option value="">请选择省份</option>
						</select>
					</span> 
				</div>
			</div> 
			<div class="row">
				<span class="label">城市：</span>
				<div class="info"> 
					<span class="year">
						<div class="num">请选择城市</div> <i class="s-icon fa fa-angle-down"></i> <select
						id="city" class="opt">
							<option value="0">请选择城市</option>
							</select>
					</span>
				</div>
			</div> 
			<div class="row">
				<span class="label">区县：</span>
				<div class="info">
					<span class="year">
						<div class="num">请选择区县</div> <i class="s-icon fa fa-angle-down"></i> <select
						id="country" class="opt">  
							<option value="0">请选择区县</option>
							</select>
					</span>
				</div>
			</div> 
			<div class="row">
				<span class="label">详情：</span>
				<div class="info">  
					<input type="text" id="address" value="${a.info }" class="input" placeholder="请输入收货地址详情,到门牌号">
				</div>
			</div>  
			<div class="btnBox" style="width:200px;">  
				<a class="btn" id="Add_address" href="javascript:;" style="width:200px;">提交</a> 
			</div>
		</div> 
	</div>
	
	<input id="hidPageType" type="hidden" value="4" />
	<jsp:include page="../html/footer.jsp"></jsp:include>

	<script> 
		$(".f_personal a").addClass("hover");
		var id = "${a.id}";
		$("#pro").change(function(){ 
			var proName = $(this).find("option:selected").text(); 
			$(this).prev().prev().html(proName);
			$("#city").prev().prev().html("请选择城市"); 
			$("#country").prev().prev().html("请选择区县");
			if(proName != "请选择省份"){ 
				$.getJSON("${pageContext.request.contextPath}/admin/assets/js/address.js",function(json){
					var len = json.length;
					for(var i = 0; i < len; i++){
						var proItem = json[i];
						if(proItem.name == proName){
							var cityArr = proItem.city;
							$("#city").empty(); 
							$("#city").append("<option value=''>请选择城市</option>"); 
							$.each(cityArr,function(i,item){
								var option = "<option src='" + item.id + "'>" + item.name + "</option>"; 
								$("#city").append(option); 
							});
							break;
						}
					}
					
				});
			}
		});
		$("#city").change(function(){ 
			var proName = $("#pro").find("option:selected").text();
			var cityName = $(this).find("option:selected").text(); 
			$(this).prev().prev().html(cityName);
			$("#country").prev().prev().html("请选择区县");
			if(proName != "请选择省份" && cityName != "请选择城市"){ 
				$.getJSON("${pageContext.request.contextPath}/admin/assets/js/address.js",function(json){
					var len = json.length;
					for(var i = 0; i < len; i++){
						var proItem = json[i];
						
						if(proItem.name == proName){
							console.log(proItem.name);
							var cityArr = proItem.city;
							for(var j = 0; j < cityArr.length; j++){
								var cityItem = cityArr[j];
								if(cityItem.name == cityName){
									console.log(cityItem.name); 
									var arr = cityItem.district;
									$("#country").empty();  
									$("#country").append("<option value=''>请选择区县</option>"); 
									$.each(arr,function(i,item){
										var option = "<option src='" + item.id + "'>" + item.name + "</option>"; 
										$("#country").append(option); 
									});
								}
							}
							break;
						}
					}
					
				});
			}
		});
		$("#country").change(function(){
			var countryName = $(this).find("option:selected").text(); 
			$(this).prev().prev().html(countryName); 
		});
		if(id == ''){ 
			
			//新的 
			$.getJSON("${pageContext.request.contextPath}/admin/assets/js/address.js",function(json){
				console.log(json); 
				$.each(json,function(i,item){ 
					var option = "<option src='" + item.id + "'>" + item.name + "</option>"; 
					$("#pro").append(option); 
				});
				
			});
		}
		else{
			var proName = "${a.pro}";
			var cityName = "${a.city}"; 
			var countryName = "${a.county}"; 
			$("#pro").prev().prev().html(proName);
			$("#city").prev().prev().html(cityName);
			$("#country").prev().prev().html(countryName);
			 
			//新的  
			$.getJSON("${pageContext.request.contextPath}/admin/assets/js/address.js",function(json){
				for(var i = 0; i < json.length; i++){
					var item = json[i];
					if(item.name == proName){ 
						var option = "<option selected src='" + item.id + "'>" + item.name + "</option>"; 
						$("#pro").append(option);
					}
					else{
						var option = "<option src='" + item.id + "'>" + item.name + "</option>"; 
						$("#pro").append(option);
					}
					 
					if(item.name == proName){
						for(var j = 0; j < item.city.length; j++){
							var item2 = item.city[j]; 
							if(item2.name == cityName){ 
								var option2 = "<option selected src='" + item2.id + "'>" + item2.name + "</option>"; 
								$("#city").append(option2); 
							}
							else{
								var option2 = "<option src='" + item2.id + "'>" + item2.name + "</option>"; 
								$("#city").append(option2); 
							}
							
							if(item2.name == cityName){   
								for(var k = 0; k < item2.district.length; k++){
									var item3 = item2.district[k]; 
									if(item3.name == countryName){  
										var option3 = "<option selected src='" + item3.id + "'>" + item3.name + "</option>"; 
										$("#country").append(option3); 
									}
									else{
										var option3 = "<option src='" + item3.id + "'>" + item3.name + "</option>"; 
										$("#country").append(option3); 
									}
								}
							}
						}
					}
				}     
			});
		} 
	</script>
</body>
</html>
