<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>商家入驻</title> 
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath }js/storeRuzhuFun.js" language="javascript" type="text/javascript"></script>

</head> 
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	<input type="hidden" value="${a.id }" id="storeId">
	<div class="shop_cart">
   		<div class="schedule"><ul><li class="on"><em>1</em>入驻商家信息</li><li><em>2</em>入驻商铺信息</li><li><em>3</em>审核完成</li></ul></div>
	</div>

	<div class="form_box">
		<div class='col'>
			<span>店铺名称：</span> 
			<input id="name" value="${a.name }" placeholder="请输入商家名称" />
		</div> 
		<div class='col'>
			<span>城市管家关键词：</span> 
			<input id="searchKey" value="${a.searchKey }" placeholder="所属城市管家关键词，可以不填写" />
		</div> 
		<div class='col'>
			<span>手机号码：</span>
			<input id="mobile" value="${a.mobile }" placeholder="请输入商家联系方式" />
		</div>
		<div class='col'>
			<span>入驻类目：</span>
			<div class="info">
				<span class="year">
				<div class="num" id="cateName">请选择店铺入驻类目</div> 
					<i class="s-icon fa fa-angle-down"></i> 
					<select id="cate" class="opt">
						<option value="">请选择入驻类目</option>
						<c:forEach items="${ cateList }" var="item">
						<option value="${item.id }">${item.name }</option>
						</c:forEach>
					</select> 
				</span>  
			</div>  
		</div>
		<div class='col'>
			<span>所在省份：</span>
			<div class="info">
				<span class="year">
				<div class="num" id="ANAME">请选择省份</div> 
					<i class="s-icon fa fa-angle-down"></i> 
					<select id="pro" class="opt">
						<option value="">请选择省份</option>
					</select>
				</span>  
			</div> 
		</div>
		<div class='col'>
			<span>所在城市：</span>
			<div class="info"> 
					<span class="year">
						<div class="num">请选择城市</div> <i class="s-icon fa fa-angle-down"></i> <select
						id="city" class="opt">
							<option value="0">请选择城市</option>
							</select>
					</span>
				</div>
		</div>
		<div class='col'>
			<span>所在区县：</span>
			<div class="info">
					<span class="year">
						<div class="num">请选择区县</div> <i class="s-icon fa fa-angle-down"></i> <select
						id="country" class="opt">  
							<option value="0">请选择区县</option>
							</select>
					</span>
				</div>
		</div>
		<div class='col'>
			<span>地址详情：</span>
			<input id="address" value="${a.address }" placeholder="请输入地址详情" />
		</div>
	</div>  
	<input type="hidden" value="${isReRuzhu }"	id="isReRuzhu">
	<div class="btnBox" style="width:200px;">   
		<a class="btn" id="ruzhuNextBtn" href="javascript:;" style="width:200px;">下一步</a> 
	</div>
			
	<input id="hidPageType" type="hidden" value="4" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script> 
		$(".f_personal a").addClass("hover");
		var id = "${a.id}";
		$("#pro").change(function(){ 
			var proName = $(this).find("option:selected").text(); 
			$(this).prev().prev().html(proName);
			$("#city").prev().prev().html("请选择城市"); 
			$("#country").prev().prev().html("请选择区县");
			if(proName != "请选择省份"){ 
				$.getJSON("${pageContext.request.contextPath}/admin/assets/js/address.js",function(json){
					var len = json.length;
					for(var i = 0; i < len; i++){
						var proItem = json[i];
						if(proItem.name == proName){
							var cityArr = proItem.city;
							$("#city").empty(); 
							$("#city").append("<option value=''>请选择城市</option>"); 
							$.each(cityArr,function(i,item){
								var option = "<option src='" + item.id + "'>" + item.name + "</option>"; 
								$("#city").append(option); 
							});
							break;
						}
					}
					
				});
			}
		});
		$("#city").change(function(){ 
			var proName = $("#pro").find("option:selected").text();
			var cityName = $(this).find("option:selected").text(); 
			$(this).prev().prev().html(cityName);
			$("#country").prev().prev().html("请选择区县");
			if(proName != "请选择省份" && cityName != "请选择城市"){ 
				$.getJSON("${pageContext.request.contextPath}/admin/assets/js/address.js",function(json){
					var len = json.length;
					for(var i = 0; i < len; i++){
						var proItem = json[i];
						
						if(proItem.name == proName){
							console.log(proItem.name);
							var cityArr = proItem.city;
							for(var j = 0; j < cityArr.length; j++){
								var cityItem = cityArr[j];
								if(cityItem.name == cityName){
									console.log(cityItem.name); 
									var arr = cityItem.district;
									$("#country").empty();  
									$("#country").append("<option value=''>请选择区县</option>"); 
									$.each(arr,function(i,item){
										var option = "<option src='" + item.id + "'>" + item.name + "</option>"; 
										$("#country").append(option); 
									});
								}
							}
							break;
						}
					}
					
				});
			}
		});
		$("#country").change(function(){
			var countryName = $(this).find("option:selected").text(); 
			$(this).prev().prev().html(countryName); 
		});
		if(id == ''){ 
			
			//新的 
			$.getJSON("${pageContext.request.contextPath}/admin/assets/js/address.js",function(json){
				console.log(json); 
				$.each(json,function(i,item){ 
					var option = "<option src='" + item.id + "'>" + item.name + "</option>"; 
					$("#pro").append(option); 
				});
				
			});
		}
		else{
			var proName = "${a.pro}";
			var cityName = "${a.city}";  
			var countryName = "${a.country}"; 
			$("#pro").prev().prev().html(proName);
			$("#city").prev().prev().html(cityName);
			$("#country").prev().prev().html(countryName);
			 
			//新的  
			$.getJSON("${pageContext.request.contextPath}/admin/assets/js/address.js",function(json){
				for(var i = 0; i < json.length; i++){
					var item = json[i];
					if(item.name == proName){ 
						var option = "<option selected src='" + item.id + "'>" + item.name + "</option>"; 
						$("#pro").append(option);
					}
					else{
						var option = "<option src='" + item.id + "'>" + item.name + "</option>"; 
						$("#pro").append(option);
					}
					 
					if(item.name == proName){
						for(var j = 0; j < item.city.length; j++){
							var item2 = item.city[j]; 
							if(item2.name == cityName){ 
								var option2 = "<option selected src='" + item2.id + "'>" + item2.name + "</option>"; 
								$("#city").append(option2); 
							}
							else{
								var option2 = "<option src='" + item2.id + "'>" + item2.name + "</option>"; 
								$("#city").append(option2); 
							}
							
							if(item2.name == cityName){   
								for(var k = 0; k < item2.district.length; k++){
									var item3 = item2.district[k]; 
									if(item3.name == countryName){  
										var option3 = "<option selected src='" + item3.id + "'>" + item3.name + "</option>"; 
										$("#country").append(option3); 
									}
									else{
										var option3 = "<option src='" + item3.id + "'>" + item3.name + "</option>"; 
										$("#country").append(option3); 
									}
								}
							}
						}
					}
				}     
			});
		}  
		$("#cate").change(function(){
			var cate = $(this).find("option:selected").text(); 
			$(this).prev().prev().html(cate); 
		}); 
		var cate = "${a.cate.id}";
		if(cate != ""){
			$("#cate").val(cate);   
			$("#cate").prev().prev().html("${a.cate.name}");
		}
	</script> 
</body>
</html>
