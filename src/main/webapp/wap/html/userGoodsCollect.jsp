<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>收藏的商品</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath }js/userGoodsCollectFun.js?v=160903" language="javascript" type="text/javascript"></script>
<style type="text/css">
.more_notice {
    position: relative;
} 
</style>
</head>
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	
	<div class="marginB" id="loadingPicBlock">
		<div id="c_div1" class="my_order_list marginB">
			<p class="fav_count" style=""> 
				您收藏了<em id="fav_total_num">0</em>个商品<span class="fav_edit"
					id="edit_btn">取消全部</span>
			</p>
			
		</div>
		<div id="divLoading" class="loading clearfix">
							<b></b>正在加载
						</div>
		
		<div class="weixin-mask" style="display: none;"></div>
	</div>
	
	
	<input id="hidPageType" type="hidden" value="4" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script> 
		$(".f_personal a").addClass("hover");
	</script>
</body>
</html>
