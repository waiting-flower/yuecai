<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<title>信息修改</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
 <link href="${skinPath }css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/member.css?v=151209" rel="stylesheet" type="text/css" />
<script src="${skinPath }js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath }js/userInfoModifyFun.js?v=1408155" language="javascript" type="text/javascript"></script>
</head>
<body fnav="1" class="g-acc-bg">
    
    <input name="hidOldName" type="hidden" id="hidOldName" value="${user.nick}"/>
    <input name="hidFlag" type="hidden" id="hidFlag" value="${t}" />

    <c:choose> 
    	<c:when test="${t == 1 }">
    	<div class="edit-wrapper">
            <input name="txtUserName" type="text" id="txtUserName" class="inp-info" maxlength="20" value="${user.nick }" />
            <p class="txt">昵称长度为2-20个字符，有汉字、字母、数字或"_-"组成</p>
            <a id="btnT1" href="javascript:;" class="s-btn">保存</a>
        </div>
    	</c:when> 
    	<c:when test="${t==7 }">
        <div class="edit-wrapper">
            <textarea name="txtSign" id="txtSign" class="inp-info" maxlength="200">${user.sign}</textarea>
            <p class="txt">个性签名最多两百个字符</p>
            <a id="btnT7" href="javascript:;" class="s-btn">保存</a>
        </div> 
    	</c:when>
    	<c:otherwise> 
    	<div class="edit-wrapper">
            <input name="txtUserName" type="text" id="txtUserName" class="inp-info" maxlength="20" value="${user.nick}" />
            <p class="txt">昵称长度为2-20个字符，有汉字、字母、数字或"_-"组成</p>
            <a id="btnT1" href="javascript:;" class="s-btn">保存</a>
        </div>
    	</c:otherwise>
    </c:choose>
    
    
<input id="hidPageType" type="hidden" value="-1" />
<input id="hidIsHttps" type="hidden" value="1" />
<input id="hidSiteVer" type="hidden" value="v47" />
<input id="hidWxDomain" type="hidden" value="" />
<input id="hidOpenID" type="hidden" value=""/>
<jsp:include page="../html/footer.jsp"></jsp:include>
<script> 
		$(".f_personal a").addClass("hover");
	</script> 
</body>
</html>
