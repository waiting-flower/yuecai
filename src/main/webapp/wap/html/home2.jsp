<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>  
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" name="viewport" />
    <meta content="telephone=no" name="format-detection" />
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
    <link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
    <link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
    <link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
    <link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
    <link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
    <script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
    <script id="pageJS" data="${skinPath}js/indexFun.js?v=161012" language="javascript" type="text/javascript"></script>
<title>转米商城首页</title>  
</head> 
<body fnav="1" class="g-acc-bg" style="background:#f7f7f7;">  
	<div class="pro-s-box thin-bor-bottom" id="divSearch" style="position:fixed;"> 
		<div class="dingwei" onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?location&returnUrl=index'"> 
			<i class="fa fa-map-marker"></i><span id="location_top_cityName">定位中..</span>
			<i class="fa fa-caret-down"></i> 
		</div>
		<div class="box">
			<div class="border">
				<div class="border-inner"></div>
			</div>
			<div class="input-box"> 
				<i class="s-icon"></i> <input type="text" placeholder="搜索您喜欢的商品"
					id="txtSearch"> <i class="c-icon" id="btnClearInput"
					style="display: none "></i>
			</div>
		</div> 
		<a href="javascript:;" class="s-btn" id="btnSearch">搜索</a>
	</div>    
	<div class="search-info" style="display: none;bottom:0px;position:fixed;">
            <div class="hot">
                <p class="title">热门搜索</p>
                <ul id="ulSearchHot" class="hot-list clearfix">
                	<c:forEach items="${ keyList }" var="item"  varStatus="status">
                    <li wd="${item }"><a class="items">${item }</a>
                    </li> 
                    </c:forEach>
                </ul>
            </div>
            <div class="history" style="display: none">
                <p class="title">历史记录</p>
                <div class="his-inner" id="divSearchHotHistory"></div>
            </div>
        </div>

	<!-- 焦点图 -->
	<div class="hotimg-wrapper">
		<div class="hotimg-top"></div>
		<section id="sliderBox" class="hotimg">
		<div class="loading clearfix">
			<b></b>正在加载
		</div>
		</section>
	</div>
	 
	<div class="my_msg">
		<i class="my_i"><img alt="" src="${skinPath }/images/msg.png"></i>
		<ul class="gonggao_list"> 
			<!-- 
			<li><span><em>公告</em> 公告区域1 </span></li>
			<li><span><em>公告</em> 公告区域2 </span></li>
			<li><span><em>公告</em> 公告区域3 </span></li> 
			 -->
		</ul>      
		<em class="more_notice">更多</em>  
		<i class="right_i" onclick="location.href='${pageContext.request.contextPath}/shopWap.html?noticeList'"></i> 
	</div>
	
	<!-- 精选商户开始 -->
	<c:if test="${isShowHotStore == '1' }">
	<div class="store_box">   
		<div class="tit" style="padding-top:0px;"	  onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?storeList'">
			<img alt="" src="${skinPath }images/store.png">
			精选商户
			<i class="right_i"  style="right:5px;"></i>
		</div>
		<div class="three_store_box">
			<c:forEach items="${ storeList }" var="item"  varStatus="status">
			<c:if test="${status.index <= 2 }">
			<div class="item" onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?storeDetail&id=${item.id }'"> 
				<span class="title">${item.name }</span>
				<span class="region">${item.city }  ${item.country }</span>
				<img src="${item.imgUrl}" class="tu">
			</div> 
			</c:if> 
			</c:forEach>
		</div>
		<div class="two_store_box">
			<c:forEach items="${ storeList }" var="item"  varStatus="status">
			<c:if test="${status.index >2 }">
			<div class="item" onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?storeDetail&id=${item.id }'"> 
				<span class="title">${item.name }</span>
				<span class="region">${item.city }  ${item.country }</span>
				<span class="tags">精选</span> 
				<img src="${item.imgUrl}" class="tu"> 
			</div>  
			</c:if>
			</c:forEach>
		</div>
	</div>
	</c:if> 
	<!-- 精选商户结束 -->
	
	<!-- 品类中心开始 -->
	<div class="store_box">
		<div class="tit"	  onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?cateList'">
			<img alt="" src="${skinPath }images/pinlei.png">
			品类中心  
			<i class="right_i" style="right:5px;display:none;"></i>
		</div>
		<div class="pinlei_two">
			<c:forEach items="${ cateList }" var="item"  varStatus="status">
			<c:if test="${status.index < 2 }">
			<div class="item cate${status.index + 1 }"  onclick="location.href='${pageContext.request.contextPath}/shopWap.html?storeList&cateId=${item.id }'"> 
				<span class="cate_name">${item.name }</span>
				<img src="${item.imgUrl }" class="pinlei_tu">
			</div> 
			</c:if>
			</c:forEach>
		</div> 
		<div class="pinlei_three" >
			<c:forEach items="${ cateList }" var="item"  varStatus="status">
			<c:if test="${status.index > 1 }">
			<div class="item cate${status.index + 1 }"  onclick="location.href='${pageContext.request.contextPath}/shopWap.html?storeList&cateId=${item.id }'"> 
				<span class="cate_name">${item.name }</span>
				<img src="${item.imgUrl }" class="pinlei_tu">
			</div>
			</c:if>
			</c:forEach>
		</div>
	</div>  
	<!-- 品类中心结束 -->
	
	<c:if test="${not empty goodsList }">
	<!-- 新品上线开始 -->
	<div class="store_box">  
		<div class="tit" style="position:relative;"   onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?list&isNew=1'"> 
			<img alt="" src="${skinPath }images/icon_news.png">
			新品上线   
			<i class="right_i" style="right:5px;display:none;"></i>
		</div>
		<div class="news_goods_list_box">
			<c:forEach items="${ goodsList }" var="item">
			<a href="${pageContext.request.contextPath}/shopWap.html?goodsDetail&id=${item.id }" style="margin-right:5px;" >
				<div class="news_goods_box">
					<div class="goods_img_box">  
						<img alt="" src="${item.newsImgUrl }">
					</div>
					<div class="goods_name">${item.name }</div>
					<div class="now_price">￥${item.price }<del style="margin-right:10px;">￥${item.oldPrice }</del></div> 
				</div>    
			</a>
			</c:forEach>
		</div>
	</div> 
	</c:if> 
	<!-- 新品上线结束 -->
	
	
	<!-- 特价商品开始 --> 
	<c:if test="${not empty tejiaGoodsList }">
	<div class="store_box"> 
		<div class="tit" style="position:relative;"> 
			<img alt="" src="${skinPath }images/icon_tejia.png">
			特价商品   
			<i class="right_i" style="right:5px;display:none;"></i>
		</div> 
		<div class="tejia_box">
			<c:forEach items="${ tejiaGoodsList }" var="item"  varStatus="status">
			<a href="${pageContext.request.contextPath}/shopWap.html?goodsDetail&id=${item.id }">
				<div class="tejiaItem ${status.index % 2 ==  0?'tejia1':'' }">
					<span class="name">${item.name }</span> 
					<span class="tags">${item.tags }</span> 
					<span class="price">￥ ${item.price }</span>  
					<img alt="" src="${item.zhekouImgUrl}">
				</div>    
			</a>
			</c:forEach>
		</div>
	</div>   
	</c:if> 
	<!-- 特价商品结束 -->
	
	<div class="h55"></div>
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script type="text/javascript">
	$(".f_home a").addClass("hover"); 
	function CreateScript(src){
        var el = document.createElement('script');
        console.log(el);
        el.src = src;
        el.async = true;
        el.defer = true;
        document.body.appendChild(el);
    }
   /**
    * 经纬度解析返回
    * @param rs
    * @returns 
    */ 
   function renderReverse(rs) {
       // var rs = JSON.stringify(rs); 
	   console.log(rs); 
       var city = rs["result"]['addressComponent']['city'];
       var county = rs["result"]['addressComponent']['district'];
       city = city.toString(); 
       county = county.toString();  
       var cityName = cookie.get("cityName");
       var countyName = cookie.get("countyName");
       cookie.set("cityName",city);
	   cookie.set("countyName",county);
       $("#location_top_cityName").html(city + "-" + county);   
   } 
		
		
		BNJSReady(function() { 
		    BNJS.ui.hideLoadingPage();
		    var lat = BNJS.location.latitude;
		    var lon = BNJS.location.longitude;
		    //获取到经纬度信息    
		    var url = "https://api.map.baidu.com/geocoder/v2/";
		    var data = { 
		    		callback: 'renderReverse', 
		    		location:lat + "," + lon,  
		    		output : "json", 
		    		pois : "1",   
		    		ak: "GHsIH3jHrSrL0Q3gY82VfX4Bbp3VSCR7" 
		            
		    };
		    //组合url 
		    var buffer = []; 
		    for (var key in data) {
		        buffer.push(key + '=' + encodeURIComponent(data[key]));
		    } 
		    var fullpath = url + '?' + buffer.join('&');
		    CreateScript(fullpath);
		}); 
	
	</script>
</body>
</html>