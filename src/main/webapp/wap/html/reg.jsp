<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>用户注册</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet"
	type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet"
	type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet"
	type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet"
	type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet"
	type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522"
	rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet"
	type="text/css" />
		<link href="${skinPath }css/login2.css?v=170522" rel="stylesheet"
	type="text/css" />  
<script src="${skinPath}js/jquery190.js" language="javascript"
	type="text/javascript"></script>
	<script id="pageJS" data="${skinPath }js/regFun.js" language="javascript" type="text/javascript"></script>
	
</head>
<body fnav="0" id="loadingPicBlock" class="g-acc-bg">

	<div class="con">
		<div class="user-center user-register of-hidden">
			<div class="hd j-tab-title">
				<ul class="dis-box">
					<li class="box-flex active">新用户注册</li>
				</ul>
			</div>
			<div id="j-tab-con"
				class="swiper-container-horizontal swiper-container-autoheight swiper-container-android">
				<div class="swiper-wrapper" style="height: 371px;">
					<section class="swiper-slide swiper-no-swiping swiper-slide-active"
						style="width: 402px;">
						<div class="b-color-f  user-login-ul user-register-ul">
							<div class="text-all dis-box j-text-all login-li"
								name="mobilediv">
								<div class="box-flex input-text">
									<input class="j-input-text" id="mobile" name="mobile" type="tel"
										placeholder="请输入手机号"> <i
										class="iconfont icon-guanbi1 close-common j-is-null"></i>
								</div>
							</div>
							<div class="text-all dis-box j-text-all login-li  m-top10"
								name="passworddiv">
								<div class="box-flex input-text">
									<input class="j-input-text" id="password" name="password" type="password" placeholder="请设置6-20位登录密码"> <i
										class="iconfont icon-guanbi1 close-common j-is-null"></i>
								</div>
								<i class="iconfont icon-yanjing is-yanjing j-yanjing disabled"></i>
							</div> 
							<div class="text-all dis-box j-text-all login-li m-top10"
								name="mobile_codediv">
								<div class="box-flex input-text"> 
									<input class="j-input-text" name="mobile_code" id="snsCode" type="number" placeholder="请输入短信验证码">
									<i class="iconfont icon-guanbi1 close-common j-is-null"></i>
								</div>
								<a type="button" 
									class="ipt-check-btn ipt-check-btn-new disabled" href="javascript:;"
									id="btnYzm"><span>获取短信验证码</span></a>
							</div> 
						</div>  

						<input type="hidden" name="enabled_sms" value="1">     
						<button id="btnReg"  type="button" class="btn-submit min-two-btn br-5">提交注册</button>
						<div class="u-l-register fl">
							注册即视为同意《<a 
								href="javascript:;"
								class="a-first">用户注册协议</a>》
						</div>
  
						<a
							href="${pageContext.request.contextPath}/shopWap.html?login"
							class="a-first u-l-register" style="margin-top: 8.2rem">已注册直接登录</a>

					</section>

				</div>
			</div>
		</div>
	</div>

	<input id="hidPageType" type="hidden" value="-1" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script> 
		$(".footer").hide();
	</script>
</body>
</html>
