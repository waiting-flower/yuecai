<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>城市选择</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/address.js" language="javascript" type="text/javascript"></script>
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script src="${skinPath}js/app_plusGeolocation.js" type="text/javascript" charset="utf-8"></script>
<script id="pageJS" data="${skinPath}js/locationFun.js?time=a22132322332323232" language="javascript" type="text/javascript"></script>
<style type="text/css">
.quxian_list{
	position:fixed; 
	left:0;
	top:0;
	width:100%;
	height:100%;
	background:#fff; 
	z-index:8999; 
	display:none;
}
.neirong_scroll{  
	overflow-x : hidden;
	overflow-y : auto; 
	margin-bottom:60px; 
}
.quxian_list .tit{  
	    width: 94%;
    padding: 14px 3%;
    font-size: 14px;
    color: #343434;
    border-bottom: 1px solid #e5e5e5;
    background: #f7f7f7; 
}
.quxian_list .xian_item{
	width: 94%;
    padding: 10px 3%;
    border-bottom: 1px solid #e5e5e5;
    line-height: 25px;
    height: 23px; 
	
}
.quxian_list .xian_op{
	position:absolute;
	bottom:0px;
	left:0px;
	width:100%; 
	height:50px;
	line-height:50px;
	font-size:14px;   
	background:#ef4238;
	color:#fff;
	text-align:center;
}
</style>
</head>       
<body fnav="1"  id="loadingPicBlock" style="background:#f1f1f1;"  class="g-acc-bg">
	<input type="hidden" value="${returnUrl }" id="returnUrl">
	<div class="now_location" id="link_1">
		当前：<span id="location_top_cityName"></span> <span class="now_location_tip">GPS定位</span> 
		<span class="location_re_dingwei"><i class="fa fa-map-marker"></i>重新定位</span>
	</div>	
	
	<div class="hot_location_div" >
		<span>已定位城市</span>
		<div class="hot_item_div_list"> 
			<div class="hot_item" id="now_dingwei_city"></div>
		</div> 
		<span id="link_HOT">热门城市</span> 
		<div class="hot_item_div_list">
			<div class="hot_item">北京市</div>
			<div class="hot_item">上海市</div>
			<div class="hot_item">广州市</div>
			<div class="hot_item">深圳市</div>
			<div class="hot_item">成都市</div>
			<div class="hot_item">西安市</div>
		</div>  
	</div>
	
	<div class="location_scroll_list">
		<!-- 
		<a href="javascript:;" id="link_A">A</a>
		<div class="city_list">
			<div class="city_item">北京市</div> 
			<div class="city_item">北京市</div> 
			<div class="city_item">北京市</div> 
		</div>
		 -->
	</div>
	
	<div class="location_letter_div"> 
		<div class="letter_list">  
			<div class="letter_item" letter="1">#</div>
			<div class="letter_item" letter="HOT">热门</div>
			<div class="letter_item" letter="A">A</div>
			<div class="letter_item" letter="B">B</div>
			<div class="letter_item" letter="C">C</div> 
			<div class="letter_item" letter="D">D</div>
			<div class="letter_item" letter="E">E</div>
			<div class="letter_item" letter="F">F</div>
			<div class="letter_item" letter="G">G</div>
			<div class="letter_item" letter="H">H</div>
			<div class="letter_item" letter="J">J</div>
			<div class="letter_item" letter="K">K</div>
			<div class="letter_item" letter="L">L</div>
			<div class="letter_item" letter="M">M</div>
			<div class="letter_item" letter="N">N</div>
			<div class="letter_item" letter="P">P</div>
			<div class="letter_item" letter="Q">Q</div>
			<div class="letter_item" letter="R">R</div>
			<div class="letter_item" letter="S">S</div>
			<div class="letter_item" letter="T">T</div> 
			<div class="letter_item" letter="W">W</div>
			<div class="letter_item" letter="X">X</div>
			<div class="letter_item" letter="Y">Y</div>
			<div class="letter_item" letter="Z">Z</div>
		</div>
	</div>
	
	<div class="quxian_list"> 
		<div class="tit">选择区县</div>
		<div class="neirong_scroll">
			<!-- 
			<div class='xian_item'>睢宁县</div>
			<div class="xian_item">
				睢宁县
			</div>
			<div class="xian_item">
				睢宁县
			</div>
			 -->
		</div>
		<div class="xian_op">关闭</div>
	</div>
	
	
	<input id="hidPageType" type="hidden" value="-1" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script>  
	   function CreateScript(src){
	        var el = document.createElement('script');
	        console.log(el);
	        el.src = src;
	        el.async = true;
	        el.defer = true;
	        document.body.appendChild(el);
	    }
	   /**
	    * 经纬度解析返回
	    * @param rs
	    * @returns 
	    */ 
	   function renderReverse(rs) {
	       // var rs = JSON.stringify(rs); 
		   console.log(rs); 
	       var city = rs["result"]['addressComponent']['city'];
	       var county = rs["result"]['addressComponent']['district'];
	       city = city.toString(); 
	       county = county.toString(); 
	       cookie.set("isOther",'0');   
	       $("#now_dingwei_city").attr("city",city); 
	       $("#now_dingwei_city").attr("country",county);   
	       $("#now_dingwei_city").html(city + "-" + county); 
	   } 
	   var n_lat = null;
	   var n_lon = null;
	   
		if (window.plus) {
			plusReady();
		} else {
			document.addEventListener('plusready', plusReady, false);
		}
	   	function plusReady() {
	   		if(navigator.userAgent.match(/Html5Plus/i)) {
	   			//5+引擎环境
	   			plus.geolocation.getCurrentPosition(function(p){
	   				var lat = p.coords.latitude;
	   			    var lon = p.coords.longitude;
	   			    //获取到经纬度信息      
	   			    var url = "https://api.map.baidu.com/geocoder/v2/";
	   			    var data = { 
	   			    		callback: 'renderReverse', 
	   			    		location:lat + "," + lon,  
	   			    		output : "json", 
	   			    		pois : "1",   
	   			    		ak: "GHsIH3jHrSrL0Q3gY82VfX4Bbp3VSCR7" 
	   			            
	   			    };
	   			    //组合url 
	   			    var buffer = []; 
	   			    for (var key in data) {
	   			        buffer.push(key + '=' + encodeURIComponent(data[key]));
	   			    } 
	   			    var fullpath = url + '?' + buffer.join('&');
	   			    CreateScript(fullpath);
	   			}, function(e){
	   				alert('Geolocation error: ' + e.message);
	   			} );  
	   	 
	   		} 
		}
	   	
	BNJSReady(function() {
	    BNJS.ui.hideLoadingPage();
	    var lat = BNJS.location.latitude;
	    var lon = BNJS.location.longitude;
	    n_lat = lat;
	    n_lon = lon;
	    //获取到经纬度信息  
	    var url = "https://api.map.baidu.com/geocoder/v2/";
	    var data = { 
	    		callback: 'renderReverse', 
	    		location:lat + "," + lon,  
	    		output : "json", 
	    		pois : "1",   
	    		ak: "GHsIH3jHrSrL0Q3gY82VfX4Bbp3VSCR7" 
	            
	    };
	    //组合url
	    var buffer = []; 
	    for (var key in data) {
	        buffer.push(key + '=' + encodeURIComponent(data[key]));
	    } 
	    var fullpath = url + '?' + buffer.join('&');
	    CreateScript(fullpath);
	});
	$(".location_re_dingwei").click(function(){
		$("#now_dingwei_city").html("定位中..."); 
	    //获取到经纬度信息    
	    var url = "https://api.map.baidu.com/geocoder/v2/";
	    var data = { 
	    		callback: 'renderReverse', 
	    		location:n_lat + "," + n_lon,  
	    		output : "json", 
	    		pois : "1",    
	    		ak: "GHsIH3jHrSrL0Q3gY82VfX4Bbp3VSCR7" 
	            
	    };
	    //组合url
	    var buffer = []; 
	    for (var key in data) {
	        buffer.push(key + '=' + encodeURIComponent(data[key]));
	    } 
	    var fullpath = url + '?' + buffer.join('&');
	    CreateScript(fullpath); 
	});
		$(".footer").hide();
	</script>
</body>
</html>
