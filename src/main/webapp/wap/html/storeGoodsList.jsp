<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" 
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>店铺${cate.name }商品列表</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath}js/storeGoodsListFun.js?v=160903" language="javascript" type="text/javascript"></script>
</head>      
<body fnav="0"  class="g-acc-bg">
	<div style="margin-bottom:55px;">
       <input type="hidden" value="${cate.id }" id="storeCate">
	 	 
		<input type="hidden" value="${store.id }" id="storeId">
	   
	<div class="store_head_div" style="background-image:url('${store.backImgUrl}');background-size:100% 100%;"> 
		<img src="${store.imgUrl }">
		<div class="name">${store.name }</div>
		<div class="num" style="display:none;">已有${store.collectNum }人关注</div>
		<c:if test="${isCollect == '1' }">
		<div class="store_gz act" id="yiguanzhu" style="display:none;">
			<i class="fa fa-heart"></i>
			<span>已关注</span>
		</div>  
		</c:if>
		<c:if test="${isCollect == '0' }">
		<div class="store_gz" id="weiguanzhu" style="display:none;">  
			<i class="fa fa-heart-o"></i>
			<span>点击关注 </span>
		</div>
		</c:if>
	</div>
  
        <div class="all-list-wrapper" style="top:101px;"> 
            <div class="good-list-wrapper" style="padding-left:0px;">
                <div class="good-menu thin-bor-bottom">
                    <ul class="good-menu-list" id="ulOrderBy">
                        <li sorttype="10" class="current"><a href="javascript:;">综合</a>
                        </li> 
                        <li sorttype="20"><a href="javascript:;">销量</a>
                        </li>
                        <li sorttype="50"><a href="javascript:;">新品</a>
                        </li>
                        <li sorttype="30"><a href="javascript:;">价格</a><span class="i-wrap"><i class="up"></i><i class="down"></i></span>
                        </li>
                        <!--价值(由高到低30,由低到高31)-->
                    </ul>   
                </div> 
				<div class="good-list-inner" style="padding-left:0px;"> 
					<div class="good-list-box" id="loadingPicBlock">
						
						<div class="goods_list"> 
							<ul id="ulGoodsList"> 
								<div class="search_prolist cols_2 type_twotitle" id="itemList">
								</div>
							</ul>  
						</div>
						<div id="divLoading" class="loading clearfix" style="display: none;">
							<b></b>正在加载
						</div> 
						<div id="noneTipDiv"></div>
					</div>
					
				</div>
				  
			</div>
    	</div>
    </div> 

	<div id="div_fastnav" class="fast-nav-wrapper">
			<ul class="fast-nav">
				<li id="li_top2" style="display:none;"><a href="javascript:;"><i
						class="nav-top"></i></a></li>
			</ul>
		</div> 
	 
	 
	<input id="hidPageType" type="hidden" value="-1" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script> 
	</script> 
</body>
</html>
