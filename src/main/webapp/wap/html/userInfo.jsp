<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <title>个人资料</title>
    <meta content="yes" name="apple-mobile-web-app-capable" />
    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
    <meta content="telephone=no" name="format-detection" />
     <link href="${skinPath }css/comm.css?v=170522" rel="stylesheet" type="text/css" />
	<link href="${skinPath }css/member.css?v=151209" rel="stylesheet" type="text/css" />
	<script src="${skinPath }js/jquery190.js" language="javascript" type="text/javascript"></script>
    <script id="pageJS" data="${skinPath }js/userInfoFun.js?v=1408155" language="javascript" type="text/javascript"></script>
    <style type="text/css">
    .face-img {
	    width: 54px;
	    height: 54px;
	    overflow: hidden;
	    display: block;
	    margin: 8px 0;
		}
	.face-img img{
		width:100%;
		height:100%; 
		border-radius:50%; 
	}
	.tuichudlu{
		text-align: center;
    border-left: 1px solid #e5e5e5;
    border-right: 1px solid #e5e5e5;
    width: 80%; 
    margin: 0 auto;
    border-radius: 5px;
    margin-top: 20px;
	}
    </style>
</head>
<body fnav="1" class="g-acc-bg">
    <input name="hidSex" type="hidden" id="hidSex" value="${user.sex}" />
    <div class="sub_nav">
        <div class="link-wrapper">
            <a href="${pageContext.request.contextPath}/shopWap.html?userHeadUrl" style="height:70px;">
				<span class="face-img fl"><img src="${user.headUrl}"></span>   
				<i style="top:30px;"></i>         
            	<span class="fr" style="line-height:70px;">点击去修改</span>
            </a> 
        </div> 
        <div class="link-wrapper">
            <a href="${pageContext.request.contextPath}/shopWap.html?userInfoModify&t=1"><em>昵称</em><i></i><span class="fr">${user.nick }</span></a>
            <a href="javascript:;"><em>性别</em><i></i><span class="fr">
            <c:if test="${user.sex == '1' }">女</c:if>
            <c:if test="${user.sex == '2' }">男</c:if>
            <c:if test="${user.sex == '3' }">保密</c:if>
            </span>
            	<select id="selSex" class="sex">
            	<option value="2">男</option>
				<option value="1">女</option>
				<option value="3">保密</option></select> </a> 
            <a href="javascript:;"><em>绑定手机</em><strong>可以用与登录</strong><i></i><span class="fr">${user.mobile}</span></a>
        </div>
        <div class="link-wrapper"> 
            <a href="javascript:;"><em>当前城市</em><i></i><span class="fr"></span></a>
        </div> 
        <div class="link-wrapper"> 
            <a href="${pageContext.request.contextPath}/shopWap.html?userInfoModify&t=7" class="underwrite"><em>个性签名</em><i></i><span>${user.sign}</span></a>
        </div>  
        <div class="link-wrapper2">  
            <a href="javascript:;" class="tuichudlu">退出登录</a>
        </div> 
    </div> 
    
<input id="hidPageType" type="hidden" value="-1" />
<input id="hidIsHttps" type="hidden" value="1" />
<input id="hidSiteVer" type="hidden" value="v47" />
<input id="hidWxDomain" type="hidden" value="" />
<input id="hidOpenID" type="hidden" value=""/>
<jsp:include page="../html/footer.jsp"></jsp:include>
<script> 
		$(function(){
			$(".tuichudlu").click(function(){
				cookie.del("userId"); 
				location.href = "${pageContext.request.contextPath}/shopWap.html?logout"; 
			});
		});
		$(".f_personal a").addClass("hover");
	</script>
</body>
</html>
