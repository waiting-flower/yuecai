<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>我的售后订单列表</title> 
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />

<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/member.css?v=151209" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath }js/userOrderSalesAfterListFun.js" language="javascript" type="text/javascript"></script>
<style type="text/css">
page{
  background: #fff;
}
.info{
  width: 92%;
  padding: 0 4%;
}
.width{
  width: 33.3%;
  text-align: center;
}

.active{
  color:#ff4544;
  border-bottom: 4rpx #ff4544 solid;
}
.info .info-title{
  width: 100%;
  height: 80rpx;
  border-bottom: 1rpx #ccc solid;
  /* font-weight: bold; */
  background-color: #fff;
  position: fixed;
  left: 0;
  top: 0;
  z-index: 1000;
}
.info .info-title .info-text{
  height: 80rpx;
  line-height: 80rpx;
}
.list{
  width: 94%;
  margin: 0 auto;
  margin-top: 50px;
}
.list .main_item{
  width: 100%;
  padding-bottom: 10px;
   border-bottom: 1px solid #e5e5e5;
}
.list .item{
  width: 100%;
  display: flex;
 
}
.list .item .img_box{
  flex:3;
} 
.list .item .img_box img{
  width: 100%;
  height: 100%;
}
.list .item .content_box{
  flex:7;
  margin-left: 10px;
}
.content_box .name{
  display: block;
  width: 100%;
  color: #454545;
  font-size: 13px;
}
.content_box .size_box{
  display: flex;
  width: 100%;
}
.content_box .size_box .tags{
	background:#f60;
	color:#fff;
	padding:2px 5px;
	text-align:center; 
	border-radius:5px;
	margin-right:5px; 
	margin-top:5px; 
}
.size_box .size{
  flex: 3;
  color: #333;
  font-size: 12px;
}
.size_box .jiesuanjia{
  color: #ef4238; 
  flex: 2;
  font-size: 12px;
}
.list .main_item .footer_box{
  width: 100%;
  display: flex;
  margin-top: 10px;
}
.footer_box .beizhu{
  color: #fff;
  background: #fd4238;
  font-size: 12px;
  height: 20px;
  padding:2px 5px;  
  border-radius: 5px;
  text-align: center;
  line-height: 20px;
  
}
.footer_box .beizhu_neirong{
  color: #454545;
  flex: 8;
  font-size: 12px;
  margin-left: 10px;
  line-height: 20px;
}
.footer_box .shenqing_btn{
  flex: 2;
  color: #fff;
  background: #fd4238;
  font-size: 12px;
  width: 120px;
  height: 20px;
  border-radius: 5px;
  text-align: center;
  line-height: 20px;
}
.footer_box .order_tips{
   flex: 2;
  color: #fd4238;
  font-size: 12px;
  width: 120px;
  height: 20px;
  border-radius: 5px;
  text-align: center;
  line-height: 20px;
} 
</style>
</head> 
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	<input name="hidUserID" type="hidden" id="hidUserID" value="${hidUserID}" />
    <input name="hidType" type="hidden" id="hidType" value="${type}" />
	<div class="marginB">
			<div class="weui-navbar"  
				style="height: 44px;float:left;position:fixed;top:0px;left:0px; background: #fff;">
				<a 
					class="weui-navbar__item proinfo-tab-tit font-14 weui-bar__item--on"
					tar="allOrder" toId="-1">全部</a> <a
					class="weui-navbar__item proinfo-tab-tit font-14" tar="order1" toId="1">退货中</a>
				<a class="weui-navbar__item proinfo-tab-tit font-14" tar="order2" toId="2">已退货</a>
			</div>    
			<div class='list' id="allOrder" >
			  
			</div>
			<div id="divLoading" class="loading clearfix">
				<b></b>正在加载 
			</div>
		</div> 
		
	
	<input id="hidPageType" type="hidden" value="4" />
	<input id="hidOpenID" type="hidden" value="" />
	<jsp:include page="../html/footer.jsp"></jsp:include>

	<script>  
		$(".f_personal a").addClass("hover");
	</script>
</body>
</html>
