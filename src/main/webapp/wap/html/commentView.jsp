<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>评价</title>
    <meta content="app-id=984819816" name="apple-itunes-app" />
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" name="viewport" />
    <meta content="yes" name="apple-mobile-web-app-capable" />
    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
    <meta content="telephone=no" name="format-detection" />
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" /> 
    <meta http-equiv="Expires" content="0" />
    <link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
    <link href="${skinPath}css/comment.css?v=170522" rel="stylesheet" type="text/css" />
    <link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
    <script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
    <script id="pageJS" data="${skinPath}js/commentViewFun.js?v=161012" language="javascript" type="text/javascript"></script>
</head>   
<body fnav="1" class="g-acc-bg" style="background: #e8e8ed;"> 
 		 
	<input type="hidden" value="${comment.id}" id="commentId">
	<div class="marginB wx_wrap" id="loadingPicBlock" style="margin-bottom:65px;">
		<img style="width: 100%;display:none;" src="//img11.360buyimg.com/jdphoto/s640x120_jfs/t2458/213/2246045201/14670/59125143/56ab30aaN98c63f1f.png">
		<div class="comment_my_detail">
			<div class="head">
				<img class="image" 
					src="${comment.user.headUrl }">
				<p class="name">
					${comment.user.nick }<small>${comment.msgTime}</small>
				</p>
				<a class="link" href="javascript:void(0)" id="viewBtn" tag="viewBtn"
					data-count="100" ptag="7046.2.1">${comment.replyNum} 回复</a>
			</div>
			 
			<a href="${pageContext.request.contextPath}/shopWap.html?goodsDetail&id=${goods.id }" class="content"> 
				<img class="image" src="${goods.imgUrl}" width="50" height="50">             
				<p class="name">${goods.name}</p> 
				<p class="price"></p> 
			</a>

			<div class="article">
				<div class="stars">
					<i class="cur"></i> <i class="cur"></i> <i class="cur"></i> <i
						class="cur"></i> <i class="cur"></i>
				</div>  
				<p>${comment.msgContent }</p>
				<c:set var="string1" value="${comment.imgList}" />
				<c:set value="${ fn:split(string1, ',') }" var="imgList" />
				<c:forEach items="${ imgList }" var="item">
				<img style="width: 100%;" src="${item}">
				</c:forEach>  
				<div class="btn_more" style="display:none;">
					<a href="javascript:void(0)" id="moreImgBtn" tag="moreImgBtn"
						style="display:" ptag="7046.2.2">查看更多</a>
				</div>
			</div>

			<div class="mod_btns"> <div class="mod_btn bg_2" id="commentBtn" tag="commentBtn" replyid="-1" ptag="7046.2.3">回复</div> </div>
			
			
			<ul class="comment_userList" id="commentList" style="">
				
				<%for(int i=0;i<0;i++){ %>
				<li id="comment211423038"> <img class="image" src="//misc.360buyimg.com/user/myjd-2015/css/i/peisong.jpg"> <div class="inner">  <p class="name"><em>由***尚</em>回复<em>鏖***杀</em><span>2017-10-26 21:40:12</span></p>  <p class="text"> 说太对了 </p> <a class="link" href="javascript:void(0);" tag="replyBtn" replyid="211423038" guid="1ae95278-0d38-4caa-8c73-08e560dd2876" id="btn211423038" ptag="7046.2.6">回复</a> </div> <div class="comment_form" tag="replayPanel" style="display:none;" id="panel211423038"> <div class="textarea_wrap"> <textarea placeholder="输入你的评论（150字以内）" tag="replyText" id="text211423038" maxlength="150"></textarea> </div> <div class="mod_btns"> <div class="mod_btn disabled bg_2" tag="replySubmit" client="22" guid="1ae95278-0d38-4caa-8c73-08e560dd2876" replyid="211423038" id="submit211423038" nickname="由***尚" ptag="7046.2.8">发表</div> </div> </div> </li>
				<%} %>
			</ul>
			
		</div>

		<div class="gwq_comment" style="display:none;" id="layer_comment">
			<div class="gwq_comment_inner"> 
				<textarea placeholder="我也说一句..." id="msgContent"></textarea>
				<div class="emoji_btn" style="display:none;">表情</div>
			</div>  
			<div class="gwq_comment_btn">
				<a href="javascript:;" data-tourl="7084.1.4" id="btn_cancle" class="color_grey">取消</a>
				<a href="javascript:;" data-tourl="7084.1.3" id="btn_save" class="color_blue">发送</a>
			</div>
		</div>

		<jsp:include page="./footer.jsp"></jsp:include>
		<input id="hidPageType" type="hidden" value="0" /> 
		<input id="hidIsHttps" type="hidden" value="1" /> 
		<input id="hidSiteVer" type="hidden" value="v47" /> 
		<input id="hidWxDomain" type="hidden" value="" /> 
		<input id="hidOpenID" type="hidden" value="" />

		<div class="weixin-mask" style="display: none;"></div>
	</div>
</body> 
</html>
