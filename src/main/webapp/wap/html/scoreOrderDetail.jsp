<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>       
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>积分订单详情</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
    <meta content="yes" name="apple-mobile-web-app-capable" />
    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
    <meta content="telephone=no" name="format-detection" />
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <link href="${skinPath }css/comm.css?v=170522" rel="stylesheet" type="text/css" />
    <link href="${skinPath }css/my.css?v=161012" rel="stylesheet" type="text/css" />
    <link href="${skinPath }css/newShop.css?v=161012" rel="stylesheet" type="text/css" />
    <link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
    <script src="${skinPath }js/jquery190.js" language="javascript" type="text/javascript"></script>
    <script id="pageJS" data="${skinPath }js/userOrderDetailFun.js" language="javascript" type="text/javascript"></script>
    <style type="text/css">
    .user_orderbox_hd:after{
    	border:none; 
    }
    </style> 
</head>  
<body id="loadingPicBlock" class="g-acc-bg">
    <input name="hidUserID" type="hidden" id="hidUserID" value="${hidUserID}" />
     <input name="hidOrderId" type="hidden" id="hidOrderId" value="${a.id}" />
    <div>   
		<div class="marginB">
			<div class="order_state">
				<div class="order_info">
					<div class="inner">
						<div class="inner_line pState">
							<span class="title">订单状态：</span>
							<div class="content">
								<em><c:choose>
						<c:when test="${a.orderState =='1' }"> 
							等待商家发货
						</c:when>
						<c:when test="${a.orderState =='2' }">
							已发货，待收货
						</c:when>
						<c:when test="${a.orderState =='4' }">
							已完成
						</c:when>
						<c:otherwise><!-- -2:已取消 -->
							已取消 
						</c:otherwise>
					</c:choose></em>
							</div>
						</div>
						<div class="inner_line"> 
							<span class="title">订单编号：</span>
							<div class="content">${a.orderNo }</div>
						</div>
						<!-- 发货的有快递 -->   
						<c:if test="${a.orderState > 1 }">
						<div class="inner_line"> 
							<span class="title">快递公司：</span>
							<div class="content">${a.expressName }</div>
						</div>
						<div class="inner_line"> 
							<span class="title">快递单号：</span>   
							<div class="content"><a style="color:#3985ff;" href="${pageContext.request.contextPath}/shopWap.html?orderWuliu&danhao=${a.expressCode }">${a.expressCode }&nbsp;&nbsp;&nbsp;点击查看物流</a></div>
						</div>
						</c:if>
						<div class="inner_line">
							<span class="title">下单时间：</span>
							<div class="content">${a.createDate }</div>
						</div>
						<div class="inner_line">
							<span class="title">其他说明：</span>
							<div class="content">${a.storeBak }</div>
						</div>
					</div> 
				</div> 
				
				<div class="mod_btns">
					<c:if test="${a.orderState == '2' }"><!-- 待支付 -->
					<a href="javascript:;" id="btn_confirm_receive" class="mod_btn bg_1 toPay">确认收货</a>
					</c:if>
				</div> 
				<div class="mod_btns">
					<a href="${pageContext.request.contextPath}/shopWap.html?scoreOrderList" class="mod_btn bg_6 ">返回订单列表</a> 
				</div> 
			</div>
			
			<!-- 订单详情开始 -->
			<div class="order_detail" id="detailDiv">
				<p>
					<span>商品积分：</span><b>${a.score }</b> （积分兑换）
				</p> 
				<p>
					<span>订单运费：</span><b>0.00</b>
				</p>
				<p> 
					<span>收货地址：</span>${a.address }
				</p>
				<p>
					<span>收货人：</span>${a.name } ${a.mobile } 
				</p>
				<p>
					<span>配送方式：</span> 普通快递 &nbsp;&nbsp;&nbsp;<em class="color_black"></em>
				</p>
				<p style="padding-left: 86px;"> 
					<span style="width: 92px;">期望送货时间：</span>尽快送达&nbsp;&nbsp;
				</p>
			</div>
			<!-- 订单详情结束 -->
			 
			<div class="about4">  
				<div class="detail_gap"></div>
				<div class="order_goods">  
					<a href="javascript:;" class="order_shopBar"> 
						积分商城订单 
						<span>共计${a.num}件商品</span>
					</a>      
					<div class="good" style="border-bottom:1px solid #e5e5e5;padding-bottom:10px;">
						<a href="${pageContext.request.contextPath}/shopWap.html?scoreGoodsDetail&id=${a.goods.id}">
							<div class="good_cover">
								<img src="${a.goods.imgUrl }" > 
							</div> 
						</a>
						<div class="info"> 
							<a href="${pageContext.request.contextPath}/shopWap.html?scoreGoodsDetail&id=${a.goods.id}">
								<div class="good_name">${a.goods.name }</div>
								<p class="good_price my_goods_price">积分：${a.goods.score}</p>
								<p class="good_count">×${a.num}</p> 
							</a>   
						</div> 
					</div>
				</div>
			</div>
		</div> 
		
		

		<input id="hidPageType" type="hidden" value="3" />
<input id="hidIsHttps" type="hidden" value="1" />
<input id="hidSiteVer" type="hidden" value="v47" />
<input id="hidWxDomain" type="hidden" value="https://m.1yyg.com" />
<input id="hidOpenID" type="hidden" value=""/>
<jsp:include page="./footer.jsp"></jsp:include>
    </div>

<script type="text/javascript">
$(".f_personal a").addClass("hover");
</script>
</body>
</html>