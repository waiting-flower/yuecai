<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<title>订单确认</title>
<meta content="app-id=984819816" name="apple-itunes-app" />
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath }css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/cartList.css?v=161012" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/my.css?v=161012" rel="stylesheet" type="text/css" /> 
<link href="${skinPath }css/app.css?v=161012" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<style type="text/css"> 
.addrListBox{
	width:100%; 
	float:none;
	margin-left:0;
	margin-top:20px;
}

.addrListBox .item{
	height:140px;
	width:96%;
	padding:10px 2%;
	background:#fff;
	float:none;
	position:relative;
}
.addrListBox .item .tag {
    position: absolute;
    background: rgb(214, 207, 202);
    color: #fff;
    bottom: 0px;
    right: 0px;
    width: 120px;
    height: 30px;
    line-height: 30px;
    text-align: center;
}
.addrListBox .item .tit{
	width:100%;
	height:40px;
	line-height:40px;
	font-size:16px;
	font-weight:bold;
	color:#333;
}

.addrListBox .item .tit span{
	float:left;
}
.addrListBox .item .tit .opt{
	float:right;
} 
.addrListBox .item .tit .opt .edit{
	width:20px;
	height:20px;
	background:#2373ef;
	color:#fff;
	line-height:20px;
	text-align:center;  
	padding: 2px;
}

.addrListBox .item .tit .opt .close{
	width:20px;
	height:20px;
	background:#ef4723;
	color:#fff;
	line-height:20px;
	text-align:center;
	padding: 2px;  
}
.addrListBox .item .addrName{
	font-size:16px;
	font-weight:bold;
	color:#333;
	width:100%;
	line-height:30px; 
}
.addrListBox .item .addrInfo{
	font-size:14px;
	color:#666;
	width:100%;
	line-height:20px; 
}
.addrListBox .item .addrTel{
	font-size:12px;
	color:#777;
	line-height:18px; 	
}
.addrListBox .btn{
	width:100%;
	height:35px;
	font-size:16px;
	background:#f60;
	color:#fff;
	text-align:center;
	border-radius:5px; 
	float:left; 
	line-height:35px;
	margin-top:20px; 
}

</style>
<script src="${skinPath }js/jquery190.js" language="javascript"
	type="text/javascript"></script>
<script id="pageJS" data="${skinPath }js/confirmCartOrderFun.js?v=161012"
	language="javascript" type="text/javascript"></script>
</head>
<body id="loadingPicBlock" class="g-acc-bg">
	<input id="tradeNo" type="hidden" value="${tradeNo}" /> 
	<input id="addressId" type="hidden" value="${a.id}" />
	<input type="hidden" value="${diff}" id="diff"> 
	 	
	<div>
		<div class="g-pay-lst">
			<div class="Order_address">
				<c:if test="${not empty a }">
					<span class="address_title">收货地址</span> 
					<a
						href="${pageContext.request.contextPath}/shopWap.html?userAddressList&tradeNo=${tradeNo}&op=choice"
						class="fa fa-pencil-square-o modify"></a>
					<div class="address_info">
						<p style="text-align: left; background-color: #fff;">${a.pro}${a.city }${a.county}${a.info}</p>
						<p style="text-align: left; background-color: #fff;">${a.name }
							${a.mobile}</p>
					</div> 
				</c:if>
				<c:if test="${empty a}">
					<a
						style="width: 100%; text-align: center; color: #1E9FFF; padding: 10px 0;"
						href="${pageContext.request.contextPath}/shopWap.html?userAddressDetail&tradeNo=${tradeNo}&op=choice"
						class="fa fa-pencil-square-o">点击新增收货地址</a>
				</c:if>
			</div>
		</div> 

		<c:forEach items="${ mapList }" var="map">
		<div class="user_orderbox">
			<div class="user_orderbox_hd">
				<span>单号：${map.order.orderNo }</span><span class="ord-status-txt-ts fr">
					<!-- 时间：<strong id="time_show" style="font-size: 12px;">0分</strong> -->
				</span>
			</div> 
			<!-- 商品列表 -->
			<div class="u_o_body">
				<!-- 订单里面的每一个商品对象 -->
				<c:forEach items="${ map.detailList }" var="item">
					<div class="u_goods_detail">
						<!-- 商品的图片 -->
						<div class="u_goods_img_box"> 
							<a href="${pageContext.request.contextPath}/shopWap.html?goodsDetail&id=${item.sku.goods.id}"><img class="u_goods_img"
								src="${item.sku.goods.imgUrl }" alt=""></a>
						</div>
						<!-- 商品的信息 -->
						<div class="u_g_info">
							<h1 class="weui-media-box__desc">   
								<a href="${pageContext.request.contextPath}/shopWap.html?goodsDetail&id=${item.sku.goods.id}" class="ord-pro-link">${item.sku.goods.name}</a>
							</h1>
							<p class="weui-media-box__desc">
								<span>${item.sku.goods.name2} </span>
							</p> 
							<div class="clear mg-t-10"> 
								<div class="wy-pro-pri fl">  
									¥<em class="num font-15">${item.sku.price}</em>
								</div>
								<div class="pro-amount fr">
									<span class="font-13">数量×${item.num }</span>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
			<!-- 商品列表结束 -->
			<div class="ord-statistics">
				<span>共<em class="num">${map.order.num }</em>件商品， 
				</span> <span class="wy-pro-pri">总金额：¥<em class="num font-15" 
					id="orderTotalMoney">${map.order.money}</em></span><span>(含运费<b>￥0</b>)
				</span>
			</div>
		</div>
		
		</c:forEach>
		
		<div class="buy_msg" id="remarkDiv" style="">
			<div class="buy_msg_tit">订单备注</div>  
			<input type="text" placeholder="选填，给商家留言" id="remark" value="${map.order.bak }" >
		</div> 
		
		<div class="other_pay marginB">  
	        <a href="javascript:;" id="payWx" class="wzf checked"><i></i>微信支付：<span class="gray9">(￥${money})</span><em class="orange fr"></em></a>
	        <a href="javascript:;" style="display:none;" id="payZfb"><i></i>支付宝支付：<span class="gray9">(￥${money})</span><em class="orange fr"></em></a>
	    </div>     
	    
	    <div class="h55"></div> 
		<div class="g-Total-bt"> 
			<dd>
				<a id="btnPay"  href="javascript:;" class="orangeBtn fr w_account">立即支付</a>
			</dd>
		</div>  
	
		<div id="payDivTips" style="z-index:100;position:fixed;background:rgba(0,0,0,.7);left:0px;top:0px;width:100%;height:100%;display:none;"> 
			<div style="border-radius:8px;width:260px;height:140px;background:#f7f7f7;position:absolute;left:50%;top:50%;margin-left:-130px;margin-top:-70px;">
			 	<div style="width:100%;text-align:center;height:50px;line-height:50px;margin-top:0px;border-bottom:1px solid #e5e5e5;">请确认支付是否完成</div>
			 	<div id="payOkBtn" style="width:100%;text-align:center;color:#e93b3d;font-size:14px;height:45px;line-height:45px;border-bottom:1px solid #e5e5e5;">已完成支付</div>   
			 	<div id="payRepeatBtn" style="width:100%;text-align:center;color:#777;font-size:12px;height:45px;line-height:45px;">支付遇到问题，重新支付</div>   
			</div>   
		</div> 

		<c:if test="${trade.isConfirm == '1' }">
		<script type="text/javascript"> 
			location.href = "${pageContext.request.contextPath}/shopWap/payOkCart-${trade.tradeNo}.html";
		</script>
		</c:if>
		<c:if test="${isPay == '1' }">
		<script type="text/javascript">
			$(function(){
				$("#payDivTips").show();
			});
		</script>
		</c:if> 
		<input id="hidPageType" type="hidden" value="-1" /> 
		<jsp:include page="./footer.jsp"></jsp:include>
		<script type="text/javascript">
		//操作cookie的对象
		var cookie = {
			set: function(name, value) {
				var Days = 30;
				var exp = new Date();
				exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
				document.cookie = name + '=' + escape(value) + ';expires=' + exp.toGMTString();
			}, 
			get: function(name) {
				var arr, reg = new RegExp('(^| )' + name + '=([^;]*)(;|$)');
				if(arr = document.cookie.match(reg)) {
					return unescape(arr[2]);
				} else {
					return null;
				}
			},
			del: function(name) {
				var exp = new Date();
				exp.setTime(exp.getTime() - 1);
				var cval = getCookie(name);
				if(cval != null) {
					document.cookie = name + '=' + cval + ';expires=' + exp.toGMTString();
				}
			}
		}; 
		var tradeNo = cookie.get("tradeNo"); 
		if(tradeNo == "${trade.tradeNo}"){
			$("#payDivTips").show(); 
		}
			$(".footer").hide();
			
		</script>
	</div>
</body>
</html>
