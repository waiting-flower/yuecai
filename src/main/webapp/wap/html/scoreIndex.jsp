<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>积分商城首页</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath}js/scoreIndexFun.js?v=160903" language="javascript" type="text/javascript"></script>

<style type="text/css">
.cate_list{
	font-size:13px;
	position:relative;
}
.cate_box_list{
	position:absolute;
	left:0; 
	top : 38px;  
	background:#e8e8f0;
	width:100%;
	z-index:11;   
	display:none; 
} 
.cate_box_list:before {
   	background: #e8e8f0;
    content: "";
    width: 15px; 
    height: 15px;
    display: inline-block;
    position: absolute;
    left: 40%;
    top: -6px;
    transform: rotate(45deg);
}
</style>
</head>
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	
	
	
	
	<div style="margin-bottom:55px;">
        <input type="hidden" value="${cate.id }" id="cateId">

        <div class="all-list-wrapper" style="top:0;"> 
            <div class="good-list-wrapper" style="padding-left:0px;">
                <div class="good-menu thin-bor-bottom">
                    <ul class="good-menu-list" id="ulOrderBy">
                        <li sorttype="10" class="current"><a href="javascript:;">综合</a>
                        </li> 
                        <li sorttype="20"><a href="javascript:;">销量</a>
                        </li>
                        <li sorttype="50"><a href="javascript:;">新品</a>
                        </li>
                        <li sorttype="30"><a href="javascript:;">积分</a><span class="i-wrap"><i class="up"></i><i class="down"></i></span>
                        </li>
                        <!--价值(由高到低30,由低到高31)--> 
                        <li class="isCate">
                        	<div class="cate_list"><em>选择分类</em>
                        	<div class="cate_box_list">
                        		<c:forEach items="${cateList }" var="item">
                        		<div class="cate_item" itemName="${item.name }" itemId="${item.id}">${item.name }</div>
                        		</c:forEach>
                        	</div>
                        	</div>
                        </li> 
                    </ul>   
                </div>   
                
				<div class="good-list-inner" style="padding-left:0px;"> 
					<div class="good-list-box" id="loadingPicBlock">
						
						<div class="goods_list"> 
							<ul id="ulGoodsList"> 
								<div class="search_prolist cols_2 type_twotitle" id="itemList">
								</div>
							</ul>  
						</div>
						<div id="divLoading" class="loading clearfix" style="display: none;">
							<b></b>正在加载
						</div> 
						<div id="noneTipDiv"></div>
					</div>
					
				</div>
				  
			</div>
    	</div>
    </div> 

	<div id="div_fastnav" class="fast-nav-wrapper">
			<ul class="fast-nav">
				<li id="li_top2" style="display:none;"><a href="javascript:;"><i
						class="nav-top"></i></a></li>
			</ul>
		</div> 
		
	<input id="hidPageType" type="hidden" value="4" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script>  
		$(".f_gift a").addClass("hover");
	</script>
</body>
</html>
