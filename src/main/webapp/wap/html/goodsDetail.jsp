<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>商品详情</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" /> 
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/fightGroup.css?v=151105" rel="stylesheet" type="text/css" />  
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" /> 
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath}js/goodsDetail.js?v=2323232" language="javascript" type="text/javascript"></script>
<style type="text/css">
 .ll_fadeIn { 
    opacity: 1;
}
.shareBox{
	height: 16px;
    line-height: 16px;
    background-color: #e93b3d;
    font-size: 12px;
    color: #fff;
    border-radius: 16px 0 0 16px;
    padding: 4px 10px;
    position: absolute;
    top: 20px;
    right: 0px;
    z-index: 11;   
}
.tongzhi_show_box{
   z-index: 20001;
  position: fixed;
  width: 80%;
  left: 10%;
  top: 40%;
  margin-top: -100px;
  background: #fff;
  padding: 20px 0px;
  border-radius: 5px;
}
.tongzhi_show_box .tz_title{
  width: 90%;
  margin: 0 auto;
  font-size: 16px;
  color: #fd5960;
  font-weight: bold;
  text-align: center;
}
.tongzhi_show_box .img_box{
  width: 100px;
  height: 100px;
  margin: 0 auto;
  margin-top: 10px;
  border: 10px solid #fd5960;
  padding: 10px;
}
.tongzhi_show_box .img_box image{
  width: 100px;
  height: 100px;
}
.tongzhi_show_box .tz_info{
  font-size: 12px;
  color: #454545;
  width: 90%;  
  margin: 0 auto;
  margin-top: 10px;
  line-height:18px;  
}
.tongzhi_show_box .tz_btn{
  width: 80%;
  height: 25px;
  line-height: 25px;
  border-radius: 5px;
  background: #fd5960;
  color: #fff;
  font-size: 13px;
  margin: 0 auto;
  margin-top: 10px;
  text-align: center;
  padding: 3px 0px;
}
.masktz{
    position: fixed;
    z-index: 20000;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: #000;
    opacity: .8;
}
</style>
</head> 
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	<script type="text/javascript">
	var imgList = "${goods.imgList}"; 
	</script>
	<input type="hidden" value="${goods.id }" id="goodsId">
	<input type="hidden" value="${goods.price }" id="hidPrice">
  
	<div id="shareRule" style="display:none;">
		<div class='tongzhi_show_box'>
		  <div class='tz_title'>积分规则说明</div>
		  <div class='tz_info'>  
		  	1：您可以通过转发商品的方式获取积分，同一个商品每天转发前3次有效，超过3次转发无积分哦<br>
		  	2：购买该商品获取积分，每次购买可以获取${store.buyGoodsScore }
		  	<c:if test="${empty store.buyGoodsScore  }">${cb.buyGoodsScore }</c:if>
		  	<br>  
		  	3：积分可以在积分商城兑换商品<br>  
		  	 
		  </div>
		</div>
		<div class='masktz' ></div>
	</div>
	<c:if test="${not empty cb.shareGoodsScore }">
	<div class="shareBox">
		分享赚积分 
	</div> 
	<input type="hidden" value="${cb.id }" id="cityButlerId">
	<input type="hidden" value="${store.id }" id="storeId">
	</c:if> 
	
	<!-- 产品图 -->
	<div class="a_slider">
		<div class="slider8" id="slider_img_box">
		<div class="loading clearfix">
			<b></b>正在加载
		</div>
		</div>
		<span id="slider_pager"></span>    
	</div>  
	<!-- 产品信息 --> 
	<div class="tuan_good" style="margin-bottom:0px;">
		<div class="tuan_good_tit type_twoline"><span>${goods.name }</span></div>
		<div class="tuan_good_desc">${goods.name2 }</div>
		<div class="tuan_good_price">
			<span class="tuan_good_price_tag">优惠价</span> <span
				class="tuan_good_price_new"> ¥<strong id="goodsPrice">${currSku.price }</strong> 
			</span> <span class="tuan_good_price_old"> 原价：<del>¥${goods.oldPrice }</del></span>
			<span class="tuan_good_price_old" style="color:#454545;margin-left:10px;"> 已售：${goods.showSaleNum }</span>
		</div> 
	</div>
	<div class="detail_gap" style=""></div> 
	<c:if test="${not empty serverList }">
	<!-- 商品服务开始 --> 
	<ul class="detail_serve" id="serviceArea" ptag="7001.1.24">
		<c:forEach items="${ serverList }" var="item">
		<li class="detail_serve_item ">${item.name }</li> 
		</c:forEach>
	</ul>
	<!-- 商品服务结束 --> 
	</c:if> 
	
	<ul class="detail_serve" id="choiceGoods" ptag="7001.1.24">
		<li class="goods_p_li"><span class="part-note-msg">已选</span>
		<div id="specDetailInfo" class="base-txt">
				&nbsp;&nbsp; <span class="amount">0件</span>
			</div></li>
	</ul>
	<div class="detail_gap" style=""></div>	
 
 	<div class="sku_container sku_container_on" id="skuCont">
		<div class="sku_wrap">
			<c:if test="${not empty paramsList }">
			<div id="propertyDiv">
				<c:forEach items="${ paramsList }" var="item">
					<div class="sku" id="sku1" ptag="7001.1.5">
						<h3>${item.paramsName }</h3>
						<div class="sku_list">
							<c:set var="string1" value="${item.parmasValues}" />
							<c:set value="${ fn:split(string1, ',') }" var="paramsValuesList" />
							<c:forEach items="${ paramsValuesList }" var="params" varStatus="status">
								<c:choose>
									<c:when test="${status.index == 0 }">
										<span class="option option_selected" paramsName="${item.paramsName }"
											itemName="${params}">${params }</span>
									</c:when>
									<c:otherwise>   
										<span class="option" paramsName="${item.paramsName }"
											itemName="${params}">${params }</span>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</div>
					</div>
				</c:forEach>
			</div>
			<input type="hidden" value="1" id="isHasParams">
			</c:if>
			<c:if test="${empty paramsList}">
			<input type="hidden" value="0" id="isHasParams">
			</c:if> 
			<div class="sku sku_num" id="skuNum" style="padding-top: 2px;">
				<h3>数量</h3>
				<div class="num_wrap">
					<span class="minus minus_disabled" id="btn_minus" ptag="7001.1.11"></span>
					<input class="num" id="buyNum" type="tel" value="1"> <span
						class="plus" id="btn_plus" ></span>
				</div>
			</div>
		</div> 
	</div>
	
	<div class="detail_gap" style=""></div>

	<div class="detail_row detail_row_cmt"
		onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?goodsComment&id=${goods.id}'"
		id="summaryEnter" style="background: #fff;">
		<h3 class="tit" id="summaryTitle">商品评价（<span class="num" id="evalNo2">0</span>）</h3>
		<i class="icon_promote" id="summaryEnterIco"></i>
		<p class="good" id="evalRateP">
			好评 <strong id="evalRate">${goods.niceComment*100}%</strong>
		</p> 
		<p class="count"></p>
	</div>     
	<p class="cmt_none_tips" id="evalNone" style="display:none;background:#fff;text-align:center;padding:30px 0px;font-size:13px;">暂无评价，欢迎您购买之后留下宝贵的评价</p> 
	<div class="detail_row detail_cmt" style="" id="commentList_div">
		<div class="cmt_list_wrap">
			<ul class="cmt_list" id="evalDet_main">
			</ul> 
		</div> 
	</div>
	
	<div class="detail_gap" style=""></div>
	<div class="detail_row detail_row_cmt"
		id="summaryEnter" style="background: #fff;">
		<h3 class="tit" id="summaryTitle">商品详情</h3>
	</div> 
	<div class="h5-1yyg-v1 marginB">
		<div id="divGoodsDesc" class="detailContent z-minheight" style="display: none;">
		${goods.info } 
		</div> 
	</div> 	
	    
	<div class="image_viewer" style="display:none;"> 
		<div class="imgViewTit" id="img_tit">1/0</div>
		<div class="inner" id="imageViewer" style="backface-visibility: hidden; transform: translateX(0%); transition: all 0.3s ease;">
		</div>  
	</div>
	
	<input type="hidden" value="${isColelct }" id="isCollect"> 
	<!-- foot bar --> 
	<div class="new_pro_footer"> 
		<div class="new_btn_group">
			<c:if test="${isCollect == '1' }">
			<a href="javascript:;" class="new_btn_item sc_btn act">
				<i class="fa fa-heart"></i>
				<span>已收藏</span>  
			</a>
			</c:if>
			<c:if test="${isCollect == '0' }">
			<a href="javascript:;" class="new_btn_item sc_btn">
				<i class="fa fa-heart-o"></i>
				<span>收藏</span>
			</a>
			</c:if> 
			<a href="${pageContext.request.contextPath}/shopWap.html?cart" class="new_btn_item cart_tip_term">
				<i class="fa fa-cart-plus" style="position:relative;"></i>
				购物车    
			</a>  
			<c:if test="${goods.store.cate.isSpecial == '0' }">  
			<a href="${pageContext.request.contextPath}/shopWap.html?cityShop&storeId=${goods.store.id}" class="new_btn_item">
				<i class="fa fa-dashboard"></i>
				店铺  
			</a>  
			</c:if>
			<c:if test="${goods.store.cate.isSpecial == '1' }">
			<a href="${pageContext.request.contextPath}/shopWap.html?storeDetail&id=${goods.store.id}" class="new_btn_item">
				<i class="fa fa-dashboard"></i>
				店铺 
			</a>
			</c:if>
			<button id="btn_kaituan" class="tuan_fixbtns_btn new_btn_item3">我要购买</button>  
		</div>
	</div> 

	<!-- 商品服务弹出层 -->
	<div id="popupServe" class="detail_serve_main">
		<div class="main">
			<div class="header">
				服务说明<i class="close" id="closeServe"></i>
			</div>
			<div class="body">
				<c:forEach items="${ serverList }" var="item" varStatus="status">
				<div class="section">
					<div class="detail_serve_main_tit ">${item.name }</div>
					<p class="detail_serve_main_content">${item.info }</p>
				</div> 
				</c:forEach>
			</div>
			<div class="mod_btns">
				<div class="mod_btn bg_2" id="btn_serve">确定</div>
			</div>
		</div>
	</div>
	<!-- 商品服务弹出层结束 -->
	
	<!-- 快速购买导航弹出层 -->
	<div class="tuan_layer tuan_layer_v2" id="pintuanLayer">
		<div class="tuan_layer_inner">
			<span class="tuan_layer_close" id="closeGoods"></span>
			<div class="tuan_layer_goods">
				<div class="good_cover">
					<img class="ll_fadeIn" id="mask_goodsImgUrl" src="${goods.imgUrl }"
						loaded="1" style="opacity: 1;">
				</div>
				<div class="good_info"> 
					<p class="price">    
						<span>优惠价</span>¥<strong id="mask_price">${currSku.price}</strong>
					</p>
					<p class="sku"> 
						已选<em id="tip_select"></em>
					</p> 
					<p class="sku">   
						库存<em id="tip_sku">${currSku.stockNum }</em>
					</p>
				</div>
			</div>
			<div class="tuan_layer_content">
				<c:forEach items="${ paramsList }" var="item">
				<div class="tuan_good_sku"> 
					<div class="sku_label">${item.paramsName }</div>
					<div class="sku_select">
						<c:set var="string1" value="${item.parmasValues}" />
						<c:set value="${ fn:split(string1, ',') }" var="paramsValuesList" />
						<c:forEach items="${ paramsValuesList }" var="params" varStatus="status">  
						<span class="option ${status.index == 0 ? 'selected' : ''}" paramsName="${item.paramsName }" itemName="${params }">${params }</span>
						</c:forEach> 
					</div>
				</div>
				</c:forEach>
				<div class="tuan_good_sku">
					<div class="sku_label">数量</div>
					<div class="num_wrap_v2">
						<span class="minus disable" id="btn_minus2"><i class="row"></i></span>
						<div class="text_wrap">
							<input type="tel" class="text" value="1" id="buyNum2">
						</div>
						<span class="plus" id="btn_plus2"><i class="row"></i><i class="col"></i></span>
					</div>
				</div>
			</div>
			<div id="btnToCard" class="btn color2">加入购物车</div>
			<div id="btnBuyGoods"   class="btn">立即购买</div>
		</div>
	</div>
	<!-- 快速购买导航弹出层结束 -->
	
	  
	<input id="currStockNum" type="hidden" value="${currSku.stockNum }" />
	<input id="currSkuId" type="hidden" value="${currSku.id }" />
	
	<input id="goodsType" type="hidden" value="${goods.goodsType }" />
	
	
	<input id="hidPageType" type="hidden" value="-1" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script>  
		$(".footer").hide(); 
		$(".f_personal a").addClass("hover");
	</script>
	
	<script> 
	/*
     * 智能机浏览器版本信息:
     *
     */
     var browser = {
         versions: function () {
             var u = navigator.userAgent, app = navigator.appVersion;
             return {//移动终端浏览器版本信息 
                 trident: u.indexOf('Trident') > -1, //IE内核
                 presto: u.indexOf('Presto') > -1, //opera内核
                 webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
                 gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
                 mobile: !!u.match(/AppleWebKit.*Mobile.*/) || !!u.match(/AppleWebKit/), //是否为移动终端
                 ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
                 android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
                 iPhone: u.indexOf('iPhone') > -1 || u.indexOf('Mac') > -1, //是否为iPhone或者QQHD浏览器
                 iPad: u.indexOf('iPad') > -1, //是否iPad
                 webApp: u.indexOf('Safari') == -1 //是否web应该程序，没有头部与底部
             };
         }(),
         language: (navigator.browserLanguage || navigator.language).toLowerCase()
     }
     var _swidth = 0;
     var _dwidth = 0;
     var _gheight = 0;
     var setZoomFun = function (isResize) {
         var _goodsdesc = $("#divGoodsDesc").show();
         var _hwidth = _goodsdesc.width();//$("header.g-header").width();
         if (_hwidth == _swidth) { return; }
         _swidth = _hwidth;//window.screen.width;
         if (_dwidth == 0) {
             _dwidth = $(document).width();
         }
         if (_gheight == 0) {
             _gheight = _goodsdesc.height();
         }
         if (!isResize) {
             _goodsdesc.find("img").each(function () {
             	var E = "src2";
                 var H = $(this).attr(E);
                 var src = $(this).attr("src");
                 
                 $(this).wrap('<a href="javascript:;" thref="' + src + '" class="js-smartPhoto2" data-caption="预览" data-id="预览" data-group="info">');
                 $(this).attr("src2", src);
                 $(this).attr("src","${skinPath}images/chuyilian.jpg").show();
                 $(this).css({"width":"100%","height":"auto"});  

             });
         }
         var _zoom = parseFloat(_swidth / _dwidth);
         console.log(_zoom); 
         if (_zoom >= 1 || _zoom <= 0) {
             return;
         }
     } 

     $(document).ready(function () {
         setZoomFun(false);
         initBigPic();  
     });

     $(window).resize(function () {
         setZoomFun(true);
     });

		
     function initBigPic(){
    	 //以下代码为点击图片查看大图代码
    	var currIndex = 0;
        var allImgArr = $("#divGoodsDesc").find(".js-smartPhoto2");
    	for(var i = 0; i < allImgArr.length; i++){
    		var ao = $(allImgArr[i]);
    		var thref = ao.attr("thref");
    		var left = i *100;   
    		$("#imageViewer").append('<img style="left:' + left + '%;" src="' + thref + '" class="ll_fadeIn" >');
    	}
    	$("#divGoodsDesc").find(".js-smartPhoto2").click(function(){
    		var index = $("#divGoodsDesc").find(".js-smartPhoto2").index(this); 
    		console.log("当前索引" + index); 
    		currIndex = index; 
    		var left = 0 - index *100;
    		$(".image_viewer").show();       
    		$("#img_tit").html((index + 1) + "/" + allImgArr.length); 
    		$(".inner").css("transform","translateX(" + left + "%)");
    		$(".a_header").hide();
    	}); 
    	 
    	$(".image_viewer").on("touchstart", function(e) {
    		console.log(111); 
    	    // 判断默认行为是否可以被禁用
    	    if (e.cancelable) {
    	        // 判断默认行为是否已经被禁用
    	        if (!e.defaultPrevented) {
    	            e.preventDefault();
    	        }
    	    }   
    	    startX = e.originalEvent.changedTouches[0].pageX,
    	    startY = e.originalEvent.changedTouches[0].pageY;
    	});
    	$(".image_viewer").on("touchend", function(e) {         
    	    // 判断默认行为是否可以被禁用
    	    if (e.cancelable) {
    	        // 判断默认行为是否已经被禁用
    	        if (!e.defaultPrevented) {
    	            e.preventDefault(); 
    	        }
    	    }               
    	    moveEndX = e.originalEvent.changedTouches[0].pageX,
    	    moveEndY = e.originalEvent.changedTouches[0].pageY,
    	    X = moveEndX - startX,
    	    Y = moveEndY - startY;
    	    //左滑
    	    if ( X > 0 ) {
    	        console.log('左滑');  
    	        
    	        if(currIndex <= 0){ 
    	        	return false;
    	        }
    			currIndex--;
    	        var left = 0 - currIndex *100;
    			$("#img_tit").html((currIndex + 1) + "/" + allImgArr.length); 
    			$(".inner").css("transform","translateX(" + left + "%)");
    	    }
    	    //右滑
    	    else if ( X < 0 ) {
    	    	console.log('右滑');    
    	    	 
    	    	if(currIndex + 1 >= allImgArr.length){
    	    		return false;
    	    	}
    			currIndex++; 
    	    	var left = 0 - currIndex *100; 
    	    	$("#img_tit").html((currIndex + 1) + "/" + allImgArr.length); 
    			$(".inner").css("transform","translateX(" + left + "%)");
    	    }
    	    //下滑
    	    else if ( Y > 0) {
    	        //alert('下滑');    
    	    }
    	    //上滑
    	    else if ( Y < 0 ) {
    	        //alert('上滑');     
    	    } 
    	    //单击
    	    else{ 
    	    	console.log('单击');
    	    	$(".image_viewer").hide();     
    			$(".a_header").show();
    	    }
    	});
    }
	</script>   
</body>
</html>
