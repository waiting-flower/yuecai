<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>我的订单列表</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />

<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/member.css?v=151209" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath }js/userOrderListFun.js" language="javascript" type="text/javascript"></script>
</head> 
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	<input name="hidUserID" type="hidden" id="hidUserID" value="${hidUserID}" />
    <input name="hidType" type="hidden" id="hidType" value="${type}" />
	<div class="marginB">
			<div class="weui-navbar"  
				style="height: 44px;float:left;position:fixed;top:0px;left:0px; background: #fff;">
				<a
					class="weui-navbar__item proinfo-tab-tit font-14"
					tar="allOrder" toId="-1">全部</a> <a
					class="weui-navbar__item proinfo-tab-tit font-14" tar="order1" toId="0">待付款</a>
				<a class="weui-navbar__item proinfo-tab-tit font-14" tar="order2" toId="1">待发货</a>
				<a class="weui-navbar__item proinfo-tab-tit font-14" tar="order3" toId="2">待收货</a>
				<a class="weui-navbar__item proinfo-tab-tit font-14" tar="order4" toId="3">待评价</a>
			</div> 
			<div id="allOrder" class="my_order_list" style="margin-top:44px;">
				  
			</div>
			<div id="divLoading" class="loading clearfix">
				<b></b>正在加载 
			</div>
		</div> 
		
	
	<input id="hidPageType" type="hidden" value="4" />
	<input id="hidIsHttps" type="hidden" value="1" />
	<input id="hidSiteVer" type="hidden" value="v47" />
	<input id="hidWxDomain" type="hidden" value="" />
	<input id="hidOpenID" type="hidden" value="" />
	<jsp:include page="../html/footer.jsp"></jsp:include>

	<script>  
		$(".f_personal a").addClass("hover");
	</script>
</body>
</html>
