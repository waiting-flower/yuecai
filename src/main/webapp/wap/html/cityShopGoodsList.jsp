<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>${cate.name}商品列表</title> 
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
    <link href="${skinPath}css/cityShop.css?v=123330522" rel="stylesheet" type="text/css" />

<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath}js/cityShopGoodsListFun.js?v=160903" language="javascript" type="text/javascript"></script>
</head>     
<body fnav="0"  class="g-acc-bg">
	<div style="margin-bottom:55px;">
        <input type="hidden" value="${cateId}" id="cateId">
        <input type="hidden" value="${modulesId}" id="modulesId">

        <div class="all-list-wrapper" style="top:0;"> 
            <div class="good-list-wrapper" style="padding-left:0px;background:#f8f8f8;">
                <div class="good-menu thin-bor-bottom">
                    <ul class="good-menu-list" id="ulOrderBy">
                        <li sorttype="10" class="current"><a href="javascript:;">综合</a>
                        </li> 
                        <li sorttype="20"><a href="javascript:;">销量</a>
                        </li>
                        <li sorttype="50"><a href="javascript:;">新品</a>
                        </li>
                        <li sorttype="30"><a href="javascript:;">价格</a><span class="i-wrap"><i class="up"></i><i class="down"></i></span>
                        </li>
                        <!--价值(由高到低30,由低到高31)-->
                    </ul>   
                </div>   
                
				<div class="good-list-inner" style="padding-left:0px;"> 
					<div class="good-list-box" id="loadingPicBlock">
						
						<div class="goods_list"> 
							<ul id="ulGoodsList"> 
								<div class="menu_goods_list_box" id="itemList">
								</div>
							</ul>   
						</div>
						<div id="divLoading" class="loading clearfix" style="display: none;">
							<b></b>正在加载
						</div> 
						<div id="noneTipDiv"></div>
					</div>
					
				</div>
				  
			</div>
    	</div>
    </div> 

	<div id="div_fastnav" class="fast-nav-wrapper">
			<ul class="fast-nav">
				<li id="li_top2" style="display:none;"><a href="javascript:;"><i
						class="nav-top"></i></a></li>
			</ul>
		</div> 
	 
	 
	 <div class="h30" style="width:100%;height:50px;"></div>
	 
	<div class='goods_bot_bar'>
	  <div class='item item1' >
	  <a  href="${pageContext.request.contextPath}/shopWap.html?cityShop&cityButlerId=${cityButlerId}">
	    <img  src='${skinPath }images/tabbar_1a.png'>
	    <div class='name'>首页</div></a> 
	  </div> 
	  <div class='item item1'>
	  <a  href="${pageContext.request.contextPath}/shopWap.html?cityCate&cityButlerId=${cityButlerId}">
	    <img  src='${skinPath }images/tabbar_2a.png'>
	    <div class='name'>分类</div>
	    </a>
	  </div>  
	  <div class='item item1'>  
	    <a href="${pageContext.request.contextPath}/shopWap.html?cart">
	    <img  src='${skinPath }images/tabbar_4a.png'>
	    <div class='name'>购物车<span class="em" style="color:#fff;display:none;"></span></div>
	    </a>
	  </div>
	  <div class='item item1'>
	  	<a href="${pageContext.request.contextPath}/shopWap.html?userCenter">
	    <img  src='${skinPath }images/tabbar_5a.png'>
	    <div class='name'>我的</div> 
	    </a>
	  </div>
	</div>
	 
	<input id="hidPageType" type="hidden" value="-1" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script> 
	$(".footer").remove(); 
	</script> 
</body>
</html>
