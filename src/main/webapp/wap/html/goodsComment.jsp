<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>商品评论</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />

<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/member.css?v=151209" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath}js/goodsCommentListFun.js?v=160901" language="javascript" type="text/javascript"></script>
<style type="text/css">
.detail_row_cmt p.good {
    left: 50px; 
    color: #e4393c;
}
</style>
</head>
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	
	
	<input type="hidden" value="${goods.id}" id="goodsId">

	<div class="detail_extra">
		<div class="detail_row detail_row_cmt" id="summaryEnter"
			ptag="7001.1.27" style="" hashval="summary">
			<h3 class="tit" id="summaryTitle">评价</h3>
			<i class="icon_promote" id="summaryEnterIco"></i>
			<p class="good" id="evalRateP">
				好评 <strong id="evalRate">${goods.niceComment*100}%</strong>
			</p>
			<p class="count">
				共 <span class="num" id="evalNo2">0</span> 条
			</p> 
		</div>
		<p class="cmt_none_tips" id="evalNone" style="display: none;text-align:center;padding:30px 0px;font-size:13px;">暂无评价，欢迎您购买之后留下宝贵的评价</p> 
		<div class="detail_row detail_cmt" style="" id="commentList_div">
			 
			<div class="cmt_list_wrap">
				<ul class="cmt_list" id="evalDet_main">
				</ul>
			</div>
		</div>
		
	<input id="hidPageType" type="hidden" value="4" />
	<input id="hidIsHttps" type="hidden" value="1" />
	<input id="hidSiteVer" type="hidden" value="v47" />
	<input id="hidWxDomain" type="hidden" value="" />
	<input id="hidOpenID" type="hidden" value="" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script> 
	</script>
</body>
</html>
