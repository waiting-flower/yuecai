<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>订单取消</title> 
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath }js/userOrderCancleFun.js" language="javascript" type="text/javascript"></script>
</head>
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">

	<div id="content">
		<div class="order_selectList">
			<h3>
				请选择取消订单的原因（<em>必选</em>）：
			</h3> 
			<ul class="cancle_reason_list">
				<li reasonid="订单发货太慢了" class="">订单发货太慢了</li>
				<li reasonid="操作有误（商品、地址等选错）" class="">操作有误（商品、地址等选错）</li>
				<li reasonid="重复下单/误下单" class="">重复下单/误下单</li>
				<li reasonid="其他渠道价格更低" class="">其他渠道价格更低</li>
				<li reasonid="该商品商城降价了" class="">该商品商城降价了</li>
				<li reasonid="不想买了" class="">不想买了</li> 
				<li reasonid="商品无货" class="">商品无货</li>
				<li reasonid="其他原因" class="">其他原因</li>
			</ul>
		</div>    
		<div class="mod_btns" style="margin: 15px 10px;">  
			<a href="javascript:void(0);" id="btnCancle" class="mod_btn bg_4 cancleJs" style="border: 1px solid #ddd;">取消</a> <a
				href="javascript:void(0);" id="btnConfirm" class="mod_btn bg_1 showJs">确认</a>
		</div> 
	</div>
  
	<input id="hidOrderId" type="hidden" value="${a.id }" />
	<input id="hidPageType" type="hidden" value="4" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script> 
		$(".f_personal a").addClass("hover");
	</script>
</body>
</html>
