<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>积分商品详情</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" /> 
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/fightGroup.css?v=151105" rel="stylesheet" type="text/css" />  
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" /> 
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath}js/scoreGoodsDetail.js?v=2323232" language="javascript" type="text/javascript"></script>
<style type="text/css">
 .ll_fadeIn { 
    opacity: 1;
}
</style>
</head> 
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	<script type="text/javascript">
	var imgList = "${goods.imgList}"; 
	</script>
	<input type="hidden" value="${goods.id }" id="goodsId">
	<input type="hidden" value="${goods.score }" id="hidScore">
 
	
	
	<!-- 产品图 -->
	<div class="a_slider">
		<div class="slider8" id="slider_img_box">
		<div class="loading clearfix">
			<b></b>正在加载
		</div>
		</div>
		<span id="slider_pager"></span>    
	</div>  
	<!-- 产品信息 --> 
	<div class="tuan_good" style="margin-bottom:0px;">
		<div class="tuan_good_tit type_twoline"><span>${goods.name }</span></div>
		<div class="tuan_good_price">
			<span class="tuan_good_price_tag">所需积分</span> <span
				class="tuan_good_price_new"> <strong id="goodsPrice">${goods.score }</strong> 
			</span>
		</div>
	</div>
	<div class="detail_gap" style=""></div> 
	<div class="sku_container sku_container_on" id="skuCont">
		<div class="sku_wrap">
			<div class="sku sku_num" id="skuNum" style="padding-top: 2px;">
				<h3>数量</h3>
				<div class="num_wrap">
					<span class="minus minus_disabled" id="btn_minus" ptag="7001.1.11"></span>
					<input class="num" id="buyNum" type="tel" value="1"> <span
						class="plus" id="btn_plus" ></span>
				</div>
			</div>
		</div> 
	</div>
	<div class="detail_gap" style=""></div>  
	<div class="detail_row detail_row_cmt"
		id="summaryEnter" style="background: #fff;">
		<h3 class="tit" id="summaryTitle">商品详情</h3>
	</div> 
	<div class="h5-1yyg-v1 marginB">
		<div id="divGoodsDesc" class="detailContent z-minheight" style="display: none;    background: #fff;
    width: 94%;
    padding: 10px 3%;"> 
		${goods.info } 
		</div> 
	</div> 	
	    
	<div class="image_viewer" style="display:none;"> 
		<div class="imgViewTit" id="img_tit">1/0</div>
		<div class="inner" id="imageViewer" style="backface-visibility: hidden; transform: translateX(0%); transition: all 0.3s ease;">
		</div>  
	</div>
	
	<!-- foot bar --> 
	<div class="new_pro_footer"> 
		<div class="new_btn_group">
			<button id="btn_kaituan" class="tuan_fixbtns_btn new_btn_item3">立即兑换</button>  
		</div>
	</div> 
	
	<input id="hidPageType" type="hidden" value="-1" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script>  
		$(".footer").hide(); 
		$(".f_personal a").addClass("hover");
	</script>
	
	<script> 
	/*
     * 智能机浏览器版本信息:
     *
     */
     var browser = {
         versions: function () {
             var u = navigator.userAgent, app = navigator.appVersion;
             return {//移动终端浏览器版本信息 
                 trident: u.indexOf('Trident') > -1, //IE内核
                 presto: u.indexOf('Presto') > -1, //opera内核
                 webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
                 gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
                 mobile: !!u.match(/AppleWebKit.*Mobile.*/) || !!u.match(/AppleWebKit/), //是否为移动终端
                 ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
                 android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
                 iPhone: u.indexOf('iPhone') > -1 || u.indexOf('Mac') > -1, //是否为iPhone或者QQHD浏览器
                 iPad: u.indexOf('iPad') > -1, //是否iPad
                 webApp: u.indexOf('Safari') == -1 //是否web应该程序，没有头部与底部
             };
         }(),
         language: (navigator.browserLanguage || navigator.language).toLowerCase()
     }
     var _swidth = 0;
     var _dwidth = 0;
     var _gheight = 0;
     var setZoomFun = function (isResize) {
         var _goodsdesc = $("#divGoodsDesc").show();
         var _hwidth = _goodsdesc.width();//$("header.g-header").width();
         if (_hwidth == _swidth) { return; }
         _swidth = _hwidth;//window.screen.width;
         if (_dwidth == 0) {
             _dwidth = $(document).width();
         }
         if (_gheight == 0) {
             _gheight = _goodsdesc.height();
         }
         if (!isResize) {
             _goodsdesc.find("img").each(function () {
             	var E = "src2";
                 var H = $(this).attr(E);
                 var src = $(this).attr("src");
                 
                 $(this).wrap('<a href="javascript:;" thref="' + src + '" class="js-smartPhoto2" data-caption="预览" data-id="预览" data-group="info">');
                 $(this).attr("src2", src);
                 $(this).attr("src","${skinPath}images/chuyilian.jpg").show();
                 $(this).css({"width":"100%","height":"auto"});  

             });
         }
         var _zoom = parseFloat(_swidth / _dwidth);
         console.log(_zoom); 
         if (_zoom >= 1 || _zoom <= 0) {
             return;
         }
     } 

     $(document).ready(function () {
         setZoomFun(false);
         initBigPic();  
     });

     $(window).resize(function () {
         setZoomFun(true);
     });

		
     function initBigPic(){
    	 //以下代码为点击图片查看大图代码
    	var currIndex = 0;
        var allImgArr = $("#divGoodsDesc").find(".js-smartPhoto2");
    	for(var i = 0; i < allImgArr.length; i++){
    		var ao = $(allImgArr[i]);
    		var thref = ao.attr("thref");
    		var left = i *100;   
    		$("#imageViewer").append('<img style="left:' + left + '%;" src="' + thref + '" class="ll_fadeIn" >');
    	}
    	$("#divGoodsDesc").find(".js-smartPhoto2").click(function(){
    		var index = $("#divGoodsDesc").find(".js-smartPhoto2").index(this); 
    		console.log("当前索引" + index); 
    		currIndex = index; 
    		var left = 0 - index *100;
    		$(".image_viewer").show();       
    		$("#img_tit").html((index + 1) + "/" + allImgArr.length); 
    		$(".inner").css("transform","translateX(" + left + "%)");
    		$(".a_header").hide();
    	}); 
    	 
    	$(".image_viewer").on("touchstart", function(e) {
    		console.log(111); 
    	    // 判断默认行为是否可以被禁用
    	    if (e.cancelable) {
    	        // 判断默认行为是否已经被禁用
    	        if (!e.defaultPrevented) {
    	            e.preventDefault();
    	        }
    	    }   
    	    startX = e.originalEvent.changedTouches[0].pageX,
    	    startY = e.originalEvent.changedTouches[0].pageY;
    	});
    	$(".image_viewer").on("touchend", function(e) {         
    	    // 判断默认行为是否可以被禁用
    	    if (e.cancelable) {
    	        // 判断默认行为是否已经被禁用
    	        if (!e.defaultPrevented) {
    	            e.preventDefault(); 
    	        }
    	    }               
    	    moveEndX = e.originalEvent.changedTouches[0].pageX,
    	    moveEndY = e.originalEvent.changedTouches[0].pageY,
    	    X = moveEndX - startX,
    	    Y = moveEndY - startY;
    	    //左滑
    	    if ( X > 0 ) {
    	        console.log('左滑');  
    	        
    	        if(currIndex <= 0){ 
    	        	return false;
    	        }
    			currIndex--;
    	        var left = 0 - currIndex *100;
    			$("#img_tit").html((currIndex + 1) + "/" + allImgArr.length); 
    			$(".inner").css("transform","translateX(" + left + "%)");
    	    }
    	    //右滑
    	    else if ( X < 0 ) {
    	    	console.log('右滑');    
    	    	 
    	    	if(currIndex + 1 >= allImgArr.length){
    	    		return false;
    	    	}
    			currIndex++; 
    	    	var left = 0 - currIndex *100; 
    	    	$("#img_tit").html((currIndex + 1) + "/" + allImgArr.length); 
    			$(".inner").css("transform","translateX(" + left + "%)");
    	    }
    	    //下滑
    	    else if ( Y > 0) {
    	        //alert('下滑');    
    	    }
    	    //上滑
    	    else if ( Y < 0 ) {
    	        //alert('上滑');     
    	    } 
    	    //单击
    	    else{ 
    	    	console.log('单击');
    	    	$(".image_viewer").hide();     
    			$(".a_header").show();
    	    }
    	});
    }
	</script>   
</body>
</html>
