<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>登录</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet"
	type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet"
	type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet"
	type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522"
	rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet"
	type="text/css" /> 
	<link href="${skinPath }css/login2.css?v=170522" rel="stylesheet"
	type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript"
	type="text/javascript"></script>
	<script id="pageJS" data="${skinPath }js/loginFun.js" language="javascript" type="text/javascript"></script>
	
</head>
<body fnav="0" id="loadingPicBlock" class="g-acc-bg">


	<div class="con">
		<section class="user-center user-login margin-lr p-r">
			<div class="user-login-header-box">
				<div class="user-login-header">
					<div class="user-login-header-img">
						<div class="img-commom" style="height: 80px;">
							<img style="border-radius: 100%"
								src="${skinPath }images/get_avatar.png" class="img-height">
						</div>
					</div>
				</div>
			</div>
			<div class="b-color-f  user-login-ul user-login-ul-after">
				<div class="text-all dis-box j-text-all login-li "
					name="usernamediv">  
					<i class="fa fa-user"></i>
					<div class="box-flex input-text">
						<input class="j-input-text" name="userName" id="userName"
							 type="text" 
							placeholder="请输入您注册的手机号码"> <i
							class="iconfont icon-guanbi1 close-common j-is-null"></i>
					</div>
				</div>
				<div class="text-all dis-box j-text-all login-li m-top10"
					name="passworddiv">  
					<i class="fa fa-key"></i>
					<div class="box-flex input-text">
						<input class="j-input-text" id="password" name="password" type="password" placeholder="请输入6-20位登录密码"> <i
							class="iconfont icon-guanbi1 close-common j-is-null"></i>
					</div> 
				</div>
			</div> 
			<button type="button" id="btnLogin" class="btn-submit min-two-btn br-5">立即登录</button>
		 
		<div class="dis-box user-login-list m-top10">
			<div class="box-flex">
				<a class="list-password f-04" 
					href="${pageContext.request.contextPath}/shopWap.html?findPass">忘记密码</a>
			</div>
			<div class="box-flex">
				<a class="list-new f-04"
					href="${pageContext.request.contextPath}/shopWap.html?reg">新用户注册</a>
			</div>
		</div>
		</section>
	</div>
	<input id="hidPageType" type="hidden" value="-1" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script>
		$(".footer").hide();
	</script>
</body>
</html>
