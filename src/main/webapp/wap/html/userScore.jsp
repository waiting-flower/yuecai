<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>我的积分</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" /><meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath }css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/member.css?v=151106" rel="stylesheet" type="text/css" />
<script src="${skinPath }js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath }js/userScoreFun.js?v=161108" language="javascript" type="text/javascript"></script></head>
<body fnav="1">
    
    <div id="wrapper" class="wrapper marginB">
        <div class="blessings-title colorbbb"> 
            <p class="gray9">可用<em class="orange">${user.score }</em>积分</p>积分可以通过商城购买商品获得，可换取很多礼品
        </div> 
        <div class="blessings-con">
            <dl id="dlList">
                <dt><i class="gray6">积分明细</i><b></b></dt>
            </dl>
            <div class="loading clearfix g-acc-bg"><b></b>正在加载</div>
        </div>
        
<input id="hidPageType" type="hidden" value="-1" />
<input id="hidIsHttps" type="hidden" value="1" />
<input id="hidSiteVer" type="hidden" value="v47" /> 
<input id="hidOpenID" type="hidden" value=""/>
<jsp:include page="../html/footer.jsp"></jsp:include>
    </div>
</body>
</html>
