<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>公告列表</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath}js/noticeListFun.js?v=161012" language="javascript" type="text/javascript"></script>
</head>
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	<div class="quan_navbar">
		<div class="quan_navbar_inner">
			<a nt="0" href="javascript:;" class="quan_navbar_item cur"><span>全部</span></a>
			<a nt="1" href="javascript:;" class="quan_navbar_item"><span>公告</span></a> 
			<a nt="2" href="javascript:;" class="quan_navbar_item"><span>政策</span></a>
			<a nt="3" href="javascript:;" class="quan_navbar_item"><span>规范</span></a>
		</div>
	</div>   
	<div class="marginB" id="loadingPicBlock" style="width:94%;padding:10px 3%;background:#fff;">
		<div class="relatebox-articles">
        <ul id="ul_body">
        	
			<!-- 
			<li>
                <a href="/article.html?uc_param_str=frdnsnpfvecpntnwprdssskt&amp;from=relate&amp;wm_aid=ef1b5f244f5a41b38116db10a51c2305&amp;wm_id=61ed073d1eda41248861e687187c099d">
            		<div class="pages-article-relate_item-content">
            			<div class="pages-article-relate_image bigImgContainer article-img"><img class="galleryLink" uc-image-reader_state="disabled" width="110" height="82" src="http://image.uc.cn/o/wemedia/s/upload/2017/8147568bb30ce67c0ba057c72aea147ex800x451x76.jpeg;,3,jpegx;3,x164,;6,C-C,220x164"></div>
            			<div class="relatebox-articleinfo">
            				<h4 id="pages-article-relate_item-title" class="pages-article-relate_item-title">英国一劫匪因太渴望得到钱, 用挖掘机偷走ATM机</h4>
            				<div class="relatebox-articleinfo-desc">
                                <span id="pages-article-relate_item-wmname">
                                	<i class="fa fa-eye"></i>&nbsp;&nbsp;444
                                </span>
                                <span class="pages-article-relate_item-wmname">2017-05-14 15:22:07</span>
            				</div>
            			</div>
                  <div class="pages-article-relate_item-border-container">
                    <div id="pages-article-relate_item-border" class="pages-article-relate_item-border"></div>
                  </div>
            		</div>
            	</a>
            </li> 			
			 -->            
        </ul>
        <div id="divLoading" class="loading clearfix">
			<b></b>正在加载
		</div> 
    </div>
	
	
	
	<input id="hidPageType" type="hidden" value="4" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script> 
		$(".f_personal a").addClass("hover");
	</script>
</body>
</html>
