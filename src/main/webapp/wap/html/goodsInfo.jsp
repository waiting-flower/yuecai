<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>图文详情</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
</head>
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	
	<div class="h5-1yyg-v1 marginB">
	        <div id="divGoodsDesc" class="detailContent z-minheight" style="display: none;">
	            ${goods.info } 
	        </div>
	</div> 	
	    
	<div class="image_viewer" style="display:none;"> 
		<div class="imgViewTit" id="img_tit">1/0</div>
		<div class="inner" id="imageViewer" style="backface-visibility: hidden; transform: translateX(0%); transition: all 0.3s ease;">
		</div>  
	</div>
	 
	
	<input id="hidPageType" type="hidden" value="4" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script> 
	</script>
	<script> 
	/*
     * 智能机浏览器版本信息:
     *
     */
     var browser = {
         versions: function () {
             var u = navigator.userAgent, app = navigator.appVersion;
             return {//移动终端浏览器版本信息 
                 trident: u.indexOf('Trident') > -1, //IE内核
                 presto: u.indexOf('Presto') > -1, //opera内核
                 webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
                 gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
                 mobile: !!u.match(/AppleWebKit.*Mobile.*/) || !!u.match(/AppleWebKit/), //是否为移动终端
                 ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
                 android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
                 iPhone: u.indexOf('iPhone') > -1 || u.indexOf('Mac') > -1, //是否为iPhone或者QQHD浏览器
                 iPad: u.indexOf('iPad') > -1, //是否iPad
                 webApp: u.indexOf('Safari') == -1 //是否web应该程序，没有头部与底部
             };
         }(),
         language: (navigator.browserLanguage || navigator.language).toLowerCase()
     }
     var _swidth = 0;
     var _dwidth = 0;
     var _gheight = 0;
     var setZoomFun = function (isResize) {
         var _goodsdesc = $("#divGoodsDesc").show();
         var _hwidth = _goodsdesc.width();//$("header.g-header").width();
         if (_hwidth == _swidth) { return; }
         _swidth = _hwidth;//window.screen.width;
         if (_dwidth == 0) {
             _dwidth = $(document).width();
         }
         if (_gheight == 0) {
             _gheight = _goodsdesc.height();
         }
         if (!isResize) {
             _goodsdesc.find("img").each(function () {
             	var E = "src2";
                 var H = $(this).attr(E);
                 var src = $(this).attr("src");
                 
                 $(this).wrap('<a href="javascript:;" thref="' + src + '" class="js-smartPhoto2" data-caption="预览" data-id="预览" data-group="info">');
                 $(this).attr("src2", src);
                 $(this).attr("src","${skinPath}images/chuyilian.jpg").show();
                 $(this).css({"width":"100%","height":"auto"});  

             });
         }
         var _zoom = parseFloat(_swidth / _dwidth);
         console.log(_zoom); 
         if (_zoom >= 1 || _zoom <= 0) {
             return;
         }
         // document.title = _zoom;
         if (browser.versions.ios || browser.versions.iPhone || browser.versions.iPad) {
             _goodsdesc.css("-webkit-transform-origin", "left top");
             _goodsdesc.css("-moz-transform-origin", "left top");
             _goodsdesc.css("-o-transform-origin", "left top");

             _goodsdesc.css("-webkit-transform", "scale(" + _zoom + ")");
             _goodsdesc.css("-moz-transform", "scale(" + _zoom + ")");
             _goodsdesc.css("-o-transform", "scale(" + _zoom + ")");

             _goodsdesc.css("height", _gheight * _zoom + "px");

         } else {
         	 _goodsdesc.css("zoom", _zoom);
         }
     }

     $(document).ready(function () {
         setZoomFun(false);
         initBigPic();  
     });

     $(window).resize(function () {
         setZoomFun(true);
     });

		
     function initBigPic(){
    	 //以下代码为点击图片查看大图代码
    	var currIndex = 0;
        var allImgArr = $("#divGoodsDesc").find(".js-smartPhoto2");
    	for(var i = 0; i < allImgArr.length; i++){
    		var ao = $(allImgArr[i]);
    		var thref = ao.attr("thref");
    		var left = i *100;   
    		$("#imageViewer").append('<img style="left:' + left + '%;" src="' + thref + '" class="ll_fadeIn" >');
    	}
    	$("#divGoodsDesc").find(".js-smartPhoto2").click(function(){
    		var index = $("#divGoodsDesc").find(".js-smartPhoto2").index(this); 
    		console.log("当前索引" + index); 
    		currIndex = index; 
    		var left = 0 - index *100;
    		$(".image_viewer").show();       
    		$("#img_tit").html((index + 1) + "/" + allImgArr.length); 
    		$(".inner").css("transform","translateX(" + left + "%)");
    		$(".a_header").hide();
    	}); 
    	 
    	$(".image_viewer").on("touchstart", function(e) {
    	    // 判断默认行为是否可以被禁用
    	    if (e.cancelable) {
    	        // 判断默认行为是否已经被禁用
    	        if (!e.defaultPrevented) {
    	            e.preventDefault();
    	        }
    	    }   
    	    startX = e.originalEvent.changedTouches[0].pageX,
    	    startY = e.originalEvent.changedTouches[0].pageY;
    	});
    	$(".image_viewer").on("touchend", function(e) {         
    	    // 判断默认行为是否可以被禁用
    	    if (e.cancelable) {
    	        // 判断默认行为是否已经被禁用
    	        if (!e.defaultPrevented) {
    	            e.preventDefault(); 
    	        }
    	    }               
    	    moveEndX = e.originalEvent.changedTouches[0].pageX,
    	    moveEndY = e.originalEvent.changedTouches[0].pageY,
    	    X = moveEndX - startX,
    	    Y = moveEndY - startY;
    	    //左滑
    	    if ( X > 0 ) {
    	        console.log('左滑');  
    	        
    	        if(currIndex <= 0){ 
    	        	return false;
    	        }
    			currIndex--;
    	        var left = 0 - currIndex *100;
    			$("#img_tit").html((currIndex + 1) + "/" + allImgArr.length); 
    			$(".inner").css("transform","translateX(" + left + "%)");
    	    }
    	    //右滑
    	    else if ( X < 0 ) {
    	    	console.log('右滑');    
    	    	 
    	    	if(currIndex + 1 >= allImgArr.length){
    	    		return false;
    	    	}
    			currIndex++; 
    	    	var left = 0 - currIndex *100; 
    	    	$("#img_tit").html((currIndex + 1) + "/" + allImgArr.length); 
    			$(".inner").css("transform","translateX(" + left + "%)");
    	    }
    	    //下滑
    	    else if ( Y > 0) {
    	        //alert('下滑');    
    	    }
    	    //上滑
    	    else if ( Y < 0 ) {
    	        //alert('上滑');     
    	    } 
    	    //单击
    	    else{ 
    	    	console.log('单击');
    	    	$(".image_viewer").hide();     
    			$(".a_header").show();
    	    }
    	});
    }
	</script> 
</body>
</html>
