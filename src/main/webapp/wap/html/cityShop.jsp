<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>  
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" name="viewport" />
    <meta content="telephone=no" name="format-detection" />
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
    <link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
    <link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
    <link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
    <link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
    <link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
    <link href="${skinPath }css/swiper.min.css?v=170522" rel="stylesheet" type="text/css" />
   	<link href="${skinPath}css/cityShop.css?v=173330522" rel="stylesheet" type="text/css" />
    <script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
    <script src="${skinPath}js/swiper.min.js" language="javascript" type="text/javascript"></script>
    <script id="pageJS" data="${skinPath}js/cityShopFun.js?v=161012" language="javascript" type="text/javascript"></script>
    <style type="text/css">
    .swiper-slide {
        background-position: center;
        background-size: cover;
        width: 70%;
    }      
    </style>
<title>商城首页</title>  
</head>   
<body fnav="1" class="g-acc-bg" style="background:#fff;"> 
	
	<div class="city_shop_search_box">
		<div class="c_s_search">
			<div class="cs_input_box">
					<i class="fa fa-search"></i>
					<input type="text" id="city_search_key" placeHolder="搜索您喜欢的商品名称">
				</div>
				<div class="cs_search_btn" id="city_search_btn">搜索</div> 
		</div>
	</div>
	  
	<!-- 轮播图 -->
	<div class="swiper-container swiper-container-banner">
        <div class="swiper-wrapper">
        	<c:forEach items="${bannerList }" var="item">
            <div class="swiper-slide">
            	<a href="${ietm.href }">
            		<img class="banner_img" src="${item.imgUrl }">
            	</a>
            </div> 
            </c:forEach> 
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div>
	
	<c:if test="${not empty menuList1 }">
	<div class='head_menu'> 
		<c:forEach items="${menuList1 }" var="item">
		<div class='menu_icon'   
			onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?cityMenu&menuId=${item.id}&cityButlerId=${cityButlerId }'"
			>
			<div class="img_icon_box">
				<img src='${item.imgUrl}'> 
			</div> 
			<span>${item.name }</span>
		</div>
		</c:forEach>
	</div>
	</c:if>
	 
	<c:if test="${not empty menuList2 }">
	<div class='head_menu' > 
		<c:forEach items="${menuList2 }" var="item">
		<div class='menu_icon'  
			onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?cityMenu&menuId=${item.id}&cityButlerId=${cityButlerId }'"
			>
			<div class="img_icon_box">
				<img src='${item.imgUrl}'> 
			</div> 
			<span>${item.name }</span>
		</div>
		</c:forEach>
	</div>
	</c:if>  
	 
	<c:forEach items="${dataList }" var="item">
	<div class="store_box" style="padding-bottom:0px;">    
		<div class="tit" style="color:#343434;font-size:15px;font-weight:bold;"
		 onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?cityShopGoodsList&modulesId=${item.modules.id }&cityButlerId=${cityButlerId }'"
			 style="position:relative;padding-top:0px;"> 
			<img class="spiliner" src="${skinPath }images/home_a.png">
			${item.modules.name } 
			<span class="for_more"> 
			更多<img class="more_img" src="${skinPath}images/enter.png">
			</span>
		</div>  
		<c:if test="${item.modules.showType == '1' }">
		<div class="news_goods_list_box">
			<c:forEach items="${ item.goodsList }" var="item2">
			<a href="${pageContext.request.contextPath}/shopWap.html?goodsDetail&id=${item2.goods.id }" style="margin-right:5px;" >
				<div class="news_goods_box"> 
					<div class="goods_img_box">  
						<img alt="" src="${item2.goods.imgUrl }">
					</div>
					<div class="goods_name">${item2.goods.name }</div>
					<div class="now_price"><span></span>￥${item2.goods.price }</div> 
				</div>    
			</a>
			</c:forEach> 
		</div> 
		</c:if> 
		
		<c:if test="${item.modules.showType == '2' }">
		<div class="v2_new_goods_list">
			<c:forEach items="${item.goodsList}" var="item2">
			<a href="${pageContext.request.contextPath}/shopWap.html?goodsDetail&id=${item2.goods.id }"> 
				<div class="news_goods_box">
					<div class="goods_img_box">   
						<img alt="" src="${item2.goods.imgUrl }">
					</div>
					<div class="goods_name">${item2.goods.name }</div>
					<div class="now_price">￥${item2.goods.price }</div> 
				</div>    
			</a> 
			</c:forEach> 
		</div> 
		</c:if> 
		<c:if test="${item.modules.showType == '3' }">

		<section class="pc-banner"> 
		<div class="swiper-container swiper-container2" style="margin-top: 15px;">
			<div class="swiper-wrapper">
				<c:forEach items="${item.goodsList}" var="item2">
				<div class="swiper-slide swiper-slide-center none-effect">
					<a href="${pageContext.request.contextPath}/shopWap.html?goodsDetail&id=${item2.goods.id }"> <img 
						src="${item2.goods.imgUrl }">
						<div class="footer_goods">
							<div class="f_goods_name">${item2.goods.name }</div>
							<span class="f_goods_price">￥${item2.goods.price }</span> 
							<span class="f_goods_cart"><i class="fa fa-shopping-cart" ></i>加入购物车</span>
						</div>
					</a>  
					<div class="layer-mask"></div>
				</div>
				</c:forEach>
			</div>
		</div>
		</section>
		</c:if>
	</div>
	</c:forEach>
	 
	<div class="h30" style="width:100%;height:50px;"></div>
	 
	<div class='goods_bot_bar'>
	  <div class='item item1' >
	    <img  src='${skinPath }images/tabbar_1b.png'>
	    <div class='name' style='color:#fd5960;'>首页</div>
	  </div>
	  <div class='item item1'>
	  	<a href="${pageContext.request.contextPath}/shopWap.html?cityCate&cityButlerId=${cityButlerId}">
	    <img  src='${skinPath }images/tabbar_2a.png'>
	    <div class='name'>分类</div>
	    </a> 
	  </div> 
	  <div class='item item1'>  
	    <a href="${pageContext.request.contextPath}/shopWap.html?cart">
	    <img  src='${skinPath }images/tabbar_4a.png'>
	    <div class='name'>购物车<span class="em" style="color:#fff;display:none;"></span></div>
	    </a>
	  </div>
	  <div class='item item1'>
	  	<a href="${pageContext.request.contextPath}/shopWap.html?userCenter">
	    <img  src='${skinPath }images/tabbar_5a.png'>
	    <div class='name'>我的</div> 
	    </a>
	  </div>
	</div>
	
	<input id="hidPageType" type="hidden" value="-1" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script type="text/javascript">
		$(".footer").hide(); 
		$(function(){ 
			var swiper = new Swiper('.swiper-container2',{
				effect: 'coverflow',
		        grabCursor: true, 
		        loop:true,  
		        centeredSlides: true,  
		        slidesPerView: 'auto',
				onInit: function(swiper) {
					swiper.slideTo(1, 1000, false);
				}, 
		        coverflow: { 
		            rotate: 50,
		            stretch: 0,
		            depth: 200,  
		            modifier: 1, 
		            slideShadows : true
		        } 
			});
			  
			var swiperBanner = new Swiper('.swiper-container-banner',{
				pagination: '.swiper-pagination',
				paginationClickable: true
			}); 
  
		}); 
	</script> 
</body>
</html> 