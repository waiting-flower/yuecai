<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>全部分类</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script src="${skinPath}js/app_plusGeolocation.js" type="text/javascript" charset="utf-8"></script>
<script id="pageJS" data="${skinPath }js/cateListFun.js?v=xs222ss2" language="javascript" type="text/javascript"></script>
<style type="text/css">
.lazy0{ 
	padding: 10px;
    position: relative;
    z-index: 1;
    margin: 0 10px 10px;
    background: #fff;
	margin-left: 0;
    margin-right: 0;
    border-radius: 0;
    box-shadow: 1px 1px 1px #eee;
    padding-left: 0;
    margin-bottom: 5px;
    list-style: none;
}
.lazy0 a.img {
    float: left;
    width: 80px;
    height: 80px;
    background: #f5f5f5;
    overflow: hidden;
}
.lazy0 a.img  img {
    width: 100%; 
    display: block;
}
.lazy0 .text {
    padding: 0 0 0 90px; 
    padding-left: 90px;
    height: 80px;
}
.lazy0 .text h3 {
    font-size: 16px;
    color: #333;
    line-height: 30px;
    margin: 0;
    overflow: hidden;
        font-size: 14px;
    height: 25px;
    position: relative;
    z-index: 1;
    top: -3px; 
}
.lazy0 .text .nr {
    font-size: 14px;
    color: #888;
    margin-bottom: 10px;
}
.lazy0 .text .nr span {
    color: #FF7800;
    padding: 0 3px;
}
.lazy0 .text .button {
    padding-top: 5px;
    height: 25px;
    line-height: 25px;
} 
.lazy0 .text .button .money {
    color: #FF2B22;
    font-size: 20px;
    margin-right: 10px;
} 
.lazy0 .text .button i {
    font-style: normal;
    font-size: 12px;
} 
.lazy0 .text .button a {
    background: rgba(252,63,120,1);
    background: linear-gradient(-90deg,rgba(252,77,81,1),rgba(252,63,120,1));
    border-radius: 90px;
    color: #fff; 
    font-size: 13px;
    padding: 3px 15px;
    float: right;
    line-height: 18px;
}
</style>
</head>
<body fnav="1" class="g-acc-bg">  
	<div class="pro-s-box thin-bor-bottom" id="divSearch" style="background:#fff;position:fixed;">  
		<div class="dingwei" onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?location&returnUrl=cateList'"> 
			<i class="fa fa-map-marker"></i><span id="location_top_cityName">定位中..</span>
			<i class="fa fa-caret-down"></i> 
		</div>
		<div class="box"> 
			<div class="border">
				<div class="border-inner" style="background:#f7f7f7; "></div>
			</div>
			<div class="input-box">
				<i class="s-icon"></i> <input type="text" placeholder="搜索您喜欢的商品"
					id="txtSearch"> <i class="c-icon" id="btnClearInput"
					style="display: none"></i>
			</div>
		</div> 
		<a href="javascript:;" class="s-btn" id="btnSearch">搜索</a>  
	</div>
	<div class="search-info" style="display: none;bottom:50px;">
            <div class="hot">
                <p class="title">热门搜索</p>
                <ul id="ulSearchHot" class="hot-list clearfix">
                    <c:forEach items="${ keyList }" var="item"  varStatus="status">
                    <li wd="${item }"><a class="items">${item }</a>
                    </li> 
                    </c:forEach> 
                </ul>
            </div>
            <div class="history" style="display: none">
                <p class="title">历史记录</p>
                <div class="his-inner" id="divSearchHotHistory"></div>
            </div>
        </div>
	
	<div class="cate_list_box">
		<c:forEach items="${cateList }" var="item">
		<div class="cate_Item" cateid="46"
			style="display: inline-block; width: 20%;"> 
			<a href="${pageContext.request.contextPath}/shopWap.html?storeList&cateId=${item.id}">
			<div style="width: 42px; height: 42px; margin: 0 auto;">
				<img alt="" style="width: 42px; height: 42px;"
					src="${item.imgUrl }">
			</div>   
			<span  
				style="display: block; margin-top: 5px; width: 100%;    
				text-align: center; font-size: 12px; color: #454545;">${item.name}</span>
			</a>
		</div>
		</c:forEach>  
	</div>	
	
	<input id="hidPageType" type="hidden" value="4" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script>  
		$(".f_announced a").addClass("hover");
	</script>
</body>
</html>
