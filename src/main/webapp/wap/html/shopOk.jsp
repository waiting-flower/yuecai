<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>     
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>购买成功</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath }css/cartList.css?v=161012" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/member.css?v=151209" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath }js/payOkFun.js" language="javascript" type="text/javascript"></script>
</head> 
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	<div class="g-Cart-list" style="margin-bottom:10px;">
	
		<div class="g-pay-auto gray6">
				<div class="z-pay-tips">
					<s></s>支付成功，请等待商家发货
				</div> 
				<div style="margin-top:10px;width:100%;height:60px;line-height:20px;display:block;font-size:12px;font-weight:normal;">
					<div style="width:100%;display:block;float:left;">
						<label style="width:50%;text-align:right;float:left;">支付方式：</label>
						<label style="width:50%;text-align:left;float:left;color:red;">在线支付</label>
					</div> 
					<div style="width:100%;display:block;float:left;">
						<label style="width:50%;text-align:right;float:left;">支付金额：</label>
						<label style="width:50%;text-align:left;float:left;color:red;">${order.money }</label>
					</div>
					<div style="width:100%;display:block;float:left;">
						<label style="width:50%;text-align:right;float:left;">订单号码：</label> 
						<label style="width:50%;text-align:left;float:left;color:red;">${order.orderNo }</label>
					</div>
					<div style="width:100%;display:block;float:left;">
						<label style="width:50%;text-align:right;float:left;">商品数量：</label>
						<label style="width:50%;text-align:left;float:left;color:red;">${order }</label>
					</div>
				</div>
			</div>  
			<div class="m_btn" style="width:90%;margin:0 auto;"> 
				<a href="${pageContext.request.contextPath}/shopWap.html?orderDetail&id=${order.id}"
					class="whiteBtn fl gray6">查看订单</a> <a    
					href="${pageContext.request.contextPath}/shopWap.html?cateList"
					class="orangeBtn fr">继续购物</a>
			</div> 
			<div class="order_goods" style="margin-top:15px;">
				<div class="order_shopBar">
					订单号：${order.orderNo }<span>共计${fn:length(detailList)}件商品</span> 
				</div>
				<c:forEach items="${ detailList }" var="item">
					<div class="good" data_item="4193770">
						<a href="${pageContext.request.contextPath}/shopWap.html?goodsDetail&id=${item.sku.goods.id}">
							<div class="good_cover">
								<img src="${item.sku.goods.imgUrl }" >
							</div>
						</a>
						<div class="info">  
							<a href="${pageContext.request.contextPath}/shopWap.html?goodsDetail&id=${item.sku.goods.id}">
								<div class="good_name">${item.sku.goods.name }</div>
								<p class="sku_coll">${item.sku.goods.name2}</p>
								<p class="good_price my_goods_price">¥${item.sku.goods.price}</p>
								<p class="good_count">×${item.num}</p>
							</a> 
							<div class="good_btns" style="display:none;"> 
								<a class="good_btn color_blue" >申请售后</a>
							</div> 
						</div>
					</div>
					</c:forEach>
				
			</div>
	</div>
			
	
	<input id="hidPageType" type="hidden" value="4" />
	<input id="hidIsHttps" type="hidden" value="1" />
	<input id="hidSiteVer" type="hidden" value="v47" />
	<input id="hidWxDomain" type="hidden" value="" />
	<input id="hidOpenID" type="hidden" value="" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
 
	<script type="text/javascript">
	$(".f_car a").addClass("hover");
	</script>
</body>
</html>
