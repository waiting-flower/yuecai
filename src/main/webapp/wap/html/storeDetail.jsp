<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>${store.name }</title> 
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=232344" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath}js/storeDetailFun.js?v=23236222cxa" language="javascript" type="text/javascript"></script>
</head>
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg"> 
	<input type="hidden" value="${store.id }" id="storeId">
	<div class="pro-s-box" id="divSearch" style="position:relative;z-index:1;background-color: rgb(255, 73, 94);">
		<div class="box">
			<div class="border">
				<div class="border-inner"></div>
			</div> 
			<div class="input-box" >   
				<i class="s-icon"></i> <input type="text" placeholder="搜索您喜欢的商品" 
					id="txtSearch"> <i class="c-icon" id="btnClearInput"
					style="display: none"></i> 
			</div>
		</div> 
		<a href="javascript:;" class="s-btn" id="btnSearch">搜索</a>
	</div>  
	<div class="search-info" style="display: none;bottom:0px;position:fixed;">
            <div class="hot">
                <p class="title">热门搜索</p>
                <ul id="ulSearchHot" class="hot-list clearfix">
                    <c:forEach items="${ keyList }" var="item"  varStatus="status">
                    <li wd="${item }"><a class="items">${item }</a>
                    </li>  
                    </c:forEach>
                </ul>
            </div>
            <div class="history" style="display: none">
                <p class="title">历史记录</p> 
                <div class="his-inner" id="divSearchHotHistory"></div>
            </div>
        </div>
	   
	<div class="store_head_div" style="background-image:url('${store.backImgUrl}');background-size: 100%;background-position: center;"> 
		<img src="${store.imgUrl }">
		<div class="name">${store.name }</div>
		<div class="num" style="display:none;">已有${store.collectNum }人关注</div>
		<c:if test="${isCollect == '1' }">
		<div class="store_gz act" id="yiguanzhu" style="display:none;">
			<i class="fa fa-heart"></i>
			<span>已关注</span>
		</div>  
		</c:if>
		<c:if test="${isCollect == '0' }">
		<div class="store_gz" id="weiguanzhu" style="display:none;">  
			<i class="fa fa-heart-o"></i> 
			<span>点击关注 </span>
		</div>
		</c:if>
	</div>
	<div class="store_data_count">
		<div class="dc_item">
			<a>
				<span class="store_dc_num">${goodsCount }</span>
				<span class="store_dc_sm">全部商品</span>
			</a>
		</div>
		<div class="dc_item">
			<a>
				<span class="store_dc_num">${newGoodsCount }</span>
				<span class="store_dc_sm">最近上新</span>
			</a>
		</div>
		<!-- 
		<div class="dc_item">
			<a>
				<span class="store_dc_num">${zhekouGoodsCount }</span>
				<span class="store_dc_sm">折扣商品</span>
			</a>
		</div>
		 -->
	</div>
	
	<!-- 展示商户的轮播 -->
	<c:if test="${not empty bannerList }">
	<!-- 焦点图 -->
	<div class="hotimg-wrapper" style="margin-top:10px;">
		<div class="hotimg-top"></div>
		<section id="sliderBox" class="hotimg">
			<ul class="slides"> 
				<c:forEach items="${ bannerList }" var="item">
				<li><a href="javascript:;"><img src="${item.imgUrl }" alt="" /></a></li>
				</c:forEach> 
			</ul>
		</section>
	</div>
	</c:if>
	
	<div class="cplb_tit_box"> 
		<h1>商品列表</h1> 
		<a href="${pageContext.request.contextPath}/shopWap.html?storeGoodsList&id=${store.id}">更多<i class="fa fa-angle-right"></i></a>
	</div> 
	<div class="good-list-box" id="loadingPicBlock">
		<div class="goods_list"> 
			<ul id="ulGoodsList"> 
				<div class="search_prolist cols_2 type_twotitle" id="itemList">
					 
				</div>
			</ul>  
		</div>				
						
		<div id="divLoading" class="loading clearfix" style="display: none;">
			<b></b>正在加载
		</div> 
		<div id="noneTipDiv"></div>
	</div>
	    
	<div class="h55"></div>
	
	<div class="store_footer">   
		<div class="footer_item">  
			<a href="${pageContext.request.contextPath}/shopWap.html?storeInfo&id=${store.id}">店铺详情</a>
		</div> 
		<div class="footer_item"> 
			<i class="fa fa-list" style="margin-right:5px;"></i>热门分类
			<div class="footer_sub_menu_store">
				<c:forEach items="${ cateList }" var="item"> 
				<div class="store_sub_menut_item">  
				<a href="${pageContext.request.contextPath}/shopWap.html?storeGoodsList&id=${store.id}&cateId=${item.id}">
				${item.name }
				</a>
				</div>
				</c:forEach>
			</div>
		</div>
		<div class="footer_item kefu_show">客服微信</div>
	</div>
	 
	<div class="show-temark-div ">
    	<h4>${store.name }</h4>  
    	<div class="img-new-box"><img src="${store.qrCode }"></div>
     	<p>微信扫一扫联系客户</p>
  	</div>
	<div class="mask-filter-div "></div> 
	
	<input id="hidPageType" type="hidden" value="-1" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script>  
		$(".footer").hide();
	</script>
</body>
</html>
