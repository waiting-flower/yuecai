<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>商城分类</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/swiper.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/cityShop.css?v=173330522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script src="${skinPath}js/swiper.min.js" language="javascript" type="text/javascript"></script> 
<script id="pageJS" data="${skinPath }js/cityCate.js?v=xs222ss2" language="javascript" type="text/javascript"></script>
<style type="text/css">
.swiper-container-banner {
    padding-bottom: 0px; 
} 
.swiper-slide img.banner_img {
    border-radius: 8px;
        height: 120px; 
} 
.swiper-slide img {
    height: 100px;
    min-height: 100px;
    border: 0;
}
.right-cate {
    background-color: #f5f5f5;
    margin-left: 100px;
}    
.top-cate {
    background:#fff; 
} 
.top-cate a.cur {
    background: #f5f5f5;
    color: #f44623; 
    border-right: 0;
    text-align: center;
}
.top-cate a.cur:before {
    content: '';
    position: absolute;
    width: 2px;
    height: 100%;
    top: 0;
    left: 0; 
    background-color: #ef4238;
}
.xnmb-ind-tit span:after {
    content: "";
    width: .2rem;
    height: 1rem;
    background: #ef4238; 
    position: absolute;
    left: 0;
    top: .6rem;
}
div.elecfenlei a img {
    width: 50px;
    height: 50px;
    border-radius: 50%;
    display: block;
    margin: 0 auto;
}
div.elecfenlei a {
    height:auto;
    margin: 0;   
    width: 33.3%;
    color:#666;  
} 
.child-cate{
	width:90%;
	margin:0 auto;
	background:#fff;
	border:1px solid #e5e5e5;
	border-radius:8px;  
	margin-top:10px;  
}
</style>
</head>  
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
		
	 	<div class="city_shop_search_box" style="position:absolute;left:0;right:0;top:0px;border:none;">
			<div class="c_s_search">
				<div class="cs_input_box">
					<i class="fa fa-search"></i>
					<input type="text" id="city_search_key" placeHolder="搜索您喜欢的商品名称">
				</div>
				<div class="cs_search_btn" id="city_search_btn">搜索</div>
			</div>
		</div>

		
		<div class="all-list-wrapper" style="top:44px;">
 
            <div class="menu-list-wrapper top-cate" id="divSortList" style="width:100px;text-align:center;">
					<c:forEach items="${ cList1 }" var="item"> 
					<a href="javascript:;" class="" id="${item.id}" itemId="${item.id }">
						${item.name }
					</a>  
					</c:forEach>
            </div>

            <div class="good-list-wrapper"> 
				<div class="good-list-inner" style="padding-top:0px;padding-left:0px;">
					<div class="good-list-box" id="loadingPicBlock">
						<div class="goods_list right-cate"> 
								<!-- 轮播图 -->
								<div class="swiper-container swiper-container-banner">
							        <div class="swiper-wrapper">
							        	<c:forEach items="${bannerList }" var="item">
							            <div class="swiper-slide">
							            	<a href="${ietm.href }">
							            		<img class="banner_img" src="${item.imgUrl }">
							            	</a>
							            </div> 
							            </c:forEach> 
							        </div>
							    </div> 
								<c:forEach items="${ cList1 }" var="item">    
								<div class="shop_tip" id="child_div_${item.id}">
									<!-- 
																		<div class="xnmb-ind-tit" id="more_goods_article"><span>${item.name}</span></div>
									
									 -->
									<div class="child-cate elecfenlei" style="display:block;">
										<c:forEach items="${ cList2 }" var="item2">
											<c:if test="${item.id == item2.parentCate.id }">
											<a href="${pageContext.request.contextPath}/shopWap.html?cityShopGoodsList&cateId=${item2.id}&cityButlerId=${cityButlerId }"><img
												src="${item2.imgUrl }"><span>${item2.name}</span></a>
												</c:if>
										</c:forEach>  
									</div> 
								</div>
								</c:forEach>
						</div>
					</div> 
				</div>
			</div>
    </div> 
	
	<div class="h30" style="width:100%;height:50px;"></div>
	 
	<div class='goods_bot_bar'>
	  <div class='item item1' >
	  <a  href="${pageContext.request.contextPath}/shopWap.html?cityShop&cityButlerId=${cityButlerId}">
	    <img  src='${skinPath }images/tabbar_1a.png'>
	    <div class='name'>首页</div></a> 
	  </div> 
	  <div class='item item1'>
	    <img  src='${skinPath }images/tabbar_2b.png'>
	    <div class='name' style='color:#fd5960;'>分类</div>
	  </div>  
	  <div class='item item1'>  
	    <a href="${pageContext.request.contextPath}/shopWap.html?cart">
	    <img  src='${skinPath }images/tabbar_4a.png'>
	    <div class='name'>购物车<span class="em" style="color:#fff;display:none;"></span></div>
	    </a>
	  </div>
	  <div class='item item1'>
	  	<a href="${pageContext.request.contextPath}/shopWap.html?userCenter">
	    <img  src='${skinPath }images/tabbar_5a.png'>
	    <div class='name'>我的</div> 
	    </a>
	  </div>
	</div>
	<script>
			$(".top-cate a").eq(0).addClass("cur");
			var myId = $(".top-cate a").eq(0).attr("id");
			//$("#child_div_" + myId).show();
			 
			$(".top-cate a").click(function() {
				$(this).addClass("cur").siblings().removeClass("cur");
				var i = $(this).attr("itemId");
				var index = $(this).index(); 
				var itemHeight = $(this).height();
				$("#divSortList").stop().animate({
	                scrollTop: index * itemHeight
	            }, 500); 
				console.log("index的值为" + index); 
				//$(".child-cate").eq(index).siblings().hide();
				var top = $("#child_div_" + i).position().top;
				var divArr = $(".goods_list").find(".shop_tip");
				var height = 0;
				for(var i = 0; i < index; i++){
					var div = $(divArr[i]);
					height += div.height();
					console.log(height);
				}
				console.log(top);   
				$(".goods_list").animate({  
	                 scrollTop: height
	             }, 500);   
			}); 
		</script> 
	<input id="hidPageType" type="hidden" value="-1" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script>  
		$(".footer").remove();
		var swiperBanner = new Swiper('.swiper-container-banner',{
			paginationClickable: false 
		});
	</script>
</body>
</html>
