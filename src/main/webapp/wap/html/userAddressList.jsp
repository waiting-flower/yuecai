<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>我的地址列表</title> 
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />

<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/member.css?v=151209" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath }js/userAddressListFun.js" language="javascript" type="text/javascript"></script>

</head>
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
		<input type="hidden" id="op" value="${op }">
		<input type="hidden" id="orderId" value="${orderId }">
		<input type="hidden" id="tradeNo" value="${tradeNo }">
		<input type="hidden" id="scoreGoodsId" value="${scoreGoodsId }">
		<input type="hidden" id="num" value="${num }">
	
	<div class="userInfo">
		<div class="userHeadBox"> 
		<img alt="" src="${user.headUrl}">
		</div> 
		<span>${user.nick }</span>  
		<span class="mobile">${user.mobile}</span>
	</div> 
	
	
	<div class="addrListBox">
		<div id="addList"></div>
		<!-- 
		<div class="item">
			<div class="tag">点击选择</div>
			<div class="tit">
				<span>收货地址</span>
				<div class="opt">
					<i class="fa fa-pencil edit"></i>
					<i class="fa fa-times close"></i>
				</div>
			</div>
			<div class="addrName"><i style="margin-right:5px;" class="fa fa-user"></i>xxx</div>
			<div class="addrInfo">江苏省-徐州市-睢宁县-睢城镇 xx小区5号楼2单元404</div>
			<div class="addrTel"><i style="margin-right:5px;" class="fa fa-mobile"></i></div>
		</div> 
		 -->
		<a href="${pageContext.request.contextPath}/shopWap.html?userAddressDetail" class="btn">新增收货地址</a>
	</div>
	
	<input id="hidPageType" type="hidden" value="4" />
	<jsp:include page="../html/footer.jsp"></jsp:include>

	<script> 
		$(".f_personal a").addClass("hover");
		function goClick(obj, href) {
			$(obj).empty();
			location.href = href;
		}
		if (navigator.userAgent.toLowerCase().match(/MicroMessenger/i) != "micromessenger") {
			$(".m-block-header").show();
		}
	</script>
</body>
</html>
