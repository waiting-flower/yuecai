<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>用户中心</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" /> 
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" /> 
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/member.css?v=151209" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" /> 
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
</head>
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	
	<header class="user-header-box">
		<div class="padding-all">
			<a href="${pageContext.request.contextPath}/shopWap.html?userHeadUrl">
				<div class="user-header">
					<div class="heaer-img img-commom" style="height: 98px;">
						<c:if test="${not empty user.headUrl }">
						<img src="${user.headUrl }" class="img-height">
						</c:if>
						<c:if test="${empty user.headUrl }"> 
						<img src="${skinPath}images/logo.png" class="img-height">
						</c:if>
					</div>
				</div>
			</a> 
			<a href="${pageContext.request.contextPath}/shopWap.html?userHeadUrl" class="box-flex">
				<div class="header-admin">
					<h4 class="ellipsis-one f-06">${user.nick }</h4>
				</div>
			</a>
			<div class="header-icon">
				<!-- <a class="youxiang" href="/mobile/index.php?m=user&amp;a=messagelist">
										<label class="f-02">消息</label>
				</a> -->

				<a class="shezhi" href="${pageContext.request.contextPath}/shopWap.html?userInfo">
					<i class="fa fa-cog"></i>
				</a> 
			</div>
		</div>
	</header>
	
	  
	<!-- 订单开始 -->
	<div class='user_service' style='padding-bottom:10px;'>
	  <div class='tit'>我的订单
	    <div class="right_nav_btn" onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?orderList&type=-1'">查看全部
	      <i class="fa fa-angle-right" style="margin-left:5px;"></i>
	    </div> 
	  </div>
	  <div class='service_list'>
	 
	    <div class='item' style='width:20%;' onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?orderList&type=1'">
	        <div class='img_box'>
	          <img src='${skinPath }/images/icon_15.png'>
	          <c:if test="${daizhifu > 0 }"><em>${daizhifu }</em></c:if>
	        </div>
	        <span>待支付</span>  
	    </div>
	
	    <div class='item' style='width:20%;' onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?orderList&type=2'">
	        <div class='img_box'>
	          <img src='${skinPath }/images/icon_14.png'>
	          <c:if test="${daifahuo > 0 }">
	          	<em>${daifahuo }</em>
	          </c:if>
	        </div> 
	        <span>待发货</span>
	    </div>
	    <div class='item' style='width:20%;' onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?orderList&type=3'">
	        <div class='img_box'>
	          <img src='${skinPath }/images/icon_16.png'/>
	          <c:if test="${daishouhuo > 0 }">
	          	<em>${daishouhuo }</em>
	          </c:if>
	        </div>
	        <span>待收货</span> 
	    </div> 
	    <div class='item' style='width:20%;' onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?orderList&type=4'">
	        <div class='img_box'>
	          <img src='${skinPath }/images/icon-19.png'>
	          <c:if test="${daipingjia > 0 }">  
	          	<em>${daipingjia }</em>
	          </c:if>
	        </div>
	        <span>待评价</span> 
	    </div>   
	    <div class='item' style='width:20%;' onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?orderSalesAfterList'">
	        <div class='img_box'>
	          <img src='${skinPath }/images/icon_18.png'>
	          <c:if test="${shouhou > 0 }">
	          	<em>${shouhou }</em>
	          </c:if>
	        </div> 
	        <span>售后记录</span>
	    </div> 
	  </div>
	</div>
	<!-- 订单结束 --> 
	
	<div class="m-help-main">
		<ul class="m-help-list">     
			<li class="thin-bor-bottom"><a href="${pageContext.request.contextPath}/shopWap.html?userScore"
				class="flex-box"> <strong><i class="fa fa-database"></i>我的积分</strong>   <i
					class="z-set fa fa-angle-right"></i> 
			</a></li> 
			<li class="thin-bor-bottom"><a href="${pageContext.request.contextPath}/shopWap.html?scoreOrderList"
				class="flex-box"> <strong><i class="fa fa-gift"></i>积分订单</strong>   <i
					class="z-set fa fa-angle-right"></i>  
			</a></li> 
			<li class="thin-bor-bottom"><a href="${pageContext.request.contextPath}/shopWap.html?userAddressList"
				class="flex-box"> <strong><i class="fa fa-address-card"></i>收货地址</strong>  
					<i class="z-set fa fa-angle-right"></i> 
			</a></li>
			<li class="thin-bor-bottom"><a href="${pageContext.request.contextPath}/shopWap.html?userInfo"
				class="flex-box"> <strong><i class="fa fa-address-book-o"></i>个人资料</strong>  
					<i class="z-set fa fa-angle-right"></i>
			</a></li> 
			<li class="thin-bor-bottom"><a href="${pageContext.request.contextPath}/shopWap.html?userGoodsCollect"
				class="flex-box"> <strong><i class="fa fa-heart-o"></i>收藏的商品</strong> 
					<i class="z-set fa fa-angle-right"></i>
			</a></li>    
			<li style="display:none;" class="thin-bor-bottom"><a href="${pageContext.request.contextPath}/shopWap.html?userStoreCollect"
				class="flex-box"> <strong><i class="fa fa-star-o"></i>关注的店铺</strong>  
					<i class="z-set fa fa-angle-right"></i>
			</a></li> 
			<li class="thin-bor-bottom"><a href="${pageContext.request.contextPath}/shopWap.html?storeRuzhu"
				class="flex-box"> <strong><i class="fa fa-dashboard"></i>商家入驻</strong>   <i
					class="z-set fa fa-angle-right"></i>
			</a></li> 
			
			
			<!-- 
			<li class="thin-bor-bottom"><a href="/weifenxiao/shopWapController.html?userFenxiao"
				class="flex-box"> <strong><i class="fa fa-share-alt-square"></i>我的推广码</strong>   <i
					class="z-set fa fa-angle-right"></i>
			</a></li>
			<li class="thin-bor-bottom"><a href="${pageContext.request.contextPath}/shopWap.html?userAuth"
				class="flex-box"> <strong><i class="fa fa-rebel"></i>授权</strong>   <i
					class="z-set fa fa-angle-right"></i>
			</a></li> 
			 
			<li class="thin-bor-bottom"><a href="${pageContext.request.contextPath}/shopWapController.html?findPass"
				class="flex-box"> <strong><i class="fa fa-key"></i>找回密码</strong>   <i
					class="z-set fa fa-angle-right"></i> 
			</a></li> 
			 -->
		</ul> 
	</div>   
	
	<p class="colorbbb" style="text-align:center;width:100%;padding:10px 0px;">独家运营单位：<span style="color:#ef4238;">北京远中和科技有限公司</span> </p>  
	 <p class="colorbbb" style="text-align:center;width:100%;padding:0px 0px 10px 0px;">客服热线：<span style="color:#ef4238;">400-060-8798</span> (工作日9:00-11:30，13:30-17:30)</p> 
	<!--  
	<nav class="b-color-f user-nav-box m-top08"> 
		<div class="box_box ul-4 text-c b-color-f">
			<a href="/mobile/index.php?m=user&amp;a=collectionlist">
				<label><i class="fa fa-heart-o fa-3x color-fe"></i></label>
				<p class="f-02 col-7">收藏的商品</p> 
			</a>  
			<a href="/mobile/index.php?m=user&amp;a=storelist" class="">
				<label><i class="fa fa-star-o fa-3x color-f9c"></i></label>
				<p class="f-02 col-7">关注的店铺</p>
			</a>
			<a href="/mobile/index.php?m=merchants">
				<label><i class="fa fa-github-alt fa-3x color-98"></i></label>
				<p class="f-02 col-7">商家入驻</p>
			</a>
			<a href="/mobile/index.php?m=user&amp;a=history">
				<label><i class="fa fa-eye fa-3x color-c78"></i></label>
				<p class="f-02 col-7">浏览记录</p>
			</a>
		</div>
	</nav>  
	 -->   
	 <div class="h55"></div>
	<input id="hidPageType" type="hidden" value="4" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script> 
		$(".f_personal a").addClass("hover");
	</script>
</body>
</html>
