<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>${store.name }</title> 
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=2323" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath}js/storeDetailFun.js?v=161012" language="javascript" type="text/javascript"></script>
</head>
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	<input type="hidden" value="${store.id }" id="storeId">
	<div class="store_head_div" style="background-image:url('${store.backImgUrl}');background-size: 100%;background-position: center;"> 
		<img src="${store.imgUrl }">
		<div class="name">${store.name }</div>
		<div class="num" style="display:none;">已有${store.collectNum }人关注</div>
		<c:if test="${isCollect == '1' }">
		<div class="store_gz act" id="yiguanzhu" style="display:none;">
			<i class="fa fa-heart"></i>
			<span>已关注</span>
		</div>  
		</c:if>
		<c:if test="${isCollect == '0' }">
		<div class="store_gz" id="weiguanzhu" style="display:none;">  
			<i class="fa fa-heart-o"></i>
			<span>点击关注 </span>
		</div>
		</c:if>
	</div>
	<div class="store_data_count">
		<div class="dc_item">
			<a> 
				<span class="store_dc_num">${goodsCount }</span>
				<span class="store_dc_sm">全部商品</span>
			</a> 
		</div>
		<div class="dc_item">
			<a>
				<span class="store_dc_num">${newGoodsCount }</span>
				<span class="store_dc_sm">最近上新</span>
			</a>
		</div>  
	</div>  
	
	<div class="store_info_contact">
		<div class="store_info_item kefu_show">
			<span class="name">客服微信</span>
			<div class="info">&nbsp;</div> 
			<i class="fa fa-qrcode fa-2x"></i>
		</div>  
		<div class="store_info_item">
			<span class="name">商家电话</span> 
			<div class="info">
			<a href="tel:${store.mobile}">${store.mobile}</a>
			</div> 
			<i class="fa fa-phone fa-2x"></i>
		</div>   
	</div>
	
	<div class="store_info_contact">
		<div class="store_info_item">
			<span class="name">店铺简介</span>
			<div class="info">&nbsp;</div>   
			<i class="fa fa-info fa-2x" style="margin-right:5px;"></i>
		</div>   
		<div class="store_info_item" onclick="toMap();">
			<span class="name">所在地区</span>  
			<div class="info">${store.pro }&nbsp;${store.city }&nbsp;${store.country }</div> 
			<i class="fa fa-map fa-2x"></i>
		</div>  
		<div class="store_info_item" onclick="toMap();">
			<span class="name">详细地址</span>   
			<div class="info">${store.address }</div> 
			<i class="fa fa-map fa-2x"></i>
		</div>
		<div class="store_info_item">
			<span class="name">主营类目</span> 
			<div class="info">${store.cate.name}</div> 
			<i class="fa fa-sitemap fa-2x"></i>
		</div>   
		
	</div>
	
	<div class="h55"></div>
	
	<div class="store_footer">   
		<div class="footer_item">   
			<a href="${pageContext.request.contextPath}/shopWap.html?storeDetail&id=${store.id}">店铺首页</a>
		</div> 
		<div class="footer_item"> 
			<i class="fa fa-list" style="margin-right:5px;"></i>热门分类
			<div class="footer_sub_menu_store">
				<c:forEach items="${ cateList }" var="item"> 
				<div class="store_sub_menut_item"> 
				<a href="${pageContext.request.contextPath}/shopWap.html?storeGoodsList&id=${store.id}&cateId=${item.id}">
				${item.name }
				</a>
				</div>
				</c:forEach>
			</div>
		</div>
		<div class="footer_item kefu_show">客服微信</div>
	</div>
	 
	<div class="show-temark-div ">
    	<h4>${store.name }</h4> 
    	<div class="img-new-box"><img src="${store.qrCode }"></div>
     	<p>微信扫一扫联系客户</p>
  	</div>
	<div class="mask-filter-div "></div>
	
	<div class="h55"></div>
	
	<input id="hidPageType" type="hidden" value="-1" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script>  
		$(".footer").hide(); 
		function showResult(res){
			console.log(res);
			var lng = res.result.location.lng;
			var lat = res.result.location.lat;
			location.href = "${pageContext.request.contextPath}/shopWap.html?map&lng=" + lng + "&lat=" + lat;
		}
		function toMap(){
			var url =  "http://api.map.baidu.com/geocoder/v2/?address=${store.pro}${store.city}${store.country}${store.address}"
					+ "&output=json&ak="  
					+ "QNo49R0NgQ473BxbBvDtE0DUnaf69A9x"
					+ "&callback=showResult"; 
			// $.getScript(url);
			
		}
	</script> 
</body>
</html>
