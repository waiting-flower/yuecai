<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>       
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>评价订单</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
    <meta content="yes" name="apple-mobile-web-app-capable" />
    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
    <meta content="telephone=no" name="format-detection" />
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <link href="${skinPath }css/comm.css?v=170522" rel="stylesheet" type="text/css" />
    <link href="${skinPath }css/comment.css?v=161012" rel="stylesheet" type="text/css" />
    <link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
    <script src="${skinPath }js/jquery190.js" language="javascript" type="text/javascript"></script>
    <script id="pageJS" data="${skinPath }js/commentOrderFun.js" language="javascript" type="text/javascript"></script>
    <style type="text/css">
    .user_orderbox_hd:after{
    	border:none; 
    }
    </style>
</head>  
<body id="loadingPicBlock" class="g-acc-bg">
<input name="hidUserID" type="hidden" id="hidUserID" value="${hidUserID}" />
<input name="hidUserID" type="hidden" id="userId" value="${userId}" />
<div class="marginB"> 
<input type="file" id="file" style="opacity: 0;position: fixed;bottom: -100px">
<input type="file" id="file2" style="opacity: 0;position: fixed;bottom: -100px">
		<c:forEach items="${ mapList }" var="item">
		<div class="comment_item" id="item_${item.detail.id }">
			<a href="${pageContext.request.contextPath}/shopWap.html?goodsDetail&id=${item.detail.sku.goods.id}" class="content"> <img
				class="image" 
				src="${item.detail.sku.goods.imgUrl}"
				width="50" height="50">
				<p class="name">${item.detail.sku.goods.name}</p>
				<p class="name">${item.detail.sku.goods.name}</p>
				<p class="price">¥${item.detail.sku.price}</p>
			</a>
			<div class="foot"> 
				<div class="action">
					<c:choose>
						<c:when test="${item.detail.isPost == '1' }"><!-- active -->
						<a href="javascript:void(0)" postId="${item.detail.postId}" class="btn btn_0 btn_viewcomment" 
							data-productid="${item.detail.id}" data-orderid="${a.id}"
							data-goodsid="${item.detail.sku.goods.id}" 
							style="width: 90px;" data-show="0">查看评价<i></i></a> 
						<a href="javascript:void(0)"  postId="${item.detail.postId}" class="btn btn_1 btn_addpic"
							data-productid="${item.detail.id}" data-orderid="${a.id}" 
							data-goodsid="${item.detail.sku.goods.id}" 
							data-sensitive="false" data-show="0">添加图片<i></i></a> 
						</c:when>
						<c:otherwise>
						<a href="javascript:void(0)"  
							data-productid="${item.detail.id}" data-orderid="${a.id}"
							data-goodsid="${item.detail.sku.goods.id}" class="btn btn_1 btn_write_comment" 
							data-show="0">填写评价<i></i></a>  
						</c:otherwise>
					</c:choose>
				</div>
			</div> 
			<div class="inner" id="fillin_${item.detail.id}" style="display: none;">
				<i class="arrow"></i>
				<div class="comment_view">
					<span class="label">评分：</span>
					<div class="stars" data-tag="comment-scores">
						<i s="1" class="cur"></i>
						<i s="2" class="cur"></i>
						<i s="3" class="cur"></i>
						<i s="4" class="cur"></i>
						<i s="5" class="cur"></i>
					</div> 
				</div> 
				<div class="comment_textarea">
					<div class="textarea_wrap">
						<textarea data-tag="comment-text" maxlength="150"
							placeholder="这次买的商品满意吗？写点心得给其他顾客参考吧。长度在6-500字之间"></textarea>
					</div>
				</div> 
				<div class="comment_images comment_images_row">
					<span class="label">添加图片<small>（您最多可以上传10张图片）</small></span>
					<ul class="images">
						<li id="file_${item.detail.id }"><a href="javascript:;" data-commid="${item.detail.id}"
							data-tag="upload_addimg" class="btn_add btn_new_add"></a></li>	
					</ul>
				</div>
				<!-- <div class="comment_gwq">
					<span class="select selected" data-tag="anonymousCheckbox">匿名发表</span>
				</div> -->
				<div class="mod_btns">
					<a href="javascript:void(0)" class="mod_btn bg_2" 
						data-tag="submitBtn" data-goodsid="${item.detail.sku.goods.id}" 
						data-productid="${item.detail.id }" data-orderid="${a.id}">发表评价</a>
				</div>
			</div>
			<div class="inner" id="viewcomment_${item.detail.id}" style="display: none;">
					<i class="arrow arrow_1"></i>
					<div class="inner_text">
						<div class="stars">
							<i class="sss_tip"></i> 
							<i class="sss_tip"></i>  
							<i class="sss_tip"></i> 
							<i class="sss_tip"></i>  
							<i class="sss_tip"></i>
						</div>
						<p>${item.post.msgContent}</p>
					</div>
			</div>
			<c:if test="${item.detail.isPost == '1' }">
			<script type="text/javascript"> 
			$(function(){
				var score = "${item.post.commentScore}";   
				var stars = $("#viewcomment_${item.detail.id}").find("i.sss_tip"); 
 				var num = parseInt(score);
				for(var i = 0; i < num; i++){
					$(stars[i]).addClass("cur");
				}
			}); 
			</script> 
			</c:if>		 
			<div class="inner" id="addimgbtn_${item.detail.id}" style="display: none;">
					<i class="arrow"></i>
					<div class="inner_images">
						<span class="label">添加图片<small>（您最多可以上传10张图片）</small></span>
						<ul class="images" id="curr_images_${item.post.id }">
							<li id="edit_file_${item.post.id }"><a href="javascript:;" data-commid="${item.detail.id}"  postId="${item.post.id}"
							data-tag="upload_addimg" class="btn_add btn_edit_add"></a></li>	
						</ul>
						<p>
							<a href="javascript:void(0)" data-tag="imgsubmitbtn" class="fabiaotupian"
								data-orderid="${a.id }" postId="${item.post.id}">发表图片</a>
						</p>
					</div>
				</div>
			</div>
		</c:forEach>
		  
	<div class="comment_text" style="display:none;">
        <p>评价、传图均能获得积分奖励！好的晒单可以获取更多的积分</p>
        <p><a href="javascript:;" data-tag="openrule" id="openRuleBtn">详细奖励规则</a></p>
    </div>  
	</div>
	
	
	<!-- 积分奖励规则 -->
	<div class="tuan_info_layer" id="ruletips">
    <span class="btn" id="rule_close" style="z-index: 100;right: 10px;top: 10px;">×</span>
    <div class="inner content scroll show_scroll" id="rulecontent" style="height: 95%; overflow: hidden;">
        <p class="title"><em>评价时效：</em></p>
        <p class="text">您可以在订单完成后的90天内进行商品评价（某些品类除外）。</p>
        <p class="title"><em>评价规则：</em></p>
        <p class="text">转米鼓励用户发表真实、客观、原创的评价，您的评价会直接影响商品好评率以及店铺动态评分。 </p>
        <p class="text">转米会对评价和图片进行审核，且转米有权删除违法、涉黄、违反道德的评价及图片，且审核通过后的评价和图片才能展示给其他用户。</p>
        <p class="text">同一订单中的相同商品或者下单时间相隔15日内不同订单中的相同商品只能评价一次。</p>
        <p class="text">系统会对退换货商品已经产生的商品评价进行删除，并扣除已获得积分</p>

        <p class="title"><em>评价送积分规则：</em></p>
        <p class="text">1.评价送积分，商品金额大于20元商品（虚拟商品除外）进行评价，并通过审核后，根据商品购买价和您的评价内容为您发放积分</p>
        <table>
            <thead>
            <tr><th>商品价格</th><th>字数≥10</th></tr>
            </thead>
            <tbody>
            <tr><td>20≤转米价＜100</td><td>10</td></tr>
            <tr><td>转米价≥100</td><td>20</td></tr>
            </tbody>
        </table>
        <p class="text">2.传图送积分，商品金额大于20元商品（虚拟商品除外）上传图片，并通过审核后，根据商品转米价为您发放积分</p>
        <table>
            <thead>
            <tr><th>商品价格</th><th>积分数量</th></tr>
            </thead>
            <tbody>
            <tr><td>20≤转米价＜100</td><td>10</td></tr>
            <tr><td>转米价≥100</td><td>20</td></tr>
            </tbody> 
        </table> 
        <div style="width:100%;height:48px;"></div>
    </div>
</div>
	
     
<input id="hidPageType" type="hidden" value="3" />
<input id="hidIsHttps" type="hidden" value="1" />
<input id="hidSiteVer" type="hidden" value="v47" />
<input id="hidWxDomain" type="hidden" value="" />
<input id="hidOpenID" type="hidden" value=""/>
<jsp:include page="./footer.jsp"></jsp:include>

<script type="text/javascript">
$(".f_personal a").addClass("hover");
</script>
</body>
</html>