<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>订单申请售后</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script src="${skinPath}js/upload/base64image.js" language="javascript" type="text/javascript"></script> 
<script id="pageJS" data="${skinPath }js/userOrderSalesAfterFun.js" language="javascript" type="text/javascript"></script>
<style type="text/css">
.list{
  width: 94%;
  padding: 10px 3%;
  background: #fff;
}
.list .main_item{
  width: 100%;
  padding-bottom: 10px;
   border-bottom: 1px solid #e5e5e5;
}
.list .item{
  width: 100%;
  display: flex;
 
}
.list .item .img_box{
  flex:2;
}  
.list .item .img_box img{
  width: 100%;
  height: 100%;
}
.list .item .content_box{
  flex:7;
  margin-left: 10px;
}
.content_box .name{
  display: block;
  width: 100%;
  color: #454545;
  font-size: 13px;
}
.content_box .size_box{
  display: flex;
  width: 100%;
}
.size_box .size{
  flex: 3;
  color: #333;
  font-size: 12px;
}
.size_box .jiesuanjia{
  color: #333;
  flex: 2;
  font-size: 12px;
}

.ques_box{
  margin-top: 10px;
  width: 94%;
  padding: 10px 3%;
  background: #fff;
}
.ques_box .tit{
  color: #666;
  font-size: 14px;
}
.ques_box .tit .tips{
  color: #fd4238;
}

.ques_list{
  width: 100%;
}
.ques_list .ques_item{
  width: 27%; 
  margin-right: 5%;
  color: #454545;
  background: #e5e5e5;
  border-radius: 5px;
  display: inline-block;
  height: 25px;
  line-height: 25px;
  text-align: center;
  margin-top: 10px;
  font-size: 12px;
  padding: 2px 0px;
}
.ques_list .ques_item.curr{
  background: #fd4238;
  color: #fff;
}

.miaoshu_box{
  width: 94%;
  padding: 10px 3%;
  background: #fff;
  margin-top: 10px;
}
.miaoshu_box .tit{
  color: #666;
  font-size: 14px;
}
.miaoshu_box textarea{
  background: #e5e5e5;
  width: 96%;
  padding: 2px 2%;
  height: 54px;
  color: #454545;
  margin-top: 10px;
  border-radius: 5px;
} 
.miaoshu_box textarea::placeholder{
	font-size:13px;  
}
.ttt_place{
  font-size: 12px;
  color: #999;
  line-height: 18px;
}

.img_tips{
  color: #fd4238;
  margin-top: 10px;
  font-size: 13px;
}

.imgList{
  width: 100%;
  margin: 0 auto;
  margin-top: 10px;
 }

.imgList .imgItem{
  width: 21%;
  margin-left: 2%;
  height: auto;
  display: inline-block;
  position: relative;
 }  
.imgList .imgItem img{
  width: 100%;
 }
 
 .imgList .imgItem .del {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    width: 19px;
    height: 19px;
    background: url(${skinPath}images/upimg_pub.png) no-repeat -4px 0;
    background-size: 23px auto;
    text-indent: -999em;
    overflow: hidden; 
}
 
.wuliu_box{
  width: 94%;
  padding: 10px 3%;
  margin-top: 10px;
  background: #fff;
}

.wuliu_box .tit{
  color: #fd4238;
  font-size: 14px;
}
.wuliu_box .dizhi{
  font-size: 12px;
  color: #454545;
  margin-top: 10px;
  width: 100%;
}
.wuliu_box .shoujianren{
  font-size: 12px;
  color: #454545;
  width: 100%;
}
.wuliu_box .mobile{
  font-size: 12px;
  color: #454545;
  width: 100%;
}
.wuliu_box .mobile .fuzhi_btn{
  float: right;
  margin-right: 10px;
  border: 1px solid #e5e5e5;
  color: #454545;
  text-align: center;
  padding: 3px 5px;
}
.input_box{
  border-bottom: 1px solid #333;
  margin-top: 10px;
}
.tijiaoBtn{
  width: 94%;
  margin: 0 auto;
  margin-top: 20px;
  text-align: center;
  color: #fff;
  background: #fd4238;
  margin-bottom: 20px;
  height: 30px;
  line-height: 30px;
  border-radius: 5px;
}
.show_tips{
  color: #fd4238;
  width: 94%;
  margin: 0 auto;
  margin-top: 10px;
  font-size: 12px;  
}
</style>
</head>
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	<input type="hidden" value="${a.id }" id="orderDetailId">
	<div class='list'> 
	  <div class='main_item'>
	    <div class='item'>
	      <div class='img_box'> 
	        <img   src='${a.sku.goods.imgUrl }'>
	      </div>
	      <div class='content_box'>   
	        <div class='name'>${a.sku.goods.name}&nbsp;&nbsp;</div>
	        <div class='name'>${a.sku.goods.name2 }</div>
	        <div class='name'>数量:${a.num}</div>
	        <div class='size_box'>   
	          <div class='size'>${a.sku.parmasValuesJson}</div> 
	          <div class='jiesuanjia'>结算价：${a.sku.price}</div>
	        </div> 
	      </div>
	    </div>
	  </div>
	</div>
	
	<div class='ques_box'>
	  <div class='tit'>选择问题</div> 
	  <div class='ques_list questionList'>
	    <div class='ques_item' data-cate='1' ques="货品漏发">货品漏发</div>
	    <div class='ques_item' data-cate='2' ques="质量问题">质量问题</div>
	    <div class='ques_item' data-cate='3' ques="发错款号">发错款号</div>
	    <div class='ques_item' data-cate='4' ques="发错颜色">发错颜色</div>
	    <div class='ques_item' data-cate='5' ques="发错尺寸">发错尺寸</div>
	    <div class='ques_item' data-cate='6' ques="其他问题">其他问题</div>
	  </div>   
	</div> 
	
	<div class='ques_box' >
	  <div class='tit'>选择服务</div>
	  <div class='ques_list serviceList'> 
	    <div class='ques_item' service="退货退款">退货退款</div>
	    <div class='ques_item' service="退货换货">退货换货</div>
	  </div>  
	</div> 
	
	<div class='miaoshu_box' id="wentiMiaoshu" >
	  <div class='tit'>问题描述</div>
	  <textarea placeholder='请您描述问题的详细信息' id="info"></textarea>
	  <div class='img_tips'>请上传问题货品照片</div>
	  <div class='imgList' id="imgList"> 
	    <div class='imgItem item2' style="position:relative;">  
	      <img style='height:80px;' src='${skinPath }images/plus2.png'>
	      <input type="file" id="uploadImg" style="opacity: 0;position: absolute;top:0;left:0;width:100%;height:100%;">
	    </div>  
	  </div> 
	</div>
	
	
	 
	<div class='wuliu_box' id="wuliuBox">
	  <div class='tit'>请将您的货品寄回到以下地址，请输入退货快递单号</div>
	  <div class='dizhi'>退货地址：${store.pro}${store.city}${store.country}${store.address}</div>
	  <div class='shoujianren'>收件人：${store.name}</div>
	  <div class='mobile'>电话：${store.mobile}</div> 
	  
	  <div class='input_box' style='margin-top:10px;'>
	    <input type='text' id='expressName' placeholder='请输入物流公司名称'></input>
	  </div>  
	  <div class='input_box' style="margin-top:20px;"> 
	    <input type='text' id='expressCode' placeholder='请输入快递单号'></input>
	  </div>
	</div>
	<div class='show_tips'>非质量问题的退换货，运费请自理</div>
	<div class='tijiaoBtn' id='saveShouhou'>提交</div>
	
	 
	<div class="h55"></div>
	<input id="hidPageType" type="hidden" value="4" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script> 
		$(".f_personal a").addClass("hover");
	</script>
</body>
</html>
