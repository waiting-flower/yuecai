<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<title>用户头像上传</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
 <link href="${skinPath }css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/member.css?v=151209" rel="stylesheet" type="text/css" />
<script src="${skinPath }js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath }js/userHeadUrlFun.js?v=1408155" language="javascript" type="text/javascript"></script>

<style type="text/css">
.hide {
	display: none;
}

.show {
	display: block;
}

.pic_edit {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 100;
	background: #fff;
	opacity: .8
}
.pic_edit .head_span{ 
	width:100%;
	height:30px;
	padding:5px 0px;
	font-size:14px;
	line-height:30px;
	background:#f60;
	color:#fff;
	text-align:center;
	display:block;
}
#clipArea {
	margin: 0 auto;
	height: 300px;
	background: #f2f2f2;
}

#clipBtn {
	margin-top: 25px;
	background-color: #f60;
	color: #fff;
	padding: 8px 20px;
	border-radius: 5px;
	width: 40%;
}

#upload2 {
	margin-top: 25px;
	background-color: #90b830;
	color: #fff;
	padding: 8px 20px;
	border-radius: 5px;
	width: 40%;
	margin-left:9%; 
}

#hit {
	position: fixed;
	top: 19%;
	left: 9.375%;
	background: gainsboro;
}

.logo {
	position: absolute;
	bottom: 12%;
	z-index: 100;
	width: 46%;
	left: 27%;
}

@media screen and (max-height: 450px) {
	.show_labour .show_img {
		width: 75%;
		margin-top: 22%;
	}
	.show_labour .show5 {
		width: 80%;
		left: 10%;
		margin-top: 22%;
	}
	.show_labour .show5_btn {
		width: 62%;
	}
	#clipBtn, #upload2 {
		margin-top: 0px;
		padding: 5px 20px;
	}
}

.lazy_tip {
	z-index: 0;
	width: 0;
	height:0;
}
.lazy_cover {
	width: 100%;
	height: 100%;
	background-color: black;
	z-index: 1000;
	color: #4eaf7a;
	font-size: 25px;
	opacity: 0.7;
	position: fixed;
	top: 0;
	left: 0;
}

#plan {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	clear: both;
	height: 100%;
	display: none;
	background: rgb(255, 255, 255);
	vertical-align: baseline;
	text-align: center;
	line-height: 1.5;
	padding-top: 25%;
}

#plan canvas {
	clear: both;
}
</style>
</head>
<body fnav="1" class="g-acc-bg">
 <!--加载资源-->
<div class="lazy_tip" id="lazy_tip"><span>1%载入中......</span></div>
<div class="lazy_cover"></div>
<div class="resource_lazy hide"></div>
<div class="pic_edit">
	<span class="head_span">双指旋转和双指缩放</span>
	<div id="clipArea"></div>  
	<button id="upload2">上传照片</button>
    <button id="clipBtn">确认裁剪</button>
	<input type="file" id="file" style="opacity: 0;position: fixed;bottom: -100px">
		<div id="plan" style="display: none">
			比例剪切后尺寸
			<canvas id="myCanvas"></canvas>
		</div>
	</div>
<img src="" fileName="" id="hit" style="display:none;z-index: 9">
<div id="cover"></div>
    
    
<input id="hidPageType" type="hidden" value="-1" />
<input id="hidIsHttps" type="hidden" value="1" />
<input id="hidSiteVer" type="hidden" value="v47" />
<input id="hidWxDomain" type="hidden" value="" />
<input id="hidOpenID" type="hidden" value=""/>
<jsp:include page="../html/footer.jsp"></jsp:include>
</body>
</html>
