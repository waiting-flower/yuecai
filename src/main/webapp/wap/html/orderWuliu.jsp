<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>物流查询</title>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" />
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script id="pageJS" data="${skinPath}js/userWuliu.js?v=161012" language="javascript" type="text/javascript"></script>
<style>
.edit-wrapper {
    padding: 10px;
}

.edit-wrapper .inp-info {
    display: block;
    height: 36px;
    border: 1px solid #e6e6e6;
    border-radius: 3px;
    background-color: #fff;
    padding: 8px 10px;
    box-sizing: border-box;
    width: 100%;
    line-height: 20px;
    font-size: 14px;
    color: #666;
}

.edit-wrapper .txt {
    color: #bbb;
    font-size: 12px;
    line-height: 20px;
    word-break: break-all;
    white-space: nowrap;
    padding-top: 4px;
}
.edit-wrapper .s-btn {
    margin: 26px 5px 0;
    display: block;
    height: 45px;
    line-height: 45px;
    text-align: center;
    font-size: 16px;
    color: #fff;
    background-color: #f60;
    border-radius: 5px;
}
</style>
</head>
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	
	
	<div class="marginB" id="loadingPicBlock" style="margin-bottom:65px;">
		
		<div class="edit-wrapper">
            <input name="txtUserName" type="text" id="code" class="inp-info" maxlength="20" value="${danhao}">
            <p class="txt">输入您的物流单号</p>
            <a id="query_btn" href="javascript:;" class="s-btn">查询</a>
        </div>

		<div class="result-success" id="success" style="">
			<div class="result-top" id="resultTop">
				<span id="sortSpan" class="col1-up" title="切换排序">时间</span> <span
					class="col2">地点和跟踪进度</span>
			</div> 
			<ul id="result" class="result-list">
				<!-- 
				<li class=""><div class="col1">
						<dl>
							<dt>2017.10.10</dt>
							<dd>19:10</dd>
						</dl>
					</div>
					<div class="col2">
						<span class="step"><span class="line1"></span><span
							class="line2"></span><span class="point"></span></span>
					</div>
					<div class="col3">河北省石家庄市辛集市公司(点击查询电话) 已揽收</div></li>
				<li class=""><div class="col1">
						<dl>
							<dt>2017.10.10</dt>
							<dd>19:31</dd>
						</dl>
					</div>
					<div class="col2">
						<span class="step"><span class="line1"></span><span
							class="line2"></span><span class="point"></span></span>
					</div>
					<div class="col3">河北省石家庄市辛集市公司 已打包</div></li>
				<li class=""><div class="col1">
						<dl>
							<dt>2017.10.10</dt>
							<dd>19:59</dd>
						</dl>
					</div>
					<div class="col2">
						<span class="step"><span class="line1"></span><span
							class="line2"></span><span class="point"></span></span>
					</div>
					<div class="col3">河北省石家庄市辛集市公司 已发出,下一站 石家庄转运中心</div></li>
				<li class=""><div class="col1">
						<dl>
							<dt>2017.10.11</dt>
							<dd>02:55</dd>
						</dl>
					</div>
					<div class="col2">
						<span class="step"><span class="line1"></span><span
							class="line2"></span><span class="point"></span></span>
					</div>
					<div class="col3">石家庄转运中心 已收入</div></li>
				<li class=""><div class="col1">
						<dl>
							<dt>2017.10.11</dt>
							<dd>02:55</dd>
						</dl>
					</div>
					<div class="col2">
						<span class="step"><span class="line1"></span><span
							class="line2"></span><span class="point"></span></span>
					</div>
					<div class="col3">石家庄转运中心 已发出,下一站 淮安转运中心</div></li>
				<li class=""><div class="col1">
						<dl>
							<dt>2017.10.12</dt>
							<dd>21:33</dd>
						</dl>
					</div>
					<div class="col2">
						<span class="step"><span class="line1"></span><span
							class="line2"></span><span class="point"></span></span>
					</div>
					<div class="col3">淮安转运中心 已收入</div></li>
				<li class=""><div class="col1">
						<dl>
							<dt>2017.10.13</dt>
							<dd>01:05</dd>
						</dl>
					</div>
					<div class="col2">
						<span class="step"><span class="line1"></span><span
							class="line2"></span><span class="point"></span></span>
					</div>
					<div class="col3">淮安转运中心 已发出,下一站 江苏省徐州市睢宁县</div></li>
				<li class=""><div class="col1">
						<dl>
							<dt>2017.10.13</dt>
							<dd>08:06</dd>
						</dl>
					</div>
					<div class="col2">
						<span class="step"><span class="line1"></span><span
							class="line2"></span><span class="point"></span></span>
					</div>
					<div class="col3">江苏省徐州市睢宁县公司 已收入</div></li>
				<li class=""><div class="col1">
						<dl>
							<dt>2017.10.13</dt>
							<dd>09:45</dd>
						</dl>
					</div>
					<div class="col2">
						<span class="step"><span class="line1"></span><span
							class="line2"></span><span class="point"></span></span>
					</div>
					<div class="col3">
						江苏省徐州市睢宁县公司(点击查询电话)刘** 派件中 派件员电话<a
							href="/courier/detail_527FF0F96A92EFF95EEAA29709DCF3C3.html">15312666355</a>
					</div></li>
				<li class="last finish"><div class="col1">
						<dl>
							<dt>2017.10.13</dt>
							<dd>17:15</dd>
						</dl>
					</div>
					<div class="col2">
						<span class="step"><span class="line1"></span><span
							class="point"></span></span>
					</div>
					<div class="col3">客户 签收人: 本人签收 已签收 感谢使用圆通速递，期待再次为您服务</div></li>
				
				 -->
			</ul>
		</div>
	</div>
	
	
	<input id="hidPageType" type="hidden" value="4" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script> 
		$(".f_personal a").addClass("hover");
	</script>
</body>
</html>
