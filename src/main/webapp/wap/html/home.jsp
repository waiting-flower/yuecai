<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>  
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" name="viewport" />
    <meta content="telephone=no" name="format-detection" />
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
    <link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
    <link href="${skinPath}css/goods.css?v=161109" rel="stylesheet" type="text/css" />
    <link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
    <link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
    <link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
	<script src="${skinPath}js/app_plusGeolocation.js" type="text/javascript" charset="utf-8"></script>
    <script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
    <script id="pageJS" data="${skinPath}js/indexFun.js?G=xx22xx3cxwdf012" language="javascript" type="text/javascript"></script>
<title>商城首页</title>  
</head> 
<body fnav="1" class="g-acc-bg" style="background:#fff;">  
	<div class="pro-s-box thin-bor-bottom" id="divSearch" style="position:fixed;background:#fff;"> 
		<div class="dingwei" onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?location&returnUrl=index'"> 
			<i class="fa fa-map-marker"></i><span id="location_top_cityName">定位中..</span>
			<i class="fa fa-caret-down"></i> 
		</div>
		<div class="box">
			<div class="border"> 
				<div class="border-inner" style="background:#f7f7f7; "></div>
			</div>
			<div class="input-box"> 
				<i class="s-icon"></i> <input type="text" placeholder="搜索您喜欢的商品"
					id="txtSearch"> <i class="c-icon" id="btnClearInput"
					style="display: none "></i>
			</div>
		</div> 
		<a href="javascript:;" class="s-btn" id="btnSearch">搜索</a>
	</div>    
	<div class="search-info" style="display: none;bottom:0px;position:fixed;">
            <div class="hot">
                <p class="title">热门搜索</p>
                <ul id="ulSearchHot" class="hot-list clearfix">
                	<c:forEach items="${ keyList }" var="item"  varStatus="status">
                    <li wd="${item }"><a class="items">${item }</a>
                    </li> 
                    </c:forEach>
                </ul>
            </div>
            <div class="history" style="display: none">
                <p class="title">历史记录</p>
                <div class="his-inner" id="divSearchHotHistory"></div>
            </div>
        </div>

	<!-- 焦点图 -->
	<div class="hotimg-wrapper"> 
		<section id="sliderBox" class="hotimg">
			<ul class="slides" id="sliderBox_ul">
				<div class="loading clearfix">
					<b></b>正在加载
				</div>
			</ul>
		</section>  
	</div>
	 
	<div class='head_menu' style="margin-top:15px;display:none;">
		<div class='menu_icon'  onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?storeList&cateId=1'">
			<img src='${skinPath }images/hicon1.png'> <span>美容</span>
		</div>
		<div class='menu_icon'  onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?storeList&cateId=18'">
			<img src='${skinPath }images/hicon2.png'> <span>服装</span>
		</div>
		<div class='menu_icon'  onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?storeList&cateId=46'">
			<img src='${skinPath }images/hicon3.png'> <span>电脑办公</span>
		</div>   
		<div class='menu_icon'  onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?storeList&cateId=85'">
			<img src='${skinPath }images/hicon4.png'> <span>家居家装</span>
		</div> 
		<div class='menu_icon' onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?cateList'">
			<img src='${skinPath }images/hicon5.png'> <span>更多</span>
		</div>
	</div> 
	
	<div class="store_box" style="padding-bottom:0px;">  
		<div class="tit" style="position:relative;padding-top:0px;"> 
			新品上线    
		</div>  
		<div class="v2_new_goods_list">
			<c:forEach items="${ goodsList }" var="item">
			<a href="${pageContext.request.contextPath}/shopWap.html?goodsDetail&id=${item.id }"> 
				<div class="news_goods_box">
					<div class="goods_img_box">  
						<img alt="" src="${item.newsImgUrl }">
					</div>
					<div class="goods_name">${item.name }</div>
					<div class="now_price">￥${item.price }</div> 
				</div>    
			</a>
			</c:forEach> 
		</div> 
	</div>
	<div class="hr_driven"></div> 
	<div class="store_box" style="padding-top:0px;margin-top:0px;"> 
		<div class="tit" style="position:relative;margin-top:10px;"   onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?list&isTejia=1'"> 
			特价商品 <i class="right_i" style="right:5px;"></i> 
		</div>  
		<div class="news_goods_list_box">
			<c:forEach items="${ tejiaGoodsList }" var="item">
			<a href="${pageContext.request.contextPath}/shopWap.html?goodsDetail&id=${item.id }" style="margin-right:5px;" >
				<div class="news_goods_box">
					<div class="goods_img_box">  
						<img alt="" src="${item.zhekouImgUrl }">
					</div>
					<div class="goods_name">${item.name }</div>
					<div class="now_price"><span>限时价：</span>￥${item.price }</div> 
				</div>    
			</a>
			</c:forEach> 
		</div> 
	</div>
	
	<div class="hr_driven"></div> 	
	<div class="store_box" style="padding-bottom:0px;">  
		<div class="tit" style="position:relative;padding-top:0px;" 
			 onclick="javascript:location.href='${pageContext.request.contextPath}/shopWap.html?taobaoList'"> 
			免费福利  <i class="right_i" style="right:5px;"></i> 
		</div>  
		<div class="v2_new_goods_list" id="taobaoGoodsList">
		</div> 
	</div>
	
	
	<div class="my_msg" style="width:94%;margin:0 auto;background:#f7f7f7;border-radius:8px;">   
		<ul class="gonggao_list">    
		</ul>       
		<em class="more_notice" onclick="location.href='${pageContext.request.contextPath}/shopWap.html?noticeList'">更多</em>  
		<i class="right_i" onclick="location.href='${pageContext.request.contextPath}/shopWap.html?noticeList'"></i> 
	</div> 
			 
	
	<div class="h55"></div> 
	<div class="h30" style="width:100%;height:20px;"></div>
	<jsp:include page="../html/footer.jsp"></jsp:include>
	<script type="text/javascript">
	$(".f_home a").addClass("hover"); 
	function CreateScript(src){
        var el = document.createElement('script');
        console.log(el);
        el.src = src;
        el.async = true;
        el.defer = true;
        document.body.appendChild(el);
    }
   /**
    * 经纬度解析返回
    * @param rs
    * @returns 
    */ 
   function renderReverse(rs) {
       // var rs = JSON.stringify(rs); 
	   console.log(rs); 
       var city = rs["result"]['addressComponent']['city'];
       var county = rs["result"]['addressComponent']['district'];
       city = city.toString(); 
       county = county.toString();  
       var cityName = cookie.get("cityName");
       var countyName = cookie.get("countyName");
       cookie.set("cityName",city);
	   cookie.set("countyName",county);
       $("#location_top_cityName").html(city + "-" + county);   
   } 
		
   
   	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener('plusready', plusReady, false);
	}
   	function plusReady() {
   		if(navigator.userAgent.match(/Html5Plus/i)) {
   			//5+引擎环境
   			plus.geolocation.getCurrentPosition(function(p){
   				var lat = p.coords.latitude;
   			    var lon = p.coords.longitude;
   			    //获取到经纬度信息      
   			    var url = "https://api.map.baidu.com/geocoder/v2/";
   			    var data = { 
   			    		callback: 'renderReverse', 
   			    		location:lat + "," + lon,  
   			    		output : "json", 
   			    		pois : "1",   
   			    		ak: "GHsIH3jHrSrL0Q3gY82VfX4Bbp3VSCR7" 
   			            
   			    };
   			    //组合url 
   			    var buffer = []; 
   			    for (var key in data) {
   			        buffer.push(key + '=' + encodeURIComponent(data[key]));
   			    } 
   			    var fullpath = url + '?' + buffer.join('&');
   			    CreateScript(fullpath);
   			}, function(e){
   				alert('Geolocation error: ' + e.message);
   			} );  
   	 
   		} 
	}

		BNJSReady(function() { 
		    BNJS.ui.hideLoadingPage();
		    var lat = BNJS.location.latitude;
		    var lon = BNJS.location.longitude;
		    //获取到经纬度信息    
		    var url = "https://api.map.baidu.com/geocoder/v2/";
		    var data = { 
		    		callback: 'renderReverse', 
		    		location:lat + "," + lon,  
		    		output : "json", 
		    		pois : "1",   
		    		ak: "GHsIH3jHrSrL0Q3gY82VfX4Bbp3VSCR7" 
		            
		    };
		    //组合url 
		    var buffer = []; 
		    for (var key in data) {
		        buffer.push(key + '=' + encodeURIComponent(data[key]));
		    } 
		    var fullpath = url + '?' + buffer.join('&');
		    CreateScript(fullpath);
		}); 
	
	</script>
</body>
</html>