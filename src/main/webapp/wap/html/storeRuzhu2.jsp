<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>  	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>商家入驻</title> 
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<link href="${skinPath}css/comm.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/my.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/index.css?v=161109" rel="stylesheet" type="text/css" />
<link href="${skinPath}css/newShop.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/font-awesome.min.css?v=170522" rel="stylesheet" type="text/css" />
<link href="${skinPath }css/app.css?v=170522" rel="stylesheet" type="text/css" /> 
<script src="${skinPath}js/jquery190.js" language="javascript" type="text/javascript"></script>
<script src="${skinPath}js/upload/base64image.js" language="javascript" type="text/javascript"></script> 
<script id="pageJS" data="${skinPath }js/storeRuzhuFun2.js" language="javascript" type="text/javascript"></script>
</head>     
<body fnav="1"  id="loadingPicBlock"  class="g-acc-bg">
	<input type="hidden" value="${a.id }" id="storeId">
	<div class="shop_cart"> 
   		<div class="schedule"><ul><li><em>1</em>入驻商家信息</li><li class="on"><em>2</em>入驻商铺信息</li><li><em>3</em>审核完成</li></ul></div>
	</div>
	   
	<div class="store_ruzhu_box"> 
		<div class="title">店铺图片</div>
		<div class='logo_box'> 
			<div class='item'>  
				<div class='btn'>选择图片</div>
				<img id='imgUrl_img' src='http://a2.xzyouyou.com/FtrJjzy4WcA10nKamdx7x4FzBH7-' >
				<span>店铺logo图</span>
				<input type="file" id="imgUrl_file" style="opacity: 0;position: absolute;top:0;left:0;width:100%;height:100%;">
			</div>  
			<div class='item'>
				<div class='btn' bindtap='choiseFileQrCode'>选择图片</div>
				<img id='qrCode_img' src='http://a2.xzyouyou.com/FtrJjzy4WcA10nKamdx7x4FzBH7-' >
				<span>客服二维码</span>    
				<input type="file" id="qrCode_file" style="opacity: 0;position: absolute;top:0;left:0;width:100%;height:100%;">
			</div>
		</div> 
	</div>
	
	<div class="store_ruzhu_box"> 
		<div class="title">资质图片</div>
		<div class='logo_box'> 
			<div class='item'>  
				<div class='btn'>选择图片</div>
				<img id='yingyezhizhao_img' src='http://a2.xzyouyou.com/FtrJjzy4WcA10nKamdx7x4FzBH7-' >
				<span>营业执照</span>
				<input type="file" id="yingyezhizhao_file" style="opacity: 0;position: absolute;top:0;left:0;width:100%;height:100%;">
			</div>  
			<div class='item'>
				<div class='btn' bindtap='choiseFileQrCode'>选择图片</div>
				<img id='xiangguanzizhi_img' src='http://a2.xzyouyou.com/FtrJjzy4WcA10nKamdx7x4FzBH7-' >
				<span>相关资质</span>    
				<input type="file" id="xiangguanzizhi_file" style="opacity: 0;position: absolute;top:0;left:0;width:100%;height:100%;">
			</div>
		</div> 
	</div>
	 
	<div class="store_ruzhu_box"> 
		<div class="title">门店图片</div>
		<div class='logo_box'> 
			<div class='item'>
				<div class='btn'>选择图片</div>
				<img id='mendianzhaopai_img' src='http://a2.xzyouyou.com/FtrJjzy4WcA10nKamdx7x4FzBH7-' >
				<span>门店招牌</span>
				<input type="file" id="mendianzhaopai_file" style="opacity: 0;position: absolute;top:0;left:0;width:100%;height:100%;">
			</div>
			<div class='item'> 
				<div class='btn' bindtap='choiseFileQrCode'>选择图片</div>
				<img id='dianneihuanjing_img' src='http://a2.xzyouyou.com/FtrJjzy4WcA10nKamdx7x4FzBH7-'>
				<span>店内环境</span>  
				<input type="file" id="dianneihuanjing_file" style="opacity: 0;position: absolute;top:0;left:0;width:100%;height:100%;">
			</div>
		</div>
	</div>
	 
	 
	<div class="store_ruzhu_box">
		<div class="title">商家身份证图片</div>
		<div class='logo_box'>
			<div class='item'>
				<div class='btn'>选择图片</div>
				<img id='idCardA_img' src='http://a2.xzyouyou.com/FtrJjzy4WcA10nKamdx7x4FzBH7-' >
				<span>身份证正面</span>
				<input type="file" id="idCardA_file" style="opacity: 0;position: absolute;top:0;left:0;width:100%;height:100%;">
			</div>
			<div class='item'>
				<div class='btn' bindtap='choiseFileQrCode'>选择图片</div>
				<img id='idCardB_img' src='http://a2.xzyouyou.com/FtrJjzy4WcA10nKamdx7x4FzBH7-' >
				<span>身份证反面</span>  
				<input type="file" id="idCardB_file" style="opacity: 0;position: absolute;top:0;left:0;width:100%;height:100%;">
			</div> 
			<div class='item'> 
				<div class='btn' bindtap='choiseFileQrCode'>选择图片</div>
				<img id='idCardPerson_img' src='http://a2.xzyouyou.com/FtrJjzy4WcA10nKamdx7x4FzBH7-' >
				<span>手持身份证</span>   
				<input type="file" id="idCardPerson_file" style="opacity: 0;position: absolute;top:0;left:0;width:100%;height:100%;"> 
			</div>  
		</div>
	</div>


	<div class="btnBox" style="width:200px;">    
		<a class="btn btnBlue" id="ruzhuPreBtn" href="javascript:;" >上一步</a> 
		<a class="btn" id="ruzhuNextBtn" href="javascript:;" >下一步</a> 
	</div> 
			          
	<div class="h55" ></div>
	<input id="hidPageType" type="hidden" value="4" />
	<jsp:include page="../html/footer.jsp"></jsp:include>
</body>
</html>
