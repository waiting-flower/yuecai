/**
 *
 */
$(function () {
	var fail = function (f) {
		$.PageDialog.fail(f);
	};
	var ok = function (f) {
		$.PageDialog.ok(f);
	};  
    var init = function () {
    	$("#city_search_btn").click(function(){
			var searchKey = $("#city_search_key").val();
			if(searchKey == ""){
				fail("请输入关键词");
				return false; 
			}
			var key = encodeURI(encodeURI(searchKey));
            $.post(contextPath + "judgeIsCityButler",{
            	searchKey : searchKey
            },function(json){ 
            	if(json.code == "0"){
            		location.href = webPath + "searchResult&keyword=" + key;
            	}
            	else{ 
            		location.href = webPath + "cityShop&cityButlerId=" + json.id;
            	}
            },"json");
		});
    }; 
    Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160304",init);  
});