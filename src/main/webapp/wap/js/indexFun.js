/**
 *
 */
$(function () {

    var init = function () {
    	var initMenuIndex = function(){
    		var box = $("#sliderBox_ul");
    		$.getJSON(contextPath + "getMenuIndex", function (json) {
    			var list = json.list;
    			var imgUrl = "http://zhuanmi.xzyouyou.com/FrC0J6P6k53U2ZPcGwIyRPsydIjh";
    			for(var i = 0; i < list.length; i++){
    				var ui = "<li>";
    				var cateList = list[i].cateList;
    				var cateList1 = [];
    				var cateList2 = [];
    				if(cateList.length > 5){
    					cateList1 = cateList.slice(0,5);
    					cateList2 = cateList.slice(5,cateList.length);
    					console.log(cateList2); 
    					ui = ui + "<div style='width:100%;'>"
    					for(var j = 0; j < cateList1.length;j++){
        					var cate = cateList1[j]; 
        					ui = ui + '<div class="cateItem" cateId="' + cate.id +'"  style="display:inline-block;width:20%;">'
					   		+ ' 	<div style="width:42px;height:42px;margin:0 auto;">'
					   		+ '       <img alt="" style="width:42px;height:42px;"' 
					   		+ 'src="' + (cate.imgUrl ? cate.imgUrl : imgUrl) + '">'
					   		+ '    </div>' 
					   		+ '    <span style="display:block;margin-top:5px;width:100%;text-align:center;font-size:12px;color:#343434;">' + cate.name + '</span> '
					   		+ '</div>'
    					}
    					ui = ui + "</div>";
    					ui = ui + "<div style='width:100%;margin-top:15px;'>"
    					for(var j = 0; j < cateList2.length;j++){
        					var cate = cateList2[j]; 
        					ui = ui + '<div class="cateItem" cateId="' + cate.id +'"  style="display:inline-block;width:20%;">'
					   		+ ' 	<div style="width:42px;height:42px;margin:0 auto;">'
					   		+ '       <img alt="" style="width:42px;height:42px;" src="' + (cate.imgUrl ? cate.imgUrl : imgUrl) + '">'
					   		+ '    </div>' 
					   		+ '    <span style="display:block;margin-top:5px;width:100%;text-align:center;font-size:12px;color:#343434;">' + cate.name + '</span> '
					   		+ '</div>'
    					}
    					ui = ui + "</div>";
    				}
    				else{
    					cateList1 = cateList.slice(0,cateList.length);
    					ui = ui + "<div style='width:100%;'>"
    					for(var j = 0; j < cateList1.length;j++){
        					var cate = cateList[j];  
        					ui = ui + '<div class="cateItem" cateId="' + cate.id +'" style="display:inline-block;width:20%;">'  
					   		+ ' 	<div style="width:42px;height:42px;margin:0 auto;">'
					   		+ '       <img alt="" style="width:42px;height:42px;" src="' + (cate.imgUrl ? cate.imgUrl : imgUrl) + '">'
					   		+ '    </div>' 
					   		+ '    <span style="display:block;margin-top:5px;width:100%;text-align:center;font-size:12px;color:#343434;">' + cate.name + '</span> '
					   		+ '</div>'
    					}
    					ui = ui + "</div>";
    				}
    				ui = ui + "</li>";
    				var G = $(ui);
    				G.find(".cateItem").click(function(){ 
    					var itemId = $(this).attr("cateId");
    					if(itemId == "162"){  
    						location.href = webPath + "taobaoList";
    					}
    					else{
    						location.href = webPath + "storeList&cateId=" + itemId;	
    					}
    					
    				})
    				box.append(G); 
    			}
    			$("#sliderBox").flexslider({
    	            slideshow: true
    	        }); 
    		});
    	}
           
    	//加载轮播图片
        var o = function () {
            var z = $("#sliderBox");
            $.getJSON(contextPath + "getBannerList", "type=1", function (G) {
            	console.log(G);    
                if (G.code == 0) {
                    var F = G.list;
                    var E = "<ul>";
                    var C = ""; 
                    for (var D = 0; D < F.length; D++) {   
                        E += '<li style="background-color:' + F[D].alt + ';"><a href="' + F[D].href + '"><img src="' + F[D].imgUrl + '" alt="" /></a></li>'
                    }
                    E += "</ul>";
                    var A = $(E); 
                    A.addClass("slides");
                    z.empty().append(A).flexslider({
                        slideshow: true
                    });
                }
            });
        }; 
        o(); //加载轮播
        //initMenuIndex(); //加载轮播菜单
        initNoticeList(); 
        
        var cityName = cookie.get("cityName");
        var countyName = cookie.get("countyName");
        if(cityName == undefined){ 
         	
        } 
        else{ 
        	$("#location_top_cityName").html(cityName + countyName);
        } 
    }; 
    var userId = cookie.get("userId");
    if(userId != undefined){
    	$.getJSON(contextPath + "saveRobotLogin&userId=" + userId,function(json){
    		
    	});
    }
    var initNoticeList = function(){
    	var vdata = {
    		"currPage" : 1,
    		"pageSize" : 8
    	};
    	var nType = {
    		"1" : "公告",
    		"2" : "政策", 
    		"3" : "规范" 
    	};
    	$.getJSON(contextPath + "getNoticeList",vdata,function(json){
    		var len = json.listItems.length;
    		console.log(json); 
    		$.each(json.listItems,function(i,item){ 
    			var ui = '<li><span><em>' + nType[item.noticeType] + '</em>' + item.title + '</span></li>';
    			var G = $(ui);
    			G.click(function(){ 
    				location.href = webPath + "noticeDetail&id=" + item.id; 
    			});
    			$(".gonggao_list").append(G);
    		});
    		var index = 1; 
        	setInterval(function(){ 
        		var top = 0 - index * 45; 
        		$(".gonggao_list li:first").animate({ 
        			marginTop:  top + "px" 
        		},500,function(){    
        			index = index + 1;  
        			if(index >= len){
        				index = 0;
        			}
        		}); 
        	},3000);
    	});
    	
    	
    	var initTabao = function(){
    		$.getJSON(contextPath + "getTaobaoIndexData",function(json){
    			var list = json.goodsList; 
    			$.each(list,function(i,item){ 
    				console.log(item);  
    				var ui = '<a href="javascript:;">'
    					+ '    <div class="news_goods_box">'
    					+ '        <div class="goods_img_box">' 
    					+ '            <img alt="" src="' + item.picUrl + '">'
    					+ '        </div>'
    					+ '        <div class="goods_name_taobao">' + item.title + '</div>'   
    					+ '        <div class="now_price">￥' + item.pirce + '<em style="float:right">领券减：' + item.couponAmount + '</em></div>'
    					+ '    </div>'   
    					+ '</a>'; 
    				var G = $(ui);   
    				G.click(function(){
    					//var myUrl = encodeURIComponent("https:" + item.couponClickUrl); 
    					//BNJS.page.start('bainuo://web?url=' + myUrl);
    					location.href = item.couponClickUrl; 
    				});   
    				$("#taobaoGoodsList").append(G); 
    			});
    			console.log(json); 
    		});
    	}
    	initTabao();  
    }
    Base.getScript(Gobal.Skin + "js/Flexslider.js?v=160722", function(){
    	 Base.getScript(Gobal.Skin + "js/search.js?v=160722", init)
    });
});