$(function(){
	var init = function(){
		var c = function (t) {
            var myreg=/^[1][3,4,5,7,8][0-9]{9}$/;
            return myreg.test(t)
        }; 
		$("#btnReg").click(function(){
			var password = $("#password").val();
			var mobile = $("#mobile").val();
			var snsCode = $("#snsCode").val();
			if(password == null || password == ""){
				$.PageDialog.fail("请输入您的密码");
				return false;
			}
			if(password.length < 6 || password.length > 20){
				$.PageDialog.fail("密码应该在6-20位");
				return false; 
			}
			if(mobile == null || mobile == ""){
				$.PageDialog.fail("请输入您的手机号码");
				return false;
			}
			if(!c(mobile)){
				$.PageDialog.fail("请输入正确的手机号码格式");
				return false; 
			}
			if(snsCode == null || snsCode == ""){
				$.PageDialog.fail("请输入您收到的短信验证码");
				return false;
			}
			$.post(contextPath + "saveReg",{
				userName : mobile,
				password : password,
				mobile : mobile,
				snsCode : snsCode
			},function(json){ 
				if(json.code == 1){
					$.PageDialog.fail("您输入的用户名已存在，请重新输入");
				}
				else if(json.code == 2){
					$.PageDialog.fail("您输入的手机号码已经存在，请重新输入");
				}
				else if(json.code == 3){
					$.PageDialog.fail("您的短信验证码不正确");
				}
				else{
					$.PageDialog.ok("注册成功",function(){
						location.href = webPath + "index";
					});
				}
			},"json");
		});
		var time = 60;
		var inter; 
		$("#btnYzm").click(function(){
			var tempHtml = $("#btnYzm span").html();
			if(tempHtml != "获取短信验证码"){
				return false;
			}
			var mobile = $("#mobile").val();
			if(mobile == null || mobile == ""){
				$.PageDialog.fail("请输入您的手机号码");
				return false;
			} 
			if(!c(mobile)){
				$.PageDialog.fail("请输入正确的手机号码格式");
				return false;
			}   
			$.getJSON(contextPath + "sendSnsCode&snsType=reg&mobile=" + mobile,function(json){
				if(json.code == 1){
					$.PageDialog.fail("您输入的手机号码已经被注册");
				} 
				if(json.code == 2){ 
					$.PageDialog.fail("验证码发送失败");
				} 
				else{
					var auto = function(){
						var ttt = time;
						if(time < 10){
							ttt = "0" + time;
						}     
						var nttt = "重新发送" + ttt + "s"; 
						$("#btnYzm span").html(nttt);
						time--;  
						if(ttt <= 1){
							time = 60;    
							$("#btnYzm span").html("获取短信验证码");
							clearInterval(inter); 
						}
					} 
					inter = setInterval(auto, 1000);
				}
			});
		});
	}
	Base.getScript(Gobal.Skin + "/js/pageDialog.js?v=160606", init);
});