/**
 * 用户订单列表页面
 */
var record = null;
var opType = -1; 
var params = {
	currPage : 1,
	pageSize : 10
}
$(function(){
	var loading = $("#divLoading");
	var oBody = $("#allOrder");
	var hidType = $("#hidType").val();
	//dialog confrim
	var r = function (L, F, G, E, N) {
		
         var H = 255;
         var K = 126;
         if (typeof (E) != "undefined") {
             H = E
         }
         if (typeof (E) != "undefined") {
             K = N
         }
         var M = null;
         var O = '<div class="clearfix m-round u-tipsEject"><div class="u-tips-txt">' 
         	+ L + '</div><div class="u-Btn"><div class="u-Btn-li"><a href="javascript:;" id="btnMsgCancel" class="z-CloseBtn">取消</a></div><div class="u-Btn-li"><a id="btnMsgOK" href="javascript:;" class="z-DefineBtn">确定</a></div></div></div>';
         var J = function () {
             var P = $("#pageDialog");
             P.find("a.z-DefineBtn").click(function () {
                 if (typeof (F) != "undefined" && F != null) {
                     F();
                 }
                 I.close();
             });
             P.find("a.z-CloseBtn").click(function () {
                 if (typeof (G) != "undefined" && G != null) {
                     G()
                 }
                 I.cancel()
             })
         };
         var I = new $.PageDialog(O, {
             W: H,
             H: K,
             close: true,
             autoClose: false,
             ready: J
         })
     };
     
	var init = function(){
		var loadData = function(){
			
			var u = function(){
				var i = function(){
					return "currPage=" + params.currPage + "&pageSize=" + params.pageSize + "&opType=" + opType;
				} 
				console.log("从服务器获取数据 params=" + i());
				$.getJSON(contextPath + "getScoreOrderList",i(),function(json){
					console.log("获取服务器数据成功" + json.totalPage);
					loading.hide();
					if(json.totalPage <= params.currPage){
						console.log("没有下一页的数据了,设置状态变更isload=true");
						_IsLoading = true; 
					}
					else{
						_IsLoading = false;
					}
					if(json.code == 1){
						oBody.html(Gobal.NoneHtml);
					}
					else{ 
						$.each(json.listItems,function(i,item){
							
							var G = '<div class="user_orderbox">'
								+ '<div class="user_orderbox_hd"><span>单号：' + item.orderNo + '</span><span class="ord-status-txt-ts fr">';
								if(item.orderState == "1"){
									G += '等待商家发货 ';
								}
								else if(item.orderState == "2"){
									if(item.goodsType == "1"){
										G += '已发货，待收货';
									} 
									else{
										G += '已完成';
									}
									
								}
								
								else if(item.orderState == "3"){
									G += '已收货';
								}
								
								G += '</span></div>'
								+ '<!-- 商品列表 -->'
								+ '<div class="u_o_body">'
								$.each(item.goodsList,function(j,c){
									G +='<div class="u_goods_detail">'
										+ '<!-- 商品的图片 -->'
										+ '<div class="u_goods_img_box">'
										+ '<a href="' + webPath + 'scoreGoodsDetail&id=' + c.goodsId 
										+ '&sku=' + c.paramsInfoIds + '"><img class="u_goods_img" src="' 
										+ c.imgUrl + '" alt=""></a>'
										+ '</div>'
										+ '<!-- 商品的信息 -->'
										+ '<div class="u_g_info">' 
										+ '<h1 class="weui-media-box__desc"><a href="' + webPath + 'goodsDetail&id=' + c.goodsId + '&sku=' + c.paramsInfoIds + '" class="ord-pro-link">' + c.name + '</a></h1>'
										+ '<p class="weui-media-box__desc"></p>'
										+ '<div class="clear mg-t-10">'  
										+ '<div class="wy-pro-pri fl">积分：<em class="num font-15">' + c.score + '</em></div>'
										+ '<div class="pro-amount fr"><span class="font-13">数量×<em class="name">' + c.num + '</em></span></div>'
										+ '</div>'
										+ '</div>'
										+ '	</div>';
								});
								G+= '</div>' 
								+ '<!-- 商品列表结束 -->'
								+ '<div class="ord-statistics">' 
								+ '<span>共<em class="num">' + item.num + '</em>件商品，'
								+ '</span> <span class="wy-pro-pri">总积分：<em class="num font-15">' 
								+ item.score + '</em></span>'
								+ '<span>'
								+ '</span>' 
								+ '</div>'
								+ '<!-- 订单底部操作-->'
								+ '<div class="u_o_foot">'; 
								if(item.orderState == "2" && item.goodsType == '1'){
									G += '<a href="javascript:;" orderId="' + item.id + '" class="ords-btn-dele btn_receive_order">确认收货</a>';
								}   
								if(item.goodsType == "1"){
									G += '<a href="' + webPath + 'scoreOrderDetail&id=' + + item.id + '" class="ords-btn-info">订单详情</a>'
								} 
								else if(item.goodsType == "2"){
									G += '<a href="' + item.clickUrl + '" class="ords-btn-info">查看优惠券</a>'
								}
								+ '</div>'
								+ '</div>';
								var GL = $(G);
								GL.find("a.btn_receive_order").click(function(){
									var id = $(this).attr("orderId");
									var S = function(){
										$.getJSON(contextPath + "confirmReceiveScoreOrder&orderId=" + id,function(json){
											if(json.code == 0){ 
												$.PageDialog.ok("确认收货成功",function(){
													location.reload();
												});
											}
											else if(json.code == 1){
												$.PageDialog.fail(json.msg);
											}
											else if(json.code == 10){
												location.href = webPath + "login";
											}
										});
									}
									r("是否确认收货",S);
								}); 
								oBody.append(GL);
						});
					}
				})
			}
			this.initData = function(){
				u();
			}
			this.getNextPage = function(){
				params.currPage++;
				u();
			}
		}
		var nav = $(".weui-navbar"); 
		nav.find("a").click(function(){ 
			$(this).addClass("weui-bar__item--on").siblings().removeClass("weui-bar__item--on");
//			var tar = $(this).attr("tar");
//			$(".my_order_list").hide();
//			$("#" + tar).show();
			var toId = $(this).attr("toId");
			opType = toId;
			_IsLoading = false;
			params.currPage = 1;
			oBody.html("");
			record.initData();
		});
		record = new loadData();
		var changeData = function(){
			loading.show();
			record.getNextPage();
		}  
		console.log("hidType=" + hidType);   
		if(hidType != "-1" && hidType != ""){  
			nav.find("a").eq(parseInt(hidType)).click(); 
		}
		else{
			nav.find("a").eq(0).addClass("weui-bar__item--on");
			record.initData();
		}
		scrollForLoadData(changeData);
	};
	Base.getScript(Gobal.Skin + "/js/pageDialog.js?v=160606",init);
});