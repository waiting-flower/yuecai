$(function(){
	console.log(addressJSONList);   
	var letterArr = ["A","B","C","D","E","F","G","H","J","K","L","M","N","P","Q","R","S","T","W","X","Y","Z"];
	var location_scroll_list = $(".location_scroll_list"); 
	var ui = [];
	for(var i = 0; i < letterArr.length; i++){
		ui.push('<a href="javascript:;" id="link_' + letterArr[i] + '">' + letterArr[i] + '</a>');
		ui.push('<div class="city_list">');
		for(j = 0; j < addressJSONList.length; j++){
			var proItem = addressJSONList[j];
			var cityArr = proItem.city;
			if(cityArr == undefined){
				continue;   
			}
			for(var k = 0; k < cityArr.length; k++){
				var cityItem = cityArr[k];
				if(cityItem.cityCode != undefined && cityItem.cityCode.toUpperCase() == letterArr[i]){
					ui.push('<div class="city_item">' + cityItem.name + '</div>');
				}   
			} 
		}   
		ui.push('</div>');
	} 
	location_scroll_list.html(ui.join("")); 
	var height = $(window).height() - 100;  
	$(".neirong_scroll").css("height",height+ "px"); 
	var cityName = cookie.get("cityName");
    var countyName = cookie.get("countyName");
    if(cityName == undefined){ 
     	
    } 
    else{ 
    	$("#location_top_cityName").html(cityName + countyName);
    } 
    
	$(".letter_item").click(function(){
		var letter = $(this).attr("letter");  
		$(this).addClass("act").siblings().removeClass("act");
		document.location.hash = "link_" + letter; 
	});
	$(".xian_op").click(function(){
		$(".quxian_list").hide();
	});
	$(".city_item").click(function(){
		//改变城市
		var city = $(this).html();
		$.getJSON(myPath + "admin/assets/js/address.js",function(json){
			for(var i = 0; i < json.length; i++){
				var shouBreak = false;
				var item = json[i];//省份
				var cityArr = item.city;
				if(cityArr == undefined){ 
					continue; 
				}
				for(var j = 0; j < cityArr.length; j++){
					var cityItem = cityArr[j]; 
					var isBreak = false;
					if(cityItem != undefined && cityItem.name == city){
						console.log("找到了");  
						var quxianList = cityItem.district; 
						for(var k = 0; k < quxianList.length; k++){
							var item3 = quxianList[k]; 
							$(".quxian_list").show();
							var ui = "<div class='xian_item'>" + item3.name + "</div>";
							var G = $(ui);
							G.click(function(){
								var country = $(this).html();
								cookie.set("countyName",country);
								cookie.set("cityName",city);
								cookie.set("isOther","1");  
								var returnUrl = $("#returnUrl").val();
								location.href = webPath + returnUrl + "&isFromLocation=1"; 
							});  
							$(".neirong_scroll").append(G);
						}
						isBreak = true;
						break;  
					} 
					if(isBreak){shouBreak = true;break}
				}
				if(shouBreak){
					break; 
				}
			}     
		});
		
	});
	$("#now_dingwei_city").click(function(){
		var city = $(this).attr("city");
		var country = $(this).attr("country");
		cookie.set("countyName",country);
		cookie.set("cityName",city); 
		var returnUrl = $("#returnUrl").val();
		location.href = webPath + returnUrl;   
	});
	$(".hot_item").click(function(){
		var city = $(this).html();
		$.getJSON(myPath + "admin/assets/js/address.js",function(json){
			for(var i = 0; i < json.length; i++){
				var shouBreak = false;
				var item = json[i];//省份
				var cityArr = item.city;
				if(cityArr == undefined){ 
					continue; 
				}
				for(var j = 0; j < cityArr.length; j++){
					var cityItem = cityArr[j]; 
					var isBreak = false;
					if(cityItem != undefined && cityItem.name == city){
						console.log("找到了");  
						var quxianList = cityItem.district; 
						for(var k = 0; k < quxianList.length; k++){
							var item3 = quxianList[k]; 
							$(".quxian_list").show();
							var ui = "<div class='xian_item'>" + item3.name + "</div>";
							var G = $(ui);
							G.click(function(){
								var country = $(this).html();
								cookie.set("cityName",city);
								cookie.set("countyName",country);
								cookie.set("isOther","1"); 
								var returnUrl = $("#returnUrl").val();
								location.href = webPath + returnUrl + "&isFromLocation=1"; 
							});  
							$(".neirong_scroll").append(G);
						}
						isBreak = true;
						break;  
					} 
					if(isBreak){shouBreak = true;break}
				}
				if(shouBreak){
					break; 
				}
			}     
		}); 
	});
});