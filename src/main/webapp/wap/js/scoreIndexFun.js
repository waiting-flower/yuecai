$(function(){
	var recFun = null;
	var params = {
		currPage : 1,
		pageSize : 10,
		sortType : 1,  
		cateId :"" 
	};
	var divLoading = $("#divLoading");
	var _isLoading = false;
	var fail = function (f) {
		$.PageDialog.fail(f);
	};
	var ok = function (f) {
		$.PageDialog.ok(f);
	};
	 
	//dialog confrim
	var r = function (L, F, G, E, N) {
		
         var H = 255;
         var K = 126;
         if (typeof (E) != "undefined") {
             H = E
         }
         if (typeof (E) != "undefined") {
             K = N
         }
         var M = null;
         var O = '<div class="clearfix m-round u-tipsEject"><div class="u-tips-txt">' 
         	+ L + '</div><div class="u-Btn"><div class="u-Btn-li"><a href="javascript:;" id="btnMsgCancel" class="z-CloseBtn">取消</a></div><div class="u-Btn-li"><a id="btnMsgOK" href="javascript:;" class="z-DefineBtn">确定</a></div></div></div>';
         var J = function () {
             var P = $("#pageDialog");
             P.find("a.z-DefineBtn").click(function () {
                 if (typeof (F) != "undefined" && F != null) {
                     F();
                 }
                 I.close();
             });
             P.find("a.z-CloseBtn").click(function () {
                 if (typeof (G) != "undefined" && G != null) {
                     G()
                 }
                 I.cancel()
             })
         };
         var I = new $.PageDialog(O, {
             W: H,
             H: K,
             close: true,
             autoClose: false,
             ready: J
         })
     };
	//初始化
	var init = function(){
		var j = $("#li_top2");
		$(".good-list-box").scroll(function () {
            if ($(".good-list-box").scrollTop() > 100) {
                j.show();
            } else {
                j.hide();
            }
        })
        j.click(function(){
        	 $(this).hide();
        	 $(".good-list-box").animate({
                 scrollTop: 0
             }, 500);
        });
		var D = $("#ulOrderBy");
		$(".cate_box_list").find(".cate_item").click(function(event){
			event.stopPropagation();
			var itemId = $(this).attr("itemId");
			var itemName = $(this).attr("itemName");
			$(".cate_list").find("em").html(itemName); 
			params.cateId = itemId; 
			$(".cate_box_list").css("display","none");
			params.currPage = 1; 
			recFun.initData();
		});
        D.on("click", "li", function () {
        	var isCate = $(this).hasClass("isCate");
        	if(isCate){
        		if($(".cate_box_list").css("display") == "block"){
        			$(".cate_box_list").css("display","none");
        		}
        		else{ 
        			$(".cate_box_list").css("display","block");
        		}
        		return false;
        	}
            var F = $(this);
            F.addClass("current").siblings().removeClass("current");
            var E = F.attr("sortType");
            params.sortType = E; 
            if (F.index() == 3) {
                if (E == 30) {
                    F.attr("sortType", 31);
                    F.find("i:eq(1)").addClass("sort").siblings().removeClass("sort")
                } else {
                    F.attr("sortType", 30);
                    F.find("i:eq(0)").addClass("sort").siblings().removeClass("sort")
                }
            } else {
                F.parent().find("i").removeClass("sort");
            }
            params.currPage = 1;
            recFun.initData();
        });//排序点击事件 
        
		var loadData = function(){
			var u = function(){
				divLoading.show();
				var i = function(){
					return "currPage=" + params.currPage + "&pageSize=" + params.pageSize  
					+ "&sortType=" + params.sortType 
					+ "&cateId=" + params.cateId;
				} 
				console.log("请求参数=" + i()); 
				$.getJSON(contextPath + "getScoreGoodsList",i(),function(json){
					if(json.code == 1){//没有任何记录的情况
						_isLoading = true;   
						console.log("无记录显示图片"); 
						$("#noneTipDiv").html('<div class="wx1yyg-kylin-norecords"><i class="wx1yyg-kylin-icon kylin-null-06"></i><strong class="gray9">暂无商品哦~</strong></div>')
					}
					else{ 
						$("#goodsCount").html(json.totalRecords); 
						$.each(json.listItems,function(i,item){  
							var ui = '<div class="search_prolist_item item_longcover" id="' + item.id + '" >'
								+ '    <div class="search_prolist_item_inner" >'
								+ '        <div class="search_prolist_cover" rd="0-4-4">'
								+ '            <img class="photo" src="' + item.imgUrl + '">'
								+ '        </div>'
								+ '        <div class="search_prolist_info">'    
								+ '            <div class="search_prolist_title" style="height:34px;white-space: normal;">' + item.name 
								+ '            </div>'
								+ '            <div class="search_prolist_price">'   
								+ '                <strong >所需积分<em><span class="int">' + item.score + '</span></em> </strong>'
								+ '            </div>'  
								+ '        </div>' 
								+ '    </div>'
								+ '</div>'
							var G = $(ui); 
							G.click(function () { 
								var goodsType = item.goodsType;
								if(goodsType == "1"){
									location.href = webPath + "scoreGoodsDetail&id=" + $(this).attr("id");
								}
								else{
									var S = function(){   
										var goodsId = item.id; 
										$.getJSON(contextPath + "createScoreCouponGoods&id=" + goodsId,function(json){
											if(json.code == 0){
												ok("兑换成功",function(){ 
													location.href = item.clickUrl;
												});
											}
											else{  
												fail("积分不足");
											}
										});
							    	}  
							    	r("您确定要兑换该优惠券吗？", S)
								}
	                        });
							$("#itemList").append(G); 
						}); 
						if(json.totalPage <= params.currPage){
							console.log("没有更多记录哦"); 
							$("#noneTipDiv").html(Gobal.NoneHtml2("没有更多记录了")); 
							_isLoading = true;
						} 
						else{
							_isLoading = false;
						}
					}
					divLoading.hide();
				});
			}
			this.initData = function(){ 
				$("#itemList").empty(); 
				u();
			}
			this.getNextPage = function(){
				params.currPage++;
				u();
			}
		} 
		recFun = new loadData();
		recFun.initData();
		$(".good-list-box").scroll(function(){
			var o = $(this);
			var theight = o.height();
//			console.log("theight=" + theight);
			var uiDivHeight = $(".goods_list").height();
			var top = o.scrollTop();
//			console.log("top=" + top);
			var scp = top + theight;
			if(scp + 222 > uiDivHeight){
				console.log("满足条件1");
				if(!_isLoading){
					console.log("获取分页");
					recFun.getNextPage();
					_isLoading = true;
				}
			}
		}); 
		var isNew = $("#isNew").val();
		if(isNew == "1"){ 
			$("#ulOrderBy").find("li").eq(2).click();
		}
	};
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160304",function(){
		Base.getScript(Gobal.Skin + "js/search.js?v=160523", init);
	}); 
});