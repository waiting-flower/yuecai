var isCanvasSupported = function() {
	var elem = document.createElement('canvas');
	return !!(elem.getContext && elem.getContext('2d'));
}

// 压缩方法
var compress = function(event, callback) {
	if (typeof (FileReader) === 'undefined') {
		console.log("当前浏览器内核不支持base64图标压缩");
		// 调用上传方式 不压缩
	} else {
		try {
			var file = event.currentTarget.files[0];
			if (!/image\/\w+/.test(file.type)) {
				alert("请确保文件为图像类型");
				return false;
			}
			var reader = new FileReader();
			reader.onload = function(e) {
				var image = $('<img/>');
				image.load(function() {
					console.log("开始压缩");
					var square = 700;
					var canvas = document.createElement('canvas');
					var imageWidth;
					var imageHeight;
					imageHeight = Math.round(square * this.height
							/ this.width);
					imageWidth = square;
					canvas.width = square;
					canvas.height = imageHeight;
					var context = canvas.getContext('2d');
					context.drawImage(this, 0, 0, imageWidth,
							imageHeight);
					var data = canvas.toDataURL('image/jpeg',0.7);
					// 压缩完成执行回调
					callback(data); 
				});
				image.attr('src', e.target.result);
			};
			reader.readAsDataURL(file);
		} catch (e) {
			console.log("压缩失败!");
			// 调用上传方式 不压缩
		}
	}
}