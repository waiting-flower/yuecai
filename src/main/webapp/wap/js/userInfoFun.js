$(function () {
    var a = function () {
        var e = function (i) {
            $.PageDialog.fail(i)
        };
        var h = function (j, i) {
            $.PageDialog.ok(j, i)
        };

        var b = false;
        var c = {
            userSex: ""
        };
        var f = function (i) {
            var j = {
                sex: c.userSex,
            }; 
            $.post(contextPath + "updateUserInfo", j, function (k) {
                var l = k.code;
                if (l == 0) {
                    if (typeof (i) == "function") {
                        i()
                    } 
                    h("保存成功")
                } else {
                    if (l == 1) {
                        var m = k.num;
                        if (m == -2 || m == -9) {
                            e("昵称已存在")
                        } else {
                            if (m == -4 || m == -5) {
                                e("签名不符合网站规范")
                            } else {
                                e("保存失败[" + m + "]")
                            }
                        }
                    } else {
                        if (l == 10) {
                            location.reload()
                        } else {
                            if (l == -4) {
                                e("昵称不符合规则")
                            } else {
                                e("保存失败,请重试[" + l + "]")
                            }
                        }
                    }
                }
                b = false
            }, "json")
        };
        $("#selSex").bind("change", function () {
            var l = $(this);
            var i = parseInt(l.val());
            var k = l.find("option[value='" + i + "']");
            if (k.length > 0) {
                var j = k.text();
                c.column = "userSex";
                c.userSex = i;
                f(function () {
                    $("#hidSex").val(l.val());
                    l.prev().html(j)
                })
            }
        });
        var hidSex = $("#hidSex").val();
        $("#selSex").val(hidSex); 
    };
    Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160304", a)
}); 
 