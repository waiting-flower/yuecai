/**
 * 
 */
$(function(){
	 
	
	var init = function(){
		//dialog confrim
		var r = function (L, F, G, E, N) {
			
	         var H = 255;
	         var K = 126;
	         if (typeof (E) != "undefined") {
	             H = E
	         }
	         if (typeof (E) != "undefined") {
	             K = N
	         }
	         var M = null;
	         var O = '<div class="clearfix m-round u-tipsEject"><div class="u-tips-txt">' 
	         	+ L + '</div><div class="u-Btn"><div class="u-Btn-li"><a href="javascript:;" id="btnMsgCancel" class="z-CloseBtn">取消</a></div><div class="u-Btn-li"><a id="btnMsgOK" href="javascript:;" class="z-DefineBtn">确定</a></div></div></div>';
	         var J = function () {
	             var P = $("#pageDialog");
	             P.find("a.z-DefineBtn").click(function () {
	                 if (typeof (F) != "undefined" && F != null) {
	                     F();
	                 }
	                 I.close();
	             });
	             P.find("a.z-CloseBtn").click(function () {
	                 if (typeof (G) != "undefined" && G != null) {
	                     G()
	                 }
	                 I.cancel()
	             })
	         };
	         var I = new $.PageDialog(O, {
	             W: H,
	             H: K,
	             close: true,
	             autoClose: false,
	             ready: J
	         })
	     };
	    
	    $("#closeDeal").click(function(){
	    	var id = $("#hidOrderId").val();
			var S = function(){   
				location.href = webPath + "orderCancle&id=" + id;
			} 
			r("是否确定取消订单",S);
	    });
		$("#btn_confirm_receive").click(function(){
			var id = $("#hidOrderId").val();
			var S = function(){
				$.getJSON(contextPath + "confirmReceiveOrder&orderId=" + id,function(json){
					if(json.code == 0){
						$.PageDialog.ok("确认收货成功",function(){
							location.reload();
						});
					} 
					else if(json.code == 1){
						$.PageDialog.fail(json.msg);
					}
					else if(json.code == 10){
						location.href = webPath + "login";
					}
				});
			}
			r("是否确认收货",S);
		});
	}; 
	Base.getScript(Gobal.Skin + "/js/pageDialog.js?v=160606",init);
});