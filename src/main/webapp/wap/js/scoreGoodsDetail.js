/**
 * 
 */
$(function(){
	var fail = function (f) {
		$.PageDialog.fail(f)
	};
	var ok = function (f,r) {
		$.PageDialog.ok(f,r)
	};
	var goodsId = $("#goodsId").val();
	var goodsType = $("#goodsType").val();
	var buyNum = $("#buyNum");  
	var init = function(){ 
		var imgArr = imgList.split(",");
		var ui = "";
		$.each(imgArr,function(i,item){
			ui += '<div class="slide"><img style="width:100%;" src="' + item + '"></div>';
		})
		$("#slider_img_box").html(ui);
        $('.slider8').bxSlider({ 
			infiniteLoop: true, 
			adaptiveHeight: true,  
			startSlides: 1, 
	        slideMargin: 10,  
	        auto: true,  
	        pager: true,                           // true / false - display a pager 
	        pagerSelector: '#slider_pager',                // jQuery selector - element to contain the pager. ex: '#pager' 
	        pagerType: 'short',                  // 'full', 'short' - if 'full' pager displays 1,2,3... if 'short' pager displays 1 / 4   如果设置full，将显示1，2，3……，如果设置short，将显示1/4 . 
	        pagerLocation: 'bottom',            // 'bottom', 'top' - location of pager 页码的位置 
	        pagerShortSeparator: '/'          // string - ex: 'of' pager would display 1 of 4 页面分隔符 
	    });//图片轮播组件
        
        var initClickEvent = function(){
        	//商品数量变更的点击事件
        	$("#btn_minus").click(function(){
        		var n = parseInt(buyNum.val());
        		if(n <= 1){
        			$("#btn_minus").addClass("disable")
        			return false; 
        		}
        		buyNum.val(n - 1);
        		refreshTip();
            });
            $("#btn_plus").click(function(){
            	var n = parseInt(buyNum.val());
            	buyNum.val(n + 1);
            	if($("#btn_minus").hasClass("disable")){
            		$("#btn_minus").removeClass("disable")
            	}
            	refreshTip();
            }); 
        };
        initClickEvent();
        $("#btn_kaituan").click(function(){
        	//立即兑换按钮
        	startBuyGoods();
        });
	};  
	
	var startBuyGoods = function(){ 
		var goodsNum = buyNum.val(); 
		var i = function(){
			return "id=" + goodsId + "&num=" + goodsNum;
		} 
		console.log(i()); 
		$.getJSON(contextPath + "createScoreGoodsReady",i(),function(json){
			if(json.code == 10){
				$.PageDialog.ok("您尚未登录",function(){
					location.href = webPath + "login";   
				});
				return false;
			}
			if(json.code == 1){ 
				$.PageDialog.fail(json.msg);
				return false;
			}
			else if(json.code == 0){      
//				alert("购买的链接地址为：" + webPath + "confirmOrder=1&orderId=" + json.orderId);
				location.href = webPath + "scoreGoodsConfirm&goodsId=" + goodsId + "&num=" + goodsNum;
			}
		});
    	$("#pintuanLayer").removeClass("show"); 
	};
	 
	    
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160304", function () {
		Base.getScript(Gobal.Skin + "js/jquery.bxslider.js?v=160523", function(){
			Base.getScript(Gobal.Skin + "js/CartComm.js?v=160523", init);
			
		});
    });
});