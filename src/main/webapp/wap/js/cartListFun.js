/**
 * 
 */
$(function(){
	
	var init = function(){
		var body = $("#cartBody");
	    var mycartpay = $("#mycartpay");
	   
		var refreshMoney = function(){
			var findLi = body.find("li");
			var len = findLi.length; 
			var price = 0;
			var allNum = 0;
			for(var i = 0; i < len; i++){
				var item = findLi[i];  
				var p = $(item).attr("price");//单个的价格
				var num = $(item).find(".txtNum").val();
				allNum = allNum + parseInt(num)
				price = price + parseFloat(p) * parseInt(num);
			}
			price = parseFloat(price).toFixed(2); 
			$("#allPrice").html(price); 
			$("#goodsNum").html(allNum);
		}  
		var listCart = function(json){
			if(json.code == 1){
				var k = $("#divNone"); 
				k.html("");
				k.append('<s></s><p>主银，您的购物车还是空的哦~</p><a href="' + webPath + 'cateList' + '" class="orangeBtn">立即去购买</a>').show();
				mycartpay.hide();
			}
			else{
				
				$.each(json.list,function(i,cartItem){ 
					var inerHtml = [];    
					var goodsList = cartItem.goodsList; 
					inerHtml.push('<div><div class="cart_store_name" >' + cartItem.storeName + '</div>');
					for(var i = 0; i < goodsList.length; i++){
						var item = goodsList[i];  
						var skuStr = "";
						var sku = JSON.parse(item.sku);
						for(var kk in sku){
							console.log(kk);
							console.log(sku[kk]);  
							skuStr += kk + "&nbsp;" + sku[kk] + "&nbsp;";
						} 
						inerHtml.push('<li price="' + item.price + '"  goodsId="' + item.goodsId + '" cartId="' + item.cartId + '">');  
						inerHtml.push('		<a class="fl u-Cart-img" href=".html">');
						inerHtml.push('		<img src="' + item.imgUrl + '" src2="' + item.imgUrl + '" border="0" alt="">');
						inerHtml.push('		</a>'); 
						inerHtml.push('		<div class="u-Cart-r">');  
						inerHtml.push('			<a href="" class="gray6">' + item.goodsName + item.goodsName2 + '</a>');
						inerHtml.push('			<span class="gray9">');  
						inerHtml.push('			<em>价格:￥' + item.price + '</em>');
						inerHtml.push('         <em>&nbsp;/&nbsp;</em><em class="gray9" style="margin: 0;">' + skuStr + '</em>');
						inerHtml.push('			</span>');
						inerHtml.push('		<div class="num-opt">');
						inerHtml.push('			<em class="num-mius" ><i></i></em>'); 
						inerHtml.push('			<input class="txtNum" name="num" maxlength="6" type="text" value="' + item.num + '" codeID="1" />');
						inerHtml.push('			<em class="num-add"><i></i></em>');
						inerHtml.push('		</div> ');    
						inerHtml.push('		<a href="javascript:;" cartId="' + item.cartId + '" name="delLink" id="test" class="z-del"><s></s></a>');
						inerHtml.push('		</div>');
						inerHtml.push('</li>');
					}
					inerHtml.push("</div>");   
					var G = $(inerHtml.join(""));
					var txtNum = G.find("#txtNum");  
					if(parseInt(txtNum.val()) < 2){
						G.find(".num-mius").addClass("dis");
					} 
					G.find(".num-mius").click(function(){
						if($(this).hasClass("dis")){
							return false;
						} 
						var cartId = G.find("li").attr("cartId");
						var nowNum = $(this).parent().find(".txtNum");
						var newNum = parseInt(nowNum.val()) - 1;
						if(newNum == 0){
							$(this).addClass("dis");
							return;
						}
						$.getJSON(contextPath + "updateCart&id=" + cartId + "&num=" + newNum,function(json){
							if(json.code == 0){
								nowNum.val(newNum);
								refreshMoney();
							}
						});
						
					});   
					G.find(".num-add").click(function(){
						var cartId = G.find("li").attr("cartId");
						var nowNum = $(this).parent().find(".txtNum");
						var newNum = parseInt(nowNum.val()) + 1;
						if(newNum > 1){ 
							G.find(".num-mius").removeClass("dis");
						} 
						$.getJSON(contextPath + "updateCart&id=" + cartId + "&num=" + newNum,function(json){
							if(json.code == 0){
								nowNum.val(newNum) ;
								refreshMoney();
							}
						});
					});
					G.find("a.z-del").click(function(){
						var O = $(this);
						var S = function(){   
							var cartId = O.attr("cartId"); 
							$.getJSON(contextPath + "deleteCart&id=" + cartId,function(json){
								if(json.code == 0){
									ok("删除成功",function(){
										location.reload();  
									});
								}
							});
				    	}
				    	r("您确定要删除该商品吗？", S)
					});   
					body.append(G);
				});   
				mycartpay.show(); 
				refreshMoney();
			}
			$(".loading").hide();
		};
		$.getJSON(contextPath + "getCartData",listCart);
		//dialog confrim
		var r = function (L, F, G, E, N) {
			
	         var H = 255;
	         var K = 126;
	         if (typeof (E) != "undefined") {
	             H = E
	         }
	         if (typeof (E) != "undefined") {
	             K = N
	         }
	         var M = null;
	         var O = '<div class="clearfix m-round u-tipsEject"><div class="u-tips-txt">' 
	         	+ L + '</div><div class="u-Btn"><div class="u-Btn-li"><a href="javascript:;" id="btnMsgCancel" class="z-CloseBtn">取消</a></div><div class="u-Btn-li"><a id="btnMsgOK" href="javascript:;" class="z-DefineBtn">确定</a></div></div></div>';
	         var J = function () {
	             var P = $("#pageDialog");
	             P.find("a.z-DefineBtn").click(function () {
	                 if (typeof (F) != "undefined" && F != null) {
	                     F();
	                 }
	                 I.close();
	             });
	             P.find("a.z-CloseBtn").click(function () {
	                 if (typeof (G) != "undefined" && G != null) {
	                     G()
	                 }
	                 I.cancel()
	             })
	         };
	         var I = new $.PageDialog(O, {
	             W: H,
	             H: K,
	             close: true,
	             autoClose: false,
	             ready: J
	         })
	     };
	     //结算按钮点击事件
	     $("#a_payment").click(function(){ 
	    	 var S = function(){
	    		 $.getJSON(contextPath + "createCartOrder&money=" + $("#allPrice").html() + "&goodsNum=" + $("#goodsNum").html(),function(json){
	    			 //生成订单
	    			 if(json.code == 0){
	    				 //订单生成之后清空购物车
	    				location.href = webPath + "confirmCartOrder&tradeNo=" + json.tradeNo;//进入订单确认页面页面
	    			 }
	    			 else if(json.code == 10){ 
	    				 location.href = webPath + "login"; 
	    			 }
	    			 else{ 
	    				 
	    				 $.PageDialog.fail("订单生成失败，请重新尝试");
	    			 }
	    		 });
	    	 }
	    	 var c = $("#goodsNum").html();
			 var m = $("#allPrice").html();
			 S(); 
//	    	 r("是否确认生成订单，商品数量：" + c + "<br/>订单总金额：" + m, S);
	     });
	     
	}
	
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160606",init);
});