$(function(){
	var obj = null;
	var loading = $("#divLoading");
	var bd = $("#ul_body");
	var params = {
		currPage : 1,
		pageSize : 10,
		noticeType : 0
	};
	var init = function(){
		var loadData = function(){
			var u = function(){
				var i = function(){
					return "currPage=" + params.currPage + "&pageSize=" + params.pageSize + "&noticeType=" + params.noticeType;
				}
				$.getJSON(contextPath + "getNoticeList",i(),function(json){
					loading.hide(); 
					if(json.code == 1){
						bd.html(Gobal.NoneHtml);
					}
					else if(json.code == 0){
						$.each(json.listItems,function(i,item){
							var G = '<li>'
						         + '       <a href="' + webPath + 'noticeDetail&id=' + item.id + '">'
						         + '   		<div class="pages-article-relate_item-content">'
						         + '			<div class="pages-article-relate_image bigImgContainer article-img"><img class="galleryLink" uc-image-reader_state="disabled" width="110" height="82" src="' + item.imgUrl + '"></div>'
						         + '   			<div class="relatebox-articleinfo">'
						         + '   				<h4 id="pages-article-relate_item-title" class="pages-article-relate_item-title">' + item.title + '</h4>'
						         + '   				<div class="relatebox-articleinfo-desc">'
						         + '                       <span id="pages-article-relate_item-wmname">'
						         + '                       	<i class="fa fa-eye"></i>&nbsp;&nbsp;' + item.lookCount
						         + '                       </span>'
						         + '                       <span class="pages-article-relate_item-wmname">' + item.createDate + '</span>'
						         + '   				</div>'
						         + '   			</div>'
						         + '         <div class="pages-article-relate_item-border-container">'
						         + '           <div id="pages-article-relate_item-border" class="pages-article-relate_item-border"></div>'
						         + '         </div>'
						         + '   		</div>'
						         + '    	</a>'
						         + '   </li>';
							bd.append(G);
						});
						
					}
				});
			}
			this.getFirstPage = function(){
				u();
			}
			this.getNextPage = function(){
				params.currPage++;
				u();
			}
		};
		var clickEvent = function(){
			$(".quan_navbar_inner a").click(function(){
				$(this).addClass("cur").siblings().removeClass("cur");
				loading.show();
				var nt = $(this).attr("nt");
				params.noticeType = nt;
				params.currPage = 1;
				bd.html("");
				obj.getFirstPage();
			});
		};  
		obj = new loadData();
		obj.getFirstPage(); 
        scrollForLoadData(obj.getNextPage);
		clickEvent(); 
	}
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160304",init);
});