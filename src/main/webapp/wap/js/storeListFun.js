$(function(){
	var divLoading = $("#divLoading");
	var recFun = null;
	var params1 = {
		currPage : 1,
		pageSize : 10
	};
	//dialog confrim
	var r = function (L, F, G, E, N) {
		
         var H = 255;
         var K = 126;
         if (typeof (E) != "undefined") {
             H = E
         }
         if (typeof (E) != "undefined") {
             K = N
         }
         var M = null;
         var O = '<div class="clearfix m-round u-tipsEject"><div class="u-tips-txt">' 
         	+ L + '</div><div class="u-Btn"><div class="u-Btn-li"><a href="javascript:;" id="btnMsgCancel" class="z-CloseBtn">取消</a></div><div class="u-Btn-li"><a id="btnMsgOK" href="javascript:;" class="z-DefineBtn">确定</a></div></div></div>';
         var J = function () {
             var P = $("#pageDialog");
             P.find("a.z-DefineBtn").click(function () {
                 if (typeof (F) != "undefined" && F != null) {
                     F();
                 }
                 I.close();
             });
             P.find("a.z-CloseBtn").click(function () {
                 if (typeof (G) != "undefined" && G != null) {
                     G()
                 }
                 I.cancel()
             })
         };
         var I = new $.PageDialog(O, {
             W: H,
             H: K,
             close: true,
             autoClose: false,
             ready: J
         })
     };
	var init = function(){
		 
		var g = function(){
			var data = function(){
				var i = function(){    
					return "currPage=" + params1.currPage + "&pageSize=" + params1.pageSize + "&cateCode=" + $("#cateId").val();
				} 
				$.getJSON(contextPath + "getStoreList",i(),function(json){
					divLoading.hide(); 
					if(json.code == 10){
						location.href = webPath + "login";
					}
					else if(json.code == 1){
						$("#storeList").html(Gobal.NoneHtmlEx("更多商户正在入驻审核中"));
						_IsLoading = true;
					} 
					else{ 
						$.each(json.listItems,function(i,item){
							var G = '<div class="store_item">'
							+ '			<div class="store_img_box">'
							+ '				<img alt="" src="' + item.imgUrl + '">'
							+ '			</div>'
							+ '			<div class="content_box">'
							+ '				<div class="name">' + item.name + '</div>'
//							+ '				<span class="num">已经有' + item.collectNum + '人关注</span>'
							+ '				<span class="juli" style="color:#999">' + item.address + '</span>'
							+ '			</div>';  
//							if(item.isCollect == 0){
//								G = G + '			<div class="op_box">'
//								+ '				<div class="not_guanzhu guanzhubtn">'
//								+ '					<i class="fa fa-heart"></i>'
//								+ '					关注'
//								+ '				</div>'
//								+ '			</div>';
//							}
//							else{  
//								G = G + '			<div class="op_box">'
//								+ '				<div class="yi_guanzhu guanzhubtn">'
//								+ '					已关注'
//								+ '				</div>'
//								+ '			</div>';
//							}  
							G = G + '		</div> ';
							var temp = $(G); 
							temp.click(function(){ 
								location.href = webPath + "storeDetail&id=" + item.id;
							}).find(".guanzhubtn").click(function(T){
								var _this = $(this);
								stopBubble(T);   
								if(_this.hasClass("yi_guanzhu")){
									//取消关注
									$.getJSON(contextPath + "saveCancleStoreCollect",{
					        			id : item.id
					        		},function(json){
					        			if(json.code == 10){
					        				location.href = webPath + "login";
					        			}
					        			else if(json.code == 0){ 
					        				$.PageDialog.ok("取消关注成功",function(){
					        					temp.find(".guanzhubtn").removeClass("yi_guanzhu").addClass("not_guanzhu");   
					        					temp.find(".guanzhubtn").html('<i class="fa fa-heart"></i>关注');
					        				});  
					        			}
					        			else if(json.code == 1){ 
					        				$.PageDialog.ok("取消关注成功");
					        			}
					        		});
								}
								else{
									//添加关注
									$.getJSON(contextPath + "saveStoreCollect",{
					        			id : item.id
					        		},function(json){ 
					        			if(json.code == 10){
					        				location.href = webPath + "login";
					        			} 
					        			else if(json.code == 0){
					        				$.PageDialog.ok("关注成功",function(){
					        					temp.find(".guanzhubtn").removeClass("not_guanzhu").addClass("yi_guanzhu");   
					        					temp.find(".guanzhubtn").html('已关注');
					        				}); 
					        			}
					        			else if(json.code == 1){
					        				$.PageDialog.ok("关注成功");
					        			}
					        		});
								}
                                                                                                                                              							});
							$("#storeList").append(temp);   
						});
						if(json.totalPage <= params1.currPage){
							console.log("没有数据了，所以不需要继续加载了"); 
							$("#storeList").append(Gobal.NoneHtml2("没有更多记录了"));
							_IsLoading = true; 
						}
						else{
							_IsLoading = false; 
						}
					}  
					
				});
			}
			this.initData = function(){
				data();
			} 
			this.getNextData = function(){
				params1.currPage++; 
				data();
			}
		}
		recFun = new g();
		recFun.initData();
		var e = function(){
			recFun.getNextData();
		}
		scrollForLoadData(e);
	};
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160304",init);
})