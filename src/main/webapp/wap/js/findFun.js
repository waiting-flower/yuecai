$(function () {
    var init = function () {
        //发现的轮播banner事件
        var o = function () {
            var z = $("#sliderBox");
            $.getJSON(contextPath + "getBanner&type=2", function (G) {
                if (G.code == 0) {
                    var F = G.listItems;
                    var E = "<ul>";
                    var C = "";
                    for (var D = 0; D < F.length; D++) {
                        C = F[D].url;
                        if (C.indexOf("?") > -1) {
                            C += "&pf=weixin"
                        } else {
                            C += "?pf=weixin"
                        }
                        var B = C.replace("http://m.1yyg.com", Gobal.WxDomain + "/" + Gobal.SiteVer);
                        E += '<li style="background-color:' + F[D].alt + ';"><a href="' + B + '"><img src="' + F[D].src + '" alt="" /></a></li>'
                    }
                    E += "</ul>";
                    var A = $(E);
                    A.addClass("slides");
                    z.empty().append(A).flexslider({
                        slideshow: true
                    })
                }
            });
        };
        o(); //banner
    };
    var art = function () {
        var a = $("#more_goods_article");
        var s = function (json) {
            $.each(json.listItems, function (i, item) {
                var tags;  
                if (item.tags != "" && item.tags != null) {
                    tags = item.tags.split(",");
                } else { 
                    tags = null;
                }  
                var htmls = '<div class="myyk-pro-items">' + '<div class="pro-upuser">' + '	<a href="#"><img src="http://www.17sucai.com/preview/138800/2016-11-01/web_-_%E5%BE%AE%E4%BF%A11/images/touxiang.png"></a><span>飞翔的小泥鳅~</span>' + '</div>' + '<div class="proimg" ><img src="' + item.imgUrl + '" style="width:100%;height:60vw;border-radius:5px;"><div class="propri">';
                if (tags != null) {
                    for (var i = 0; i < tags.length; i++) { 
                        htmls = htmls + '<span>' + tags[i] + '</span>';
                    }
                }
                htmls = htmls + '	</div>' + '</div>' + '<div class="pro-tit">' + item.title + '</div>' + '<div class="pro-info">' + item.summary + '</div>' + '<div class="pro-opt tab">' + '	<a href="javascript:;" class="pro-opt-btn"><i class="ly-icon"></i><span>' + item.commentNum + '</span></a>' + '	<a href="javascript:;" class="pro-opt-btn"><i class="zanle-icon"></i><span>' + item.raiseNum + '</span></a>' + '	<div class="pro-shop-btn">' + '		<a href="javascript:;">购买</a>' + '	</div>' + '</div>' + '</div>';
                var G = $(htmls);
                G.click(function () {
                    location.href = webPath + "articleDetail&id=" + item.id;
                }).find("div.pro-upuser").find("a").click(function () {
                    alert("点击了用户的事件");
                });
                a.after(G);

            })
        }
        $.getJSON(contextPath + "getArticleList", s);
    }
    art();
    Base.getScript(Gobal.Skin + "/js/pageDialog.js?v=160606", function () {
        Base.getScript(Gobal.Skin + "js/Flexslider.js?v=160722", init);
    });
});