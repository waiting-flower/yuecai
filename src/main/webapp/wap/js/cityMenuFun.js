$(function(){
	var recFun = null;
	var menuId = $("#menuId").val();
	var params = {
		currPage : 1,
		pageSize : 10,
		menuId : menuId
	};
	var divLoading = $("#divLoading");
	var _isLoading = false;
	var fail = function (f) {
		$.PageDialog.fail(f);
	};
	var ok = function (f) {
		$.PageDialog.ok(f);
	};
	var init = function(){
		$(".good-menu-list2 li").click(function(){
			var itemId = $(this).attr("itemId");
			$(".good-menu-list2 li").removeClass("curr");
			$(this).addClass("curr");
			params.menuId = itemId; 
			params.currPage = 1;
            recFun.initData();
		});
		if(menuId != ""){
			$(".good-menu-list2 li").removeClass("curr");
			var menuLiArr = $(".good-menu-list2 li");
			for(var i = 0; i < menuLiArr.length; i++){
				var menuItem = $(menuLiArr[i]);
				var itemId = menuItem.attr("itemId");
				if(itemId == menuId){
					menuItem.addClass("curr"); 
				}
			}
		}
		var loadData = function(){
			var u = function(){
				divLoading.show();
				var i = function(){
					return "currPage=" + params.currPage + "&pageSize=" + params.pageSize  + "&menuId=" + params.menuId;
				}  
				console.log("请求参数=" + i()); 
				$.getJSON(contextPath + "getGoodsList",i(),function(json){
					if(json.code == 1){//没有任何记录的情况
						_isLoading = true;   
						console.log("无记录显示图片"); 
						$("#noneTipDiv").html('<div class="wx1yyg-kylin-norecords"><i class="wx1yyg-kylin-icon kylin-null-06"></i><strong class="gray9">暂无商品哦~</strong></div>')
					}
					else{ 
						$("#goodsCount").html(json.totalRecords); 
						$.each(json.listItems,function(i,item){  
							var ui = '<div class="menu_goods_item">'
								+ '	<img src="' + item.imgUrl + '">'
								+ '	<div class="menu_goods_name">' + item.name + '</div>'
								+ '	<div class="menu_goods_price">￥' + item.price + '</div>' 
								+ '	<div class="menu_goods_com">' + item.commentNum + '评价 &nbsp;' + (item.niceComment * 100) + '%好评</div>'
								+ '</div>'
							var G = $(ui);  
							console.log(G);
							G.click(function () {  
	                            location.href = webPath + "goodsDetail&id=" + item.id;
	                        });
							$("#itemList").append(G); 
						}); 
						if(json.totalPage <= params.currPage){
							console.log("没有更多记录哦"); 
							$("#noneTipDiv").html(Gobal.NoneHtml2("没有更多记录了")); 
							_isLoading = true;
						} 
						else{
							_isLoading = false;
						}
					}
					divLoading.hide();
				});
			}
			this.initData = function(){ 
				$("#itemList").empty(); 
				u();
			}
			this.getNextPage = function(){
				params.currPage++;
				u();
			}
		} 
		recFun = new loadData();
		recFun.initData();
		$(".good-list-box").scroll(function(){
			var o = $(this);
			var theight = o.height();
			var uiDivHeight = $(".goods_list").height();
			var top = o.scrollTop();
			var scp = top + theight;
			if(scp + 222 > uiDivHeight){
				console.log("满足条件1");
				if(!_isLoading){
					console.log("获取分页");
					recFun.getNextPage();
					_isLoading = true;
				}
			}
		}); 
	};
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160304",init);
});