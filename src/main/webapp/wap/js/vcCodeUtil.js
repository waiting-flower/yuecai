var vcCodeUtil = {
    getCharException: "请求异常，请重试",
    reqVcCodeToomany: "请求验证码太频繁，请稍后再试",
    reqVcCodeException: "请求异常，请稍后重试",
    vcCanvasClickMiss: "",
    vcCodePassed: "验证通过",
    canvasWidth: 0,
    canvasHeight: 0,
    containerWidth: 0,
    btnWidth: 0,
    isAdvancedVcCode: false,
    isVcCodeValidated: false,
    isDragEnabled: true,
    $dialogObj: null,
    $dragBtn: null,
    $dragBtnLeft: null,
    $tips: null,
    $selectedChar: null,
    $vcCanvas: null,
    $dragBtnContainer: null,
    $canvasContainer: null,
    $vcCodeRefresh: null,
    key: null,
    codeId : null,
    canvasClickRightCallback: null,
    canvasClickTooManyCallback: null,
    dialogReadyFun: function (b) {
        var c = this;
        var a = {
            canvasWidth: 260,
            canvasHeight: 138
        };
        b = $.extend(a, b);
        c.$dialogObj = b.$dialogObj;
        c.key = b.key;
        c.codeId = b.codeId;
        c.canvasWidth = b.canvasWidth;
        c.canvasHeight = b.canvasHeight;
        c.canvasClickRightCallback = b.canvasClickRightCallback;
        c.canvasClickTooManyCallback = b.canvasClickTooManyCallback;
        c.isAdvancedVcCode = b.isAdvancedVcCode;
        c.isVcCodeValidated = b.isVcCodeValidated;
        c.isDragEnabled = b.isDragEnabled;
        c.$dragBtn = c.$dialogObj.find("#dragBtn");
        c.$dragBtnLeft = c.$dialogObj.find("#dragBtnLeft");
        c.$tips = c.$dialogObj.find("#vcCodeTips");
        c.$selectedChar = c.$dialogObj.find("#selectedChar");
        c.$vcCanvas = c.$dialogObj.find("#vcCanvas");
        c.$dragBtnContainer = c.$dialogObj.find("#dragBtnContainer");
        c.$canvasContainer = c.$dialogObj.find("#canvasContainer");
        c.$vcCodeRefresh = c.$dialogObj.find("#vcCodeRefresh");
        c.containerWidth = c.$dragBtnContainer.outerWidth();
        c.btnWidth = c.$dragBtn.outerWidth();
        c.$dragBtn.draggable({
            containment: "#dragBtnContainer",
            start: function () {
                if (c.isDragEnabled === false) {
                    return false
                }
                c.$tips.text("")
            }, drag: function (e, f) {
                var d = f.position.left;
                c.$dragBtnLeft.css("width", d + "px")
            }, stop: function (e, f) {
                var d = f.position.left;
                if (d < c.containerWidth - c.btnWidth) {
                    c.$dragBtn.animate({
                        left: "0"
                    });
                    c.$dragBtnLeft.animate({
                        width: "0"
                    })
                } else { 
                	$.getJSON(myPath + "veriCode/getWapVcChar.do?mobile=" + c.key,function(g){//wap版本的获取key值
                        if (g.state == 1) {
                            if (!!c.canvasClickTooManyCallback) {
                                c.canvasClickTooManyCallback()
                            } 
                            return false
                        } else {
                            if (g.state == 0) {
                                c.$vcCodeRefresh.show();
                                var h = g.vcChar;
                                c.$selectedChar.text(h);
                                c.$canvasContainer.addClass("canvas-showloading");
                                c.$dragBtnContainer.children(".vc-slide-text").hide();
                                c.$dragBtnContainer.children(".vc-slideBtnLeft").find("span").show();
                                c.$dragBtn.css({
                                    "float": "left",
                                    left: d + "px"
                                });
                                c.codeId = g.str;
                                c.$vcCanvas.attr("src",myPath + "veriCode/getWapVcImg.html?" + c.getVcImgParam(g.str)).load(function () {
                                    c.$vcCanvas.show()
                                });
                                c.isDragEnabled = false;
                                c.isVcCodeValidated = false
                            }
                        }
                    })
                }
            }
        });
        c.toolEvtReg()
    }, canvasClickActive: true,
    toolEvtReg: function () {
        var a = this;
        var b = null;
        a.$vcCodeRefresh.click(function () {
            if (b != null) {
                console.log("too fast！！");
                return
            }
            b = setTimeout(function () {
                b = null
            }, 200);
            a.showVcCode();
            a.$tips.text("")
        });
        a.$vcCanvas.click(function (f) {
            if (a.canvasClickActive === false) {
                return false
            }
            a.canvasClickActive = false;
            var d = a.$vcCanvas.offset().left;
            var h = a.$vcCanvas.offset().top;
            var c = f.pageX - d;
            var g = f.pageY - h;  
            $.getJSON(myPath + "veriCode/vcCodeCompare.do?mobile=" + a.key + "&x=" + c + "&y=" + g,function(e){//检验
                a.canvasClickActive = true;
                if (e.state == 1) {
                    if (e.num == 1) {
                        a.$tips.text(a.vcCanvasClickMiss);
                        a.showVcCode();
                        return false
                    } else {
                        if (!!a.canvasClickTooManyCallback) {
                            a.canvasClickTooManyCallback()
                        }
                        return false
                    }
                } else {
                    if (e.state == 0) {
                        a.$tips.text(a.vcCodePassed);
                        a.isVcCodeValidated = true;
                        if (!!a.canvasClickRightCallback) {
                            a.canvasClickRightCallback(e.str)
                        }
                    }
                }
            })
        })
    }, resetVcCode: function () {
        var a = this;
        a.$dragBtnLeft.css("width", "0");
        a.$dragBtn.animate({
            left: "0"
        });
        a.$dragBtnContainer.children(".vc-slide-text").show();
        a.$dragBtnContainer.children(".vc-slideBtnLeft").find("span").hide();
        a.$canvasContainer.removeClass("canvas-showloading")
    }, showVcCode: function () {
        var a = this;
        a.$vcCanvas.hide();
        $.getJSON(myPath + "veriCode/getWapVcChar.do?mobile=" + this.key,function(b){//wap版本的获取key值
            if (b.state == 1) {
                if (!!a.canvasClickTooManyCallback) {
                    a.canvasClickTooManyCallback()
                }
                return false
            } else {
                if (b.state == 0) {
                    var c = b.vcChar;
                    a.$selectedChar.text(c);
                    a.$canvasContainer.addClass("canvas-showloading");
                    a.$dragBtnContainer.children(".vc-slide-text").hide();
                    a.$dragBtnContainer.children(".vc-slideBtnLeft").find("span").show();
                    a.codeId = b.str;
                    a.$vcCanvas.attr("src", myPath + "veriCode/getWapVcImg.html?" + a.getVcImgParam(b.str)).load(function () {
                        a.$vcCanvas.show()
                    });
                    a.isDragEnabled = false;
                    a.isVcCodeValidated = false
                }
            }
        })
    }, getVcImgParam: function (c) {//对应vericode id
        var b = this;
        var a = "width=" + b.canvasWidth + "&height=" + b.canvasHeight + "&vcCharCookieKey=" + c;
        return a
    }
}; 