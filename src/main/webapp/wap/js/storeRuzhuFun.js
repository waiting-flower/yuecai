$(function(){
	var init = function(){
		var fail = function (r) {
			$.PageDialog.fail(r);
	    };
	    var ok = function (s, r) {
	    	$.PageDialog.ok(s, r);
	    };
	    
	     
		$("#ruzhuNextBtn").click(function(){
			
			var name = $("#name").val();
			var mobile = $("#mobile").val();
			var cate = $("#cate").val();
			var pro = $("#pro").val();
			var city = $("#city").val();
			var country = $("#country").val();
			var address = $("#address").val();
			var searchKey = $("#searchKey").val();
			if(name == ""){
				fail("请输入店铺名称");
				return false;
			}
			if(mobile == ""){
				fail("请输入店铺联系方式");
				return false;
			}
			if(cate == ""){
				fail("请选择入驻类目");
				return false;
			}
			if(pro == ""){
				fail("请选择省份");
				return false;
			}
			if(city == ""){
				fail("请选择城市");
				return false;
			}
			if(country == ""){
				fail("请选择区县");
				return false;
			}
			if(address == ""){
				fail("请输入店铺详细地址");
				return false;
			}
			var vData = {
				name : name,
				id : $("#storeId").val(),
				mobile : mobile,
				pro : pro,
				cate : cate ,
				city : city,
				country : country,
				address : address,
				searchKey : searchKey
			}; 
			$.post(contextPath + "saveStoreRuzhu1",vData,function(json){
				if(json.code == "10"){
					location.href = webPath + "login&returnUrl=storeRuzhu"; 
				}
				else if(json.code == "0"){
					var isReRuzhu = $("#isReRuzhu").val();
					ok("提交成功",function(){   
						location.href = webPath + "storeRuzhu2&isReRuzhu=" + isReRuzhu;
					});
				}
			},"json");
			 
		});
	}
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160606",init);
});