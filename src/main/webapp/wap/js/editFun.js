var userUploadFun = null;
function UploadPhotoScriptAPI(b, a) {
    if (b != "") {
        userUploadFun.ShowUploadErr(b)
    } else {
        userUploadFun.appHtml(a)
    }
}
$(function(){
	var title = $("#title");//文章标题
	var content = $("#content");//文章内容
	var btn_fabu = $("#btn_fabu");//发布按钮
	
	var init = function(){
		var picUI = [];//图片数组集合
		
		var fail = function(o){
			$.PageDialog.fail(o);
		}
		var ok = function(o){
			$.PageDialog.ok(o);
		}
		this.ShowUploadErr = function(err){
			fail(err);
		}
		var appHtml = function(src){ 
			picUI.push(src);
			var html = '<li class="tt"><a href="javascript:;"><img src="' + src +'"/></a><i key="' + src + '" class="op"></i>';
			var G = $(html);
			G.find(".op").click(function(){
				var tempUI = [];
				var thisKey = $(this).attr("key");
				console.log("当前的图片数组大小为：" + picUI.length);
				for(var i = 0; i < picUI.length; i++){
					if(picUI[i] != thisKey){
						tempUI.push(picUI[i]);
					}
				}
				picUI = tempUI;
				$(this).parent().remove();
				console.log("删除后的图片数组大小为：" + picUI.length);
				var t = $(".e_img_list ul").find("li.tt").length;
				console.log(t + "size" );
				if(t < 10){
					$("#plus_div").show(); 
				}
			});
			console.log(html);
			$("#plus_div").before(G);
		};

		btn_fabu.click(function(){
			saveTemp();
		});
		
		var file = $("#input_file");
		file.bind("change",function(){
			var t = $(".e_img_list ul").find("li.tt").length;
			console.log(t + "size" );
			if(t >= 9){
				$("#plus_div").hide();
			}
			$(".tip_info").hide(); 
			var m = $(this).val();
	        if (m != "") {
	             var l = (m.substr(m.length - 5)).substr((m.substr(m.length - 5)).indexOf(".") + 1).toLowerCase();
	             if (l != "gif" && l != "jpg" && l != "jpeg" && l != "bmp" && l != "png") {
	                 $(this).val("");
	                 fail("只能上传JPG、GIF、PNG或BMP图片!");
	                 return false
	             } else {
	                fail("上传中,请稍后");
	                var e = $("#pageForm");
	                e.submit()
	             }
	         } else {
	             fail("请先选择需要上传的文件!");
	             return false
	         }
	        var size = 0;
	        size = this.files[0].size
	        var myEsize = (size/1024).toFixed(2);
			if(myEsize > 10240){
				fail("上传文件不能超过10M"); 
				return false;
			}
			console.log("图片大小验证通过，开始执行上传");
			start_upload(this,size);
		});  
		
		var start_upload = function(obj,size){  
	        if(!isCanvasSupported){  
	           console.log("不支持的浏览器");  
	        }else{  
	           compress(event, function(base64Img){  
	               // 如需保存  或 把base64转成图片  ajax提交后台处理  
	               putb64(base64Img,size);
	           });  
	        }   
		}   

		//七牛云base64上传图片
		var putb64 = function(pic, size) {
			var token = function(){
				console.log("获取token令牌");
				$.getJSON(contextPath + "getUpToken",function(json){//token令牌获取
				
					console.log("开始上传七牛云");
					console.log("令牌获取成功，开始执行上传");
					var url = "http://upload.qiniu.com/putb64/-1"; 
					var xhr = new XMLHttpRequest();
					xhr.onreadystatechange = function() {
						console.log(xhr.readyState);
						if (xhr.readyState == 4) {
							console.log("返回text" + xhr.responseText);
							var ttt = JSON.parse(xhr.responseText); 
							var key = ttt.key;
							console.log("上传成功，得到key" + key);
							appHtml("http://oy44y8tcw.bkt.clouddn.com/" + key);//添加图片
						}  
					}
					xhr.open("POST", url, true);
					xhr.setRequestHeader("Content-Type", "application/octet-stream");
					xhr.setRequestHeader("Authorization","UpToken " + json.upToken);//json.uptoken
					xhr.send(pic.split(",")[1]);
				});
			}
			token();
		}
		
		//或者发布1
		var saveTemp = function(type){ 
			console.log("保存方法：type=" + type)
			var con = content.val();
			var at = title.val();
			if(at.length > 35 || at.length < 6){
				fail("标题6-35个字"); 
				return false;
			}
			if(picUI.length < 2){
				fail("至少上传两张图片");
				return false;
			}
			var postData = function(){
				var vData = {
					title : at,
					content : con,
					pics : picUI.join(",")
				};
				console.log(picUI.join(","));
				var s = function(json){
					console.log("保存文章，返回" + json.code);
					if(json.code == 10){
						fail("您尚未登录，请登录后再进行操作");
						var tt = function(){
							var s = function(){
								location.href = webPath + "login";
							}
							setTimeout(s,400);
						};
						tt();
					}
					else{
						ok("发布成功,即将跳转到列表页面");
						var tt = function(){
							var s = function(){
								location.href = webPath + "articleList";
							}
							setTimeout(s,500);
						};
						tt();
					}
				}
				//post方式提交
				$.post(contextPath + "saveUserArticle",vData,s,"json");
			};
			postData();
		}
	}
	var cc = function(){
		userUploadFun = new init();
	}
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160606",cc);
});