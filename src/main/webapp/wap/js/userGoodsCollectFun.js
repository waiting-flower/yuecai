$(function(){
	var divLoading = $("#divLoading");
	var goodsRecFun = null;
	var params1 = {
		currPage : 1,
		pageSize : 10
	};
	//dialog confrim
	var r = function (L, F, G, E, N) {
		
         var H = 255;
         var K = 126;
         if (typeof (E) != "undefined") {
             H = E
         }
         if (typeof (E) != "undefined") {
             K = N
         }
         var M = null;
         var O = '<div class="clearfix m-round u-tipsEject"><div class="u-tips-txt">' 
         	+ L + '</div><div class="u-Btn"><div class="u-Btn-li"><a href="javascript:;" id="btnMsgCancel" class="z-CloseBtn">取消</a></div><div class="u-Btn-li"><a id="btnMsgOK" href="javascript:;" class="z-DefineBtn">确定</a></div></div></div>';
         var J = function () {
             var P = $("#pageDialog");
             P.find("a.z-DefineBtn").click(function () {
                 if (typeof (F) != "undefined" && F != null) {
                     F();
                 }
                 I.close();
             });
             P.find("a.z-CloseBtn").click(function () {
                 if (typeof (G) != "undefined" && G != null) {
                     G()
                 }
                 I.cancel()
             })
         };
         var I = new $.PageDialog(O, {
             W: H,
             H: K,
             close: true,
             autoClose: false,
             ready: J
         })
     };
	var init = function(){
		$("#edit_btn").click(function(){
			var cancle = function(){
				$.getJSON(contextPath + "saveCancleAllGoods&type=3",function(json){
					if(json.code == 0){
						$.PageDialog.ok("取消收藏成功");
						location.reload();
					}
				});
			} 
			r("是否全部取消收藏的商品",cancle);
		});
		var g = function(){
			var data = function(){
				var i = function(){
					return "currPage=" + params1.currPage + "&pageSize=" + params1.pageSize;
				}
				$.getJSON(contextPath + "getUserCollectGoodsList",i(),function(json){
					divLoading.hide();
					if(json.totalPage <= params1.currPage){
						console.log("没有数据了，所以不需要继续加载了");
						_IsLoading = true;
					}
					if(json.code == 10){
						location.href = webPath + "login";
					}
					else if(json.code == 1){
						$("#c_div1").html(Gobal.NoneHtmlEx("您还没有任何的收藏商品哦"));
						_IsLoading = true;
					} 
					else{ 
						$("#fav_total_num").html(json.totalRecords);
						$.each(json.listItems,function(i,item){
							var G = '<div class="fav_item" id="fav_item' + item.id + '" index="1" page="1">'
								+ '		<a href="javascript:;" class="fav_link fav_link_goods"> <img class="image"'
								+ '				src="' + item.imgUrl + '"'
								+ '				width="100" height="100">'  
								+ '				<p class="name">' + item.name + '</p>'
								+ '				<p class="sku"> <span></span><span>' + item.skuTags + '</span>  </p>'
								+ '				<p class="price seperator" id="price_box_1352431963" favprice="39">'
								+ '					<span class="price_value"'
								+ '						style="color: #e4393c; float: left"><b'
								+ '						style="font-size: 10px;">¥&nbsp;</b>' + item.price + '.<b'
								+ '						style="font-size: 10px;">00</b></span><span itemid="' + item.id + '" class="more_notice btn">取消收藏</span>'
								+ '				</p>'
								+ '			</a>' 
								+ '		</div>';
							var temp = $(G); 
							temp.click(function(){
								location.href = webPath + "goodsDetail&id=" + item.goodsId;
							}).find("a").find("p.price").find("span.more_notice").click(function(T){
								stopBubble(T); 
								var id = $(this).attr("itemid"); 
								var cancle = function(){     
									$.getJSON(contextPath + "saveCancleGoodsCollect&id=" + item.goodsId,function(json){
										if(json.code == 0){
											$.PageDialog.ok("取消收藏成功");
											$("#fav_item" + id).remove();
										}
									}); 
								} 
								r("是否取消收藏该商品",cancle);
							});
							$("#c_div1").append(temp);   
						});
					}
					_IsLoading = true;
				});
			}
			this.initData = function(){
				data();
			}
			this.getNextData = function(){
				params1.currPage++; 
				data();
			}
		}
		var a = function(){
			var data = function(){
				var i = function(){
					return "currPage=" + params2.currPage + "&pageSize=" + params2.pageSize;
				}
				$.getJSON(contextPath + "getCollectArticle",i(),function(json){
					console.log("获取收藏的文章数据" + jsonToStr(json) + " 数据大小为" + json.listItems.length);
					divLoading.hide(); 
					if(json.totalPage <= params2.currPage){
						console.log("没有数据了，所以不需要继续加载了");
						_IsLoading = true;
					}
					if(json.code == 10){
						location.href = webPath + "login";
					}
					else if(json.code == 1){
						$("#c_div2").html(Gobal.NoneHtmlEx("您还没有任何的收藏文章哦"));
						_IsLoading = true;
					}
					else{
						$("#fav_total_num2").html(json.totalRecords);
						$.each(json.listItems,function(i,item){
							var G = '<div class="fav_item" id="fav_item' + item.id + '" index="' + item.id + '" page="1">'
								+ '		<a href="javascript:;" class="fav_link fav_link_goods"> <img class="image"'
								+ '				src="' + item.imgUrl + '"'
								+ '				width="100" height="100">'   
								+ '				<p class="name">' + item.title + '</p>'
								+ '				<p class="sku">' + item.content + '</p>'  
								+ '				<div class="_temp" style="display:inline-block;margin-top:10px;width:100%;">'
								+ '					<span class="price_value" uid="' + item.userId + '"'   
								+ '						style="float: left;line-height:30px;color:#3985ff;">' 
								+'					<img style="width:30px;height:30px;border-radius:50%;vertical-align: middle;margin-right:15px;" src="' + item.headUrl + '">' + item.nick + '.<b'
								+ '						style="font-size: 10px;">00</b></span><span style="margin-top:8px;" itemid="' + item.id + '" class="more_notice btn">取消收藏</span>'
								+ '				</div>' 
								+ '			</a>' 
								+ '		</div>';
							var temp = $(G); 
							temp.click(function(){
								var index = $(this).attr("index"); 
								location.href = webPath + "articleDetail&id=" + index;
							}).find("a").find("div._temp").find("span.more_notice").click(function(T){
								stopBubble(T); 
								var id = $(this).attr("itemid");
								var cancle = function(){
									$.getJSON(contextPath + "updateArticleCollect&articleId=" + id + "&opType=2",function(json){
										if(json.code == 0){
											$.PageDialog.ok("取消收藏成功");
											$("#fav_item" + id).remove();
										}
									});  
								} 
								r("是否取消收藏该文章",cancle);
							}); 
							temp.find("a").find("div._temp").find("span.price_value").click(function(T){
								var uid = $(this).attr("uid");
								stopBubble(T); 
								location.href = webPath + "userHome&id=" + uid;
							}); 
							$("#c_div2").append(temp);   
						});
					}
				});
			}
			this.initData = function(){
				data();
			}
			this.getNextData = function(){
				params2.currPage++;
				data();
			}
		}
		goodsRecFun = new g();
		goodsRecFun.initData();
		var e = function(){
			if(keyIndex == 0){
				goodsRecFun.getNextData();
			}
			else{
				if(articleRecFun == null){
					articleRecFun = new a();
					articleRecFun.initData();
				}
				else{
					console.log("getNextData");
					articleRecFun.getNextData();
				} 
			}
		}
		scrollForLoadData(e);
		$(".weui-navbar a").click(function(){
			$(this).addClass("weui-bar__item--on").siblings().removeClass("weui-bar__item--on");
			var index = $(this).index() + 1; 
			$(".my_order_list").hide();
			$("#c_div" + index).show();  
			_IsLoading = false;
			if(index == 1){
				keyIndex = 0;
			}
			else{
				keyIndex = 1;
				if(articleRecFun == null){
					articleRecFun = new a();
					articleRecFun.initData();
				}
			}
		});
	};
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160304",init);
})