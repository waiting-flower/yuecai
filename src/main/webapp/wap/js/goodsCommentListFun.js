$(function(){
	var params = {
		"currPage" : 1,
		"pageSize" : 10,
		"goodsId" : $("#goodsId").val()
	};
	var divLoading = $("#divLoading");
	var recFun = null;
	var init = function(){ 
		var loadData = function(){
			var u = function(){
				var i = function(){
					return "currPage=" + params.currPage + "&pageSize=" + params.pageSize + "&goodsId=" + params.goodsId;
				}
				$.getJSON(contextPath + "getGoodsCommentList",i(),function(json){
					if(params.currPage >= json.currPage){
						_IsLoading = true; 
					}
					if(json.code == 1){
						$("#commentList_div").hide();
						$("#evalNone").show();
						_IsLoading = true;
					}
					else{
						$("#evalNo2").html(json.totalRecords);
						$.each(json.listItems,function(i,item){
							var skuStr = "";
							var sku = JSON.parse(item.sku);
							for(var kk in sku){
								console.log(kk);
								console.log(sku[kk]);  
								skuStr += kk + "&nbsp;" + sku[kk] + "&nbsp;";
							} 
							
							var score = item.score;
							var width = score/5 *100;
							var imgList = item.imgList;
							var imgHtml = "";
							if(imgList != null && imgList != ""){
								imgHtml += '<div class="cmt_att">';
								var imgArr = imgList.split(",");
								for(var i = 0; i < imgArr.length; i++){    
									imgHtml += '<span style="margin-right:10px;"  class="img"><img src="' + imgArr[i] + '"></span>';
								} 
								imgHtml += '</div>'; 
							}
							var ui = '<li>' 
								+ '    <div class="cmt_user" ptag="">'
								+ '        <span class="user">' + item.nick + '</span> '
								+ '        <span class="credit"><span style="width: ' + width + '%"></span></span> <span class="date">' + item.msgTime + '</span>'
								+ '    </div>'
								+ '    <div class="cmt_cnt" ptag="">' + item.msgContent + '</div>'
								+ imgHtml
								+ '    <div class="cmt_sku">'   
								+ '        <span>' + skuStr + '</span>'
								+ '        <div class="reply">' + item.replyNum
								+ '            <a  href="' + webPath + 'commentView&goodsId=' + $("#goodsId").val() + '&commentId=' + item.id + '" class="btn">回复</a>'
								+ '        </div>' 
								+ '    </div>'
								+ '</li>'; 
							$("#evalDet_main").append(ui);
						});
						_IsLoading = false;
					}
				});
			}
			this.initData = function(){
				u();
			}
			this.getNextPage = function(){
				params.currPage++;
				u();
			}
		}
		recFun = new loadData();
		recFun.initData(); 
		scrollForLoadData(recFun.getNextPage);
	}; 
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160304",init);
});