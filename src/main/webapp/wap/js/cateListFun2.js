$(function(){
	var recFun = null;
	var params = {
		currPage : 1,
		pageSize : 10,
		sortType : 1, 
		cateCode : ""
	};
	var divLoading = $("#divLoading");
	var _isLoading = false;
	var fail = function (f) {
		$.PageDialog.fail(f)
	};
	var ok = function (f) {
		$.PageDialog.ok(f)
	};
	/**
     * 分类点击事件
     */
    var sortEvent = function () {
    	$("#noneTipDiv").html(''); 
    	var C = true;
    	
        var A = $("#divSortList");
        var B = A.find("ul");
        B.on("click", "li", function () {
            var I = $(this);
            var F = I.index();
            var E = I.height();
            I.addClass("current").siblings().removeClass("current");
            A.stop().animate({
                scrollTop: F * E
            }, 500);
            params.cateCode = I.attr("cateCode");
            var H = I.attr("reletype");
            if (H == 3) {
                var G = I.attr("linkaddr");
                window.location.href = G
            } else {
            	params.orderFlag = 10;
                D.find("li[orderflag=10]").addClass("current").siblings().removeClass("current");
                if (C) {
                	params.currPage = 1; 
                	recFun.initData();
                } else {
                    C = true;
                }
            }
        });//end 左侧分类事件
        var D = $("#ulOrderBy");
        D.on("click", "li", function () {
            var F = $(this);
            F.addClass("current").siblings().removeClass("current");
            var E = F.attr("sortType");
            params.sortType = E; 
            if (F.index() == 3) {
                if (E == 30) {
                    F.attr("sortType", 31);
                    F.find("i:eq(1)").addClass("sort").siblings().removeClass("sort")
                } else {
                    F.attr("sortType", 30);
                    F.find("i:eq(0)").addClass("sort").siblings().removeClass("sort")
                }
            } else {
                F.parent().find("i").removeClass("sort")
            }
            params.currPage = 1;
            recFun.initData()
        });
    };
	
	//初始化
	var init = function(){ 
		var cityName = cookie.get("cityName");
		var countyName = cookie.get("countyName");
		console.log(cityName); 
	    if(cityName == undefined){ 
	    	
	    } 
	    else{
	    	$("#location_top_cityName").html(cityName + countyName);
	    } 
	     
		sortEvent(); //注册分类点击、排序事件  
		var loadData = function(){
			var u = function(){
				divLoading.show();
				var i = function(){
					return "currPage=" + params.currPage + "&pageSize=" + params.pageSize
							+ "&sortType=" + params.sortType + "&cateCode=" + params.cateCode;
				} 
				console.log("请求参数=" + i());     
				$.getJSON(contextPath + "getStoreList",i(),function(json){
					if(json.code == 1){//没有任何记录的情况
						_isLoading = true;    
						$("#noneTipDiv").html('<div class="wx1yyg-kylin-norecords"><i class="wx1yyg-kylin-icon kylin-null-06"></i><strong class="gray9">暂无商户入驻哦~</strong></div>')
					}
					else{ 
						$("#noneTipDiv").html(''); 
						$.each(json.listItems,function(i,item){
							var G = '<div class="store_item">'
							+ '			<div class="store_img_box">'
							+ '				<img alt="" src="' + item.imgUrl + '">'
							+ '			</div>'
							+ '			<div class="content_box">'  
							+ '				<div class="name">' + item.name + '</div>'
							//+ '				<span class="num">' + item.collectNum + '人关注</span>'
							+ '				<span class="juli">' + item.tags + '</span>'
							+ '			</div>';
							/*if(item.isCollect == 0){
								G = G + '	<div class="op_box">'
								+ '				<div class="not_guanzhu guanzhubtn">'
								+ '					<i class="fa fa-heart"></i>'
								+ '					关注'
								+ '				</div>'
								+ '			</div>'; 
							}
							else{  
								G = G + '	<div class="op_box">'
								+ '				<div class="yi_guanzhu guanzhubtn">'
								+ '					已关注'
								+ '				</div>'
								+ '			</div>';
							}*/  
							G = G + '		</div> ';
							var temp = $(G); 
							temp.click(function(){ 
								location.href = webPath + "storeDetail&id=" + item.id;
							}).find(".guanzhubtn").click(function(T){
								var _this = $(this);
								stopBubble(T);   
								if(_this.hasClass("yi_guanzhu")){
									//取消关注
									$.getJSON(contextPath + "saveCancleStoreCollect",{
					        			id : item.id
					        		},function(json){
					        			if(json.code == 10){
					        				location.href = webPath + "login";
					        			}
					        			else if(json.code == 0){ 
					        				$.PageDialog.ok("取消关注成功",function(){
					        					temp.find(".guanzhubtn").removeClass("yi_guanzhu").addClass("not_guanzhu");   
					        					temp.find(".guanzhubtn").html('<i class="fa fa-heart"></i>关注');
					        				});  
					        			}
					        			else if(json.code == 1){ 
					        				$.PageDialog.ok("取消关注成功");
					        			}
					        		});
								}
								else{
									//添加关注
									$.getJSON(contextPath + "saveStoreCollect",{
					        			id : item.id
					        		},function(json){ 
					        			if(json.code == 10){
					        				location.href = webPath + "login";
					        			} 
					        			else if(json.code == 0){
					        				$.PageDialog.ok("关注成功",function(){
					        					temp.find(".guanzhubtn").removeClass("not_guanzhu").addClass("yi_guanzhu");   
					        					temp.find(".guanzhubtn").html('已关注');
					        				}); 
					        			}
					        			else if(json.code == 1){
					        				$.PageDialog.ok("关注成功");
					        			}
					        		});
								}
                                                                                                                                              							});
							$("#storeList").append(temp);   
						}); 
						if(json.totalPage <= params.currPage){
							console.log("没有更多记录哦");   
							$("#noneTipDiv").html(Gobal.LookForPC);  
							_isLoading = true;
						} 
						else{
							_isLoading = false;
						}
					}
					divLoading.hide();
				});
			}
			this.initData = function(){ 
				$("#storeList").empty(); 
				$("#noneTipDiv").html("");  
				u();  
			}
			this.getNextPage = function(){
				params.currPage++;
				u();
			}
		} 
		recFun = new loadData();
		recFun.initData();
		$(".good-list-box").scroll(function(){
			var o = $(this);
			var theight = o.height();
//			console.log("theight=" + theight);
			var uiDivHeight = $(".goods_list").height();
			var top = o.scrollTop();
//			console.log("top=" + top);
			var scp = top + theight;
			if(scp + 222 > uiDivHeight){
				console.log("满足条件1");
				if(!_isLoading){
					console.log("获取分页");
					recFun.getNextPage();
					_isLoading = true;
				}
			}
		});
		var isNew = $("#hiddenIsNew").val();
		if(isNew == "1"){
			$("#ulOrderBy").find("li").eq(2).click();
		}
	};  
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160304",function(){
		Base.getScript(Gobal.Skin + "js/search.js?v=160304",init);   
	}); 
});