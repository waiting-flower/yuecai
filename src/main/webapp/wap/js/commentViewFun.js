$(function(){
	var params = {
		"currPage" : 1,
		"pageSize" : 10,
		"commentId" : $("#commentId").val()
	};
	var divLoading = $("#divLoading"); 
	var recFun = null;
	var toUserId = "";
	var init = function(){
		var loadData = function(){
			var u = function(){
				console.log("执行服务获取数据");
				var i = function(){
					return "currPage=" + params.currPage + "&pageSize=" + params.pageSize + "&commentId=" + params.commentId;
				} 
				$.getJSON(contextPath + "getCommentViewList",i(),function(json){
					if(json.totalPage <= params.currPage){
						console.log("执行完毕"); 
						_IsLoading = true; 
					}   
					if(json.code == 1){
						$("#evalNone").show();
						_IsLoading = true;
					}
					else{
						if(json.listItems.length < 1){
							_IsLoading = true; 
						}
						$.each(json.listItems,function(i,item){
							var ui = '<li id="comment211423038">' 
								+ '    <img class="image" src="' + item.headUrl + '">'
								+ '    <div class="inner">' 
								+ '        <p class="name"><em>' + item.nick + '</em><em>  ' + item.replyToNick + '</em><span>' + item.msgTime + '</span>'
								+ '        </p>'
								+ '        <p class="text">' + item.msgContent + '</p> '
								+ '	<a class="link" href="javascript:void(0);" replyid="' + item.userId + '" id="btn211423038" ptag="7046.2.6">回复</a> '
								+ '    </div>'
								+ '</li>';
							var G = $(ui);
							G.find("div.inner").find("a").click(function(){
								var replyid = $(this).attr("replyid");
								toUserId = replyid;
								$("#layer_comment").show();
							});
							$("#commentList").append(G);
						});
						_IsLoading = false;
					}
				});
			}
			this.initData = function(){
				u();
			}
			this.getNextPage = function(){
				if(_IsLoading){
					return; 
				}
				params.currPage++;
				u();
			}
		}
		recFun = new loadData();
		recFun.initData(); 
		scrollForLoadData(recFun.getNextPage);
		 
		
		$("#commentBtn").click(function(){
			$("#layer_comment").show();
		});
		
		$("#btn_cancle").click(function(){
			$("#layer_comment").hide();
		});
		
		$("#btn_save").click(function(){
			var msgContent = $("#msgContent").val();
			var commentId = $("#commentId").val();
			var vdata = {
					msgContent : msgContent,
					commentId : commentId,
					toUserId : toUserId 
			}; 
			$.post(contextPath + "saveCommentView",vdata,function(json){
				if(json.code == 0){
					$.PageDialog.ok("评论成功",function(){
						$("#layer_comment").hide();
						location.reload();
					});
				}
			},"json");
		});
	}; 
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160304",init);
});