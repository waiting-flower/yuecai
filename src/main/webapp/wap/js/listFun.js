$(function(){
	var recFun = null;
	var cateCode = $("#cateCode").val();
	var params = {
		currPage : 1,
		pageSize : 10,
		sortType : 1, 
		cateCode : cateCode,   
		storeId : $("#storeId").val(),
		key : $("#searchKey").val() ,
		isTejia : $("#isTejia").val()
	};
	var divLoading = $("#divLoading");
	var _isLoading = false;
	var fail = function (f) {
		$.PageDialog.fail(f);
	};
	var ok = function (f) {
		$.PageDialog.ok(f);
	};
	 
	//初始化
	var init = function(){
		var j = $("#li_top2");
		$(".good-list-box").scroll(function () {
            if ($(".good-list-box").scrollTop() > 100) {
                j.show();
            } else {
                j.hide();
            }
        })
        j.click(function(){
        	 $(this).hide();
        	 $(".good-list-box").animate({
                 scrollTop: 0
             }, 500);
        });
		var D = $("#ulOrderBy");
        D.on("click", "li", function () {
            var F = $(this);
            F.addClass("current").siblings().removeClass("current");
            var E = F.attr("sortType");
            params.sortType = E; 
            if (F.index() == 3) {
                if (E == 30) {
                    F.attr("sortType", 31);
                    F.find("i:eq(1)").addClass("sort").siblings().removeClass("sort")
                } else {
                    F.attr("sortType", 30);
                    F.find("i:eq(0)").addClass("sort").siblings().removeClass("sort")
                }
            } else {
                F.parent().find("i").removeClass("sort");
            }
            params.currPage = 1;
            recFun.initData();
        });//排序点击事件 
        
		var loadData = function(){
			var u = function(){
				divLoading.show();
				var i = function(){
					return "currPage=" + params.currPage + "&pageSize=" + params.pageSize  + "&storeId=" + params.storeId  
							+ "&sortType=" + params.sortType + "&storeCate=" + params.cateCode + "&keyword=" + params.key + "&isTejia=" + params.isTejia;
				} 
				console.log("请求参数=" + i()); 
				$.getJSON(contextPath + "getGoodsList",i(),function(json){
					if(json.code == 1){//没有任何记录的情况
						_isLoading = true;   
						console.log("无记录显示图片"); 
						$("#noneTipDiv").html('<div class="wx1yyg-kylin-norecords"><i class="wx1yyg-kylin-icon kylin-null-06"></i><strong class="gray9">暂无商品哦~</strong></div>')
					}
					else{ 
						$("#goodsCount").html(json.totalRecords); 
						$.each(json.listItems,function(i,item){  
							var ui = '<div class="search_prolist_item item_longcover" id="' + item.id + '" >'
								+ '    <div class="search_prolist_item_inner" >'
								+ '        <div class="search_prolist_cover" rd="0-4-4">'
								+ '            <img class="photo" src="' + item.imgUrl + '">'
								+ '        </div>'
								+ '        <div class="search_prolist_info">'
								+ '            <div class="search_prolist_title" data-line="1" rd="0-4-4">' + item.name 
								+ '            </div>'
								+ '            <div class="search_prolist_price">' 
								+ '                <strong ><em><span class="int">￥' + item.price + '</span></em> </strong>'
								+ '            </div>'
								+ '            <div class="search_prolist_other text_small" rd="0-4-4">'
								+ '                <span class="search_prolist_comment"><span id="com_16983074369">' + item.commentNum +
									'</span>人评价</span> <span class="search_prolist_rate">好评率' + (item.niceComment * 100) + '%</span> '
								+ '            </div>'
								+ '        </div>'
								+ '    </div>'
								+ '</div>'
							var G = $(ui); 
							console.log(G);
							G.click(function () { 
	                            location.href = webPath + "goodsDetail&id=" + $(this).attr("id");
	                        });
							$("#itemList").append(G); 
						}); 
						if(json.totalPage <= params.currPage){
							console.log("没有更多记录哦"); 
							$("#noneTipDiv").html(Gobal.NoneHtml2("没有更多记录了")); 
							_isLoading = true;
						} 
						else{
							_isLoading = false;
						}
					}
					divLoading.hide();
				});
			}
			this.initData = function(){ 
				$("#itemList").empty(); 
				u();
			}
			this.getNextPage = function(){
				params.currPage++;
				u();
			}
		} 
		recFun = new loadData();
		recFun.initData();
		$(".good-list-box").scroll(function(){
			var o = $(this);
			var theight = o.height();
//			console.log("theight=" + theight);
			var uiDivHeight = $(".goods_list").height();
			var top = o.scrollTop();
//			console.log("top=" + top);
			var scp = top + theight;
			if(scp + 222 > uiDivHeight){
				console.log("满足条件1");
				if(!_isLoading){
					console.log("获取分页");
					recFun.getNextPage();
					_isLoading = true;
				}
			}
		}); 
		var isNew = $("#isNew").val();
		if(isNew == "1"){ 
			$("#ulOrderBy").find("li").eq(2).click();
		}
	};
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160304",function(){
		Base.getScript(Gobal.Skin + "js/search.js?v=160523", init);
	}); 
});