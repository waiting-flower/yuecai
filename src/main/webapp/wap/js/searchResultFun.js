$(function(){
	var recFun = null;
	var params = {
		currPage : 1,
		pageSize : 10,
		sortType : 1, 
		cateId : "0",
		key : "" 
	};
	var recFun2 = null;
	var params2 = {
		currPage : 1,
		pageSize : 10,
		key : "" 
	};
	var divLoading = $("#divLoading");
	var _isLoading = false;
	var divLoading2 = $("#divLoading2");
	var _isLoading2 = false;
	var fail = function (f) {
		$.PageDialog.fail(f)
	};
	var ok = function (f) {
		$.PageDialog.ok(f)
	};
	var isInitStore = false;
	var tabTag = "goods";
	//初始化
	var init = function(){
		var D = $("#ulOrderBy");
        D.on("click", "li", function () {
            var F = $(this);
            F.addClass("current").siblings().removeClass("current");
            var stype = $(this).attr("stype");
            if(stype == "store"){
            	if(!isInitStore){
               	 	params2.currPage = 1;
                    recFun2.initData();
                    $("#storeList").show();
                    $("#goodsList").hide(); 
                    isInitStore = true;
                    $("#storeResult").show();
               		$("#goodsResult").hide();
               }
               else{ 
               		$("#storeList").show();
               		$("#goodsList").hide(); 
               		tabTag = "store";
               		$("#storeResult").show();
               		$("#goodsResult").hide();
               }
            	tabTag = "store";
            }
            else{
            	$("#storeList").hide();
           		$("#goodsList").show(); 
           		tabTag = "goods"; 
           		$("#storeResult").hide();
           		$("#goodsResult").show();
            }
            
           
        });//排序点击事件 
        
		var loadData = function(){
			var u = function(){
				divLoading.show();
				var i = function(){
					var N = $("#txtSearch").val();
					var key = encodeURI(encodeURI(N));
					return "currPage=" + params.currPage + "&pageSize=" + params.pageSize
							+ "&sortType=" + params.sortType + "&cateId=" + params.cateId + "&keyword=" + key;
				} 
				console.log("请求参数=" + i()); 
				$.getJSON(contextPath + "getGoodsList",i(),function(json){
					if(json.code == 1){//没有任何记录的情况
						_isLoading = true;   
						console.log("无记录显示图片"); 
						$("#noneTipDiv").html('<div class="wx1yyg-kylin-norecords"><i class="wx1yyg-kylin-icon kylin-null-06"></i><strong class="gray9">暂无商品哦~</strong></div>')
					}
					else{   
						$("#noneTipDiv").html('');
						$("#goodsCount").html(json.totalRecords); 
						$.each(json.listItems,function(i,item){     
							var ui = '<div class="search_prolist_item" style="width:94%;padding:10px 3%;border-bottom:1px solid #e5e5e5;" id="' + item.id + '">'
								+'    <div class="search_prolist_item_inner">'  
								+'        <div class="search_prolist_cover" style="border:1px solid #e5e5e5;" rd="0-4-1">'
								+'            <img class="photo" src="' + item.imgUrl + '">'
								+'        </div>'
								+'        <div class="search_prolist_info">'  
								+'            <div class="search_prolist_title">' + item.name + '</div>'
								+'            <div class="search_prolist_price"> <strong>  <em>¥ <span class="int">' + item.price + '</span></em>  </strong> '
								+'            </div>' 
								+'            <div class="search_prolist_line" rd="0-4-1" id="nprice_15314244651" spmark="5" hascou="1"> <i class="mod_tag"></i>'
								+'            </div>' 
								+'            <div class="search_prolist_other text_small" rd="0-4-1"> <span class="search_prolist_comment" rd="0-4-1"><span id="com_15314244651">' + item.commentNum + '</span>人评价</span> <span class="search_prolist_rate" rd="0-4-1">商品评分<span id="rate_15314244651">' + item.score + '</span></span>'
								+'            </div>' 
								+'        </div>'
								+'    </div>'
								+'</div>'
							var G = $(ui);
							G.click(function () {
	                            location.href = webPath + "goodsDetail&id=" + $(this).attr("id");
	                        });
							$("#ulGoodsList").append(G); 
						}); 
						if(json.totalPage <= params.currPage){
							console.log("没有更多记录哦"); 
							$("#noneTipDiv").html(Gobal.NoneHtml2("没有更多记录了")); 
							_isLoading = true;
						} 
						else{
							_isLoading = false;
						}
					}
					divLoading.hide();
				});
			}
			this.initData = function(){ 
				$("#ulGoodsList").empty(); 
				u();
			}
			this.getNextPage = function(){
				params.currPage++;
				u();
			}
		} 
		recFun = new loadData();
		recFun.initData();
		$(".good-list-box").scroll(function(){
			var o = $(this);
			var theight = o.height();
			if(tabTag == "goods"){
				var uiDivHeight = $(".goods_list").height();
				var top = o.scrollTop();
//				console.log("top=" + top);
				var scp = top + theight;
				if(scp + 222 > uiDivHeight){
					console.log("满足条件1");
					if(!_isLoading){
						console.log("获取分页");
						recFun.getNextPage();
						_isLoading = true;
					}
				}
			}
			else{
				var uiDivHeight = $("#storeList").height();
				var top = o.scrollTop();
				var scp = top + theight;
				if(scp + 222 > uiDivHeight){
					console.log("满足条件1");
					if(!_isLoading2){
						console.log("获取分页");
						recFun2.getNextData();
						_isLoading2 = true;
					}
				}
			}
			
		});
		
		
		var g = function(){
			var data = function(){
				var i = function(){    
					var N = $("#txtSearch").val();
					var key = encodeURI(encodeURI(N));
					return "currPage=" + params2.currPage + "&pageSize=" + params2.pageSize + "&keyword=" + key;
				} 
				$.getJSON(contextPath + "getStoreList",i(),function(json){
					if(json.totalPage <= params2.currPage){
						console.log("没有数据了，所以不需要继续加载了");
						_IsLoading2 = true;
					}
					if(json.code == 1){
						$("#noneTipDiv").html(Gobal.NoneHtmlEx("更多商户正在入驻审核中"));
						_IsLoading2 = true;
					} 
					else{ 
						$("#noneTipDiv").html("");
						$("#storeCount").html(json.totalRecords);
						$.each(json.listItems,function(i,item){
							var G = '<div class="store_item">'
							+ '			<div class="store_img_box">'
							+ '				<img alt="" src="' + item.imgUrl + '">'
							+ '			</div>'
							+ '			<div class="content_box">' 
							+ '				<div class="name">' + item.name + '</div>'
							+ '				<span class="juli" style="color:#777">' + item.address + '</span>'
							+ '			</div>';  
							G = G + '		</div> ';
							var temp = $(G); 
							temp.click(function(){
								location.href = webPath + "storeDetail&id=" + item.id; 
							});
							$("#storeList").append(temp);   
						});
					}
					if(json.totalPage <= params2.currPage){
						console.log("没有更多记录哦"); 
						$("#noneTipDiv").html(Gobal.NoneHtml2("没有更多记录了")); 
						_isLoading2 = true;
					} 
					else{
						_isLoading2 = false;
					}
					divLoading.hide();
				}); 
			}
			this.initData = function(){
				divLoading.show();
				data();
			} 
			this.getNextData = function(){
				params2.currPage++; 
				data();
			}
		}
		recFun2 = new g();
	};
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160304",function(){
		Base.getScript(Gobal.Skin + "js/search.js?v=160523", init);
	}); 
});