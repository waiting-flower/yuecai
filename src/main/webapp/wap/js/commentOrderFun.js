/**
 * 
 */
$(function(){
	var init = function(){
		var fail = function(s){
			$.PageDialog.fail(s);
		}
		var ok = function(s,r){
			$.PageDialog.ok(s,r);
		}
		var currentItemId = null;
		//显示与隐藏晒单div
		$(".btn_write_comment").click(function(){
			var goodsId = $(this).attr("data-goodsid");
			var detailId = $(this).attr("data-productid");
			var orderId = $(this).attr("data-orderid");
			var isShow = $(this).attr("data-show");
			if(isShow == 0){
				$(this).attr("data-show","1");
				$("#fillin_" + detailId).fadeIn();
			}
			else{
				$(this).attr("data-show","0"); 
				$("#fillin_" + detailId).fadeOut();
			}
		});
		//显示与隐藏已经晒单的内容div
		$(".btn_viewcomment").click(function(){
			var goodsId = $(this).attr("data-goodsid");
			var detailId = $(this).attr("data-productid");
			var orderId = $(this).attr("data-orderid");
			var isShow = $(this).attr("data-show");
			if(isShow == 0){
				$(this).attr("data-show","1");
				$("#viewcomment_" + detailId).fadeIn();
				$(this).addClass("active");
			} 
			else{
				$(this).attr("data-show","0"); 
				$("#viewcomment_" + detailId).fadeOut();
				$(this).removeClass("active");
			}
		});
		//另外加一个入口以供上传图片使用
		$(".btn_addpic").click(function(){
			var goodsId = $(this).attr("data-goodsid");
			var detailId = $(this).attr("data-productid");
			var orderId = $(this).attr("data-orderid");
			var isShow = $(this).attr("data-show");
			if(isShow == 0){
				$(this).attr("data-show","1");
				$("#addimgbtn_" + detailId).fadeIn();
				$(this).addClass("active");
			}  
			else{
				$(this).attr("data-show","0"); 
				$("#addimgbtn_" + detailId).fadeOut();
				$(this).removeClass("active");
			}
		});
		
		//发布晒单按钮
		$(".mod_btn").click(function(){
			var goodsId = $(this).attr("data-goodsid");
			var detailId = $(this).attr("data-productid");
			var orderId = $(this).attr("data-orderid");   
			var textarea = $("#fillin_" + detailId).find("textarea").val();
			if(textarea == "" || textarea.length < 6){
				fail("您的晒单内容太短了哦"); 
				return false;
			}
			var iArr = $("#fillin_" + detailId).find("div.stars").find("i.cur");
			var commentScore = iArr.length;
			var ui = [];
			var liArr = $("#fillin_" + detailId).find("ul.images").find("li._userFile");
			$.each(liArr,function(i,item){
				ui.push($(item).find("img").attr("src"));
			}); 
			var imgList = ui.join(",");
			var vData = {
					comType : 1,
					msgContent : textarea,
					otherId : goodsId,
					commentScore : commentScore,
					imgList : imgList,
					detailId : detailId,
					orderId : orderId,
					userId : $("#userId").val()
				};   
			$.post(contextPath + "saveOrderComment",vData,function(json){
				console.log(json);  
				if(json.code == 10){
					location.href = webPath + "login";
				}
				else if(json.code == 0){
					ok("晒单成功，感谢您的分享哦",function(){
						location.href = webPath + "userCenter"; 
					});
				}
				else if(json.code == 1){
					fail(json.msg);
				}
			},"json")
		});
		$(".fabiaotupian").click(function(){
			var postId = $(this).attr("postId");
			var ui = []; 
			var liArr = $("#curr_images_" + postId).find("li._userFile");
			$.each(liArr,function(i,item){
				ui.push($(item).find("img").attr("src"));
			}); 
			var imgList = ui.join(",");
			var vdata = {
				"postId" : postId,
				"imgList" : imgList
			};
			var obj = $(this);
			$.getJSON(contextPath + "addPostImg",vdata,function(json){
				if(json.code == 0){
					ok("添加成功",function(){
						obj.parent().parent().parent().hide();
					});
				}
			});
			
		});
		$(".stars").find("i").click(function(){
			if($(this).hasClass("sss_tip")){
				return false;  
			} 
			var s = parseInt($(this).attr("s"));
			var parent = $(this).parent(); 
			var iArr = parent.find("i").removeClass("cur");
			for(var i = 1; i <= s; i++){ 
				$(iArr[i - 1]).addClass("cur");
			}
		});
		$(".btn_new_add").click(function(){
			$("#file").click();
			currentItemId = $(this).attr("data-commid");
		});
		var currentPostId = "";
		$(".btn_edit_add").click(function(){
			$("#file2").click();
			currentPostId = $(this).attr("postId");
		});
		$("#file2").bind("change",function(){
			var m = $(this).val();
	        if (m != "") {
	             var l = (m.substr(m.length - 5)).substr((m.substr(m.length - 5)).indexOf(".") + 1).toLowerCase();
	             if (l != "gif" && l != "jpg" && l != "jpeg" && l != "bmp" && l != "png") {
	                 $(this).val("");
	                 fail("只能上传JPG、GIF、PNG或BMP图片!");
	                 return false
	             } else {
	                fail("上传中,请稍后");
	                start_upload2(this);
	             }
	         } else {
	             fail("请先选择需要上传的文件!");
	             return false
	         }
		});
		var start_upload2 = function(obj){  
	        if(!isCanvasSupported){  
	           console.log("不支持的浏览器");  
	        }else{  
	           compress(event, function(base64Img){  
	               console.log(base64Img);  
	               // 如需保存  或 把base64转成图片  ajax提交后台处理  
	               putb642(base64Img);
	           });  
	        }   
		}   
		var putb642 = function(img){
			$.getJSON(contextPath + "getUpToken?userId=1",function(json){//token令牌获取
				console.log("令牌获取成功，开始执行上传"); 
				var url = "http://upload.qiniup.com/putb64/-1"; 
				var xhr = new XMLHttpRequest(); 
				xhr.onreadystatechange = function() {
					console.log(xhr.readyState);   
					if (xhr.readyState == 4) {    
						console.log("返回text" + xhr.responseText);
						var ttt = JSON.parse(xhr.responseText); 
						var key = ttt.key;
						console.log("上传成功，得到key" + key); 
						var headUrl = "http://youji.7working.com/" + key;//添加图片
						$("#edit_file_" + currentPostId).before('<li class="_userFile"><img style="width:50px;height:50px;" src="' + headUrl + '"></li>');
					}  
				}
				xhr.open("POST", url, true); 
				xhr.setRequestHeader("Content-Type", "application/octet-stream");
				xhr.setRequestHeader("Authorization","UpToken " + json.upToken);//json.uptoken
				xhr.send(img.split(",")[1]);
			});
	}
		
		$("#file").bind("change",function(){
			var m = $(this).val();
	        if (m != "") {
	             var l = (m.substr(m.length - 5)).substr((m.substr(m.length - 5)).indexOf(".") + 1).toLowerCase();
	             if (l != "gif" && l != "jpg" && l != "jpeg" && l != "bmp" && l != "png") {
	                 $(this).val("");
	                 fail("只能上传JPG、GIF、PNG或BMP图片!");
	                 return false
	             } else {
	                fail("上传中,请稍后");
	                start_upload(this);
	             }
	         } else {
	             fail("请先选择需要上传的文件!");
	             return false
	         }
		});
		var start_upload = function(obj){  
	        if(!isCanvasSupported){  
	           console.log("不支持的浏览器");  
	        }else{  
	           compress(event, function(base64Img){  
	               console.log(base64Img);  
	               // 如需保存  或 把base64转成图片  ajax提交后台处理  
	               putb64(base64Img);
	           });  
	        }   
		}   
		var putb64 = function(img){
				$.getJSON(contextPath + "getUpToken?userId=1",function(json){//token令牌获取
					console.log("令牌获取成功，开始执行上传");
					var url = "http://upload.qiniup.com/putb64/-1"; 
					var xhr = new XMLHttpRequest();
					xhr.onreadystatechange = function() {
						console.log(xhr.readyState);
						if (xhr.readyState == 4) {
							console.log("返回text" + xhr.responseText);
							var ttt = JSON.parse(xhr.responseText); 
							var key = ttt.key;
							console.log("上传成功，得到key" + key);
							var headUrl = "http://youji.7working.com/" + key;//添加图片
							$("#file_" + currentItemId).before('<li class="_userFile"><img style="width:50px;height:50px;" src="' + headUrl + '"></li>');
						}  
					}
					xhr.open("POST", url, true); 
					xhr.setRequestHeader("Content-Type", "application/octet-stream");
					xhr.setRequestHeader("Authorization","UpToken " + json.upToken);//json.uptoken
					xhr.send(img.split(",")[1]);
				});
		}
		$("#openRuleBtn").click(function(){
			$("#ruletips").addClass("show");  
		}); 
		$("#rule_close").click(function(){
			$("#ruletips").removeClass("show");
		});
	}
	Base.getScript(Gobal.Skin + "/js/pageDialog.js?v=160606",function(){
		Base.getScript(Gobal.Skin + "/js/base64image.js?v=160606",init);
	});
});
