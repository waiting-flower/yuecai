$(function(){
	var recFun = null;
	var params = {
		currPage : 1,
		pageSize : 10
	};
	var divLoading = $("#divLoading");
	var _isLoading = false;
	var fail = function (f) {
		$.PageDialog.fail(f);
	};
	var ok = function (f) {
		$.PageDialog.ok(f);
	};
	 
	//初始化
	var init = function(){
		var j = $("#li_top2");
		$(".good-list-box").scroll(function () {
            if ($(".good-list-box").scrollTop() > 100) {
                j.show();
            } else {
                j.hide();
            }
        })
        j.click(function(){
        	 $(this).hide();
        	 $(".good-list-box").animate({
                 scrollTop: 0
             }, 500);
        });
		var loadData = function(){
			var u = function(){
				divLoading.show();
				var i = function(){
					return "currPage=" + params.currPage + "&pageSize=" + params.pageSize;
				} 
				console.log("请求参数=" + i() + "开始请求"); 
				$.getJSON(contextPath + "getTaobaoGoodsList",i(),function(json){
					console.log("请求完成"); 
					if(json.code == 1){//没有任何记录的情况
						_isLoading = true;   
						console.log("无记录显示图片"); 
						$("#noneTipDiv").html('<div class="wx1yyg-kylin-norecords"><i class="wx1yyg-kylin-icon kylin-null-06"></i><strong class="gray9">暂无商品哦~</strong></div>')
					}
					else{ 
						$("#goodsCount").html(json.totalRecords); 
						$.each(json.listItems,function(i,item){  
							var ui = '<div class="search_prolist_item item_longcover" id="' + item.id + '" >'
								+ '    <div class="search_prolist_item_inner" >'
								+ '        <div class="search_prolist_cover" rd="0-4-4">'
								+ '            <img class="photo" src="' + item.imgUrl + '">'
								+ '        </div>' 
								+ '        <div class="search_prolist_info">'  
								+ '            <div class="search_prolist_title">' + item.name 
								+ '            </div>'
								+ '            <div class="search_prolist_price">' 
								+ '                <strong ><em><span class="int">￥' + item.price + '</span></em> </strong>'
								+ '                <span style="float:right;color:#e93b3d;" class="search_prolist_rate">领券立减' + item.coupon + '</span> '
								+ '            </div>'  
								+ '        </div>'
								+ '    </div>' 
								+ '</div>'
							var G = $(ui); 
							G.click(function(){
//								var myUrl = encodeURIComponent("https:" + item.couponClickUrl); 
//		    					BNJS.page.start('bainuo://web?url=' + myUrl);  
								location.href = item.couponClickUrl;
		    				});
							$("#itemList").append(G); 
						}); 
						if(json.totalPage <= params.currPage){
							console.log("没有更多记录哦"); 
							$("#noneTipDiv").html(Gobal.NoneHtml2("没有更多记录了")); 
							_isLoading = true;
						} 
						else{
							_isLoading = false;
						}
					}
					divLoading.hide();
				});
			}
			this.initData = function(){ 
				$("#itemList").empty(); 
				u();
			}
			this.getNextPage = function(){
				params.currPage++;
				u();
			}
		} 
		recFun = new loadData();
		recFun.initData();
		$(".good-list-box").scroll(function(){
			var o = $(this);
			var theight = o.height();
//			console.log("theight=" + theight);
			var uiDivHeight = $(".goods_list").height();
			var top = o.scrollTop();
//			console.log("top=" + top);
			var scp = top + theight;
			if(scp + 222 > uiDivHeight){
				console.log("满足条件1");
				if(!_isLoading){
					console.log("获取分页");
					recFun.getNextPage();
					_isLoading = true;
				}
			}
		}); 
	};
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160304",function(){
		Base.getScript(Gobal.Skin + "js/search.js?v=160523", init);
	}); 
});