/**
 * 
 */
$(function(){
	var init = function(){
		loadImgFun(0); 
		$("#btnPay").click(function(){ 
			var payType = "wx";
			if($("#payZfb").hasClass("checked")){
				payType = "zfb";
			}
			
			var tradeNo = $("#tradeNo").val();
			var addressId = $("#addressId").val();
			if(addressId == ""){
				$.PageDialog.fail("您尚未确认收货地址，无法提交订单");
				return false;
			}
			var remark = $("#remark").val();
			$.post(contextPath + "confirmCartOrder",{
				tradeNo : tradeNo,
				addrId : addressId,
				remark : remark ,
				payType : payType 
			},function(json){
				if(json.code == 0){
					var redirect_url = "http://www.fifty.mobi/shopWap.html?confirmCartOrder&tradeNo=" + tradeNo + "&isPay=1";   
					var redirect_url2 = encodeURI(redirect_url);
					cookie.set("tradeNo",tradeNo);  
					location.href = json.toPage + "&" + redirect_url;
					$("#payDivTips").show(); 
				}
				else if(json.code == 2){ 
					$.PageDialog.fail("订单已支付成功");
				}
			},"json");
		}); 
		$("#payOkBtn").click(function(){
			location.href = myPath + "shopWap/payOkCart-" + $("#tradeNo").val() + ".html";
		});
		$("#payRepeatBtn").click(function(){
			$("#payDivTips").hide();
		});
		$("#payZfb").click(function(){
			$("#payZfb").addClass("checked");
			$("#payWx").removeClass("checked");
		});   
		$("#payWx").click(function(){
			$("#payWx").addClass("checked");
			$("#payZfb").removeClass("checked");
		});
	};
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160606",function(){
		Base.getScript(Gobal.Skin + "js/CartComm.js?v=160606",init);
	});
}); 