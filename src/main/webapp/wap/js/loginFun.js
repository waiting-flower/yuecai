$(function(){
	var init = function(){
		var c = function (t) {
            var myreg=/^[1][3,4,5,7,8][0-9]{9}$/;
            return myreg.test(t)
        }; 
        
		$("#btnLogin").click(function(){
			var mobile = $("#mobile").val();
			var snsCode = $("#snsCode").val();
			if(mobile == null || mobile == ""){
				$.PageDialog.fail("请输入您的手机号码");
				return false; 
			}
			if(!c(mobile)){
				$.PageDialog.fail("请输入正确的手机号码格式");
				return false; 
			}
			if(mobile.substr(0,3) == '166'){
				$.PageDialog.fail("运营商不支持166手机号段，请更换手机号码");
				return false; 
			}
			if(snsCode == null || snsCode == ""){
				$.PageDialog.fail("请输入您收到的短信验证码");
				return false;
			}
			$.post(contextPath + "saveLogin",{
				mobile : mobile,
				snsCode : snsCode
			},function(json){ 
				if(json.code == 3){
					$.PageDialog.fail("您的短信验证码不正确");
				}
				else if(json.code == -1){ 
					$.PageDialog.fail("您因为违规操作已被限制使用");
				}
				else{
					cookie.set("userId",json.id); 
					$.PageDialog.ok("登录成功",function(){
						location.href = webPath + "index";
					});
				}
			},"json");
		});
		
		
		
		var time = 60;
		var inter; 
		$("#btnYzm").click(function(){
			var tempHtml = $("#btnYzm span").html();
			if(tempHtml != "获取短信验证码"){
				return false;
			}
			var mobile = $("#mobile").val();
			if(mobile == null || mobile == ""){
				$.PageDialog.fail("请输入您的手机号码");
				return false;
			} 
			if(!c(mobile)){
				$.PageDialog.fail("请输入正确的手机号码格式");
				return false;
			}   
			$.getJSON(contextPath + "sendSnsCode&snsType=login&mobile=" + mobile,function(json){
				if(json.code == 2){ 
					$.PageDialog.fail("验证码发送失败");
				} 
				else{
					var auto = function(){
						var ttt = time;
						if(time < 10){
							ttt = "0" + time;
						}     
						var nttt = "重新发送" + ttt + "s"; 
						$("#btnYzm span").html(nttt);
						time--;  
						if(ttt <= 1){
							time = 60;    
							$("#btnYzm span").html("获取短信验证码");
							clearInterval(inter); 
						}
					} 
					inter = setInterval(auto, 1000);
				}
			});
		});
	}
	Base.getScript(Gobal.Skin + "/js/pageDialog.js?v=160606", init);
});