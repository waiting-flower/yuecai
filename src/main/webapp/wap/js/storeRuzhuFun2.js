$(function(){
	var fail = function (r) {
		$.PageDialog.fail(r);
    };
    var ok = function (s, r) {
    	$.PageDialog.ok(s, r);
    };
    
	var init = function(){
		$("#ruzhuPreBtn").click(function(){
			location.href = webPath + "storeRuzhu&isReRuzhu=1"; 
		});
		$("#ruzhuNextBtn").click(function(){ 
			var temp = "http://a2.xzyouyou.com/FtrJjzy4WcA10nKamdx7x4FzBH7-";
			var yingyezhizhao = $("#yingyezhizhao_img").attr("src");
			var xiangguanzizhi = $("#xiangguanzizhi_img").attr("src");
			var idCardA = $("#idCardA_img").attr("src");
			var idCardB = $("#idCardB_img").attr("src");
			var idCardPerson = $("#idCardPerson_img").attr("src");
			var mendianzhaopai = $("#mendianzhaopai_img").attr("src");
			var dianneihuanjing = $("#dianneihuanjing_img").attr("src");
			
			var imgUrl = $("#imgUrl_img").attr("src");
			var qrCode = $("#qrCode_img").attr("src");
			
			if(imgUrl == temp){
				fail("请上传店铺logo图片");
				return false;
			}
			if(qrCode == temp){
				fail("请上传客服二维码图片");
				return false;
			}
			if(yingyezhizhao == temp){
				fail("请上传营业执照");
				return false;
			}
			if(xiangguanzizhi == temp){
				fail("请上传相关资质");
				return false;
			}
			if(mendianzhaopai == temp){
				fail("请上传门店招牌");
				return false;
			}
			if(dianneihuanjing == temp){
				fail("请上传店内环境");
				return false;
			}
			if(idCardA == temp){
				fail("请上传身份证正面");
				return false;
			}
			if(idCardB == temp){
				fail("请上传身份证反面");
				return false;
			}
			if(idCardPerson == temp){
				fail("请上传手持身份证照片");
				return false;
			}
			var vData = {
					idCardA : idCardA,
					id : $("#storeId").val(),
					idCardB : idCardB,
					idCardPerson : idCardPerson,
					dianneihuanjing : dianneihuanjing ,
					mendianzhaopai : mendianzhaopai,
					yingyezhizhao : yingyezhizhao,
					xiangguanzizhi : xiangguanzizhi,
					imgUrl : imgUrl,
					qrCode : qrCode
				};
				$.post(contextPath + "saveStoreRuzhu2",vData,function(json){
					if(json.code == 10){
						location.href = webPath + "login&returnUrl=storeRuzhu2"; 
					}
					else if(json.code == 0){
						ok("提交成功",function(){
							location.href = webPath + "storeRuzhu3"; 
						});
					}
				},"json"); 
			/*
			 * private String yingyezhizhao;//营业执照
	private String xiangguanzizhi;//相关资质
	private String idCardA;//身份证正面
	private String idCardB;//身份证反面
	private String idCardPerson;//手持身份证
	private String mendianzhaopai;//门店招牌
	private String dianneihuanjing;//店内环境
			 * */
		});
		var imgUrl_file = $("#imgUrl_file"); //logo封面图
		imgUrl_file.bind("change",function(){
			var m = $(this).val();
	        if (m != "") {
	             var l = (m.substr(m.length - 5)).substr((m.substr(m.length - 5)).indexOf(".") + 1).toLowerCase();
	             if (l != "gif" && l != "jpg" && l != "jpeg" && l != "bmp" && l != "png") {
	                 $(this).val("");
	                 fail("只能上传JPG、GIF、PNG或BMP图片!");
	                 return false
	             } else {
	                fail("上传中,请稍后");
	             }
	         } else {
	             fail("请先选择需要上传的文件!");
	             return false
	         }
	        var size = 0;
	        size = this.files[0].size
	        var myEsize = (size/1024).toFixed(2);
			if(myEsize > 10240){
				fail("上传文件不能超过10M"); 
				return false;
			}
			console.log("图片大小验证通过，开始执行上传");
			start_upload(this,size,"imgUrl_img");
		}); 
		
		
		var qrCode_file = $("#qrCode_file"); //营业执照
		qrCode_file.bind("change",function(){
			var m = $(this).val();
	        if (m != "") {
	             var l = (m.substr(m.length - 5)).substr((m.substr(m.length - 5)).indexOf(".") + 1).toLowerCase();
	             if (l != "gif" && l != "jpg" && l != "jpeg" && l != "bmp" && l != "png") {
	                 $(this).val("");
	                 fail("只能上传JPG、GIF、PNG或BMP图片!");
	                 return false
	             } else {
	                fail("上传中,请稍后");
	             }
	         } else {
	             fail("请先选择需要上传的文件!");
	             return false
	         }
	        var size = 0;
	        size = this.files[0].size
	        var myEsize = (size/1024).toFixed(2);
			if(myEsize > 10240){
				fail("上传文件不能超过10M"); 
				return false;
			}
			console.log("图片大小验证通过，开始执行上传");
			start_upload(this,size,"qrCode_img");
		}); 
		
		var yingyezhizhao_file = $("#yingyezhizhao_file"); //营业执照
		yingyezhizhao_file.bind("change",function(){
			var m = $(this).val();
	        if (m != "") {
	             var l = (m.substr(m.length - 5)).substr((m.substr(m.length - 5)).indexOf(".") + 1).toLowerCase();
	             if (l != "gif" && l != "jpg" && l != "jpeg" && l != "bmp" && l != "png") {
	                 $(this).val("");
	                 fail("只能上传JPG、GIF、PNG或BMP图片!");
	                 return false
	             } else {
	                fail("上传中,请稍后");
	             }
	         } else {
	             fail("请先选择需要上传的文件!");
	             return false
	         }
	        var size = 0;
	        size = this.files[0].size
	        var myEsize = (size/1024).toFixed(2);
			if(myEsize > 10240){
				fail("上传文件不能超过10M"); 
				return false;
			}
			console.log("图片大小验证通过，开始执行上传");
			start_upload(this,size,"yingyezhizhao_img");
		}); 
		
		
		var xiangguanzizhi_file = $("#xiangguanzizhi_file"); //相关资质
		xiangguanzizhi_file.bind("change",function(){
			var m = $(this).val();
	        if (m != "") {
	             var l = (m.substr(m.length - 5)).substr((m.substr(m.length - 5)).indexOf(".") + 1).toLowerCase();
	             if (l != "gif" && l != "jpg" && l != "jpeg" && l != "bmp" && l != "png") {
	                 $(this).val("");
	                 fail("只能上传JPG、GIF、PNG或BMP图片!");
	                 return false
	             } else {
	                fail("上传中,请稍后");
	             }
	         } else {
	             fail("请先选择需要上传的文件!");
	             return false
	         }
	        var size = 0;
	        size = this.files[0].size
	        var myEsize = (size/1024).toFixed(2);
			if(myEsize > 10240){
				fail("上传文件不能超过10M"); 
				return false;
			}
			console.log("图片大小验证通过，开始执行上传");
			start_upload(this,size,"xiangguanzizhi_img");
		}); 
		 
		
		var mendianzhaopai_file = $("#mendianzhaopai_file"); //门店招牌 
		mendianzhaopai_file.bind("change",function(){
			var m = $(this).val();
	        if (m != "") {
	             var l = (m.substr(m.length - 5)).substr((m.substr(m.length - 5)).indexOf(".") + 1).toLowerCase();
	             if (l != "gif" && l != "jpg" && l != "jpeg" && l != "bmp" && l != "png") {
	                 $(this).val("");
	                 fail("只能上传JPG、GIF、PNG或BMP图片!");
	                 return false
	             } else {
	                fail("上传中,请稍后");
	             }
	         } else {
	             fail("请先选择需要上传的文件!");
	             return false
	         }
	        var size = 0;
	        size = this.files[0].size
	        var myEsize = (size/1024).toFixed(2);
			if(myEsize > 10240){
				fail("上传文件不能超过10M"); 
				return false;
			}
			console.log("图片大小验证通过，开始执行上传");
			start_upload(this,size,"mendianzhaopai_img");
		}); 
		
		
		var dianneihuanjing_file = $("#dianneihuanjing_file"); //店内环境
		dianneihuanjing_file.bind("change",function(){
			var m = $(this).val();
	        if (m != "") {
	             var l = (m.substr(m.length - 5)).substr((m.substr(m.length - 5)).indexOf(".") + 1).toLowerCase();
	             if (l != "gif" && l != "jpg" && l != "jpeg" && l != "bmp" && l != "png") {
	                 $(this).val("");
	                 fail("只能上传JPG、GIF、PNG或BMP图片!");
	                 return false
	             } else {
	                fail("上传中,请稍后");
	             }
	         } else {
	             fail("请先选择需要上传的文件!");
	             return false
	         }
	        var size = 0;
	        size = this.files[0].size
	        var myEsize = (size/1024).toFixed(2);
			if(myEsize > 10240){
				fail("上传文件不能超过10M"); 
				return false;
			}
			console.log("图片大小验证通过，开始执行上传");
			start_upload(this,size,"dianneihuanjing_img");
		}); 
		
		
		
		var idCardA_file = $("#idCardA_file"); //身份证正面
		idCardA_file.bind("change",function(){
			var m = $(this).val();
	        if (m != "") {
	             var l = (m.substr(m.length - 5)).substr((m.substr(m.length - 5)).indexOf(".") + 1).toLowerCase();
	             if (l != "gif" && l != "jpg" && l != "jpeg" && l != "bmp" && l != "png") {
	                 $(this).val("");
	                 fail("只能上传JPG、GIF、PNG或BMP图片!");
	                 return false
	             } else {
	                fail("上传中,请稍后");
	             }
	         } else {
	             fail("请先选择需要上传的文件!");
	             return false
	         }
	        var size = 0;
	        size = this.files[0].size
	        var myEsize = (size/1024).toFixed(2);
			if(myEsize > 10240){
				fail("上传文件不能超过10M"); 
				return false;
			}
			console.log("图片大小验证通过，开始执行上传");
			start_upload(this,size,"idCardA_img");
		}); 
		
		var idCardB_file = $("#idCardB_file"); //身份证反面
		idCardB_file.bind("change",function(){
			var m = $(this).val(); 
	        if (m != "") {
	             var l = (m.substr(m.length - 5)).substr((m.substr(m.length - 5)).indexOf(".") + 1).toLowerCase();
	             if (l != "gif" && l != "jpg" && l != "jpeg" && l != "bmp" && l != "png") {
	                 $(this).val("");
	                 fail("只能上传JPG、GIF、PNG或BMP图片!");
	                 return false
	             } else {
	                fail("上传中,请稍后");
	             }
	         } else {
	             fail("请先选择需要上传的文件!");
	             return false
	         }
	        var size = 0;
	        size = this.files[0].size
	        var myEsize = (size/1024).toFixed(2);
			if(myEsize > 10240){
				fail("上传文件不能超过10M"); 
				return false;
			}
			console.log("图片大小验证通过，开始执行上传");
			start_upload(this,size,"idCardB_img");
		}); 
		
		var idCardPerson_file = $("#idCardPerson_file"); //手持身份证
		idCardPerson_file.bind("change",function(){
			var m = $(this).val();
	        if (m != "") {
	             var l = (m.substr(m.length - 5)).substr((m.substr(m.length - 5)).indexOf(".") + 1).toLowerCase();
	             if (l != "gif" && l != "jpg" && l != "jpeg" && l != "bmp" && l != "png") {
	                 $(this).val("");
	                 fail("只能上传JPG、GIF、PNG或BMP图片!");
	                 return false
	             } else {
	                fail("上传中,请稍后");
	             }
	         } else {
	             fail("请先选择需要上传的文件!");
	             return false
	         }
	        var size = 0;
	        size = this.files[0].size
	        var myEsize = (size/1024).toFixed(2);
			if(myEsize > 10240){
				fail("上传文件不能超过10M"); 
				return false;
			}
			console.log("图片大小验证通过，开始执行上传");
			start_upload(this,size,"idCardPerson_img");
		}); 
		
		
		
		var start_upload = function(obj,size,imgId){  
	        if(!isCanvasSupported){  
	           console.log("不支持的浏览器");  
	        }else{  
	           compress(event, function(base64Img){  
	               // 如需保存  或 把base64转成图片  ajax提交后台处理  
	               putb64(base64Img,size,imgId);
	           });  
	        }   
		}   
		
		var appImg = function(url,imgId){
			$("#" + imgId).attr("src",url);
		}
		//七牛云base64上传图片
		var putb64 = function(pic, size,imgId) {
			var token = function(){
				console.log("获取token令牌");
				$.getJSON(contextPath + "getUpToken",function(json){//token令牌获取
				
					console.log("开始上传七牛云");
					console.log("令牌获取成功，开始执行上传");
					var url = "http://upload.qiniu.com/putb64/-1"; 
					var xhr = new XMLHttpRequest();
					xhr.onreadystatechange = function() {
						console.log(xhr.readyState);
						if (xhr.readyState == 4) {
							console.log("返回text" + xhr.responseText);
							var ttt = JSON.parse(xhr.responseText); 
							var key = ttt.key;
							console.log("上传成功，得到key" + key);
							appImg("http://zhuanmi.xzyouyou.com/" + key,imgId);//添加图片
						}    
					}
					xhr.open("POST", url, true);
					xhr.setRequestHeader("Content-Type", "application/octet-stream");
					xhr.setRequestHeader("Authorization","UpToken " + json.upToken);//json.uptoken
					xhr.send(pic.split(",")[1]);
				});
			}
			token();
		}
	}
	
	  
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160606",init);
});