/**
 * 
 */
$(function(){
	var init = function(){
		loadImgFun(0); 
		$("#btnPay").click(function(){
			var payType = "wx";
			if($("#payZfb").hasClass("checked")){
				payType = "zfb";
			}
			var num = $("#num").val();
			var goodsId = $("#goodsId").val();
			var addressId = $("#addressId").val();
			if(addressId == ""){
				$.PageDialog.fail("您尚未确认收货地址，无法提交订单");
				return false;
			}
			var remark = $("#remark").val();
			$.post(contextPath + "saveScoreGoodsOrder",{
				id : goodsId,
				addrId : addressId,
				remark : remark,
				num : num 
			},function(json){
				console.log(json); 
				if(json.code == 0){ 
//					var redirect_url = "https://m.zhuanmi.mobi/shopWap.html?confirmOrder&orderId=" + orderId + "&isPay=1";   
//					var redirect_url2 = encodeURI(redirect_url);
//					cookie.set("orderId",orderId);  
//					location.href = json.toPage + "&" + redirect_url;
					location.href = webPath + "scoreGoodsBuyOk&orderId=" + json.orderId;
				}
				else{ 
					$.PageDialog.fail(json.msg);
				}
			},"json");
		});
	};
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160606",function(){
		Base.getScript(Gobal.Skin + "js/CartComm.js?v=160606",init);
	});
}); 