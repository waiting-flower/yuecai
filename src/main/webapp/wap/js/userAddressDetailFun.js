$(function(){
	
	var init = function(){
		var fail = function (f) {
			$.PageDialog.fail(f)
		};
		var ok = function (f) {
			$.PageDialog.ok(f)
		};
		var c = function (t) {
            var myreg=/^[1][3,4,5,7,8][0-9]{9}$/;
            return myreg.test(t)
        }; 
		
		
		$("#Add_address").click(function(){
			var addrId = $("#addrId").val();
			var name = $("#name").val(); 
			var mobile = $("#mobile").val(); 
			var address = $("#address").val();
			var pro = $("#pro").val();
			var city = $("#city").val();
			var country = $("#country").val();
			if(pro == ""){
				fail("请选择省份");
				return false;
			}
			if(city == ""){
				fail("请选择城市");
				return false;
			}
			if(country == ""){
				fail("请选择区县");
				return false;
			}
			
			if(name == ""){
				fail("请输入收件人姓名");
				$("#name").focus();
				return false;
			}
			if(mobile == ""){
				fail("请输入收件人联系手机");
				$("#mobile").focus();
				return false;
			}
			if(!c(mobile)){ 
				$.PageDialog.fail("请输入正确的手机号码格式");
				return false; 
			}
			if(address == ""){
				fail("请输入收货详细地址");
				$("#address").focus();
				return false;
			}
			var vData = {
				pro : pro,
				city : city,
				county : country,
				name : name,
				mobile : mobile,
				info :address,
				id : addrId
			} 
			$.post(contextPath + "saveAddress",vData,function(json){
				if(json.code == 0){
					ok("操作成功");
					var h = function(){ 
						var orderId = $("#orderId").val();
						var tradeNo = $("#tradeNo").val();
						var scoreGoodsId = $("#scoreGoodsId").val();
						var num = $("#num").val();
						if(orderId == ""){
							if(scoreGoodsId != ""){
								G.click(function(){
									location.href = webPath + "scoreGoodsConfirm&goodsId=" 
										+ scoreGoodsId + "&num=" + num + "&addId=" + item.id;
								});
							}
							location.href = webPath + "userAddressList";
						}
						else{
							if(tradeNo == ""){
								location.href = webPath + "confirmOrder&orderId=" + orderId + "&addId=" + json.id;	
							}
							else{
								location.href = webPath + "confirmCartOrder&tradeNo=" + tradeNo + "&addId=" + json.id;
							}
						}
						
					}
					setTimeout(h,250);
				}
				else if(json.code == 10){
					location.href = webPath + "login";
				}
				else{
					fail("服务器繁忙，请稍后重新尝试");
				}
			},"json");
		});
	}
	Base.getScript(Gobal.Skin + "/js/pageDialog.js?v=160606",init);
});