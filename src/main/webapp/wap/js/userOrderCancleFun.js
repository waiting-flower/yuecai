$(function(){
	var init = function(){
		//dialog confrim
		var r = function (L, F, G, E, N) {
			
	         var H = 255;
	         var K = 126;
	         if (typeof (E) != "undefined") {
	             H = E
	         }
	         if (typeof (E) != "undefined") {
	             K = N
	         }
	         var M = null;
	         var O = '<div class="clearfix m-round u-tipsEject"><div class="u-tips-txt">' 
	         	+ L + '</div><div class="u-Btn"><div class="u-Btn-li"><a href="javascript:;" id="btnMsgCancel" class="z-CloseBtn">取消</a></div><div class="u-Btn-li"><a id="btnMsgOK" href="javascript:;" class="z-DefineBtn">确定</a></div></div></div>';
	         var J = function () {
	             var P = $("#pageDialog");
	             P.find("a.z-DefineBtn").click(function () {
	                 if (typeof (F) != "undefined" && F != null) {
	                     F();
	                 }
	                 I.close();
	             });
	             P.find("a.z-CloseBtn").click(function () {
	                 if (typeof (G) != "undefined" && G != null) {
	                     G()
	                 }
	                 I.cancel()
	             })
	         };
	         var I = new $.PageDialog(O, {
	             W: H,
	             H: K, 
	             close: true,
	             autoClose: false,
	             ready: J
	         })
	     };
	    
	     $(".cancle_reason_list li").click(function(){
	    	 $(".cancle_reason_list").find("li").removeClass("selected"); 
	    	 $(this).addClass("selected");  
	     });
	     $("#btnConfirm").click(function(){
	    	 var reason = $(".cancle_reason_list").find(".selected").attr("reasonid");
	    	 if(reason == ""){
	    		 $.PageDialog.fail("请选择取消原因")
	    		 return false; 
	    	 }
	    	 var id = $("#hidOrderId").val();
				var S = function(){
					var reason = $(".cancle_reason_list").find(".selected").attr("reasonid");
					$.post(contextPath + "confirmCancleOrder&orderId=" + id,{
						reason : reason
					},function(json){
						if(json.code == 0){ 
							$.PageDialog.ok("订单成功，付款将原路返回到您的支付账号",function(){
								location.href = webPath + "orderList"; 
							});
						} 
						else if(json.code == 1){
							$.PageDialog.fail(json.msg);
						}
						else if(json.code == 10){
							location.href = webPath + "login";
						} 
					},"json");
				}
	    	 r("是否确认取消该订单",S); 
	     });
	    $("#btnCancle").click(function(){
	    	history.go(-1); 
	    });
	};
	Base.getScript(Gobal.Skin + "/js/pageDialog.js?v=160606",init);
});