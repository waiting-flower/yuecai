$(function(){
	var hammer = '';
	var currentIndex = 0;
	var body_width = $('body').width();
	var body_height = $('body').height();
	var init = function(){
		var fail = function (r) {
			$.PageDialog.fail(r);
	    };
	    var ok = function (s, r) {
	    	$.PageDialog.ok(s, r);
	    };
//		$('#upload2').on('touchstart', function () {
//			//图片上传按钮
//			console.log("图片按钮touchStart");
//			$('#file').click();
//		}); 
		$('#upload2').bind('click', function () {
			console.log("图片按钮点击");
			//图片上传按钮
			$('#file').click();
		}); 

		var saveImageInfo = function() {
			var filename = $('#hit').attr('fileName');
			var img_data = $('#hit').attr('src');
			if(img_data==""){
				//show tip
				fail("图片上传异常，请刷新页面重新尝试");
				return false;
			}
		   	render(img_data);
		   	console.log(img_data);  
		   	var upHead = function(url){
		   		$.getJSON(contextPath + "saveUserPhoto&url=" + url,function(json){
		   			if (json.code == 0) {
						//data 为返回文件名；
						ok('头像提交成功',function(){
							location.href = webPath + "userInfo";
						});
					}
		   		}); 
		   	}
		   	$.getJSON(contextPath + "getUpToken",function(json){//token令牌获取
				
				console.log("开始上传七牛云");
				console.log("令牌获取成功，开始执行上传");
				var url = "http://upload-z1.qiniup.com/putb64/-1"; 
				var xhr = new XMLHttpRequest();
				xhr.onreadystatechange = function() {
					console.log(xhr.readyState);
					if (xhr.readyState == 4) {
						console.log("返回text" + xhr.responseText);
						var ttt = JSON.parse(xhr.responseText); 
						var key = ttt.key;
						console.log("上传成功，得到key" + key);
						var headUrl = "http://zhuanmi.xzyouyou.com/" + key;//添加图片
						upHead(headUrl); 
					}  
				}
				xhr.open("POST", url, true);
				xhr.setRequestHeader("Content-Type", "application/octet-stream");
				xhr.setRequestHeader("Authorization","UpToken " + json.upToken);//json.uptoken
				xhr.send(img_data.split(",")[1]);
			});
		}

		/*获取文件拓展名*/
		var getFileExt = function(str) {
			var d = /\.[^\.]+$/.exec(str);
			return d;
		}
		var render = function(src){  
			 var MAX_HEIGHT = 256;  //Image 缩放尺寸 
		    // 创建一个 Image 对象  
		    var image = new Image();  
			
		    // 绑定 load 事件处理器，加载完成后执行  
		    image.onload = function(){  
				// 获取 canvas DOM 对象  
		  		var canvas = document.getElementById("myCanvas"); 
		        // 如果高度超标  
		        if(image.height > MAX_HEIGHT) {  
		            // 宽度等比例缩放 *=  
		            image.width *= MAX_HEIGHT / image.height;  
		            image.height = MAX_HEIGHT;  
		        }  
		        // 获取 canvas的 2d 环境对象,  
		        // 可以理解Context是管理员，canvas是房子  
		        var ctx = canvas.getContext("2d");  
		        // canvas清屏  
		        ctx.clearRect(0, 0, canvas.width, canvas.height); 
		        canvas.width = image.width;        // 重置canvas宽高  
		        canvas.height = image.height;  
		        // 将图像绘制到canvas上  
		        ctx.drawImage(image, 0, 0, image.width, image.height);  
		        // !!! 注意，image 没有加入到 dom之中  
				
			 var dataurl = canvas.toDataURL("image/jpeg");  
			 var imagedata =  encodeURIComponent(dataurl); 
		    };  
		    // 设置src属性，浏览器会自动加载。  
		    // 记住必须先绑定render()事件，才能设置src属性，否则会出同步问题。  
		    image.src = src;	
		};  
		$("#clipArea").photoClip({
			width: body_width * 0.8125,
			height: body_width * 0.8125,
			file: "#file",
			view: "#hit",
			ok: "#clipBtn",
			loadStart: function () {
				console.log("照片读取中");
				$('.lazy_tip span').text('');
				$('.lazy_cover,.lazy_tip').show();
			},
			loadComplete: function () {
				console.log("照片读取完成");
				$('.lazy_cover,.lazy_tip').hide();
			},
			clipFinish: function (dataURL) {
				$('#hit').attr('src', dataURL);
				saveImageInfo();
			}
		});
	};
	
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160304",function(){
		Base.getScript(Gobal.Skin + "/js/upload/sonic.js?v=160304", function(){
			Base.getScript(Gobal.Skin + "/js/upload/comm.js?v=160304", function(){
				Base.getScript(Gobal.Skin + "/js/upload/hammer.js?v=160304", function(){
					Base.getScript(Gobal.Skin + "/js/upload/iscroll-zoom.js?v=160304", function(){
						Base.getScript(Gobal.Skin + "/js/upload/jquery.photoClip.js?v=160304", init);
					})
				})
			})
		})
	})
});