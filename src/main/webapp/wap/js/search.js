	var p = function () {
		var c = 15;
        var G = $("#divSearch"),
            E = G.find("input"),
            C = $(".history");
        var B = {
            boxShow: function () {
                G.addClass("focus-box");
                $(".search-info").show();  
//                $(".all-list-wrapper").hide(); 
                $(".cate_list_box").hide(); 
                $(".footer").hide();
            }, boxHide: function () {
                G.removeClass("focus-box");
                $(".search-info").hide();
//                $(".all-list-wrapper").show();
                $(".cate_list_box").show();
                $(".footer").show();  
            }, setHash: function () {
                if (document.location.hash == "") {
                    this.boxHide();
                    E.blur(); 
                } else {
                    if (document.location.hash = "#sbox") {
                        this.boxShow();
                    }
                }
            }
        };
        if (document.location.hash === "#sbox") {
            B.boxShow()
        }
        E.on("focus", function () {
            document.location.hash = "#sbox"
        });
        $(window).on("hashchange", function () {
            B.setHash()
        });
        Array.prototype.remove = function (I) {
            var H = this.indexOf(I);
            if (H > -1) {
                this.splice(H, 1)
            }
        };
        var F = {
            searchData: [],
            add: function (H) {
                if (H == null || H == undefined || H == "") {
                    return
                }
                this.del(H);
                var I = "";
                this.searchData = window.localStorage.searchData;
                if (this.searchData != undefined && this.searchData.length > 0) {
                    I = H + "," + this.searchData;
                    this.searchData = I.split(",");
                    if (this.searchData.length > c) {
                        this.searchData.splice(c, 1)
                    }
                    I = this.searchData.toString()
                } else {
                    I = H
                }
                window.localStorage.setItem("searchData", I)
            }, del: function (I) {
                this.searchData = window.localStorage.searchData;
                if (this.searchData != undefined && this.searchData.length > 0) {
                    var H = this.searchData.split(",");
                    if (H.length > 0 && H.indexOf(I) > -1) {
                        H.remove(I);
                        window.localStorage.setItem("searchData", H.toString())
                    }
                }
            }, clear: function () {
                delete localStorage.searchData
            }, get: function () {
                this.searchData = window.localStorage.searchData
            }, fresh: function (L) {
                this.searchData = window.localStorage.searchData;
                if (this.searchData != undefined && this.searchData.length > 0) {
                    var H = this.searchData.split(","),
                        I = "",
                        M = '<ul class="his-list thin-bor-top">',
                        K = "";
                    for (var J = 0; J < H.length; J++) {
                        K += '<li wd="' + H[J] + '" class="thin-bor-bottom"><a class="items">' + H[J] + "</a></li>"
                    }
                    if (K != "") {
                        I = M + K + "</ul>";
                        I += '<div class="cle-cord thin-bor-bottom" id="btnClear">清空历史记录</div>';
                        L.on("click", "li", function () {
                            var O = $(this),
                                N = O.attr("wd");
                            F.add(N);
                            F.fresh(L);
                            var key = encodeURI(encodeURI(N)); 
                            $.post(contextPath + "judgeIsCityButler",{
                            	searchKey : N
                            },function(json){
                            	if(json.code == "0"){
                            		location.href = webPath + "searchResult&keyword=" + key;
                            	}
                            	else{
                            		location.href = webPath + "cityShop&cityButlerId=" + json.id;
                            	}
                            },"json");
                        });
                        L.on("click", "#btnClear", function () {
                            F.clear();
                            F.fresh(L)
                        });
                        L.html(I); 
                        C.show();
                    }
                } else {
                    L.html("");
                    C.hide();
                }
            }
        };
        var D = $("#divSearchHotHistory"),
            A = $("#btnClearInput");
        F.fresh(D);
        $("#ulSearchHot").on("click", "li", function () {
            var I = $(this),
                H = I.attr("wd");
            F.add(H);
            var key = encodeURI(encodeURI(H));
            $.post(contextPath + "judgeIsCityButler",{
            	searchKey : H
            },function(json){
            	if(json.code == "0"){
            		location.href = webPath + "searchResult&keyword=" + key;
            	}
            	else{
            		location.href = webPath + "cityShop&cityButlerId=" + json.id;
            	}
            },"json");
        });
        $("#btnSearch").click(function () {
            var H = E.val();
            F.add(H);
            if (H == "") {
                H = "爆款"
            }
            var key = encodeURI(encodeURI(H));  
            $.post(contextPath + "judgeIsCityButler",{
            	searchKey : H
            },function(json){
            	if(json.code == "0"){
            		location.href = webPath + "searchResult&keyword=" + key;
            	}
            	else{
            		location.href = webPath + "cityShop&cityButlerId=" + json.id;
            	}
            },"json");
        });
        E.bind("input", function () {
            if ($(this).val() == "") {
                A.hide()
            } else {
                A.show()
            }
        });
        A.bind("click", function () {
            E.val("").focus();
            $(this).hide()
        })
    };// 搜索框事件结束
    p();  