/**
 * 
 */
$(function(){
	var init = function(){
		loadImgFun(0); 
		$("#btnPay").click(function(){
			var payType = "wx";
			if($("#payZfb").hasClass("checked")){
				payType = "zfb";
			}
			var isXuni = $("#isXuni").val();
			var orderId = $("#orderId").val();
			var addressId = $("#addressId").val();
			if(addressId == "" && isXuni == "0"){
				$.PageDialog.fail("您尚未确认收货地址，无法提交订单");
				return false;
			}
			var remark = $("#remark").val();
			if(payType == "wx"){
				$.post(contextPath + "confirmOrder",{
					orderId : orderId,
					addrId : addressId,
					remark : remark,
					payType : payType
				},function(json){
					console.log(json); 
					if(json.code == 0){ 
						var redirect_url = "http://www.fifty.mobi/shopWap.html?confirmOrder&orderId=" + orderId + "&isPay=1";   
						var redirect_url2 = encodeURI(redirect_url);
						cookie.set("orderId",orderId);  
						location.href = json.toPage + "&" + redirect_url;
						$("#payDivTips").show(); 
					}
					else if(json.code == 2){ 
						$.PageDialog.fail("订单已支付成功");
					}
				},"json");
			} 
			else{
				location.href = myPath +  'shopWap/aplipay.html?orderId=' + orderId + "&addrId=" + addressId + "&remark=" + remark;
			}
		});
		$("#payOkBtn").click(function(){
			location.href = myPath + "shopWap/payOk-" + $("#orderId").val() + ".html";
		});
		$("#payRepeatBtn").click(function(){
			$("#payDivTips").hide();
		});
		$("#payZfb").click(function(){
			$("#payZfb").addClass("checked");
			$("#payWx").removeClass("checked");
		});   
		$("#payWx").click(function(){
			$("#payWx").addClass("checked");
			$("#payZfb").removeClass("checked");
		});    
	};
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160606",function(){
		Base.getScript(Gobal.Skin + "js/CartComm.js?v=160606",init);
	});
}); 