$(function () {
    var a = function () {
        var f = 10;
        var e = null;
        var b = $("#dlList");
        var d = $("div.loading");
        var c = function () {
            var i = 0;
            var g = {
                currPage: 1,
                pageSize: f
            };
            var h = function () {
                return "currPage=" + g.currPage + "&pageSize=" + g.pageSize;
            };
            var j = function () {
                $.getJSON(contextPath + "getUserScoreList", h(), function (o) {
                    if (o.code == 0) {
                    	d.hide();
                        var m = "";
                        var n = o.listItems;
                        var l = n.length;
                        for (var k = 0; k < l; k++) { 
                            m += '<dd class="colorbbb"><span class="gray6">'      
                            	+ n[k].pointFrom + "</span>" + n[k].logTime + (n[k].pointType ==  'plus' ? '<em class="green" style="right:20px;">收入' 
                            			+ n[k].pointVal + "</em>" : '<em class="orange" style="right:20px;">支出' 
                            				+ n[k].pointVal + "</em>") + "</dd>" 
                        }
                        b.append(m);
                        if (g.currPage < o.totalPage) {
                            _IsLoading = false
                        } 
                    } else {
                        if (o.code == 10) {
                            location.href = webPath + "login"; 
                        } else {
                            d.hide();
                            _IsLoading = true;
                            if (o.code == 1) {
                                b.append(Gobal.NoneHtmlEx("暂无记录"))
                            }
                        }
                    }
                })
            };
            this.initPage = function () {
                j();
            };
            this.getNextPage = function () {
            	console.log("获取下一页数据");
                g.currPage++;
                j();
            }
        };
        e = new c();
        e.initPage();
        scrollForLoadData(e.getNextPage)
    };
    a()
});