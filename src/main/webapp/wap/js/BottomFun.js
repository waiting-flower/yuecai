var Gobal = new Object();

//操作cookie的对象
var cookie = {
	set: function(name, value) {
		var Days = 30;
		var exp = new Date();
		exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
		document.cookie = name + '=' + escape(value) + ';expires=' + exp.toGMTString();
	}, 
	get: function(name) {
		var arr, reg = new RegExp('(^| )' + name + '=([^;]*)(;|$)');
		if(arr = document.cookie.match(reg)) {
			return unescape(arr[2]);
		} else {
			return null;
		}
	},
	del: function(name) {
		var exp = new Date();
		exp.setTime(exp.getTime() - 1);
		var cval = getCookie(name);
		if(cval != null) {
			document.cookie = name + '=' + cval + ';expires=' + exp.toGMTString();
		}
	}
}; 
/**
 * 时间戳转换为Yyyy-mm-dd
 * @param obj
 * @returns {String}
 */
function fmtDate(obj){
    var date =  new Date(obj);
    var y = 1900+date.getYear();
    var m = "0"+(date.getMonth()+1);
    var d = "0"+date.getDate();
    return y+"-"+m.substring(m.length-2,m.length)+"-"+d.substring(d.length-2,d.length);
}
function getDateTimeStamp(dateStr){
	 return Date.parse(dateStr.replace(/-/gi,"/"));
}
function getDateDiff(dateStr){
	var dateTimeStamp = getDateTimeStamp(dateStr);
	var minute = 1000 * 60;
	var hour = minute * 60;
	var day = hour * 24;
	var halfamonth = day * 15;
	var month = day * 30;
	var now = new Date().getTime();
	var diffValue = now - dateTimeStamp;
	if(diffValue < 0){return;}
	var monthC =diffValue/month;
	var weekC =diffValue/(7*day);
	var dayC =diffValue/day;
	var hourC =diffValue/hour;
	var minC =diffValue/minute;
	if(monthC>=1){
		result="" + parseInt(monthC) + "月前";
	}
	else if(weekC>=1){
		result="" + parseInt(weekC) + "周前";
	}
	else if(dayC>=1){
		result=""+ parseInt(dayC) +"天前";
	}
	else if(hourC>=1){
		result=""+ parseInt(hourC) +"小时前";
	}
	else if(minC>=1){
		result=""+ parseInt(minC) +"分钟前";
	}else
	result="刚刚";
	return result;
}
function loadImgFun(c) {
    var b = $("#loadingPicBlock");
    if (b.length > 0) {
        var i = "src2";
        Gobal.LoadImg = b.find("img[" + i + "]");
        var a = function () {
            return $(window).scrollTop()
        };
        var e = function () {
            return $(window).height() + a() + 50
        };
        var h = function () {
            Gobal.LoadImg.each(function (j) {
                if ($(this).offset().top <= e()) {
                    var k = $(this).attr(i);
                    if (k) {
                        $(this).attr("src", k).removeAttr(i).show()
                    }
                }
            })
        };
        var d = 0;
        var f = -100;
        var g = function () {
            d = a();
            if (d - f > 50) {
                f = d;
                h()
            }
        };
        if (c == 0) {
            $(window).bind("scroll", g)
        }
        g()
    }
}
Gobal.scrollChangeFastNav = function () {
    var d = 0,
        b = 0,
        a = 0,
        e = $("#div_fastnav"),
        f = e.children().eq(-1),
        c = $("#li_menu", e);
    $window = $(window);
    if (e.length < 1) {
        return
    }
    $window.on("touchstart", function (g) {
        d = g.originalEvent.targetTouches[0].clientY
    });
    $window.on("touchend", function (g) {
        b = g.originalEvent.changedTouches[0].clientY, a = b - d;
        if (Math.abs(a) > 10) {
            if (a > 0) {
                if (!e.hasClass("show")) {
                    e.removeClass("show hide").addClass("show")
                }
            } else {
                if (a < 0) {
                    if (!e.hasClass("hide")) {
                        if (f.css("display") === "block") {
                            c.trigger("click")
                        }
                        e.removeClass("show hide").addClass("hide")
                    }
                }
            }
        }
    })
};
var IsMasked = false;
var addNumToCartFun = null;
var _IsLoading = false;
var _IsIOS = false; 
 
function scrollForLoadData(a) {
    $(window).scroll(function () {
        var c = $(document).height();
        var b = $(window).height();
        var d = $(document).scrollTop() + b;
        if (c - d <= b * 4) {
            if (!_IsLoading && a) {
                _IsLoading = true;
                a()
            }
        } 
    })
}
(function () {
    var b = $("#hidIsHttps").val() == "1" ? true : false;
    Gobal.SiteVer = $("#hidSiteVer").length == 0 ? "v1" : $("#hidSiteVer").val();
    //Gobal.WxDomain = $("#hidWxDomain").length == 0 ? "https://weixin.1yyg.com" : $("#hidWxDomain").val();
    Gobal.WxDomain = myDomain;
    Gobal.Skin = mySkinUrl;
    Gobal.LoadImg = null;
    Gobal.LoadHtml = '<div class="loadImg">正在加载</div>';
    Gobal.LoadPic = "https://skin.1yyg.net/" + Gobal.SiteVer + "/weixin/images/loading2.gif?v=130820";
    Gobal.NoneHtml = '<div class="noRecords colorbbb clearfix"><s></s>暂无记录</div>';
    Gobal.NoneHtml2 = function(c){ 
    	return '<div class="noRecords colorbbb clearfix">' + c + '</div>';
    } 
    Gobal.NoneHtmlEx = function (c) {
        return '<div class="noRecords colorbbb clearfix"><s class="z-y9"></s>' + c + "</div>"
    };
    Gobal.LookForPC = '<div class="g-suggest clearfix">没有更多记录了</div>';
    Gobal.ErrorHtml = function (c) {
        return '<div class="g-suggest clearfix">抱歉，加载失败，请重试[' + c + "]</div>"
    }; 
    Gobal.unlink = "javascript:void(0);";
    Gobal.IsWeixin = (navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == "micromessenger");
    loadImgFun(0);
    var a = function () {
        if (!Gobal.IsWeixin) {
            $.cookie("_wxSiteFlag", "tm", {
                domain: myDomain,
                path: "/",
                expires: 365
            })
        } else {
            $.cookie("_wxSiteFlag", "tm", {
                domain: myDomain,
                path: "/",
                expires: -1
            })
        }
        _IsIOS = browser.versions.ios || browser.versions.iPhone || browser.versions.iPad;
        var m = $("#btnCart");
        if (m.length > 0) {
            Base.getScript(Gobal.Skin + "js/CartComm.js?v=160523", function () {
                /*var p = new $CartComm();
                p.getShopCartNum(function (q) {
                    if (q > 0) {
                        m.find("i").html(q > 99 ? '<b class="tomore" num="' + q + '">...</b>' : '<b num="' + q + '">' + q + "</b>")
                    }
                })*/  
            })
        }
        addNumToCartFun = function (p) {
            m.find("i").html(p > 99 ? '<b class="tomore" num="' + p + '">...</b>' : '<b num="' + p + '">' + p + "</b>")
        };
        var l = function (q) {
            var p = new Date();
            q.attr("src", q.attr("data") + "?v=" + GetVerNum()).removeAttr("id").removeAttr("data")
        };
        var h = $("#pageJS", "head");
        if (h.length > 0) {
            l(h)
        } else {
            h = $("#pageJS", "body");
            if (h.length > 0) {
                l(h)
            }
        }
        document.body.addEventListener("touchmove", function (p) {
            if (IsMasked) {
                p.preventDefault()
            }
        }, false);
        var g = $("body").attr("fnav");
        if (g == "1" || g == "2" || g == "3") {
            var n = true;
            var i = '<div id="div_fastnav"  class="fast-nav-wrapper">';
            i += '<ul class="fast-nav">';
            if (g != "3") {
                i += '<li id="li_menu"><a href="javascript:;"><i class="nav-menu"></i></a></li>'
            }
            if (g != "2") {
                i += '<li id="li_top" style="display:none;"><a href="javascript:;"><i class="nav-top"></i></a></li>'
            }      
            i += '<li id="li_back" style="border-top: 1px solid #8b8b8b;"><a href="javascript:history.go(-1);"><i class="nav-fanhui"></i></a></li>';
            i += "</ul>";
            if (g != "3") {
                i += '<div class="sub-nav" style="display:none;">';
                i += '<a href="' + webPath + 'index"><i class="home"></i>首页</a>';
                i += '<a href="' + webPath + 'cateList"><i class="announced"></i>分类</a>';
                i += '<a href="' + webPath + 'search"><i class="single"></i>搜索</a>'; 
                i += '<a href="' + webPath + 'userCenter"><i class="personal"></i>我的</a>';
                i += '<a href="' + webPath + 'cart"><i class="shopcar"></i>购物车</a>';
                if (Gobal.IsWeixin) {  
                    if ($("#hidOpenID").val() == "") {
                        i += '<a href="javascript:;" id="a_subscribe"><i class="follow"></i>关注微信号</a>'
                    }
                    if (!_IsIOS) {
                        i += "<a href=\"javascript:location.replace('" + location.href.replace(/\/v(\d+)\//, "/v" + Math.ceil(Math.random() * 1000) + "/") + '\');"><i class="refresh"></i>刷新</a>'
                    }
                }
                i += "</div>"
            }
            i += "</div>";
            var o = $("#div_fastnav");
            if (o.length == 0) {
                o = $(i)
            }
            if (g != "3") {
                var d = $(".sub-nav", o);
                var k = d.children("a").length;
                if (k == 4) {
                    d.addClass("four")
                } else {
                    if (k == 5) {
                        d.addClass("five")
                    } else {
                        if (k == 6) {
                            d.addClass("six")
                        } else {
                            if (k == 7) {
                                d.addClass("seven")
                            }
                        }
                    }
                }
                var c = $("#li_menu", o);
                var f = null;
                c.bind("click", function () {
                    if (n == false) {
                        return
                    }
                    if (f != null) {
                        clearTimeout(f)
                    }
                    if ($(this).attr("isshow") == "1") {
                        d.fadeOut("fast");
                        $(this).attr("isshow", "0")
                    } else {
                        d.fadeIn("fast", function () {
                            f = setTimeout(function () {
                                d.fadeOut("fast");
                                c.attr("isshow", "0")
                            }, 5000)
                        });
                        $(this).attr("isshow", "1")
                    }
                });
                o.bind("click", function (p) {
                    stopBubble(p)
                }).find("#a_subscribe").bind("click", function () {
                    d.fadeOut("fast");
                    c.attr("isshow", "0");
                    var p = function () {
                        var r = null;
                        var q = '<div class="index-code-wrap"><h6>长按识别二维码</h6><div class="code"><img src="http://oy44y8tcw.bkt.clouddn.com/qrcode_for_gh_608d2370fe47_430.jpg" alt="Yago商城"></div><a  href="javascript:;" class="close-code"><i class="z-set"></i></a></div>';
                        var s = function () {
                            $("a.close-code").click(function () {
                                r.cancel()
                            })
                        };
                        r = new $.PageDialog(q, {
                            W: 300,
                            H: 300,
                            autoClose: false,
                            ready: s,
                            isTop: false
                        })
                    };
                    Base.getScript(Gobal.Skin + "/js/pageDialog.js?v=160304", p)
                });
                $("html").bind("click", function () {
                    d.fadeOut("fast");
                    c.attr("isshow", "0")
                })
            }
            if (g != "2") {
                var j = $("#li_top", o);
                j.bind("click", function () {
                    $(this).hide();
                    $("body,html").animate({
                        scrollTop: 0
                    }, 500)
                });
                $(window).scroll(function () {
                    if ($(window).scrollTop() > 100) {
                        j.show()
                    } else {
                        j.hide()
                    }
                })
            }
            o.appendTo("body")
        }
        var e = $(".footer").find("a");
        e.on("touchstart", function () {
            if (!$(this).hasClass("hover")) {
                e.removeClass("active").eq(e.index(this)).addClass("active");
                setTimeout(function () {
                    e.removeClass("active")
                }, 1000)
            }
        })
        
        var GORDER_TIP = null;
        var lastGetTime = "";
        var orderNewsTip = function(){
        	var isOpenOrderTip = $("#isOpenOrderTip").val();
        	if(isOpenOrderTip == undefined || isOpenOrderTip != '1'){
        		return false; 
        	}   
        	console.log("定时请求最新订单");
        	$.getJSON(contextPath + "getLastNewOrder&lastGetTime=" + lastGetTime,function(json){
        		console.log(jsonToStr(json));  
        		if(json.code == 0){  
        			var ui = '<div id="fastOrderTip" style="position:fixed;background:#000;opacity:.7;line-height:30px;border-radius:30px;-moz-border-radius:30px; top:50px;padding-right:10px;left:10px;width:auto;height:30px;color:#fff;z-index:9999;">'
        				+ '	<img style="float:left;vertical-align: middle;width:30px;height:30px;border-radius:50%;" src="' + json.headUrl + '">'
        				+ '	<span style="float:left;margin-left:5px;max-width:60px;overflow: hidden;white-space: nowrap;">' + json.nick + '</span><span style="float:left;margin:0px 5px;">下了一笔订单</span>'
        				+ '	<em style="float:left;">' + json.time + '秒前</em>'
        				+ '</div>'  
        				var G = $(ui);
        			lastGetTime = json.lastGetTime; 
            		$(document.body).append(G);
            		setTimeout(function(){G.remove();}, 4000);
        		}  
        		else{
        			console.log("无最新订单需要处理");
        		}
        	});
        }   
        //GORDER_TIP = setInterval(orderNewsTip, 6000);
    };
    Base.getScript(Gobal.Skin + "/js/Comm.js?v=160304", a)
})();