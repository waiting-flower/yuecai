/**
 * 
 */
$(function(){
	var fail = function (f) {
		$.PageDialog.fail(f)
	};
	var ok = function (f,r) {
		$.PageDialog.ok(f,r)
	};
	var goodsId = $("#goodsId").val();
	var goodsType = $("#goodsType").val();

	$(".sc_btn").click(function(){
		var isCollect = $("#isCollect").val();
		if($(this).hasClass("act")){
			$.getJSON(contextPath + "saveCancleGoodsCollect",{
    			id : goodsId
    		},function(json){
    			if(json.code == 10){
    				location.href = webPath + "login";
    			}
    			else if(json.code == 0){ 
    				$.PageDialog.ok("取消收藏成功",function(){
    					$(".sc_btn").removeClass("act");   
    					$(".sc_btn").find("i").removeClass("fa-heart").addClass("fa-heart-o");
    					$(".sc_btn").find("span").html("收藏");
    				});  
    			}
    			else if(json.code == 1){ 
    				$.PageDialog.ok("取消收藏成功");
    			}
    		});
		}
		else{
			$.getJSON(contextPath + "saveGoodsCollect",{
    			id : goodsId
    		},function(json){
    			if(json.code == 10){
    				location.href = webPath + "login";
    			}
    			else if(json.code == 0){
    				$.PageDialog.ok("收藏成功",function(){
    					$(".sc_btn").addClass("act"); 
    					$(".sc_btn").find("i").removeClass("fa-heart-o").addClass("fa-heart");
    					$(".sc_btn").find("span").html("已收藏");
    				}); 
    			}
    			else if(json.code == 1){
    				$.PageDialog.ok("收藏成功");
    			} 
    		});
		}
	});
	var currSkuId = $("#currSkuId").val();
	var currStockNum = $("#currStockNum").val();
	var skuListJson = null;
	$.getJSON(contextPath + "getGoodsSkuJson&id=" + goodsId,function(json){
		console.log(json); 
		skuListJson = json.skuJson; 
	});
	var buyNum = $("#buyNum");
	var buyNum2 = $("#buyNum2");
	var init = function(){ 
		$(".shareBox").click(function(){
			$("#shareRule").show();
		});
		$(".masktz").click(function(){
			$("#shareRule").hide();
		});   
		var imgArr = imgList.split(",");
		var ui = "";
		$.each(imgArr,function(i,item){
			ui += '<div class="slide"><img style="width:100%;" src="' + item + '"></div>';
		})
		$("#slider_img_box").html(ui);
        $('.slider8').bxSlider({ 
			infiniteLoop: true, 
			adaptiveHeight: true,  
			startSlides: 1, 
	        slideMargin: 10,  
	        auto: true,  
	        pager: true,                           // true / false - display a pager 
	        pagerSelector: '#slider_pager',                // jQuery selector - element to contain the pager. ex: '#pager' 
	        pagerType: 'short',                  // 'full', 'short' - if 'full' pager displays 1,2,3... if 'short' pager displays 1 / 4   如果设置full，将显示1，2，3……，如果设置short，将显示1/4 . 
	        pagerLocation: 'bottom',            // 'bottom', 'top' - location of pager 页码的位置 
	        pagerShortSeparator: '/'          // string - ex: 'of' pager would display 1 of 4 页面分隔符 
	    });//图片轮播组件
        
		paramsInfoEvent();
        var initClickEvent = function(){
        	//商品数量变更的点击事件
        	$("#btn_minus").click(function(){
        		var n = parseInt(buyNum.val());
        		if(n <= 1){
        			$("#btn_minus").addClass("disable")
        			return false; 
        		}
        		buyNum.val(n - 1);
        		buyNum2.val(n - 1);
        		refreshTip();
            });
        	$("#btn_minus2").click(function(){
        		$("#btn_minus").click();
        	});
            $("#btn_plus").click(function(){
            	var n = parseInt(buyNum.val());
            	buyNum.val(n + 1);
            	buyNum2.val(n + 1);
            	if($("#btn_minus").hasClass("disable")){
            		$("#btn_minus").removeClass("disable")
            	}
            	refreshTip();
            }); 
            $("#btn_plus2").click(function(){
        		$("#btn_plus").click();
        	}); 
            $("#serviceArea").click(function(){
            	$("#popupServe").addClass("show"); 
            });
            $("#closeServe").click(function(){
            	$("#popupServe").removeClass("show"); 
            });
            $("#btn_serve").click(function(){
            	$("#popupServe").removeClass("show"); 
            }); 
            
            $("#choiceGoods").click(function(){
            	$("#pintuanLayer").addClass("show"); 
            });
            $("#closeGoods").click(function(){
            	$("#pintuanLayer").removeClass("show"); 
            }); 
            $("#btnBuyGoods").click(function(){//购买商品
            	startBuyGoods();
            }); 
            $("#btn_kaituan").click(function(){
            	$("#pintuanLayer").addClass("show");  
            });
        };
        initClickEvent();  
	};  
	
	var startBuyGoods = function(){ 
		var goodsNum = buyNum.val(); 
		var i = function(){
			return "skuId=" + currSkuId + "&num=" + goodsNum;
		} 
		console.log(i()); 
		$.getJSON(contextPath + "createBuyGoodsReady",i(),function(json){
			if(json.code == 10){
				$.PageDialog.ok("您尚未登录",function(){
					location.href = webPath + "login";   
				});
				return false;
			}
			if(json.code == 1){ 
				$.PageDialog.fail(json.msg);
				return false;
			}
			else if(json.code == 0){      
//				alert("购买的链接地址为：" + webPath + "confirmOrder=1&orderId=" + json.orderId);
				location.href = webPath + "confirmOrder=1&orderId=" + json.orderId;
			}
		});
    	$("#pintuanLayer").removeClass("show"); 
	};
	refreshTip(); 
	/**
	 * 变更参数配置之后
	 * 需要通过ajax到后台获取该参数的产品数据
	 * 商品加入购物车，需要保存商品ID，商品规格ID，数量3个参数
	 */
	function paramsInfoEvent(){
		$(".sku_select span").click(function(){ 
			$(this).addClass("selected").siblings().removeClass("selected"); 
			refreshTip();
		}); 
		$(".sku_list span").click(function(){
			$(this).addClass("option_selected").siblings().removeClass("option_selected"); 
			var pName = $(this).attr("paramsName");
			var itemName = $(this).attr("itemName");
			var spanArr = $(".sku_select").find("span");
			for(var i = 0; i < spanArr.length; i++){
				var spanItem = $(spanArr[i]);   
				console.log( pName + "=====" + itemName  + spanItem.attr("itemName"));   
				if(spanItem.attr("paramsName") == pName && spanItem.attr("itemName") == itemName){
					console.log("匹配到了");  
					spanItem.click(); 
					break;
				}
			} 
			refreshTip();
		});
	}
	/**
	 * 刷新选中提示信息
	 */
	function refreshTip(){
		var spans = $(".sku_select").find("span.selected");
		var ui = [];
		for(var i = 0; i < spans.length;i++){ 
			var s = spans[i];  
			var name = $(s).html();
			console.log("refreshTip:" + name); 
			ui.push(name);
		}  
		var len = spans.length;
		if(len == 1){
			//只有一个参数的时候
			//有两个参数的时候 
			if(skuListJson != null){
				var span1 = $(spans[0]);
				var p1 = span1.attr("paramsName"); 
				var v1 = $(span1).html();
				console.log(p1 + ":" + v1); 
				for(var i = 0; i < skuListJson.length;i++){
					var item = skuListJson[i];
					var paramsJson = JSON.parse(item.parmasValuesJson);
					console.log(paramsJson[p1]);   
					if(paramsJson[p1] == v1){
						//参数匹配的情况下
						console.log("找到了");
						console.log(item.price);  
						var stockNum = item.stockNum;
						var imgUrl = item.imgUrl;
						if(imgUrl != null){
							$("#mask_goodsImgUrl").attr(item.imgUrl); 
						}
						$("#tip_sku").html(stockNum); 
						$("#mask_price").html(item.price);
						$("#goodsPrice").html(item.price);  
						currStockNum = stockNum; 
						currSkuId = item.id; 
						if(stockNum < 1){
							fail("库存不足");
							
						}
						break;
					}
				}
			}
		}
		else if(len == 2){ 
			//有两个参数的时候 
			if(skuListJson != null){
				var span1 = $(spans[0]);
				var span2 = $(spans[1]);
				var p1 = span1.attr("paramsName"); 
				var v1 = $(span1).html();
				var p2 = span2.attr("paramsName");
				var v2 = $(span2).html();
				console.log(p1 + v1 + "\n" + p2 + v2); 
				for(var i = 0; i < skuListJson.length;i++){
					var item = skuListJson[i];
					var paramsJson = JSON.parse(item.parmasValuesJson);
					console.log(paramsJson[p1]);   
					if(paramsJson[p1] == v1 && paramsJson[p2] == v2){
						//参数匹配的情况下
						console.log("找到了");
						console.log(item.price);  
						var stockNum = item.stockNum;
						var imgUrl = item.imgUrl;
						if(imgUrl != null){
							$("#mask_goodsImgUrl").attr(item.imgUrl); 
						}
						$("#tip_sku").html(stockNum); 
						$("#mask_price").html(item.price);
						$("#goodsPrice").html(item.price);  
						currStockNum = stockNum; 
						currSkuId = item.id;
						if(stockNum < 1){
							fail("库存不足");
							
						}
						break;
					}
				}
			}
 		}
		else if(len == 3){
			//有三个参数的时候
			if(skuListJson != null){
				var span1 = $(spans[0]);
				var span2 = $(spans[1]);
				var p1 = span1.attr("paramsName"); 
				var v1 = $(span1).html();
				var p2 = span2.attr("paramsName");
				var v2 = $(span2).html();
				var p3 = span3.attr("paramsName");
				var v3 = $(span3).html();
				console.log(p1 + v1 + "\n" + p2 + v2); 
				for(var i = 0; i < skuListJson.length;i++){
					var item = skuListJson[i];
					var paramsJson = JSON.parse(item.parmasValuesJson);
					console.log(paramsJson[p1]);    
					if(paramsJson[p1] == v1 && paramsJson[p2] == v2 && paramsJson[p3] == v3){
						//参数匹配的情况下
						console.log("找到了"); 
						console.log(item.price);  
						var stockNum = item.stockNum;
						var imgUrl = item.imgUrl;
						if(imgUrl != null){
							$("#mask_goodsImgUrl").attr(item.imgUrl); 
						}
						$("#tip_sku").html(stockNum); 
						$("#mask_price").html(item.price);
						$("#goodsPrice").html(item.price);  
						currStockNum = stockNum; 
						currSkuId = item.id;
						if(stockNum < 1){
							fail("库存不足");
							
						}
						break;
					}
				}
			}
		}
		var tt = ui.join(",") + '&nbsp;&nbsp;&nbsp;<span class="amount">' + buyNum.val() + '件</span>';
		$("#specDetailInfo").html(tt);
		$("#tip_select").html(tt); 
	} 
	
	$("#btnToCard").click(function(){
		//加入购物车 
		console.log("用户点击加入购物车事件");
		if(goodsType != "1"){
			fail("非实物商品，请直接购买"); 
			return false;
		}
		$.getJSON(contextPath + "saveGoodsToCard",{
			skuId : currSkuId,
			num : buyNum.val() 
		},function(json){
			if(json.code == 10){
				ok("您尚未登录",function(){
					location.href = webPath + "login";
				}); 
			} 
			else if(json.code == 1){
				fail("库存不足");
				$("#pintuanLayer").removeClass("show"); 
			}
			else{  
				ok("加入成功",function(){
					$("#pintuanLayer").removeClass("show");  
				});
			}
		});
	});
	
	var loadData = function(){
		var u = function(){
			var i = function(){
				return "currPage=1&pageSize=5&goodsId=" + goodsId;
			}
			$.getJSON(contextPath + "getGoodsCommentList",i(),function(json){
				if(json.code == 1){
					$("#commentList_div").hide();
					$("#evalNone").show();
				}
				else{
					$("#evalNo2").html(json.totalRecords);
					$.each(json.listItems,function(i,item){
						var skuStr = "";
						var sku = JSON.parse(item.sku);
						for(var kk in sku){
							console.log(kk);
							console.log(sku[kk]);  
							skuStr += kk + "&nbsp;" + sku[kk] + "&nbsp;";
						} 
						
						var score = item.score;
						var width = score/5 *100;
						var imgList = item.imgList;
						var imgHtml = "";
						if(imgList != null && imgList != ""){
							imgHtml += '<div class="cmt_att">';
							var imgArr = imgList.split(",");
							for(var i = 0; i < imgArr.length; i++){    
								imgHtml += '<span style="margin-right:10px;"  class="img"><img src="' + imgArr[i] + '"></span>';
							} 
							imgHtml += '</div>'; 
						}
						var ui = '<li>' 
							+ '    <div class="cmt_user" ptag="">'
							+ '        <span class="user">' + item.nick + '</span> '
							+ '        <span class="credit"><span style="width: ' + width + '%"></span></span> <span class="date">' + item.msgTime + '</span>'
							+ '    </div>'
							+ '    <div class="cmt_cnt" ptag="">' + item.msgContent + '</div>'
							+ imgHtml
							+ '    <div class="cmt_sku">'   
							+ '        <span>' + skuStr + '</span>'
							+ '        <div class="reply">' + item.replyNum
							+ '            <a  href="' + webPath + 'commentView&goodsId=' + $("#goodsId").val() + '&commentId=' + item.id + '" class="btn">回复</a>'
							+ '        </div>' 
							+ '    </div>'
							+ '</li>'; 
						$("#evalDet_main").append(ui);
					});
				}
			});
		}
		this.initData = function(){
			u();
		}
	}
	var recFun = new loadData();
	recFun.initData();  
	
	
	    
	Base.getScript(Gobal.Skin + "js/pageDialog.js?v=160304", function () {
		Base.getScript(Gobal.Skin + "js/jquery.bxslider.js?v=160523", function(){
			Base.getScript(Gobal.Skin + "js/CartComm.js?v=160523", init);
			
		});
    });
});