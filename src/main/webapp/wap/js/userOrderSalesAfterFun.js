$(function(){
	var fail = function (r) {
		$.PageDialog.fail(r);
    };
    var ok = function (s, r) {
    	$.PageDialog.ok(s, r);
    };
    
	var init = function(){
		$(".questionList .ques_item").click(function(){
			$(".questionList .ques_item").removeClass("curr");
			$(this).addClass("curr");
			var cate = $(this).attr("data-cate");
			if(cate != "1"){
				$("#wuliuBox").show();
			}
			else{
				$("#wuliuBox").hide(); 
			}
		});
		$(".serviceList .ques_item").click(function(){
			$(".serviceList .ques_item").removeClass("curr");
			$(this).addClass("curr"); 
		});
		
		$("#saveShouhou").click(function(){
			var orderDetailId = $("#orderDetailId").val();
			var expressName = $("#expressName").val();
			var expressCode = $("#expressCode").val();
			var questionId = $(".questionList").find(".curr").attr("data-cate");
			var question = $(".questionList").find(".curr").attr("ques");
			var service = $(".serviceList").find(".curr").attr("service");
			var info = $("#info").val();
			var imgList = [];
			var imgArr = $("#imgList").find(".uploadImg_shouhou");
			for(var i = 0; i < imgArr.length; i++){ 
				var img = $(imgArr[i]).find("img").attr("src");
				imgList.push(img);  
			}
			 
			if(question == '' || question == undefined){
				fail("请选择问题原因");
				return false;
			}   
			if(service == "" || service == undefined){
				fail("请选择售后服务");
				return false;
			}
			if(info == ""){
				fail("请输入问题描述");
				return false;
			}
			
			if(imgArr.length < 1){
				fail("请上传图片");
				return false;
			}
			
			if(questionId != "1"){
				if(expressName == ""){
					fail("请输入快递公司名称");
					return false;
				}
				if(expressCode == ""){
					fail("请输入快递公司单号");
					return false;
				} 
			}
			var vData = {
				question : question,
				service : service,
				imgList : imgList.join(","),
				info : info,
				expressName : expressName,
				expressCode : expressCode,
				detailId : orderDetailId
			};
			$.post(contextPath + "saveSalesAfter",vData,function(json){
				if(json.code == 0){
					ok("提交成功，请等待商家处理",function(){
						location.href = webPath + "orderSalesAfterList"; 
					});
				}
				else if(json.code == 1){
					fail("您已经提交过了售后服务，请不要重复提交");
				}
				else if(json.code == 2){
					fail("收货已经超过7天，不能退款");
				}
			},"json"); 
		});
		
		var uploadImg = $("#uploadImg"); //售后照片
		uploadImg.bind("change",function(){
			var m = $(this).val();
	        if (m != "") {
	             var l = (m.substr(m.length - 5)).substr((m.substr(m.length - 5)).indexOf(".") + 1).toLowerCase();
	             if (l != "gif" && l != "jpg" && l != "jpeg" && l != "bmp" && l != "png") {
	                 $(this).val("");
	                 fail("只能上传JPG、GIF、PNG或BMP图片!");
	                 return false
	             } else {
	                fail("上传中,请稍后");
	             }
	         } else {
	             fail("请先选择需要上传的文件!");
	             return false
	         }
	        var size = 0;
	        size = this.files[0].size
	        var myEsize = (size/1024).toFixed(2);
			if(myEsize > 10240){
				fail("上传文件不能超过10M"); 
				return false;
			}
			console.log("图片大小验证通过，开始执行上传");
			start_upload(this,size,"idCardPerson_img");
		}); 
		
		
		
		var start_upload = function(obj,size,imgId){  
	        if(!isCanvasSupported){  
	           console.log("不支持的浏览器");  
	        }else{  
	           compress(event, function(base64Img){  
	               // 如需保存  或 把base64转成图片  ajax提交后台处理  
	               putb64(base64Img,size,imgId);
	           });  
	        }   
		}   
		
		var appImg = function(url,imgId){
			$("#" + imgId).attr("src",url);
			var ui = "<div class='imgItem uploadImg_shouhou'><div class='del'>删除</div><img src='" + url + "' style='height:80px;'></div>";
			var G = $(ui);
			G.find(".del").click(function(){
				$(this).parent().remove(); 
			});          
			$("#imgList").prepend(G);  
		}
		//七牛云base64上传图片
		var putb64 = function(pic, size,imgId) {
			var token = function(){
				console.log("获取token令牌");
				$.getJSON(contextPath + "getUpToken",function(json){//token令牌获取
				
					console.log("开始上传七牛云");
					console.log("令牌获取成功，开始执行上传");
					var url = "http://upload.qiniu.com/putb64/-1"; 
					var xhr = new XMLHttpRequest();
					xhr.onreadystatechange = function() {
						console.log(xhr.readyState);
						if (xhr.readyState == 4) {
							console.log("返回text" + xhr.responseText);
							var ttt = JSON.parse(xhr.responseText); 
							var key = ttt.key;
							console.log("上传成功，得到key" + key);
							appImg("http://oy44y8tcw.bkt.clouddn.com/" + key,imgId);//添加图片
						}    
					}
					xhr.open("POST", url, true);
					xhr.setRequestHeader("Content-Type", "application/octet-stream");
					xhr.setRequestHeader("Authorization","UpToken " + json.upToken);//json.uptoken
					xhr.send(pic.split(",")[1]);
				});
			}
			token();
		}
	}; 
	Base.getScript(Gobal.Skin + "/js/pageDialog.js?v=160606",init);
});