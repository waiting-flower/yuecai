/**
 * 
 */
$(function(){
	var init = function(){
		var l = $(".loading");
		var op = $("#op").val();
		//dialog confrim
		var r = function (L, F, G, E, N) {
			
	         var H = 255;
	         var K = 126;
	         if (typeof (E) != "undefined") {
	             H = E
	         }
	         if (typeof (E) != "undefined") {
	             K = N
	         }
	         var M = null;
	         var O = '<div class="clearfix m-round u-tipsEject"><div class="u-tips-txt">' 
	         	+ L + '</div><div class="u-Btn"><div class="u-Btn-li"><a href="javascript:;" id="btnMsgCancel" class="z-CloseBtn">取消</a></div><div class="u-Btn-li"><a id="btnMsgOK" href="javascript:;" class="z-DefineBtn">确定</a></div></div></div>';
	         var J = function () {
	             var P = $("#pageDialog");
	             P.find("a.z-DefineBtn").click(function () {
	                 if (typeof (F) != "undefined" && F != null) {
	                     F();
	                 }
	                 I.close();
	             });
	             P.find("a.z-CloseBtn").click(function () {
	                 if (typeof (G) != "undefined" && G != null) {
	                     G()
	                 }
	                 I.cancel()
	             })
	         };
	         var I = new $.PageDialog(O, {
	             W: H,
	             H: K,
	             close: true,
	             autoClose: false,
	             ready: J
	         })
	     };
		var g = function(){ 
			$.getJSON(contextPath + "getAddressList",function(json){
				l.hide();  
				if(json.code == 10){
					$.PageDialog.ok("您未登录",function(){
						location.href = webPath + "login";
					});
				}
				else if(json.code == 1){
					$("#addList").html(Gobal.NoneHtml);  
				}
				else if(json.code == 0){
					$("#addList").html("");  
					var orderId = $("#orderId").val();
					var tradeNo = $("#tradeNo").val();
					var scoreGoodsId = $("#scoreGoodsId").val();
					var num = $("#num").val();
					$.each(json.list,function(i,item){  
						var ui = '<div class="item" style="margin-top:10px;">' 
							+ '    <div class="tag" style="display:none;">点击选择</div>'
							+ '    <div class="tit">';
						if(item.isDefault == "1"){
							ui = ui + '        <span>收货地址【默认地址】</span>'
							+ '        <div class="opt">';      
						}
						else{ 
							ui = ui + '        <span>收货地址</span>'
							+ '        <div class="opt">'       
							+ '            <i class="fa fa-eraser set_default_address" style="bakcground:#f60;">设置为默认</i>'
						}
							
						ui = ui	+ '            <i class="fa fa-pencil edit"></i>'
							+ '            <i class="fa fa-times close"></i>'
							+ '        </div>' 
							+ '    </div>' 
							+ '    <div class="addrName"><i style="margin-right:5px;" class="fa fa-user"></i>' + item.name + '</div>'
							+ '    <div class="addrInfo">' + item.pro + '-' + item.city + '-' + item.county + ' ' + item.info + '</div>'
							+ '   <div class="addrTel"><i style="margin-right:5px;" class="fa fa-mobile"></i>' + item.mobile + '</div>'
							+ '</div>'; 
						var G = $(ui);
						if(orderId != ""){ 
							G.click(function(){
								location.href = webPath + "confirmOrder&orderId=" + orderId + "&addId=" + item.id;
							});
						} 
						if(tradeNo != ""){ 
							G.click(function(){
								location.href = webPath + "confirmCartOrder&tradeNo=" + tradeNo + "&addId=" + item.id;
							});
						} 
						if(scoreGoodsId != ""){
							G.click(function(){
								location.href = webPath + "scoreGoodsConfirm&goodsId=" 
									+ scoreGoodsId + "&num=" + num + "&addId=" + item.id;
							});
						}
						G.find(".set_default_address").click(function(T){
							stopBubble(T);
							var S = function(){ 
								$.getJSON(contextPath + "setDefaultAddress&id=" + item.id,function(json){
									if(json.code == 0){
										$.PageDialog.ok("设置成功",function(){
											location.reload();
										});
									} 
									else if(json.code == 1){
										$.PageDialog.fail(json.msg);
									}
								});
							}
							r("是否确认设置该地址为默认地址", S);
						});
						G.find(".edit").click(function(T){
							stopBubble(T);
							location.href = webPath + "userAddressDetail&id=" + item.id;
						});
						G.find(".close").click(function(){
							var S = function(){ 
								$.getJSON(contextPath + "deleteAddress&id=" + item.id,function(json){
									if(json.code == 0){
										$.PageDialog.ok("删除成功",function(){
											location.reload();
										});
									} 
									else if(json.code == 1){
										$.PageDialog.fail(json.msg);
									}
								});
							}
							r("是否删除该地址", S);
						});
						$("#addList").append(G);
					});
				}
			});
		}
		g();
	};
	Base.getScript(Gobal.Skin + "/js/pageDialog.js?v=160606",init);
});