$(function () {
	var storeId = $("#storeId").val();
	var params = {
			currPage : 1,
			pageSize : 10,
			sortType : 1
	};
	var divLoading = $("#divLoading");
	var _isLoading = false;
	
    var init = function () {
    	$(".mask-filter-div").click(function(){
    		$(".mask-filter-div").removeClass("show");
    		$(".show-temark-div").removeClass("show");
    	}); 
    	$(".kefu_show").click(function(){
    		$(".mask-filter-div").addClass("show");
    		$(".show-temark-div").addClass("show");
    	});
    	$("#sliderBox").flexslider({
            slideshow: true
        }); 
    	$(".footer_item").click(function(){
    		$(this).toggleClass("active"); 
    	}); 
    	$(".store_gz").click(function(){
    		if($(this).hasClass("act")){
    			$.getJSON(contextPath + "saveCancleStoreCollect",{
        			id : storeId
        		},function(json){
        			if(json.code == 10){
        				location.href = webPath + "login";
        			}
        			else if(json.code == 0){ 
        				$.PageDialog.ok("取消关注成功",function(){
        					$(".store_gz").removeClass("act");   
        					$(".store_gz").find("i").removeClass("fa-heart").addClass("fa-heart-o");
        					$(".store_gz").find("span").html("点击关注");
        				});  
        			}
        			else if(json.code == 1){ 
        				$.PageDialog.ok("取消关注成功");
        			}
        		});
    		}
    		else{
    			$.getJSON(contextPath + "saveStoreCollect",{
        			id : storeId
        		},function(json){
        			if(json.code == 10){
        				location.href = webPath + "login";
        			}
        			else if(json.code == 0){
        				$.PageDialog.ok("关注成功",function(){
        					$(".store_gz").addClass("act"); 
        					$(".store_gz").find("i").removeClass("fa-heart-o").addClass("fa-heart");
        					$(".store_gz").find("span").html("已关注");
        				}); 
        			}
        			else if(json.code == 1){
        				$.PageDialog.ok("关注成功");
        			}
        		});
    		}
    		
    	});
    	
    	var loadData = function(){
			var u = function(){
				divLoading.show();
				var i = function(){
					return "currPage=" + params.currPage + "&pageSize=" + params.pageSize 
							+ "&sortType=" + params.sortType + "&storeId=" + storeId;
				} 
				console.log("请求参数=" + i()); 
				$.getJSON(contextPath + "getGoodsList",i(),function(json){
					if(json.code == 1){//没有任何记录的情况
						_isLoading = true;   
						console.log("无记录显示图片"); 
						$("#noneTipDiv").html('<div class="wx1yyg-kylin-norecords"><i class="wx1yyg-kylin-icon kylin-null-06"></i><strong class="gray9">暂无商品哦~</strong></div>')
					}
					else{ 
						$("#goodsCount").html(json.totalRecords); 
						$.each(json.listItems,function(i,item){  
							var ui = '<div class="search_prolist_item item_longcover" id="' + item.id + '" >'
								+ '    <div class="search_prolist_item_inner" >'
								+ '        <div class="search_prolist_cover" rd="0-4-4">'
								+ '            <img class="photo" src="' + item.imgUrl + '">'
								+ '        </div>'
								+ '        <div class="search_prolist_info">'
								+ '            <div class="search_prolist_title" data-line="1" rd="0-4-4">' + item.name 
								+ '            </div>'
								+ '            <div class="search_prolist_price">' 
								+ '                <strong ><em><span class="int">￥' + item.price + '</span></em> </strong>'
								+ '            </div>'
								+ '            <div class="search_prolist_other text_small" rd="0-4-4">'
								+ '                <span class="search_prolist_comment"><span id="com_16983074369">' + item.commentNum +
									'</span>人评价</span> <span class="search_prolist_rate">好评率' + (item.niceComment * 100) + '%</span> '
								+ '            </div>'
								+ '        </div>'
								+ '    </div>'
								+ '</div>'
							var G = $(ui); 
							console.log(G);
							G.click(function () { 
	                            location.href = webPath + "goodsDetail&id=" + $(this).attr("id");
	                        });
							$("#itemList").append(G); 
						}); 
						if(json.totalPage <= params.currPage){
							console.log("没有更多记录哦"); 
							$("#noneTipDiv").html(Gobal.NoneHtml2("没有更多记录了")); 
							_isLoading = true;
						} 
						else{
							_isLoading = false;
						}
					}
					divLoading.hide();
				});
			}
			this.initData = function(){ 
				$("#itemList").empty(); 
				u();
			}
			this.getNextPage = function(){
				params.currPage++;
				u();
			}
		} 
		recFun = new loadData();
		recFun.initData();   
    };
    Base.getScript(Gobal.Skin + "/js/pageDialog.js?v=160606", function () {
        Base.getScript(Gobal.Skin + "js/Flexslider.js?v=160722", function(){
        	Base.getScript(Gobal.Skin + "js/search.js?v=160722", init);  
        });
    });
});